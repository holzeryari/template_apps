# properties file
source /home/tomcat/conf/conf.properties

nohup java -XX:MaxRAM=500m -XX:+UseSerialGC -Xss512k -Xms64m -Xmx1584m -XX:MaxPermSize=1814m -XX:MaxMetaspaceSize=1200m -jar /home/tomcat/api-crm/api-crm.jar -Dsun.misc.URLClassPath.disableJarChecking=true 1>/home/tomcat/api-crm/logs/api-crm.log 2>&1 &
echo api-crm started
