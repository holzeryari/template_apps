#!/bin/bash

# properties file
source /home/tomcat/conf/conf.properties

export JAVA_OPTS="-server -Xss2048k -Xms64m -Xmx1084m -XX:MaxPermSize=768m -verbose:gc -XX:+UseConcMarkSweepGC -XX:+CMSIncrementalMode"
export JAVA_HOME=/java
export CATALINA_HOME=/usr/local/tomcat8
export CATALINA_BASE=/home/tomcat/api-rus
export PATH=$PATH:$JAVA_HOME/bin:$CATALINA_HOME/bin

# Setea el entorno para utilizar el archivo application-env.yml
export CATALINA_OPTS="-Dspring.profiles.active=env"
