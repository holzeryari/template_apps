#!/bin/bash

export JAVA_HOME=/java6
export CATALINA_HOME=/usr/local/tomcat6
export CATALINA_BASE=/home/tomcat/compras
export PATH=$PATH:$JAVA_HOME/bin:$CATALINA_HOME/bin
export SIS_ENVIRONMENT_NAME=env
export JAVA_OPTS="-server -Xss2048k -Xms64m -Xmx1084m -XX:MaxPermSize=768m -verbose:gc -XX:+UseConcMarkSweepGC -XX:+CMSIncrementalMode"
export JAVA_OPTS=$JAVA_OPTS" -Djava.net.preferIPv4Stack=true -Dhttps.protocols=TLSv1.1,TLSv1.2"
