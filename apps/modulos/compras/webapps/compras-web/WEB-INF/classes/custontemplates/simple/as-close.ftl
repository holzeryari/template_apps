<#assign disabedControls = stack.findValue("disabedControl") />
<#assign checked=0>
<#if parameters.canPerform=="false" || parameters.enabled=="false" >
	<#assign checked=1>
</div><#rt/>
<#else>
	<#list disabedControls as disabedControl>
		<#if parameters.get("name")==disabedControl.name>
			<#assign checked=1>
</div><#rt/>
			<#break>
		</#if>
	</#list>	
</#if>
<#if checked == 0>
<#include "as-closeView.ftl" />
</#if>
