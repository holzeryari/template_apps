<#--
-->
<a<#rt/>
<#if parameters.id?if_exists != "">
 <#if parameters.canPerform=="false" || parameters.enabled=="false" >
 id="security-${parameters.id?html}"<#rt/>
 <#else>
 id="${parameters.id?html}"<#rt/>
 </#if>
</#if>
<#if parameters.href?if_exists != "">
 href="${parameters.href}"<#rt/>
</#if>
<#if parameters.tabindex?exists>
 tabindex="${parameters.tabindex?html}"<#rt/>
</#if>
<#if parameters.cssClass?exists>
 class="${parameters.cssClass?html}"<#rt/>
</#if>
<#if parameters.cssStyle?exists>
 style="${parameters.cssStyle?html}"<#rt/>
</#if>
<#if parameters.title?exists>
 title="${parameters.title?html}"<#rt/>
</#if>
<#include "scripting-events.ftl" />
<#include "common-attributes.ftl" />
><#rt/>