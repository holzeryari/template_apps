<#assign disabedControls = stack.findValue("disabedControl") />

<#assign checked=0>
<#list disabedControls as disabedControl>
	<#if parameters.get("name")==disabedControl.name>

		<#assign checked=1>
		<#if stack.findValue(parameters.get("name"))?exists>
			<#assign value = stack.findValue(parameters.get("name"))>
			${value?string("dd/MM/yyyy")}
		<#else>
			&nbsp;
		</#if>
		<#break>
	</#if>
</#list>
<#if checked == 0>

<#include "calendarView.ftl" />
</#if>
