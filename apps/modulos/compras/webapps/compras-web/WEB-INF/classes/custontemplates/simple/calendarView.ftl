<input type="text"<#rt/>
 name="${parameters.name?default("")?html}"<#rt/>
<#if parameters.get("size")?exists>
 size="${parameters.get("size")?html}"<#rt/>
</#if>
<#if parameters.maxlength?exists>
 maxlength="${parameters.maxlength?html}"<#rt/>
</#if>
<#if stack.findValue(parameters.get("name"))?exists>
<#assign value = stack.findValue(parameters.get("name"))>
 value="${value?string("dd/MM/yyyy")}"<#rt/>
</#if>
<#if parameters.disabled?default(false)>
 disabled="disabled"<#rt/>
</#if>
<#if parameters.readonly?default(false)>
 readonly="readonly"<#rt/>
</#if>
<#if parameters.tabindex?exists>
 tabindex="${parameters.tabindex?html}"<#rt/>
</#if>
<#if parameters.id?exists>
 id="${parameters.id?html}"<#rt/>
</#if>
<#if parameters.cssClass?exists>
 class="${parameters.cssClass?html}"<#rt/>
</#if>
<#if parameters.cssStyle?exists>
 style="${parameters.cssStyle?html}"<#rt/>
</#if>
<#if parameters.title?exists>
 title="${parameters.title?html}"<#rt/>
</#if>
<#include "scripting-events.ftl" />
<#include "common-attributes.ftl" />
/>
<a href="javascript://nop/"
	id="rc${parameters.id?html}"
	name="rc${parameters.id?html}"
	onClick="displayCalendar($('${parameters.id?html}'),'${parameters.format?html}',this); resizeWindows();"
	>
	<img src="${request.contextPath}/common/images/calendario.gif" alt="Seleccione una Fecha" title="Seleccione una Fecha" border="0" align="absmiddle" hspace="3"/>
</a>
