<#macro formatedMoney money>
<#assign moneyFormatted = money> 
${moneyFormatted?string("0.00")}
</#macro>

<#macro formatedShortAccount accountNumber>
XXXXX${accountNumber?substring(11,13)}/${accountNumber?substring(13)}
</#macro>

<#macro accountType accountNumber>
  <#assign type = accountNumber?substring(0,1)?number>
  
  <#if type == 0>
CA
  <#elseif type == 1>
CC
  <#elseif type == 2>
DL
  </#if>  
</#macro>

<#macro accountTypeFull accountNumber>
  <#assign type = accountNumber?substring(0,1)?number>
  <#if type == 0>
Cuenta Corriente
  <#elseif type == 1>
Caja de Ahorro
  <#elseif type == 2>
Caja de Ahorro Dolar
  </#if>
</#macro>


<#macro accountTypeByIdentifierSystem identifierSystem>
  <#if identifierSystem == "CT">
Credi Cuenta
  <#elseif identifierSystem == "CC">
Cuenta Corriente en Pesos
  <#elseif identifierSystem == "CA">
Caja de Ahorro en Pesos
  <#elseif identifierSystem == "US">
Cuenta Corriente en D&oacute;lares
  <#elseif identifierSystem == "DL">
Caja de Ahorro en D&oacute;lares
  </#if>
</#macro>

<#macro accountTypeByIdentifierSystemForSMS identifierSystem>
  <#if identifierSystem == "CT">
Credi Cuenta
  <#elseif identifierSystem == "CC">
Cuenta Corriente en Pesos
  <#elseif identifierSystem == "CA">
Caja de Ahorro en Pesos
  <#elseif identifierSystem == "US">
Cuenta Corriente en Dolares
  <#elseif identifierSystem == "DL">
Caja de Ahorro en Dolares
  </#if>
</#macro>

<#macro creditCardType card>
   <#assign type = card?substring(0,2)>
	<#if type == "CB">
Cabal
	<#elseif type == "MA">
Master Card
	<#elseif mark == "AM">
American Express
	<#elseif type == "VI">
Visa	  
	</#if>  
</#macro>

<#macro markType mark>
	<#if mark == "CB">
Cabal
	<#elseif mark == "MA">
Master
	<#elseif mark == "AM">
American Express
	<#elseif mark == "VI" >
Visa
	</#if>  
</#macro>

<#macro formatedDate date>
  <#assign dateGenerated = date?date("yyyyMMdd")>
${dateGenerated?trim}
</#macro>

<#macro formatedDateShort date>
  <#assign dateGenerated = date?date("yyMMdd")>

${dateGenerated?trim}
</#macro>


<#macro formatedTime time>
  <#assign timeGenerated = time?time("HHmmss")>

${timeGenerated?string.short}
</#macro>

<#macro formatedTimeHHmm time>
  <#assign timeGenerated = time?time("HHmm")>
  
${timeGenerated?string.short}
</#macro>
