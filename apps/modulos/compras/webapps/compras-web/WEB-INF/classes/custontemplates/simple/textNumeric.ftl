<#setting locale="en_US">
<#assign disabedControls = stack.findValue("disabedControl") />
<#assign checked=0>
<#list disabedControls as disabedControl>
	<#if parameters.get("name")==disabedControl.name>
		<#assign checked=1>
		<#if parameters.nameValue?exists>
 			<#assign value = stack.findValue(parameters.get("name"))>
${value?c} &nbsp;
		</#if>
		<#break>
	</#if>
</#list>
<#if checked == 0>

<#include "textNumericView.ftl" />
</#if>