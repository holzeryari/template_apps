<#if parameters.canPerform=="true">
	<a<#rt/>
	<#if parameters.id?if_exists != "">
		id="${parameters.id?html}"<#rt/>
	</#if>
	<#if parameters.href?if_exists != "">
		href="${parameters.href}"<#rt/>
	</#if>
	<#if parameters.tabindex?exists>
		tabindex="${parameters.tabindex?html}"<#rt/>
	</#if>
	<#if parameters.cssClass?exists>
		class="${parameters.cssClass?html}"<#rt/>
	</#if>
	<#if parameters.cssStyle?exists>
		style="${parameters.cssStyle?html}"<#rt/>
	</#if>
	<#if parameters.title?exists>
		title="${parameters.title}"<#rt/>
	</#if>
	<#if parameters.rel?exists>
		rel="${parameters.rel?html}"<#rt/>
	</#if>

	<#include "scripting-events.ftl" />
	<#include "common-attributes.ftl" />
	>

	<#if parameters.label?exists>
		${parameters.label}<#rt/>
	</#if>
	<#rt/>
</#if>
