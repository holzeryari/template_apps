<%@ taglib prefix="s" uri="/WEB-INF/tlds/struts-tags.tld"%>
<%@ taglib uri="/WEB-INF/tlds/ajaxtags.tld" prefix="ajax"%>

<table width="100%" border="0" cellspacing="0" cellpadding="0" style=" border-top:#FFFFFF 3px solid;border-left:#FFFFFF 10px solid;border-right:#FFFFFF 10px solid">
	<tr>
		<td CLASS="title-001">Funcionalidad fuera del alcance de esta release</td>
	</tr>
    <tr>
      <td style="border-top:#CCCCCC 1px solid">&nbsp;</td>
    </tr>
    <tr>
      <td height="400" align="right" class="txt-002"><table width="60%" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
          <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="0" bgcolor="#FAB070"><image src="${pageContext.request.contextPath}/common/images/tbl_crv_izq.gif" width="5" height="20" /></td>
                <td width="100%" bgcolor="#FAB070" class="txt-003"><strong>404</strong></td>
                <td width="0" bgcolor="#FAB070"><image src="${pageContext.request.contextPath}/common/images/tbl_crv_der.gif" width="5" height="20" /></td>
              </tr>
          </table></td>
        </tr>
        <tr>
          <td height="100" align="center" class="txt-002" style="border:#FAB070 1px solid; padding:2px">La transacci&oacute;n que intenta ejecutar no esta disponible en este momento. </td>
        </tr>

      </table></td>
    </tr>
  </table>