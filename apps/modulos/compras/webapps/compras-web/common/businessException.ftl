<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td height="50" align="left" class="txt-002" style="font-size:10px;background:#F7F8E0;border:#a5a4a3 1px solid; padding:1px;">
			<ul>
				<#list exception.descriptions as description >
					<#if description?exists>
						<li><span class="errorMessage">${description}</span></li>
					</#if>
				</#list>
			</ul>
		</td>
	</tr>
</table>