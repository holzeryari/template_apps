<#macro formatedDate date>
    <#assign dateGenerated = date?date("yyyyMMdd")>
    ${dateGenerated?string("dd/MM/yyyy")}
</#macro>
<#macro formatedDateTime date>
    <#assign dateGenerated = date?datetime>
    ${dateGenerated?string("dd/MM/yyyy HH:mm:ss")}
</#macro>