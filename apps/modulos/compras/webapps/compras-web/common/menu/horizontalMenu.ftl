<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>


	<!-- menu -->
	<div id="capaItemsMenu" class="capaItemsMenu">
		
		<ul>
		<@vc.anchors target="contentTrx">
			<li>
				<@security.a
					templateDir="menutemplates" 
					securityCode="CUF0740" 
					label="Maestros"
					rel="submenu1"
					href=""
					cssClass="cursorMenu"> 
				</@security.a>
			</li>
			
			<li>
				<@security.a 
					templateDir="menutemplates" 
					securityCode="CUF0741" 
					label="Dep&oacute;sitos"
					rel="submenu2"
					href=""
					cssClass="cursorMenu">
				</@security.a>
			</li>
			<li>
				<@security.a 
					templateDir="menutemplates" 
					securityCode="CUF0742" 
					label="Bienes"
					rel="submenu3"
					href=""
					cssClass="cursorMenu">
				</@security.a>
			</li>
			<li>
				<@security.a 
					templateDir="menutemplates" 
					securityCode="CUF0743" 
					label="Compras y Contrataciones"
					rel="submenu4"
					href=""
					cssClass="cursorMenu">
				</@security.a>
			</li>
			<li>
				<@security.a 
					templateDir="menutemplates" 
					securityCode="CUF0744" 
					label="Servicios"
					rel="submenu5"
					href=""
					cssClass="cursorMenu">
				</@security.a>
			</li>
			<li>
				<@security.a 
					templateDir="menutemplates" 
					securityCode="CUF0745" 
					label="Proveedores"
					rel="submenu6"
					href=""
					cssClass="cursorMenu">
				</@security.a>
			</li>
			 <#--<li>
				<@security.a 
					templateDir="menutemplates" 
					securityCode="CUF0746" 
					label="Carteleria"
					rel="submenu7"
					href=""
					cssClass="cursorMenu">
				</@security.a>
			</li>-->
			<li>
				<@security.a 
					templateDir="menutemplates" 
					securityCode="CUF0747" 
					label="Contratos"
					rel="submenu8"
					href=""
					cssClass="cursorMenu">
				</@security.a>
			</li>
			<li>
				<@security.a 
					templateDir="menutemplates" 
					securityCode="CUF0748" 
					label="Clientes"
					rel="submenu9"
					href=""
					cssClass="cursorMenu">
				</@security.a>
			</li>
			<li>
				<@security.a 
					templateDir="menutemplates" 
					securityCode="CUF0749" 
					label="Comprobantes"
					rel="submenu10"
					href=""
					cssClass="cursorMenu">
				</@security.a>
			</li>
			<li>
				<@security.a 
					templateDir="menutemplates" 
					securityCode="CUF0751" 
					label="Pagos"
					rel="submenu11"
					href=""
					cssClass="cursorMenu">
				</@security.a>
			</li>
			</@vc.anchors>
			<li>
				<a target="_blank" 
				href="${request.contextPath}/ayuda/ayudaIndiceView.action">				
				<img src="${request.contextPath}/common/images/ayudaIndice.gif" border="0" align="absmiddle" alt="Manual de Usuario" >
				</a>				
	
				<a target="_blank" 
				href="${request.contextPath}/ayuda/ayudaView.action">				
				<img src="${request.contextPath}/common/images/ayuda.gif" border="0" align="absmiddle" alt="Ayuda">
				</a>				
				
			</li>
		</ul>
	</div>
	<@vc.anchors target="contentTrx">			
	<!-- submenu -->
	<div id="submenu1" class="capaSubmenu">
		<@security.a 
			templateDir="menutemplates" 
			securityCode="CUF0001" 
			label="Clientes"
			href="${request.contextPath}/maestro/cliente/view.action?flowControl=init&amp;navigationId=buscar-clientes">
		</@security.a>
		<@security.a 
			templateDir="menutemplates" 
			securityCode="CUF0020" 
			label="Dep&oacute;sitos"
			href="${request.contextPath}/maestro/deposito/view.action?flowControl=init&amp;navigationId=buscar-depositos">
		</@security.a>
		<@security.a 
			templateDir="menutemplates" 
			securityCode="CUF0040" 
			label="Rubros"
			href="${request.contextPath}/maestro/rubro/view.action?flowControl=init&amp;navigationId=buscar-rubros">
		</@security.a>
		<@security.a 
			templateDir="menutemplates" 
			securityCode="CUF0040" 
			label="Equipo de Ingreso"
			href="${request.contextPath}/maestro/equipo/view.action?flowControl=init&amp;navigationId=buscar-equipos">
		</@security.a>		
		<@security.a 
			templateDir="menutemplates" 
			securityCode="CUF0580" 
			label="Tipo Unidades"
			href="${request.contextPath}/maestro/tipoUnidad/view.action?flowControl=init&amp;navigationId=buscar-tipoUnidad">
		</@security.a>
		<@security.a 
			templateDir="menutemplates" 
			securityCode="CUF0060" 
			label="Productos/Bienes"
			href="${request.contextPath}/maestro/productoBien/view.action?flowControl=init&amp;navigationId=buscar-productosBienes">
		</@security.a>		
		<@security.a 
			templateDir="menutemplates" 
			securityCode="CUF0080" 
			label="Servicios"
			href="${request.contextPath}/maestro/servicio/view.action?flowControl=init&amp;navigationId=buscar-servicios">
		</@security.a>				
		<@security.a 
			templateDir="menutemplates" 
			securityCode="CUF0100" 
			label="Personas"
			href="${request.contextPath}/maestro/persona/view.action?flowControl=init&amp;navigationId=buscar-personas">
		</@security.a>	
		<@security.a 
			templateDir="menutemplates" 
			securityCode="CUF0120" 
			label="Proveedores"
			href="${request.contextPath}/maestro/proveedor/view.action?flowControl=init&amp;navigationId=buscar-proveedores">
		</@security.a>
		<@security.a 
			templateDir="menutemplates" 
			securityCode="CUF0306" 
			label="Carteles"
			href="${request.contextPath}/maestro/carteleria/view.action?flowControl=init&amp;navigationId=buscar-carteleria">
		</@security.a>	
	</div>
		
	<div id="submenu2" class="capaSubmenu">
		<@security.a 
			templateDir="menutemplates" 
			securityCode="CUF0140" 
			label="Ajustar Existencia de Productos"
			href="${request.contextPath}/deposito/ajusteExistencia/view.action?flowControl=init&amp;navigationId=buscar-ajusteExistencia">
		</@security.a>	
		<@security.a 
			templateDir="menutemplates" 
			securityCode="CUF0160" 
			label="Egresar Productos"
			href="${request.contextPath}/deposito/egresoProducto/view.action?flowControl=init&amp;navigationId=buscar-egresoProducto">
		</@security.a>
		<@security.a 
			templateDir="menutemplates" 
			securityCode="CUF0180" 
			label="Ingresar Productos"
			href="${request.contextPath}/deposito/ingresoProducto/view.action?flowControl=init&amp;navigationId=buscar-ingresoProducto">
		</@security.a>		
		<@security.a 
			templateDir="menutemplates" 
			securityCode="CUF0440" 
			label="Recepcionar Productos/Bienes"
			href="${request.contextPath}/deposito/recepcionProductoBien/view.action?flowControl=init&amp;navigationId=buscar-recepcionProductoBien">
		</@security.a>
		<@security.a 
			templateDir="menutemplates" 
			securityCode="CUF0460" 
			label="Devolver Productos/Bienes"
			href="${request.contextPath}/deposito/devolucionProductoBien/view.action?flowControl=init&amp;navigationId=buscar-devolucionProductoBien">
		</@security.a>		
		<@security.a 
			templateDir="menutemplates" 
			securityCode="CUF0200" 
			label="Consultar Existencias de Productos"
			href="${request.contextPath}/deposito/consultaExistencia/view.action?flowControl=init&amp;navigationId=buscar-existencias">
		</@security.a>		
		<@security.a 
			templateDir="menutemplates" 
			securityCode="CUF0480" 
			label="Consultar Movimientos de Productos"
			href="${request.contextPath}/deposito/consultaMovimiento/view.action?flowControl=init&amp;navigationId=buscar-movimiento">
		</@security.a>	
		<@security.a 
			templateDir="menutemplates" 
			securityCode="CUF0500" 
			label="Consultar Consumos de Productos"
			href="${request.contextPath}/producto/consumoProducto/view.action?flowControl=init&amp;navigationId=buscar-consumoProducto">
		</@security.a>			
	</div>

	<div id="submenu3" class="capaSubmenu">
		<@security.a 
			templateDir="menutemplates" 
			securityCode="CUF0220" 
			label="Inventariar Bienes"
			href="${request.contextPath}/bien/inventarioBien/view.action?flowControl=init&amp;navigationId=buscar-inventarioBien">
		</@security.a>	
		<@security.a 
			templateDir="menutemplates" 
			securityCode="CUF0240" 
			label="Gestionar Bienes Inventariados"
			href="${request.contextPath}/bien/inventarioBien/viewBienInventariado.action?flowControl=init&amp;navigationId=buscar-bienInventariado">
		</@security.a>	
		<@security.a 
			templateDir="menutemplates" 
			securityCode="CUF0260" 
			label="Transferir Bienes"
			href="${request.contextPath}/bien/transferenciaBien/view.action?flowControl=init&amp;navigationId=buscar-transferenciaBien">
		</@security.a>	
		<@security.a 
			templateDir="menutemplates" 
			securityCode="CUF0280" 
			label="Embargar Bienes"
			href="${request.contextPath}/bien/embargoBien/view.action?flowControl=init&amp;navigationId=buscar-embargoBien">
		</@security.a>	
	</div>

	<div id="submenu4" class="capaSubmenu">
		<@security.a 
			templateDir="menutemplates" 
			securityCode="CUF0320" 
			label="Gestionar FSC/FSS"
			href="${request.contextPath}/compraContratacion/fsc_fss/view.action?flowControl=init&amp;navigationId=buscar-fsc_fss">
		</@security.a>	
		<@security.a 
			templateDir="menutemplates" 
			securityCode="CUF0340" 
			label="Asignar Proveedores a FSC/FSS"
			href="${request.contextPath}/compraContratacion/asignacionProveedor/view.action?flowControl=init&amp;navigationId=buscar-asignacionProveedor">
		</@security.a>			
		<@security.a 
			templateDir="menutemplates" 
			securityCode="CUF0340" 
			label="Gestionar Pedidos de Cotizaci&oacute;n"
			href="${request.contextPath}/compraContratacion/pedidoCotizacion/view.action?flowControl=init&amp;navigationId=buscar-pedidoCotizacion">
		</@security.a>
		<@security.a 
			templateDir="menutemplates" 
			securityCode="CUF0380" 
			label="Gestionar OC/OS"
			href="${request.contextPath}/compraContratacion/oc_os/view.action?flowControl=init&amp;navigationId=buscar-oc_os">
		</@security.a>		
	</div>

	<div id="submenu5" class="capaSubmenu">
		<@security.a 
			templateDir="menutemplates" 
			securityCode="CUF0620" 
			label="Recepcionar Servicios"
			href="${request.contextPath}/servicio/prestacionServicio/view.action?flowControl=init&amp;navigationId=buscar-prestacionServicio">
		</@security.a>
		<@security.a 
			templateDir="menutemplates" 
			securityCode="CUF0600" 
			label="Evaluar OS"
			href="${request.contextPath}/servicio/evaluacionOS/view.action?flowControl=init&amp;navigationId=buscar-evaluacionOS">
		</@security.a>		
	</div>

	<div id="submenu6" class="capaSubmenu">
		<@security.a 
			templateDir="menutemplates" 
			securityCode="CUF0680" 
			label="Calificaciones de Proveedores"
			href="${request.contextPath}/proveedor/calificacionEvaluacionProveedor/view.action?flowControl=init&amp;navigationId=buscar-calificacionEvaluacionProveedor">
		</@security.a>
		<@security.a 
			templateDir="menutemplates" 
			securityCode="CUF0400" 
			label="Per&iacute;odos de Evaluaci&oacute;n"
			href="${request.contextPath}/proveedor/periodoEvaluacion/view.action?flowControl=init&amp;navigationId=buscar-periodoEvaluacion">
		</@security.a>
		<@security.a 
			templateDir="menutemplates" 
			securityCode="CUF0660" 
			label="Advertencias de Proveedores"
			href="${request.contextPath}/proveedor/advertenciaProveedor/view.action?flowControl=init&amp;navigationId=buscar-advertenciaProveedor">
		</@security.a>
		<@security.a 
			templateDir="menutemplates" 
			securityCode="CUF0420" 
			label="Evaluar Proveedores"
			href="${request.contextPath}/proveedor/evaluacionProveedor/view.action?flowControl=init&amp;navigationId=buscar-evaluacionProveedor">
		</@security.a>
		<@security.a 
			templateDir="menutemplates" 
			securityCode="CUF0800" 
			label="Consultar Cuentas de Proveedores"
			href="${request.contextPath}/proveedor/consultaCuentaProveedor/view.action?flowControl=init&amp;navigationId=buscar-consultaCuentaProveedor">
		</@security.a>
		
	</div>

	<#--<div id="submenu7" class="capaSubmenu">
		<@security.a 
			templateDir="menutemplates" 
			securityCode="CUF0306" 
			label="Gestionar Carteles"
			href="${request.contextPath}/carteleria/view.action?flowControl=init&amp;navigationId=buscar-carteleria">
		</@security.a>
	</div>-->

	<div id="submenu8" class="capaSubmenu">
		<@security.a 
			templateDir="menutemplates" 
			securityCode="CUF0520" 
			label="Inmuebles"
			href="${request.contextPath}/inmueble/view.action?flowControl=init&amp;navigationId=buscar-inmueble">
		</@security.a>
		<@security.a 
			templateDir="menutemplates" 
			securityCode="CUF0306" 
			label="Gestionar Contratos, Acuerdos y Convenios"
			href="${request.contextPath}/contrato/view.action?flowControl=init&amp;navigationId=buscar-contrato">
		</@security.a>
	</div>

	<div id="submenu9" class="capaSubmenu">
		<@security.a 
			templateDir="menutemplates" 
			securityCode="CUF0560" 
			label="Recepcionar Bienes desde Clientes"
			href="${request.contextPath}/cliente/recepcionBien/view.action?flowControl=init&amp;navigationId=buscar-recepcionBien">
		</@security.a>
		<@security.a 
			templateDir="menutemplates" 
			securityCode="CUF0700" 
			label="Gestionar Fondos Fijos"
			href="${request.contextPath}/comprobante/fondoFijo/view.action?flowControl=init&amp;navigationId=buscar-fondoFijo">
		</@security.a>
	</div>

	<div id="submenu10" class="capaSubmenu">
		<@security.a 
			templateDir="menutemplates" 
			securityCode="CUF0640" 
			label="Gestionar Comprobantes"
			href="${request.contextPath}/comprobante/factura/view.action?flowControl=init&amp;navigationId=buscar-factura">
		</@security.a>
		<@security.a 
			templateDir="menutemplates" 
			securityCode="CUF0720" 
			label="Rendicion y Reintegro de Gastos"
			href="${request.contextPath}/comprobante/rendicionReintegroGastos/view.action?flowControl=init&amp;navigationId=buscar-rendicionReintegroGastos">
		</@security.a>
		<@security.a 
			templateDir="menutemplates" 
			securityCode="CUF0780" 
			label="Consultar Imputaciones por Rubro"
			href="${request.contextPath}/comprobante/consultaImputacionRubro/view.action?flowControl=init&amp;navigationId=buscar-consultaImputacionRubro">
		</@security.a>
		<@security.a 
			templateDir="menutemplates" 
			securityCode="CUF0820" 
			label="Consultar Imputaciones por L&iacute;neas Presupuestarias"
			href="${request.contextPath}/comprobante/consultaImputacionLinea/view.action?flowControl=init&amp;navigationId=buscar-consultaImputacionLinea">
		</@security.a>
	</div>
	
	<div id="submenu11" class="capaSubmenu">
		<@security.a 
			templateDir="menutemplates" 
			securityCode="CUF0760" 
			label="Gestionar Orden de Pago"
			href="${request.contextPath}/pago/view.action?flowControl=init&amp;navigationId=buscar-ordenPago">
		</@security.a>
	</div>
	
	<script type="text/javascript">
		cssdropdown.startchrome("capaItemsMenu")
	</script>
	
</@vc.anchors>