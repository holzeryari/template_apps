<%@ taglib prefix="s" uri="/WEB-INF/tlds/struts-tags.tld"%>
<%@ taglib uri="/WEB-INF/tlds/ajaxtags.tld" prefix="ajax"%>

<% response.setStatus(999); %>

<html>
<head>
<link rel="stylesheet" href="${pageContext.request.contextPath}/common/styles/login.css" type="text/css" />
</head>
<s:head />
<body>

<s:fielderror />

<s:form action="/j_acegi_security_check" theme="simple">

	<div id="capaPrincipal" class="capaPrincipal">	
		
		<%-- Bloque Superior Pantalla Login--%>
		<div id="capaSuperiorLogin" class="capaSuperiorLogin">		
			<table id="tablaSuperiorLogin" class="tablaSuperiorLogin" cellspacing="4">
				<tr>
						<td width="10%" align="left"><img src="${pageContext.request.contextPath}/common/images/logoMenuRUS.jpg" border="0" align="absmiddle"></td>
						<td width="65%" align="center">
							Sistema&nbsp;de&nbsp;Compras&nbsp;de&nbsp;R&iacute;o&nbsp;Uruguay&nbsp;Seguros
						</td>
						<td width="25%" align="right" class="textoLogoFraseCalidad"><img src="${pageContext.request.contextPath}/common/images/LogoDNVFraseRecortada.jpg" border="0" align="right">
							Primera Aseguradora <br> con Calidad Certificada <br> en Gesti&oacute;n Integral <br> de Seguros <br> ISO 9001:2000
						</td>
				</tr>
					
				</table>
			</div>	

			<%-- Bloque Central Pantalla Login --%>
			<div id="capaCentralLogin" class="capaCentralLogin">		
				
				<%-- error --%>
				<div id="errorLogin" class="errorLogin">
					<s:property value="%{exception.code}" />
				</div>	
				
				<table id="tablaCentralLogin" class="tablaCentralLogin" align="center">
						
						<%-- titulo Login --%>
						<tr  height="5%">
							<td class="tituloLogin" colspan="3"><b>Login</b> <img src="${pageContext.request.contextPath}/common/images/key224.gif" align="absmiddle"></td>
						</tr>
						
						<tr height="10%">
							<td align="right" width="20%" class="campoLogin">Usuario:</td>
							<td width="30%" align="left" class="campoLogin"><s:textfield  cssClass="textarea" name="j_username" /></td>
							<td width="10%" class="campoLogin">&nbsp;</td>
						</tr>
						<tr height="10%">
							<td  align="right" width="20%" class="campoLogin">Password:</td>
							<td width="30%" align="left" class="campoLogin"><s:password cssClass="textarea" name="j_password" /></td>
							<td width="10%" class="campoLogin">&nbsp;</td>
						</tr>
						<tr height="5%">
							<td align="right" colspan="3" class="botonera">
							<span
								style="padding-top: 10px;"> <s:submit
								cssClass="boton" value="Ingresar" /> </span></td>
						
						</tr>
					</table>
				</div>	
				
				<%-- Bloque Inferior Pantalla Login--%>
				<%-- <div id="capaInferiorLogin" class="capaInferiorLogin">		
					<table id="tablaInferiorLogin" class="tablaInferiorLogin">
						<tr>
							<td align="right"><img src="${pageContext.request.contextPath}/common/images/LogoDNVFraseRecortada.jpg" border="0" align="right">
							Primera Aseguradora <br> con Calidad Certificada <br> en Gesti&oacute;n Integral de Seguros <br> ISO 9001:2000
							</td>
						</tr>
					</table>
				</div>--%>	
							
		</div><%-- fin capaPrincipal --%>
		
</s:form>

</body>
</html>


