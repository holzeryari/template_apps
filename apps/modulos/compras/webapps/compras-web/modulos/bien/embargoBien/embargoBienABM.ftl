<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign tiles=JspTaglibs["/WEB-INF/tlds/tiles-jsp.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<@s.hidden id="webFlow" name="webFlow"/>
<@s.hidden id="viewState" name="viewState"/>
<@s.hidden id="embargoBien.oid" name="embargoBien.oid"/>
<@s.hidden id="embargoBien.estado" name="embargoBien.estado.ordinal()"/>
<@s.hidden id="embargoBien.versionNumber" name="embargoBien.versionNumber"/>

<#if navigationId == "finalizarEmbargoBien">
	<@s.hidden id="embargoBien.fechaInicio" name="embargoBien.fechaInicio"/>
</#if>						

<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
			<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos del Embargo de Bienes</b></td>
				<td>
					<div align="right">
						<@s.a templateDir="custontemplates" id="modificarEmbargoBien" name="modificarEmbargoBien" href="javascript://nop/" cssClass="ocultarIcono">
							<b>Modificar</b><img src="${request.contextPath}/common/images/modificar.gif"  border="0" align="absmiddle" hspace="3" >
						</@s.a>
					</div>
				</td>
			</tr>
		</table>
		
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>	
			<tr>
	      		<td class="textoCampo">N&uacute;mero:</td>
	      		<td class="textoDato" colspan="3"><@s.property default="&nbsp;" escape=false value="embargoBien.numero"/></td>
			</tr>
			<tr>
	      		<td class="textoCampo">Fecha Inicio:</td>
				<td class="textoDato">
					<@vc.rowCalendar 
						templateDir="custontemplates" 
						id="embargoBien.fechaInicio" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="embargoBien.fechaInicio" 
						title="Fecha Inicio" />
				</td>
      			<td class="textoCampo">Fecha Fin:</td>
				<td class="textoDato">
					<@vc.rowCalendar 
						templateDir="custontemplates" 
						id="embargoBien.fechaFin" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="embargoBien.fechaFin" 
						title="Fecha Fin" />
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Juzgado:</td>
      			<td class="textoDato">
	      			<@s.textfield 
      					templateDir="custontemplates" 
						id="embargoBien.juzgado" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="embargoBien.juzgado" 
						title="Juzgado" />
				</td>
				<td class="textoCampo">Car&aacute;tula:</td>
      			<td class="textoDato">
	      			<@s.textfield 
      					templateDir="custontemplates" 
						id="embargoBien.caratula" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="embargoBien.caratula" 
						title="Caratula" />
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Expediente:</td>
      			<td class="textoDato" colspan="3">
	      			<@s.textfield 
      					templateDir="custontemplates" 
						id="embargoBien.expediente" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="embargoBien.expediente" 
						title="Expediente" />
				</td>
			</tr>
	    	<tr>
				<td class="textoCampo">Observaciones:</td>
      			<td class="textoDato" colspan="3">
					<@s.textarea
						templateDir="custontemplates"
						cols="89" rows="2"
						cssClass="textarea"
						id="embargoBien.observaciones"
						name="embargoBien.observaciones"
						label="Observaciones"/>
				</td>
    		</tr>
	    	<tr>
				<td class="textoCampo">Estado:</td>
      			<td  class="textoDato" colspan="3">
					<@s.property default="&nbsp;" escape=false value="embargoBien.estado"/></td>
				</td>
			</tr>
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>
		</table>		
	</div>
	

