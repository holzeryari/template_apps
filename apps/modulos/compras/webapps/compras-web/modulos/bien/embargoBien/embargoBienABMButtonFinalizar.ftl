<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<div id="capaSeparadora" class="capaSeparadora">&nbsp;</div>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancelar" type="button" name="btnCancelar" value="Cancelar" class="boton"/>
				<input id="finalizar" type="button" name="btnFinalizar" value="Finalizar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/bien/embargoBien/finalizarEmbargoBien.action" 
  source="finalizar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="embargoBien.oid={embargoBien.oid},embargoBien.fechaInicio={embargoBien.fechaInicio},embargoBien.fechaFin={embargoBien.fechaFin}"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancelar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=${navegacionIdBack},flowControl=back"/>
