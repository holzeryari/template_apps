<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<div id="capaSeparadora" class="capaSeparadora">&nbsp;</div>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancelar" type="button" name="btnCancelar" value="Cancelar" class="boton"/>
				<input id="embargar" type="button" name="btnEmbargar" value="Embargar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/bien/embargoBien/iniciarEmbargoBien.action" 
  source="embargar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="embargoBien.oid={embargoBien.oid}"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancelar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=buscar-embargoBien,flowControl=back"/>

  <@vc.htmlContent 
  baseUrl="${request.contextPath}/bien/embargoBien/updateEmbargoBienView.action" 
  source="modificarEmbargoBien" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId=administrar-updateDetalle,flowControl=regis,embargoBien.oid={embargoBien.oid},embargoBien.versionNumber={embargoBien.versionNumber}"/>
  