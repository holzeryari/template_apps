<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>


<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="left">
				<input id="cancelar" type="button" name="btnCancelar" value="Cancelar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>


<@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancelar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=buscar-embargoBien,flowControl=back"/>

  <@vc.htmlContent 
  baseUrl="${request.contextPath}/bien/embargoBien/updateEmbargoIniciadoView.action" 
  source="modificarEmbargoBien" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId=administrar-updateDetalle,flowControl=regis,embargoBien.oid={embargoBien.oid},embargoBien.versionNumber={embargoBien.versionNumber}"/>
  