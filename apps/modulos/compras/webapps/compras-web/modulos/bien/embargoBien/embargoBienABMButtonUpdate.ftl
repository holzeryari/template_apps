<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancelar" type="button" name="btnCancelar" value="Cancelar" class="boton"/>
				<input id="modificar" type="button" name="btnModificar" value="Modificar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/bien/embargoBien/updateEmbargoBien.action" 
  source="modificar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="embargoBien.oid={embargoBien.oid},embargoBien.fechaInicio={embargoBien.fechaInicio},embargoBien.juzgado={embargoBien.juzgado},embargoBien.caratula={embargoBien.caratula},embargoBien.expediente={embargoBien.expediente},embargoBien.observaciones={embargoBien.observaciones},embargoBien.estado=${embargoBien.estado}"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancelar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=administrarEmbargoBien,flowControl=back"/>
