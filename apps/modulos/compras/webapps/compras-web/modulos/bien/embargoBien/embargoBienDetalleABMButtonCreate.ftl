<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancelar" type="button" name="btnCancelar" value="Cancelar" class="boton"/>
				<input id="agregar" type="button" name="btnAgregar" value="Agregar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/bien/embargoBien/createEmbargoBienDetalle.action" 
  source="agregar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="embargoBienDetalle.embargoBien.oid={embargoBienDetalle.embargoBien.oid},embargoBienDetalle.montoEmbargo={embargoBienDetalle.montoEmbargo},embargoBienDetalle.bienInventariado.oid={embargoBienDetalle.bienInventariado.oid}"/>
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancelar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId=administrarEmbargoBien,flowControl=back"/>
