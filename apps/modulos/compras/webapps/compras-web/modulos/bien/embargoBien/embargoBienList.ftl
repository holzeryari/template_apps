
<#setting locale="es_AR">

<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>

<@s.hidden id="navigationId" name="navigationId"/>

<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
        	<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
			<tr>
				<td><b>Datos del Embargo de Bienes</b></td>
				<td>
					<div align="right">
						<@security.a 
								templateDir="custontemplates" 
								securityCode="CUF0281" 
								enabled="true" 
								cssClass="item" 
								id="crear"										
								href="javascript://nop/">
								<b >Agregar</b>										
								<img src="${request.contextPath}/common/images/agregar.gif"  border="0" align="absmiddle" hspace="3" >
							</@security.a>					
					</div>
				</td>
			</tr>
		</table>

		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
	    		<td class="textoCampo" colspan="4">&nbsp;</td>
	    	</tr>		
			<tr>
		  		<td class="textoCampo">N&uacute;mero:</td>
		      	<td class="textoDato">
	      			<@s.textfield 
	      				templateDir="custontemplates" 
						id="embargoBien.numero" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="embargoBien.numero" 
						title="N&uacute;mero"/>
				</td>							
				<td class="textoCampo">Estado:</td>
	      		<td class="textoDato">
	      			<@s.select 
						templateDir="custontemplates" 
						id="estado" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="estado" 
						list="estadoList" 
						listKey="key" 
						listValue="description" 
						value="embargoBien.estado.ordinal()"
						title="Estado"
						headerKey="0"
						headerValue="Todos"/>
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Fecha Inicio Desde:</td>
	      		<td class="textoDato">
					<@vc.rowCalendar 
						templateDir="custontemplates" 
						id="fechaInicioDesde" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="fechaInicioDesde" 
						title="Fecha Inicio Desde" />
				</td>
				<td class="textoCampo">Fecha Inicio Hasta:</td>
	      		<td class="textoDato">
					<@vc.rowCalendar 
						templateDir="custontemplates" 
						id="fechaInicioHasta" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="fechaInicioHasta" 
						title="Fecha Inicio Hasta" />
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Fecha Fin Desde:</td>
      			<td class="textoDato">
					<@vc.rowCalendar 
						templateDir="custontemplates" 
						id="fechaFinDesde" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="fechaFinDesde" 
						title="Fecha Fin Desde" />
				</td>
				<td class="textoCampo">Fecha Fin Hasta:</td>
      			<td class="textoDato">
					<@vc.rowCalendar 
						templateDir="custontemplates" 
						id="fechaFinHasta" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="fechaFinHasta" 
						title="Fecha Fin Hasta" />
				</td>
			</tr>
			<tr>
	    		<td colspan="4" class="lineaGris" align="right">
	      			<input id="limpiar" type="button" name="btnVolver" value="Limpiar" class="boton"/>		
	      			<input id="buscar" type="button" name="btnVolver" value="Buscar" class="boton"/>
	    		</td>
			</tr>	
		</table>
		</div>	
		<!-- Resultado Filtro -->
		<@s.if test="embargoBienList!=null">
			<#assign embargoBienNumero = "0" />
			<@s.if test="embargoBien.numero != null">
				<#assign embargoBienNumero = "${embargoBien.numero}" />
			</@s.if>

			<#assign embargoBienEstado = "0" />
			<@s.if test="embargoBien.estado != null">
					<#assign embargoBienEstado = "${embargoBien.estado.ordinal()}" />
			</@s.if>

			<#assign embargoBienFechaInicioDesde = "" />
			<@s.if test="fechaInicioDesde != null">
				<#assign embargoBienFechaInicioDesde = "${fechaInicioDesde?string('dd/MM/yyyy')}" />
			</@s.if>

			<#assign embargoBienFechaInicioHasta = "" />
			<@s.if test="fechaInicioHasta != null">
				<#assign embargoBienFechaInicioHasta = "${fechaInicioHasta?string('dd/MM/yyyy')}" />
			</@s.if>

			<#assign embargoBienFechaFinDesde = "" />
			<@s.if test="fechaFinDesde != null">
				<#assign embargoBienFechaFinDesde = "${fechaFinDesde?string('dd/MM/yyyy')}" />
			</@s.if>

			<#assign embargoBienfechaFinHasta = "" />
			<@s.if test="fechaFinHasta != null">
				<#assign embargoBienfechaFinHasta = "${fechaFinHasta?string('dd/MM/yyyy')}" />
			</@s.if>

			<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
				<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>Embargos de Bienes encontrados</td>
				</tr>
			</table>
					
			<@vc.anchors target="contentTrx">
	        	<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="embargoBienList" id="embargoBien" pagesize=15  defaultsort=2 partialList=true size="recordSize" keepStatus=true excludedParams="resetFlag">
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select" class="botoneraAnchoCon4" title="Acciones">
						<div class="alineacion">
								<@security.a 
									templateDir="custontemplates" 
									securityCode="CUF0283" 
									enabled="embargoBien.readable" 
									cssClass="item" 
									href="${request.contextPath}/bien/embargoBien/readEmbargoBienView.action?embargoBien.oid=${embargoBien.oid?c}&navigationId=administrarEmbargoBien&flowControl=regis">
									<img src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver" border="0">
								</@security.a>
						</div>
						<div class="alineacion">	
							<#if embargoBien.estado.ordinal() == 1>	
								<@security.a 
									templateDir="custontemplates" 
									securityCode="CUF0284" 
									enabled="embargoBien.updatable"
									cssClass="item"  
									href="${request.contextPath}/bien/embargoBien/administrarEmbargoBienView.action?embargoBien.oid=${embargoBien.oid?c}&navigationId=administrarEmbargoBien&flowControl=regis">
									<img src="${request.contextPath}/common/images/administrar.gif" alt="Administrar" title="Administrar" border="0">
								</@security.a>
							<#else>
								<@security.a 
									templateDir="custontemplates" 
									securityCode="CUF0284" 
									enabled="embargoBien.updatable"
									cssClass="item"  
									href="${request.contextPath}/bien/embargoBien/administrarEmbargoIniciadoView.action?embargoBien.oid=${embargoBien.oid?c}&navigationId=administrarEmbargoBien&flowControl=regis">
									<img src="${request.contextPath}/common/images/administrar.gif" alt="Administrar" title="Administrar" border="0">
								</@security.a>							
							</#if>	
						</div>
						<div class="alineacion">
								<#if embargoBien.estado.ordinal() == 1>
									<@security.a 
										templateDir="custontemplates" 
										securityCode="CUF0285" 
										enabled="embargoBien.eraseable"
										cssClass="item"  
										href="${request.contextPath}/bien/embargoBien/deleteEmbargoBienView.action?embargoBien.oid=${embargoBien.oid?c}&navigationId=eliminarEmbargoBien&navegacionIdBack=${navigationId}&flowControl=regis">
										<img src="${request.contextPath}/common/images/eliminar.gif" alt="Eliminar" title="Eliminar" border="0">
									</@security.a>				
								<#elseif embargoBien.estado.ordinal() == 2>		
									<@security.a 
										templateDir="custontemplates" 
										securityCode="CUF0286" 
										enabled="embargoBien.eraseable"
										cssClass="item"  
										href="${request.contextPath}/bien/embargoBien/anularEmbargoBienView.action?embargoBien.oid=${embargoBien.oid?c}&navigationId=anularEmbargoBien&navegacionIdBack=${navigationId}&flowControl=regis">
										<img src="${request.contextPath}/common/images/anular.gif" alt="Anular" title="Anular" border="0">
									</@security.a>				
								<#else>
									<@security.a 
										templateDir="custontemplates" 
										securityCode="CUF0285" 
										enabled="embargoBien.eraseable"
										cssClass="item"  
										href="">
										<img src="${request.contextPath}/common/images/eliminar.gif" alt="Eliminar" title="Eliminar" border="0">
									</@security.a>				
								</#if>						
						</div>
						<div class="alineacion">	
								<@security.a 
									templateDir="custontemplates" 
									securityCode="CUF0287" 
									enabled="embargoBien.enable"
									cssClass="item"  
									href="${request.contextPath}/bien/embargoBien/finalizarEmbargoBienView.action?embargoBien.oid=${embargoBien.oid?c}&navigationId=finalizarEmbargoBien&navegacionIdBack=${navigationId}&flowControl=regis">
									<img src="${request.contextPath}/common/images/desactivar.gif" alt="Finalizar" title="Finalizar" border="0">
								</@security.a>
						</div>
						
						</@display.column>

						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="numero" title="N&uacute;mero" />				

						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="fechaInicio" format="{0,date,dd/MM/yyyy}" title="Fecha Inicio" />

						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="fechaFin" format="{0,date,dd/MM/yyyy}" title="Fecha Fin" />

						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="juzgado" title="Juzgado" />

						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="expediente" title="Expediente" />

						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="estado" title="Estado" />

					</@display.table>
				</@vc.anchors>
			</div>	
		</@s.if>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/bien/embargoBien/search.action"
  source="buscar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="resetFlag=true,navigationId={navigationId},flowControl=regis,embargoBien.numero={embargoBien.numero},embargoBien.estado={estado},fechaInicioDesde={fechaInicioDesde},fechaInicioHasta={fechaInicioHasta},fechaFinDesde={fechaFinDesde},fechaFinHasta={fechaFinHasta}"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/bien/embargoBien/view.action"
  source="limpiar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=regis"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/bien/embargoBien/createEmbargoBienView.action" 
  source="crear" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=abm-embargoBien-create,flowControl=regis"/>
