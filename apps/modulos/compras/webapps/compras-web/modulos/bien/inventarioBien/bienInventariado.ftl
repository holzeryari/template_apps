<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<@s.hidden id="navigationId" name="navigationId"/>
<@s.hidden id="bienInventariado.versionNumber" name="bienInventariado.versionNumber"/>
<@s.hidden id="bienInventariado.rubro.oid" name="bienInventariado.rubro.oid"/>
<@s.hidden id="bienInventariado.oid" name="bienInventariado.oid"/>
<@s.hidden id="bienInventariado.cliente.oid" name="bienInventariado.cliente.oid"/>
<@s.hidden id="bienInventariado.estadoServicio" name="bienInventariado.estado.ordinal()"/>
<@s.hidden id="tipoI" name="bienInventariado.inventarioBien.tipoInventario.ordinal()"/>
<@s.hidden id="bienInventariado.factura.pkFactura.nro_proveedor" name="bienInventariado.factura.pkFactura.nro_proveedor"/>
<@s.hidden id="bienInventariado.factura.pkFactura.nro_factura" name="bienInventariado.factura.pkFactura.nro_factura"/>
<@s.hidden id="bienInventariado.factura.pkFactura.sucursal" name="bienInventariado.factura.pkFactura.sucursal"/>
<@s.hidden id="bienInventariado.factura.pkFactura.tip_docum" name="bienInventariado.factura.pkFactura.tip_docum"/>
<@s.hidden id="bienInventariado.factura.numeroString" name="bienInventariado.factura.numeroString"/>
<@s.hidden id="bienInventariado.bien.oid" name="bienInventariado.bien.oid"/>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>

<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos del Bien Inventariado</b></td>
			</tr>
		</table>
		
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
		<tr>
        	<td class="textoCampo" colspan="4">&nbsp;</td>
        </tr>	
		<tr>
  			<td class="textoCampo">N&uacute;mero del bien inventariado:</td>
  			<td class="textoDato">
  			<@s.textfield 
  					templateDir="custontemplates" 
					id="bienInventariado.numeroInventario" 
					name="bienInventariado.numeroInventario"
					cssClass="textarea"
					cssStyle="width:160px" 
					name="bienInventariado.numeroInventario"
					title="N&uacute;meto de Inventario"/>
  			</td>
  			<td class="textoCampo">Fecha Alta:</td>
			<td class="textoDato">
				<@s.if test="bienInventariado.fechaAlta != null">
					<#assign fechaAlta = bienInventariado.fechaAlta> 
					${fechaAlta?string("dd/MM/yyyy")}	
				</@s.if>	&nbsp;				
			</td>	      			
		</tr>	
		<tr>
			<td class="textoCampo">Nro.Factura Anterior al Sistema:</td>
  			<td class="textoDato">
			     <@s.textfield 
  					templateDir="custontemplates" 
					id="bienInventariado.numeroFactura" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="bienInventariado.numeroFactura"									
					title="N&uacute;mero de Factura"/>	
			</td>
			<td class="textoCampo">Factura:</td>
	  		<td class="textoDato">
				<@s.property default="&nbsp;" escape=false value="bienInventariado.factura.numeroString"/>
				<@s.a templateDir="custontemplates" id="seleccionarFactura" 
				name="seleccionarFactura" href="javascript://nop/" cssClass="ocultarIcono">
				<img src="${request.contextPath}/common/images/buscar.gif" border="0" align="absmiddle" hspace="3" >
				</@s.a>			
										
				<#if bienInventariado?exists && bienInventariado.factura?exists && bienInventariado.factura.pkFactura?exists>
					<@vc.anchors target="contentTrx">	
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0643" 
							enabled="factura.readable"
							id="verFactura"
							name="verFactura" 
							cssClass="ocultarIcono" 
							href="${request.contextPath}/comprobante/factura/readView.action?factura.pkFactura.nro_factura=${bienInventariado.factura.pkFactura.nro_factura?c}&factura.pkFactura.nro_proveedor=${bienInventariado.factura.pkFactura.nro_proveedor?c}&factura.pkFactura.sucursal=${bienInventariado.factura.pkFactura.sucursal?c}&factura.pkFactura.tip_docum=${bienInventariado.factura.pkFactura.tip_docum?c}&navigationId=factura-visualizar&flowControl=regis&navegacionIdBack=${navigationId}">
							<img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver"  border="0">
						</@security.a>
					</@vc.anchors>
				</#if>
			</td>							
		</tr>
		<tr>
			<td class="textoCampo">Valor Amortizado: </td>
			<td class="textoDato">
				<@s.property default="&nbsp;" escape=false value="bienInventariado.valorAmortizado"/>
			</td>
			<td class="textoCampo">Valor Adquisici&oacute;n: </td>
			<td class="textoDato">
				 <@s.textfield 
  					templateDir="custontemplates" 
					id="bienInventariado.valorAdquisicion" 
					cssClass="textarea"
					template="textMoney"   
					cssStyle="width:160px" 
					name="bienInventariado.valorAdquisicion"								
					title="Valor Adquisici&oacute;n"/>	
			</td>
		</tr>
		<tr>
  			<td class="textoCampo">Identificador: </td>
			<td class="textoDato">
				 <@s.textfield 
  					templateDir="custontemplates" 
					id="bienInventariado.numeroSerie" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="bienInventariado.numeroSerie"						
					title="Identificador"/>	
			</td>
			<td class="textoCampo">Cliente:</td>
			<td class="textoDato">
				<@s.property default="&nbsp;" escape=false value="bienInventariado.cliente.descripcion"/>
				<@s.a templateDir="custontemplates" id="seleccionarCliente" name="seleccionarCliente" href="javascript://nop/" cssClass="ocultarIcono">
					<img src="${request.contextPath}/common/images/buscar.gif" border="0" align="absmiddle" hspace="3" >
				</@s.a>	
			</td>
		</tr>
		
		<@s.if test="bienInventariado.fechaBaja != null">
    	<tr>
  			<td class="textoCampo">Fecha Baja: </td>
			<td class="textoDato">
				<#assign fechaBaja = bienInventariado.fechaBaja> 
				${fechaBaja?string("dd/MM/yyyy")}
				&nbsp;	
			</td>
			<td class="textoCampo">Tipo Baja:</td>
			<td class="textoDato">
				<@s.property default="&nbsp;" escape=false value="bienInventariado.tipoBaja"/>
			</td>
		</tr>
  		<tr>
			<td class="textoCampo">Motivo Baja:</td>
  			<td class="textoDato" colspan="3">
				<@s.property default="&nbsp;" escape=false value="bienInventariado.motivoBaja"/>
			</td>					
		</tr>
    	</@s.if>
							
		<tr>
			<td class="textoCampo">Estado:</td>
			<td class="textoDato" colspan="3">
				<@s.property default="&nbsp;" escape=false value="bienInventariado.estado"/>
			</td>
		</tr>
		<tr><td class="textoCampo" colspan="4">&nbsp;</td></tr>
	</table>
	</div>