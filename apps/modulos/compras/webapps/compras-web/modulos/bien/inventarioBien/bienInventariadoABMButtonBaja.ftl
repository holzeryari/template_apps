<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>


<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancelar" type="button" name="btnCancelar" value="Cancelar" class="boton"/>
				<input id="modificar" type="button" name="btnModificar" value="Baja" class="boton"/>
			</td>
		</tr>	
	</table>
</div>



<@vc.htmlContent 
  baseUrl="${request.contextPath}/bien/inventarioBien/bajaBienInventariado.action" 
  source="modificar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="bienInventariado.oid={bienInventariado.oid},bienInventariado.fechaBaja={bienInventariado.fechaBaja},bienInventariado.tipoBaja={bienInventariado.tipoBaja},bienInventariado.motivoBaja={bienInventariado.motivoBaja}"/>
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action"  
  source="cancelar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=buscar-bienInventariado,flowControl=back"/>
