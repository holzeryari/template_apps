<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancelar" type="button" name="btnCancelar" value="Cancelar" class="boton"/>
				<input id="modificar" type="button" name="btnModificar" value="Modificar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>


<@vc.htmlContent 
  baseUrl="${request.contextPath}/bien/inventarioBien/updateBienInventariado.action" 
  source="modificar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="bienInventariado.oid={bienInventariado.oid},bienInventariado.numeroFactura={bienInventariado.numeroFactura},bienInventariado.valorAdquisicion={bienInventariado.valorAdquisicion},bienInventariado.numeroSerie={bienInventariado.numeroSerie},bienInventariado.inventarioBien.oid={bienInventariado.inventarioBien.oid},inventarioBien.tipoInventario={tipoI},bienInventariado.cliente.oid={bienInventariado.cliente.oid},bienInventariado.factura.pkFactura.nro_proveedor={bienInventariado.factura.pkFactura.nro_proveedor},bienInventariado.factura.pkFactura.nro_factura={bienInventariado.factura.pkFactura.nro_factura},bienInventariado.factura.pkFactura.sucursal={bienInventariado.factura.pkFactura.sucursal},bienInventariado.factura.pkFactura.tip_docum={bienInventariado.factura.pkFactura.tip_docum},bienInventariado.factura.numeroString={bienInventariado.factura.numeroString},navegacionIdBack=${navegacionIdBack}"/>
  
 
<@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancelar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=${navegacionIdBack},flowControl=back"/>

  
   <@vc.htmlContent 
  baseUrl="${request.contextPath}/bien/inventarioBien/selectCliente.action" 
  source="seleccionarCliente" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId=${navigationId},flowControl=change,navegacionIdBack=${navegacionIdBack},bienInventariado.numeroSerie={bienInventariado.numeroSerie},bienInventariado.inventarioBien.oid={bienInventariado.inventarioBien.oid},bienInventariado.bien.oid={bienInventariado.bien.oid},bienInventario.cliente.oid={bienInventariado.cliente.oid},bienInventariado.cliente.descripcion={bienInventariado.cliente.descripcion},bienInventariado.oid={bienInventariado.oid},bienInventariado.factura.pkFactura.nro_proveedor={bienInventariado.factura.pkFactura.nro_proveedor},bienInventariado.factura.pkFactura.nro_factura={bienInventariado.factura.pkFactura.nro_factura},bienInventariado.factura.pkFactura.sucursal={bienInventariado.factura.pkFactura.sucursal},bienInventariado.factura.pkFactura.tip_docum={bienInventariado.factura.pkFactura.tip_docum},bienInventariado.factura.numeroString={bienInventariado.factura.numeroString}"/>
  
  
   <@vc.htmlContent 
  baseUrl="${request.contextPath}/bien/inventarioBien/selectComprobante.action" 
  source="seleccionarFactura" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=${navigationId},flowControl=change,navegacionIdBack=${navegacionIdBack},bienInventariado.numeroSerie={bienInventariado.numeroSerie},bienInventariado.inventarioBien.oid={bienInventariado.inventarioBien.oid},bienInventariado.bien.oid={bienInventariado.bien.oid},bienInventario.cliente.oid={bienInventariado.cliente.oid},bienInventariado.cliente.descripcion={bienInventariado.cliente.descripcion},bienInventariado.oid={bienInventariado.oid},bienInventariado.factura.pkFactura.nro_proveedor={bienInventariado.factura.pkFactura.nro_proveedor},bienInventariado.factura.pkFactura.nro_factura={bienInventariado.factura.pkFactura.nro_factura},bienInventariado.factura.pkFactura.sucursal={bienInventariado.factura.pkFactura.sucursal},bienInventariado.factura.pkFactura.tip_docum={bienInventariado.factura.pkFactura.tip_docum},bienInventariado.factura.numeroString={bienInventariado.factura.numeroString}"/>
 
  	