<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>
<div id="errorTrx"></div>

<@s.hidden id="navigationId" name="navigationId"/>
<@s.hidden id="bienInventariado.versionNumber" name="bienInventariado.versionNumber"/>
<@s.hidden id="bienInventariado.rubro.oid" name="bienInventariado.rubro.oid"/>
<@s.hidden id="bienInventariado.cliente.oid" name="bienInventariado.cliente.oid"/>
<@s.hidden id="bienInventariado.oid" name="bienInventariado.oid"/>
<@s.hidden id="bienInventariado.estadoServicio" name="bienInventariado.estado.ordinal()"/>

<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos del Bien Inventariado</b></td>
			</tr>
		</table>
		
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
		<tr>
        	<td class="textoCampo" colspan="4">&nbsp;</td>
        </tr>	
		<tr>
  			<td class="textoCampo">N&uacute;mero del bien inventariado:</td>
  			<td class="textoDato"><@s.property default="&nbsp;" escape=false value="bienInventariado.numeroInventario"/></td>
  			<td class="textoCampo">Fecha Alta:</td>
			<td class="textoDato">
				<@s.if test="bienInventariado.fechaAlta != null">
					<#assign fechaAlta = bienInventariado.fechaAlta> 
					${fechaAlta?string("dd/MM/yyyy")}	
				</@s.if>
				&nbsp;
			</td>
		</tr>	
		<tr>
			<td class="textoCampo">Tipo Inventario:</td>
			<td class="textoDato" colspan="3">
				<@s.property default="&nbsp;" escape=false value="bienInventariado.inventarioBien.tipoInventario"/>
			</td>
		</tr>
		<tr>
			<td class="textoCampo">Nro.Factura Anterior al Sistema:</td>
  			<td class="textoDato">
      			<@s.property default="&nbsp;" escape=false value="bienInventariado.numeroFactura"/>
			</td>
				<td class="textoCampo">Factura:</td>
	  		<td class="textoDato">
				<@s.property default="&nbsp;" escape=false value="bienInventariado.factura.numeroString"/>			
				<#if bienInventariado?exists && bienInventariado.factura?exists && bienInventariado.factura.pkFactura?exists>
					<@vc.anchors target="contentTrx">	
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0643" 
							enabled="factura.readable"
							id="verFactura"
							name="verFactura" 
							cssClass="ocultarIcono" 
							href="${request.contextPath}/comprobante/factura/readView.action?factura.pkFactura.nro_factura=${bienInventariado.factura.pkFactura.nro_factura?c}&factura.pkFactura.nro_proveedor=${bienInventariado.factura.pkFactura.nro_proveedor?c}&factura.pkFactura.sucursal=${bienInventariado.factura.pkFactura.sucursal?c}&factura.pkFactura.tip_docum=${bienInventariado.factura.pkFactura.tip_docum?c}&navigationId=factura-visualizar&flowControl=regis&navegacionIdBack=${navigationId}">
							<img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver"  border="0">
						</@security.a>
					</@vc.anchors>
				</#if>
			</td>	
		</tr>
		<tr>
			<td class="textoCampo">Valor Adquisici&oacute;n: </td>
			<td class="textoDato">
				<@s.property default="&nbsp;" escape=false value="bienInventariado.valorAdquisicion"/>
			</td>
			<td class="textoCampo">Valor Amortizado: </td>
			<td class="textoDato" colspan="3">
				<@s.property default="&nbsp;" escape=false value="bienInventariado.valorAmortizado"/>
			</td>
		</tr>
		<tr>
  			<td class="textoCampo">Identificador: </td>
			<td class="textoDato">
			<@s.property default="&nbsp;" escape=false value="inventarioBien.numeroSerie"/>
			</td>
			<td class="textoCampo">Cliente:</td>
			<td class="textoDato">
			<@s.property default="&nbsp;" escape=false value="inventarioBien.cliente.descripcion"/>
			</td>
		</tr>
		<tr>
  		<td class="textoCampo">Fecha Baja: </td>
		<td class="textoDato">
			<@vc.rowCalendar 
				templateDir="custontemplates" 
				id="bienInventariado.fechaBaja" 
				cssClass="textarea"
				cssStyle="width:160px" 
				name="bienInventariado.fechaBaja"
				title="Fecha Baja" />
				</td>
		<td class="textoCampo">Tipo Baja:</td>
		<td class="textoDato">
			<@s.select 
				templateDir="custontemplates" 
				id="tipoBaja" 
				cssClass="textarea"
				cssStyle="width:170px" 
				name="bienInventariado.tipoBaja" 
				list="tipoBajaList" 
				listKey="key" 
				listValue="description" 
				value="bienInventariado.tipoBaja.ordinal()"
				title="Tipo Baja"
				headerKey="0"
				headerValue="Seleccionar" 
				 />
			</td>
		</tr>
  		<tr>
			<td class="textoCampo">Motivo:</td>
  			<td  class="textoDato"colspan="3">
			<@s.textarea	
					templateDir="custontemplates" 						  
					cols="89" rows="2"	      					
					cssClass="textarea"
					id="bienInventariado.motivoBaja"							 
					name="bienInventariado.motivoBaja"  
					label="Motivo de Baja"														
					 />
			</td>					
		</tr>
		<tr><td colspan="4" class="textoCampo">&nbsp;</td></tr>							
	</table>
	</div>