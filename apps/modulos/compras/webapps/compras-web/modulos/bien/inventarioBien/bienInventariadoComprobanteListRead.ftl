<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>
<@s.hidden id="navigationId" name="navigationId"/>
<@vc.anchors target="contentTrx">	

<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
	<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
		<tr>
			<td><b>Comprobantes</b></td>
		</tr>
	</table>

	<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="bienInventariado.facturaBienInventariadoList" id="facturaBienInventariado" >
          		
  		
					<@display.column headerClass="tbl-contract-service-select" class="botoneraAnchoCon2" title="Acciones">
						<div class="alineacion">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0643" 
							enabled="factura.readable" 
							cssClass="item" 
							id="ver"
							name="ver" 
							href="${request.contextPath}/comprobante/factura/readView.action?factura.pkFactura.nro_factura=${facturaBienInventariado.factura.pkFactura.nro_factura?c}&factura.pkFactura.nro_proveedor=${facturaBienInventariado.factura.pkFactura.nro_proveedor?c}&factura.pkFactura.sucursal=${facturaBienInventariado.factura.pkFactura.sucursal?c}&factura.pkFactura.tip_docum=${facturaBienInventariado.factura.pkFactura.tip_docum?c}&navigationId=factura-visualizar&flowControl=regis&navegacionIdBack=${navigationId}">
							<img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver"  border="0">
						</@security.a>
						</div>
					</@display.column>
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" title="Nro." class="botoneraAnchoCon4" property="factura.numeroString"/>
										
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="factura.fechaEmision" format="{0,date,dd/MM/yyyy}"  title="Fecha Emisi&oacute;n" />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" title="Proveedor">  
						<#if facturaBienInventariado.factura.proveedor?exists>
						${facturaBienInventariado.factura.proveedor.detalleDePersona.razonSocial}
						<#if facturaBienInventariado.factura.proveedor.detalleDePersona.nombre?exists>
						 ${facturaBienInventariado.factura.proveedor.detalleDePersona.nombre}
						 </#if>
						 </#if>
					</@display.column> 
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" title="Total" >
									${facturaBienInventariado.factura.totalFactura?string(",##0.00")}
					 </@display.column>
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="factura.estado" title="Estado" />		
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="factura.claseComprobante" title="Clase" />
					
						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="tipoGasto" title="Tipo Gasto" />
					
				

				</@display.table>
	</div>
</@vc.anchors>
