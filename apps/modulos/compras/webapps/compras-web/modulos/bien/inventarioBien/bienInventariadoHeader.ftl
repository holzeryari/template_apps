<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<@s.hidden id="bienInventariado.inventarioBien.oid" name="bienInventariado.inventarioBien.oid"/>
<@s.hidden id="bienInventariado.oid" name="bienInventariado.oid"/>
<@s.hidden id="bienInventariado.bien.oid" name="bienInventariado.bien.oid"/>
<@s.hidden id="bienInventariado.cliente.oid" name="bienInventariado.cliente.oid"/>

	<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
			<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos del Inventario</b></td>
			</tr>
		</table>
		
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
		<tr>
        	<td class="textoCampo" colspan="4">&nbsp;</td>
        </tr>	
		<tr>
  			<td class="textoCampo">N&uacute;mero:</td>
  			<td class="textoDato">
  				<@s.property default="&nbsp;" escape=false value="bienInventariado.inventarioBien.numero"/>
  			</td>
  			<td  class="textoCampo">Fecha: </td>
			<td class="textoDato">
				<@s.if test="bienInventariado.inventarioBien.fecha != null">
					<#assign fechaAjuste = bienInventariado.inventarioBien.fecha> 
					${fechaAjuste?string("dd/MM/yyyy")}	
				</@s.if>									
				<@s.else>									
					&nbsp;
				</@s.else>									
			</td>
		</tr>	
		<tr>
			<td class="textoCampo">Tipo Inventario:</td>
  			<td class="textoDato" colspan="3">
  				<@s.property default="&nbsp;" escape=false value="bienInventariado.inventarioBien.tipoInventario"/>
			</td>
		</tr>
		<tr>
			<td class="textoCampo">Observaciones:</td>
  			<td  class="textoDato" colspan="3">
      			<@s.property default="&nbsp;" escape=false value="bienInventariado.inventarioBien.observaciones"/>
				&nbsp;
			</td>
		</tr>
		<tr>
			<td class="textoCampo">Estado:</td>
  			<td class="textoDato" colspan="3">
				<@s.property default="&nbsp;" escape=false value="bienInventariado.inventarioBien.estado"/></td>
			</td>
		</tr>
		<tr><td class="textoCampo" colspan="4">&nbsp;</td></tr>
	</table>
	</div>


	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos del Bien</b></td>
			</tr>
		</table>
		
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
		<tr>
        	<td class="textoCampo" colspan="4">&nbsp;</td>
        </tr>	
		<tr>
  			<td class="textoCampo">C&oacute;digo:</td>
  			<td class="textoDato">
  					<@s.property default="&nbsp;" escape=false value="bienInventariado.bien.oid"/>
  			</td>
  			<td class="textoCampo">Descripci&oacute;n: </td>
			<td class="textoDato">
				<@s.property default="&nbsp;" escape=false value="bienInventariado.bien.descripcion"/>
			</td>	      			
		</tr>	
		<tr>
			<td class="textoCampo">Marca:</td>
  			<td class="textoDato">
  					<@s.property default="&nbsp;" escape=false value="bienInventariado.bien.marca"/>
			</td>
			<td class="textoCampo">Modelo:</td>
  			<td  class="textoDato">
      			<@s.property default="&nbsp;" escape=false value="bienInventariado.bien.modelo"/>
			</td>
		</tr>
		<tr>
			<td class="textoCampo">Valor Mercado:</td>
			<td  class="textoDato">
				<@s.property default="&nbsp;" escape=false value="bienInventariado.bien.valorMercado"/></td>
			</td>
			<td class="textoCampo">Fecha Valor Mercado:</td>
			<td  class="textoDato">
				<@s.property default="&nbsp;" escape=false value="bienInventariado.bien.fechaValorMercado"/></td>
			</td>
		</tr>
		<tr>
			<td class="textoCampo">Rubro:</td>
			<td  class="textoDato">
				<@s.property default="&nbsp;" escape=false value="bienInventariado.bien.rubro.descripcion"/></td>
			</td>
			<td class="textoCampo">Es cr&iacute;tico:</td>
			<td  class="textoDato">
				<@s.property default="&nbsp;" escape=false value="bienInventariado.bien.critico"/></td>
			</td>
		</tr>
		<tr>
			<td class="textoCampo">C&oacute;digo de barras:</td>
			<td  class="textoDato">
				<@s.property default="&nbsp;" escape=false value="bienInventariado.bien.codigoBarra"/></td>
			</td>
			<td class="textoCampo">Amortizaci&oacute;n:</td>
			<td  class="textoDato">
				<@s.property default="&nbsp;" escape=false value="bienInventariado.bien.amortizacion"/> (%)</td>
			</td>
		</tr>
		<tr>
			<td class="textoCampo">Es registrable:</td>
			<td  class="textoDato" colspan="3">
				<@s.property default="&nbsp;" escape=false value="bienInventariado.bien.registrable"/></td>
			</td>
		</tr>
		<tr>
			<td class="textoCampo">Es veh&iacute;culo:</td>
			<td  class="textoDato">
				<@s.property default="&nbsp;" escape=false value="bienInventariado.bien.vehiculo"/></td>
			</td>

				    		
			
			<td class="textoCampo">Estado:</td>
			<td  class="textoDato">
				<@s.property default="&nbsp;" escape=false value="bienInventariado.bien.estado"/></td>
			</td>
		</tr>
		<tr><td class="textoCampo" colspan="4">&nbsp;</td></tr>
	</table>				
	</div>
