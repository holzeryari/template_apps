<#setting locale="es_AR">

<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>

<@s.hidden id="navigationId" name="navigationId"/>
<@s.hidden id="bienInventariado.bien.descripcion" name="bienInventariado.bien.descripcion"/>
<@s.hidden id="bienInventariado.bien.rubro.oid" name="bienInventariado.bien.rubro.oid"/>
<@s.hidden id="bienInventariado.bien.rubro.descripcion" name="bienInventariado.bien.rubro.descripcion"/>
<@s.hidden id="bienInventariado.cliente.oid" name="bienInventariado.cliente.oid"/>
<@s.hidden id="bienInventariado.cliente.descripcion" name="bienInventariado.cliente.descripcion"/>
<@s.hidden id="posicionInicial" name="posicionInicial"/>
<@s.hidden id="bienInventariado.bien.oid" name="bienInventariado.bien.oid"/>


<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
        	<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
			<tr>
				<td><b>Datos del Bien</b></td>
			</tr>
		</table>
		
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
	    		<td class="textoCampo" colspan="4">&nbsp;</td>
	    	</tr>
			<tr>
				<td class="textoCampo">Bien:</td>
      			<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="bienInventariado.bien.descripcion"/>
					<@s.a templateDir="custontemplates" id="seleccionarBien" name="seleccionarBien" href="javascript://nop/">
						<img src="${request.contextPath}/common/images/buscar.gif"  border="0" align="absmiddle" hspace="3" >
					</@s.a>	
				</td>
				<td class="textoCampo">N&uacute;mero de Inventario:</td>
      			<td class="textoDato">
	      			<@s.textfield 
      					templateDir="custontemplates" 
						id="bienInventariado.numeroInventario" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="bienInventariado.numeroInventario" 
						title="N&uacute;mero de Inventario" />
				</td>
    		</tr>					
			<tr>
				<td class="textoCampo">Rubro:</td>
      			<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="bienInventariado.bien.rubro.descripcion"/>
					<@s.a templateDir="custontemplates" id="seleccionarRubro" name="seleccionarRubro" href="javascript://nop/">
						<img src="${request.contextPath}/common/images/buscar.gif" border="0" align="absmiddle" hspace="3" >
					</@s.a>		
					</td>
				<td class="textoCampo">Embargado:</td>
      			<td class="textoDato">
      			<@s.select 
						templateDir="custontemplates" 
						id="bienInventariado.estaEmbargado" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="bienInventariado.estaEmbargado" 
						list="estaEmbargadoList" 
						listKey="key" 
						listValue="description" 
						value="bienInventariado.estaEmbargado.ordinal()"										
						headerKey="0" 
	                    headerValue="Todos"
	                    title="Embargado"
			       		 />
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Cliente:</td>
      			<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="bienInventariado.cliente.descripcion"/>
					<@s.a templateDir="custontemplates" id="seleccionarCliente" name="seleccionarCliente" href="javascript://nop/">
						<img src="${request.contextPath}/common/images/buscar.gif"  border="0" align="absmiddle" hspace="3" >
					</@s.a>		
				</td>
				<td class="textoCampo">Estado: </td>
				<td class="textoDato">
					<@s.select 
						templateDir="custontemplates" 
						id="bienInventariado.estado" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="bienInventariado.estado" 
						list="estadoList" 
						listKey="key" 
						listValue="description" 
						value="bienInventariado.estado.ordinal()"
						headerKey="0" 
	                    headerValue="Todos"										
						title="Estado" />
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Fecha Alta Desde:</td>
      			<td class="textoDato" align="left" >
					<@vc.rowCalendar 
						templateDir="custontemplates" 
						id="fechaDesde" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="fechaDesde" 
						title="Fecha Desde" />
				</td>
				<td class="textoCampo">Fecha Alta Hasta:</td>
      			<td class="textoDato">
					<@vc.rowCalendar 
						templateDir="custontemplates" 
						id="fechaHasta" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="fechaHasta" 
						title="Fecha Hasta" />
				</td>
			</tr>
				<tr>
	    		<td colspan="4" class="lineaGris" align="right">
	      			<input id="limpiar" type="button" name="btnVolver" value="Limpiar" class="boton"/>		
	      			<input id="buscar" type="button" name="btnVolver" value="Buscar" class="boton"/>
	    		</td>
			</tr>	
		</table>
	</div>		
	<!-- Resultado Filtro -->
	<@s.if test="bienInventariadoList!=null">
					
		<#assign numeroInventario = "" />
		<@s.if test="bienInventariado.numeroInventario != null">
			<#assign numeroInventario = "${bienInventariado.numeroInventario}" />
		</@s.if>
		<#assign descripcion = "" />
		<@s.if test="bienInventariado.bien.descripcion != null">
			<#assign descripcion = "${bienInventariado.bien.descripcion}" />
		</@s.if>
		<#assign bienDescripcion = "" />
		<@s.if test="bienInventariado.bien.descripcion != null">
			<#assign bienDescripcion = "${bienInventariado.bien.descripcion}" />
		</@s.if>
		<#assign rubroDescripcion = "" />
		<#assign rubroOid = "0" />
		<@s.if test="bienInventariado.bien.rubro != null && bienInventariado.bien.rubro.oid != null && bienInventariado.bien.rubro.descripcion != null">
			<#assign rubroDescripcion = "${bienInventariado.bien.rubro.descripcion}" />
			<#assign rubroOid = "${bienInventariado.bien.rubro.oid}" />
		</@s.if>
		<#assign clienteDescripcion = "" />
		<#assign clienteOid = "0" />
		<@s.if test="bienInventariado.cliente != null && bienInventariado.cliente.oid != null && bienInventariado.cliente.descripcion != null">
			<#assign clienteDescripcion = "${bienInventariado.cliente.descripcion}" />
			<#assign clienteOid = "${bienInventariado.cliente.oid}" />
		</@s.if>
		<#assign bienInventariadoEstado = "${bienInventariado.estado.ordinal()}" />
		<#assign bienInventariadoEstaEmbargado = "${bienInventariado.estaEmbargado.ordinal()}" />
		
		<#assign bienInventariadoFechaDesde = "" />
		<@s.if test="fechaDesde != null">
			<#assign bienInventariadoFechaDesde = "${fechaDesde?string('dd/MM/yyyy')}" />
		</@s.if>

		<#assign bienInventariadoFechaHasta = "" />
		<@s.if test="fechaHasta != null">
			<#assign bienInventariadoFechaHasta = "${fechaHasta?string('dd/MM/yyyy')}" />
		</@s.if>

		<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
			<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
					<tr>
						<td>Bienes encontrados</td>
						<td>
							<div class="alineacionDerecha">
								<@security.a 
									templateDir="custontemplates" 
									securityCode="CUF0241" 
									cssClass="item" 
									href="${request.contextPath}/bien/inventarioBien/printXLSBienInventariado.action?bienInventariado.numeroInventario=${numeroInventario}&bienInventariado.bien.descripcion=${bienDescripcion}&bienInventariado.estaEmbargado=${bienInventariadoEstaEmbargado}&bienInventariado.bien.rubro.descripcion=${rubroDescripcion}&bienInventariado.cliente.descripcion=${clienteDescripcion}&bienInventariado.estado=${bienInventariadoEstado}&bienInventariado.bien.rubro.oid=${rubroOid}&bienInventariado.cliente.oid=${clienteOid}&fechaDesde=${bienInventariadoFechaDesde}&fechaHasta=${bienInventariadoFechaHasta}">
									<img src="${request.contextPath}/common/images/imprimirExcel.gif" title="Exportar a Excel" align="absmiddle" border="0" hspace="3">
								</@security.a>		
							</div>
							<div class="alineacionDerecha">
								<@security.a 
										templateDir="custontemplates" 
										securityCode="CUF0241" 
										cssClass="item" 
										href="${request.contextPath}/bien/inventarioBien/printPDFBienInventariado.action?bienInventariado.numeroInventario=${numeroInventario}&bienInventariado.bien.descripcion=${bienDescripcion}&bienInventariado.estaEmbargado=${bienInventariadoEstaEmbargado}&bienInventariado.bien.rubro.descripcion=${rubroDescripcion}&bienInventariado.cliente.descripcion=${clienteDescripcion}&bienInventariado.estado=${bienInventariadoEstado}&bienInventariado.bien.rubro.oid=${rubroOid}&bienInventariado.cliente.oid=${clienteOid}&fechaDesde=${bienInventariadoFechaDesde}&fechaHasta=${bienInventariadoFechaHasta}">
										<img src="${request.contextPath}/common/images/imprimirPdf.gif" title="Exportar a PDF" align="absmiddle" border="0" hspace="3">
									</@security.a>
							</div>
							<div class="alineacionDerecha">
								<b>Pos.Inicial:</b>
								<@s.select 
									templateDir="custontemplates" 
									id="posi" 
									cssClass="textarea"
									cssStyle="width:50px" 
									name="posi" 
									list="posicionInicialList" 
									listKey="key" 
									listValue="description" 
									value="posicionInicial"
									title="Posicion Inicial"
									onchange="javascript:posicionInicialSelect(this);" 
									/>
									<@security.a 
									templateDir="custontemplates" 
									securityCode="CUF0241" 
									cssClass="item" 
									id="codigoBarraLink"
									name="codigoBarraLink"
									onclick="javascript:codigoBarraLinkOnClick();"
									href="${request.contextPath}/bien/inventarioBien/printPDFCodigoBarraBienInventariado.action?posicionInicial=${posicionInicial}&bienInventariado.numeroInventario=${numeroInventario}&bienInventariado.bien.descripcion=${bienDescripcion}&bienInventariado.estaEmbargado=${bienInventariadoEstaEmbargado}&bienInventariado.bien.rubro.descripcion=${rubroDescripcion}&bienInventariado.cliente.descripcion=${clienteDescripcion}&bienInventariado.estado=${bienInventariadoEstado}&bienInventariado.bien.rubro.oid=${rubroOid}&bienInventariado.cliente.oid=${clienteOid}&fechaDesde=${bienInventariadoFechaDesde}&fechaHasta=${bienInventariadoFechaHasta}">
									<img src="${request.contextPath}/common/images/barcode.gif" title="Generacion de Codigos de Barras" align="absmiddle" border="0" hspace="3">
								</@security.a>
						</div>	
					</td>
				</tr>
			</table>
			
				<@vc.anchors target="contentTrx" ajaxFlag="ajax">
			         <@display.table class="tablaDetalleCuerpo" cellpadding="3" name="bienInventariadoList" id="bienInventariado" pagesize=15 partialList=true size="recordSize" keepStatus=true excludedParams="resetFlag">
								
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select"  class="botoneraAnchoCon3" title="Acciones">
					<div class="alineacion">				
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0242" 
							enabled="bienInventariado.readable" 
							cssClass="item" 
							href="${request.contextPath}/bien/inventarioBien/readBienInventariadoView.action?bienInventariado.oid=${bienInventariado.oid?c}&navegacionIdBack=buscar-bienInventariado&navigationId=verBienInventariado&flowControl=regis">
							<img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver" border="0">
						</@security.a>
						</div>
						<div class="alineacion">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0243" 
							enabled="bienInventariado.updatable"
							cssClass="item"  
							href="${request.contextPath}/bien/inventarioBien/updateBienInventariadoGestionarView.action?bienInventariado.oid=${bienInventariado.oid?c}&navigationId=administrar-updateDetalle&flowControl=regis&navegacionIdBack=buscar-bienInventariado">
							<img  src="${request.contextPath}/common/images/modificar.gif" alt="Modificar" title="Modificar" border="0">
						</@security.a>									
						</div>
						<div class="alineacion">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0244" 
							enabled="bienInventariado.eraseable"
							cssClass="item"  
							href="${request.contextPath}/bien/inventarioBien/bajaBienInventariadoView.action?bienInventariado.oid=${bienInventariado.oid?c}&navegacionIdBack=buscar-bienInventariado&flowControl=regis&navigationId=bienInventariado-baja">
							<img  src="${request.contextPath}/common/images/desactivar.gif" alt="Baja" title="Baja" border="0">
						</@security.a>									
						</div>
					</@display.column>
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="numeroInventario" title="Nro. Inventario" />
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="bien.descripcion" title="Bien" />
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="bien.rubro.descripcion" title="Rubro" />
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="fechaAlta" format="{0,date,dd/MM/yyyy}" title="Fecha Alta" />
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="cliente.descripcion" title="Cliente" />
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="estaEmbargado" title="Embargado" />
										
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="estado" title="Estado" />
					
				</@display.table>
			</@vc.anchors>
		</div>	
	</@s.if>
	
<@vc.htmlContent 
  baseUrl="${request.contextPath}/bien/inventarioBien/searchBienInventariado.action" 
  source="buscar" 
  success="contentTrx" 
  failure="errorTrx" 
 parameters="resetFlag=true,navigationId={navigationId},flowControl=regis,bienInventariado.numeroInventario={bienInventariado.numeroInventario},bienInventariado.bien.descripcion={bienInventariado.bien.descripcion},bienInventariado.estaEmbargado={bienInventariado.estaEmbargado}, bienInventariado.bien.rubro.oid={bienInventariado.bien.rubro.oid},bienInventariado.bien.rubro.descripcion={bienInventariado.bien.rubro.descripcion},bienInventariado.cliente.oid={bienInventariado.cliente.oid},bienInventariado.cliente.descripcion={bienInventariado.cliente.descripcion},bienInventariado.estado={bienInventariado.estado},posicionInicial={posicionInicial},fechaDesde={fechaDesde},fechaHasta={fechaHasta},bienInventariado.bien.oid={bienInventariado.bien.oid}"/>
 
<@vc.htmlContent 
  baseUrl="${request.contextPath}/bien/inventarioBien/viewBienInventariado.action" 
  source="limpiar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=regis"/>
  
  
 <@vc.htmlContent 
  baseUrl="${request.contextPath}/bien/inventarioBien/selectCliente.action" 
  source="seleccionarCliente" 
  success="contentTrx" 
  failure="errorTrx" 
   parameters="navigationId={navigationId},flowControl=change,navegacionIdBack={navigationId},bienInventariado.numeroInventario={bienInventariado.numeroInventario},bienInventariado.bien.descripcion={bienInventariado.bien.descripcion},bienInventariado.estaEmbargado={bienInventariado.estaEmbargado}, bienInventariado.bien.rubro.oid={bienInventariado.bien.rubro.oid},bienInventariado.bien.rubro.descripcion={bienInventariado.bien.rubro.descripcion},bienInventariado.cliente.oid={bienInventariado.cliente.oid},bienInventariado.cliente.descripcion={bienInventariado.cliente.descripcion},bienInventariado.estado={bienInventariado.estado},posicionInicial={posicionInicial},fechaDesde={fechaDesde},fechaHasta={fechaHasta},bienInventariado.bien.oid={bienInventariado.bien.oid}"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/bien/inventarioBien/selectBienSearch.action" 
  source="seleccionarBien" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=change,navegacionIdBack={navigationId},bienInventariado.numeroInventario={bienInventariado.numeroInventario},bienInventariado.bien.descripcion={bienInventariado.bien.descripcion},bienInventariado.estaEmbargado={bienInventariado.estaEmbargado}, bienInventariado.bien.rubro.oid={bienInventariado.bien.rubro.oid},bienInventariado.bien.rubro.descripcion={bienInventariado.bien.rubro.descripcion},bienInventariado.cliente.oid={bienInventariado.cliente.oid},bienInventariado.cliente.descripcion={bienInventariado.cliente.descripcion},bienInventariado.estado={bienInventariado.estado},posicionInicial={posicionInicial},fechaDesde={fechaDesde},fechaHasta={fechaHasta},bienInventariado.bien.oid={bienInventariado.bien.oid}"/>
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/bien/inventarioBien/selectRubro.action" 
  source="seleccionarRubro" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=change,navegacionIdBack={navigationId},bienInventariado.numeroInventario={bienInventariado.numeroInventario},bienInventariado.bien.descripcion={bienInventariado.bien.descripcion},bienInventariado.estaEmbargado={bienInventariado.estaEmbargado}, bienInventariado.bien.rubro.oid={bienInventariado.bien.rubro.oid},bienInventariado.bien.rubro.descripcion={bienInventariado.bien.rubro.descripcion},bienInventariado.cliente.oid={bienInventariado.cliente.oid},bienInventariado.cliente.descripcion={bienInventariado.cliente.descripcion},bienInventariado.estado={bienInventariado.estado},posicionInicial={posicionInicial},fechaDesde={fechaDesde},fechaHasta={fechaHasta},bienInventariado.bien.oid={bienInventariado.bien.oid}"/>
    
  