<#setting locale="es_AR">

<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>

<@s.if test="embargoBienDetalleList != null && embargoBienDetalleList.size > 0">
	<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
		<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
			<tr>
				<td>Embargos</td>
			</tr>
		</tbody>
	</table>
	

	<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="embargoBienDetalleList" id="embargoBienDetalle" defaultsort=1 >

		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="embargoBien.fechaInicio" format="{0,date,dd/MM/yyyy}" title="Fecha Inicio" />

		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="embargoBien.fechaFin" format="{0,date,dd/MM/yyyy}" title="Fecha Fin" />

		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="embargoBien.juzgado" title="Juzgado" />

		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="embargoBien.caratula" title="Car&aacute;tula" />

		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="embargoBien.expediente" title="Expediente" />

		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="embargoBien.estado" title="Estado" />

		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="montoEmbargo" title="Monto" />

	</@display.table>
	</div>
</@s.if>
