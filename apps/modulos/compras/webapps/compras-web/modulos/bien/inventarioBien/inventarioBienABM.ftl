<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign tiles=JspTaglibs["/WEB-INF/tlds/tiles-jsp.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<@s.hidden id="inventarioBien.oid" name="inventarioBien.oid"/>
<@s.hidden id="inventarioBien.versionNumber" name="inventarioBien.versionNumber"/>

<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
			<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos del Inventario de Bienes</b></td>
				<td>
					<div align="right">
						<@s.a templateDir="custontemplates" id="modificarInventarioBien" name="modificarInventarioBien" href="javascript://nop/" cssClass="ocultarIcono">
							<b>Modificar</b><img src="${request.contextPath}/common/images/modificar.gif"  border="0" align="absmiddle" hspace="3" >
						</@s.a>
					</div>
				</td>
			</tr>
		</table>
		
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>				
			<tr>
      			<td class="textoCampo">N&uacute;mero:</td>
      			<td class="textoDato"><@s.property default="&nbsp;" escape=false value="inventarioBien.numero"/></td>
      			<td  class="textoCampo">Fecha: </td>
				<td class="textoDato">
					<@s.if test="inventarioBien.fecha != null">
						<#assign fechaAjuste = inventarioBien.fecha> 
						${fechaAjuste?string("dd/MM/yyyy")}
					</@s.if>
					<@s.else>
						&nbsp;
					</@s.else>
				</td>	      			
			</tr>	
			<tr>
				<td class="textoCampo">Tipo Inventario:</td>
	  			<td class="textoDato">
	      			<@s.select 
						templateDir="custontemplates" 
						id="inventarioBien.tipoInventario"
						name="inventarioBien.tipoInventario"
						cssClass="textarea"
						cssStyle="width:160px"																 
						list="tipoInventarioList" 
						listKey="key" 
						listValue="description" 
						value="inventarioBien.tipoInventario.ordinal()"
						title="Tipo Inventario"
						headerKey="0"
						headerValue="Seleccionar" 
						/>
				</td>
				<td class="textoCampo">Fecha Alta:</td>
	      		<td class="textoDato">
						<@vc.rowCalendar 
							templateDir="custontemplates" 
							id="inventarioBien.fechaAlta" 
							cssClass="textarea"
							cssStyle="width:160px" 
							name="inventarioBien.fechaAlta" 
							title="Fecha Alta" />
				</td>
	    	</tr>
	    	<tr>
				<td class="textoCampo">Observaciones:</td>
	      		<td  class="textoDato" colspan="3">
					<@s.textarea	
						templateDir="custontemplates" 						  
						cols="89" rows="4"	     			
						cssClass="textarea"
						id="inventarioBien.observaciones"							 
						name="inventarioBien.observaciones"  
						label="Observaciones"														
						 />
				</td>					
	    	</tr>
			<tr>
				<td class="textoCampo">Estado:</td>
	      		<td  class="textoDato" colspan="3">
					<@s.property default="&nbsp;" escape=false value="inventarioBien.estado"/></td>
				</td>
			</tr>
			<tr><td colspan="4" class="textoCampo">&nbsp;</td></tr>
		</table>		
	</div> 	
			
		
<@vc.htmlContent 
  baseUrl="${request.contextPath}/bien/inventarioBien/updateInventarioBienView.action" 
  source="modificarInventarioBien" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="inventarioBien.oid={inventarioBien.oid},inventarioBien.versionNumber={inventarioBien.versionNumber}"/>



