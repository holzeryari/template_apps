
<#setting locale="es_AR">

<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>

<@s.hidden id="navigationId" name="navigationId"/>

	<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
        	<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
			<tr>
				<td><b>Datos del Inventario de Bien</b></td>
				<td>
					<div align="right">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0221" 
							enabled="true" 
							cssClass="item" 
							id="crear"										
							href="javascript://nop/">
							<b >Agregar</b>										
							<img src="${request.contextPath}/common/images/agregar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
						</@security.a>					
					</div>
				</td>
			</tr>
		</table>

		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
	    		<td class="textoCampo" colspan="4">&nbsp;</td>
	    	</tr>		
			<tr>
				<td class="textoCampo">N&uacute;mero:</td>
      			<td class="textoDato">
      			<@s.textfield 
      					templateDir="custontemplates" 
						id="inventarioBien.numero" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="inventarioBien.numero" 
						title="N&uacute;mero" />
				</td>							
	      		<td class="textoCampo">Tipo Inventario:</td>
	      		<td class="textoDato">
	      			<@s.select 
							templateDir="custontemplates" 
							id="tipoInventario" 
							cssClass="textarea"
							cssStyle="width:165px" 
							name="tipoInventario" 
							list="tipoInventarioList" 
							listKey="key" 
							listValue="description" 
							value="inventarioBien.tipoInventario.ordinal()"
							title="Estado"
							headerKey="0"
							headerValue="Todos"							
							/>
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Fecha Alta Desde:</td>
      			<td class="textoDato">
					<@vc.rowCalendar 
					templateDir="custontemplates" 
					id="fechaDesde" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="fechaDesde" 
					title="Fecha Desde" />
				</td>
				<td class="textoCampo">Fecha Alta Hasta:</td>
      			<td class="textoDato">
				<@vc.rowCalendar 
					templateDir="custontemplates" 
					id="fechaHasta" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="fechaHasta" 
					title="Fecha Hasta" />
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Estado:</td>
      			<td class="textoDato" colspan="3">
      			<@s.select 
						templateDir="custontemplates" 
						id="inventarioBien.estado" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="inventarioBien.estado" 
						list="estadoList" 
						listKey="key" 
						listValue="description" 
						value="inventarioBien.estado.ordinal()"
						title="Estado"
						headerKey="0"
						headerValue="Todos"							
						/>
				</td>							
			</tr>
			<tr>
	    		<td colspan="4" class="lineaGris" align="right">
	      			<input id="limpiar" type="button" name="btnVolver" value="Limpiar" class="boton"/>		
	      			<input id="buscar" type="button" name="btnVolver" value="Buscar" class="boton"/>
	    		</td>
			</tr>	
		</table>
	</div>				
	<!-- Resultado Filtro -->
	<@s.if test="inventarioBienList!=null">
		<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
			<#assign inventarioBienNumero = "0" />
			<@s.if test="inventarioBien.numero != null">
				<#assign inventarioBienNumero = "${inventarioBien.numero}" />
			</@s.if>
		
			<#assign inventarioBienEstado = "0" />
			<@s.if test="inventarioBien.estado != null">
					<#assign inventarioBienEstado = "${inventarioBien.estado.ordinal()}" />
			</@s.if>
		
			<#assign inventarioBienFechaDesde = "" />
			<@s.if test="fechaDesde != null">
				<#assign inventarioBienFechaDesde = "${fechaDesde?string('dd/MM/yyyy')}" />
			</@s.if>
		
			<#assign inventarioBienFechaHasta = "" />
			<@s.if test="fechaHasta != null">
				<#assign inventarioBienFechaHasta = "${fechaHasta?string('dd/MM/yyyy')}" />
			</@s.if>
			
			<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>Inventarios de Bienes encontrados</td>
					<td>
						<div class="alineacionDerecha">
							<@security.a 
								templateDir="custontemplates" 
								securityCode="CUF0222" 
								cssClass="item" 
								href="${request.contextPath}/bien/inventarioBien/printXLS.action?inventarioBien.numero=${inventarioBienNumero}&inventarioBien.estado=${inventarioBienEstado}&fechaDesde=${inventarioBienFechaDesde}&fechaHasta=${inventarioBienFechaHasta}">
								<img src="${request.contextPath}/common/images/imprimirExcel.gif" title="Exportar a Excel" align="absmiddle" border="0" hspace="3">
							</@security.a>
						</div>
						<div class="alineacionDerecha">
							<b>Imprimir</b>
							<@security.a 
								templateDir="custontemplates" 
								securityCode="CUF0222" 
								cssClass="item" 
								href="${request.contextPath}/bien/inventarioBien/printPDF.action?inventarioBien.numero=${inventarioBienNumero}&inventarioBien.estado=${inventarioBienEstado}&fechaDesde=${inventarioBienFechaDesde}&fechaHasta=${inventarioBienFechaHasta}">
								<img src="${request.contextPath}/common/images/imprimirPdf.gif" title="Exportar a PDF" align="absmiddle" border="0" hspace="3">
							</@security.a>
						</div>	
					</td>
				</tr>
			</table>

			<@vc.anchors target="contentTrx">			
          		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="inventarioBienList" id="inventarioBien" pagesize=15 defaultsort=2 partialList=true size="recordSize" keepStatus=true excludedParams="resetFlag">
          			<@display.column headerClass="tbl-contract-service-select" class="botoneraAnchoCon4" title="Acciones">
						<div class="alineacion">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0223" 
							enabled="inventarioBien.readable" 
							cssClass="item" 
							href="${request.contextPath}/bien/inventarioBien/readInventarioBienView.action?inventarioBien.oid=${inventarioBien.oid?c}&navigationId=inventarioBien-visualizar&flowControl=regis">
							<img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver"  border="0">
						</@security.a>
						</div>
						<div class="alineacion">	
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0224" 
							enabled="inventarioBien.updatable"
							cssClass="item"  
							href="${request.contextPath}/bien/inventarioBien/administrarInventarioBienView.action?inventarioBien.oid=${inventarioBien.oid?c}&navigationId=inventarioBien-administrar&flowControl=regis">
							<img  src="${request.contextPath}/common/images/administrar.gif" alt="Administrar" title="Administrar"  border="0">
						</@security.a>
						</div>
						<div class="alineacion">
						<#if inventarioBien.estado.ordinal() == 1>
							<@security.a 
								templateDir="custontemplates" 
								securityCode="CUF0225" 
								enabled="inventarioBien.eraseable"
								cssClass="item"  
								href="${request.contextPath}/bien/inventarioBien/deleteInventarioBienView.action?inventarioBien.oid=${inventarioBien.oid?c}&navigationId=inventarioBien-eliminar&flowControl=regis">
								<img  src="${request.contextPath}/common/images/eliminar.gif" alt="Eliminar" title="Eliminar"  border="0">
							</@security.a>	
							
						<#elseif inventarioBien.estado.ordinal() == 2>		
							<@security.a 
								templateDir="custontemplates" 
								securityCode="CUF0226" 
								enabled="inventarioBien.eraseable"
								cssClass="item"  
								href="${request.contextPath}/bien/inventarioBien/bajaInventarioBienView.action?inventarioBien.oid=${inventarioBien.oid?c}&navigationId=inventarioBien-baja&flowControl=regis">
								<img  src="${request.contextPath}/common/images/anular.gif" alt="Anular" title="Anular"  border="0">
							</@security.a>	
						<#else>
							<@security.a 
								templateDir="custontemplates" 
								securityCode="CUF0225" 
								enabled="false"
								cssClass="item"  
								href="">
								<img  src="${request.contextPath}/common/images/eliminar.gif" alt="Eliminar" title="Eliminar"  border="0">
							</@security.a>					
						</#if>						
						</div>
						<div class="alineacion">						
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0227" 
							enabled="inventarioBien.readable" 
							cssClass="no-rewrite" 
							href="${request.contextPath}/bien/inventarioBien/printInventarioBienPDF.action?inventarioBien.oid=${inventarioBien.oid?c}">
							<img  src="${request.contextPath}/common/images/imprimir.gif" alt="Imprimir" title="Imprimir"  border="0">
						</@security.a>							
						</div>

					</@display.column>
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="numero" title="N&uacute;mero" />
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="tipoInventario" title="Tipo" />
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="estado" title="Estado" />					
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="fechaAlta" format="{0,date,dd/MM/yyyy}"  title="Fecha Alta" />

					

				</@display.table>
			</@vc.anchors>
		</div>
	</@s.if>
			
<@vc.htmlContent 
  baseUrl="${request.contextPath}/bien/inventarioBien/search.action"
  source="buscar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="resetFlag=true,navigationId={navigationId},flowControl=regis,inventarioBien.estado={inventarioBien.estado},inventarioBien.numero={inventarioBien.numero},fechaDesde={fechaDesde},fechaHasta={fechaHasta},inventarioBien.tipoInventario={tipoInventario}"/>
  		      

<@vc.htmlContent 
  baseUrl="${request.contextPath}/bien/inventarioBien/view.action"
  source="limpiar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=regis"/>
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/bien/inventarioBien/createInventarioBienView.action" 
  source="crear" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=inventarioBien-crear"/>
