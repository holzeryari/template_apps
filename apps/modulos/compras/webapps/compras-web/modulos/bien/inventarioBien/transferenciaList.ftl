<#setting locale="es_AR">

<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>

<@s.if test="transferenciaList != null && transferenciaList.size > 0">
	<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
		<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
			<tr>
				<td>Transferencias</td>
			</tr>
		</tbody>
	</table>

	<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="transferenciaList" id="transferenciaBien" defaultsort=1 >

		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="fecha" format="{0,date,dd/MM/yyyy}" title="Fecha" />

		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="clienteOrigen.descripcion" title="Cliente Origen" />

		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="clienteDestino.descripcion" title="Cliente Destino" />

	</@display.table>
	</div>
</@s.if>
