<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancelar" type="button" name="btnCancelar" value="Cancelar" class="boton"/>
				<input id="modificar" type="button" name="btnModificar" value="Modificar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>


<@vc.htmlContent 
  baseUrl="${request.contextPath}/bien/transferenciaBien/updateTransferenciaBien.action" 
  source="modificar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="transferenciaBien.oid={transferenciaBien.oid},transferenciaBien.clienteOrigen.oid={transferenciaBien.clienteOrigen.oid},transferenciaBien.clienteOrigen.descripcion={transferenciaBien.clienteOrigen.descripcion},transferenciaBien.clienteDestino.oid={transferenciaBien.clienteDestino.oid},transferenciaBien.observaciones={transferenciaBien.observaciones}"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancelar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=administrarTransferenciaBien,flowControl=back"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/bien/transferenciaBien/abmSelectClienteDestino.action" 
  source="seleccionarClienteDestino" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId=${navigationId},flowControl=change,transferenciaBien.oid={transferenciaBien.oid},transferenciaBien.clienteOrigen.oid={transferenciaBien.clienteOrigen.oid},transferenciaBien.clienteOrigen.descripcion={transferenciaBien.clienteOrigen.descripcion},transferenciaBien.clienteDestino.oid={transferenciaBien.clienteDestino.oid},transferenciaBien.clienteDestino.descripcion={transferenciaBien.clienteDestino.descripcion},transferenciaBien.observaciones={transferenciaBien.observaciones}"/>
