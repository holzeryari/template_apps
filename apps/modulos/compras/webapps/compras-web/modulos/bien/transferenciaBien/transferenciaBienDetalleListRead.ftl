<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<@vc.anchors target="contentTrx" ajaxFlag="ajax">
	<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
		<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
			<tr>
				<td>Bienes a Transferir</td>
				<td>
					<div align="right">
						<@s.a href="${request.contextPath}/bien/transferenciaBien/selectBienInventariado.action?navegacionIdBack=administrarTransferenciaBien&transferenciaBien.clienteOrigen.oid=${transferenciaBien.clienteOrigen.oid?c}&transferenciaBien.clienteOrigen.descripcion=${transferenciaBien.clienteOrigen.descripcion}&transferenciaBien.oid=${transferenciaBien.oid?c}" templateDir="custontemplates" id="agregarBienInventariado" name="agregarBienInventariado" cssClass="ocultarIcono">
						<b>Agregar</b><img src="${request.contextPath}/common/images/agregar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
					</@s.a>		
					</div>
				</td>
			</tr>
		</table>			

		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="transferenciaBien.transferenciaBienDetalleList" id="transferenciaBienDetalle" defaultsort=2>
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select" class="botoneraAnchoCon2" title="Acciones">
				<a href="${request.contextPath}/bien/transferenciaBien/readTransferenciaBienDetalleView.action?transferenciaBienDetalle.oid=${transferenciaBienDetalle.oid?c}&navegacionIdBack=${navigationId}&navigationId=transferenciaBienDetalle-ver"><img src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver" border="0"></a>
				&nbsp;
			</@display.column>
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="numero" title="N&uacute;mero Item" defaultorder="ascending"/>
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="bienInventariado.numeroInventario" title="Nro. Inventario" />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloDescripcion" property="bienInventariado.bien.descripcion" title="Descripci&oacute;n" />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloDescripcion" property="bienInventariado.bien.marca" title="Marca" />					
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloDescripcion" property="bienInventariado.bien.modelo" title="Modelo" />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="bienInventariado.numeroSerie" title="Identificador" />
		</@display.table>
	</div>	
</@vc.anchors>

