<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>


<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
	<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>Ordenes de Compra</td>
				</tr>
	</table>

	
	<@s.if test="recepcionProductoBien.estado.ordinal()==1">					
		<@vc.anchors target="contentTrx" ajaxFlag="ajax">					
		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="recepcionProductoBien.recepcionProductoBienOCEPList" id="recepcionProductoBienOCEP" defaultsort=2>	
								
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select"  class="botoneraAnchoCon2" title="Acciones">
				<div class="alineacion">	
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0383" 
							enabled="oc_os.readable" 
							cssClass="item" 
							id="verOCOS"
							name="verOCOS"
							href="${request.contextPath}/compraContratacion/oc_os/readOC_OSView.action?oc_os.oid=${recepcionProductoBienOCEP.ocos.oid?c}&navigationId=oc_os-visualizar&flowControl=regis&navegacionIdBack=${navigationId}">
							<img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver"  border="0">
						</@security.a>
				</div>
											
			</@display.column>
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="ocos.numero" title="N&uacute;mero" />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="ocos.fecha" format="{0,date,dd/MM/yyyy}"  title="Fecha" />	
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="ocos.importe" title="Importe" />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="ocos.estado" title="Estado" />
						
		</@display.table>
		</@vc.anchors>
	</@s.if>
	<@s.if test="recepcionProductoBien.estado.ordinal()==2">
		<@vc.anchors target="contentTrx" ajaxFlag="ajax">
		<table class="tablaDetalleCuerpo" cellpadding="3">
			<#assign ultimaOC=0>
			<#assign recepcionProductoBienOCEPlista = recepcionProductoBien.recepcionProductoBienOCEPList>
			<#list recepcionProductoBienOCEPlista as recepcionProductoBienOCEP>
				<#if ultimaOC != recepcionProductoBienOCEP.ocos.oid>									
					<tr>
						<th>Acciones</th>
						<th>Numero</th>
						<th>Fecha</th>
						<th>Importe</th>
						<th>Estado</th>							
					</tr>	
					<#assign ultimaOC=recepcionProductoBienOCEP.ocos.oid>	
					<tr>
						<td class="botoneraAnchoCon1"> 
							<div class="alineacion">
								<@security.a 
									templateDir="custontemplates" 
									securityCode="CUF0383" 
									enabled="oc_os.readable" 
									cssClass="item" 
									id="verOCOS"
									name="verOCOS"
									href="${request.contextPath}/compraContratacion/oc_os/readOC_OSView.action?oc_os.oid=${recepcionProductoBienOCEP.ocos.oid?c}&navigationId=oc_os-visualizar&flowControl=regis&navegacionIdBack=${navigationId}">
									<img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver"  border="0">
								</@security.a>													
							</div>
						</td>	
						<td class="estiloNumero">${recepcionProductoBienOCEP.ocos.numero}</td>
						<td class="estiloNumero">${recepcionProductoBienOCEP.ocos.fecha?string("dd/MM/yyyy")}</td>
						<td class="estiloNumero">${recepcionProductoBienOCEP.ocos.importe}</td>
						<td class="estiloTexto">${recepcionProductoBienOCEP.ocos.estado}</td>														   
					</tr>
											
					<tr>
						<td>&nbsp;</td>
						<th class="botoneraAnchoCon1">								
								<b>Entregas Parciales</b>									
						</th>
						<th align="center">Fecha</th>
						<th align="center" colspan="2">Observaciones</th>							
					</tr>	
				</#if>	
				<#if recepcionProductoBienOCEP.entregaParcial?exists>	
					<tr>						
						<td>&nbsp;</td>
						<td class="botoneraAnchoCon1">
							&nbsp;																		
						</td>
						<td class="estiloNumero">${recepcionProductoBienOCEP.entregaParcial.fecha?string("dd/MM/yyyy")}</td>
						<td class="estiloTexto" colspan="2"><#if recepcionProductoBienOCEP.entregaParcial.observaciones?exists>${recepcionProductoBienOCEP.entregaParcial.observaciones}</#if>&nbsp;</td>
					</tr>	
				</#if>																		
			</#list>						
		</table>
	</@vc.anchors>
</@s.if>
</div>
<div id="capaSeparadora" class="capaSeparadora">&nbsp;</div>