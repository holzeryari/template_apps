<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign tiles=JspTaglibs["/WEB-INF/tlds/tiles-jsp.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<@s.hidden id="navigationId" name="navigationId"/>
<@s.hidden id="recepcionProductoBien.oid" name="recepcionProductoBien.oid"/>
<@s.hidden id="recepcionProductoBien.versionNumber" name="recepcionProductoBien.versionNumber"/>
<@s.hidden id="recepcionProductoBien.proveedor.nroProveedor" name="recepcionProductoBien.proveedor.nroProveedor"/>
<@s.hidden id="recepcionProductoBien.proveedor.detalleDePersona.razonSocial" name="recepcionProductoBien.proveedor.detalleDePersona.razonSocial"/>
<@s.hidden id="recepcionProductoBien.proveedor.detalleDePersona.nombre" name="recepcionProductoBien.proveedor.detalleDePersona.nombre"/>

<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
			<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos de la Recepci&oacute;n de Producto/Bien</b></td>
				<td>
					<div align="right">
						<@s.a templateDir="custontemplates" id="modificarRecepcionProductoBien" name="modificarRecepcionProductoBien" href="javascript://nop/" cssClass="ocultarIcono">
							<b>Modificar</b><img src="${request.contextPath}/common/images/modificar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
						</@s.a>
					</div>
				</td>
			</tr>
		</table>
			
		<@tiles.insertAttribute name="recepcion"/>
	</div>	
	<@tiles.insertAttribute name="oc"/>
	<@tiles.insertAttribute name="detalle"/>
			
		