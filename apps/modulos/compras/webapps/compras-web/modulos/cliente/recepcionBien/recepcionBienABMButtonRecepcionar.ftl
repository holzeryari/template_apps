<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancelar" type="button" name="btnVolver" value="Cancelar" class="boton"/>
				<input id="recepcionar" type="button" name="btnRecepcionar" value="Recepcionar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

  
  
  <@vc.htmlContent 
   baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancelar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=buscar-recepcionBien,flowControl=back"/>
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/cliente/recepcionBien/realizarRecepcion.action" 
  source="recepcionar" 
  success="contentTrx" 
  failure="errorTrx"  
  preFunction="listaEntregasParciales"
  parameters="entregaSeleccion={entregaSeleccion},recepcionProductoBien.oid={recepcionProductoBien.oid}"/>
  

  <@vc.htmlContent 
  baseUrl="${request.contextPath}/cliente/recepcionBien/updateView.action" 
  source="modificarRecepcionProductoBien" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="recepcionProductoBien.oid={recepcionProductoBien.oid},recepcionProductoBien.versionNumber={recepcionProductoBien.versionNumber},navigationId=recepcionBien-actualizar"/>

  