<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancelar" type="button" name="btnVolver" value="Cancelar" class="boton"/>
				<input id="update" type="button" name="btnModificar" value="Modificar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/cliente/recepcionBien/update.action" 
  source="update" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="recepcionProductoBien.versionNumber={recepcionProductoBien.versionNumber},recepcionProductoBien.oid={recepcionProductoBien.oid},recepcionProductoBien.cliente.oid={recepcionProductoBien.cliente.oid},recepcionProductoBien.cliente.descripcion={recepcionProductoBien.cliente.descripcion},recepcionProductoBien.deposito.oid={recepcionProductoBien.deposito.oid},recepcionProductoBien.proveedor.nroProveedor={recepcionProductoBien.proveedor.nroProveedor},recepcionProductoBien.proveedor.razonSocial={recepcionProductoBien.proveedor.razonSocial},recepcionProductoBien.proveedor.nombre={recepcionProductoBien.proveedor.nombre},recepcionProductoBien.numeroFactura={recepcionProductoBien.numeroFactura},recepcionProductoBien.numeroRemito={recepcionProductoBien.numeroRemito},recepcionProductoBien.observaciones={recepcionProductoBien.observaciones},recepcionProductoBien.fechaRecepcion={recepcionProductoBien.fechaRecepcion}"/>
  

<@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancelar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=recepcionBien-administrar,flowControl=back"/>  

    