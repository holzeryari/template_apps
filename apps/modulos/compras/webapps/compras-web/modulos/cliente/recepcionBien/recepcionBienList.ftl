
<#setting locale="es_AR">

<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>
<#assign tiles=JspTaglibs["/WEB-INF/tlds/tiles-jsp.tld"]>


<@s.hidden id="navigationId" name="navigationId"/>
<@s.hidden id="recepcionProductoBien.cliente.descripcion" name="recepcionProductoBien.cliente.descripcion"/>
<@s.hidden id="recepcionProductoBien.proveedor.nroProveedor" name="recepcionProductoBien.proveedor.nroProveedor"/>
<@s.hidden id="recepcionProductoBien.proveedor.detalleDePersona.razonSocial" name="recepcionProductoBien.proveedor.detalleDePersona.razonSocial"/>
<@s.hidden id="recepcionProductoBien.proveedor.detalleDePersona.nombre" name="recepcionProductoBien.proveedor.detalleDePersona.nombre"/>

<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
        	<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
			<tr>
				<td><b>Datos de la Recepci&oacute;n de Bien</b></td>
				<td>
					<div align="right">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0561" 
							enabled="true" 
							cssClass="item" 
							id="crear"										
							href="javascript://nop/">
							<b >Agregar</b>										
							<img src="${request.contextPath}/common/images/agregar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
						</@security.a>			
					</div>
				</td>
			</tr>
		</table>
		
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
	    		<td class="textoCampo" colspan="4">&nbsp;</td>
	    	</tr>
			<tr>
				<td class="textoCampo">N&uacute;mero:</td>
      			<td class="textoDato">
      			<@s.textfield 
      					templateDir="custontemplates" 
						id="recepcionProductoBien.numero" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="recepcionProductoBien.numero" 
						title="N&uacute;mero" />
				</td>							
      			<td class="textoCampo">Proveedor:</td>
					<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="recepcionProductoBien.proveedor.detalleDePersona.razonSocial" />
					<@s.property default="&nbsp;" escape=false value="recepcionProductoBien.proveedor.detalleDePersona.nombre" />
					<@s.a templateDir="custontemplates" id="seleccionarProveedor" name="seleccionarProveedor" href="javascript://nop/" cssClass="ocultarIcono">
							<img src="${request.contextPath}/common/images/buscar.gif"  border="0" align="absmiddle" hspace="3" >
					</@s.a>	
				</td>						
			</tr>
			<tr>
			<td class="textoCampo">Estado:</td>
	  			<td class="textoDato" >
	  			<@s.select 
							templateDir="custontemplates" 
							id="estado" 
							cssClass="textarea"
							cssStyle="width:165px" 
							name="estado" 
							list="estadoList" 
							listKey="key" 
							listValue="description" 
							value="recepcionProductoBien.estado.ordinal()"
							title="Estado"
							headerKey="0"
							headerValue="Todos"							
							/>
				</td>
	           <@tiles.insertAttribute name="cliente"/>
			</tr>

			<tr>
				<td class="textoCampo">Fecha Recepci&oacute;n Desde:</td>
      			<td class="textoDato">
					<@vc.rowCalendar 
					templateDir="custontemplates" 
					id="fechaDesde" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="fechaDesde" 
					title="Fecha Recepcion Desde" />
				</td>
				<td class="textoCampo">Fecha Recepci&oacute;n Hasta:</td>
      			<td class="textoDato">
				<@vc.rowCalendar 
					templateDir="custontemplates" 
					id="fechaHasta" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="fechaHasta" 
					title="Fecha Recepcion Hasta" />
				</td>
			</tr>
				<tr>
	    		<td colspan="4" class="lineaGris" align="right">
	      			<input id="limpiar" type="button" name="btnVolver" value="Limpiar" class="boton"/>		
	      			<input id="buscar" type="button" name="btnVolver" value="Buscar" class="boton"/>
	    		</td>
			</tr>	
		</table>
	</div>
			
		
    <!-- Resultado Filtro -->
	
	<@s.if test="recepcionProductoBienList!=null">
				
				
				<#assign recepcionProveedor = "0" />				
				<@s.if test="recepcionProductoBien.proveedor != null && recepcionProductoBien.proveedor.nroProveedor!=null">
					<#assign recepcionProveedor = "${recepcionProductoBien.proveedor.nroProveedor}" />
				</@s.if>
				
				
				<#assign recepcionNumero = "0" />				
				<@s.if test="recepcionProductoBien.numero != null">
					<#assign recepcionNumero = "${recepcionProductoBien.numero}" />
				</@s.if>
			
				<#assign recepcionCliente = "0" />
				<@s.if test="recepcionProductoBien.cliente!= null && recepcionProductoBien.cliente.oid != null">
					<#assign recepcionCliente = "${recepcionProductoBien.cliente.oid}" />
				</@s.if>			

				<#assign recepcionEstado = "0" />
				<@s.if test="recepcionProductoBien.estado != null ">
					<#assign recepcionEstado = "${recepcionProductoBien.estado.ordinal()}" />
				</@s.if>

				<#assign recepcionFechaDesde = "" />
				<@s.if test="fechaDesde != null">
					<#assign recepcionFechaDesde = "${fechaDesde?string('dd/MM/yyyy')}" />
				</@s.if>

				<#assign recepcionFechaHasta = "" />
				<@s.if test="fechaHasta != null">
					<#assign recepcionFechaHasta = "${fechaHasta?string('dd/MM/yyyy')}" />
				</@s.if>
		<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
			<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>Recepciones de Bienes encontrados</td>
					<td>
						<div class="alineacionDerecha">
							<@security.a 
								templateDir="custontemplates" 
								securityCode="CUF0442" 
								cssClass="item" 
									href="${request.contextPath}/cliente/recepcionBien/printXLS.action?recepcionProductoBien.estado=${recepcionEstado}&recepcionProductoBien.numero=${recepcionNumero}&fechaDesde=${recepcionFechaDesde}&fechaHasta=${recepcionFechaHasta}&recepcionProductoBien.proveedor.nroProveedor=${recepcionProveedor}">
								<img src="${request.contextPath}/common/images/imprimirExcel.gif" title="Exportar a Excel" align="absmiddle" border="0" hspace="3">
							</@security.a>
						</div>
						<div class="alineacionDerecha">
							<b>Imprimir</b>
							<@security.a 
								templateDir="custontemplates" 
								securityCode="CUF0442" 
								cssClass="item" 
								href="${request.contextPath}/cliente/recepcionBien/printPDF.action?recepcionProductoBien.estado=${recepcionEstado}&recepcionProductoBien.numero=${recepcionNumero}&fechaDesde=${recepcionFechaDesde}&fechaHasta=${recepcionFechaHasta}&recepcionProductoBien.proveedor.nroProveedor=${recepcionProveedor}">
								<img src="${request.contextPath}/common/images/imprimirPdf.gif" title="Exportar a PDF" align="absmiddle" border="0" hspace="3">
							</@security.a>
						</div>	
					</td>
				</tr>
			</table>

		<@vc.anchors target="contentTrx">			
  		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="recepcionProductoBienList" id="recepcionProductoBien" pagesize=15 defaultsort=2 partialList=true size="recordSize" keepStatus=true excludedParams="resetFlag">
        	<@display.column headerClass="tbl-contract-service-select" class="botoneraAnchoCon4" title="Acciones">
				<div class="alineacion">
				<@security.a 
					templateDir="custontemplates" 
					securityCode="CUF0563" 
					enabled="recepcionProductoBien.readable" 
					cssClass="item" 
					href="${request.contextPath}/cliente/recepcionBien/readView.action?recepcionProductoBien.oid=${recepcionProductoBien.oid?c}&navigationId=recepcionBien-visualizar&flowControl=regis&navegacionIdBack=${navigationId}">
					<img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver"  border="0">
				</@security.a>
				</div>
				<div class="alineacion">	
				<@security.a 
					templateDir="custontemplates" 
					securityCode="CUF0564" 
					enabled="recepcionProductoBien.updatable"
					cssClass="item"  
					href="${request.contextPath}/cliente/recepcionBien/administrarView.action?recepcionProductoBien.oid=${recepcionProductoBien.oid?c}&navigationId=recepcionBien-administrar&flowControl=regis">
					<img  src="${request.contextPath}/common/images/administrar.gif" alt="Administrar" title="Administrar"  border="0">
				</@security.a>
				</div>
				<div class="alineacion">
				<@security.a 
					templateDir="custontemplates" 
					securityCode="CUF0565" 
					enabled="recepcionProductoBien.eraseable"
					cssClass="item"  
					href="${request.contextPath}/cliente/recepcionBien/deleteView.action?recepcionProductoBien.oid=${recepcionProductoBien.oid?c}&navigationId=recepcionBien-eliminar&flowControl=regis">
					<img  src="${request.contextPath}/common/images/eliminar.gif" alt="Eliminar" title="Eliminar"  border="0">
				</@security.a>				
				</div>
				<div class="alineacion">
				<@security.a 
					templateDir="custontemplates" 
					securityCode="CUF0566" 
					enabled="recepcionProductoBien.readable" 
					cssClass="no-rewrite" 
					href="${request.contextPath}/cliente/recepcionBien/printRecepcionBienPDF.action?recepcionProductoBien.oid=${recepcionProductoBien.oid?c}&navigationId=recepcionBien-eliminar&">
					<img  src="${request.contextPath}/common/images/imprimir.gif" alt="Imprimir" title="Imprimir"  border="0">
				</@security.a>		
							
				</div>
			</@display.column>


			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="numero" title="Nro." />				
								
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" title="Proveedor">
				<#if recepcionProductoBien.proveedor?exists>  
				${recepcionProductoBien.proveedor.detalleDePersona.razonSocial}
					<#if recepcionProductoBien.proveedor.detalleDePersona.nombre?exists> 
					  ${recepcionProductoBien.proveedor.detalleDePersona.nombre}
					</#if>
				 
				</#if>
			</@display.column>
			
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="fechaRecepcion" format="{0,date,dd/MM/yyyy}"  title="Fecha Recepci&oacute;n" /> 
				
			
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="cliente.descripcion" title="Cliente" />

			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="estado" title="Estado" />		

		</@display.table>
	</@vc.anchors>
	</div>
	</@s.if>
	
<@vc.htmlContent 
  baseUrl="${request.contextPath}/cliente/recepcionBien/search.action"
  source="buscar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="resetFlag=true,navigationId={navigationId},flowControl=regis,recepcionProductoBien.estado={estado},recepcionProductoBien.proveedor.nroProveedor={recepcionProductoBien.proveedor.nroProveedor},recepcionProductoBien.proveedor.detalleDePersona.razonSocial={recepcionProductoBien.proveedor.detalleDePersona.razonSocial},recepcionProductoBien.proveedor.detalleDePersona.nombre={recepcionProductoBien.proveedor.detalleDePersona.nombre},recepcionProductoBien.deposito.oid={recepcionProductoBien.deposito.oid},recepcionProductoBien.cliente.oid={recepcionProductoBien.cliente.oid},recepcionProductoBien.cliente.descripcion={recepcionProductoBien.cliente.descripcion},recepcionProductoBien.numero={recepcionProductoBien.numero},fechaDesde={fechaDesde},fechaHasta={fechaHasta}"/>
  		      

<@vc.htmlContent 
  baseUrl="${request.contextPath}/cliente/recepcionBien/view.action"
  source="limpiar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=regis"/>
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/cliente/recepcionBien/createView.action" 
  source="crear" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=recepcionBien-crear,flowControl=regis"/>
  
  <@vc.htmlContent 
  baseUrl="${request.contextPath}/cliente/recepcionBien/selectProveedorSearch.action" 
  source="seleccionarProveedor" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=change,recepcionProductoBien.estado={estado},recepcionProductoBien.proveedor.nroProveedor={recepcionProductoBien.proveedor.nroProveedor},recepcionProductoBien.proveedor.detalleDePersona.razonSocial={recepcionProductoBien.proveedor.detalleDePersona.razonSocial},recepcionProductoBien.proveedor.detalleDePersona.nombre={recepcionProductoBien.proveedor.detalleDePersona.nombre},recepcionProductoBien.deposito.oid={recepcionProductoBien.deposito.oid},recepcionProductoBien.cliente.oid={recepcionProductoBien.cliente.oid},recepcionProductoBien.cliente.descripcion={recepcionProductoBien.cliente.descripcion},recepcionProductoBien.numero={recepcionProductoBien.numero},fechaDesde={fechaDesde},fechaHasta={fechaHasta}"/>
  
  <@vc.htmlContent 
  baseUrl="${request.contextPath}/cliente/recepcionBien/selectClienteSearch.action" 
  source="seleccionarCliente" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=change,recepcionProductoBien.estado={estado},recepcionProductoBien.proveedor.nroProveedor={recepcionProductoBien.proveedor.nroProveedor},recepcionProductoBien.proveedor.detalleDePersona.razonSocial={recepcionProductoBien.proveedor.detalleDePersona.razonSocial},recepcionProductoBien.proveedor.detalleDePersona.nombre={recepcionProductoBien.proveedor.detalleDePersona.nombre},recepcionProductoBien.deposito.oid={recepcionProductoBien.deposito.oid},recepcionProductoBien.cliente.oid={recepcionProductoBien.cliente.oid},recepcionProductoBien.cliente.descripcion={recepcionProductoBien.cliente.descripcion},recepcionProductoBien.numero={recepcionProductoBien.numero},fechaDesde={fechaDesde},fechaHasta={fechaHasta}"/>
  
  
