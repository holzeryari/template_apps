<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign tiles=JspTaglibs["/WEB-INF/tlds/tiles-jsp.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>


<@s.hidden id="asignacionProveedor.oid" name="asignacionProveedor.oid"/>
<@s.hidden id="asignacionProveedor.versionNumber" name="asignacionProveedor.versionNumber"/>

<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
			<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos de la Asignaci&oacute;n de Proveedor</b></td>
				<td>
					<#if !asignacionProveedor.desahibilitarBotonesAsignacion>
						<div align="right">
							<@s.a templateDir="custontemplates" id="modificarAsignacionProveedor" name="modificarAsignacionProveedor" href="javascript://nop/" cssClass="ocultarIcono">
								<b>Modificar</b><img src="${request.contextPath}/common/images/modificar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
							</@s.a>
						</div>
					</#if>
				</td>
			</tr>
		</table>

		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>		
			<tr>
	      		<td class="textoCampo">N&uacute;mero:</td>
	      		<td class="textoDato"><@s.property default="&nbsp;" escape=false value="asignacionProveedor.numero"/></td>
	      		<td  class="textoCampo">Fecha Asignaci&oacute;n: </td>
				<td class="textoDato">
					<@s.if test="asignacionProveedor.fechaAsignacion != null">
					<#assign fechaAsignacion = asignacionProveedor.fechaAsignacion> 
					${fechaAsignacion?string("dd/MM/yyyy")}	
					</@s.if>
					<@s.else>
						&nbsp;
					</@s.else>								
				</td>	      			
			</tr>	

			<tr>
				<td class="textoCampo">Tipo:</td>
      			<td class="textoDato">
      			<@s.select 
							templateDir="custontemplates" 
							id="asignacionProveedor.tipo" 
							cssClass="textarea"
							cssStyle="width:165px" 
							name="asignacionProveedor.tipo" 
							list="tipoList" 
							listKey="key" 
							listValue="description" 
							value="asignacionProveedor.tipo.ordinal()"
							title="Tipo"
							headerKey="0"
							headerValue="Todos"							
							/>
				</td>
				<td class="textoCampo">Fecha L&iacute;mite Recepci&oacute;n de Cotizaciones:</td>
      			<td class="textoDato">
					<@vc.rowCalendar 
					templateDir="custontemplates" 
					id="asignacionProveedor.fechaLimiteCotizacion" 
					cssClass="textarea"
					cssStyle="width:160px"						
					name="asignacionProveedor.fechaLimiteCotizacion" 
					title="Fecha Limite de Cotizacion" />
				</td>	
    		</tr>
	    		
    		<tr>
				<td class="textoCampo">Fecha de Entrega/Prestaci&oacute;n:</td>
      			<td class="textoDato" colspan="3">
					<@vc.rowCalendar 
					templateDir="custontemplates" 
					id="asignacionProveedor.fechaEntrega" 
					cssClass="textarea"
					cssStyle="width:160px"						
					name="asignacionProveedor.fechaEntrega" 
					title="Fecha Entrega" />
				</td>	
			</tr>
				
			<tr>
				<td class="textoCampo">Lugar de Entrega/Prestaci&oacute;n:</td>
      			<td  class="textoDato" colspan="3">
				<@s.textarea	
						templateDir="custontemplates" 						  
						cols="89" rows="4"	      					
						cssClass="textarea"
						id="asignacionProveedor.lugarEntrega"							 
						name="asignacionProveedor.lugarEntrega"  
						label="Lugar de Entrega/Prestaci&oacute;n"														
						 />
				</td>					
    		</tr>
	    		
    		<tr>
				<td class="textoCampo">Observaciones:</td>
      			<td  class="textoDato" colspan="3">
				<@s.textarea	
						templateDir="custontemplates" 						  
						cols="89" rows="4"	      					
						cssClass="textarea"
						id="asignacionProveedor.observaciones"							 
						name="asignacionProveedor.observaciones"  
						label="Observaciones"														
						 />&nbsp;
				</td>					
    		</tr>
	    		
    		<tr>
				<td class="textoCampo">Estado:</td>
      			<td  class="textoDato" colspan="3">
					<@s.property default="&nbsp;" escape=false value="asignacionProveedor.estado"/></td>
				</td>
			</tr>
			<tr><td class="textoCampo" colspan="4">&nbsp;</td></tr>
	    </table>		
			
		</div>				
		<@tiles.insertAttribute name="formulariosAsignados"/>
			
		<@tiles.insertAttribute name="asigancionProveedorDetalle"/>			
			
		<@tiles.insertAttribute name="pedidosCotizacionGenerados"/>
			
		<@tiles.insertAttribute name="oc_osGeneradas"/>
		
		<@tiles.insertAttribute name="productoServicioNoSolicitados"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/compraContratacion/asignacionProveedor/updateAsignacionProveedorView.action" 
  source="modificarAsignacionProveedor" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="asignacionProveedor.oid={asignacionProveedor.oid},navigationId=asigancionProveedor-modificar"/>



