<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancelar" type="button" name="btnCancelar" value="Cancelar" class="boton"/>
				<input id="update" type="button" name="btnModificar" value="Modificar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/compraContratacion/asignacionProveedor/updateAsignacionProveedor.action" 
  source="update" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="asignacionProveedor.versionNumber={asignacionProveedor.versionNumber},asignacionProveedor.oid={asignacionProveedor.oid},asignacionProveedor,asignacionProveedor.fechaLimiteCotizacion={asignacionProveedor.fechaLimiteCotizacion},asignacionProveedor.fechaEntrega={asignacionProveedor.fechaEntrega},asignacionProveedor.tipo={asignacionProveedor.tipo},asignacionProveedor.lugarEntrega={asignacionProveedor.lugarEntrega},asignacionProveedor.observaciones={asignacionProveedor.observaciones}"/>
  

<@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancelar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=administrar-asignacionProveedor,flowControl=back"/>
  
  