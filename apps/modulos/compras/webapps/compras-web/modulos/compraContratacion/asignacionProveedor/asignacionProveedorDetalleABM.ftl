<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign tiles=JspTaglibs["/WEB-INF/tlds/tiles-jsp.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<@s.hidden id="asignacionProveedorDetalle.oid" name="asignacionProveedorDetalle.oid"/>
<@s.hidden id="asignacionProveedorDetalle.versionNumber" name="asignacionProveedorDetalle.versionNumber"/>

<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
			<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos de la Asignaci&oacute;n de Proveedor</b></td>
			</tr>
		</table>
	
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>
			<tr>
      			<td class="textoCampo">N&uacute;mero:</td>
      			<td class="textoDato">
      				<@s.property default="&nbsp;" escape=false value="asignacionProveedorDetalle.asignacionProveedor.numero"/>
      			</td>
      			<td  class="textoCampo">Fecha Asignaci&oacute;n: </td>
				<td class="textoDato" >
					<@s.if test="asignacionProveedorDetalle.asignacionProveedor.fechaAsignacion != null">
					<#assign fechaAsignacion = asignacionProveedorDetalle.asignacionProveedor.fechaAsignacion> 
					${fechaAsignacion?string("dd/MM/yyyy")}	
					</@s.if>		&nbsp;							
				</td>	      			
			</tr>	

			<tr>
			<td class="textoCampo">Tipo:</td>
      			<td class="textoDato">
      			<@s.property default="&nbsp;" escape=false value="asignacionProveedorDetalle.asignacionProveedor.tipo"/>
      			</td>
				<td class="textoCampo">Fecha L&iacute;mite Recepci&oacute;n de Cotizaciones:</td>
      			<td class="textoDato">	
      			<@s.if test="asignacionProveedorDetalle.asignacionProveedor.fechaLimiteCotizacion != null">
				<#assign fechaLimiteCotizacion = asignacionProveedorDetalle.asignacionProveedor.fechaLimiteCotizacion> 
				${fechaLimiteCotizacion?string("dd/MM/yyyy")}	
				</@s.if>	
				&nbsp;
				</td>	
    		</tr>
	    		
    		<tr>
				<td class="textoCampo">Fecha de Entrega/Prestaci&oacute;n:</td>
      			<td class="textoDato" colspan="3">
      			<@s.if test="asignacionProveedorDetalle.asignacionProveedor.fechaEntrega != null">
				<#assign fechaEntrega = asignacionProveedorDetalle.asignacionProveedor.fechaEntrega> 
				${fechaEntrega?string("dd/MM/yyyy")}
				</@s.if>	
				</td>	
			</tr>
			<tr>
				<td class="textoCampo">Lugar de Entrega/Prestaci&oacute;n:</td>
      			<td  class="textoDato"colspan="3">
      			<@s.property default="&nbsp;" escape=false value="asignacionProveedorDetalle.asignacionProveedor.lugarEntrega"/>							 
				</td>					
    		</tr>
	    		
    		<tr>
				<td class="textoCampo">Observaciones:</td>
      			<td  class="textoDato" colspan="3">
      				<@s.property default="&nbsp;" escape=false value="asignacionProveedorDetalle.asignacionProveedor.observaciones"/>
      				&nbsp;
				</td>					
    		</tr>
	    		
    		<tr>
				<td class="textoCampo">Estado:</td>
      			<td  class="textoDato" colspan="3">
					<@s.property default="&nbsp;" escape=false value="asignacionProveedorDetalle.asignacionProveedor.estado"/>
				</td>
      		</tr>
	    </table>
	   </div> 		
	
		<@s.if test="asignacionProveedorDetalle.productoBien != null">
			<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
				<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            		<tr>
						<td><b>Datos del Producto/Bien</b></td>
					</tr>
				</table>
	
				<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
					<tr>
        				<td class="textoCampo" colspan="4">&nbsp;</td>
        			</tr>
					
					<tr>
		      			<td class="textoCampo">C&oacute;digo:</td>
		      			<td class="textoDato">
		      				<@s.property default="&nbsp;" escape=false value="asignacionProveedorDetalle.productoBien.oid"/>
		      			</td>
		      			<td  class="textoCampo">Descripci&oacute;n: </td>
						<td class="textoDato">
						<@s.property default="&nbsp;" escape=false value="asignacionProveedorDetalle.productoBien.descripcion"/>		
						</td>	      			
					</tr>	
					<tr>
		      			<td class="textoCampo">Tipo:</td>
			      		<td class="textoDato">
			      			<@s.property default="&nbsp;" escape=false value="asignacionProveedorDetalle.productoBien.tipo"/>						      			
			      		</td>		      		
						<td class="textoCampo">Rubro:</td>
	      				<td class="textoDato">
	      					<@s.property default="&nbsp;" escape=false value="asignacionProveedorDetalle.productoBien.rubro.descripcion"/>
						</td>
		    		</tr>
					<tr>
						<td class="textoCampo">Marca: </td>
						<td  class="textoDato">
							<@s.property default="&nbsp;" escape=false value="asignacionProveedorDetalle.productoBien.marca"/>										
						</td>
						<td class="textoCampo">Modelo: </td>
						<td  class="textoDato">
							<@s.property default="&nbsp;" escape=false value="asignacionProveedorDetalle.productoBien.modelo"/>										
						</td>
		    		</tr>
		    		<tr>
						<td class="textoCampo">Valor Mercado: </td>
						<td  class="textoDato">
							<@s.property default="&nbsp;" escape=false value="bien.valorMercado"/>									
						</td>
						<td class="textoCampo">Fecha Valor Mercado: </td>
						<td  class="textoDato">
							<@s.property default="&nbsp;" escape=false value="bien.fechaValorMercado"/>									
						</td>
		    		</tr>
					<tr>
						<td class="textoCampo">C&oacute;digo de Barra: </td>
						<td  class="textoDato">
							<@s.property default="&nbsp;" escape=false value="asignacionProveedorDetalle.productoBien.codigoBarra"/>										
						</td>
						<td class="textoCampo">Es Cr&iacute;tico: </td>
						<td  class="textoDato">
							<@s.property default="&nbsp;" escape=false value="fsc_fssDetalle.productoBien.critico"/>						      			
						</td>
		    		</tr>
		    		<tr>
						<td class="textoCampo">Reposici&oacute;n Autm&aacute;tica: </td>
						<td  class="textoDato">	
							<@s.property default="&nbsp;" escape=false value="producto.reposicionAutomatica"/>						      											
						</td>
						<td class="textoCampo">Existencia M&iacute;nima: </td>
						<td  class="textoDato">
							<@s.property default="&nbsp;" escape=false value="producto.existenciaMinima"/>
						</td>
		    		</tr>
		    		<tr>
		    		<td class="textoCampo">Tipo Unidad:</td>
		    		<td  class="textoDato" colspan="3">
							<@s.property default="&nbsp;" escape=false value="producto.tipoUnidad.codigo"/>
						</td>
		    		</tr>
					<tr>
						<td class="textoCampo">Es Registrable: </td>
						<td  class="textoDato" colspan="3">
						<@s.property default="&nbsp;" escape=false value="bien.registrable"/>						      			
						</td>
					</tr>
					<tr>
						<td class="textoCampo">Es Veh&iacute;culo: </td>
						<td  class="textoDato" colspan="3"><@s.property default="&nbsp;" escape=false value="bien.vehiculo"/></td>
		    		</tr>							
					<tr>
						<td class="textoCampo">Estado: </td>
		      			<td class="textoDato" colspan="3"><@s.property default="&nbsp;" escape=false value="productoBien.estado"/></td>
		    		</tr>
		    		<tr>					    		
						<td class="textoCampo">Cantidad Solicitada: </td>
						<td  class="textoDato"  colspan="3">
						<@s.property default="&nbsp;" escape=false value="asignacionProveedorDetalle.cantidadSolicitadaTotal"/>					      			
						</td>
					</tr>
					<tr><td colspan="4" class="textoCampo">&nbsp;</td></tr>
			</table>
		</div>	
		</@s.if>
			
		<@s.if test="asignacionProveedorDetalle.servicio != null">
			<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
				<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            		<tr>
						<td><b>Datos del Servicio</b></td>
					</tr>
				</table>
	
				<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
					<tr>
        				<td class="textoCampo" colspan="4">&nbsp;</td>
        			</tr>	
					<tr>
		      			<td class="textoCampo">C&oacute;digo:</td>
		      			<td class="textoDato">
			      			<@s.property default="&nbsp;" escape=false value="asignacionProveedorDetalle.servicio.oid"/>			      			
		      			<td class="textoCampo" >Descripci&oacute;n</td>
						<td class="textoDato">
							<@s.property default="&nbsp;" escape=false value="asignacionProveedorDetalle.servicio.descripcion"/>			
					</tr>	
					<tr>
						<td class="textoCampo">Rubro:</td>
		      			<td class="textoDato">
		      				<@s.property default="&nbsp;" escape=false value="asignacionProveedorDetalle.servicio.rubro.descripcion"/>
					     </td>
						<td class="textoCampo">Cr&iacute;tico: </td>
						<td class="textoDato">
							<@s.property default="&nbsp;" escape=false value="asignacionProveedorDetalle.servicio.critico"/>								
						</td>
					</tr>
		
					<tr>
		      			<td class="textoCampo">Proveedor &Uacute;nico:</td>
						<td class="textoDato">
							<@s.property default="&nbsp;" escape=false value="asignacionProveedorDetalle.servicio.proveedorUnico"/>
						</td>
						<td class="textoCampo">Proveedor:</td>
						<td class="textoDato">							
							<@s.property default="&nbsp;" escape=false value="asignacionProveedorDetalle.servicio.proveedor.nombreFantasia"/>
						</td>
		    		</tr>
			    							
					<tr>			
						<td class="textoCampo">Tipo:</td>
			      		<td class="textoDato">
			     			<@s.property default="&nbsp;" escape=false value="asignacionProveedorDetalle.servicio.tipo"/>
			      		</td>				      		
						<td class="textoCampo">Estado:</td>
						<td class="textoDato" colspan="3">
							<@s.property default="&nbsp;" escape=false value="asignacionProveedorDetalle.servicio.estado"/>
						</td>
					</tr>	
					<tr><td colspan="4" class="textoCampo">&nbsp;</td></tr>		
			</table>
			</div>
		</@s.if>	

	<@vc.anchors target="contentTrx" ajaxFlag="ajax">

	<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
		<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
			<tr>
				<td>FSC/FSS</td>
				
			</tr>
		</table>

						
		<!-- Resultado Filtro -->						
		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="fsc_fssDetalleList" id="fsc_fssDetalle" defaultsort=2 decorator="ar.com.riouruguay.web.actions.compraContratacion.fsc_fss.decorators.FSC_FSSDecorator">	
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select"  class="botoneraAnchoCon1" title="Acciones">
				<a href="${request.contextPath}/compraContratacion/fsc_fss/readFSC_FSSView.action?fsc_fss.oid=${fsc_fssDetalle.fsc_fss.oid?c}&navegacionIdBack=${navigationId}&navigationId=visualizar-fsc_fss&flowControl=regis"><img  src="${request.contextPath}/common/images/ver.gif" alt="Eliminar" title="Eliminar" border="0"></a>
			</@display.column>
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="fsc_fss.numero" title="N&uacute;mero" />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="fsc_fss.tipoFormulario" title="Tipo" />
		 	<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="fsc_fss.cliente.descripcion" title="Cliente" />									
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="fsc_fss.fechaSolicitud" format="{0,date,dd/MM/yyyy}"  title="Fecha Solicitud" />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="fsc_fss.montoEstimado" title="Monto Estimado" />			
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="fsc_fss.estado" title="Estado" />
			<#if asignacionProveedorDetalle.asignacionProveedor.tipo.ordinal()==1>		
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="cantidadSolicitada" title="Cant. Solicitada" />
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="cantidadRepuesta" title="Cant. Repuesta" />
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="cantidadSolicitadaPendiente" title="Cant. Solicitada Pendiente" />
			<#else>
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="cantidadSolicitada" title="Cantidad Solicitada" />	
			</#if>
		</@display.table>
	</div>	
</@vc.anchors>


<@tiles.insertAttribute name="proveedor"/>



