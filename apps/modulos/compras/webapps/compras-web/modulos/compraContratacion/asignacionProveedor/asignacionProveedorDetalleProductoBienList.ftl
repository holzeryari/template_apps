<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>

<#if asignacionProveedor.asignacionProveedorDetalleList.size()!=0>

	<@vc.anchors target="contentTrx" ajaxFlag="ajax">
	<div id="capaSeparadora" class="capaSeparadora">&nbsp;</div>
	<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
		<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
			<tr>
				<td>Productos/Bienes solicitados en los FSC/FSS</td>
			</tr>
		</table>

		<table class="tablaDetalleCuerpo" cellpadding="3" >
			<#assign asignacionProveedorDetalleLista = asignacionProveedor.asignacionProveedorDetalleList>
			<#list asignacionProveedorDetalleLista as asignacionProveedorDetalle>
			<tr>
				<th align="center">Acciones</th>
				<th align="center" width="1">N&uacute;mero</th>
				<th align="center" class="botoneraAnchoCon4" width="1">C&oacute;digo</th>
				<th align="center" >Descripci&oacute;n</th>
				<th align="center">Marca</th>
				<th align="center">Modelo</th>
				<th align="center">Rubro</th>
				<th align="center">Cantidad Solicitada</th>
				<th align="center">Existencia</th>
			</tr>
						
			<tr>
				
				<td colsapn="1" class="botoneraAnchoCon4"> 
					<div class="alineacion">
						<@s.a templateDir="custontemplates" id="verAsignacionProveedorDetalle" name="verAsignacionProveedorDetalle" href="${request.contextPath}/compraContratacion/asignacionProveedor/readAsignacionProveedorDetalleView.action?asignacionProveedorDetalle.oid=${asignacionProveedorDetalle.oid?c}&navegacionIdBack=${navigationId}&navigationId=asignacionProveedorDetalle-visualizar&flowControl=regis" cssClass="ocultarIcono">
							<img src="${request.contextPath}/common/images/ver.gif"  border="0" align="absmiddle" hspace="3">
						</@s.a>
					</div>
					<div class="alineacion"
						<#if !asignacionProveedor.desahibilitarBotonesAsignacion>
							<@s.a templateDir="custontemplates" id="eliminarAsignacionProveedorDetalle" name="eliminarAsignacionProveedorDetalle" href="${request.contextPath}/compraContratacion/asignacionProveedor/deleteAsignacionProveedorDetalleView.action?asignacionProveedorDetalle.oid=${asignacionProveedorDetalle.oid?c}&navegacionIdBack=${navigationId}&navigationId=asignacionProveedorDetalle-eliminar&flowControl=regis" cssClass="ocultarIcono">
								<img src="${request.contextPath}/common/images/eliminar.gif" border="0" align="absmiddle" hspace="3" >
							</@s.a>
						</#if>
					</div>
					<#if asignacionProveedorDetalle.archivo?exists>	
						<a href="javascript://nop/" class="item" >
							<img src="${request.contextPath}/common/images/bajar.png" alt="Bajar Archivo" title="Bajar Archivo" border="0" 
								onclick="window.open('${request.contextPath}/compraContratacion/asignacionProveedor/asignacionProveedorRedirectArchivoView.action?asignacionProveedorDetalle.oid=${asignacionProveedorDetalle.oid?c}', '_blank','width=400,height=400,resizable=yes,scrollbars=yes,left=100,top=80,toolbar=no,menubar=no,location=no'); return false;">
						</a>											
					</#if>						
					<a href="javascript://nop/" class="item">
						<img src="${request.contextPath}/common/images/imagenUP.gif" alt="Adjuntar Archivo" title="Adjuntar Archivo" border="0" 
							onclick="window.open('${request.contextPath}/archivos/uploadArchivoView.action?asignacionProveedorDetalle.oid=${asignacionProveedorDetalle.oid?c}&navegacionIdBack=administrar', '_blank','width=600,height=400,resizable=yes,scrollbars=yes,left=100,top=80,toolbar=no,menubar=no,location=no'); return false;">
					</a>
				</td>
				<td class="estiloNumero">${asignacionProveedorDetalle.numero}	&nbsp;	</td>
				<td class="estiloNumero">${asignacionProveedorDetalle.productoBien.oid}	&nbsp;	</td>
				<td class="estiloTexto">${asignacionProveedorDetalle.productoBien.descripcion}&nbsp;</td>
				<td class="estiloTexto">${asignacionProveedorDetalle.productoBien.marca}&nbsp;</td>
				<td class="estiloTexto">${asignacionProveedorDetalle.productoBien.modelo}&nbsp;</td>
				<td class="estiloTexto">${asignacionProveedorDetalle.productoBien.rubro.descripcion}&nbsp;</td>
				<td class="estiloNumero">${asignacionProveedorDetalle.cantidadSolicitadaTotal}&nbsp;</td>
				<td class="estiloNumero">
				<#if asignacionProveedorDetalle.producto?exists>
					${asignacionProveedorDetalle.producto.cantidadTotalExistencia}
				<#else>
					&nbsp;
				</#if>
				</td>			   
			</tr>
						
			<tr>
				<td colspan="1">&nbsp;</td>
				<th colspan="1" nowrap>
					<div align="center">
						<b>Proveedor</b>
						<@s.a templateDir="custontemplates" id="agregarProveedor" name="agregarProveedor" href="${request.contextPath}/compraContratacion/asignacionProveedor/selectProveedor.action?navegacionIdBack=${navigationId}&asignacionProveedorDetalle.oid=${asignacionProveedorDetalle.oid?c}" cssClass="ocultarIcono">
							<img src="${request.contextPath}/common/images/agregar.gif"  border="0" align="absmiddle" hspace="3" title="Agregar Proveedor">
						</@s.a>
 					</div>
				</th>	
				<th align="center">Condici&oacute;n</th>									 	
				<th align="center">Nombre Fantas&iacute;a</th>
				<th align="center">Raz&oacute;n Social</th>
				<th align="center">Nombre</th>
				<th align="center">CUIT/CUIL</th>
				<th align="center">ICP</th>
				<th align="center">Situaci&oacute;n</th>									
			</tr>
						
			<#assign proveedorLista = asignacionProveedorDetalle.proveedorList>
			<#list proveedorLista as proveedor>
												
				<tr>
					<td colspan="1">&nbsp;</td>
					<td colspan="1" class="botoneraAnchoCon2">
						<div class="alineacion">								
							<@s.a templateDir="custontemplates" id="verProveedor" name="verProveedor"							
							href="${request.contextPath}/maestro/proveedor/readView.action?proveedor.nroProveedor=${proveedor.nroProveedor?c}&navigationId=read-proveedor&navegacionIdBack=${navigationId}&flowControl=regis">
								<img src="${request.contextPath}/common/images/ver.gif" border="0" align="absmiddle" hspace="3">
							</@s.a>
						</div>
						<div class="alineacion">
							<#if !asignacionProveedor.desahibilitarBotonesAsignacion>	
								<@s.a templateDir="custontemplates" id="eliminarProveedor" name="eliminarProveedor" href="${request.contextPath}/compraContratacion/asignacionProveedor/deleteProveedorAsignado.action?proveedor.nroProveedor=${proveedor.nroProveedor?c}&asignacionProveedorDetalle.oid=${asignacionProveedorDetalle.oid?c}&navegacionIdBack=${navigationId}&navigationId=proveedor-visualizar&flowControl=regis" cssClass="ocultarIcono">
									<img src="${request.contextPath}/common/images/eliminar.gif"  border="0" align="absmiddle" hspace="3" >
								</@s.a>	
							</#if>	
						</div>						
					</td>
					<td colspan="1" class="botoneraAnchoCon4">	
					<div class="alineacion" >				
						<#if proveedor.esCooperativa?exists && proveedor.esCooperativa.ordinal()==2>
						<img src="${request.contextPath}/common/images/pino.gif"  border="0" align="absmiddle" alt="Es cooperativa" title="Es cooperativa" >								
						<#else>
						<div class="item">
						<img src="${request.contextPath}/common/images/pino.gif" border="0" align="absmiddle" alt="No es cooperativa" title="No es cooperativa">
						</div>
						</#if>
					</div>
						
					<div class="alineacion">							
						<#if proveedor.adtividadRegion?exists && proveedor.adtividadRegion.ordinal()==2>
						<img src="${request.contextPath}/common/images/sailboat.gif"  border="0" align="absmiddle" alt="Tiene actividad en la region" title="Tiene actividad en la region" >								
						<#else>
						<div class="item">
						<img src="${request.contextPath}/common/images/sailboat.gif" border="0" align="absmiddle" alt="No tiene actividad en la region" title="No tiene actividad en la region">
						</div>
						</#if>	
					</div>
					<div class="alineacion">
						<#if proveedor.detalleDePersona.socio?exists>
						<img src="${request.contextPath}/common/images/manosAgarradas.gif" border="0" align="absmiddle" alt="Es socio" title="Es socio"  >								
						<#else>
						<div class="item">
						<img src="${request.contextPath}/common/images/manosAgarradas.gif"  border="0" align="absmiddle"  alt="No es socio" title="No es socio" >
						</div>
						</#if>
					</div>
					<div classs="alineacion">
						<#if proveedor.solvenciaEconomica?exists && proveedor.solvenciaEconomica.ordinal()==1>
							<img src="${request.contextPath}/common/images/green-dollar-icon-16_1.png"  border="0" align="absmiddle" alt="Solvencia Econ&oacute;mica '1-Normal'" title="Solvencia Econ&oacute;mica '1-Normal'" >
						</#if>
						<#if proveedor.solvenciaEconomica?exists && proveedor.solvenciaEconomica.ordinal()==2>
							<img src="${request.contextPath}/common/images/green-dollar-icon-16_2.png"  border="0" align="absmiddle" alt="Solvencia Econ&oacute;mica '1-Normal'" title="Solvencia Econ&oacute;mica '2-Con seguimiento especial'" >
						</#if>	
						<#if proveedor.solvenciaEconomica?exists && proveedor.solvenciaEconomica.ordinal()==3>
							<img src="${request.contextPath}/common/images/green-dollar-icon-16_3.png"  border="0" align="absmiddle" alt="Solvencia Econ&oacute;mica '1-Normal'" title="Solvencia Econ&oacute;mica '3-Con problemas'" >
						</#if>	
						<#if proveedor.solvenciaEconomica?exists && proveedor.solvenciaEconomica.ordinal()==4>
							<img src="${request.contextPath}/common/images/green-dollar-icon-16_4.png"  border="0" align="absmiddle" alt="Solvencia Econ&oacute;mica '1-Normal'" title="Solvencia Econ&oacute;mica '4-Con alto riesgo de insolvencia'" >
						</#if>
						<#if proveedor.solvenciaEconomica?exists && proveedor.solvenciaEconomica.ordinal()==5>
							<img src="${request.contextPath}/common/images/green-dollar-icon-16_5.png"  border="0" align="absmiddle" alt="Solvencia Econ&oacute;mica '1-Normal'" title="Solvencia Econ&oacute;mica '5-Irrecuperable'" >
						</#if>	
						<#if proveedor.solvenciaEconomica?exists && proveedor.solvenciaEconomica.ordinal()==6>
							<img src="${request.contextPath}/common/images/green-dollar-icon-16_6.png"  border="0" align="absmiddle" alt="Solvencia Econ&oacute;mica '1-Normal'" title="Solvencia Econ&oacute;mica '6-Irrecuperable con disposici&oacute;n t&eacute;cnica'" >
						</#if>			
					</div>	
					</td>
					<td colspan="1" class="estiloTexto">
						<#if proveedor.nombreFantasia?exists>
							${proveedor.nombreFantasia} 
						</#if>&nbsp;
					</td>
					<td colspan="1" class="estiloTexto">
						<#if proveedor.detalleDePersona.razonSocial?exists>
							${proveedor.detalleDePersona.razonSocial}
						</#if>&nbsp;
					</td>
					<td colspan="1" class="estiloTexto">
						<#if proveedor.detalleDePersona.nombre?exists>
							${proveedor.detalleDePersona.nombre}
						</#if>&nbsp;
					</td>
					<td colspan="1" class="estiloNumero">
						<#if proveedor.detalleDePersona.persona.docFiscal?exists>
    						${proveedor.detalleDePersona.persona.docFiscal?c}
    					</#if>&nbsp;
					</td>
					<td colspan="1" class="estiloNumero">
						<#if proveedor.ultimoICP?exists>
    						${proveedor.ultimoICP}
    					</#if>&nbsp;
					</td>	
					<td colspan="1" class="estiloNumero">
						<#if proveedor.situacionICP?exists>
    						${proveedor.situacionICP}
    					</#if>&nbsp;
					</td>	
				</tr>	
								
				</#list>
						<tr><td colspan="10">&nbsp;</td></tr>
			</#list>						
	</table>	
	</div>

</@vc.anchors>
</#if>

