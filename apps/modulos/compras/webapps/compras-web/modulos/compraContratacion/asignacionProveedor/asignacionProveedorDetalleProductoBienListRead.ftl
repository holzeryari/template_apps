<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>

<#if asignacionProveedor.asignacionProveedorDetalleList.size()!=0>
	<div id="capaSeparadora" class="capaSeparadora">&nbsp;</div>
	<@vc.anchors target="contentTrx" ajaxFlag="ajax">
	<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
		<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
			<tr>
				<td>Productos/Bienes solicitados en los FSC/FSS</td>
			</tr>
		</table>
	
		<table class="tablaDetalleCuerpo" cellpadding="3" >
			<#assign asignacionProveedorDetalleLista = asignacionProveedor.asignacionProveedorDetalleList>
			<#list asignacionProveedorDetalleLista as asignacionProveedorDetalle>
				<tr>
					<th colspan="1">Acciones</th>
					<th colspan="1" align="center">N&uacute;mero</th>
					<th colspan="1" align="center">C&oacute;digo</th>
					<th colspan="1" align="center">Descripci&oacute;n</th>
					<th colspan="1" align="center">Marca</th>
					<th colspan="1" align="center">Modelo</th>
					<th colspan="1" align="center">Rubro</th>
					<th colspan="1" align="center">Cantidad Solicitada</th>
					<th colspan="1" align="center">Existencia</th>
				</tr>
								
				<tr>
					<td class="botoneraAnchoCon1"> 
						<div align="center">
						<@s.a templateDir="custontemplates" id="verAsignacionProveedorDetalle" name="verAsignacionProveedorDetalle" href="${request.contextPath}/compraContratacion/asignacionProveedor/readAsignacionProveedorDetalleView.action?asignacionProveedorDetalle.oid=${asignacionProveedorDetalle.oid?c}&navegacionIdBack=${navigationId}&navigationId=asignacionProveedorDetalle-visualizar&flowControl=regis" cssClass="ocultarIcono">
							<img src="${request.contextPath}/common/images/ver.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
						</@s.a>
					
						</div>
					</td>
					<td class="estiloNumero">${asignacionProveedorDetalle.numero}</td>
					<td class="estiloNumero">${asignacionProveedorDetalle.productoBien.oid}</td>
					<td class="estiloTexto">${asignacionProveedorDetalle.productoBien.descripcion}</td>
					<td class="estiloTexto">${asignacionProveedorDetalle.productoBien.marca}</td>
					<td class="estiloTexto">${asignacionProveedorDetalle.productoBien.modelo}</td>
					<td class="estiloTexto">${asignacionProveedorDetalle.productoBien.rubro.descripcion}</td>
					<td class="estiloNumero">${asignacionProveedorDetalle.cantidadSolicitadaTotal}</td>	
					<td class="estiloNumero">
					<#if asignacionProveedorDetalle.producto?exists>
						${asignacionProveedorDetalle.producto.cantidadTotalExistencia}
					<#else>
						-
					</#if>
					</td>			   
				</tr>
								
				<tr>
					<td colspan="1">&nbsp;</td>
					<th colspan="1" nowrap align="center">
							<b>Proveedor</b>
					</th>	
					<th colspan="1" align="center">Condici&oacute;n</th>									 	
					<th colspan="2" align="center">Nombre Fantas&iacute;a</th>
					<th colspan="1" align="center">Raz&oacute;n Social</th>
					<th colspan="2" align="center">Nombre</th>
					<th colspan="1" align="center">CUIT/CUIL</th>
				</tr>
				
				<#assign proveedorLista = asignacionProveedorDetalle.proveedorList>
				<#list proveedorLista as proveedor>
										
				<tr>
					<td colspan="1">&nbsp;</td>
					<td colspan="1" align="center" >								
						<@s.a templateDir="custontemplates" id="verProveedor" name="verProveedor"
						href="${request.contextPath}/maestro/proveedor/readView.action?proveedor.nroProveedor=${proveedor.nroProveedor?c}&navigationId=read-proveedor&navegacionIdBack=${navigationId}&flowControl=regis">
							<img src="${request.contextPath}/common/images/ver.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
						</@s.a>						
					</td>
					<td colspan="1" class="botoneraAnchoCon3">	
					<div class="alineacionMenor" >				
						<#if proveedor.esCooperativa?exists && proveedor.esCooperativa.ordinal()==2>
						<img src="${request.contextPath}/common/images/pino.gif"  title="Es Cooperativa" border="0" align="absmiddle" >								
						<#else>
						<div class="item">
						<img src="${request.contextPath}/common/images/pino.gif"  title="NO es Cooperativa" border="0" align="absmiddle" >
						</div>
						</#if>
					</div>
						
					<div class="alineacionMenor">							
						<#if proveedor.adtividadRegion?exists && proveedor.adtividadRegion.ordinal()==2>
						<img src="${request.contextPath}/common/images/sailboat.gif"  title="Actividad en Region" border="0" align="absmiddle" >								
						<#else>
						<div class="item">
						<img src="${request.contextPath}/common/images/sailboat.gif" title="NO tiene Actividad en Region" border="0" align="absmiddle" >
						</div>
						</#if>	
					</div>
					<div class="alineacionMenor">
						<#if proveedor.detalleDePersona.socio?exists>
						<img src="${request.contextPath}/common/images/manosAgarradas.gif"  title="Es Socio" border="0" align="absmiddle" >								
						<#else>
						<div class="item">
						<img src="${request.contextPath}/common/images/manosAgarradas.gif" title="NO es Socio"  border="0" align="absmiddle" >
						</div>
						</#if>
					</div>
						
					</td>
					<td colspan="2" class="estiloTexto">
						<#if proveedor.nombreFantasia?exists>
							${proveedor.nombreFantasia}
						</#if>&nbsp;
					</td>
					<td colspan="1" class="estiloTexto">
						<#if proveedor.detalleDePersona.razonSocial?exists>
							${proveedor.detalleDePersona.razonSocial}
						</#if>&nbsp;
					</td>
					<td colspan="2" class="estiloTexto">
						<#if proveedor.detalleDePersona.nombre?exists>
							${proveedor.detalleDePersona.nombre}
						</#if>&nbsp;
					</td>
					<td colspan="1" class="estiloNumero">
						<#if proveedor.detalleDePersona.persona.docFiscal?exists>
    						${proveedor.detalleDePersona.persona.docFiscal?c}
    					</#if>&nbsp;
					</td>
				
				</tr>	
												
			</#list>
			<tr><td colspan="9">&nbsp;</td></tr>
		</#list>						
	</table>	
	</div>
	</@vc.anchors>

</#if>

