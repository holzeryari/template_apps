
<#setting locale="es_AR">

<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign tiles=JspTaglibs["/WEB-INF/tlds/tiles-jsp.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>

<@s.hidden id="navigationId" name="navigationId"/>
<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
        	<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
			<tr>
				<td><b>Datos de la Asignaci&oacute;n</b></td>
				<td>
					<div align="right">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0341" 
							enabled="true" 
							cssClass="item" 
							id="crear"										
							href="javascript://nop/">
							<b>Agregar</b>										
							<img src="${request.contextPath}/common/images/agregar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
						</@security.a>				
					</div>
				</td>
			</tr>
		</table>

		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
	    		<td class="textoCampo" colspan="4">&nbsp;</td>
	    	</tr>
	    	<tr>
				<td class="textoCampo">Producto/Bien:</td>
				<td class="textoDato">
				<@s.hidden id="productoBien.oid" name="productoBien.oid"/>
				<@s.hidden id="productoBien.descripcion" name="productoBien.descripcion"/>
				<@s.property default="&nbsp;" escape=false value="productoBien.descripcion"/>					
					<@s.a href="javascript://nop/"
						templateDir="custontemplates" id="seleccionarProductoBien" name="seleccionarProductoBien" cssClass="ocultarIcono">
						<img src="${request.contextPath}/common/images/buscar.gif" border="0" align="absmiddle" hspace="3" >
					</@s.a>	
				</td>
				<td class="textoCampo" align="right">&nbsp;</td>
				<td class="textoDato" align="left">&nbsp;</td>
			</tr>
			<tr>
				<td class="textoCampo">N&uacute;mero:</td>
	      		<td class="textoDato">
	      			<@s.textfield 
      					templateDir="custontemplates" 
						id="asignacionProveedor.numero" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="asignacionProveedor.numero" 
						title="N&uacute;mero" />
					</td>							
	      		
					<td class="textoCampo">Tipo:</td>
	      			<td class="textoDato">
	      			<@s.select 
						templateDir="custontemplates" 
						id="asignacionProveedor.tipo" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="asignacionProveedor.tipo" 
						list="tipoList" 
						listKey="key" 
						listValue="description" 
						value="asignacionProveedor.tipo.ordinal()"
						title="Tipo"
						headerKey="0"
						headerValue="Todos"							
						/>
					</td>
			</tr>
			<tr>					      					
				<td class="textoCampo">Estado:</td>
      			<td class="textoDato" colspan="3">
      			<@s.select 
							templateDir="custontemplates" 
							id="asignacionProveedor.estado" 
							cssClass="textarea"
							cssStyle="width:165px" 
							name="asignacionProveedor.estado" 
							list="estadoList" 
							listKey="key" 
							listValue="description" 
							value="asignacionProveedor.estado.ordinal()"
							title="Estado"
							headerKey="0"
							headerValue="Todos"							
							/>
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Fecha Asignaci&oacute;n Desde:</td>
      			<td class="textoDato">
					<@vc.rowCalendar 
					templateDir="custontemplates" 
					id="fechaAsignacionDesde" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="fechaAsignacionDesde" 
					title="Fecha Asignaci&oacute;n Desde" />
				</td>
				<td class="textoCampo">Fecha Asignaci&oacute;n Hasta:</td>
      			<td class="textoDato">
				<@vc.rowCalendar 
					templateDir="custontemplates" 
					id="fechaAsignacionHasta" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="fechaAsignacionHasta" 
					title="Fecha Asignaci&oacute;n Hasta" />
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Fecha L&iacute;mite Recepci&oacute;n Cotizaciones Desde:</td>
      			<td class="textoDato">
					<@vc.rowCalendar 
					templateDir="custontemplates" 
					id="fechaLimiteCotizacionDesde" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="fechaLimiteCotizacionDesde" 
					title="Fecha L&iacute;mite Recepci&oacute;n Cotizaciones Desde" />
				</td>
				<td class="textoCampo">Fecha L&iacute;mite Recepci&oacute;n Cotizaciones Hasta:</td>
	      		<td class="textoDato">
					<@vc.rowCalendar 
						templateDir="custontemplates" 
						id="fechaLimiteCotizacionHasta" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="fechaLimiteCotizacionHasta" 
						title="Fecha L&iacute;mite Recepci&oacute;n Cotizaciones Hasta" />
					</td>
			</tr>
				<tr>
	    			<td colspan="4" class="lineaGris" align="right">
	      			<input id="limpiar" type="button" name="btnVolver" value="Limpiar" class="boton"/>		
	      			<input id="buscar" type="button" name="btnVolver" value="Buscar" class="boton"/>
	    			</td>
				</tr>	
			</table>
		</div>	
			
		<!-- Resultado Filtro -->
		<@s.if test="asignacionProveedorList!=null">

								
			<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
				<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>Asignaciones de Proveedores encontrados</td>
					
				</tr>
			</table>	

			<@vc.anchors target="contentTrx">			
          		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="asignacionProveedorList" id="asignacionProveedor" pagesize=15 partialList=true size="recordSize" keepStatus=true excludedParams="resetFlag">
          		
  		        <#assign classBotonera = "botoneraAnchoCon4" />
  		        <#assign classTexto = "estiloTexto" />
          		<#assign classNumero = "estiloNumero" />
          		
          		<#if asignacionProveedor.enable>
	          		<#assign classBotonera = "botoneraAnchoCon4Color" />
	          		<#assign classTexto = "estiloTextoColor" />
	          		<#assign classNumero = "estiloNumeroColor" />	          	
          		</#if>
  		
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select" class="${classBotonera}" title="Acciones">
						<div class="alineacion">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0343" 
							enabled="asignacionProveedor.readable" 
							cssClass="item" 
							href="${request.contextPath}/compraContratacion/asignacionProveedor/readAsignacionProveedorView.action?asignacionProveedor.oid=${asignacionProveedor.oid?c}&navigationId=visualizar-asignacionProveedor&flowControl=regis">
							<img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver"  border="0">
						</@security.a>
						</div>
						
						<div class="alineacion">				
								<@security.a 
									templateDir="custontemplates" 
									securityCode="CUF0344" 
									enabled="asignacionProveedor.updatable"
									cssClass="item"  
									href="${request.contextPath}/compraContratacion/asignacionProveedor/administrarAsignacionProveedorView.action?asignacionProveedor.oid=${asignacionProveedor.oid?c}&navigationId=administrar-asignacionProveedor&flowControl=regis">
									<img  src="${request.contextPath}/common/images/administrar.gif" alt="Administrar" title="Administrar"  border="0">
								</@security.a>							
						</div>
						<div class="alineacion">		
						<@security.a 
								templateDir="custontemplates" 
								securityCode="CUF0345" 
								enabled="asignacionProveedor.eraseable"
								cssClass="item"  
								href="${request.contextPath}/compraContratacion/asignacionProveedor/deleteAsignacionProveedorView.action?asignacionProveedor.oid=${asignacionProveedor.oid?c}&navigationId=delete-asignacionProveedor&flowControl=regis">
								<img  src="${request.contextPath}/common/images/eliminar.gif" alt="Eliminar" title="Eliminar"  border="0">
						</@security.a>								
					</div>
					<div class="alineacion">						
							<@security.a 
								templateDir="custontemplates" 
								securityCode="CUF0346" 
								enabled="asignacionProveedor.enable"
								cssClass="item"  
								href="${request.contextPath}/compraContratacion/asignacionProveedor/generarOCOSMasivaView.action?asignacionProveedor.oid=${asignacionProveedor.oid?c}&navigationId=generarOCOSMasiva-asignacionProveedor&flowControl=regis">
								<img  src="${request.contextPath}/common/images/generarOCOS.gif" alt="Generar OC/OS Masiva" title="Generar OC/OS"  border="0">
							</@security.a>	
										
						</div>
						

					</@display.column>
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="${classNumero}" property="numero" title="N&uacute;mero" />
				
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="${classTexto}" property="tipo" title="Tipo" />
			 			
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="${classNumero}" property="fechaAsignacion" format="{0,date,dd/MM/yyyy}"  title="Fecha Asignacion" />
			
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="${classNumero}" property="fechaLimiteCotizacion" format="{0,date,dd/MM/yyyy}"  title="Fecha L&iacute;mite Recepci&oacute;n Cotizaciones" />			
				
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="${classTexto}" property="estado" title="Estado" />		

				</@display.table>
			</@vc.anchors>
		</div>	
	</@s.if>
	  

<@vc.htmlContent 
  baseUrl="${request.contextPath}/compraContratacion/asignacionProveedor/search.action"
  source="buscar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="resetFlag=true,navigationId={navigationId},flowControl=regis,asignacionProveedor.estado={asignacionProveedor.estado},asignacionProveedor.numero={asignacionProveedor.numero},fechaAsignacionDesde={fechaAsignacionDesde},fechaAsignacionHasta={fechaAsignacionHasta},asignacionProveedor.tipo={asignacionProveedor.tipo},productoBien.oid={productoBien.oid},productoBien.descripcion={productoBien.descripcion},fechaLimiteCotizacionDesde={fechaLimiteCotizacionDesde},fechaLimiteCotizacionHasta={fechaLimiteCotizacionHasta}"/>
  		      

<@vc.htmlContent 
  baseUrl="${request.contextPath}/compraContratacion/asignacionProveedor/view.action"
  source="limpiar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=regis"/>
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/compraContratacion/asignacionProveedor/createAsignacionProveedorView.action" 
  source="crear" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=asignacionProveedor-create,flowControl=regis"/>
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/compraContratacion/asignacionProveedor/selectProductoBienSearch.action" 
  source="seleccionarProductoBien" 
  success="contentTrx" 
  failure="errorTrx"
  parameters="navigationId={navigationId},flowControl=change,asignacionProveedor.estado={asignacionProveedor.estado},asignacionProveedor.numero={asignacionProveedor.numero},fechaAsignacionDesde={fechaAsignacionDesde},fechaAsignacionHasta={fechaAsignacionHasta},asignacionProveedor.tipo={asignacionProveedor.tipo},productoBien.oid={productoBien.oid},productoBien.descripcion={productoBien.descripcion},fechaLimiteCotizacionDesde={fechaLimiteCotizacionDesde},fechaLimiteCotizacionHasta={fechaLimiteCotizacionHasta}"/>
