
<#setting locale="es_AR">

<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign tiles=JspTaglibs["/WEB-INF/tlds/tiles-jsp.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>

<@s.hidden id="navigationId" name="navigationId"/>
<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
			<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos de la Asignaci&oacute;n de Proveedor</b></td>
			</tr>
		</table>

		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>		

			<tr>
				<td class="textoCampo">N&uacute;mero:</td>
      			<td class="textoDato">
      			<@s.textfield 
      					templateDir="custontemplates" 
						id="asignacionProveedor.numero" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="asignacionProveedor.numero" 
						title="N&uacute;mero" />
				</td>							
	      		
				<td class="textoCampo">Tipo:</td>
      			<td class="textoDato">
      			<@s.select 
							templateDir="custontemplates" 
							id="asignacionProveedor.tipo" 
							cssClass="textarea"
							cssStyle="width:165px" 
							name="asignacionProveedor.tipo" 
							list="tipoList" 
							listKey="key" 
							listValue="description" 
							value="asignacionProveedor.tipo.ordinal()"
							title="Tipo"
							headerKey="0"
							headerValue="Todos"							
							/>
				</td>
			</tr>
			<tr>					      					
				<td class="textoCampo">Estado:</td>
      			<td class="textoDato" colspan="3">
      			<@s.select 
							templateDir="custontemplates" 
							id="asignacionProveedor.estado" 
							cssClass="textarea"
							cssStyle="width:165px" 
							name="asignacionProveedor.estado" 
							list="estadoList" 
							listKey="key" 
							listValue="description" 
							value="asignacionProveedor.estado.ordinal()"
							title="Estado"
							headerKey="0"
							headerValue="Todos"							
							/>
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Fecha Asignaci&oacute;n Desde:</td>
      			<td class="textoDato">
					<@vc.rowCalendar 
					templateDir="custontemplates" 
					id="fechaAsignacionDesde" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="fechaAsignacionDesde" 
					title="Fecha Asignaci&oacute;n Desde" />
				</td>
				<td class="textoCampo">Fecha Asignaci&oacute;n Hasta:</td>
      			<td class="textoDato">
				<@vc.rowCalendar 
					templateDir="custontemplates" 
					id="fechaAsignacionHasta" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="fechaAsignacionHasta" 
					title="Fecha Asignaci&oacute;n Hasta" />
				</td>
				
			</tr>
			<tr>
				<td class="textoCampo">Fecha L&iacute;mite Recepci&oacute;n Cotizaciones Desde:</td>
      			<td class="textoDato">
					<@vc.rowCalendar 
					templateDir="custontemplates" 
					id="fechaLimiteCotizacionDesde" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="fechaLimiteCotizacionDesde" 
					title="Fecha L&iacute;mite Recepci&oacute;n Cotizaciones Desde" />
				</td>
				<td class="textoCampo">Fecha L&iacute;mite Recepci&oacute;n Cotizaciones Hasta:</td>
      			<td class="textoDato">
				<@vc.rowCalendar 
					templateDir="custontemplates" 
					id="fechaLimiteCotizacionHasta" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="fechaLimiteCotizacionHasta" 
					title="Fecha L&iacute;mite Recepci&oacute;n Cotizaciones Hasta" />
				</td>
			</tr>
				<tr>
	    			<td colspan="4" class="lineaGris" align="right">
	      			<input id="limpiar" type="button" name="btnVolver" value="Limpiar" class="boton"/>		
	      			<input id="buscar" type="button" name="btnVolver" value="Buscar" class="boton"/>
	    			</td>
				</tr>	
			</table>
		</div>	
			
		<!-- Resultado Filtro -->
		<@s.if test="asignacionProveedorList!=null">
			<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
				<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
					<tr>
						<td  width="89%"><b>Asignaciones de Proveedores encontrados</b></td>
						</tr>
				</table>


				<@vc.anchors target="contentTrx">			
          		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="asignacionProveedorList" id="asignacionProveedorSelect" pagesize=15 partialList=true size="recordSize" keepStatus=true excludedParams="resetFlag">
          		          		
  					<#if asignacionProveedor.numero?exists>
  					
						<#assign ref="${request.contextPath}/compraContratacion/asignacionProveedor/selectAsignacionProveedor.action?">										
						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select" style=" text-align:center; display:block" title="Acciones">
							<@s.a href="${ref}asignacionProveedor.oid=${asignacionProveedorSelect.oid?c}&asignacionProveedor.numero=${asignacionProveedorSelect.numero?c}&navegacionIdBack=${navegacionIdBack}&navigationId=${navigationId}"><img src="${request.contextPath}/common/images/seleccionar.gif" alt="Seleccionar" title="Seleccionar" width="16" height="16" border="0"></@s.a>
							&nbsp;
						</@display.column>
					
					<#else>
					  		
						<#assign ref="${request.contextPath}/compraContratacion/asignacionProveedor/selectAsignacionProveedor.action?">										
						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select" style=" text-align:center; display:block" title="Acciones">
							<@s.a href="${ref}asignacionProveedor.oid=${asignacionProveedorSelect.oid?c}&navegacionIdBack=${navegacionIdBack}&navigationId=${navigationId}"><img src="${request.contextPath}/common/images/seleccionar.gif" alt="Seleccionar" title="Seleccionar" width="16" height="16" border="0"></@s.a>
							&nbsp;
						</@display.column>
					
					</#if>
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="numero" title="N&uacute;mero" />
				
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="tipo" title="Tipo" />
			 			
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="fechaAsignacion" format="{0,date,dd/MM/yyyy}"  title="Fecha Asignacion" />
			
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="fechaLimiteCotizacion" format="{0,date,dd/MM/yyyy}"  title="Fecha L&iacute;mite Recepci&oacute;n Cotizaciones" />			
				
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="estado" title="Estado" />		

				</@display.table>
			</@vc.anchors>
			</div>
		</@s.if>
		
			
	<div id="capaBotonera" class="capaBotonera">
		<table id="tablaBotonera" class="tablaBotonera">
			<tr> 
				<td align="left">
					<input id="cancel" type="button" name="btnVolver" value="Volver" class="boton"/>
				</td>
			</tr>	
		</table>
	</div>
			

<@vc.htmlContent 
  baseUrl="${request.contextPath}/compraContratacion/asignacionProveedor/selectAsignacionProveedorSearch.action"
  source="buscar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="resetFlag=true,navigationId={navigationId},flowControl=regis,asignacionProveedor.estado={asignacionProveedor.estado},asignacionProveedor.numero={asignacionProveedor.numero},fechaAsignacionDesde={fechaAsignacionDesde},fechaAsignacionHasta={fechaAsignacionHasta},asignacionProveedor.tipo={asignacionProveedor.tipo},fechaLimiteCotizacionDesde={fechaLimiteCotizacionDesde},fechaLimiteCotizacionHasta={fechaLimiteCotizacionHasta},navegacionIdBack=${navegacionIdBack}"/>
  		      

<@vc.htmlContent 
  baseUrl="${request.contextPath}/compraContratacion/asignacionProveedor/selectAsignacionProveedorView.action"
  source="limpiar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=regis,navegacionIdBack=${navegacionIdBack}"/>
  
  
 <@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId=${navegacionIdBack},flowControl=back"/>
 