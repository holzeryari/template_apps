<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign tiles=JspTaglibs["/WEB-INF/tlds/tiles-jsp.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<@s.hidden id="asignacionProveedor.oid" name="asignacionProveedor.oid"/>
<@s.hidden id="fsc_fss.oid" name="fsc_fss.oid"/>
<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
        	<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
			<tr>
				<td><b>Datos de la Asignaci&oacute;n</b></td>
			</tr>
		</table>
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
	    		<td class="textoCampo" colspan="4">&nbsp;</td>
	    	</tr>	
			<tr>
      			<td class="textoCampo">N&uacute;mero:</td>
      			<td class="textoDato">
      				<@s.property default="&nbsp;" escape=false value="asignacionProveedor.numero"/></td>
      			<td  class="textoCampo">Fecha Asignaci&oacute;n: </td>
				<td class="textoDato">
				<@s.if test="asignacionProveedor.fechaAsignacion != null">
				<#assign fechaAsignacion = asignacionProveedor.fechaAsignacion> 
				${fechaAsignacion?string("dd/MM/yyyy")}	
				</@s.if>		
				&nbsp;							
				</td>	      			
			</tr>	
			<tr>
				<td class="textoCampo">Tipo:</td>
      			<td class="textoDato">
      			<@s.property default="&nbsp;" escape=false value="asignacionProveedor.tipo"/>
      			</td>
				<td class="textoCampo">Fecha L&iacute;mite Recepci&oacute;n de Cotizaciones:</td>
      			<td class="textoDato">	
      			<@s.if test="asignacionProveedor.fechaLimiteCotizacion != null">
				<#assign fechaLimiteCotizacion = asignacionProveedor.fechaLimiteCotizacion> 
				${fechaLimiteCotizacion?string("dd/MM/yyyy")}	
				</@s.if>	
				</td>	
    		</tr>
	    		
    		<tr>
				<td class="textoCampo">Fecha de Entrega/Prestaci&oacute;n:</td>
      			<td class="textoDato" colspan="3">
      			<@s.if test="asignacionProveedor.fechaEntrega != null">
				<#assign fechaEntrega = asignacionProveedor.fechaEntrega> 
				${fechaEntrega?string("dd/MM/yyyy")}
				</@s.if>	
				&nbsp;
				</td>	
			</tr>
			<tr>
				<td class="textoCampo">Lugar de Entrega/Prestaci&oacute;n:</td>
      			<td  class="textoDato" colspan="3">
      			<@s.property default="&nbsp;" escape=false value="asignacionProveedor.lugarEntrega"/>							 
				</td>					
    		</tr>
    		
    		<tr>
				<td class="textoCampo">Observaciones:</td>
      			<td  class="textoDato" colspan="3">
      			<@s.property default="&nbsp;" escape=false value="asignacionProveedor.observaciones"/>	&nbsp;
				</td>					
    		</tr>
			<tr>
				<td class="textoCampo">Generado Automaticamente:</td>
      			<td  class="textoDato" colspan="3">
					<@s.property default="&nbsp;" escape=false value="fsc_fss.generadoAutomaticamente"/></td>
				</td>
    		</tr>	    		
    		<tr>
				<td class="textoCampo">Estado:</td>
      			<td  class="textoDato"colspan="3">
					<@s.property default="&nbsp;" escape=false value="asignacionProveedor.estado"/>
				</td>
			</tr>
			<tr><td class="textoCampo"  colspan="4">&nbsp;</td></tr>
	    </table>
	</div>
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
			<tr>
				<td><b>Datos del FSC/FSS</b></td>
			</tr>
		</table>
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
	    		<td class="textoCampo" colspan="4">&nbsp;</td>
	    	</tr>		
			<tr>
      			<td class="textoCampo">N&uacute;mero:</td>
      			<td class="textoDato"><@s.property default="&nbsp;" escape=false value="fsc_fss.numero"/></td>
      			<td  class="textoCampo">Fecha Solicitud: </td>
				<td class="textoDato">
				<@s.if test="fsc_fss.fechaSolicitud != null">
				<#assign fechaSolicitud = fsc_fss.fechaSolicitud> 
				${fechaSolicitud?string("dd/MM/yyyy")}	
				</@s.if>
				&nbsp;									
				</td>	      			
			</tr>	

			<tr>

				<td class="textoCampo">Cliente:</td>
      			<td class="textoDato">
				<@s.property default="&nbsp;" escape=false value="fsc_fss.cliente.descripcion"/>	
				</td>
				
				<td class="textoCampo">Tipo:</td>
      			<td class="textoDato">
      			<@s.property default="&nbsp;" escape=false value="fsc_fss.tipoFormulario"/>
      		
				</td>
    		</tr>
	    		
    		<tr>
				<td class="textoCampo">Fecha Entrega Estimada:</td>
      			<td class="textoDato">
 					<@s.if test="fsc_fss.fechaEntregaEstimada != null">
				<#assign fechaEntregaEstimada = fsc_fss.fechaEntregaEstimada> 
				${fechaEntregaEstimada?string("dd/MM/yyyy")}	
				</@s.if>

				</td>						
      		
				<td class="textoCampo">Monto Estimado:</td>
      			<td class="textoDato">
      			
  				<@s.property default="&nbsp;" escape=false value="fsc_fss.montoEstimado"/>

				</td>
			</tr>
    		<tr>
				<td class="textoCampo">Insumo, equipamiento y/o servicio solicitado:</td>
      			<td  class="textoDato" colspan="3">
					<@s.property default="&nbsp;" escape=false value="fsc_fss.descripcionSolicitud"/>
				&nbsp;
				</td>					
    		</tr>
    		
    		<tr>
				<td class="textoCampo">Normas y/o especificaciones:</td>
      			<td  class="textoDato" colspan="3">	      						
      			<@s.property default="&nbsp;" escape=false value="fsc_fss.normaEspecificacion"/>
      			&nbsp;
				</td>					
    		</tr>
    		
    		<tr>
				<td class="textoCampo">Requisitos de la calidad del proveedor:</td>
      			<td  class="textoDato" colspan="3">
      			  <@s.property default="&nbsp;" escape=false value="fsc_fss.requisitosCalidadProveedor"/>
				&nbsp;
				</td>					
    		</tr>

	    		<tr>
					<td class="textoCampo">Estado:</td>
	      			<td  class="textoDato" colspan="3">
						<@s.property default="&nbsp;" escape=false value="fsc_fss.estado"/></td>
					</td>

	    		</tr>
	    		<@s.if test="fsc_fss.clienteDerivado != null">	    		
	    		<tr>
					<td class="textoCampo">Cliente Derivado:</td>
	      			<td  class="textoDato">
						<@s.property default="&nbsp;" escape=false value="fsc_fss.clienteDerivado.descripcion"/></td>
					</td>

					<td class="textoCampo">&nbsp;</td>
	      			<td class="textoDato">&nbsp;</td>
	    		</tr>
	    		<tr>
					<td class="textoCampo">Observaciones:</td>
	      			<td  class="textoDato">
						<@s.property default="&nbsp;" escape=false value="fsc_fss.observaciones"/></td>
					</td>

					<td class="textoCampo">&nbsp;</td>
	      			<td class="textoDato">&nbsp;</td>
	    		</tr>
	    		</@s.if>
	    		
			</table>
		</div>	
		