<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<@vc.anchors target="contentTrx" ajaxFlag="ajax">
	<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
		<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
			<tr>
				<td>FSC/FSS asignados</td>
				<td>
					<#if !asignacionProveedor.desahibilitarBotonesAsignacion>
						<div align="right">
							<@s.a href="${request.contextPath}/compraContratacion/asignacionProveedor/selectFSC_FSS.action?asignacionProveedor.oid=${asignacionProveedor.oid?c}&asignacionProveedor.tipo=${asignacionProveedor.tipo.ordinal()?c}&navigation=seleccionar-fsc_fss&flowControl=regis" templateDir="custontemplates" id="agregarProductoBien" name="agregarProductoBien" cssClass="ocultarIcono">
								<b>Agregar</b><img src="${request.contextPath}/common/images/agregar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
							</@s.a>		
						</div>
					</#if>
				</td>	
			</tr>
		</table>

		<!-- Resultado Filtro -->						
		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="asignacionProveedor.fsc_fssList" id="fsc_fss" defaultsort=2>	
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select"  class="botoneraAnchoCon3" title="Acciones">
					<a href="${request.contextPath}/compraContratacion/asignacionProveedor/readFSC_FSSAsignacionProveedorView.action?asignacionProveedor.oid=${asignacionProveedor.oid?c}&fsc_fss.oid=${fsc_fss.oid?c}&navegacionIdBack=${navigationId}&navigationId=visualizar-fscfss&flowControl=regis"><img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver" border="0"></a>
					&nbsp;
					<#if !asignacionProveedor.desahibilitarBotonesAsignacion>
						<a href="${request.contextPath}/compraContratacion/asignacionProveedor/deleteFSC_FSSAsignacionProveedorView.action?asignacionProveedor.oid=${asignacionProveedor.oid?c}&fsc_fss.oid=${fsc_fss.oid?c}&navegacionIdBack=${navigationId}&navigationId=eliminar-fscfss&flowControl=regis"><img  src="${request.contextPath}/common/images/eliminar.gif" alt="Eliminar" title="Eliminar" border="0"></a>
						&nbsp;
					</#if>
				<#if fsc_fss.posibleReposicion?exists && fsc_fss.posibleReposicion.ordinal()==2>
					<#if !asignacionProveedor.desahibilitarBotonesAsignacion>
						<a href="${request.contextPath}/compraContratacion/asignacionProveedor/reponerView.action?asignacionProveedor.oid=${asignacionProveedor.oid?c}&fsc_fss.oid=${fsc_fss.oid?c}&navegacionIdBack=${navigationId}&navigationId=reponer-fscfss&flowControl=regis">
						<img  src="${request.contextPath}/common/images/reponer.gif" alt="Reponer" title="Reponer" border="0"></a>
						&nbsp;
					</#if>	
				</#if>
			</@display.column>
	
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="numero" title="N&uacute;mero" />

			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="tipoFormulario" title="Tipo" />
 	
	<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="cliente.descripcion" title="Cliente" />									
		
	<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="fechaSolicitud" format="{0,date,dd/MM/yyyy}"  title="Fecha Solicitud" />

	<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="montoEstimado" title="Monto Estimado" />			

	<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="estado" title="Estado" />
	<#if asignacionProveedor.tipo.ordinal()==2>
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="posibleReposicion" title="Posible Reposici&oacute;n" />
	</#if>
</@display.table>
</div>
</@vc.anchors>

