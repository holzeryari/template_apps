<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>


<@vc.anchors target="contentTrx" ajaxFlag="ajax">
	<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
		<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
			<tr>
				<td>FSC/FSS solicitados</td>
			</tr>
		</table>	
							
		<!-- Resultado Filtro -->						
		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="asignacionProveedor.fsc_fssList" id="fsc_fss" defaultsort=2>	
								
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select"  class="botoneraAnchoCon1" title="Acciones">
				
				<div align="center">		
					<a href="${request.contextPath}/compraContratacion/asignacionProveedor/readFSC_FSSAsignacionProveedorView.action?asignacionProveedor.oid=${asignacionProveedor.oid?c}&fsc_fss.oid=${fsc_fss.oid?c}&navegacionIdBack=${navigationId}&navigationId=visualizar-fscfss&flowControl=regis">
					<img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver" border="0" align="absmiddle"></a>
					&nbsp;
				</div>	
								
			</@display.column>
			
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="numero" title="N&uacute;mero" />
		
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="tipoFormulario" title="Tipo" />
		 	
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="cliente.descripcion" title="Cliente" />									
				
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="fechaSolicitud" format="{0,date,dd/MM/yyyy}"  title="Fecha Solicitud" />
		
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="montoEstimado" title="Monto Estimado" />			
			<#if asignacionProveedor.tipo.ordinal()==2>
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="estado" title="Estado" />
			</#if>
		</@display.table>
	</div>	
</@vc.anchors>

