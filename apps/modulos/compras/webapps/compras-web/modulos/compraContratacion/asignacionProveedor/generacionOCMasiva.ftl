<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>

<@vc.anchors target="contentTrx" ajaxFlag="ajax">
<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
		<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
			<tr>
				<td>Productos/Bienes solicitados en los FSC/FSS</td>
			</tr>
		</table>
					
		<table class="tablaDetalleCuerpo" cellpadding="3" >
			<#assign asignacionProveedorDetalleLista = asignacionProveedor.asignacionProveedorDetalleList>
				<#list asignacionProveedorDetalleLista as asignacionProveedorDetalle>
					<tr>
						<th align="center">Producto/Bien</th>
						<th align="center">C&oacute;digo</th>
						<th colspan="2" align="center" >Descripci&oacute;n</th>
						<th align="center">Marca</th>
						<th colspan="2" align="center">Modelo</th>
						<th colspan="2" align="center">Rubro</th>
						<th align="center">Cantidad Solicitada</th>
						<th align="center">Resto Cant. Solicitada </th>
					</tr>
					<tr>
						<td class="botoneraAnchoCon2"> 
							<div align="center">
							<@s.a templateDir="custontemplates" id="verAsignacionProveedorDetalle" name="verAsignacionProveedorDetalle" href="${request.contextPath}/compraContratacion/asignacionProveedor/readAsignacionProveedorDetalleView.action?asignacionProveedorDetalle.oid=${asignacionProveedorDetalle.oid?c}&navegacionIdBack=${navigationId}&navigationId=asignacionProveedorDetalle-visualizar&flowControl=regis" cssClass="ocultarIcono">
								<img src="${request.contextPath}/common/images/ver.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
							</@s.a>
							</div>
						</td>	
						<td class="estiloNumero">${asignacionProveedorDetalle.productoBien.oid}</td>
						<td colspan="2" class="estiloTexto">${asignacionProveedorDetalle.productoBien.descripcion}</td>
						<td class="estiloTexto">${asignacionProveedorDetalle.productoBien.marca}</td>
						<td class="estiloTexto" colspan="2">${asignacionProveedorDetalle.productoBien.modelo}</td>
						<td class="estiloTexto" colspan="2">${asignacionProveedorDetalle.productoBien.rubro.descripcion}</td>
						<td class="estiloNumero">${asignacionProveedorDetalle.cantidadSolicitadaTotal}</td>
						<td class="estiloNumero">${asignacionProveedorDetalle.cantidad}</td>				   
					</tr>
					<tr>
						<td>&nbsp;</td>
						<th class="botoneraAnchoCon3">								
								<b>Cotizaciones</b>									
						</th>
						<th align="center">Raz&oacute;n Social</th>
						<th align="center">Nombre</th>
						<th align="center">F. Hasta Validez Precios</th>
						<th align="center">Cant. Max. a Proveer</th>
						<th align="center">ICP</th>
						<th align="center">Situacion</th>
						<th align="center">Cant. Min.</th>
						<th align="center">Precio</th>
						<th align="center">Cant. a Pedir</th>
					</tr>
					<#assign cotizacionLista =asignacionProveedorDetalle.proveedorCotizacionList>						
					<#assign ultimoProveedor = 0>
					<#list cotizacionLista as asignacionProveedorDetalleCotizacion>
						<#if ultimoProveedor==asignacionProveedorDetalleCotizacion.cotizacion.pedidoCotizacionDetalle.pedidoCotizacion.proveedor.nroProveedor>
							<#assign ultimoProveedor = asignacionProveedorDetalleCotizacion.cotizacion.pedidoCotizacionDetalle.pedidoCotizacion.proveedor.nroProveedor>
							
							<tr>
								<td class="estiloNumero">
									${asignacionProveedorDetalleCotizacion.cotizacion.cantidadMinima}
								</td>
								<td class="estiloNumero">												
									${asignacionProveedorDetalleCotizacion.cotizacion.precio}									
								</td>
				
								<td class="estiloNumero">
									<#if asignacionProveedorDetalleCotizacion.cantidadPedida?exists>
										${asignacionProveedorDetalleCotizacion.cantidadPedida}
									<#else>
										&nbsp;
									</#if>
								</td>
							</tr>
									
								
						<#else>
						<#assign ultimoProveedor = asignacionProveedorDetalleCotizacion.cotizacion.pedidoCotizacionDetalle.pedidoCotizacion.proveedor.nroProveedor>
					
						<tr>
					
							<td class="estiloNumero" rowspan=${asignacionProveedorDetalleCotizacion.cantidadCotizacionesProveedor}>&nbsp;</td>
							<td class="estiloNumero" rowspan=${asignacionProveedorDetalleCotizacion.cantidadCotizacionesProveedor}  align="right" class="botoneraAnchoCon3">	
								<div align="center">
									<div class="alineacionMenor">
										<@s.a templateDir="custontemplates" id="modificarCotizacionCantidaPedida" name="modificarCotizacionCantidaPedida" href="${request.contextPath}/compraContratacion/asignacionProveedor/updateCantidadPedirView.action?asignacionProveedorDetalle.oid=${asignacionProveedorDetalle.oid?c}&proveedor.nroProveedor=${asignacionProveedorDetalleCotizacion.proveedor.nroProveedor?c}&navegacionIdBack=${navigationId}&navigationId=asigancionProveedor-updateCantidadPedir&flowControl=regis" cssClass="ocultarIcono">
											<img src="${request.contextPath}/common/images/modificar.gif"  border="0" align="absmiddle" hspace="3" >
										</@s.a>
									</div>
									<div class="alineacionMenor" >				
										<#if asignacionProveedorDetalleCotizacion.proveedor.esCooperativa?exists && asignacionProveedorDetalleCotizacion.proveedor.esCooperativa.ordinal()==2>
											<img src="${request.contextPath}/common/images/pino.gif"  border="0" >								
											<#else>
											<div class="item">
											<img src="${request.contextPath}/common/images/pino.gif"  border="0"  >
											</div>
										</#if>
									</div>
									
									<div class="alineacionMenor">							
										<#if asignacionProveedorDetalleCotizacion.proveedor.adtividadRegion?exists && asignacionProveedorDetalleCotizacion.proveedor.adtividadRegion.ordinal()==2>
											<img src="${request.contextPath}/common/images/sailboat.gif" border="0" >								
											<#else>
											<div class="item">
											<img src="${request.contextPath}/common/images/sailboat.gif" border="0"  >
											</div>
										</#if>	
									</div>
									<div class="alineacionMenor">
										<#if asignacionProveedorDetalleCotizacion.proveedor.detalleDePersona.socio?exists>
											<img src="${request.contextPath}/common/images/manosAgarradas.gif"  border="0"  >								
										<#else>
											<div class="item">
												<img src="${request.contextPath}/common/images/manosAgarradas.gif"  border="0"  >
											</div>
										</#if>
									</div>
								</div>
									
								</td>
								<td class="estiloTextoAnchoFijoMenor" rowspan=${asignacionProveedorDetalleCotizacion.cantidadCotizacionesProveedor}  align="center">			
									<#if asignacionProveedorDetalleCotizacion.proveedor.detalleDePersona.razonSocial?exists>
										${asignacionProveedorDetalleCotizacion.proveedor.detalleDePersona.razonSocial}
									</#if>&nbsp;
								</td>
								<td class="estiloTextoAnchofijoMenor" rowspan=${asignacionProveedorDetalleCotizacion.cantidadCotizacionesProveedor}  align="center">
									<#if asignacionProveedorDetalleCotizacion.proveedor.detalleDePersona.nombre?exists>
										${asignacionProveedorDetalleCotizacion.proveedor.detalleDePersona.nombre}
									</#if>	&nbsp;								
								</td>

								<td class="estiloNumeroAnchoFijo" rowspan=${asignacionProveedorDetalleCotizacion.cantidadCotizacionesProveedor} align="center">
									<#if asignacionProveedorDetalleCotizacion.cotizacion.pedidoCotizacionDetalle.pedidoCotizacion.fechaValidezPrecio?exists>
										${asignacionProveedorDetalleCotizacion.cotizacion.pedidoCotizacionDetalle.pedidoCotizacion.fechaValidezPrecio?string("dd/MM/yyyy")}
									</#if>&nbsp;
								</td>
								<td class="estiloNumeroAnchoFijo" rowspan=${asignacionProveedorDetalleCotizacion.cantidadCotizacionesProveedor}  align="center">
									<#if asignacionProveedorDetalleCotizacion.cotizacion.cantidadMaxima?exists>
										${asignacionProveedorDetalleCotizacion.cotizacion.cantidadMaxima}
									</#if>&nbsp;
									
								</td>
								
								<td class="estiloNumeroAnchoFijo" rowspan=${asignacionProveedorDetalleCotizacion.cantidadCotizacionesProveedor}  align="center">
									<#if asignacionProveedorDetalleCotizacion.proveedor.ultimoICP?exists>
        								${asignacionProveedorDetalleCotizacion.proveedor.ultimoICP}
        							</#if>&nbsp;
								</td>	
								
																	
								<td class="estiloTextoAnchoFijoMenor" rowspan=${asignacionProveedorDetalleCotizacion.cantidadCotizacionesProveedor}  align="center">
									<#if asignacionProveedorDetalleCotizacion.proveedor.situacionICP?exists>
        								${asignacionProveedorDetalleCotizacion.proveedor.situacionICP}
        							</#if>&nbsp;
								</td>	
																	
								<td class="estiloNumeroAnchoFijo">
									${asignacionProveedorDetalleCotizacion.cotizacion.cantidadMinima}&nbsp;
								</td>
								<td class="estiloNumeroAnchoFijo">												
									${asignacionProveedorDetalleCotizacion.cotizacion.precio}		&nbsp;							
								</td>
								<td class="estiloNumeroAnchoFijo" rowspan="1">
									<#if asignacionProveedorDetalleCotizacion.cantidadPedida?exists>
										${asignacionProveedorDetalleCotizacion.cantidadPedida}
									<#else>
										&nbsp;
									</#if> 
						
								</td>
							</tr>
				
						</#if>												
								
						</#list>
						<tr><td colspan="11">&nbsp;</td></tr>
					</#list>						
			
				</table>
			</div>		
</@vc.anchors>



