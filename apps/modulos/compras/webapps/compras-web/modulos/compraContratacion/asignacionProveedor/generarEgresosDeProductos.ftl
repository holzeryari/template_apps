<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign tiles=JspTaglibs["/WEB-INF/tlds/tiles-jsp.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>

<@s.hidden id="navigationId" name="navigationId"/>
<@s.hidden id="navegacionIdBack" name="navegacionIdBack"/>

<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
			<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos del Egreso de Producto</b></td>
				<td>
					<div align="right">
						<@s.a templateDir="custontemplates" id="modificarAsignacionProveedor" name="modificarAsignacionProveedor" href="javascript://nop/" cssClass="ocultarIcono">
							<b>Modificar</b><img src="${request.contextPath}/common/images/modificar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
						</@s.a>
					</div>
				</td>
			</tr>
		</table>

		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>	

			<tr>
      			<td class="textoCampo">N&uacute;mero:</td>
      			<td class="textoDato"><@s.property default="&nbsp;" escape=false value="egresoProducto.numero"/></td>
      			<td  class="textoCampo">Fecha: </td>
				<td class="textoDato">
				<@s.if test="egresoProducto.fecha != null">
				<#assign fechaEgreso = egresoProducto.fecha> 
				${fechaEgreso?string("dd/MM/yyyy")}	
				</@s.if>									
				</td>	      			
			</tr>	

			<tr>
      			<td class="textoCampo">Dep&oacute;sito Origen:</td>
	      		<td class="textoDato">		
	      		<@s.property default="&nbsp;" escape=false value="egresoProducto.depositoOrigen.descripcion"/>
	      		</td>		      		
				<td class="textoCampo">Tipo Destino:</td>
      			<td class="textoDato">
      			<@s.property default="&nbsp;" escape=false value="egresoProducto.tipoDestino"/>
				</td>
    		</tr>
	    		
    		<tr>
      			<td class="textoCampo">Dep&oacute;sito Destino:</td>
	      			<td class="textoDato">
	      			<@s.property default="&nbsp;" escape=false value="egresoProducto.depositoDestino.descripcion"/>
	      		</td>		      		
			
				<td class="textoCampo">Cliente Destino:</td>
      			<td class="textoDato">
      				  <@s.property default="&nbsp;" escape=false value="egresoProducto.clienteDestino.descripcion"/>
				</td>	
			</tr>
	    	<tr>				
				<td class="textoCampo">FSC:</td>
      			<td class="textoDato" colspan="3">
      			<@s.property default="&nbsp;" escape=false value="egresoProducto.fsc_fss.numero"/>
      			</td>	
				
    		</tr>
	    	<tr>
				<td class="textoCampo">Observaciones:</td>
	      		<td  class="textoDato" colspan="3">
	      			<@s.property default="&nbsp;" escape=false value="egresoProducto.observaciones"/>
				</td>					
	    	</tr>

	    	<tr>
				<td class="textoCampo">Estado:</td>
      			<td  class="textoDato">
					<@s.property default="&nbsp;" escape=false value="egresoProducto.estado"/></td>
				</td>

				<td class="textoCampo">&nbsp;</td>
      			<td class="textoDato">&nbsp;</td>
    		</tr>
		</table>
	</div>		
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos del Producto</b></td>
			</tr>
		</table>

		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>			
			<tr>
				<td class="textoCampo">C&oacute;digo:</td>
      			<td class="textoDato"><@s.property default="&nbsp;" escape=false value="producto.oid"/></td>
      			<td  class="textoCampo">Descripci&oacute;n: </td>
				<td class="textoDato">
				 	<@s.property default="&nbsp;" escape=false value="producto.descripcion"/>
				</td>	      			
			</tr>	
			<tr>
      			<td class="textoCampo">Tipo:</td>
	      		<td class="textoDato">
	      			<@s.property default="&nbsp;" escape=false value="producto.tipo"/>
	      		</td>		      		
										
				<td class="textoCampo">Rubro:</td>
  				<td class="textoDato">
  					<@s.property default="&nbsp;" escape=false value="producto.rubro.descripcion"/>
				</td>
    		</tr>
			<tr>
				<td class="textoCampo">Marca: </td>
				<td  class="textoDato">
					<@s.property default="&nbsp;" escape=false value="producto.marca"/>
					
				</td>
				<td class="textoCampo">Modelo: </td>
				<td  class="textoDato">
					<@s.property default="&nbsp;" escape=false value="producto.modelo"/>
				</td>
    		</tr>
			<tr>
				<td class="textoCampo">Valor Mercado: </td>
				<td  class="textoDato">
					<@s.property default="&nbsp;" escape=false value="producto.valorMercado"/>
					
				</td>
				<td class="textoCampo">Fecha Valor Mercado: </td>
				<td  class="textoDato">
					<@s.property default="&nbsp;" escape=false value="producto.fechaValorMercado"/>
				</td>
    		</tr>
			<tr>
				<td class="textoCampo">C&oacute;digo de Barra: </td>
				<td  class="textoDato">
				<@s.property default="&nbsp;" escape=false value="producto.codigoBarra"/>
				</td>
				<td class="textoCampo">Es Cr&iacute;tico: </td>
				<td  class="textoDato">
				<@s.property default="&nbsp;" escape=false value="producto.critico"/>
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Reposici&oacute;n Autm&aacute;tica: </td>
				<td  class="textoDato">
				<@s.property default="&nbsp;" escape=false value="producto.reposicionAutomatica"/>
				</td>
				<td class="textoCampo">Existencia M&iacute;nima: </td>
				<td  class="textoDato">
							<@s.property default="&nbsp;" escape=false value="producto.existenciaMinima"/>
				</td>
    		</tr>
			<tr>
    		<td class="textoCampo">Tipo Unidad:</td>
    		<td  class="textoDato" colspan="3">
					<@s.property default="&nbsp;" escape=false value="producto.tipoUnidad.codigo"/>
				</td>
    		</tr>
			
			<tr>
				<td class="textoCampo">Estado: </td>
      			<td class="textoDato">
      			<@s.property default="&nbsp;" escape=false value="producto.estado"/></td>
				<td class="textoCampo">Existencia</td>
				<td  class="textoDato">
				<@s.property default="&nbsp;" escape=false value="producto.cantidadTotalExistencia"/>
				</td>
    		</tr>
			<tr>
				<td class="texto_datos" align="right">Cantidad Solicitada Pendiente en FSC: </td>
      			<td class="texto" align="left" colspan="3">
      				<@s.property default="&nbsp;" escape=false value="fsc_fssDetalle.cantidadSolicitadaPendiente"/>
      			</td>

    		</tr>
		</table>
	</div>
	<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
		<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
            <tr>
				<td><b>Existencias del Producto</b></td>
			</tr>
		</table>

		<!-- Resultado Filtro -->				
		<@vc.anchors target="contentTrx">	
			<@s.hidden id="cantidadesEgresadas" name="cantidadesEgresadas"/>				
			<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="producto.existenciaList" id="existencia" defaultsort=2>	
						
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select"  class="botoneraAnchoCon3" title="Cantidad a Egresar">
		
					<@s.textfield 
						templateDir="custontemplates"
						template="textMoney"   
						id="${existencia.oid}" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="cantidadEgresar"
						title="Cantidad a Egresar" />	
				</@display.column>
	
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service"  property="deposito.descripcion" title="Dep&oacute;sito" />
		
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service"  property="cantidad" title="Cantidad de Existencia" />
		
		</@display.table>
	</div>	
</@vc.anchors>	
<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancel" type="button" name="btnCancelar" value="Cancelar" class="boton"/>
				<input id="store" type="button" name="btnGenerar" value="Generar Egreso de Producto" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

<@vc.htmlContent 
	  baseUrl="${request.contextPath}/compras/flowControl.action" 
	  source="cancel" 
	  success="contentTrx" 
	  failure="errorTrx"  
	  parameters="navigationId={navegacionIdBack},flowControl=back"/>
	  
<@vc.htmlContent 
	  baseUrl="${request.contextPath}/compraContratacion/asignacionProveedor/generarEgresoProductos.action" 
	  source="store" 
	  success="contentTrx" 
	  failure="errorTrx" 
	  preFunction="listaCantidadesEgresar" 
	  parameters="cantidadesEgresadas={cantidadesEgresadas},asignacionProveedor.oid=${asignacionProveedor.oid},producto.oid=${producto.oid},fsc_fssDetalle.oid=${fsc_fssDetalle.oid},fsc_fss.oid=${fsc_fssDetalle.fsc_fss.oid},navegacionIdBack=${navegacionIdBack}"/>
	  	  						
