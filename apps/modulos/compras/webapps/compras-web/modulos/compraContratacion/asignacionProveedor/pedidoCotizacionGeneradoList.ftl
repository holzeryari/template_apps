<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<@s.hidden id="pedidoCotizacionList" name="pedidoCotizacionList"/>
	
<div id="capaSeparadora" class="capaSeparadora">&nbsp;</div>
	<@vc.anchors target="contentTrx" ajaxFlag="ajax">
	<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
		<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
			<tr>
				<td>Pedidos de Cotizaci&oacute;n generados</td>
			</tr>
		</table>
			
		<!-- Resultado Filtro -->						
		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="asignacionProveedor.pedidoCotizacionList" id="pedidoCotizacion" defaultsort=2>	
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select"  class="botoneraAnchoCon1" title="Acciones">		
			<div align="center">	
				<a href="${request.contextPath}/compraContratacion/pedidoCotizacion/readPedidoCotizacionView.action?pedidoCotizacion.oid=${pedidoCotizacion.oid?c}&navigationId=pedidoCotizacion-visualizar&flowControl=regis&navegacionIdBack=${navigationId}">
				<img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver" border="0" align="absmiddle"></a>
				&nbsp;
				
				<#assign pedidoCotizacionDetalleLista = pedidoCotizacion.pedidoCotizacionDetalleList>
				<#list pedidoCotizacionDetalleLista as pedidoCotizacionDetalle>
					<#if pedidoCotizacionDetalle.archivo?exists>
						<a href="javascript://nop/" class="item" >
							<img src="${request.contextPath}/common/images/bajar.png" alt="Bajar Archivo" title="Bajar Archivo" border="0"
							onclick="window.open('${request.contextPath}/compraContratacion/pedidoCotizacion/pedidoCotizacionRedirectArchivoView.action?pedidoCotizacionDetalle.oid=${pedidoCotizacionDetalle.oid?c}', '_blank','width=400,height=300,resizable=yes,scrollbars=yes,left=100,top=80,toolbar=no,menubar=no,location=no'); return false;">
						</a>
					</#if> 
				</#list>
				
			</div>
			
			</@display.column>
							
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="numero" title="Nro Pedido" />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="fechaPedido" format="{0,date,dd/MM/yyyy}"  title="Fecha Pedido" />	
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="estado" title="Estado" />	
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" title="Proveedor">  
				${pedidoCotizacion.proveedor.detalleDePersona.razonSocial}
				
				<#if pedidoCotizacion.proveedor.detalleDePersona.nombre?exists> 
						 ${pedidoCotizacion.proveedor.detalleDePersona.nombre}
				</#if>
				
				
			</@display.column>
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="proveedor.email" title="Mail Proveedor" />
	</@display.table>
	</div>
</@vc.anchors>
