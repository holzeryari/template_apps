<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>


<@vc.anchors target="contentTrx" ajaxFlag="ajax">
<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
		<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
			<tr>
				<td>Productos/Bienes solicitados</td>
			</tr>
		</table>
					
		<table class="tablaDetalleCuerpo" cellpadding="3" >
			<tr>
				<th align="center">Acciones</th>
				<th align="center">N&uacute;mero Item</th>
				<th align="center">C&oacute;digo</th>
				<th align="center">Descripci&oacute;n</th>
				<th align="center">Cant. Solicitada</th>
				<th align="center">Cant. Repuesta</th>
				<th align="center">Cant. Solicitada Pendiente</th>							
				<th align="center">Existencia</th>
			</tr>
			<#assign fsc_fssLista = fsc_fss.fsc_fssDetalleList>
				<#list fsc_fssLista as fsc_fssDetalle>
					<#if fsc_fssDetalle.producto?exists>
						<tr>
							<td class="botoneraAnchoCon2"> 
								<div align="center">
									<a href="${request.contextPath}/compraContratacion/fsc_fss/readFSC_FSSDetalleView.action?fsc_fssDetalle.oid=${fsc_fssDetalle.oid?c}&navegacionIdBack=${navigationId}"><img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver" border="0" align="absmiddle"></a>
									<#if fsc_fssDetalle.producto?exists && (fsc_fssDetalle.producto.cantidadTotalExistencia > 0) && (fsc_fssDetalle.cantidadSolicitadaPendiente>0)  >		
										<a href="${request.contextPath}/compraContratacion/asignacionProveedor/generarEgresosView.action?fsc_fssDetalle.oid=${fsc_fssDetalle.oid?c}&producto.oid=${fsc_fssDetalle.productoBien.oid?c}&asignacionProveedor.oid=${asignacionProveedor.oid?c}&navegacionIdBack=${navigationId}&navigationId=generarEgresoProductos"><img  src="${request.contextPath}/common/images/reponer.gif" alt="Reponer" title="Reponer" border="0" align="absmiddle"></a>
									</#if>					
								</div>
							</td>	
							<td class="estiloNumero">${fsc_fssDetalle.numero}	&nbsp;	</td>
							<td class="estiloNumero">${fsc_fssDetalle.producto.oid}	&nbsp;	</td>
							<td class="estiloTexto">${fsc_fssDetalle.producto.descripcion}	&nbsp;	</td>
							<td class="estiloNumero">${fsc_fssDetalle.cantidadSolicitada}	&nbsp;	</td>
							<td class="estiloNumero">
							<#if fsc_fssDetalle.cantidadRepuesta?exists>
								${fsc_fssDetalle.cantidadRepuesta}
							</#if>
								&nbsp;	
							</td>
							<td class="estiloNumero">
								<#if (fsc_fssDetalle.cantidadSolicitadaPendiente>0)>
									${fsc_fssDetalle.cantidadSolicitadaPendiente}	
								<#else>
									0		
								</#if>
							</td>	
							<td class="estiloNumero">${fsc_fssDetalle.producto.cantidadTotalExistencia}	&nbsp;	</td>			   
						</tr>
					</#if>
				</#list>
		</table>	
	</div>	
	<div id="capaSeparadora" class="capaSeparadora">&nbsp;</div>
	<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
		<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
			<tr>
				<td>Egresos de Productos efectuados</td>
			</tr>
		</table>
					
		<table class="tablaDetalleCuerpo" cellpadding="3" >
			<tr>									
				<th align="center">Acciones</th>
				<th align="center" >Nro. Egreso</th>
				<th align="center" >Fecha Egreso</th>
				<th align="center" >Dep&oacute;sito Origen</th>
				<th align="center" >Destino</th>
				<th align="center" >Producto Egresado</th>															
			</tr>
			<#assign fsc_fssLista = fsc_fss.fsc_fssDetalleList>
				<#list fsc_fssLista as fsc_fssDetalle>
					<#if fsc_fssDetalle.producto?exists>
						<#if fsc_fssDetalle.producto.listaEgresos?exists && (fsc_fssDetalle.producto.listaEgresos.size()>0)>
							<#assign egresoLista = fsc_fssDetalle.producto.listaEgresos>
							<#list egresoLista as egresoProducto>
								<#assign egresoDetalleLista = egresoProducto.egresoProductoDetalleList>
									<#list egresoDetalleLista as egresoProductoDetalle>
										<tr>
											<td>
												<@s.a 
													templateDir="custontemplates"
													cssClass="item" 
													href="${request.contextPath}/deposito/egresoProducto/readEgresoProductoView.action?egresoProducto.oid=${egresoProductoDetalle.egresoProducto.oid?c}&navegacionIdBack=${navigationId}&navigationId=egresoProducto-visualizar&flowControl=regis">
													<img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver"  border="0" align="absmiddle">
												</@s.a>
											</td>
											<td class="estiloNumero">
												<#if egresoProductoDetalle.egresoProducto.numero?exists>
													${egresoProductoDetalle.egresoProducto.numero}</#if>	&nbsp;
											</td>
											<td class="estiloNumero"> 
												<#if egresoProductoDetalle.egresoProducto.fecha?exists>${egresoProductoDetalle.egresoProducto.fecha?string("dd/MM/yyyy")}	</#if>	&nbsp;		
											</td>
											<td class="estiloTexto">
												${egresoProductoDetalle.egresoProducto.depositoOrigen.descripcion}	&nbsp;	
											</td>
											<td class="estiloTexto">
												${egresoProductoDetalle.egresoProducto.clienteDestino.descripcion}	&nbsp;	
											</td>
											<td class="estiloTexto">${egresoProductoDetalle.producto.descripcion}	&nbsp;	
											</td>
										</tr>
										</#list>
									</#list>
								</#if>	
							</#if>
						</#list>					
				</table>
			</div>		
		
		</@vc.anchors>
		
			<div id="capaSeparadora" class="capaSeparadora">&nbsp;</div>
