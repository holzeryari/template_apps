<@s.hidden id="proveedor.nroProveedor" name="proveedor.nroProveedor"/>
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos de la Persona</b></td>
			</tr>
		</table>

		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>		
				
			<tr>
				<td class="textoCampo">Identificador:</td>
				<td class="textoDato" colspan="3">
				<@s.property default="&nbsp;" escape=false value="proveedor.persona.oid"/></td>
  			</tr>	
			<tr>
				<td class="textoCampo">Tipo de Persona:</td>
				<td class="textoDato"colspan="3">
					<@s.property default="&nbsp;" escape=false value="proveedor.persona.tipoPersona"/>
				</td>
			</tr>
							
			<tr>
				<td class="textoCampo">Raz&oacute;n social:</td>
				<td class="textoDato">
						<@s.property default="&nbsp;" escape=false value="proveedor.detalleDePersona.razonSocial"/>
		  		</td>
						
				<td class="textoCampo">Nombre:</td>
				<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="proveedor.detalleDePersona.nombre"/>
				</td>
			</tr>
			
			<tr>
				<td class="textoCampo">N&uacute;mero de Documento:</td>
				<td class="textoDato">
				<@s.property default="&nbsp;" escape=false value="proveedor.persona.docPersonal"/>
				</td>	
				
				<td class="textoCampo">CUIT/CUIL:</td>
				<td class="textoDato">
						<@s.property default="&nbsp;" escape=false value="proveedor.persona.docFiscal"/>		
					</td>
			</tr>
							
			<tr>
				<td class="textoCampo">Fecha Nacimiento/Inicio:</td>
				<td class="textoDato">
				<@s.property default="&nbsp;" escape=false value="proveedor.persona.fechaNacimiento"/>
				</td>
				
				<td class="textoCampo">Sexo:</td>
				<td class="textoDato">
						<@s.property default="&nbsp;" escape=false value="proveedor.persona.sexo"/>
		  		</td>
			</tr>
			<tr><td colspan="4" class="textoCampo">&nbsp;</td></tr>
		</table>				
	</div>
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Direcci&oacute;n Fiscal</b></td>
			</tr>
		</table>

		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>											
			<tr>
				<td class="textoCampo">C&oacute;digo postal:</td>
				<td class="textoDato">
				<@s.property default="&nbsp;" escape=false value="proveedor.detalleDePersona.codigoPostal.pk.identificador"/>
				</td>
				
				<td class="textoCampo">Localidad:</td>
				<td class="textoDato">
				<@s.property default="&nbsp;" escape=false value="proveedor.detalleDePersona.codigoPostal.descripcion"/></td>										
			</tr>
			<tr>
				<td class="textoCampo">Provincia:</td>
				<td class="textoDato">
				<@s.property default="&nbsp;" escape=false value="proveedor.detalleDePersona.codigoPostal.provincia.descripcion"/></td>
				
				<td class="textoCampo">Pais:</td>
				<td class="textoDato">
				<@s.property default="&nbsp;" escape=false value="proveedor.detalleDePersona.codigoPostal.provincia.pais.descripcion"/></td>
			</tr>
			<tr>
				<td class="textoCampo">Secuencia CP:</td>
				<td class="textoDato">
				<@s.property default="&nbsp;" escape=false value="proveedor.detalleDePersona.codigoPostal.pk.secuencia"/>
				
				<td class="textoCampo">Letra CP:</td>
				<td class="textoDato">
				
				<@s.property default="&nbsp;" escape=false value="proveedor.detalleDePersona.letrasCP"/>
				&nbsp;
			</tr>
			<tr>
				<td class="textoCampo">Calle:</td>
				<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="proveedor.detalleDePersona.calle"/>
				</td>
						
				<td class="textoCampo">N&uacute;mero de Finca:</td>
				<td class="textoDato">
				<@s.property default="&nbsp;" escape=false value="proveedor.detalleDePersona.numeroFinca"/>
			</td>
			</tr>
			<tr>	
				<td class="textoCampo">Depto:</td>
				<td class="textoDato">
						<@s.property default="&nbsp;" escape=false value="proveedor.detalleDePersona.aptoCasa"/>
						&nbsp;
		  		</td>
				
				<td class="textoCampo">Entre Calles:</td>
				<td class="textoDato">
						<@s.property default="&nbsp;" escape=false value="proveedor.detalleDePersona.entreCalles"/>
						&nbsp;
				</td>
			</tr>
			<tr><td colspan="4" class="textoCampo">&nbsp;</td></tr>
		</table>
	</div>					
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Condici&oacute;n Fiscal</b></td>
			</tr>
		</table>

		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>											
			<tr>
				<td class="textoCampo">Fecha Vencimiento DGI:</td>
				<td class="textoDato">
					<@s.if test="proveedor.persona.venceFormDGI != null">
						<#assign fecha = proveedor.persona.venceFormDGI> 
						${fecha?string("dd/MM/yyyy")}	
					</@s.if>		&nbsp;							
				</td>
				<td class="textoCampo">Condici&oacute;n Fiscal Impuesto Ganancias:</td>
				<td class="textoDato">
				<@s.property default="&nbsp;" escape=false value="proveedor.persona.inscriptoGanancia"/>
				</td>		
			</tr>
			<tr>	
				<td class="textoCampo">Condici&oacute;n Fiscal IVA:</td>
				<td class="textoDato" colspan="3">
				<@s.property default="&nbsp;" escape=false value="proveedor.persona.situacionFiscal.codigo"/>
		  	</td>
		</tr>
	</table>
	</div>