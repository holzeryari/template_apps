<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>


<@vc.anchors target="contentTrx" ajaxFlag="ajax">
	<div id="capaSeparadora" class="capaSeparadora">&nbsp;</div>
	<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
		<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
			<tr>
				<td>Servicios solicitados en los FSC/FSS que no fueron pedidos</td>
			</tr>
		</table>

		<table class="tablaDetalleCuerpo" cellpadding="3" >
			<#assign asignacionProveedorDetalleLista = asignacionProveedor.asignacionProveedorDetalleList>
			<#list asignacionProveedorDetalleLista as asignacionProveedorDetalle>					
				<#if (asignacionProveedorDetalle.cantidad==0) >
				<tr>
					<th colspan="1" align="center">Acciones</th>
					<th colspan="1" align="center">C&oacute;digo</th>
					<th colspan="1" align="center" >Descripci&oacute;n</th>
					<th colspan="1" align="center">Rubro</th>
					<th colspan="1" align="center">Tipo</th>
					<th colspan="1" align="center">Proveedor &Uacute;nico</th>
				</tr>
						
				<tr>
					<td>							
						<div align="center">
						<@s.a templateDir="custontemplates" id="verAsignacionProveedorDetalle" name="verAsignacionProveedorDetalle" href="" cssClass="ocultarIcono">
							<img src="${request.contextPath}/common/images/ver.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
						</@s.a>						
						</div>								
					</td>
					<td class="estiloNumero">${asignacionProveedorDetalle.servicio.oid}</td>
					<td class="estiloTexto">${asignacionProveedorDetalle.servicio.descripcion}</td>
					<td class="estiloTexto">${asignacionProveedorDetalle.servicio.rubro.descripcion}</td>				
					<td class="estiloTexto">${asignacionProveedorDetalle.servicio.tipo}</td>
					<td class="estiloTexto">${asignacionProveedorDetalle.servicio.proveedorUnico}</td>			  			  
				</tr>
			</#if>
		</#list>						
	</table>			
	</div>
</@vc.anchors>



