<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign tiles=JspTaglibs["/WEB-INF/tlds/tiles-jsp.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>


<@s.hidden id="fsc_fss.oid" name="fsc_fss.oid"/>
<@s.hidden id="fsc_fss.versionNumber" name="fsc_fss.versionNumber"/>

<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
			<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos del FSC/FSS</b></td>
				<td>
					<div align="right">
						<@s.a templateDir="custontemplates" id="modificarFSC_FSS" name="modificarFSC_FSS" href="javascript://nop/" cssClass="ocultarIcono">
							<b>Modificar</b><img src="${request.contextPath}/common/images/modificar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
						</@s.a>
					</div>
				</td>
			</tr>
		</table>

		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>		
			<tr>
      			<td class="textoCampo">N&uacute;mero:</td>
      			<td class="textoDato"><@s.property default="&nbsp;" escape=false value="fsc_fss.numero"/></td>
      			<td  class="textoCampo">Fecha Solicitud: </td>
				<td class="textoDato">
					<@s.if test="fsc_fss.fechaSolicitud != null">
					<#assign fechaSolicitud = fsc_fss.fechaSolicitud> 
					${fechaSolicitud?string("dd/MM/yyyy")}	
					</@s.if>									
				</td>	      			
			</tr>	

			<tr>
				<@tiles.insertAttribute name="cliente"/>		

				<td class="textoCampo">Tipo:</td>
      			<td class="textoDato">
      			<@s.select 
							templateDir="custontemplates" 
							id="fsc_fss.tipoFormulario" 
							cssClass="textarea"
							cssStyle="width:165px" 
							name="fsc_fss.tipoFormulario" 
							list="tipoFormularioList" 
							listKey="key" 
							listValue="description" 
							value="fsc_fss.tipoFormulario.ordinal()"
							title="Tipo Formulario"
							headerKey="0"
							headerValue="Todos"							
							/>
				</td>
    		</tr>
	    		
    		<tr>
				<td class="textoCampo">Fecha Entrega Estimada:</td>
      			<td class="textoDato">
					<@vc.rowCalendar 
					templateDir="custontemplates" 
					id="fsc_fss.fechaEntregaEstimada" 
					cssClass="textarea"
					cssStyle="width:160px"						
					name="fsc_fss.fechaEntregaEstimada" 
					title="Fecha Desde" />
				</td>						
      		
				<td class="textoCampo">Monto Estimado:</td>
      			<td class="textoDato">
      			<@s.textfield 
      					templateDir="custontemplates" 
						id="fsc_fss.montoEstimado" 
						cssClass="textarea"
						template="textMoney" 
						cssStyle="width:160px" 
						name="fsc_fss.montoEstimado" 
						title="Monto Estimado" />
				</td>
			</tr>
				
			<tr>
				<td class="textoCampo">Insumo, equipamiento y/o servicio solicitado:</td>
      			<td  class="textoDato" colspan="3">
				<@s.textarea	
						templateDir="custontemplates" 						  
						cols="89" rows="4"	      					
						cssClass="textarea"
						id="fsc_fss.descripcionSolicitud"							 
						name="fsc_fss.descripcionSolicitud"  
						label="Insumo, equipamiento y/o servicio solicitado"														
						 />
				</td>					
    		</tr>
	    		
    		<tr>
				<td class="textoCampo" >Normas y/o especificaciones:</td>
      			<td  class="textoDato" colspan="3">
				<@s.textarea	
						templateDir="custontemplates" 						  
						cols="89" rows="4"	      					
						cssClass="textarea"
						id="fsc_fss.normaEspecificacion"							 
						name="fsc_fss.normaEspecificacion"  
						label="Normas y/o especificaciones"														
						 />
				</td>					
    		</tr>
	    		
	    	<tr>
				<td class="textoCampo">Requisitos de la calidad del proveedor:</td>
      			<td  class="textoDato" colspan="3">
				<@s.textarea	
						templateDir="custontemplates" 						  
						cols="89" rows="4"	      					
						cssClass="textarea"
						id="fsc_fss.requisitosCalidadProveedor"							 
						name="fsc_fss.requisitosCalidadProveedor"  
						label="Requisitos de la calidad del proveedor"														
						 />
				</td>					
    		</tr>

			<tr>
				<td class="textoCampo">Generado Automaticamente:</td>
      			<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="fsc_fss.generadoAutomaticamente"/></td>
				</td>
				<td class="textoCampo">Intervenido Automaticamente:</td>
      			<td  class="textoDato">
					<@s.property default="&nbsp;" escape=false value="fsc_fss.intervenidoAutomaticamente"/></td>
				</td>
    		</tr>
	    	<tr>
				<td class="textoCampo">Estado:</td>
      			<td  class="textoDato" colspan="3">
					<@s.property default="&nbsp;" escape=false value="fsc_fss.estado"/></td>
				</td>
			</tr>
	    	<@s.if test="fsc_fss.clienteDerivado != null">	    		
	    		<tr>
					<td class="textoCampo">Cliente Derivado:</td>
	      			<td  class="textoDato" colspan="3">
						<@s.property default="&nbsp;" escape=false value="fsc_fss.clienteDerivado.descripcion"/></td>
					</td>
				</tr>
		    	<tr>
					<td class="textoCampo">Observaciones:</td>
	      			<td class="textoDato" colspan="3">
						<@s.property default="&nbsp;" escape=false value="fsc_fss.observaciones"/></td>
					</td>
				</tr>
	    	</@s.if>
	    	
	    	<@s.if test="fsc_fss.comentario != null">	    
		    	<tr>
					<td class="textoCampo">Comentario Autorizaci&oacute;n/Rechazo:</td>
	      			<td  class="textoDato" colspan="3">
					<@s.textarea	
							templateDir="custontemplates" 						  
							cols="89" rows="4"	      					
							cssClass="textarea"
							id="fsc_fss.comentario"							 
							name="fsc_fss.comentario"  
							label="Comentario Autorizacion/Rechazo"														
							 />
					</td>					
	    		</tr>
    		</@s.if>
    		
	    	<tr><td colspan="4" class="textoCampo">&nbsp;</td></tr>
	    </table>
		
		</div>	
		<@tiles.insertAttribute name="detallesFormulario"/>				
	
<@vc.htmlContent 
  baseUrl="${request.contextPath}/compraContratacion/fsc_fss/updateFSC_FSSView.action" 
  source="modificarFSC_FSS" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="fsc_fss.oid={fsc_fss.oid},fsc_fss.versionNumber={fsc_fss.versionNumber},navigationId=fsc_fss-actualizar"/>



