<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancel" type="button" name="btnCancelar" value="Cancelar" class="boton"/>
				<input id="store" type="button" name="btnAgregar" value="Agregar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/compraContratacion/fsc_fss/createFSC_FSS.action" 
  source="store" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="fsc_fss.cliente.oid={fsc_fss.cliente.oid},fsc_fss.tipoFormulario={fsc_fss.tipoFormulario},fsc_fss.fechaEntregaEstimada={fsc_fss.fechaEntregaEstimada},fsc_fss.montoEstimado={fsc_fss.montoEstimado},fsc_fss.descripcionSolicitud={fsc_fss.descripcionSolicitud},fsc_fss.normaEspecificacion={fsc_fss.normaEspecificacion},fsc_fss.requisitosCalidadProveedor={fsc_fss.requisitosCalidadProveedor}"/>
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=buscar-fsc_fss,flowControl=back"/>
  
  <@vc.htmlContent 
  baseUrl="${request.contextPath}/compraContratacion/fsc_fss/selectClienteABM.action" 
  source="seleccionarCliente" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=fsc_fss-create,flowControl=change,fsc_fss.cliente.oid={fsc_fss.cliente.oid},fsc_fss.tipoFormulario={fsc_fss.tipoFormulario},fsc_fss.fechaEntregaEstimada={fsc_fss.fechaEntregaEstimada},fsc_fss.montoEstimado={fsc_fss.montoEstimado},fsc_fss.descripcionSolicitud={fsc_fss.descripcionSolicitud},fsc_fss.normaEspecificacion={fsc_fss.normaEspecificacion},fsc_fss.requisitosCalidadProveedor={fsc_fss.requisitosCalidadProveedor}"/>