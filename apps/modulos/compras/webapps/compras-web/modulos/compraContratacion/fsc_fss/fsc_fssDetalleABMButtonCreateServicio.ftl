<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancel" type="button" name="btnCancelar" value="Cancelar" class="boton"/>
				<input id="store" type="button" name="btnAgregar" value="Agregar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>


<@vc.htmlContent 
  baseUrl="${request.contextPath}/compraContratacion/fsc_fss/createFSC_FSSDetalleServicio.action" 
  source="store" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="fsc_fssDetalle.fsc_fss.oid={fsc_fssDetalle.fsc_fss.oid},fsc_fssDetalle.cantidadSolicitada={fsc_fssDetalle.cantidadSolicitada},fsc_fssDetalle.servicio.oid={fsc_fssDetalle.servicio.oid},fsc_fssDetalle.productoBien.oid={fsc_fssDetalle.productoBien.oid}"/>
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancelar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=administrar,flowControl=back"/>
 