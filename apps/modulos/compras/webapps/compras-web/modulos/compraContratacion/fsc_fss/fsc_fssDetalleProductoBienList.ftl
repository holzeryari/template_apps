<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>


<@vc.anchors target="contentTrx" ajaxFlag="ajax">
	<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
		<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
			<tr>
				<td>Productos/Bienes solicitados</td>
				<td>
					<div align="right">
						<@s.a href="${request.contextPath}/compraContratacion/fsc_fss/selectProductoBien.action?fsc_fss.oid=${fsc_fss.oid?c}" templateDir="custontemplates" id="agregarProductoBien" name="agregarProductoBien" cssClass="ocultarIcono">
						<b>Agregar</b><img src="${request.contextPath}/common/images/agregar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
						</@s.a>		
					</div>
				</td>
			</tr>
		</table>			
						
		<!-- Resultado Filtro -->						
		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="fsc_fss.fsc_fssDetalleList" id="fsc_fssDetalle" defaultsort=2>	
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select"  class="botoneraAnchoCon3" title="Acciones">
				<a href="${request.contextPath}/compraContratacion/fsc_fss/readFSC_FSSDetalleView.action?fsc_fssDetalle.oid=${fsc_fssDetalle.oid?c}&navegacionIdBack=${navigationId}"><img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver" border="0"></a>
				&nbsp;
				
				<a href="${request.contextPath}/compraContratacion/fsc_fss/updateFSC_FSSDetalleView.action?fsc_fssDetalle.oid=${fsc_fssDetalle.oid?c}&navegacionIdBack=administrar&navigationId=administrar-updateDetalle&flowControl=regis"><img  src="${request.contextPath}/common/images/modificar.gif" alt="Modificar" title="Modificar" border="0"></a>
				&nbsp;
				
				<a href="${request.contextPath}/compraContratacion/fsc_fss/deleteFSC_FSSDetalleView.action?fsc_fssDetalle.oid=${fsc_fssDetalle.oid?c}"><img  src="${request.contextPath}/common/images/eliminar.gif" alt="Eliminar" title="Eliminar" border="0"></a>
							
			</@display.column>
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service"  property="numero" class="estiloNumero" title="N&uacute;mero Item" />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service"  property="productoBien.descripcion" class="estiloTexto" title="Descripci&oacute;n" />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service"  property="productoBien.marca" class="estiloTexto" title="Marca" />					
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service"  property="productoBien.modelo" class="estiloTexto" title="Modelo" />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service"  property="cantidadSolicitada" class="estiloNumero" format="{0,number,###0.00;-###0.00}" title="Cant. Solicitada" />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service"  property="cantidadRepuesta" class="estiloNumero" format="{0,number,###0.00;-###0.00}" title="Cant. Repuesta" />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service"  property="cantidadSolicitadaPendiente"  class="estiloNumero" format="{0,number,###0.00;-###0.00}" title="Cant. Solicitada Pendiente" />
		</@display.table>
	</div>	
</@vc.anchors>

