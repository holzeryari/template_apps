<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>


<@vc.anchors target="contentTrx" ajaxFlag="ajax">
	<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
		<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
			<tr>
				<td>Servicios solicitados</td>
			</tr>
		</table>	
						
		<!-- Resultado Filtro -->						
		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="fsc_fss.fsc_fssDetalleList" id="fsc_fssDetalle" defaultsort=2>	
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select"  class="botoneraAnchoCon3" title="Acciones">
				<a href="${request.contextPath}/compraContratacion/fsc_fss/readFSC_FSSDetalleView.action?fsc_fssDetalle.oid=${fsc_fssDetalle.oid?c}&navegacionIdBack=${navigationId}"><img  src="${request.contextPath}/common/images/ver.gif" alt="Ver Servicio" title="Ver Servicio" border="0"></a>
			</@display.column>
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service"  property="numero" title="N&uacute;mero Item" />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service"  property="servicio.oid" title="C&oacute;digo" />	
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service"  property="servicio.descripcion" title="Descripci&oacute;n" />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service"  property="servicio.tipo" title="Tipo" />	
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="asignado" title="Asignado" />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="cotizado" title="Cotizado" />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" title="Pedido" >
				<#if (fsc_fssDetalle.cantidadPedida >0) >
						 Si
				 <#else>
				 		 No
				</#if>
			</@display.column>	
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" title="Prestado">
			
				<#if (fsc_fssDetalle.cantidadRecibida >0) >
						 Si
				 <#else>
				 		 No
				</#if>
			</@display.column>
		</@display.table>
	</div>

	<div id="capaSeparadora" class="capaSeparadora">&nbsp;</td></tr></div>

	<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
		<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
			<tr><td>Asignaciones relacionadas</td></tr>
		</table>	
		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="fsc_fss.asignacionProveedorList" id="asignacion" defaultsort=1>
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="numero" title="Nro. Asignaci&oacute;n" />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="estado" title="Estado" />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="fechaAsignacion" format="{0,date,dd/MM/yyyy}" title="Fecha Asignaci&oacute;n" />
		</@display.table>
	</div>
</@vc.anchors>

