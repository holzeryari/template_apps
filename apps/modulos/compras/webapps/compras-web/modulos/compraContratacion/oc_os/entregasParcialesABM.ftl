<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>


<@s.hidden id="oc_osEntregaParcial.oid" name="oc_osEntregaParcial.oid"/>
<@s.hidden id="oc_osEntregaParcial.oc_os.oid" name="oc_osEntregaParcial.oc_os.oid"/>

<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
			<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos de la OC/OS</b></td>
			</tr>
		</table>

		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>
			<tr>
      			<td class="textoCampo">N&uacute;mero:</td>
      			<td class="textDato"><@s.property default="&nbsp;" escape=false value="oc_osEntregaParcial.oc_os.numero"/></td>
      			<td  class="textoCampo">Fecha: </td>
				<td class="textoDato">
				<@s.if test="oc_osEntregaParcial.oc_os.fecha != null">
				<#assign fecha = oc_osEntregaParcial.oc_os.fecha> 
				${fecha?string("dd/MM/yyyy")}	
				</@s.if>	
				<@s.else>
				&nbsp;
				</@s.else>								
				</td>	      			
			</tr>	

			<tr>
			
				<td class="textoCampo">Tipo:</td>
      			<td class="textoDato"><@s.property default="&nbsp;" escape=false value="oc_osEntregaParcial.oc_os.tipo"/></td>
      		
				<td class="textoCampo">Proveedor:</td>
      			<td class="textoDato">
      				<@s.property default="&nbsp;" escape=false value="oc_osEntregaParcial.oc_os.proveedor.detalleDePersona.razonSocial" /><@s.property default="&nbsp;" escape=false value="oc_osEntregaParcial.oc_os.proveedor.detalleDePersona.nombre" />							
					<@s.hidden id="nroProveedor" name="oc_osEntregaParcial.oc_os.proveedor.nroProveedor"/>
					<@s.hidden id="razonSocial" name="oc_osEntregaParcial.oc_os.proveedor.detalleDePersona.razonSocial"/>
					<@s.hidden id="nombre" name="oc_osEntregaParcial.oc_os.proveedor.detalleDePersona.nombre"/>
					
				</td>
    		</tr>
	    		
	    		<#--<tr>
					<td class="texto_datos"  align="right">Fecha de Entrega/Prestaci&oacute;n:</td>
	      			<td class="texto" align="left" colspan="3">
	      			<@s.if test="oc_osEntregaParcial.oc_os.fechaEntregaPrestacion != null">
						<#assign fecha = oc_osEntregaParcial.oc_os.fechaEntregaPrestacion> 
						${fecha?string("dd/MM/yyyy")}	
					</@s.if>
	      			
						
				</tr>-->
				

	    		<tr>
					<td class="textoCampo">Lugar de Entrega/Prestaci&oacute;n:</td>
	      			<td  class="textoDato"><@s.property default="&nbsp;" escape=false value="oc_osEntregaParcial.oc_os.LugarEntregaPrestacion"/>&nbsp;</td>
					
					<td class="textoCampo">&nbsp;</td>
	      			<td class="textoDato">&nbsp;</td>				
	    		</tr>
	    		
	    		<tr>
					<td class="textoCampo" >Observaciones:</td>
	      			<td  class="textoDato"><@s.property default="&nbsp;" escape=false value="oc_osEntregaParcial.oc_os.observaciones"/>&nbsp;</td>
						
					<td class="textoCampo">&nbsp;</td>
	      			<td class="textoDato">&nbsp;</td>		
	    		</tr>
	    		
	    		<tr>
					<td class="textoCampo">Estado:</td>
	      			<td  class="textoDato">
						<@s.property default="&nbsp;" escape=false value="oc_osEntregaParcial.oc_os.estado"/></td>
					</td>

					<td class="textoCampo">&nbsp;</td>
	      			<td class="textoDato">&nbsp;</td>
	    		</tr>
	    		<tr><td colspan="4" class="textoCampo">&nbsp;</td></tr>
			</table>
		</div>			
		<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos de la Entrega Parcial</b></td>
			</tr>
		</table>

		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>
			<tr>
      			<td class="textoCampo">Fecha Entrega:</td>
      			<td class="textoDato">
      			<@vc.rowCalendar 
					templateDir="custontemplates" 
					id="oc_osEntregaParcial.fecha" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="oc_osEntregaParcial.fecha" 
					title="Fecha Entrega" />
				</td>	
					<td class="textoCampo">&nbsp;</td>
	      			<td class="textoDato">&nbsp;</td>
			</tr>
			<tr>
				<td class="textoCampo">Observaciones:</td>
	      		<td  class="textoDato" colspan="3">
					<@s.textarea	
							templateDir="custontemplates" 						  
							cols="89" rows="4"	      					
							cssClass="textarea"
							id="oc_osEntregaParcial.observaciones"							 
							name="oc_osEntregaParcial.observaciones"  
							label="Observaciones"														
							 />
				</td>		
			</tr>	
			<tr><td colspan="4" class="textoCampo">&nbsp;</td></tr>
		</table>
				
	</div>						
