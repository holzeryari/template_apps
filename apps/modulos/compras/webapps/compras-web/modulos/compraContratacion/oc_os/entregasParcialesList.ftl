<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<@vc.anchors target="contentTrx" ajaxFlag="ajax">
	<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
		<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>Entregas Parciales</td>
					<td>	
						<div align="right">
							<@s.a href="${request.contextPath}/compraContratacion/oc_os/crearEntregaParcialView.action?oc_osEntregaParcial.oc_os.oid=${oc_os.oid?c}&navegacionIdBack=${navigationId}&navigationId=entregasParciales-crear" templateDir="custontemplates" id="crearEntregaParcial" name="crearEntregaParcial" cssClass="ocultarIcono">
								<b>Agregar</b><img src="${request.contextPath}/common/images/agregar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
							</@s.a>		
						</div>
					</td>
				</tr>
		</table>			

		<@s.if test="oc_osEntregaParcial.fecha != null">
			<#assign fecha = oc_osEntregaParcial.fecha> 
		</@s.if>

		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="oc_os.oc_osEntregaParcialList" id="oc_osEntregaParcial" defaultsort=2>	
						
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select"  class="botoneraAnchoCon3" title="Acciones">
		
				<a href="${request.contextPath}/compraContratacion/oc_os/updateEntregaParcialView.action?oc_osEntregaParcial.oid=${oc_osEntregaParcial.oid?c}&navegacionIdBack=${navigationId}&navigationId=administrar-updateEntregaParcial&flowControl=regis"><img  src="${request.contextPath}/common/images/modificar.gif" alt="Modificar" title="Modificar" border="0"></a>
				&nbsp;
				
				<a href="${request.contextPath}/compraContratacion/oc_os/deleteEntregaParcialView.action?oc_osEntregaParcial.oid=${oc_osEntregaParcial.oid?c}&navegacionIdBack=${navigationId}&navigationId=administrar-deleteEntregaParcial&flowControl=regis}"><img  src="${request.contextPath}/common/images/eliminar.gif" alt="Eliminar" title="Eliminar" border="0"></a>
											
			</@display.column>
	
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="numero" title="N&uacute;mero" />
		
		
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="fecha" title="Fecha" format="{0,date,dd/MM/yyyy}"/>
		 	
			
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="observaciones" title="Observaciones" />									
				
		</@display.table>
	</div>	
</@vc.anchors>

