<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>

<#assign styleLinkImprimirOC="display:none;">
<#assign styleTextoImprimirOC="">

<#assign styleLinkEnviarOC="display:none;">
<#assign styleTextoEnviarOC="">

<#assign styleLinkImprimirOS="display:none;">
<#assign styleTextoImprimirOS="">

<#assign styleLinkEnviarOS="display:none;">
<#assign styleTextoEnviarOS="">

<#if puedeImprimirOC?exists && puedeImprimirOC>
	<#assign styleLinkImprimirOC="">
	<#assign styleTextoImprimirOC="display:none;">	
</#if>

<#if puedeEnviarOC?exists && puedeEnviarOC>
	<#assign styleLinkEnviarOC="">
	<#assign styleTextoEnviarOC="display:none;">	
</#if>


<#if puedeImprimirOS?exists && puedeImprimirOS>
	<#assign styleLinkImprimirOS="">
	<#assign styleTextoImprimirOS="display:none;">	
</#if>

<#if puedeEnviarOS?exists && puedeEnviarOS>
	<#assign styleLinkEnviarOS="">
	<#assign styleTextoEnviarOS="display:none;">	
</#if>

<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
			<#if mensajeAviso?exists && (mensajeAviso.length()>0)>
				<tr>
					<td class="estiloMensajeAviso"><@s.text name="${mensajeAviso}"/></td>
				</tr>
			</#if>
			<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	
	<@s.hidden id="ocList" name="ocList"/>
	<@s.hidden id="ocStringList" name="ocStringList"/>
	<@vc.anchors target="contentTrx">	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos de la OC/OS</b></td>
			</tr>
		</table>
			
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>
		
			<tr>
	  			<td class="textoCampo">N&uacute;mero:</td>
      			<td class="textoDato">
      			<@s.property default="&nbsp;" escape=false value="oc_os.numero" />
      				      			
				</td>							
      		
      			<td class="textoCampo">Tipo:</td>
      			<td class="textoDato">
      			<@s.property default="&nbsp;" escape=false value="oc_os.tipo" />	      				
				</td>
			</tr>
      		<tr>
      			<td class="textoCampo">Proveedor:</td>
      			<td class="textoDato">
      				<@s.property default="&nbsp;" escape=false value="oc_os.proveedor.detalleDePersona.razonSocial" />
      				<@s.property default="&nbsp;" escape=false value="oc_os.proveedor.detalleDePersona.nombre" />
      				&nbsp;
				</td>						
				<td class="textoCampo">Estado:</td>
      			<td class="textoDato" colspan="3">
      			<@s.property default="&nbsp;" escape=false value="oc_os.estado" />	      			
				</td>					
			</tr>
			
				<tr>
					<td class="textoCampo">Importe Desde:</td>
	      			<td class="textoDato">
	      			<@s.property default="&nbsp;" escape=false value="importeDesde" />	      			
					</td>	
					
					<td class="textoCampo">Importe Hasta:</td>
	      			<td class="textoDato">
	      			<@s.property default="&nbsp;" escape=false value="importeHasta" />	      			
				</tr>
				
				<tr>
					<td class="textoCampo">Fecha Desde:</td>
	      			<td class="textoDato">
	      			<@s.if test="fechaDesde != null">					 
					${fechaDesde?string("dd/MM/yyyy")}	
					</@s.if>	&nbsp;	      			
					</td>							
	      		
					<td class="textoCampo">Fecha Hasta:</td>
	      			<td class="textoDato">
	      			<@s.if test="fechaHasta != null">					 
					${fechaHasta?string("dd/MM/yyyy")}	
					</@s.if>	&nbsp;	      		
				</tr>
			<tr><td class="textoCampo" colspan="4">&nbsp;</td></tr>
			</table>
		</div>
		<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
			<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>Ordenes de Compra</td>	
				</tr>
			</table>	
						
			<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="ocList" id="oc_os" defaultsort=2>	

		
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select"  class="botoneraAnchoCon3" title="Acciones">		
			<div class="alineacion">
				<a href="${request.contextPath}/compraContratacion/oc_os/readOC_OSView.action?oc_os.oid=${oc_os.oid?c}&navigationId=oc_os-visualizar&flowControl=regis&navegacionIdBack=${navigationId}"><img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver" border="0"></a>
				&nbsp;		
			</div>
			<div class="alineacion">
				<#if oc_os.proveedor.email?exists>
					<@s.hidden id="${oc_os.oid?c}" name="${oc_os.proveedor.email}"/>
					<@s.checkbox label="${oc_os.oid}" fieldValue="${oc_os.oid?c}" name="ocSelect" value="true" onclick="javascript:changeOC(this);" />
				<#else>
					<@s.hidden id="${oc_os.oid?c}" name=""/>
					<@s.checkbox label="${oc_os.oid}" fieldValue="${oc_os.oid?c}" name="ocSelect" value="true" onclick="javascript:changeOC(this);" />
				</#if>
			</div>			
	<#--		
			<a href="${request.contextPath}/maestro/proveedor/updateView.action?proveedor.nroProveedor=${oc_os.proveedor.nroProveedor?c}&navigationId=pedidoCotizacion-visualizar&flowControl=regis&navegacionIdBack=${navigationId}">			
			<img src="${request.contextPath}/common/images/modificar.gif" border="0" align="absmiddle" hspace="3" >
			</a>	-->
			<div class="alineacion">
				<@security.a 
					templateDir="custontemplates" 
					securityCode="CUF0123" 
					enabled="true" 
					cssClass="item" 
					href="${request.contextPath}/maestro/proveedor/updateView.action?proveedor.nroProveedor=${oc_os.proveedor.nroProveedor?c}&navigationId=advertenciaProveedor-modificarProveedor&flowControl=regis&navegacionIdBack=${navigationId}">
					<img  src="${request.contextPath}/common/images/modificar.gif" alt="Modificar" title="Modificar"  border="0">
				</@security.a>
			</div>	
		</@display.column>
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="numero" title="N&uacute;mero" />
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="fecha" format="{0,date,dd/MM/yyyy}"  title="Fecha" />	
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="estado" title="Estado" />	
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTextoAnchoFijo" title="Proveedor">  
			${oc_os.proveedor.detalleDePersona.razonSocial}
			
		<#if oc_os.proveedor.detalleDePersona.nombre?exists> 
				 ${oc_os.proveedor.detalleDePersona.nombre}
		</#if>
			 
		</@display.column>
		
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTextoAnchoFijo" property="proveedor.email" title="Mail Proveedor" />
		
			
		</@display.table>
	</div>
</@vc.anchors>
			
<@s.if test="ocList!=null && ocList.size()>0">
	<div id="capaLink" class="capaLink">
		<div id="linkImprimirOC" name="linkImprimirOC" style="${styleLinkImprimirOC}" class="alineacionDerecha">					
			<@s.a 
				templateDir="custontemplates"	
				cssClass="no-rewrite" 
				id="imprimirOC"
				name="imprimirOC"	
				onclick="javascript:listaOCSelectLink(this);"									
				href="${request.contextPath}/compraContratacion/oc_os/imprimirOC.action?_"
				disabled="true">
				<b id="linkImprimirOCTexto" name="linkImprimirOCTexto" >Imprimir</b>										
				<img src="${request.contextPath}/common/images/imprimir.gif" border="0" align="absmiddle" hspace="3" >
			</@s.a>	
		</div>
		<div id="textoImprimirOC" name="textoImprimirOC" style="${styleTextoImprimirOC}" class="alineacionDerecha">
				<b id="textoImprimirOCTexto" name="textoImprimirOCTexto" class="textoGris">Imprimir</b>										
				<img src="${request.contextPath}/common/images/imprimir.gif" border="0" align="absmiddle" hspace="3" >
		</div>			
				
		<div id="linkEnviarOC" name="linkEnviarOC" style="${styleLinkEnviarOC}" class="alineacionDerecha">					
					<@s.a 
						templateDir="custontemplates"
						cssClass="item" 
						id="enviarOC"
						name="enviarOC"										
						href="javascript://nop/"
						disabled="true">
						<b id="linkEnviarOCTexto" name="linkEnviarOCTexto" >Enviar</b>										
						<img src="${request.contextPath}/common/images/enviarmail.gif" border="0" align="absmiddle" hspace="3" >
					</@s.a>
		</div>
					
					  
<@vc.htmlContent 
	  baseUrl="${request.contextPath}/compraContratacion/oc_os/enviarOC.action" 
	  source="enviarOC" 
	  success="contentTrx" 
	  failure="errorTrx" 
	  preFunction="listaOCSelect" 
	  parameters="ocStringList={ocStringList}"/> 
	
		<div id="textoEnviarOC" name="textoEnviarOC" style="${styleTextoEnviarOC}" class="alineacionDerecha">
				<b id="textoEnviarOCTexto" name="textoEnviarOCTexto" class="textoGris">Enviar</b>										
				<img src="${request.contextPath}/common/images/enviarmail.gif" border="0" align="absmiddle" hspace="3" >
		</div>
	</div>			
</@s.if>

<#-- Lista de OS-->
<@s.hidden id="osList" name="osList"/>
<@s.hidden id="osStringList" name="osStringList"/>
<@vc.anchors target="contentTrx">			
	<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
			<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>Ordenes de Servicio</td>	
				</tr>
			</table>
				
		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="osList" id="oc_os" defaultsort=2>	
			
			
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select"  class="botoneraAnchoCon3" title="Acciones">		
				<div class="alineacion">
					<a href="${request.contextPath}/compraContratacion/oc_os/readOC_OSView.action?oc_os.oid=${oc_os.oid?c}&navigationId=oc_os-visualizar&flowControl=regis&navegacionIdBack=${navigationId}"><img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver" border="0"></a>
					&nbsp;
				</div>	
				
				<div class="alineacion">
					<#if oc_os.proveedor.email?exists>
						<@s.hidden id="${oc_os.oid?c}" name="${oc_os.proveedor.email}"/>
						<@s.checkbox label="${oc_os.oid}" fieldValue="${oc_os.oid}" name="osSelect" value="true" onclick="javascript:changeOS(this);" />
					<#else>
						<@s.hidden id="${oc_os.oid?c}" name=""/>
						<@s.checkbox label="${oc_os.oid}" fieldValue="${oc_os.oid}" name="osSelect" value="true" onclick="javascript:changeOS(this);" />
					</#if>
				</div>		
		<#--		
			<a href="${request.contextPath}/maestro/proveedor/updateView.action?proveedor.nroProveedor=${oc_os.proveedor.nroProveedor?c}&navigationId=pedidoCotizacion-visualizar&flowControl=regis&navegacionIdBack=${navigationId}">			
			<img src="${request.contextPath}/common/images/modificar.gif" border="0" align="absmiddle" hspace="3" >
			</a>		-->
			<div class="alineacion">
			<@security.a 
				templateDir="custontemplates" 
				securityCode="CUF0123" 
				enabled="true" 
				cssClass="item" 
				href="${request.contextPath}/maestro/proveedor/updateView.action?proveedor.nroProveedor=${oc_os.proveedor.nroProveedor?c}&navigationId=advertenciaProveedor-modificarProveedor&flowControl=regis&navegacionIdBack=${navigationId}">
				<img  src="${request.contextPath}/common/images/modificar.gif" alt="Modificar" title="Modificar"  border="0">
			</@security.a>
			</div>
			</@display.column>
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="numero" title="N&uacute;mero" />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="fecha" format="{0,date,dd/MM/yyyy}"  title="Fecha" />	
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="estado" title="Estado" />	
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTextoAnchoFijo" title="Proveedor">  
				${oc_os.proveedor.detalleDePersona.razonSocial}
				
				<#if oc_os.proveedor.detalleDePersona.nombre?exists> 
				 ${oc_os.proveedor.detalleDePersona.nombre}
			</#if>
			
			</@display.column>
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTextoAnchoFijo" property="proveedor.email" title="Mail Proveedor" />
			
			
		</@display.table>
	
	</div>	
	</@vc.anchors>
	<@s.if test="osList!=null && osList.size()>0">
			<div id="capaLink" class="capaLink">
					
					<div id="linkImprimirOS" name="linkImprimirOS" style="${styleLinkImprimirOS}" class="alineacionDerecha">					
					<@s.a 
						templateDir="custontemplates"	
						cssClass="no-rewrite" 
						id="imprimirOS"
						name="imprimirOS"	
						onclick="javascript:listaOSSelectLink(this);"									
						href="${request.contextPath}/compraContratacion/oc_os/imprimirOS.action?_"
						disabled="true">
						<b id="linkImprimirOSTexto" name="linkImprimirOSTexto" >Imprimir</b>										
						<img src="${request.contextPath}/common/images/imprimir.gif" border="0" align="absmiddle" hspace="3" >
					</@s.a>	
				</div>
				<div id="textoImprimirOS" name="textoImprimirOS" style="${styleTextoImprimirOS}" class="alineacionDerecha">
						<b id="textoImprimirOSTexto" name="textoImprimirOSTexto">Imprimir</b>										
						<img src="${request.contextPath}/common/images/imprimir.gif" border="0" align="absmiddle" hspace="3" >
				</div>			
				
				
				<div id="linkEnviarOS" name="linkEnviarOS" style="${styleLinkEnviarOS}" class="alineacionDerecha">					
					<@s.a 
						templateDir="custontemplates"
						cssClass="item" 
						id="enviarOS"
						name="enviarOS"										
						href="javascript://nop/"
						disabled="true">
						<b id="linkEnviarOSTexto" name="linkEnviarOSTexto">Enviar</b>										
						<img src="${request.contextPath}/common/images/enviarmail.gif" border="0" align="absmiddle" hspace="3" >
					</@s.a>
					
<@vc.htmlContent 
  baseUrl="${request.contextPath}/compraContratacion/oc_os/enviarOS.action" 
  source="enviarOS" 
  success="contentTrx" 
  failure="errorTrx" 
  preFunction="listaOSSelect" 
  parameters="osStringList={osStringList}"/>						
					
				</div>
				<div id="textoEnviarOS" name="textoEnviarOS" style="${styleTextoEnviarOS}" class="alineacionDerecha">
						<b id="textoEnviarOSTexto" name="textoEnviarOSTexto" class="textoGris">Enviar</b>										
						<img src="${request.contextPath}/common/images/enviarmail.gif" border="0" align="absmiddle" hspace="3" >
				</div>
			</div>
		</@s.if>
		
<div id="capaBotonera" class="capaBotonera">
		<table id="tablaBotonera" class="tablaBotonera">
			<tr> 
				<td align="left">
					<input id="cancelar" type="button" name="btnVolver" value="Volver" class="boton"/>
				</td>
			</tr>	
		</table>
</div>

  <@vc.htmlContent 
   baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancelar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=${navegacionIdBack},flowControl=back"/>
  

	  	  				

  