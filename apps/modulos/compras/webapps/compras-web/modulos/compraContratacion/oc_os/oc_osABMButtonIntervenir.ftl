<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<div id="capaSeparadora" class="capaSeparadora">&nbsp;</div>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancelar" type="button" name="btnCancelar" value="Cancelar" class="boton"/>
				<input id="intervenir" type="button" name="btnIntervenir" value="Intervenir" class="boton"/>
			</td>
		</tr>	
	</table>
</div>



<@vc.htmlContent 
  baseUrl="${request.contextPath}/compraContratacion/oc_os/intervenirOC_OS.action" 
  source="intervenir" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="oc_os.oid={oc_os.oid},observacion={observacion},tipoIntervencionOCOS={tipoIntervencionOCOS},navegacionIdBack=${navegacionIdBack}"/>
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancelar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=${navegacionIdBack},flowControl=back"/>
  