<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign tiles=JspTaglibs["/WEB-INF/tlds/tiles-jsp.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<@s.hidden id="oc_osDetalle.oid" name="oc_osDetalle.oid"/>
<@s.hidden id="oc_osDetalle.servicio.oid" name="oc_osDetalle.servicio.oid"/>
<@s.hidden id="oc_osDetalle.productoBien.oid" name="oc_osDetalle.productoBien.oid"/>
<@s.hidden id="oc_osDetalle.oc_os.oid" name="oc_osDetalle.oc_os.oid"/>

<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
			<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos de la OC/OS</b></td>
			</tr>
		</table>
	
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>
				
			<tr>
      			<td class="textoCampo">N&uacute;mero:</td>
      			<td class="textoDato"><@s.property default="&nbsp;" escape=false value="oc_osDetalle.oc_os.numero"/></td>
      			<td  class="textoCampo">Fecha: </td>
				<td class="textoDato">
				<@s.if test="oc_osDetalle.oc_os.fecha != null">
				<#assign fecha = oc_osDetalle.oc_os.fecha> 
				${fecha?string("dd/MM/yyyy")}	
				</@s.if>	
				<@s.else>
				&nbsp;
				</@s.else>
												
				</td>	      			
			</tr>	

			<tr>
				<td class="textoCampo">Tipo:</td>
      			<td class="textoDato"><@s.property default="&nbsp;" escape=false value="oc_osDetalle.oc_os.tipo"/></td>
      		
				<td class="textoCampo">Proveedor:</td>
      			<td class="textoDato">
      				<@s.property default="&nbsp;" escape=false value="oc_osDetalle.oc_os.proveedor.detalleDePersona.razonSocial" /><@s.property default="&nbsp;" escape=false value="oc_osDetalle.oc_os.proveedor.detalleDePersona.nombre" />							
					<@s.hidden id="nroProveedor" name="oc_osDetalle.oc_os.proveedor.nroProveedor"/>
					<@s.hidden id="razonSocial" name="oc_osDetalle.oc_os.proveedor.detalleDePersona.razonSocial"/>
					<@s.hidden id="nombre" name="oc_osDetalle.oc_os.proveedor.detalleDePersona.nombre"/>
				</td>
    		</tr>
			<tr>
				<td class="textoCampo">Lugar de Entrega/Prestaci&oacute;n:</td>
      			<td  class="textoDato" colspan="3"><@s.property default="&nbsp;" escape=false value="oc_osDetalle.oc_os.LugarEntregaPrestacion"/></td>
			</tr>
	    		
    		<tr>
				<td class="textoCampo">Observaciones:</td>
      			<td  class="textoDato" colspan="3"><@s.property default="&nbsp;" escape=false value="oc_osDetalle.oc_os.observaciones"/></td>
			</tr>
	    		
    		<tr>
				<td class="textoCampo">Estado:</td>
      			<td  class="textoDato">
					<@s.property default="&nbsp;" escape=false value="oc_osDetalle.oc_os.estado"/></td>
				</td>

				<td class="textoCampo">&nbsp;</td>
      			<td class="textoDato">&nbsp;</td>
    		</tr>
    		<tr><td colspan="4" class="textoCampo">&nbsp;</td></tr>
		</table>	
	</div>	
	<@s.if test="oc_osDetalle.productoBien != null">
		<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos del Producto/Bien</b></td>
			</tr>
		</table>
	
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>	
			<tr>
      			<td class="textoCampo">C&oacute;digo:</td>
      			<td class="textoDato">
      			<@s.property default="&nbsp;" escape=false value="oc_osDetalle.productoBien.oid"/>
      			</td>
      			<td  class="textoCampo">Descripci&oacute;n: </td>
				<td class="textoDato">
				<@s.property default="&nbsp;" escape=false value="oc_osDetalle.productoBien.descripcion"/>		
				</td>	      			
			</tr>	
			<tr>
      			<td class="textoCampo">Tipo:</td>
	      		<td class="textoDato">
	      			<@s.property default="&nbsp;" escape=false value="oc_osDetalle.productoBien.tipo"/>						      			
	      		</td>		      		
										
				<td class="textoCampo">Rubro:</td>
	  			<td class="textoDato">
	  				<@s.property default="&nbsp;" escape=false value="oc_osDetalle.productoBien.rubro.descripcion"/>
				</td>
    		</tr>
			<tr>
				<td class="textoCampo">Marca: </td>
				<td  class="textoDato">
					<@s.property default="&nbsp;" escape=false value="oc_osDetalle.productoBien.marca"/>										
				</td>
				<td class="textoCampo">Modelo: </td>
				<td  class="textoDato">
					<@s.property default="&nbsp;" escape=false value="oc_osDetalle.productoBien.modelo"/>										
				</td>
    		</tr>
    		<tr>
				<td class="textoCampo">Valor Mercado: </td>
				<td  class="textoDato">
					<@s.property default="&nbsp;" escape=false value="bien.valorMercado"/>
				</td>
				<td class="textoCampo">Fecha Valor Mercado: </td>
				<td  class="textoDato">
					<@s.property default="&nbsp;" escape=false value="bien.fechaValorMercado"/>
				</td>
    		</tr>
			<tr>
				<td class="textoCampo">C&oacute;digo de Barra: </td>
				<td  class="textoDato">
					<@s.property default="&nbsp;" escape=false value="oc_osDetalle.productoBien.codigoBarra"/>										
				</td>
				<td class="textoCampo">Es Cr&iacute;tico: </td>
				<td  class="textoDato">
					<@s.property default="&nbsp;" escape=false value="oc_osDetalle.productoBien.critico"/>						      			
				</td>
    		</tr>
    		<tr>
				<td class="textoCampo">Reposici&oacute;n Autm&aacute;tica: </td>
				<td  class="textoDato">	
					<@s.property default="&nbsp;" escape=false value="producto.reposicionAutomatica"/>						      											
				</td>
				<td class="textoCampo">Existencia M&iacute;nima: </td>
				<td  class="textoDato">
					<@s.property default="&nbsp;" escape=false value="producto.existenciaMinima"/>
				</td>
    		</tr>
			<tr>
    		<td class="textoCampo">Tipo Unidad:</td>
    		<td  class="textoDato" colspan="3">
					<@s.property default="&nbsp;" escape=false value="producto.tipoUnidad.codigo"/>
				</td>
    		</tr>
			<tr>
				<td class="textoCampo">Es Registrable: </td>
				<td  class="textoDato" colspan="3">
				<@s.property default="&nbsp;" escape=false value="bien.registrable"/>						      			
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Es Veh&iacute;culo: </td>
				<td  class="textoDato" colspan="3">
					<@s.property default="&nbsp;" escape=false value="bien.vehiculo"/>
				</td>
    		</tr>							
			<tr>
				<td class="textoCampo">Estado: </td>
      			<td class="textoDato" colspan="3"><@s.property default="&nbsp;" escape=false value="productoBien.estado"/></td>
    		</tr>
					    		
    		<#if oc_osDetalle.producto?exists>
				<tr>
					<td class="textoCampo">Existencia: </td>
					<td class="textoDato" colspan="3">
					${oc_osDetalle.producto.cantidadTotalExistencia}
					</td>
				</tr>
			</#if>
					    		
    		<tr>
				<td class="textoCampo">Cantidad Solicitada: </td>
				<td  class="textoDato">
					<@s.textfield									
						templateDir="custontemplates"
						template="textMoney"  
						id="oc_osDetalle.cantidadPedida" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="oc_osDetalle.cantidadPedida" 
						title="Cantidad Pedida" />							      			
				</td>
				
				<td class="textoCampo">Precio: </td>
				<td  class="textoDato">
					<@s.textfield									
						templateDir="custontemplates"
						template="textMoney"  
						id="oc_osDetalle.precio" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="oc_osDetalle.precio" 
						title="Precio" />							      			
				</td>
			</tr>
			<tr>
				<td  class="textoCampo">Al&iacute;cuota IVA: </td>
				<td class="textoDato" >
				<@s.textfield	      								
					templateDir="custontemplates"
					template="textMoney"  
					id="oc_osDetalle.alicuotaIva" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="oc_osDetalle.alicuotaIva" 
					title="Al&iacute;cuota IVA" />
				</td>   					<td class="textoCampo">&nbsp;</td>
	      			<td class="textoDato">&nbsp;</td>	
			</tr>
			<tr><td class="textoCampo" colspan="4">&nbsp;</td></tr>
			
		</table>
	</div>			
	<div id="capaSeparadora" class="capaSeparadora">&nbsp;</div>
	<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
		<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td><b>OC con entregas pendientes del producto seleccionado</b></td>
			</tr>
		</table>
				
		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="listaOC" id="oc_osDetalle" defaultsort=2 >          		
  		
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service"  class="estiloNumero" property="oc_os.numero" title="N&uacute;mero" />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service"  class="estiloNumero" property="oc_os.fecha" format="{0,date,dd/MM/yyyy}"  title="Fecha"/>					
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" title="Proveedor">  
				${oc_osDetalle.oc_os.proveedor.detalleDePersona.razonSocial} 
				
				<#if oc_osDetalle.oc_os.proveedor.detalleDePersona.nombre?exists> 
		 			${oc_osDetalle.oc_os.proveedor.detalleDePersona.nombre}
				</#if>
				
			</@display.column> 
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" property="oc_os.importe" class="estiloNumero" title="Importe" />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service"  title="Cant. Pendiente Entrega" class="estiloNumero">
				<#assign resta=(oc_osDetalle.cantidadPedida - oc_osDetalle.cantidadRecibida) >
				${resta}
			</@display.column>
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service"  class="estiloNumero" title="Ultima Fecha Entrega">
			
			<#if oc_osDetalle.oc_os.ultimaFechaEngrega?exists>
			
				<#assign fechaOC = oc_osDetalle.oc_os.ultimaFechaEngrega> 
				${fechaOC?string("dd/MM/yyyy")}		
			</#if>
			</@display.column>


		</@display.table>
	</div>
	</@s.if>
			
	<@s.if test="oc_osDetalle.servicio != null">
		<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos del Servicio</b></td>
			</tr>
		</table>
	
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>			
			<tr>
      			<td class="textoCampo">C&oacute;digo:</td>
      			<td class="textoDato">
	      			<@s.property default="&nbsp;" escape=false value="oc_osDetalle.servicio.oid"/>			      			
      			<td class="textoCampo">Descripci&oacute;n</td>
				<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="oc_osDetalle.servicio.descripcion"/>			
			</tr>	
			<tr>
				<td class="textoCampo">Rubro:</td>
      			<td class="textoDato">
      				<@s.property default="&nbsp;" escape=false value="oc_osDetalle.servicio.rubro.descripcion"/>
			     </td>
				
				<td class="textoCampo">Cr&iacute;tico: </td>
				<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="oc_osDetalle.servicio.critico"/>								
				</td>
			</tr>
		
			<tr>
      			<td class="textoCampo">Proveedor &Uacute;nico:</td>
				<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="oc_osDetalle.servicio.proveedorUnico"/>
				</td>
				<td class="textoCampo">Proveedor:</td>
				<td class="textoDato">&nbsp;						
				
				</td>
    		</tr>
			<tr>			
				<td class="textoCampo">Tipo:</td>
	      		<td class="textoDato">
	     			<@s.property default="&nbsp;" escape=false value="oc_osDetalle.servicio.tipo"/>
	      		</td>				      		
				<td class="textoCampo">Estado:</td>
				<td class="textoDato" colspan="3">
					<@s.property default="&nbsp;" escape=false value="oc_osDetalle.servicio.estado"/>
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Precio: </td>
					<td  class="textoDato">
						<@s.textfield									
							templateDir="custontemplates"
							template="textMoney"  
							id="oc_osDetalle.precio" 
							cssClass="textarea"
							cssStyle="width:160px" 
							name="oc_osDetalle.precio" 
							title="Precio" />							      			
					</td>	
					<td  class="textoCampo">Al&iacute;cuota IVA: </td>
					<td class="textoDato">
						<@s.textfield	      								
  								templateDir="custontemplates"
								template="textMoney"  
								id="oc_osDetalle.alicuotaIva" 
								cssClass="textarea"
								cssStyle="width:160px" 
								name="oc_osDetalle.alicuotaIva" 
								title="Al&iacute;cuota IVA" />
				</td>   	
			</tr>
			
			<tr>
								<td class="textoCampo">&nbsp;</td>
	      			<td class="textoDato">&nbsp;</td>		
	      								<td class="textoCampo">&nbsp;</td>
	      			<td class="textoDato">&nbsp;</td>		
			
		</table>
	</div>
	</@s.if> 