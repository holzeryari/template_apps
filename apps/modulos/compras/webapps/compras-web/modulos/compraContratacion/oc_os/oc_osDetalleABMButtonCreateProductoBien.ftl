<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<div id="capaSeparadora" class="capaSeparadora">&nbsp;</div>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancelar" type="button" name="btnCancelar" value="Cancelar" class="boton"/>
				<input id="agregar" type="button" name="btnAgregar" value="Agregar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>


<@vc.htmlContent 
  baseUrl="${request.contextPath}/compraContratacion/oc_os/createOC_OSDetalleProductoBien.action" 
  source="agregar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="oc_osDetalle.oc_os.oid={oc_osDetalle.oc_os.oid},oc_osDetalle.cantidadPedida={oc_osDetalle.cantidadPedida},oc_osDetalle.precio={oc_osDetalle.precio},oc_osDetalle.productoBien.oid={oc_osDetalle.productoBien.oid},oc_osDetalle.servicio.oid={oc_osDetalle.servicio.oid},oc_osDetalle.alicuotaIva={oc_osDetalle.alicuotaIva}"/>
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancelar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=administrar-oc_os,flowControl=back"/>
 