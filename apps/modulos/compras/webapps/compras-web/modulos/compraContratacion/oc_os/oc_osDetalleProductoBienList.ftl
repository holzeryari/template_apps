<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>

<@vc.anchors target="contentTrx" ajaxFlag="ajax">
	<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
		<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
			<tr>
				<td>Productos/Bienes a comprar</td>
				<td>
					<div align="right">
						<@s.a href="${request.contextPath}/compraContratacion/oc_os/selectProductoBien.action?oc_os.oid=${oc_os.oid?c}&oc_os.proveedor.nroProveedor=${oc_os.proveedor.nroProveedor?c}&navigationId=seleccionar-productoBien" templateDir="custontemplates" id="agregarProductoBien" name="agregarProductoBien" cssClass="ocultarIcono">
							<b>Agregar</b><img src="${request.contextPath}/common/images/agregar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
						</@s.a>					
					</div>
				</td>
			</tr>
		</table>	
		
			
		<!-- Resultado Filtro -->						
		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="oc_os.oc_osDetalleList" id="oc_osDetalle" defaultsort=2>	
						
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select"  class="botoneraAnchoCon3" title="Acciones">
			
					
			<a href="${request.contextPath}/compraContratacion/oc_os/readOC_OSDetalleView.action?oc_osDetalle.oid=${oc_osDetalle.oid?c}&navegacionIdBack=administrar-oc_os&navigationId=administrar-readDetalle&flowControl=regis"><img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver" border="0"></a>
			&nbsp;
			
			<a href="${request.contextPath}/compraContratacion/oc_os/updateOC_OSDetalleView.action?oc_osDetalle.oid=${oc_osDetalle.oid?c}&navegacionIdBack=administrar-oc_os&navigationId=administrar-updateDetalle&flowControl=regis"><img  src="${request.contextPath}/common/images/modificar.gif" alt="Modificar" title="Modificar" border="0"></a>
			&nbsp;
			
			<a href="${request.contextPath}/compraContratacion/oc_os/deleteOC_OSDetalleView.action?oc_osDetalle.oid=${oc_osDetalle.oid?c}&navegacionIdBack=administrar-oc_os&navigationId=administrar-deleteDetalle&flowControl=regis"><img  src="${request.contextPath}/common/images/eliminar.gif" alt="Eliminar" title="Eliminar" border="0"></a>
										
		</@display.column>
		
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="numero" title="N&uacute;mero" />
	
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="productoBien.oid" title="C&oacute;digo" />
	 	
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="productoBien.descripcion" title="Descripci&oacute;n" />									
			
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="productoBien.marca" title="Marca" />
		
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="productoBien.modelo" title="Modelo" />
		
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="productoBien.rubro.descripcion" title="Rubro" />
	
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="cantidadPedida" title="Cantidad" />			
	
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" format="{0,number,###0.00;-###0.00}"  property="precio" title="Precio" />
	
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" format="{0,number,###0.00;-###0.00}"  property="subtotal" title="Subtotal" />

		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" format="{0,number,###0.00;-###0.00}"  property="alicuotaIva" title="Alicuota IVA" />

		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" format="{0,number,###0.00;-###0.00}"  property="subTotalIva" title="Subtotal IVA" />

		<@display.footer>
			<tr>
				<td colspan=11 class="estiloNumero"><b>Total:</b></td>
				<td class="estiloNumero"><b>${oc_os.totalIva?string("###0.00;-###0.00")}</b></td>
			<tr>
		</@display.footer>
	</@display.table>
	</div>	
</@vc.anchors>



