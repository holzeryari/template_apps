<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign tiles=JspTaglibs["/WEB-INF/tlds/tiles-jsp.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<@s.hidden id="oc_os.oid" name="oc_os.oid"/>
<@s.hidden id="oc_os.versionNumber" name="oc_os.versionNumber"/>

<#if (tipoIntervencionOCOS?exists && tipoIntervencionOCOS.ordinal()>0)>
<@s.hidden id="tipoIntervencionOCOS" name="tipoIntervencionOCOS.ordinal()"/>
</#if>
	<div id="capaSeparadora" class="capaSeparadora">&nbsp;</div>
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos de la Intervenci&oacute;n</b></td>
			</tr>
		</table>
	
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>
			<tr>
				<td class="textoCampo">Tipo Intervencion:</td>
      			<td  class="textoDato" >
      			<@s.select 
							templateDir="custontemplates" 
							id="tipoIntervencionOCOS" 
							cssClass="textarea"
							cssStyle="width:180px" 
							name="tipoIntervencionOCOS" 
							list="tipoIntervencionList" 
							listKey="key" 
							listValue="description" 
							value="tipoIntervencionOCOS.ordinal()"
							title="Tipo de Intervencion OCOS"
							headerKey="0"
							headerValue="Todos"							
							/>	</td>
												<td class="textoCampo">&nbsp;</td>
	      			<td class="textoDato">&nbsp;</td>					
    		</tr>
      		<tr>
				<td class="textoCampo">Observaciones:</td>
      			<td  class="textoDato" colspan="3">
				<@s.textarea	
						templateDir="custontemplates" 						  
						cols="89" rows="4"	      					
						cssClass="textarea"
						id="observaciont"							 
						name="observacion"  
						label="Observaciones"														
						 />
				</td>					
    		</tr>
		</table>	
	</div>




