
<#setting locale="es_AR">

<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>

<@s.hidden id="navigationId" name="navigationId"/>
<#assign claseEstado="textarea">		
<#assign disabledEstado="false">
<#assign clasePendienteRecepcionVencido="textareagris">		
<#assign disabledPendienteRecepcionVencido="true">		
<@s.if test="pendienteRecepcion!=null && pendienteRecepcion.ordinal()>0">
	<#assign claseEstado="textareagris">		
	<#assign disabledEstado="true">
</@s.if>
<@s.if test="pendienteRecepcion!=null && pendienteRecepcion.ordinal()>1">
	<#assign clasePendienteRecepcionVencido="textarea">		
	<#assign disabledPendienteRecepcionVencido="false">
</@s.if>
<@s.if test="pendienteFacturacion!=null && pendienteFacturacion.ordinal()>0">
	<#assign claseEstado="textareagris">		
	<#assign disabledEstado="true">
</@s.if>
		
<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
        	<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
			<tr>
				<td><b>Datos de la OC/OS</b></td>
				<td>
					<div align="right">
					<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0381" 
							enabled="true" 
							cssClass="item" 
							id="crear"										
							href="javascript://nop/">
							<b>Agregar</b>										
							<img src="${request.contextPath}/common/images/agregar.gif"  border="0" align="absmiddle" hspace="3" >
						</@security.a>						
					</div>
				</td>
			</tr>
		</table>
			
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
	    		<td class="textoCampo" colspan="4">&nbsp;</td>
	    	</tr>
	    	<tr>
				<td class="textoCampo">Producto/Bien:</td>
				<td class="textoDato">
				<@s.hidden id="productoBien.oid" name="productoBien.oid"/>
				<@s.hidden id="productoBien.descripcion" name="productoBien.descripcion"/>
				<@s.property default="&nbsp;" escape=false value="productoBien.descripcion"/>					
					<@s.a href="javascript://nop/"
						templateDir="custontemplates" id="seleccionarProductoBien" name="seleccionarProductoBien" cssClass="ocultarIcono">
						<img src="${request.contextPath}/common/images/buscar.gif" border="0" align="absmiddle" hspace="3" >
					</@s.a>	
				</td>
				<td class="textoCampo">Cliente:</td>
				<td class="textoDato">
				<@s.hidden id="oc_os.cliente.oid" name="oc_os.cliente.oid"/>
				<@s.hidden id="oc_os.cliente.descripcion" name="oc_os.cliente.descripcion"/>
				<@s.property default="&nbsp;" escape=false value="oc_os.cliente.descripcion"/>								
					<@s.a templateDir="custontemplates" id="seleccionarCliente" name="seleccionarCliente" href="javascript://nop/" cssClass="ocultarIcono">
						<img src="${request.contextPath}/common/images/buscar.gif" border="0" align="absmiddle" hspace="3" >
					</@s.a>		
				</td>
			</tr>	
			<tr>
	  			<td class="textoCampo">N&uacute;mero:</td>
      			<td class="textoDato">
      			<@s.textfield 
      				templateDir="custontemplates" 
					id="oc_os.numero" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="oc_os.numero" 
					title="Numero" />
				</td>							
      		
      			<td class="textoCampo">Tipo:</td>
      			<td class="textoDato">
      			<@s.select 
						templateDir="custontemplates" 
						id="oc_os.tipo" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="oc_os.tipo" 
						list="tipoList" 
						listKey="key" 
						listValue="description" 
						value="oc_os.tipo.ordinal()"
						title="Tipo"
						headerKey="0"
						headerValue="Todos"/>
				</td>
			</tr>
      		<tr>
      			<td class="textoCampo">Proveedor:</td>
      			<td class="textoDato">
      			<@s.property default="&nbsp;" escape=false value="oc_os.proveedor.detalleDePersona.razonSocial" />
      			<@s.property default="&nbsp;" escape=false value="oc_os.proveedor.detalleDePersona.nombre" />
      								
				<@s.hidden id="nroProveedor" name="oc_os.proveedor.nroProveedor"/>
				<@s.hidden id="razonSocial" name="oc_os.proveedor.detalleDePersona.razonSocial"/>
				<@s.hidden id="nombre" name="oc_os.proveedor.detalleDePersona.nombre"/>
				<@s.a templateDir="custontemplates" id="seleccionarProveedor" name="seleccionarProveedor" href="javascript://nop/" title="Buscar Proveedor">
					<img src="${request.contextPath}/common/images/buscar.gif"  border="0" align="absmiddle" hspace="3" >
				</@s.a>		
				</td>	
				
				<td class="textoCampo">Estado:</td>
      			<td class="textoDato" >
      			<@s.select 
						templateDir="custontemplates" 
						id="oc_os.estado" 
						cssClass="${claseEstado}"
						disabled="${disabledEstado}"
						cssStyle="width:165px" 
						name="oc_os.estado" 
						list="estadoList" 
						listKey="key" 
						listValue="description" 
						value="oc_os.estado.ordinal()"
						title="Estado"
						headerKey="0"
						headerValue="Todos"							
						/>
				</td>
				
			</tr>
				
			<tr>
				<td class="textoCampo">Importe Desde:</td>
      			<td class="textoDato" >
      			<@s.textfield 
					templateDir="custontemplates" 
					id="importeDesde" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="importeDesde" 
					title="Importe Desde" />
				</td>	
				
				<td class="textoCampo">Importe Hasta:</td>
      			<td class="textoDato">
      			<@s.textfield 
					templateDir="custontemplates" 
					id="importeHasta" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="importeHasta" 
					title="Importe Hasta" />
				</td>
			</tr>
				
			<tr>
				<td class="textoCampo">Fecha Desde:</td>
      			<td class="textoDato">
      			<@vc.rowCalendar 
					templateDir="custontemplates" 
					id="fechaDesde" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="fechaDesde" 
					title="Fecha Desde" />
				</td>							
      		
				<td class="textoCampo">Fecha Hasta:</td>
      			<td class="textoDato" align="left" width="30%">
      			<@vc.rowCalendar 
					templateDir="custontemplates" 
					id="fechaHasta" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="fechaHasta" 
					title="Fecha Hasta" />
			</tr>
			<tr>
				<td class="textoCampo">Pendiente Recepci&oacute;n:</td>
      			<td class="textoDato">
      			<@s.select 
						templateDir="custontemplates" 
						id="pendienteRecepcion" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="pendienteRecepcion" 
						list="pendienteRecepcionList" 
						listKey="key" 
						listValue="description" 
						value="pendienteRecepcion.ordinal()"
						title="Pendiente Recepcion"
						onchange="javascript:pendienteSelectOCOS(this);"							
						headerKey="0"
						headerValue="Todos"							
						/>
				</td>
				<td class="textoCampo">Pendiente Recepci&oacute;n Vencido:</td>
      			<td class="textoDato">
      			<@s.select 
						templateDir="custontemplates" 
						id="pendienteRecepcionVencido" 
						cssStyle="width:165px"
						cssClass="${clasePendienteRecepcionVencido}"
						disabled="${disabledPendienteRecepcionVencido}" 
						name="pendienteRecepcionVencido" 
						list="pendienteRecepcionVencidoList" 
						listKey="key" 
						listValue="description" 
						value="pendienteRecepcionVencido.ordinal()"
						title="Pendiente Recepcion Vencido"
						headerKey="0"
						headerValue="Todos"					
						/>
				</td>
			</tr>
			<tr>	
				
				<td class="textoCampo">Pendiente de Facturaci&oacute;n:</td>
      			<td class="textoDato">
      			<@s.select 
						templateDir="custontemplates" 
						id="pendienteFacturacion" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="pendienteFacturacion" 
						list="pendienteFacturacionList" 
						listKey="key" 
						listValue="description" 
						value="pendienteFacturacion.ordinal()"
						title="Pendiente Facturacion"
						onchange="javascript:pendienteSelectOCOS(this);"
						headerKey="0"
						headerValue="Todos"							
						/>
				</td>
				<td class="textoCampo">Incluye Producto/Bien cr&iacute;tico:</td>
      			<td class="textoDato">
      			<@s.select 
						templateDir="custontemplates" 
						id="incluyePBCritico" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="incluyePBCritico" 
						list="incluyePBCriticoList" 
						listKey="key" 
						listValue="description" 
						value="incluyePBCritico.ordinal()"
						title="Incluye Producto/Bien critico"
						headerKey="0"
						headerValue="Todos"							
						/>
				</td>
			</tr>
			<tr>
	    			<td colspan="4" class="lineaGris" align="right">
	      			<input id="limpiar" type="button" name="btnVolver" value="Limpiar" class="boton"/>		
	      			<input id="buscar" type="button" name="btnVolver" value="Buscar" class="boton"/>
	    			</td>
				</tr>	
			</table>
		</div>	
			
		
            <!-- Resultado Filtro -->
			
			<@s.if test="oc_osList!=null">
				<#assign oc_osNumero = "0" />
				<@s.if test="oc_os.numero != null">
					<#assign oc_osNumero = "${oc_os.numero}" />
				</@s.if>

				<#assign oc_osEstado = "0" />
				<@s.if test="oc_os.estado != null">
						<#assign oc_osEstado = "${oc_os.estado.ordinal()}" />
				</@s.if>

				<#assign oc_osFechaDesde = "" />
				<@s.if test="fechaDesde != null">
					<#assign oc_osFechaDesde = "${fechaDesde?string('dd/MM/yyyy')}" />
				</@s.if>

				<#assign oc_osFechaHasta = "" />
				<@s.if test="fechaHasta != null">
					<#assign oc_osFechaHasta = "${fechaHasta?string('dd/MM/yyyy')}" />
				</@s.if>
				
				<#assign oc_osImporteDesde = "" />
				<@s.if test="importeDesde != null">
					<#assign oc_osImporteDesde = "${importeDesde}" />
				</@s.if>

				<#assign oc_osImporteHasta = "" />
				<@s.if test="importeHasta != null">
					<#assign oc_osImporteHasta = "${importeHasta}" />
				</@s.if>
												
				<#assign oc_osRazonSocialProveedor = "" />
				<@s.if test=" oc_os.proveedor!= null && oc_os.proveedor.detalleDePersona.razonSocia!=null">
					<#assign oc_osRazonSocialProveedor = "${oc_os.proveedor.detalleDePersona.razonSocial}" />
				</@s.if>
				
				<#assign oc_osNombreProveedor = "" />
				<@s.if test=" oc_os.proveedor!= null && oc_os.proveedor.detalleDePersona.nombre!=null">
					<#assign oc_osNombreProveedor = "${oc_os.proveedor.detalleDePersona.nombre}" />
				</@s.if>
				
				<#assign oc_osPendienteRecepcion = "0" />
				<@s.if test="pendienteRecepcion != null">
						<#assign oc_osPendienteRecepcion = "${pendienteRecepcion.ordinal()}" />
				</@s.if>
				<#assign oc_osIncluyePBCritico = "0" />
				<@s.if test="incluyePBCritico != null">
						<#assign oc_osIncluyePBCritico = "${incluyePBCritico.ordinal()}" />
				</@s.if>
				<#assign oc_osPendienteFacturacion = "0" />
				<@s.if test="pendienteFacturacion != null">
						<#assign oc_osPendienteFacturacion = "${pendienteFacturacion.ordinal()}" />
				</@s.if>
				<#assign oc_osPendienteRecepcionVencido = "0" />
				<@s.if test="pendienteRecepcionVencido != null">
						<#assign oc_osPendienteRecepcionVencido = "${pendienteRecepcionVencido.ordinal()}" />
				</@s.if>
				
				<#assign productoBienDescripcion = "" />
				<@s.if test=" productoBien!= null && productoBien.descripcion !=null && productoBien.descripcion!=''">
					<#assign productoBienDescripcion = "${productoBien.descripcion}" />
				</@s.if>
				
				<#assign productoBienOid = "0" />
				<@s.if test=" productoBien!= null && productoBien.oid !=null">
					<#assign productoBienOid = "${productoBien.oid?c}" />
				</@s.if>
				
				<#assign fsc_fssClienteDescripcion = "" />
				<@s.if test=" fsc_fss.cliente!= null && fsc_fss.cliente.descripcion !=null && fsc_fss.cliente.descripcion!=''">
					<#assign fsc_fssClienteDescripcion = "${fsc_fss.cliente.descripcion}" />
				</@s.if>
				
				<#assign fsc_fssClienteOid = "0" />
				<@s.if test=" fsc_fss.cliente!= null && fsc_fss.cliente.oid !=null">
					<#assign fsc_fssClienteOid = "${fsc_fss.cliente.oid?c}" />
				</@s.if>
				
				<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
				<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>OC/OS encontradas</td>
					<td>
						<@vc.anchors target="contentTrx">	
						<div class="alineacionDerecha">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0382" 
							enabled="true" 
							cssClass="item" 
							id="imprimirAutorizadas"
							name="imprimirAutorizadas"												
							href="javascript://nop/">																				
							<img src="${request.contextPath}/common/images/enviarImprimir.gif" title="Imprimir/Enviar mail OC/OS Autorizadas" align="absmiddle" border="0" hspace="1" >
						</@security.a>	
						</div>
						</@vc.anchors>
						
						<div class="alineacionDerecha">
							<@security.a 
								templateDir="custontemplates" 
								securityCode="CUF0382" 
								cssClass="item" 
								href="${request.contextPath}/compraContratacion/oc_os/printXLS.action?oc_os.numero=${oc_osNumero}&oc_os.estado=${oc_osEstado}&fecha=${oc_osFechaDesde}&fechaHasta=${oc_osFechaHasta}&importeDesde=${oc_osImporteDesde}&importeHasta=${oc_osImporteHasta}&oc_os.tipo=${oc_os.tipo.ordinal()}&oc_os.proveedor.detalleDePersona.razonSocial=${oc_osRazonSocialProveedor}&oc_os.proveedor.detalleDePersona.nombre=${oc_osNombreProveedor}&pendienteRecepcion=${oc_osPendienteRecepcion}&pendienteFacturacion=${oc_osPendienteFacturacion}&pendienteRecepcionVencido=${oc_osPendienteRecepcionVencido}&productoBien.descripcion=${productoBienDescripcion}&productoBien.oid=${productoBienOid}&fsc_fss.cliente.descripcion=${fsc_fssClienteDescripcion}&fsc_fss.cliente.oid=${fsc_fssClienteOid}&incluyePBCritico={incluyePBCritico}">
								<img src="${request.contextPath}/common/images/imprimirExcel.gif" title="Exportar a Excel" align="absmiddle" border="0" hspace="1">
							</@security.a>
						</div>
						<div class="alineacionDerecha">
							<b>Imprimir</b>
							<@security.a 
								templateDir="custontemplates" 
								securityCode="CUF0382" 
								cssClass="item" 
								href="${request.contextPath}/compraContratacion/oc_os/printPDF.action?oc_os.numero=${oc_osNumero}&oc_os.estado=${oc_osEstado}&fechaDesde=${oc_osFechaDesde}&fechaHasta=${oc_osFechaHasta}&importeDesde=${oc_osImporteDesde}&importeHasta=${oc_osImporteHasta}&oc_os.tipo=${oc_os.tipo.ordinal()}&oc_os.proveedor.detalleDePersona.razonSocial=${oc_osRazonSocialProveedor}&oc_os.proveedor.detalleDePersona.nombre=${oc_osNombreProveedor}&pendienteRecepcion=${oc_osPendienteRecepcion}&pendienteFacturacion=${oc_osPendienteFacturacion}&pendienteRecepcionVencido=${oc_osPendienteRecepcionVencido}&productoBien.descripcion=${productoBienDescripcion}&productoBien.oid=${productoBienOid}&fsc_fss.cliente.descripcion=${fsc_fssClienteDescripcion}&fsc_fss.cliente.oid=${fsc_fssClienteOid}&incluyePBCritico={incluyePBCritico}">
								<img src="${request.contextPath}/common/images/imprimirPdf.gif" title="Exportar a PDF" align="absmiddle" border="0" hspace="1" >
							</@security.a>
						</div>	
					</td>
				</tr>
			</table>	
				
			<@vc.anchors target="contentTrx">	
				<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="oc_osList" id="oc_os" pagesize=15 defaultsort=3 decorator="ar.com.riouruguay.web.actions.compraContratacion.oc_os.decorators.OC_OSDecorator" partialList=true size="recordSize" keepStatus=true excludedParams="resetFlag">
          		
  		
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select" class="botoneraAnchoCon6" title="Acciones">
						<div class="alineacionMenor">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0383" 
							enabled="oc_os.readable" 
							cssClass="item" 
							href="${request.contextPath}/compraContratacion/oc_os/readOC_OSView.action?oc_os.oid=${oc_os.oid?c}&navigationId=oc_os-visualizar&flowControl=regis&navegacionIdBack=${navigationId}">
							<img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver"  border="0">
						</@security.a>
						</div>
						
						<div class="alineacionMenor">	
						<#-- Estado En Preparacion -->
						<#if oc_os.estado.ordinal() == 1>
							<@security.a 
								templateDir="custontemplates" 
								securityCode="CUF0384" 
								enabled="oc_os.updatable"
								cssClass="item"  
								href="${request.contextPath}/compraContratacion/oc_os/administrarOC_OSView.action?oc_os.oid=${oc_os.oid?c}&navigationId=administrar-oc_os&flowControl=regis&navegacionIdBack=${navigationId}">
								<img  src="${request.contextPath}/common/images/administrar.gif" alt="Administrar" title="Administrar"  border="0">
							</@security.a>
						
						<#-- Estado Pendiente autorizacion-->
						<#elseif oc_os.estado.ordinal() == 2>
							<@security.a 
								templateDir="custontemplates" 
								securityCode="CUF0389" 
								enabled="oc_os.updatable"
								cssClass="item"  
								href="${request.contextPath}/compraContratacion/oc_os/autorizarOC_OSView.action?oc_os.oid=${oc_os.oid?c}&navigationId=autorizar-oc_os&flowControl=regis&navegacionIdBack=${navigationId}">
								<img  src="${request.contextPath}/common/images/administrar.gif" alt="Autorizar/Rechazar" title="Autorizar/Rechazar"  border="0">
							</@security.a>
						
						<#-- Cualquier otro estado-->
						<#else>		
							<@security.a 
								templateDir="custontemplates" 
								securityCode="CUF0384" 
								enabled="oc_os.updatable"
								cssClass="item"  
								href="">
								<img  src="${request.contextPath}/common/images/administrar.gif" alt="Administrar" title="Administrar"  border="0">
							</@security.a>
						</#if>	
						</div>
						
						<div class="alineacionMenor">
						<#-- Estado En Preparacion -->
						<#if oc_os.estado.ordinal() == 1>
							<@security.a  
								templateDir="custontemplates" 
								securityCode="CUF0385" 
								enabled="oc_os.eraseable"
								cssClass="item"  
								href="${request.contextPath}/compraContratacion/oc_os/deleteOC_OSView.action?oc_os.oid=${oc_os.oid?c}&navigationId=oc_os-anular&flowControl=regis&navegacionIdBack=${navigationId}">
								<img  src="${request.contextPath}/common/images/eliminar.gif" alt="Eliminar" title="Eliminar"  border="0">
							</@security.a>	
						
						<#-- Estado Pendiente autorizacion o Autorizado o enviada -->
						<#elseif oc_os.estado.ordinal() == 2 || oc_os.estado.ordinal() == 3 || oc_os.estado.ordinal() == 6>
							<@security.a  
								templateDir="custontemplates" 
								securityCode="CUF0385" 
								enabled="oc_os.eraseable"
								cssClass="item"  
								href="${request.contextPath}/compraContratacion/oc_os/anularOC_OSView.action?oc_os.oid=${oc_os.oid?c}&navigationId=oc_os-anular&flowControl=regis&navegacionIdBack=${navigationId}">
								<img  src="${request.contextPath}/common/images/eliminar.gif" alt="Anular" title="Anular"  border="0">
							</@security.a>	
							
							
						<#-- Cualquier otro estado-->
						<#else>				
								<@security.a 
									templateDir="custontemplates" 
									securityCode="CUF0385" 
									enabled="false"
									cssClass="item"  
									href="">
									<img  src="${request.contextPath}/common/images/eliminar.gif" alt="Anular" title="Anular"  border="0">
								</@security.a>											
					
						</#if>	
								
					</div>
					<div class="alineacionMenor">	
						<#-- OC/OS totalmente recepcionada la intervencion es por facturacion-->
						
						<#if oc_os.cantidadItemsDetalle==oc_os.recibidaTotal && (oc_os.cantidadItemsDetalle>oc_os.facturadaTotal) >
						
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0386" 
							enabled="oc_os.enable"
							cssClass="item"  
							href="${request.contextPath}/compraContratacion/oc_os/intervenirOCOSPorFacturacionView.action?oc_os.oid=${oc_os.oid?c}&navigationId=oc_os-intervenir&flowControl=regis&navegacionIdBack=${navigationId}">
							<img  src="${request.contextPath}/common/images/intervenir.gif" alt="Intervenir" title="Intervenir"  border="0">
						</@security.a>
						
						<#elseif (oc_os.cantidadItemsDetalle>oc_os.recibidaTotal) && (oc_os.cantidadItemsDetalle==oc_os.facturadaTotal) >
						
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0386" 
							enabled="oc_os.enable"
							cssClass="item"  
							href="${request.contextPath}/compraContratacion/oc_os/intervenirOCOSPorRecepcionView.action?oc_os.oid=${oc_os.oid?c}&navigationId=oc_os-intervenir&flowControl=regis&navegacionIdBack=${navigationId}">
							<img  src="${request.contextPath}/common/images/intervenir.gif" alt="Intervenir" title="Intervenir"  border="0">
						</@security.a>
						
						<#elseif (oc_os.cantidadItemsDetalle>oc_os.recibidaTotal) && (oc_os.cantidadItemsDetalle>oc_os.facturadaTotal) >
						
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0386" 
							enabled="oc_os.enable"
							cssClass="item"  
							href="${request.contextPath}/compraContratacion/oc_os/intervenirOC_OSView.action?oc_os.oid=${oc_os.oid?c}&navigationId=oc_os-intervenir&flowControl=regis&navegacionIdBack=${navigationId}">
							<img  src="${request.contextPath}/common/images/intervenir.gif" alt="Intervenir" title="Intervenir"  border="0">
						</@security.a>
						
						<#elseif (oc_os.cantidadItemsDetalle==oc_os.recibidaTotal) && (oc_os.cantidadItemsDetalle==oc_os.facturadaTotal) >
						
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0386" 
							enabled="false"
							cssClass="item"  
							href="${request.contextPath}/compraContratacion/oc_os/intervenirOC_OSView.action?oc_os.oid=${oc_os.oid?c}&navigationId=oc_os-intervenir&flowControl=regis&navegacionIdBack=${navigationId}">
							<img  src="${request.contextPath}/common/images/intervenir.gif" alt="Intervenir" title="Intervenir"  border="0">
						</@security.a>
						</#if>
					</div>
						
						
						<div class="alineacionMenor">						
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0387" 
							enabled="oc_os.readable" 
							cssClass="no-rewrite" 
							href="${request.contextPath}/compraContratacion/oc_os/imprimirOC_OS.action?oc_os.oid=${oc_os.oid?c}&navigationId=oc_os-imprimirEnviar&flowControl=regis">
							<img  src="${request.contextPath}/common/images/imprimir.gif" alt="Imprimir" title="Imprimir"  border="0">
						</@security.a>							
						</div>
						<div class="alineacionMenor">
						<#assign puedeAgregarNoConformidad = "false" />
						<#if oc_os.estado.ordinal()==3 || oc_os.estado.ordinal()==4 || oc_os.estado.ordinal()==5 || oc_os.estado.ordinal()==6 || oc_os.estado.ordinal()==7 >
							<#assign puedeAgregarNoConformidad = "true" />
						</#if>
						<#-- OC/OS tiene No Conformidad -->
						<#if oc_os.noConformidad?exists>
							<@security.a 
								templateDir="custontemplates" 
								securityCode="CUF0388" 
								enabled="${puedeAgregarNoConformidad}" 
								cssClass="item" 
								href="${request.contextPath}/compraContratacion/oc_os/updateOC_OSNoConformidadView.action?oc_osNoConformidad.oc_os.oid=${oc_os.oid?c}&oc_osNoConformidad.oid=${oc_os.noConformidad.oid?c}&navigationId=oc_os-actualizarNoConformidad&flowControl=regis">
								<img  src="${request.contextPath}/common/images/modificarNoConformidad.gif" alt="No Conformidad" title="Modificar No Conformidad"  border="0">
							</@security.a>
						<#else>	
							<@security.a 
								templateDir="custontemplates" 
								securityCode="CUF0388" 
								enabled="${puedeAgregarNoConformidad}" 
								cssClass="item" 
								href="${request.contextPath}/compraContratacion/oc_os/createOC_OSNoConformidadView.action?oc_osNoConformidad.oc_os.oid=${oc_os.oid?c}&navigationId=oc_os-agregarNoConformidad&flowControl=regis">
								<img  src="${request.contextPath}/common/images/agregarNoConformidad.gif" alt="No Conformidad" title="Agregar No Conformidad"  border="0">
							</@security.a>
						</#if>	
						</div>
					</@display.column>
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="numero" title="Nro" />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="fecha" format="{0,date,dd/MM/yyyy}"  title="Fecha"/>
					<#--<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="tipo" title="Tipo" />-->
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" title="Proveedor">  
						${oc_os.proveedor.detalleDePersona.razonSocial} 
												
						<#if oc_os.proveedor.detalleDePersona.nombre?exists> 
				 			${oc_os.proveedor.detalleDePersona.nombre}
						</#if>
						
						
						
					</@display.column> 
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="importe" title="Importe" />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="estado" title="Estado" />					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="recibidaParcialString" title="Rec.Par." />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="recibidaTotalString" title="Rec.Tot." />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="facturadaParcialString" title="Fac.Par." />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="facturadaTotalString" title="Fac.Tot." />
					
					
				</@display.table>
			</@vc.anchors>
			</div>
			</@s.if>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/compraContratacion/oc_os/selectProveedorSearch.action" 
  source="seleccionarProveedor" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=change,oc_os.oid={oc_os.oid},oc_os.tipo={oc_os.tipo},oc_os.cliente.oid={oc_os.cliente.oid},oc_os.cliente.descripcion={oc_os.cliente.descripcion},productoBien.oid={productoBien.oid},productoBien.descripcion={productoBien.descripcion},oc_os.estado={oc_os.estado},oc_os.numero={oc_os.numero},oc_os.proveedor.nroProveedor={oc_os.proveedor.nroProveedor},fechaDesde={fechaDesde},fechaHasta={fechaHasta},importeDesde={importeDesde},importeHasta={importeHasta},pendienteFacturacion={pendienteFacturacion},incluyePBCritico={incluyePBCritico},pendienteRecepcion={pendienteRecepcion}, pendienteRecepcionVencido={pendienteRecepcionVencido}"/>


<@vc.htmlContent 
  baseUrl="${request.contextPath}/compraContratacion/oc_os/search.action"
  source="buscar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="resetFlag=true,navigationId={navigationId},flowControl=regis,oc_os.numero={oc_os.numero},oc_os.cliente.oid={oc_os.cliente.oid},oc_os.cliente.descripcion={oc_os.cliente.descripcion},productoBien.oid={productoBien.oid},productoBien.descripcion={productoBien.descripcion},oc_os.proveedor.detalleDePersona.razonSocial={oc_os.proveedor.detalleDePersona.razonSocial},oc_os.proveedor.detalleDePersona.nombre={oc_os.proveedor.detalleDePersona.nombre},oc_os.estado={oc_os.estado},oc_os.proveedor.nroProveedor={oc_os.proveedor.nroProveedor},oc_os.tipo={oc_os.tipo},fechaDesde={fechaDesde},fechaHasta={fechaHasta},importeDesde={importeDesde},importeHasta={importeHasta},pendienteFacturacion={pendienteFacturacion},incluyePBCritico={incluyePBCritico},pendienteRecepcion={pendienteRecepcion}, pendienteRecepcionVencido={pendienteRecepcionVencido}"/>
  		      

<@vc.htmlContent 
  baseUrl="${request.contextPath}/compraContratacion/oc_os/view.action"
  source="limpiar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=regis"/>
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/compraContratacion/oc_os/createOC_OSView.action" 
  source="crear" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=crearOC_OS,flowControl=regis"/>
  
  <@vc.htmlContent 
  baseUrl="${request.contextPath}/compraContratacion/oc_os/selectOCOSAutorizadas.action" 
  source="imprimirAutorizadas" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=oc_os-visualizarOCOSImprimir,flowControl=regis,navegacionIdBack=${navigationId},oc_os.cliente.oid={oc_os.cliente.oid},oc_os.cliente.descripcion={oc_os.cliente.descripcion},productoBien.oid={productoBien.oid},productoBien.descripcion={productoBien.descripcion},oc_os.numero={oc_os.numero},oc_os.proveedor.detalleDePersona.razonSocial={oc_os.proveedor.detalleDePersona.razonSocial},oc_os.proveedor.detalleDePersona.nombre={oc_os.proveedor.detalleDePersona.nombre},oc_os.estado={oc_os.estado},oc_os.proveedor.nroProveedor={oc_os.proveedor.nroProveedor},oc_os.tipo={oc_os.tipo},fechaDesde={fechaDesde},fechaHasta={fechaHasta},importeDesde={importeDesde},importeHasta={importeHasta},pendienteFacturacion={pendienteFacturacion},incluyePBCritico={incluyePBCritico},pendienteRecepcion={pendienteRecepcion}, pendienteRecepcionVencido={pendienteRecepcionVencido}"/>
 
<@vc.htmlContent 
  baseUrl="${request.contextPath}/compraContratacion/oc_os/selectProductoBienSearch.action" 
  source="seleccionarProductoBien" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=change,oc_os.oid={oc_os.oid},oc_os.tipo={oc_os.tipo},oc_os.cliente.oid={oc_os.cliente.oid},oc_os.cliente.descripcion={oc_os.cliente.descripcion},productoBien.oid={productoBien.oid},productoBien.descripcion={productoBien.descripcion},oc_os.estado={oc_os.estado},oc_os.numero={oc_os.numero},oc_os.proveedor.nroProveedor={oc_os.proveedor.nroProveedor},fechaDesde={fechaDesde},fechaHasta={fechaHasta},importeDesde={importeDesde},importeHasta={importeHasta},pendienteFacturacion={pendienteFacturacion},incluyePBCritico={incluyePBCritico},pendienteRecepcion={pendienteRecepcion}, pendienteRecepcionVencido={pendienteRecepcionVencido}"/>
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/compraContratacion/oc_os/selectClienteSearch.action" 
  source="seleccionarCliente" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=change,oc_os.oid={oc_os.oid},oc_os.tipo={oc_os.tipo},productoBien.oid={productoBien.oid},productoBien.descripcion={productoBien.descripcion},oc_os.estado={oc_os.estado},oc_os.numero={oc_os.numero},oc_os.proveedor.nroProveedor={oc_os.proveedor.nroProveedor},fechaDesde={fechaDesde},fechaHasta={fechaHasta},importeDesde={importeDesde},importeHasta={importeHasta},oc_os.cliente.oid={oc_os.cliente.oid},oc_os.cliente.descripcion={oc_os.cliente.descripcion},pendienteFacturacion={pendienteFacturacion},incluyePBCritico={incluyePBCritico},pendienteRecepcion={pendienteRecepcion}, pendienteRecepcionVencido={pendienteRecepcionVencido}"/>
  