<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancel" type="button" name="btnCancelar" value="Cancelar" class="boton"/>
				<input id="store" type="button" name="btnModificar" value="Modificar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/compraContratacion/pedidoCotizacion/updateCotizacion.action" 
  source="store" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="cotizacion.oid={cotizacion.oid},cotizacion.cantidadMinima={cotizacion.cantidadMinima},cotizacion.precio={cotizacion.precio},cotizacion.cantidadMaxima={cotizacion.cantidadMaxima},cotizacion.alicuotaIva={cotizacion.alicuotaIva}"/>
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=administrar,flowControl=back"/>
  
  