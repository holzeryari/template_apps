<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
	<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>

	
<@s.hidden id="pcStringList" name="pcStringList"/>


<#assign styleLinkImprimirPCproductoBien="display:inline;">
<#assign styleTextoImprimirPCproductoBien="">

<#assign styleLinkEnviarPCproductoBien="display:none;">
<#assign styleTextoEnviarPCproductoBien="">

<#assign styleLinkImprimirPCservicio="display:none;">
<#assign styleTextoImprimirPCservicio="">

<#assign styleLinkEnviarPCservicio="display:none;">
<#assign styleTextoEnviarPCservicio="">

<!-- <#if puedeImprimirPCproductoBien?exists && puedeImprimirPCproductoBien>
	<#assign styleLinkImprimirPCproductoBien="">
	<#assign styleTextoImprimirPCproductoBien="display:none;">	
</#if> -->

<#if puedeEnviarPCproductoBien?exists && puedeEnviarPCproductoBien>
	<#assign styleLinkEnviarPCproductoBien="">
	<#assign styleTextoEnviarPCproductoBien="display:none;">	
</#if>


<#if puedeImprimirPCservicio?exists && puedeImprimirPCservicio>
	<#assign styleLinkImprimirPCservicio="">
	<#assign styleTextoImprimirPCservicio="display:none;">	
</#if>

<#if puedeEnviarPCservicio?exists && puedeEnviarPCservicio>
	<#assign styleLinkEnviarPCservicio="">
	<#assign styleTextoEnviarPCservicio="display:none;">	
</#if>
<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
        	<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
       		<#if mensajeAviso?exists && (mensajeAviso.length()>0)>
				<tr>
					<td class="estiloMensajeAviso"><@s.text name="${mensajeAviso}"/></td>
				</tr>
			</#if>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
			<tr>
				<td><b>Datos del Pedido de Cotizaci&oacute;n</b></td>
			</tr>
		</table>
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
	    		<td class="textoCampo" colspan="4">&nbsp;</td>
	    	</tr>
			<tr>
	  			<td class="textoCampo">N&uacute;mero Pedido Cotizaci&oacute;n:</td>
      			<td class="textoDato">
					<@s.hidden id="pedidoCotizacion.proveedor.nroProveedor" name="pedidoCotizacion.proveedor.nroProveedor"/>
				<@s.property default="&nbsp;" escape=false value="pedidoCotizacion.numero" />  
				</td>							
      		
      			<td class="textoCampo">Proveedor:</td>
      			<td class="textoDato">
      				<@s.property default="&nbsp;" escape=false value="pedidoCotizacion.proveedor.detalleDePersona.razonSocial" />
      				<@s.property default="&nbsp;" escape=false value="pedidoCotizacion.proveedor.detalleDePersona.nombre" />&nbsp;
				</td>	
				
			</tr>
			<tr>
      			<td class="textoCampo">Tipo:</td>
      			<td class="textoDato">
      			<@s.hidden id="pedidoCotizacion.tipo" name="pedidoCotizacion.tipo"/>	
				<@s.property default="&nbsp;" escape=false value="pedidoCotizacion.tipo" />     				      			
				</td>	      			
      			<td class="textoCampo">Nro. Asignaci&oacute;n de Proveedores:</td>
      			<td class="textoDato">
      			<@s.property default="&nbsp;" escape=false value="pedidoCotizacion.asignacionProveedor.numero" />	      			
				</td>							
      		</tr>
	      	
			<tr>
				<td class="textoCampo">Estado:</td>
      			<td class="textoDato" colspan="3">
      			<@s.property default="&nbsp;" escape=false value="pedidoCotizacion.estado"/>
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Fecha Pedido Desde:</td>
      			<td class="textoDato">
      			<@s.if test="fechaPedidoDesde != null">					 
				${fechaPedidoDesde?string("dd/MM/yyyy")}	
				</@s.if>	&nbsp;
				</td>							
      		
				<td class="textoCampo">Fecha Pedido Hasta:</td>
      			<td class="textoDato">
      			<@s.if test="fechaPedidoHasta != null">					 
				${fechaPedidoHasta?string("dd/MM/yyyy")}	
				</@s.if>	&nbsp;
			</tr>
				
			<tr>
				<td class="textoCampo">Fecha L&iacute;mite Recepci&oacute;n Cotizaciones Desde:</td>
      			<td class="textoDato">
      			<@s.if test="fechaLimiteRecepcionCotizacionDesde != null">					 
				${fechaLimiteRecepcionCotizacionDesde?string("dd/MM/yyyy")}	
				</@s.if>	&nbsp;
      			</td>
      			
      			<td class="textoCampo">Fecha L&iacute;mite Recepci&oacute;n Cotizaciones Hasta:</td>
      			<td class="textoDato">
      			<@s.if test="fechaLimiteRecepcionCotizacionHasta != null">					 
				${fechaLimiteRecepcionCotizacionHasta?string("dd/MM/yyyy")}	
				</@s.if>	&nbsp;	      			
			</tr>
			<tr><td colspan="4" class="textoCampo">&nbsp;</td></tr>
		</table>
		</div>			
		<#-- Lista de OC-->
		<@s.hidden id="pcProductoBienList" name="ocList"/>

		<@vc.anchors target="contentTrx">	
		<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
				<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>Pedidos de Cotizaci&oacute;n de Productos/Bienes</td>
				</tr>
			</table>	 

			<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="pcProductoBienList" id="pedidoCotizacion" defaultsort=2>	
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select"  class="botoneraAnchoCon3" title="Acciones">		
				<div class="alineacion">
				<a href="${request.contextPath}/compraContratacion/pedidoCotizacion/readPedidoCotizacionView.action?pedidoCotizacion.oid=${pedidoCotizacion.oid?c}&navigationId=pedidoCotizacion-visualizar&flowControl=regis&navegacionIdBack=${navigationId}">
				<img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver" border="0"></a>
				</div>
				<div class="alineacion">
				<#if pedidoCotizacion.proveedor.email?exists>
						<@s.hidden id="${pedidoCotizacion.oid?c}" name="${pedidoCotizacion.proveedor.email}"/>
						<@s.checkbox label="${pedidoCotizacion.oid?c}" fieldValue="${pedidoCotizacion.oid?c}" name="pcProductoBienSelect" value="true" onclick="javascript:changePCproductoBien(this);" />
					<#else>
						<@s.hidden id="${pedidoCotizacion.oid?c}" name=""/>
						<@s.checkbox label="${pedidoCotizacion.oid?c}" fieldValue="${pedidoCotizacion.oid?c}" name="pcProductoBienSelect" value="true" onclick="javascript:changePCproductoBien(this);" />
				</#if>
				</div>
				<#--<a href="${request.contextPath}/maestro/proveedor/updateView.action?proveedor.nroProveedor=${pedidoCotizacion.proveedor.nroProveedor?c}&navigationId=pedidoCotizacion-visualizar&flowControl=regis&navegacionIdBack=${navigationId}">			
				<img src="${request.contextPath}/common/images/modificar.gif" border="0" align="absmiddle" hspace="3" >
				</a>-->
				<div class="alineacion">
					<@security.a 
					templateDir="custontemplates" 
					securityCode="CUF0123" 
					enabled="true" 
					cssClass="item" 
					href="${request.contextPath}/maestro/proveedor/updateView.action?proveedor.nroProveedor=${pedidoCotizacion.proveedor.nroProveedor?c}&navigationId=advertenciaProveedor-modificarProveedor&flowControl=regis&navegacionIdBack=${navigationId}">
					<img  src="${request.contextPath}/common/images/modificar.gif" alt="Modificar" title="Modificar"  border="0">
				</@security.a>
				</div>		
			</@display.column>
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="numero" title="N&uacute;mero" />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="fechaPedido" format="{0,date,dd/MM/yyyy}"  title="Fecha" />	
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="estado" title="Estado" />	
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" title="Proveedor">  
				${pedidoCotizacion.proveedor.detalleDePersona.razonSocial} 
				
				<#if pedidoCotizacion.proveedor.detalleDePersona.nombre?exists> 
					${pedidoCotizacion.proveedor.detalleDePersona.nombre}
				</#if>
				
			</@display.column>
			
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="proveedor.email" title="Mail Proveedor" />
		</@display.table>
		</div>
	</@vc.anchors>
				
	<@s.if test="pcProductoBienList!=null && pcProductoBienList.size()>0">
	<div id="capaLink" class="capaLink">
		<div id="linkImprimirPCproductoBien" name="linkImprimirPCproductoBien" style="${styleLinkImprimirPCproductoBien}" class="alineacionDerecha">					
			<@s.a 
				templateDir="custontemplates"	
				cssClass="no-rewrite" 
				id="imprimirPCproductoBien"
				name="imprimirPCproductoBien"	
				onclick="javascript:listaPCproductoBienSelectLink(this);"									
				href="${request.contextPath}/compraContratacion/pedidoCotizacion/imprimirPC.action?_">					
				<b id="linkImprimirOSTexto" name="linkImprimirOSTexto" class="ocultarIcono">Imprimir</b>										
				<img src="${request.contextPath}/common/images/imprimir.gif" border="0" align="absmiddle" hspace="3" >
			</@s.a>	
		</div>
		<!-- <div id="textoImprimirPCproductoBien" name="textoImprimirPCproductoBien" style="${styleTextoImprimirPCproductoBien}" class="alineacionDerecha">
				<b id="textoImprimirPCproductoBienTexto" name="textoImprimirPCproductoBienTexto" class="textoGris">Imprimir</b>										
				<img src="${request.contextPath}/common/images/imprimir.gif" border="0" align="absmiddle" hspace="3" >
		</div> -->	
					
					
		<div id="linkEnviarPCproductoBien" name="linkEnviarPCproductoBien" style="${styleLinkEnviarPCproductoBien}" class="alineacionDerecha">					
			<@s.a 
				templateDir="custontemplates"
				cssClass="item" 
				id="enviarPCproductoBien"
				name="enviarPCproductoBien"										
				href="javascript://nop/">						
				<b id="linkEnviarPCproductoBienTexto" name="linkEnviarPCproductoBienTexto" class="ocultarIcono">Enviar</b>										
				<img src="${request.contextPath}/common/images/enviarmail.gif" border="0" align="absmiddle" hspace="3" >
			</@s.a>
			
			<@vc.htmlContent 
		  	baseUrl="${request.contextPath}/compraContratacion/pedidoCotizacion/enviarPC.action" 
		  	source="enviarPCproductoBien" 
		  	success="contentTrx" 
		  	failure="errorTrx" 
		  	preFunction="listaPCproductoBienSelect" 
		  	parameters="pcStringList={pcStringList}"/>
		</div>
		<div id="textoEnviarPCproductoBien"  name="textoEnviarPCproductoBien" style="${styleTextoEnviarPCproductoBien}" class="alineacionDerecha">
				<b id="textoEnviarPCproductoBienTexto" name="textoEnviarPCproductoBienTexto" class="textoGris">Enviar</b>										
				<img src="${request.contextPath}/common/images/enviarmail.gif" border="0" align="absmiddle" hspace="3" >
		</div>
	</div>	
	</@s.if>


	<#-- Lista de OS-->
	<@s.hidden id="pcServicioList" name="pcServicioList"/>				
	<@vc.anchors target="contentTrx">			
	<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
				<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>Pedidos de Cotizaci&oacute;n de Servicios</td>
				</tr>
			</table>
						
		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="pcServicioList" id="pedidoCotizacion" defaultsort=2>	
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select"  class="botoneraAnchoCon3" title="Acciones">
			<div class="alineacion">		
			<a href="${request.contextPath}/compraContratacion/pedidoCotizacion/readPedidoCotizacionView.action?pedidoCotizacion.oid=${pedidoCotizacion.oid?c}&navigationId=pedidoCotizacion-visualizar&flowControl=regis&navegacionIdBack=${navigationId}">
			<img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver" border="0"></a>
			</div>
			<div class="alineacion">
			<#if pedidoCotizacion.proveedor.email?exists>
					<@s.hidden id="${pedidoCotizacion.oid?c}" name="${pedidoCotizacion.proveedor.email}"/>
					<@s.checkbox label="${pedidoCotizacion.oid?c}" fieldValue="${pedidoCotizacion.oid?c}" name="pcServicioSelect" value="true" onclick="javascript:changePCservicio(this);" />
				<#else>
					<@s.hidden id="${pedidoCotizacion.oid?c}" name=""/>
					<@s.checkbox label="${pedidoCotizacion.oid?c}" fieldValue="${pedidoCotizacion.oid?c}" name="pcServicioSelect" value="true" onclick="javascript:changePCservicio(this);" />
			</#if>
			</div>
			<#-- <a href="${request.contextPath}/maestro/proveedor/updateView.action?proveedor.nroProveedor=${pedidoCotizacion.proveedor.nroProveedor?c}&navigationId=pedidoCotizacion-visualizar&flowControl=regis&navegacionIdBack=${navigationId}">			
			<img src="${request.contextPath}/common/images/modificar.gif" border="0" align="absmiddle" hspace="3" >
			</a>-->
			<div class="alineacion">
				<@security.a 
					templateDir="custontemplates" 
					securityCode="CUF0123" 
					enabled="true" 
					cssClass="item" 
					href="${request.contextPath}/maestro/proveedor/updateView.action?proveedor.nroProveedor=${pedidoCotizacion.proveedor.nroProveedor?c}&navigationId=advertenciaProveedor-modificarProveedor&flowControl=regis&navegacionIdBack=${navigationId}">
					<img  src="${request.contextPath}/common/images/modificar.gif" alt="Modificar" title="Modificar"  border="0">
				</@security.a>
			</div>		
			</@display.column>
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="numero" title="N&uacute;mero" />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="fechaPedido" format="{0,date,dd/MM/yyyy}"  title="Fecha" />	
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="estado" title="Estado" />	
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" title="Proveedor">  
				${pedidoCotizacion.proveedor.detalleDePersona.razonSocial} 
				
				<#if pedidoCotizacion.proveedor.detalleDePersona.nombre?exists> 
				${pedidoCotizacion.proveedor.detalleDePersona.nombre}
				</#if>
				
			</@display.column>
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="proveedor.email" title="Mail Proveedor" />
		</@display.table>
		</div>
	</@vc.anchors>
	
	<@s.if test="pcServicioList!=null && pcServicioList.size()>0">
				<div id="capaLink" class="capaLink">
		<div id="linkImprimirPCservicio" name="linkImprimirPCservicio" style="${styleLinkImprimirPCservicio}" class="alineacionDerecha">					
		<@s.a 
			templateDir="custontemplates"	
			cssClass="no-rewrite" 
			id="imprimirPCservicio"
			name="imprimirPCservicio"	
			onclick="javascript:listaPCservicioSelectLink(this);"									
			href="${request.contextPath}/compraContratacion/pedidoCotizacion/imprimirPC.action?_">						
			<b id="linkImprimirOSTexto" name="linkImprimirOSTexto" >Imprimir</b>										
			<img src="${request.contextPath}/common/images/imprimir.gif" border="0" align="absmiddle" hspace="3" >
		</@s.a>	
	</div>
	<div id="textoImprimirPCservicio" name="textoImprimirPCservicio" style="${styleTextoImprimirPCservicio}" class="alineacionDerecha">
			<b id="textoImprimirPCservicioTexto" name="textoImprimirPCservicioTexto" class="textoGris">Imprimir</b>										
			<img src="${request.contextPath}/common/images/imprimir.gif" border="0" align="absmiddle" hspace="3" >
	</div>			
				
				
	<div id="linkEnviarPCservicio" name="linkEnviarPCservicio" style="${styleLinkEnviarPCservicio}" class="alineacionDerecha">					
		<@s.a 
			templateDir="custontemplates"
			cssClass="item" 
			id="enviarPCservicio"
			name="enviarPCservicio"										
			href="javascript://nop/">						
			<b id="linkEnviarPCservicioTexto" name="linkEnviarPCservicioTexto">Enviar</b>										
			<img src="${request.contextPath}/common/images/enviarmail.gif" border="0" align="absmiddle" hspace="3" >
		</@s.a>
					  
  	<@vc.htmlContent 
	  baseUrl="${request.contextPath}/compraContratacion/pedidoCotizacion/enviarPC.action" 
	  source="enviarPCservicio" 
	  success="contentTrx" 
	  failure="errorTrx" 
	  preFunction="listaPCservicioSelect" 
	  parameters="pcStringList={pcStringList}"/>
	  
	</div>
	<div id="textoEnviarPCservicio" name="textoEnviarPCservicio" style="${styleTextoEnviarPCservicio}" class="alineacionDerecha">
			<b id="textoEnviarPCservicioTexto" name="textoEnviarPCservicioTexto" class="textoGris">Enviar</b>										
			<img src="${request.contextPath}/common/images/enviarmail.gif" border="0" align="absmiddle" hspace="3" >
	</div>
		</div>
</@s.if>
		

<div id="capaBotonera" class="capaBotonera">
		<table id="tablaBotonera" class="tablaBotonera">
			<tr> 
				<td align="left">
					<input id="cancelar" type="button" name="btnVolver" value="Volver" class="boton"/>
				</td>
			</tr>	
		</table>
	</div>


  <@vc.htmlContent 
   baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancelar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=${navegacionIdBack},flowControl=back"/>


