<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign tiles=JspTaglibs["/WEB-INF/tlds/tiles-jsp.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<@s.hidden id="pedidoCotizacion.oid" name="pedidoCotizacion.oid"/>
<@s.hidden id="pedidoCotizacion.versionNumber" name="pedidoCotizacion.versionNumber"/>
<@s.hidden id="nroProveedor" name="pedidoCotizacion.proveedor.nroProveedor"/>

<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
			<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos del Pedido de Cotizaci&oacute;n</b></td>
				<td>
					<div align="right">
					<@s.a templateDir="custontemplates" id="modificarPedidoCotizacion" name="modificarPedidoCotizacion" href="javascript://nop/" cssClass="ocultarIcono">
						<b>Modificar</b><img src="${request.contextPath}/common/images/modificar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
					</@s.a>
					</div>
				</td>
			</tr>
		</table>

		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>
			<tr>
      			<td class="textoCampo">N&uacute;mero Pedido Cotizaci&oacute;n:</td>
      			<td class="textoDato"><@s.property default="&nbsp;" escape=false value="pedidoCotizacion.numero"/></td>
      			<td  class="textoCampo">Fecha Pedido: </td>
				<td class="textoDato">
					<@s.if test="pedidoCotizacion.fechaPedido != null">
						<#assign fechaPedido = pedidoCotizacion.fechaPedido> ${fechaPedido?string("dd/MM/yyyy")}	
					</@s.if>									
				</td>	      			
			</tr>	

			<tr>
				<td class="textoCampo">Proveedor:</td>
      			<td class="textoDato">
      				<@s.property default="&nbsp;" escape=false value="pedidoCotizacion.proveedor.detalleDePersona.razonSocial" /><@s.property default="&nbsp;" escape=false value="pedidoCotizacion.proveedor.detalleDePersona.nombre" />							
					<@s.a templateDir="custontemplates" id="seleccionarAsignacionProveedor" name="seleccionarAsignacionProveedor" href="javascript://nop/"  cssClass="ocultarIcono">
						<img src="${request.contextPath}/common/images/buscar.gif"  border="0" align="absmiddle" hspace="3" >
					</@s.a>		
				</td>
				<td class="textoCampo">Tipo Pedido:</td>
      			<td class="textoDato">
      			<@s.select 
							templateDir="custontemplates" 
							id="pedidoCotizacion.tipo" 
							cssClass="textarea"
							cssStyle="width:165px" 
							name="pedidoCotizacion.tipo" 
							list="tipoPedidoCotizacionList" 
							listKey="key" 
							listValue="description" 
							value="pedidoCotizacion.tipo.ordinal()"
							title="Tipo"
							headerKey="0"
							headerValue="Todos"							
							/>
				</td>
					
			</tr>
			<tr>
				<td class="textoCampo">Nro. Asignaci&oacute;n Proveedores:</td>
      			<td class="textoDato">
      			<@s.textfield 
      					templateDir="custontemplates" 
						id="pedidoCotizacion.asignacionProveedor.numero" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="pedidoCotizacion.asignacionProveedor.numero" 
						disabled="true"
						title="Asignaci&aocute;n Proveedores" />								
					<@s.a templateDir="custontemplates" id="seleccionarAsignacionProveedor" name="seleccionarAsignacionProveedor" href="javascript://nop/"  cssClass="ocultarIcono">
						<img src="${request.contextPath}/common/images/buscar.gif" border="0" align="absmiddle" hspace="3" >
					</@s.a>		
				</td>
				<td class="textoCampo">Fecha L&iacute;mite Recepci&oacute;n Cotizaciones:</td>
      			<td class="textoDato">
					<@vc.rowCalendar 
					templateDir="custontemplates" 
					id="pedidoCotizacion.fechaLimiteCot" 
					cssClass="textarea"
					cssStyle="width:160px"						
					name="pedidoCotizacion.fechaLimiteCot" 
					title="Fecha L&iacute;mite Recepci&oacute;n Cotizaciones" />
				</td>
			</tr>
			<tr>	
				<td class="textoCampo">Fecha Recepci&oacute;n Cotizaciones:</td>
      			<td class="textoDato">
					<@vc.rowCalendar 
					templateDir="custontemplates" 
					id="pedidoCotizacion.fechaRecepcion" 
					cssClass="textarea"
					cssStyle="width:160px"						
					name="pedidoCotizacion.fechaRecepcion" 
					title="Fecha Recepci&oacute;n" />
				</td>
				<td class="textoCampo">Fecha Hasta Validez Precios:</td>
      			<td class="textoDato">
					<@vc.rowCalendar 
					templateDir="custontemplates" 
					id="pedidoCotizacion.fechaValidezPrecio" 
					cssClass="textarea"
					cssStyle="width:160px"						
					name="pedidoCotizacion.fechaValidezPrecio" 
					title="Fecha Validez Precios" />
				</td>
			</tr>
				
    		<tr>
				<td class="textoCampo">Observaciones:</td>
      			<td  class="textoDato" colspan="3">
				<@s.textarea	
						templateDir="custontemplates" 						  
						cols="89" rows="4"	      					
						cssClass="textarea"
						id="pedidoCotizacion.observaciones"							 
						name="pedidoCotizacion.observaciones"  
						label="Observaciones"														
						 />
				</td>					
    		</tr>
                <tr>
                	<td class="textoCampo">Estado:</td>
                    <td class="textoDato" colspan="3">
                    	<@s.property default="&nbsp;" escape=false value="pedidoCotizacion.estado"/>
                    </td>
                </tr>
			<tr><td colspan="4" class="textoCampo">&nbsp;</td></tr>
		</table>	
	</div>		
		
<@vc.htmlContent 
  baseUrl="${request.contextPath}/compraContratacion/pedidoCotizacion/updatePedidoCotizacionView.action" 
  source="modificarPedidoCotizacion" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="pedidoCotizacion.oid={pedidoCotizacion.oid},pedidoCotizacion.versionNumber={pedidoCotizacion.versionNumber}"/>



