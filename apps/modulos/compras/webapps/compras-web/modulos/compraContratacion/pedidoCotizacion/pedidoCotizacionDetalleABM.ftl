<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign tiles=JspTaglibs["/WEB-INF/tlds/tiles-jsp.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>


<@s.hidden id="pedidoCotizacionDetalle.oid" name="pedidoCotizacionDetalle.oid"/>
<@s.hidden id="pedidoCotizacionDetalle.servicio.oid" name="pedidoCotizacionDetalle.servicio.oid"/>
<@s.hidden id="pedidoCotizacionDetalle.productoBien.oid" name="pedidoCotizacionDetalle.productoBien.oid"/>
<@s.hidden id="pedidoCotizacionDetalle.pedidoCotizacion.oid" name="pedidoCotizacionDetalle.pedidoCotizacion.oid"/>

<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
			<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos del Pedido de Cotizaci&oacute;n</b></td>
			</tr>
		</table>

		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>

			<tr>
      			<td class="textoCampo">N&uacute;mero Pedido Cotizaci&oacute;n:</td>
      			<td class="textoDato"><@s.property default="&nbsp;" escape=false value="pedidoCotizacionDetalle.pedidoCotizacion.numero"/></td>
      			<td  class="textoCampo">Fecha Pedido: </td>
				<td class="textoDato">
					<@s.if test="pedidoCotizacionDetalle.pedidoCotizacion.fechaPedido != null">
						<#assign fechaPedido = pedidoCotizacionDetalle.pedidoCotizacion.fechaPedido> ${fechaPedido?string("dd/MM/yyyy")}	
					</@s.if>									
				</td>	      			
			</tr>	

			<tr>
				<td class="textoCampo">Proveedor:</td>
      			<td class="textoDato">
      				<@s.property default="&nbsp;" escape=false value="pedidoCotizacionDetalle.pedidoCotizacion.proveedor.detalleDePersona.razonSocial" /><@s.property default="&nbsp;" escape=false value="pedidoCotizacion.proveedor.detalleDePersona.nombre" />							
				</td>
				
				<td class="textoCampo">Tipo Pedido:</td>
      			<td class="textoDato">
      			<@s.property default="&nbsp;" escape=false value="pedidoCotizacionDetalle.pedidoCotizacion.tipo"/>
      			</td>
      		</tr>
	      		
      		<tr>
				<td class="textoCampo">Nro. Asignaci&oacute;n Proveedores:</td>
      			<td class="textoDato"><@s.property default="&nbsp;" escape=false value="pedidoCotizacionDetalle.pedidoCotizacion.asignacionProveedor.numero"/></td>
      	
				<td class="textoCampo">Fecha L&iacute;mite Recepci&oacute;n Cotizaciones:</td>
      			<td class="textoDato">
				<@s.if test="pedidoCotizacionDetalle.pedidoCotizacion.fechaLimiteCot != null">
					<#assign fechaLimiteCot = pedidoCotizacionDetalle.pedidoCotizacion.fechaLimiteCot>
					<#--<@s.property default="&nbsp;" escape=false value="pedidoCotizacionDetalle.pedidoCotizacion.fechaLimiteCot" />-->	
					${fechaLimiteCot?string("dd/MM/yyyy")}	
				</@s.if>							
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Fecha Recepci&oacute;n Cotizaciones:</td>
				<td class="textoDato">
				<@s.property default="&nbsp;" escape=false value="pedidoCotizacionDetalle.pedidoCotizacion.fechaRecepcion" />							
				</td>
				<td class="textoCampo">Fecha Hasta Validez Precios:</td>
      			<td class="textoDato">
				<@s.property default="&nbsp;" escape=false value="pedidoCotizacionDetalle.pedidoCotizacion.fechaValidezPrecio" />						
				</td>
			</tr>
		
	    		
    		<tr>
				<td class="textoCampo">Observaciones:</td>
      			<td  class="textoDato" colspan="3">
				<@s.property default="&nbsp;" escape=false value="pedidoCotizacionDetalle.pedidoCotizacion.observaciones"/> 
      			</td>						
    		</tr>
    		
    		<tr>
				<td class="textoCampo">Estado:</td>
      			<td  class="textoDato" colspan="3">
					<@s.property default="&nbsp;" escape=false value="pedidoCotizacionDetalle.pedidoCotizacion.estado"/></td>
				</td>

    		</tr> 
    		<tr><td colspan="4" class="textoCampo">&nbsp;</td></tr>
		</table>	
		</div>	
			
		<@s.if test="pedidoCotizacionDetalle.productoBien != null">
			<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
				<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            		<tr>
						<td><b>Datos del Producto/Bien</b></td>
					</tr>
				</table>
				<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
					<tr>
        				<td class="textoCampo" colspan="4">&nbsp;</td>
        			</tr>			

					<tr>
		      			<td class="textoCampo">C&oacute;digo:</td>
		      			<td class="textoDato">
		      			<@s.property default="&nbsp;" escape=false value="pedidoCotizacionDetalle.productoBien.oid"/>
		      			</td>
		      			<td  class="textoCampo">Descripci&oacute;n: </td>
						<td class="textoDato">
						<@s.property default="&nbsp;" escape=false value="pedidoCotizacionDetalle.productoBien.descripcion"/>		
						</td>	      			
					</tr>	
					<tr>
		      			<td class="textoCampo">Tipo:</td>
			      		<td class="textoDato">
			      			<@s.property default="&nbsp;" escape=false value="pedidoCotizacionDetalle.productoBien.tipo"/>						      			
			      		</td>		      		
												
						<td class="textoCampo">Rubro:</td>
		      			<td class="textoDato">
		      				<@s.property default="&nbsp;" escape=false value="pedidoCotizacionDetalle.productoBien.rubro.descripcion"/>
						</td>
		    		</tr>
					<tr>
						<td class="textoCampo">Marca: </td>
						<td  class="textoDato">
							<@s.property default="&nbsp;" escape=false value="pedidoCotizacionDetalle.productoBien.marca"/>										
						</td>
						<td class="textoCampo">Modelo: </td>
						<td  class="textoDato">
							<@s.property default="&nbsp;" escape=false value="pedidoCotizacionDetalle.productoBien.modelo"/>										
						</td>
		    		</tr>
		    		<tr>
						<td class="textoCampo">Valor Mercado: </td>
						<td  class="textoDato">
							<@s.property default="&nbsp;" escape=false value="bien.valorMercado"/>
						</td>
						<td class="textoCampo">Fecha Valor Mercado: </td>
						<td  class="textoDato">
							<@s.property default="&nbsp;" escape=false value="bien.fechaValorMercado"/>
						</td>
		    		</tr>
					<tr>
						<td class="textoCampo">C&oacute;digo de Barra: </td>
						<td  class="textoDato">
							<@s.property default="&nbsp;" escape=false value="pedidoCotizacionDetalle.productoBien.codigoBarra"/>										
						</td>
						<td class="textoCampo">Es Cr&iacute;tico: </td>
						<td  class="textoDato">
							<@s.property default="&nbsp;" escape=false value="pedidoCotizacionDetalle.productoBien.critico"/>						      			
						</td>
		    		</tr>
		    		<tr>
						<td class="textoCampo">Reposici&oacute;n Autm&aacute;tica: </td>
						<td  class="textoDato">	
							<@s.property default="&nbsp;" escape=false value="producto.reposicionAutomatica"/>						      											
						</td>
						<td class="textoCampo">Existencia M&iacute;nima: </td>
						<td  class="textoDato">
							<@s.property default="&nbsp;" escape=false value="producto.existenciaMinima"/>
						</td>
		    		</tr>
		    		<tr>
		    		<td class="textoCampo">Tipo Unidad:</td>
		    		<td  class="textoDato"colspan="3">
							<@s.property default="&nbsp;" escape=false value="producto.tipoUnidad.codigo"/>
						</td>
		    		</tr>
					<tr>
						<td class="textoCampo">Es Registrable: </td>
						<td  class="textoDato" colspan="3">
						<@s.property default="&nbsp;" escape=false value="bien.registrable"/>						      			
						</td>
						
		    		</tr>
					<tr>
						<td class="textoCampo">Es Veh&iacute;culo: </td>
						<td  class="textoDato" colspan="3">
						<@s.property default="&nbsp;" escape=false value="bien.vehiculo"/>
						</td>
		    		</tr>							
					<tr>
						<td class="textoCampo">Estado: </td>
		      			<td class="textoDato" colspan="3"><@s.property default="&nbsp;" escape=false value="productoBien.estado"/></td>
		    		</tr>
					<tr><td colspan="4" class="textoCampo">&nbsp;</td></tr>    		
				</table>
				</div>
			</@s.if>
			
			<@s.if test="pedidoCotizacionDetalle.servicio != null">
				<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
				<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            		<tr>
						<td><b>Datos del Servicio</b></td>
					</tr>
				</table>
				<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
					<tr>
        				<td class="textoCampo" colspan="4">&nbsp;</td>
        			</tr>	
					
					<tr>
		      			<td class="textoCampo">C&oacute;digo:</td>
		      			<td class="textoDato">
			      			<@s.property default="&nbsp;" escape=false value="pedidoCotizacionDetalle.servicio.oid"/>			      			
		      			<td class="textoCampo">Descripci&oacute;n</td>
						<td class="textoDato">
							<@s.property default="&nbsp;" escape=false value="pedidoCotizacionDetalle.servicio.descripcion"/>			
					</tr>	
					<tr>
														
						<td class="textoCampo">Rubro:</td>
		      			<td class="textoDato">
		      				<@s.property default="&nbsp;" escape=false value="pedidoCotizacionDetalle.servicio.rubro.descripcion"/>
					     </td>
						
						<td class="textoCampo">Cr&iacute;tico: </td>
						<td class="textoDato">
							<@s.property default="&nbsp;" escape=false value="pedidoCotizacionDetalle.servicio.critico"/>								
						</td>
					</tr>
	
					<tr>
		      			<td class="textoCampo">Proveedor &Uacute;nico:</td>
						<td class="textoDato">
							<@s.property default="&nbsp;" escape=false value="pedidoCotizacionDetalle.servicio.proveedorUnico"/>
						</td>
						<td class="textoCampo">Proveedor:</td>
						<td class="textoDato">&nbsp;							
						</td>
		    		</tr>
		    		<tr>			
						<td class="textoCampo">Tipo:</td>
			      		<td class="textoDato">
			     			<@s.property default="&nbsp;" escape=false value="pedidoCotizacionDetalle.servicio.tipo"/>
			      		</td>				      		
						<td class="textoCampo">Estado:</td>
						<td class="textoDato" colspan="3">
							<@s.property default="&nbsp;" escape=false value="pedidoCotizacionDetalle.servicio.estado"/>
						</td>
					</tr>
					<tr><td colspan="4" class="textoCampo">&nbsp;</td></tr>	
				</table>
				</div>
			</@s.if>	
		
