<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<@vc.anchors target="contentTrx" ajaxFlag="ajax">
<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
			<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>Servicios a cotizar</td>
				</tr>
			</table>		 

			<table class="tablaDetalleCuerpo" cellpadding="3" >			
				<#assign pedidoCotizacionDetalleLista = pedidoCotizacion.pedidoCotizacionDetalleList>
					<#list pedidoCotizacionDetalleLista as pedidoCotizacionDetalle>
				      <tr>
							<th align="center">Acciones</th>
							<th align="center">N&uacute;mero</th>
							<th align="center">C&oacute;digo</th>
							<th align="center">Descripci&oacute;n</th>
							<th align="center">Rubro</th>
							<th align="center">Tipo</th>
							
						</tr>
						<tr>
							<td> 
								<div align="center">
								<@s.a templateDir="custontemplates" id="verDetallePedidoCotizacion" name="verDetallePedidoCotizacion" href="${request.contextPath}/compraContratacion/pedidoCotizacion/readPedidoCotizacionDetalleView.action?pedidoCotizacionDetalle.oid=${pedidoCotizacionDetalle.oid?c}&navegacionIdBack=administrar" cssClass="ocultarIcono">
									<img src="${request.contextPath}/common/images/ver.gif"  border="0" align="absmiddle" hspace="3" >
								</@s.a>
							
								</div>
							</td>
							<td class="estiloNumero">${pedidoCotizacionDetalle.numero}</td>
							<td class="estiloNumero">${pedidoCotizacionDetalle.servicio.oid}</td>
							<td class="estiloTexto">${pedidoCotizacionDetalle.servicio.descripcion}</td>
							<td class="estiloTexto">${pedidoCotizacionDetalle.servicio.rubro.descripcion}</td>
							<td class="estiloTexto">${pedidoCotizacionDetalle.servicio.tipo}</td>
									   
						</tr>
						
						<tr>
							<td colspan="1">&nbsp;</td>
							<th colspan="1" nowrap>
								<div align="center">
							<#if pedidoCotizacionDetalle.cotizacionList.size()!=0>								
									<b>Cotizaciones</b>								
							<#else>
								<b>Cotizaciones</b>
								<@s.a templateDir="custontemplates" id="agregarCotizacion" name="agregarCotizacion" href="${request.contextPath}/compraContratacion/pedidoCotizacion/createCotizacionView.action?cotizacion.pedidoCotizacionDetalle.oid=${pedidoCotizacionDetalle.oid?c}&navegacionIdBack=administrar" cssClass="ocultarIcono">
									<img src="${request.contextPath}/common/images/agregar.gif"  border="0" align="absmiddle" hspace="3" >
								</@s.a>
							</#if>
								</div>
							</th>	
							<th align="center">N&uacute;mero</th>	
							<th align="center">Precio</th>
							<th colspan="2" align="center">Al&iacute;cuota IVA</th>
						</tr>
						
						<#assign cotizacionLista = pedidoCotizacionDetalle.cotizacionList>
						<#list cotizacionLista as cotizacion>
												
						<tr>
							<td>&nbsp;</td>
							<td class="botoneraAnchoCon2">
								<div align="center">
									<div class="alineacion">
										<@s.a templateDir="custontemplates" id="modificarCotizacion" name="modificarCotizacion" href="${request.contextPath}/compraContratacion/pedidoCotizacion/updateCotizacionView.action?cotizacion.oid=${cotizacion.oid?c}&navegacionIdBack=administrar" cssClass="ocultarIcono">
											<img src="${request.contextPath}/common/images/modificar.gif"  border="0" align="absmiddle" hspace="3" >
										</@s.a>
									</div>
									<div class="alineacion">
										<@s.a templateDir="custontemplates" id="eliminarCotizacion" name="eliminarCotizacion" href="${request.contextPath}/compraContratacion/pedidoCotizacion/deleteCotizacionView.action?cotizacion.oid=${cotizacion.oid?c}&navegacionIdBack=administrar" cssClass="ocultarIcono">
											<img src="${request.contextPath}/common/images/eliminar.gif"  border="0" align="absmiddle" hspace="3" >
										</@s.a>
									</div>
								</div>	
							</td>
							<td class="estiloNumero">${cotizacion.numero}</td>
					
							<td class="estiloNumero">${cotizacion.precio}</td>
							<td colspan="2" class="estiloNumero">
							<#if cotizacion.alicuotaIva?exists>
        						${cotizacion.alicuotaIva}%
        					<#else>
        						&nbsp;
        					</#if>
							</td>						
							
						</tr>						
						</#list>
						  
					</#list>						
				</table>
			</div>		
</@vc.anchors>

