<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>

<@s.hidden id="navigationId" name="navigationId"/>
<@s.hidden id="cliente.oid" name="cliente.oid"/>
<@s.hidden id="cliente.descripcion" name="cliente.descripcion"/>
<@s.hidden id="proveedor.nroProveedor" name="proveedor.nroProveedor"/>
<@s.hidden id="proveedor.detalleDePersona.razonSocial" name="proveedor.detalleDePersona.razonSocial" />
<@s.hidden id="proveedor.detalleDePersona.nombre" name="proveedor.detalleDePersona.nombre" />
<@s.hidden id="pagado" name="pagado.ordinal()" />

<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
        	<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
			<tr>
				<td><b>Datos de la Imputaci&oacute;n</b></td>
			</tr>
		</table>
			
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
	    		<td class="textoCampo" colspan="4">&nbsp;</td>
	    	</tr>
			<tr>
				<td class="textoCampo">Fecha Pago Desde:</td>
	  			<td class="textoDato">
					<@vc.rowCalendar 
					templateDir="custontemplates" 
					id="fechaDesde" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="fechaDesde" 
					title="Fecha Desde" />
				</td>
					<td class="textoCampo">Fecha Pago Hasta:</td>
	      			<td class="textoDato">
					<@vc.rowCalendar 
						templateDir="custontemplates" 
						id="fechaHasta" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="fechaHasta" 
						title="Fecha Hasta" />
					</td>
				</tr>
				<tr>
					<td class="textoCampo">Tipo de Cliente:</td>
	      			<td class="textoDato">
		      			<@s.select 
							templateDir="custontemplates" 
							id="cliente.tipo" 
							cssClass="textarea"
							cssStyle="width:165px" 
							name="cliente.tipo" 
							list="tipoClienteList" 
							listKey="key" 
							listValue="description" 
							value="cliente.tipo.ordinal()"
							title="Tipo de Cliente"
							headerKey="0"
							headerValue="Todos"							
							/>
					</td>
					<td class="textoCampo">Cliente:</td>
	      			<td class="textoDato">
						<@s.property default="&nbsp;" escape=false value="cliente.descripcion"/>
						<@s.a templateDir="custontemplates" id="seleccionarCliente" name="seleccionarCliente" href="javascript://nop/">
							<img src="${request.contextPath}/common/images/buscar.gif"  border="0" align="absmiddle" hspace="3" >
						</@s.a>		
					</td>
				</tr>
				<tr>
					<td class="textoCampo">Pagado:</td>
	      			<td class="textoDato">
						<@s.property default="&nbsp;" escape=false value="pagado" />
					</td>
					<td class="textoCampo">Proveedor:</td>
					<td class="textoDato">
						<@s.property default="&nbsp;" escape=false value="proveedor.detalleDePersona.razonSocial" /><@s.property default="&nbsp;" escape=false value="proveedor.detalleDePersona.nombre" />
						<@s.a templateDir="custontemplates" id="seleccionarProveedor" name="seleccionarProveedor" href="javascript://nop/">
							<img src="${request.contextPath}/common/images/buscar.gif" border="0" align="absmiddle" hspace="3" >
						</@s.a>		
					</td>
				</tr>

				<tr>					
	      			<td class="textoCampo">Rubro:</td>
	      			<td class="textoDato">
						<@s.select 
							templateDir="custontemplates" 
							id="rubroNivel0" 
							cssClass="textarea"
							cssStyle="width:165px" 
							name="rubroNivel0.oid" 
							list="rubroNivel0List" 
							listKey="oid" 
							listValue="descripcion" 
							value="rubroNivel0.oid"
							title="Rubro nivel 0"
							headerKey="0"
							headerValue="Todos"  
							 /> 
					</td>

					<td class="textoCampo">Rubro nivel 1:</td>
					<td class="textoDato">
						<@s.select 
							templateDir="custontemplates" 
							id="rubroNivel1" 
							cssClass="textarea"
							cssStyle="width:165px" 
							name="rubroNivel1.oid" 
							list="rubroNivel1List" 
							listKey="oid" 
							listValue="descripcion" 
							title="Rubro nivel 1"
							headerKey="0"
							headerValue="Todos"  
							 /> 
					</td>
				</tr>
					
				<tr>
					<td class="textoCampo">Rubro nivel 2:</td>
					<td class="textoDato">
						<@s.select 
							templateDir="custontemplates" 
							id="rubroNivel2" 
							cssClass="textarea"
							cssStyle="width:165px" 
							name="rubroNivel2.oid" 
							list="rubroNivel2List" 
							listKey="oid" 
							listValue="descripcion" 
							title="Rubro nivel 2"
							headerKey="0"
							headerValue="Todos"  
							 /> 
					</td>

					<td class="textoCampo">Rubro nivel 3:</td>
					<td class="textoDato">
						<@s.select 
							templateDir="custontemplates" 
							id="rubroNivel3" 
							cssClass="textarea"
							cssStyle="width:165px" 
							name="rubroNivel3.oid" 
							list="rubroNivel3List" 
							listKey="oid" 
							listValue="descripcion" 
							title="Rubro nivel 3"
							headerKey="0"
							headerValue="Todos"						  
							 />
					</td>
				</tr>

				<tr>
					<td  class="textoCampo">L&iacute;nea Presupuestaria:</td>
					<td class="textoDato">
						<@s.select 
							templateDir="custontemplates" 
							id="lineaPresupuestaria.oid" 
							cssClass="textarea"
							cssStyle="width:165px" 
							name="lineaPresupuestaria.oid" 
							list="lineaPresupuestariaList" 
							listKey="oid" 
							listValue="descripcion" 
							value="lineaPresupuestaria.oid"
							title="Linea Presupuestaria"
							headerKey="0"
							headerValue="Todas"							
							/>			
					</td>

					<td  class="textoCampo">Sub L&iacute;nea Presupuestaria:</td>
					<td class="textoDato">
		 			 	<@s.select 
							templateDir="custontemplates" 
							id="subLineaPresupuestaria.oid" 
							cssClass="textarea"
							cssStyle="width:165px" 
							name="subLineaPresupuestaria.oid" 
							list="subLineaPresupuestariaList" 
							listKey="oid" 
							listValue="descripcion" 
							value="subLineaPresupuestaria.oid"
							title="Sub Linea Presupuestaria"
							headerKey="0"
							headerValue="Todas"							
							/>			
					</td>
				</tr>	

				<tr>
		    		<td colspan="4" class="lineaGris" align="right">
		      			<input id="limpiar" type="button" name="btnVolver" value="Limpiar" class="boton"/>		
		      			<input id="buscar" type="button" name="btnVolver" value="Buscar" class="boton"/>
		    		</td>
				</tr>	
		</table>
	</div>
		
    <!-- Resultado Filtro -->
	<@s.if test="facturaClienteList != null">
				
		<#assign facturaClienteFechaDesde = "" />
		<@s.if test="fechaDesde != null">
			<#assign facturaClienteFechaDesde = "${fechaDesde?string('dd/MM/yyyy')}" />
		</@s.if>

		<#assign facturaClienteFechaHasta = "" />
		<@s.if test="fechaHasta != null">
			<#assign facturaClienteFechaHasta = "${fechaHasta?string('dd/MM/yyyy')}" />
		</@s.if>

		<#assign facturaClienteClienteTipo = "0" />
		<@s.if test="cliente != null && cliente.tipo != null">
			<#assign facturaClienteClienteTipo = "${cliente.tipo.ordinal()}" />
		</@s.if>

		<#assign facturaClienteClienteOid = "0" />
		<@s.if test="cliente != null && cliente.oid != null">
			<#assign facturaClienteClienteOid = "${cliente.oid}" />
		</@s.if>

		<#assign facturaClienteClienteDescripcion = "" />
		<@s.if test="cliente != null && cliente.descripcion != null">
			<#assign facturaClienteClienteDescripcion = "${cliente.descripcion}" />
		</@s.if>

		<#assign facturaClientePagado = "0" />
		<@s.if test="pagado != null">
			<#assign facturaClientePagado = "${pagado.ordinal()}" />
		</@s.if>

		<#assign facturaClienteProveedorNroProveedor = "" />
		<@s.if test="proveedor.nroProveedor != null">
			<#assign facturaClienteProveedorNroProveedor = "${proveedor.nroProveedor}" />
		</@s.if>

		<#assign facturaClienteProveedorRazonSocial = "" />
		<@s.if test="proveedor.detalleDePersona.razonSocial != null">
			<#assign facturaClienteProveedorRazonSocial = "${proveedor.detalleDePersona.razonSocial}" />
		</@s.if>

		<#assign facturaClienteProveedorNombre = "" />
		<@s.if test="proveedor.detalleDePersona.nombre != null">
			<#assign facturaClienteProveedorNombre= "${proveedor.detalleDePersona.nombre}" />
		</@s.if>

		<#assign rubroNivel0Oid = "0" />
		<@s.if test="rubroNivel0 != null && rubroNivel0.oid != null">
			<#assign rubroNivel0Oid = "${rubroNivel0.oid}" />
		</@s.if>

		<#assign rubroNivel1Oid = "0" />
		<@s.if test="rubroNivel1 != null && rubroNivel1.oid != null">
			<#assign rubroNivel1Oid = "${rubroNivel1.oid}" />
		</@s.if>

		<#assign rubroNivel2Oid = "0" />
		<@s.if test="rubroNivel2 != null && rubroNivel2.oid != null">
			<#assign rubroNivel2Oid = "${rubroNivel2.oid}" />
		</@s.if>

		<#assign rubroNivel3Oid = "0" />
		<@s.if test="rubroNivel3 != null && rubroNivel3.oid != null">
			<#assign rubroNivel3Oid = "${rubroNivel3.oid}" />
		</@s.if>

		<#assign lineaPresupuestariaOid = "0" />
		<@s.if test="lineaPresupuestaria != null && lineaPresupuestaria.oid != null">
			<#assign lineaPresupuestariaOid = "${lineaPresupuestaria.oid}" />
		</@s.if>

		<#assign subLineaPresupuestariaOid = "0" />
		<@s.if test="subLineaPresupuestaria != null && subLineaPresupuestaria.oid != null">
			<#assign subLineaPresupuestariaOid = "${subLineaPresupuestaria.oid}" />
		</@s.if>
		
		<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
			<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>Imputaciones encontradas</td>
					<td>
						<div class="alineacionDerecha">
							<@security.a 
									templateDir="custontemplates" 
									securityCode="CUF0821" 
									enabled="true" 
									cssClass="item" 
									href="${request.contextPath}/comprobante/consultaImputacionLinea/printConsultaImputacionLPXLS.action?fechaDesde=${facturaClienteFechaDesde}&fechaHasta=${facturaClienteFechaHasta}&cliente.tipo=${facturaClienteClienteTipo}&cliente.oid=${facturaClienteClienteOid}&cliente.descripcion=${facturaClienteClienteDescripcion}&pagado=${facturaClientePagado}&proveedor.nroProveedor=${facturaClienteProveedorNroProveedor}&proveedor.detalleDePersona.razonSocial=${facturaClienteProveedorRazonSocial}&proveedor.detalleDePersona.nombre=${facturaClienteProveedorNombre}&rubroNivel0.oid=${rubroNivel0Oid}&rubroNivel1.oid=${rubroNivel1Oid}&rubroNivel2.oid=${rubroNivel2Oid}&rubroNivel3.oid=${rubroNivel3Oid}&lineaPresupuestaria.oid=${lineaPresupuestariaOid}&subLineaPresupuestaria.oid=${subLineaPresupuestariaOid}">
									<img src="${request.contextPath}/common/images/imprimirExcel.gif" title="Exportar Consulta de Imputaciones por Linea Presupuestaria a Excel" align="absmiddle" border="0" hspace="3">
								</@security.a>
						</div>
						<div class="alineacionDerecha">	
							<b>Imprimir</b>
							<@security.a 
									templateDir="custontemplates" 
									securityCode="CUF0821" 
									enabled="true" 
									cssClass="item" 
									href="${request.contextPath}/comprobante/consultaImputacionLinea/printConsultaImputacionLPPDF.action?fechaDesde=${facturaClienteFechaDesde}&fechaHasta=${facturaClienteFechaHasta}&cliente.tipo=${facturaClienteClienteTipo}&cliente.oid=${facturaClienteClienteOid}&cliente.descripcion=${facturaClienteClienteDescripcion}&pagado=${facturaClientePagado}&proveedor.nroProveedor=${facturaClienteProveedorNroProveedor}&proveedor.detalleDePersona.razonSocial=${facturaClienteProveedorRazonSocial}&proveedor.detalleDePersona.nombre=${facturaClienteProveedorNombre}&rubroNivel0.oid=${rubroNivel0Oid}&rubroNivel1.oid=${rubroNivel1Oid}&rubroNivel2.oid=${rubroNivel2Oid}&rubroNivel3.oid=${rubroNivel3Oid}&lineaPresupuestaria.oid=${lineaPresupuestariaOid}&subLineaPresupuestaria.oid=${subLineaPresupuestariaOid}">
									<img src="${request.contextPath}/common/images/imprimirPdf.gif" title="Exportar Consulta de Imputaciones por Linea Presupuestaria a PDF" align="absmiddle" border="0" hspace="3">
								</@security.a>
						</div>		
					</td>
				</tr>
			</table>

			<@ajax.anchors target="contentTrx">			
          		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="facturaClienteList" id="facturaCliente" pagesize=15 partialList=true size="recordSize" keepStatus=true excludedParams="resetFlag">

					<@display.column headerClass="tbl-contract-service-select" class="botoneraAnchoCon1" title="Acciones">
						<div class="alineacion">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0643" 
							enabled="factura.readable" 
							cssClass="item" 
							href="${request.contextPath}/comprobante/factura/readView.action?factura.pkFactura.nro_factura=${facturaCliente.factura.pkFactura.nro_factura?c}&factura.pkFactura.nro_proveedor=${facturaCliente.factura.pkFactura.nro_proveedor?c}&factura.pkFactura.sucursal=${facturaCliente.factura.pkFactura.sucursal?c}&factura.pkFactura.tip_docum=${facturaCliente.factura.pkFactura.tip_docum?c}&navigationId=consultaImputacionLinea-visualizarFactura&flowControl=regis&navegacionIdBack=${navigationId}">
							<img src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver" border="0">
						</@security.a>
						</div>
					</@display.column>

					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="factura.lineaPresupuestaria.descripcion" title="L&iacute;nea Presupuestaria" />

					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="factura.fechaVencimiento" format="{0,date,dd/MM/yyyy}" title="Fecha Sugerida de Pago" />

					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="importe" format="{0,number,###0.00;-###0.00}" title="Importe" />				

					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" title="Suc - Nro.">
						${facturaCliente.factura.tipoComprobante.descripcion} N&#0176; ${facturaCliente.factura.numeroString}
					</@display.column>

					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" title="Proveedor" >											

						<#if facturaCliente.factura.proveedor?exists>
							<#if facturaCliente.factura.proveedor.detalleDePersona.nombre?exists>
								${facturaCliente.factura.proveedor.detalleDePersona.razonSocial} ${facturaCliente.factura.proveedor.detalleDePersona.nombre}
							<#else>
								${facturaCliente.factura.proveedor.detalleDePersona.razonSocial}
							</#if>
						</#if>

					</@display.column>

					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="factura.fechaEmision" format="{0,date,dd/MM/yyyy}" title="Fecha Emisi&oacute;n" />

					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="origenComprobante" title="Origen Comprobante" />

					<@display.footer>
						<tr>
							<td colspan=3><b>Total:</b></td>
							<td><b>${sumaTotal?string("###0.00;-###0.00")}</b></td>
						<tr>
					</@display.footer>

				</@display.table>
			</@ajax.anchors>
		</div>	
	</@s.if>
			
<@vc.htmlContent 
  baseUrl="${request.contextPath}/comprobante/consultaImputacionLinea/search.action"
  source="buscar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="resetFlag=true,navigationId={navigationId},flowControl=regis,fechaDesde={fechaDesde},fechaHasta={fechaHasta},cliente.tipo={cliente.tipo},cliente.oid={cliente.oid},cliente.descripcion={cliente.descripcion},pagado={pagado},proveedor.nroProveedor={proveedor.nroProveedor},proveedor.detalleDePersona.razonSocial={proveedor.detalleDePersona.razonSocial},proveedor.detalleDePersona.nombre={proveedor.detalleDePersona.nombre},rubroNivel0.oid={rubroNivel0},rubroNivel1.oid={rubroNivel1},rubroNivel2.oid={rubroNivel2},rubroNivel3.oid={rubroNivel3},lineaPresupuestaria.oid={lineaPresupuestaria.oid},subLineaPresupuestaria.oid={subLineaPresupuestaria.oid}"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/comprobante/consultaImputacionLinea/view.action"
  source="limpiar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=regis"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/comprobante/consultaImputacionLinea/selectCliente.action" 
  source="seleccionarCliente" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=change,navegacionIdBack={navigationId},fechaDesde={fechaDesde},fechaHasta={fechaHasta},cliente.tipo={cliente.tipo},cliente.oid={cliente.oid},cliente.descripcion={cliente.descripcion},pagado={pagado},proveedor.nroProveedor={proveedor.nroProveedor},proveedor.detalleDePersona.razonSocial={proveedor.detalleDePersona.razonSocial},proveedor.detalleDePersona.nombre={proveedor.detalleDePersona.nombre},rubroNivel0.oid={rubroNivel0},rubroNivel1.oid={rubroNivel1},rubroNivel2.oid={rubroNivel2},rubroNivel3.oid={rubroNivel3},lineaPresupuestaria.oid={lineaPresupuestaria.oid},subLineaPresupuestaria.oid={subLineaPresupuestaria.oid}"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/comprobante/consultaImputacionLinea/selectProveedor.action" 
  source="seleccionarProveedor" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=change,navegacionIdBack={navigationId},fechaDesde={fechaDesde},fechaHasta={fechaHasta},cliente.tipo={cliente.tipo},cliente.oid={cliente.oid},cliente.descripcion={cliente.descripcion},pagado={pagado},proveedor.nroProveedor={proveedor.nroProveedor},proveedor.detalleDePersona.razonSocial={proveedor.detalleDePersona.razonSocial},proveedor.detalleDePersona.nombre={proveedor.detalleDePersona.nombre},rubroNivel0.oid={rubroNivel0},rubroNivel1.oid={rubroNivel1},rubroNivel2.oid={rubroNivel2},rubroNivel3.oid={rubroNivel3},lineaPresupuestaria.oid={lineaPresupuestaria.oid},subLineaPresupuestaria.oid={subLineaPresupuestaria.oid}"/>

<@ajax.select
  baseUrl="${request.contextPath}/comprobante/consultaImputacionLinea/refrescarSubrubro1.action" 
  source="rubroNivel0" 
  target="rubroNivel1"
  parameters="rubroNivel0.oid={rubroNivel0}"
  parser="new ResponseXmlParser()"/>

<@ajax.select
  baseUrl="${request.contextPath}/comprobante/consultaImputacionLinea/refrescarSubrubro2.action" 
  source="rubroNivel1" 
  target="rubroNivel2"
  parameters="rubroNivel1.oid={rubroNivel1}"
  parser="new ResponseXmlParser()"/>
  
<@ajax.select
  baseUrl="${request.contextPath}/comprobante/consultaImputacionLinea/refrescarSubrubro3.action" 
  source="rubroNivel2" 
  target="rubroNivel3"
  parameters="rubroNivel2.oid={rubroNivel2}"
  parser="new ResponseXmlParser()"/>

<@ajax.select
  baseUrl="${request.contextPath}/comprobante/consultaImputacionLinea/refrescarSubLinea.action" 
  source="lineaPresupuestaria.oid" 
  target="subLineaPresupuestaria.oid"
  parameters="lineaPresupuestaria.oid={lineaPresupuestaria.oid}"
  parser="new ResponseXmlParser()"/>
