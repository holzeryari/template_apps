<#setting locale="es_AR">

<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>

<div id="capaSeparadora" class="capaSeparadora">&nbsp;</td></tr></div>

<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">

<@s.if test="contratosProveedor!=null && contratosProveedor.size()>0">
	
			<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr class="alturaFila">
					<td width="40%">Contratos, Acuerdos o Convenios del Proveedor</td>					
				</tr>
			</table>	
			
			<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="contratosProveedor" id="contrato" pagesize=15  defaultsort=2>
          		
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="oid" title="N&uacute;mero" />

					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="descripcion" title="Descripci&oacute;n" />				

					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="tipoDocumento" title="Tipo Documento" />

					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="tipoContrato" title="Tipo Contrato" />
<#--
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="cliente.descripcion" title="Cliente" />
-->
					<#-- muestra RazonSocial + Nombre de la Persona, independiente del TipoBeneficiario, debido a que un Proveedor es una Persona -->
<#--
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="nombreBeneficiario" title="Beneficiario" />
-->
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="estado" title="Estado" />		

			</@display.table>

</@s.if>
</div>