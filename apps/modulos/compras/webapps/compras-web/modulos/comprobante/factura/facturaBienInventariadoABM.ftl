<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign tiles=JspTaglibs["/WEB-INF/tlds/tiles-jsp.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<@s.hidden id="facturaBienInventariado.oid" name="facturaBienInventariado.oid"/>
<@s.hidden id="facturaBienInventariado.bienInventariado.oid" name="facturaBienInventariado.bienInventariado.oid"/>

<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
			<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>
	
	<@tiles.insertAttribute name="factura"/>

	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos del Bien Inventariado</b></td>
			</tr>
		</table>

		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>
			<tr>
      			<td class="textoCampo">N&uacute;mero del bien inventariado:</td>
      			<td class="textoDato">
      			<@s.property default="&nbsp;" escape=false value="facturaBienInventariado.bienInventariado.numeroInventario"/>
      			
      			</td>
      			<td class="textoCampo">Fecha Alta:</td>
				<td class="textoDato">
					<@s.if test="facturaBienInventariado.bienInventariado.fechaAlta != null">
						<#assign fechaAlta = facturaBienInventariado.bienInventariado.fechaAlta> 
						${fechaAlta?string("dd/MM/yyyy")}	
					</@s.if>	&nbsp;				
				</td>	      			
			</tr>	
			<tr>
				<td class="textoCampo">N&uacute;mero de Factura:</td>
      			<td class="textoDato">
      			<@s.property default="&nbsp;" escape=false value="facturaBienInventariado.bienInventariado.numeroFactura"/>
				     	
				</td>
				<td class="textoCampo">Valor Adquisici&oacute;n: </td>
				<td class="textoDato">
				<@s.property default="&nbsp;" escape=false value="facturaBienInventariado.bienInventariado.valorAdquisicion"/>
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Valor Amortizado: </td>
				<td class="textoDato" colspan="3">
					<@s.property default="&nbsp;" escape=false value="facturaBienInventariado.bienInventariado.valorAmortizado"/>
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Identificador:</td>
				<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="facturaBienInventariado.bienInventariado.numeroSerie"/>
				</td>
				<td class="textoCampo">Cliente:</td>
				<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="facturaBienInventariado.bienInventariado.cliente.descripcion"/>
				</td>
			</tr>
			<@s.if test="bienInventariado.fechaBaja != null">
				<tr>
			    	<td class="textoCampo">Fecha Baja: </td>
					<td class="textoDato">
						<#assign fechaBaja = facturaBienInventariado.bienInventariado.fechaBaja> 
						${fechaBaja?string("dd/MM/yyyy")}
								&nbsp;	
					</td>
					<td class="textoCampo">Tipo Baja:</td>
					<td class="textoDato">
							<@s.property default="&nbsp;" escape=false value="facturaBienInventariado.bienInventariado.tipoBaja"/>
					</td>
			    </tr>
  			    <tr>
					<td class="textoCampo">Motivo Baja:</td>
			      	<td  class="textoDato" colspan="3">
						<@s.property default="&nbsp;" escape=false value="facturaBienInventariado.bienInventariado.motivoBaja"/>
					</td>					
	    		</tr>
		</@s.if>
		<tr>
			<td class="textoCampo">Estado:</td>
			<td class="textoDato">
				<@s.property default="&nbsp;" escape=false value="facturaBienInventariado.bienInventariado.estado"/>
			</td>
			<td class="textoCampo">Tipo de Gasto:</td>
			<td class="textoDato">
				<@s.select 
					templateDir="custontemplates" 
					id="facturaBienInventariado.tipoGasto" 
					cssClass="textarea"
					cssStyle="width:165px" 
					name="facturaBienInventariado.tipoGasto" 
					list="tipoGastoList" 
					listKey="key" 
					listValue="description" 
					value="facturaBienInventariado.tipoGasto.ordinal()"
					title="Tipo de Gasto"
					headerKey="0"
					headerValue="Seleccionar"							
					/>
			</td>
		</tr>
		<tr><td class="textoCampo" colspan="4">&nbsp;</td></tr> 			
	</table>
	</div>	       	
			
		