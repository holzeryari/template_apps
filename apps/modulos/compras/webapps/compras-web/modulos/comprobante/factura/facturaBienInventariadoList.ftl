
<#setting locale="es_AR">

<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>

<#if factura.claseComprobante?exists && (factura.claseComprobante.ordinal()==1 || factura.claseComprobante.ordinal()==4)>

<#if factura.origenComprobante?exists>

<#if factura.origenComprobante.ordinal()=1 ||
 	 factura.origenComprobante.ordinal()=2 || 
	 factura.origenComprobante.ordinal()=6 ||
	 factura.origenComprobante.ordinal()=7
	 >

<@s.hidden id="navigationId" name="navigationId"/>
<div id="capaSeparadora" class="capaSeparadora">&nbsp;</div>
<@vc.anchors target="contentTrx">	
	<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
				<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
					<tr>
						<td>Bienes Inventariados</td>
						<td>	
							<div align="right">
								<@s.a href="${request.contextPath}/comprobante/factura/selectBienInventariado.action?factura.pkFactura.nro_proveedor=${factura.pkFactura.nro_proveedor?c}&factura.pkFactura.nro_factura=${factura.pkFactura.nro_factura?c}&factura.pkFactura.sucursal=${factura.pkFactura.sucursal?c}&factura.pkFactura.tip_docum=${factura.pkFactura.tip_docum?c}&factura.proveedor.nroProveedor=${factura.proveedor.nroProveedor?c}&navigationId=${navigationId}" templateDir="custontemplates" id="agregarFondoFijoSinComprobante" name="agregarFondoFijoSinComprobante" cssClass="ocultarIcono">
									<b>Agregar</b><img src="${request.contextPath}/common/images/agregar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
								</@s.a>		
							</div>
						</td>
					</tr>
				</table>	
	
				<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="factura.facturaBienInventariadoList" id="facturaBienInventariado" pagesize=15 defaultsort=1 >
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select"  class="botoneraAnchoCon3" title="Acciones">
					<div class="alineacion">				
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0081" 
							enabled="bienInventariado.readable" 
							cssClass="item" 
							id="ver"
							name="ver"
							href="${request.contextPath}/comprobante/factura/readFacturaBienInventariadoView.action?facturaBienInventariado.oid=${facturaBienInventariado.oid?c}&navegacionIdBack=${navigationId}&navigationId=ver-facturaBienInventariado&flowControl=regis">
							<img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver" border="0">
						</@security.a>
						</div>		
						<div class="alineacion">				
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0081" 
							enabled="bienInventariado.readable" 
							cssClass="item" 
							id="ver"
							name="ver"
							href="${request.contextPath}/comprobante/factura/updateFacturaBienInventariadoView.action?facturaBienInventariado.oid=${facturaBienInventariado.oid?c}&navegacionIdBack=${navigationId}&navigationId=update-facturaBienInventariado&flowControl=regis">
							<img  src="${request.contextPath}/common/images/modificar.gif" alt="Ver" title="Ver" border="0">
						</@security.a>
						</div>						
						<div class="alineacion">
						<@s.a 
							templateDir="custontemplates"
							cssClass="item"
							id="eliminar"
							name="eliminar"  
							href="${request.contextPath}/comprobante/factura/deleteFacturaBienInventariado.action?facturaBienInventariado.oid=${facturaBienInventariado.oid?c}&navegacionIdBack=${navigationId}">
							<img  src="${request.contextPath}/common/images/eliminar.gif" alt="Baja" title="Baja" border="0">
						</@s.a>									
						</div>
					</@display.column>
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="bienInventariado.numeroInventario" title="Nro. Inventario" />
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="bienInventariado.bien.descripcion" title="Bien" />
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="bienInventariado.fechaAlta" format="{0,date,dd/MM/yyyy}" title="Fecha Alta" />
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="bienInventariado.cliente.descripcion" title="Cliente" />
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="bienInventariado.estaEmbargado" title="Embargado" />
										
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="bienInventariado.estado" title="Estado" />
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="tipoGasto" title="Tipo de Gasto" />
					
				</@display.table>
			</div>	
		</@vc.anchors>
	</#if>
</#if>
</#if>