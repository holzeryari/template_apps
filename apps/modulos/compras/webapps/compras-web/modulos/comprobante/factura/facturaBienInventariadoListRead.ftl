<#setting locale="es_AR">

<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>


<#if factura.claseComprobante?exists && (factura.claseComprobante.ordinal()==1 || factura.claseComprobante.ordinal()==4)>
	<#if factura.origenComprobante?exists>
		<#if factura.origenComprobante.ordinal()=1 ||
	 	 factura.origenComprobante.ordinal()=2 || 
		 factura.origenComprobante.ordinal()=6 ||
		 factura.origenComprobante.ordinal()=7
		 >

		<@s.hidden id="navigationId" name="navigationId"/>
		<div id="capaSeparadora" class="capaSeparadora">&nbsp;</div>
		<@vc.anchors target="contentTrx">	
			<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
				<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
					<tr>
						<td>Bienes Inventariados</td>
					</tr>
				</table>	
			
				<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="factura.facturaBienInventariadoList" id="facturaBienInventariado">
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select"  class="botoneraAnchoCon1" title="Acciones">
					<div class="alineacion">				
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0081" 
							enabled="bienInventariado.readable" 
							cssClass="item" 
							id="ver"
							name="ver"
							href="${request.contextPath}/comprobante/factura/readFacturaBienInventariadoView.action?facturaBienInventariado.oid=${facturaBienInventariado.oid?c}&navegacionIdBack=${navigationId}&navigationId=ver-facturaBienInventariado&flowControl=regis">
							<img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver" border="0">
						</@security.a>
						</div>		
		
					</@display.column>
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="bienInventariado.numeroInventario" title="Nro. Inventario" />
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="bienInventariado.bien.descripcion" title="Bien" />
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="bienInventariado.fechaAlta" format="{0,date,dd/MM/yyyy}" title="Fecha Alta" />
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="bienInventariado.cliente.descripcion" title="Cliente" />
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="bienInventariado.estaEmbargado" title="Embargado" />
										
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="bienInventariado.estado" title="Estado" />
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="tipoGasto" title="Tipo de Gasto" />
					
				</@display.table>
			</div>
		</@vc.anchors>
	</#if>
</#if>
</#if>