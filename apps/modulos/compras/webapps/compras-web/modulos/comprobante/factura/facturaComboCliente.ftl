<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<td class="textoDato">
	<@s.select 
		templateDir="custontemplates" 
		id="factura.cliente.oid" 
		cssClass="textarea"
		cssStyle="width:250px" 
		name="factura.cliente.oid"
		value="factura.cliente.oid"  
		list="clienteList" 
		listKey="oid" 
		listValue="descripcion"		
		title="Cliente"
		headerKey="" 
		headerValue="Seleccionar" />
</td>