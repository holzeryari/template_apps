<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>
<@vc.anchors target="contentTrx" ajaxFlag="ajax">

<div id="capaSeparadora" class="capaSeparadora">&nbsp;</div>
<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
				<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
					<tr>
						<td>Contratos</td>
						<td>	
							<div align="right">
								<@s.a href="${request.contextPath}/comprobante/factura/selectContrato.action?factura.pkFactura.nro_factura=${factura.pkFactura.nro_factura?c}&factura.pkFactura.nro_proveedor=${factura.pkFactura.nro_proveedor?c}&factura.pkFactura.sucursal=${factura.pkFactura.sucursal?c}&factura.pkFactura.tip_docum=${factura.pkFactura.tip_docum?c}&factura.proveedor.nroProveedor=${factura.proveedor.nroProveedor?c}&factura.proveedor.detalleDePersona.razonSocial=${factura.proveedor.detalleDePersona.razonSocial}&navigationId=${navigationId}" templateDir="custontemplates" id="agregarIngresoProductoDetalle" name="agregarIngresoProductoDetalle" cssClass="ocultarIcono">
									<b>Agregar</b><img src="${request.contextPath}/common/images/agregar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
								</@s.a>	
							</div>
						</td>
					</tr>
				</table>	

            	<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="factura.contratoList" id="contrato" pagesize=15  defaultsort=2>
        			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select" class="botoneraAnchoCon2" title="Acciones">
						<div class="alineacion">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0523" 
							enabled="contrato.readable" 
							cssClass="item" 
							id="ver"
							name="ver"
							href="${request.contextPath}/contrato/readContratoView.action?contrato.oid=${contrato.oid?c}&navigationId=contrato-visualizar&flowControl=regis&navegacionIdBack=${navigationId}">
							<img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver"  border="0">
						</@security.a>
						</div>
						<div class="alineacion">
						<@s.a 
							templateDir="custontemplates" 
							id="eliminar"
							name="eliminar"
							cssClass="item"  
							href="${request.contextPath}/comprobante/factura/deleteContrato.action?contrato.oid=${contrato.oid?c}&factura.pkFactura.nro_factura=${factura.pkFactura.nro_factura?c}&factura.pkFactura.nro_proveedor=${factura.pkFactura.nro_proveedor?c}&factura.pkFactura.sucursal=${factura.pkFactura.sucursal?c}&factura.pkFactura.tip_docum=${factura.pkFactura.tip_docum?c}&navigationId=contrato-delete&flowControl=regis">
							<img  src="${request.contextPath}/common/images/eliminar.gif" alt="Eliminar" title="Eliminar"  border="0">
						</@s.a>				
						</div>
					</@display.column>


					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="oid" title="N&uacute;mero" />

					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="descripcion" title="Descripci&oacute;n" />				

					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="tipoDocumento" title="Tipo Documento" />

					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="tipoContrato" title="Tipo Contrato" />
<#--
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="cliente.descripcion" title="Cliente" />
-->
					<#-- muestra RazonSocial + Nombre de la Persona, independiente del TipoBeneficiario, debido a que un Proveedor es una Persona -->
<#--
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="nombreBeneficiario" title="Beneficiario" />
-->
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="estado" title="Estado" />		

				</@display.table>	
		
		</div>
</@vc.anchors>

