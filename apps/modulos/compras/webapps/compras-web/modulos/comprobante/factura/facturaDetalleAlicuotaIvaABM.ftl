<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign tiles=JspTaglibs["/WEB-INF/tlds/tiles-jsp.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<@s.hidden id="facturaDetalleAlicuotaIVA.facturaDetalle.oid" name="facturaDetalleAlicuotaIVA.facturaDetalle.oid"/>
<@s.hidden id="facturaDetalleAlicuotaIVA.oid" name="facturaDetalleAlicuotaIVA.oid"/>
<@s.hidden id="facturaDetalleAlicuotaIVA.facturaDetalle.precioUnitario" name="facturaDetalleAlicuotaIVA.facturaDetalle.precioUnitario"/>
<@s.hidden id="facturaDetalleAlicuotaIVA.facturaDetalle.importeGravado" name="facturaDetalleAlicuotaIVA.facturaDetalle.importeGravado"/>

<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
			<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>
	
	<@tiles.insertAttribute name="factura"/>
		
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos de la L&iacute;nea de Factura</b></td>
			</tr>
		</table>

		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>
			<tr>					      			
				<#if facturaDetalleAlicuotaIVA.facturaDetalle.productoBien?exists>
					<td  class="textoCampo">Producto/Bien:</td>
					<td class="textoDato" colspan="3">
						<@s.property default="&nbsp;" escape=false value="facturaDetalleAlicuotaIVA.facturaDetalle.productoBien.descripcion"/>
					</td>										
				</#if>		
				<#if facturaDetalleAlicuotaIVA.facturaDetalle.servicio?exists>
					<td  class="textoCampo">Servicio:</td>
					<td class="textoDato" colspan="3">
						<@s.property default="&nbsp;" escape=false value="facturaDetalleAlicuotaIVA.facturaDetalle.servicio.descripcion"/>
					</td>
				</#if>		
			</tr>	
			<tr>						
				<td class="textoCampo">Rubro:</td>
				<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="facturaDetalleAlicuotaIVA.facturaDetalle.productoBien.rubro.descripcion"/>
				</td>
									
				<td class="textoCampo">Cuenta Contable:</td>
				<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="facturaDetalleAlicuotaIVA.facturaDetalle.cuentaContable.codigo"/>					 			 			
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Cantidad Pedida: </td>
				<td  class="textoDato">
					<@s.property default="&nbsp;" escape=false value="facturaDetalleAlicuotaIVA.facturaDetalle.cantidadPedida"/>												      			
				</td>
				<td class="textoCampo">Cantidad Facturada:</td>
				<td  class="textoDato">									
					<@s.property default="&nbsp;" escape=false value="facturaDetalleAlicuotaIVA.facturaDetalle.cantidadFacturada"/>
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Precio Cotizado: </td>
				<td  class="textoDato">
					<@s.if test="facturaDetalleAlicuotaIVA.facturaDetalle.ultimoPrecioCotizado != null">
						<#assign ultimoPrecioCotizado = facturaDetalleAlicuotaIVA.facturaDetalle.ultimoPrecioCotizado> 
						${ultimoPrecioCotizado?string(",##0.00")}	
						</@s.if>	&nbsp;																      			
				</td>
				<td class="textoCampo">Precio Unitario:</td>
				<td  class="textoDato">									
					<@s.if test="facturaDetalleAlicuotaIVA.facturaDetalle.precioUnitario != null">
						<#assign precioUnitario = facturaDetalleAlicuotaIVA.facturaDetalle.precioUnitario> 
						${precioUnitario?string(",##0.00")}	
						</@s.if>	&nbsp;				
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Precio Final:</td>
				<td  class="textoDato">
					<@s.if test="facturaDetalleAlicuotaIVA.facturaDetalle.precioFinal != null">
						<#assign precioFinal = facturaDetalleAlicuotaIVA.facturaDetalle.precioFinal> 
						${precioFinal?string(",##0.00")}	
						</@s.if>	&nbsp;												      			
				</td>
				<td class="textoCampo">Exento: </td>
				<td  class="textoDato">
					<@s.if test="facturaDetalleAlicuotaIVA.facturaDetalle.importeExento != null">
						<#assign importeExento = facturaDetalleAlicuotaIVA.facturaDetalle.importeExento> 
						${importeExento?string(",##0.00")}	
						</@s.if>	&nbsp;
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Importe Gravado:</td>
				<td  class="textoDato">
					<@s.if test="facturaDetalleAlicuotaIVA.facturaDetalle.importeGravado != null">
						<#assign importeGravado = facturaDetalleAlicuotaIVA.facturaDetalle.importeGravado> 
						${importeGravado?string(",##0.00")}	
					</@s.if>	&nbsp;												      			
				</td>
				<td class="textoCampo">Importe No Gravado: </td>
				<td  class="textoDato">
					<@s.if test="facturaDetalleAlicuotaIVA.facturaDetalle.importeNoGravado != null">
						<#assign importeNoGravado = facturaDetalleAlicuotaIVA.facturaDetalle.importeNoGravado> 
						${importeNoGravado?string(",##0.00")}	
					</@s.if>	&nbsp;
				</td>
			</tr>
			<tr><td colspan="4" class="textoCampo">&nbsp;</td></tr>								
		</table>
	</div>	
		
	<#if modoAdministrar?exists && modoAdministrar>	
		<@vc.anchors target="contentTrx">
			<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
				<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
            		<tr>
						<td>Al&iacute;cuotas IVA</td>
						<td>	
							<div align="right">
								<@s.a								  
									templateDir="custontemplates"	
									cssClass="item"  
									id="crear"
									name="crear"
									href="${request.contextPath}/comprobante/factura/createFacturaDetalleAlicuotaView.action?facturaDetalleAlicuotaIVA.facturaDetalle.oid=${facturaDetalleAlicuotaIVA.facturaDetalle.oid?c}&navigationId=createAlicuota&flowControl=regis&navegacionIdBack=${navigationId}">
									Agregar
									<img  src="${request.contextPath}/common/images/agregar.gif" alt="Modificar" title="Modificar"  border="0">
								</@s.a>	
							</div>
						</td>				
					</tr>
				</table>
				
				<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="facturaDetalleAlicuotaIVA.facturaDetalle.alicuotaIVAList" id="alicuotaIVA" pagesize=15 defaultsort=2 >
        			<@display.column headerClass="tbl-contract-service-select" class="botoneraAnchoCon2" title="Acciones">
						<@s.a  
							templateDir="custontemplates"	
							cssClass="item"  
							id="modificar"
							name="modificar"
							href="${request.contextPath}/comprobante/factura/updateFacturaDetalleAlicuotaView.action?facturaDetalleAlicuotaIVA.oid=${alicuotaIVA.oid?c}&facturaDetalleAlicuotaIVA.facturaDetalle.oid=${alicuotaIVA.facturaDetalle.oid?c}&navigationId=updateAlicuota&flowControl=regis&navegacionIdBack=${navigationId}">
							<img  src="${request.contextPath}/common/images/modificar.gif" alt="Modificar" title="Modificar"  border="0">
						</@s.a>	
				
						<@s.a  
							templateDir="custontemplates"	
							cssClass="item"  
							id="eliminar"
							name="eliminar"
							href="${request.contextPath}/comprobante/factura/deleteFacturaDetalleAlicuota.action?facturaDetalleAlicuotaIVA.oid=${alicuotaIVA.oid?c}&facturaDetalleAlicuotaIVA.facturaDetalle.oid=${alicuotaIVA.facturaDetalle.oid?c}&navegacionIdBack=${navigationId}">
							<img  src="${request.contextPath}/common/images/eliminar.gif" alt="Eliminar" title="Eliminar"  border="0">
						</@s.a>	
					</@display.column>


					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero"  property="alicuotaIVA.porcentaje" class="estiloNumero" title="Porcentaje"/>
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero"  property="montoIVA" class="estiloNumero" title="Monto Iva"/>
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero"  property="tipoImputacionIVA" class="estiloTexto" title="Tipo Imputacion"/>
					
				</@display.table>
			</div>
		</@vc.anchors>
	<#else>
		<#-- abm alicuota -->
		<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">

		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Al&iacute;cuotas IVA</b></td>
			</tr>
		</table>
		

		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>
			<tr>
				<td class="textoCampo">Porcentaje:</td>
				<td  class="textoDato">
 					<@s.select 
						templateDir="custontemplates" 
						id="facturaDetalleAlicuotaIVA.alicuotaIVA.oid" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="facturaDetalleAlicuotaIVA.alicuotaIVA.oid" 
						list="alicuotaIVAList" 
						listKey="oid" 
						listValue="porcentaje" 
						value="facturaDetalleAlicuotaIVA.alicuotaIVA.oid"
						title="Porcentaje IVA"
						headerKey="0"
						headerValue="Seleccionar"	
						onchange="javascript:calcularMontoIVA(this);" 						
						/>								      			
				</td>
				<td class="textoCampo">Tipo Imputaci&oacute;n IVA: </td>
				<td  class="textoDato">
				<@s.select 
						templateDir="custontemplates" 
						id="facturaDetalleAlicuotaIVA.tipoImputacionIVA" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="facturaDetalleAlicuotaIVA.tipoImputacionIVA" 
						list="tipoImputacionList" 
						listKey="key" 
						listValue="description" 
						value="facturaDetalleAlicuotaIVA.tipoImputacionIVA.ordinal()"
						title="Tipo Imputacion IVA"
						headerKey="0"
						headerValue="Seleccionar"
													
						/>																					      			
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Monto IVA:</td>
				<td  class="textoDato" colspan="3">
						<@s.textfield									
						templateDir="custontemplates"						
						id="facturaDetalleAlicuotaIVA.montoIVA" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="facturaDetalleAlicuotaIVA.montoIVA" 
						title="Monto IVA" />															      			
				</td>							
			</tr>
		</table>
		</div>		
	</#if>	
	