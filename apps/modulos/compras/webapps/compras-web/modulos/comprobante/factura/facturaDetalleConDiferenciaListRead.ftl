<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>


<@vc.anchors target="contentTrx" ajaxFlag="ajax">
	<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
		<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">		
			<tr>
				<td>Lineas del Comprobante Con Diferencias</td>
			</tr>
		</table>	
		
		<table class="tablaDetalleCuerpo" cellpadding="3" >
			<tr>
				<th align="center" class="botoneraAnchoCon1" >Acciones</th>
				<th align="center">Numero</th>						
				<th align="center" >
					<#if factura.origenComprobante.ordinal()==1>
						Producto/Bien
					</#if>
					<#if factura.origenComprobante.ordinal()==2>
						Servicio
					</#if>
						
				</th>						
				<th align="center">Rubro</th>
				<th align="center">Cta. Contable</th>																								
				<th align="center">Cant. Fac.</th>			
				<th align="center">Precio Uni. Cotizado</th>			
				<th align="center">Precio Uni. Facturado</th>							
				<#if discriminaIva?exists && discriminaIva>
					<th colspan="2" align="center">Alicuotas</th>
				</#if>						
			</tr>
			<#assign facturaDetalleLista = lineaDetalleConDiferenciaList>
			<#list facturaDetalleLista as facturaDetalle>
				<tr>
					<#assign cantFilas=1>
					<#if (facturaDetalle.alicuotaIVAList.size()>0)>
						<#assign cantFilas=facturaDetalle.alicuotaIVAList.size()>
					</#if>
					<td rowspan="${cantFilas}" class="botoneraAnchoCon1" > 
					<div class="alineacion">
					<@s.a 				
						templateDir="custontemplates"
						cssClass="item" 
						id="ver"
						name="ver"
						href="${request.contextPath}/comprobante/factura/readDetalleProductoBienView.action?facturaDetalle.oid=${facturaDetalle.oid?c}&navigationId=facturaDetalle-ver&flowControl=regis&navegacionIdBack=${navigationId}">
						<img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver"  border="0">
					</@s.a>
					</div>
					</td>	
					<td rowspan="${cantFilas}" class="estiloNumero"><#if facturaDetalle.numero?exists>${facturaDetalle.numero}<#else>&nbsp;</#if></td>
					<td rowspan="${cantFilas}" class="estiloTexto">
					<#if facturaDetalle.productoBien?exists>${facturaDetalle.productoBien.descripcion}<#else>&nbsp;</#if>
					<#if facturaDetalle.servicio?exists>${facturaDetalle.servicio.descripcion}<#else>&nbsp;</#if>
					</td>
					<td rowspan="${cantFilas}"  class="estiloTexto"><#if facturaDetalle.rubro.descripcion?exists>${facturaDetalle.rubro.descripcion}<#else>&nbsp;</#if></td>
					<td rowspan="${cantFilas}" class="estiloTexto"><#if facturaDetalle.cuentaContable?exists>${facturaDetalle.cuentaContable.descripcion}<#else>&nbsp;</#if></td>																						   
					<td rowspan="${cantFilas}"  class="estiloNumero"><#if facturaDetalle.cantidadFacturada?exists>${facturaDetalle.cantidadFacturada}<#else>&nbsp;</#if></td>								
					<td class="estiloNumero">								
					<#assign detOCOSLista = facturaDetalle.ocosDetalleList>
					<#list detOCOSLista as detOCOS>
					$${detOCOS.precio}<br>
					</#list>
					</td>
					<td rowspan="${cantFilas}" class="estiloNumero"><#if facturaDetalle.precioUnitario?exists>$${facturaDetalle.precioUnitario}<#else>&nbsp;</#if></td>								
						
					<#-- Alicuotas -->
					<#if discriminaIva?exists && discriminaIva>
						<#assign alicuotaLista = facturaDetalle.alicuotaIVAList>
						<#assign primero=false>
						<#list alicuotaLista as facturaDetalleAlicuota>									
							<#if primero=false>	
								<td class="estiloNumero">${facturaDetalleAlicuota.alicuotaIVA.porcentaje}%</td>
								<#assign primero=true>
							</#if>
						</#list>
					</#if>
					<#-- End Alicuotas -->								
				</tr>
				<#if discriminaIva?exists && discriminaIva>
					<#assign primero=false>
					<#assign alicuotaLista = facturaDetalle.alicuotaIVAList>							
					<#list alicuotaLista as facturaDetalleAlicuota>
						<#if primero=false>
							<#assign primero=true>
						<#else>
							<tr><td class="estiloNumero">${facturaDetalleAlicuota.alicuotaIVA.porcentaje}%</td></tr>
						</#if>
					</#list>
				</#if>	

		</#list>			
	</table>
</div>

</@vc.anchors>

