<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign tiles=JspTaglibs["/WEB-INF/tlds/tiles-jsp.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<@s.hidden id="facturaDetalle.oid" name="facturaDetalle.oid"/>

<#if cuentaContableList.size()==1>
	<@s.hidden id="facturaDetalle.cuentaContable.codigo" name="facturaDetalle.cuentaContable.codigo"/>
</#if>

<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
			<#if mensajeAviso?exists && (mensajeAviso.length()>0)>
				<tr>
					<td class="estiloMensajeAviso"><@s.text name="${mensajeAviso}"/></td>
				</tr>
			</#if>
			<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>
			
	<@tiles.insertAttribute name="factura"/>

	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos de la L&iacute;nea de Factura</b></td>
				<td>	
					<div align="right">
						<@s.a href="javascript://nop/"
							 templateDir="custontemplates" id="modificarDetalle" name="modificarDetalle" cssClass="ocultarIcono">
							<b>Modificar</b><img src="${request.contextPath}/common/images/modificar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
						</@s.a>		
					</div>
				</td>
			</tr>
		</table>

		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>
			<tr>					      			
      			<td  class="textoCampo">Producto/Bien:</td>
				<td class="textoDato" colspan="3">
				<@s.property default="&nbsp;" escape=false value="facturaDetalle.productoBien.descripcion"/>		
				</td>	      			
			</tr>	
			<tr>						
				<td class="textoCampo">Rubro:</td>
      			<td class="textoDato">
      				<@s.property default="&nbsp;" escape=false value="facturaDetalle.productoBien.rubro.descripcion"/>
				</td>
				
				<td class="textoCampo">Cuenta Contable:</td>
      			<td class="textoDato">
 			 	<@s.select 
						templateDir="custontemplates" 
						id="facturaDetalle.cuentaContable.codigo" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="facturaDetalle.cuentaContable.codigo" 
						list="cuentaContableList" 
						listKey="codigo" 
						listValue="codigo + '-' + descripcion" 
						value="facturaDetalle.cuentaContable.codigo"
						title="Cuenta Contable"
						headerKey="0"
						headerValue="Seleccionar"							
						/>		
				</td>
				
    		</tr>
    		<tr>
				<td class="textoCampo">Cantidad Pedida: </td>
				<td  class="textoDato" align="left">
				<@s.property default="&nbsp;" escape=false value="facturaDetalle.cantidadPedida"/>												      			
				</td>
				<td class="textoCampo">Cantidad Facturada:</td>
				<td  class="textoDato">									
					<@s.textfield									
					templateDir="custontemplates"
					template="textMoney"  
					id="facturaDetalle.cantidadFacturada" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="facturaDetalle.cantidadFacturada" 
					title="Cantidad Facturada" />						      			
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Precio Cotizado: </td>
				<td  class="textoDato">
					<@s.if test="facturaDetalle.ultimoPrecioCotizado != null">
						<#assign ultimoPrecioCotizado = facturaDetalle.ultimoPrecioCotizado> 
						${ultimoPrecioCotizado?string(",##0.00")}	
						</@s.if>	&nbsp;												      			
				</td>
				<td class="textoCampo">Precio Unitario:</td>
				<td  class="textoDato">									
					<@s.textfield									
						templateDir="custontemplates"
						template="textMoney"  
						id="facturaDetalle.precioUnitario" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="facturaDetalle.precioUnitario" 
						title="Precio Unitario" 
						onchange="javascript:calcularImporteGravado(this);" />							      			
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Precio Final:</td>
				<td  class="textoDato">
					
						<@s.if test="facturaDetalle.precioFinal != null">
						<#assign precio = facturaDetalle.precioFinal> 
						${precio?string(",##0.00")}	
						</@s.if>	&nbsp;	
				</td>
				
				<td class="textoCampo">Exento: </td>
				<td  class="textoDato">
				
						<@s.if test="facturaDetalle.importeExento != null">
						<#assign importeExento = facturaDetalle.importeExento> 
						${importeExento?string(",##0.00")}	
						</@s.if>	&nbsp;													      			
				</td>
			</tr>
				
			<tr>
				<td class="textoCampo">Importe Gravado: </td>
				<td  class="textoDato">
						<@s.textfield									
						templateDir="custontemplates"
						template="textMoney"  
						id="facturaDetalle.importeGravado" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="facturaDetalle.importeGravado" 
						title="Importe Gravado"
						onchange="javascript:calcularImporteNoGravado(this);" />															      			
				</td>
				<td class="textoCampo">Importe No Gravado:</td>
				<td  class="textoDato">
				<@s.textfield									
						templateDir="custontemplates"
						template="textMoney"  
						id="facturaDetalle.importeNoGravado" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="facturaDetalle.importeNoGravado" 
						title="Importe No Gravado" />
				</td>
			</tr>
			<tr><td colspan="4" class="textoCampo">&nbsp;</td></tr>								
		</table>
	</div>	
