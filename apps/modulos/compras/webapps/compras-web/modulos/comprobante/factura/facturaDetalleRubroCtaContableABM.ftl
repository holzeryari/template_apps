<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign tiles=JspTaglibs["/WEB-INF/tlds/tiles-jsp.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<@s.hidden id="facturaDetalle.oid" name="facturaDetalle.oid"/>
<@s.hidden id="navigationId" name="navigationId"/>

<@s.hidden id="facturaDetalle.rubro.oid" name="facturaDetalle.rubro.oid"/>
<@s.hidden id="facturaDetalle.rubro.descripcion" name="facturaDetalle.rubro.descripcion"/>

<#if cuentaContableList.size()==1>
	<@s.hidden id="facturaDetalle.cuentaContable.codigo" name="facturaDetalle.cuentaContable.codigo"/>
</#if>

<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
			<#if mensajeAviso?exists && (mensajeAviso.length()>0)>
				<tr>
					<td class="estiloMensajeAviso"><@s.text name="${mensajeAviso}"/></td>
				</tr>
			</#if>
			<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>
	
	<@tiles.insertAttribute name="factura"/>
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos de la L&iacute;nea de Factura</b></td>
			</tr>
		</table>

		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>
			<tr>						
				<td class="textoCampo">Concepto:</td>
				<td class="textoDato">
					 <@s.textfield 
	  					templateDir="custontemplates" 
						id="facturaDetalle.descripcion" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="facturaDetalle.descripcion" 
						title="Descripcion" />			
					</td>
									
					<td class="textoCampo">Rubro:</td>
					<td class="textoDato">
						<@s.property default="&nbsp;" escape=false value="facturaDetalle.rubro.descripcion"/>
						<@s.a templateDir="custontemplates" id="seleccionarRubro" name="seleccionarRubro" href="javascript://nop/" cssClass="ocultarIcono">
							<img src="${request.contextPath}/common/images/buscar.gif" border="0" align="absmiddle" hspace="3" >
						</@s.a>		
					</td>
				</tr>
				<tr>
					<td class="textoCampo">Cuenta Contable:</td>
					<td class="textoDato">
						<@s.select 
							templateDir="custontemplates" 
							id="facturaDetalle.cuentaContable.codigo" 
							cssClass="textarea"
							cssStyle="width:165px" 
							name="facturaDetalle.cuentaContable.codigo" 
							list="cuentaContableList" 
							listKey="codigo" 
							listValue="codigo + '-' + descripcion" 
							value="facturaDetalle.cuentaContable.codigo"
							title="Cuenta Contable"
							headerKey="0"
							headerValue="Seleccionar"							
						/>		
					</td>									
					<td class="textoCampo">Exento: </td>
					<td  class="textoDato">
						<@s.textfield									
							templateDir="custontemplates"
							template="textMoney"  
							id="facturaDetalle.importeExento" 
							cssClass="textarea"
							cssStyle="width:160px" 
							name="facturaDetalle.importeExento" 
							title="Importe Exento" />																      			
					</td>
				</tr>
						
				<tr>
					<td class="textoCampo">Importe Gravado: </td>
					<td  class="textoDato">
					<@s.textfield									
						templateDir="custontemplates"
						template="textMoney"  
						id="facturaDetalle.importeGravado" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="facturaDetalle.importeGravado" 
						title="Importe Gravado"
						onchange="javascript:calcularImporteNoGravado(this);" />															      			
					</td>
					<td class="textoCampo">Importe No Gravado:</td>
					<td  class="textoDato">
						<@s.textfield									
								templateDir="custontemplates"
								template="textMoney"  
								id="facturaDetalle.importeNoGravado" 
								cssClass="textarea"
								cssStyle="width:160px" 
								name="facturaDetalle.importeNoGravado" 
								title="Importe No Gravado" />
						</td>
				</tr>
				<tr><td colspan="4" class="textoCampo">&nbsp;</td></tr>									
				</table>
		</div>		
				
			
				
				       	
			
		