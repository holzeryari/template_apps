<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancel" type="button" name="btnCancelar" value="Cancelar" class="boton"/>
				<input id="store" type="button" name="btnAgregar" value="Agregar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/comprobante/factura/createDetalleRubroCtaContable.action" 
  source="store" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="facturaDetalle.cuentaContable.codigo={facturaDetalle.cuentaContable.codigo},facturaDetalle.rubro.oid={facturaDetalle.rubro.oid},facturaDetalle.importeNoGravado={facturaDetalle.importeNoGravado},facturaDetalle.importeGravado={facturaDetalle.importeGravado},facturaDetalle.importeExento={facturaDetalle.importeExento},facturaDetalle.descripcion={facturaDetalle.descripcion}"/>
  
 
 <@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=factura-administrar,flowControl=back"/>
  
 
   
<@vc.htmlContent 
  baseUrl="${request.contextPath}/comprobante/factura/selectRubro.action" 
  source="seleccionarRubro" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=change,facturaDetalle.cuentaContable.codigo={facturaDetalle.cuentaContable.codigo},facturaDetalle.rubro.oid={facturaDetalle.rubro.oid},facturaDetalle.importeNoGravado={facturaDetalle.importeNoGravado},facturaDetalle.importeGravado={facturaDetalle.importeGravado},navegacionIdBack=${navigationId},facturaDetalle.descripcion={facturaDetalle.descripcion},facturaDetalle.rubro.descripcion={facturaDetalle.rubro.descripcion},navegacionIdBack={navigationId}"/>
 