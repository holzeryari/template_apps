<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="left">
				<input id="cancelar" type="button" name="btnVolver" value="Volver" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action"  
  source="cancelar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=${navegacionIdBack},flowControl=back"/>

    <@vc.htmlContent 
  baseUrl="${request.contextPath}/comprobante/factura/updateDetalleServicioView.action" 
  source="modificarDetalle" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="facturaDetalle.oid=${facturaDetalle.oid?c},navigationId=facturaDetalle-modificar,flowControl=regis,navegacionIdBack=${navigationId}"/>
  