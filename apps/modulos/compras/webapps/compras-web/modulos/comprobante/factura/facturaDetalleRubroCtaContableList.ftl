<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#--
Si el origen del comprobante es por contrato, entonces no dejo ni agrgar lineas ni eliminarlas
solo puede eliminar y aregar contratos. 
-->
<#assign puedeEditarLineas="true">
<#if factura.origenComprobante.ordinal()==5 && (factura.claseComprobante.ordinal()==1 || factura.claseComprobante.ordinal()==4)>
	<#assign puedeEditarLineas="false">
</#if>

<div id="capaSeparadora" class="capaSeparadora">&nbsp;</td></tr></div>
<@vc.anchors target="contentTrx" ajaxFlag="ajax">
	<#if (factura.facturaDetalleList.size()>0)>
	<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
				<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
					<tr>
						<td>L&iacute;neas del Comprobante</td>
						<#if puedeEditarLineas=="true">
						<td>	
							<div align="right">
								<@s.a href="${request.contextPath}/comprobante/factura/preCreateFacturaDetalleRubroCtaContable.action?factura.pkFactura.nro_proveedor=${factura.pkFactura.nro_proveedor?c}&factura.pkFactura.nro_factura=${factura.pkFactura.nro_factura?c}&factura.pkFactura.sucursal=${factura.pkFactura.sucursal?c}&factura.pkFactura.tip_docum=${factura.pkFactura.tip_docum?c}&factura.proveedor.nroProveedor=${factura.proveedor.nroProveedor?c}&navigationId=facturaDetalle-create" templateDir="custontemplates" id="agregarProductoBien" name="agregarProductoBien" cssClass="ocultarIcono">
								<b>Agregar</b><img src="${request.contextPath}/common/images/agregar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
								</@s.a>		
							</div>
						</td>
						</#if>
					</tr>
				</table>	
				<table class="tablaDetalleCuerpo" cellpadding="3" >
					<#if discriminaIva?exists && !discriminaIva>
						<tr>
							<th align="center"  class="botoneraAnchoCon3" >Acciones</th>
							<th align="center">Numero</th>						
							<th align="center">Concepto</th>
							<th align="center">Rubro</th>
							<th align="center">Cta. Contable</th>
							<th align="center">Exento</th>	
							<th align="center">Grav.</th>		
							<th align="center">No Grav.</th>
						</tr>
					</#if>
						<#assign facturaDetalleLista = factura.facturaDetalleList>
						<#list facturaDetalleLista as facturaDetalle>
							<#if discriminaIva?exists && discriminaIva>
								<tr>
									<th align="center"  class="botoneraAnchoCon3" >Acciones</th>
									<th align="center">Numero</th>						
									<th align="center">Descripcion</th>
									<th align="center">Rubro</th>
									<th align="center">Cta. Contable</th>
									<th align="center">Exento</th>	
									<th align="center">Grav.</th>		
									<th align="center">No Grav.</th>
								</tr>
							</#if>
							<tr>
								<td class="botoneraAnchoCon3" > 
									<div class="alineacion">
										<@s.a 				
											templateDir="custontemplates"
											cssClass="item" 
											id="ver"
											name="ver"
											href="${request.contextPath}/comprobante/factura/readDetalleRubroCtaContableView.action?facturaDetalle.oid=${facturaDetalle.oid?c}&facturaRubroCtaContable=true&navigationId=facturaDetalle-ver&flowControl=regis&navegacionIdBack=${navigationId}">
											<img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver"  border="0">
										</@s.a>
											</div>

											<div class="alineacion">
												<@s.a  
													templateDir="custontemplates"	
													cssClass="item"  
													id="administar"
													name="administrar"
													href="${request.contextPath}/comprobante/factura/updateDetalleRubroCtaContableView.action?facturaDetalle.oid=${facturaDetalle.oid?c}&facturaRubroCtaContable=true&navigationId=facturaDetalle-administrar&flowControl=regis&navegacionIdBack=${navigationId}">
													<img  src="${request.contextPath}/common/images/modificar.gif" alt="Administrar" title="Administrar"  border="0">
												</@s.a>	
											</div>
								<#if puedeEditarLineas=="true">
											<div class="alineacion">
												<@s.a  
													templateDir="custontemplates"	
													cssClass="item"  
													id="eliminar"
													name="eliminar"
													href="${request.contextPath}/comprobante/factura/deleteDetalleRubroCtaContableView.action?facturaDetalle.oid=${facturaDetalle.oid?c}&facturaRubroCtaContable=true&navigationId=facturaDetalle-eliminar&flowControl=regis&navegacionIdBack=${navigationId}">
													<img  src="${request.contextPath}/common/images/eliminar.gif" alt="Eliminar" title="Eliminar"  border="0">
												</@s.a>	
											</div>
									</#if>					
								</td>	
								<td  class="estiloNumero"><#if facturaDetalle.numero?exists>${facturaDetalle.numero}<#else>&nbsp;</#if></td>
								<td  class="estiloTexto"><#if facturaDetalle.rubro.descripcion?exists>${facturaDetalle.descripcion}<#else>&nbsp;</#if></td>								
								<td  class="estiloTexto"><#if facturaDetalle.rubro.descripcion?exists>${facturaDetalle.cuentaContable.codigo?c} -${facturaDetalle.rubro.descripcion}<#else>&nbsp;</#if></td>
								<td  class="estiloTexto"><#if facturaDetalle.cuentaContable?exists>${facturaDetalle.cuentaContable.descripcion}<#else>&nbsp;</#if></td>
								<td  class="estiloNumero"><#if facturaDetalle.importeExento?exists>${facturaDetalle.importeExento}<#else>&nbsp;</#if></td>																
								<td  class="estiloNumero"><#if facturaDetalle.importeGravado?exists>${facturaDetalle.importeGravado}<#else>&nbsp;</#if></td>
								<td  class="estiloNumero"><#if facturaDetalle.importeNoGravado?exists>${facturaDetalle.importeNoGravado}<#else>&nbsp;</#if></td>													   
							</tr>
							<#if discriminaIva?exists && discriminaIva>
								<tr>
									<td colspan="3" align="center"  class="botoneraAnchoCon3" rowspan="${facturaDetalle.alicuotaIVAList.size()+1}">&nbsp;</td>						
									<th colspan="2" align="center">
										Alicuotas
										<@s.a  
											templateDir="custontemplates"	
											cssClass="item"  
											id="crear"
											name="crear"
											href="${request.contextPath}/comprobante/factura/createFacturaDetalleAlicuotaView.action?facturaDetalleAlicuotaIVA.facturaDetalle.oid=${facturaDetalle.oid?c}&navigationId=createAlicuota&flowControl=regis&navegacionIdBack=${navigationId}">
											<img  src="${request.contextPath}/common/images/agregar.gif" alt="Modificar" title="Modificar"  border="0">
											</@s.a>	
										</th>
										<th align="center">Porcentaje</th>
										<th align="center">Monto IVA</th>						
										<th align="center">Imputaci&oacute;n IVA</th>						
								</tr>
								<#assign facturaDetalleAlicuotaIVAList = facturaDetalle.alicuotaIVAList>
								<#list facturaDetalleAlicuotaIVAList as facturaDetalleAlicuotaIVA>
									<tr>
										<td colspan="2" align="center">
											<@s.a  
												templateDir="custontemplates"	
												cssClass="item"  
												id="modificar"
												name="modificar"
												href="${request.contextPath}/comprobante/factura/updateFacturaDetalleAlicuotaView.action?facturaDetalleAlicuotaIVA.oid=${facturaDetalleAlicuotaIVA.oid?c}&facturaDetalleAlicuotaIVA.facturaDetalle.oid=${facturaDetalle.oid?c}&navigationId=updateAlicuota&flowControl=regis&navegacionIdBack=${navigationId}">
												<img  src="${request.contextPath}/common/images/modificar.gif" alt="Modificar" title="Modificar"  border="0">
											</@s.a>	
												
											<@s.a  
												templateDir="custontemplates"	
												cssClass="item"  
												id="eliminar"
												name="eliminar"
												href="${request.contextPath}/comprobante/factura/deleteFacturaDetalleAlicuota.action?facturaDetalleAlicuotaIVA.oid=${facturaDetalleAlicuotaIVA.oid?c}&facturaDetalleAlicuotaIVA.facturaDetalle.oid=${facturaDetalle.oid?c}&navegacionIdBack=${navigationId}">
												<img  src="${request.contextPath}/common/images/eliminar.gif" alt="Eliminar" title="Eliminar"  border="0">
											</@s.a>	
										</td>
										<td align="center">${facturaDetalleAlicuotaIVA.alicuotaIVA.porcentaje}%</td>
										<td  align="center">${facturaDetalleAlicuotaIVA.montoIVA}</td>						
										<td align="center">${facturaDetalleAlicuotaIVA.tipoImputacionIVA}</td>			
									</tr>
								</#list>	
							</#if>
						</#list>			
				</table>
		</div>	
	<#else>
		<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
				<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
					<tr>
						<td>L&iacute;neas del Comprobante</td>
						<td>	
							<div align="right">
								<@s.a href="${request.contextPath}/comprobante/factura/preCreateFacturaDetalleRubroCtaContable.action?factura.pkFactura.nro_proveedor=${factura.pkFactura.nro_proveedor?c}&factura.pkFactura.nro_factura=${factura.pkFactura.nro_factura?c}&factura.pkFactura.sucursal=${factura.pkFactura.sucursal?c}&factura.pkFactura.tip_docum=${factura.pkFactura.tip_docum?c}&factura.proveedor.nroProveedor=${factura.proveedor.nroProveedor?c}&navigationId=facturaDetalle-create" templateDir="custontemplates" id="agregarProductoBien" name="agregarProductoBien" cssClass="ocultarIcono">
								<b>Agregar</b><img src="${request.contextPath}/common/images/agregar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
								</@s.a>		
							</div>
						</td>
					</tr>
				</table>	
				
				<table class="tablaDetalleCuerpo" cellpadding="3" >

					<tr>
						<th align="center"  class="botoneraAnchoCon3" >Acciones</th>
						<th align="center">Numero</th>
						<th align="center">Descripcion</th>							
						<th align="center">Rubro</th>
						<th align="center">Cta. Contable</th>
						<th align="center">Exento</th>	
						<th align="center">Grav.</th>		
						<th align="center">No Grav.</th>
					</tr>
				</table>
			</div>	
		</#if>		
		
		<div id="capaSeparadora" class="capaSeparadora">&nbsp;</td></tr></div>
		<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
				<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
					<tr>
						<td>Totales del Comprobante</td>
					</tr>
				</table>	


				<table class="tablaDetalleCuerpo" cellpadding="3" >					
					<tr>
						<td rowspan="2" align="center"  class="botoneraAnchoCon3" >
						
							<@s.a  
								templateDir="custontemplates"	
								cssClass="item"  
								id="modificarTotales"
								name="modificarTotales"
								href="${request.contextPath}/comprobante/factura/updateFacturaTotalesView.action?factura.pkFactura.nro_factura=${factura.pkFactura.nro_factura?c}&factura.pkFactura.nro_proveedor=${factura.pkFactura.nro_proveedor?c}&factura.pkFactura.sucursal=${factura.pkFactura.sucursal?c}&factura.pkFactura.tip_docum=${factura.pkFactura.tip_docum?c}&navigationId=factura-updateTotales&flowControl=regis&navegacionIdBack=${navigationId}">
								<img  src="${request.contextPath}/common/images/modificar.gif" alt="Modificar" title="Modificar"  border="0">
							</@s.a>	
						
						</td>
						<th align="center">Exento</th>
						<th align="center">Total Gravado</th>
						<th align="center">Total No Gravado</th>
						<th align="center">Subtotal</th>						
						<th align="center">Total IVA</th>
						<th align="center">Percepciones Nacionales</th>							
						<th align="center">Percepciones IB</th>
						<th align="center">Percepciones SUSS</th>
						<th align="center">Percepciones Municipales</th>
						<th align="center">Impuestos Internos</th>
						<th align="center">Total Factura</th>
				</tr>
				<tr>
						<td class="estiloNumero"><#if factura.exento?exists>${factura.exento}<#else>&nbsp;</#if></td>
						<td class="estiloNumero"><#if factura.netoGravado?exists>${factura.netoGravado}<#else>&nbsp;</#if></td>
						<td class="estiloNumero"><#if factura.noGravado?exists>${factura.noGravado}<#else>&nbsp;</#if></td>
						<td class="estiloNumero"><#if factura.subtotal?exists>${factura.subtotal}<#else>&nbsp;</#if></td>												
						<td class="estiloNumero"><#if factura.montoIVA?exists>${factura.montoIVA}<#else>&nbsp;</#if></td>
						<td class="estiloNumero"><#if factura.percepciones?exists>${factura.percepciones}<#else>&nbsp;</#if></td>							
						<td class="estiloNumero"><#if factura.percIb?exists>${factura.percIb}<#else>&nbsp;</#if></td>
						<td class="estiloNumero"><#if factura.percSUSS?exists>${factura.percSUSS}<#else>&nbsp;</#if></td>
						<td class="estiloNumero"><#if factura.percMunic?exists>${factura.percMunic}<#else>&nbsp;</#if></td>
						<td class="estiloNumero"><#if factura.impInternos?exists>${factura.impInternos}<#else>&nbsp;</#if></td>
						<td class="estiloNumero"><#if factura.totalFactura?exists>${factura.totalFactura}<#else>&nbsp;</#if></td>
				</tr>	
			</table>
		</div>
	</@vc.anchors>

