<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancelar" type="button" name="btnCancelar" value="Cancelar" class="boton"/>
				<input id="modificar" type="button" name="btnModificar" value="Modificar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/comprobante/factura/updateDetalleProductoBienServicio.action" 
  source="modificar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="facturaDetalle.oid={facturaDetalle.oid},facturaDetalle.cuentaContable.codigo={facturaDetalle.cuentaContable.codigo},facturaDetalle.precioUnitario={facturaDetalle.precioUnitario},facturaDetalle.tipoImputacionIVA={facturaDetalle.tipoImputacionIVA},facturaDetalle.cantidadFacturada={facturaDetalle.cantidadFacturada},navegacionIdBack=${navegacionIdBack}"/>
  
 
 <@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancelar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=${navegacionIdBack},flowControl=back"/>
  
 
 