<#setting locale="es_AR">

<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>


<@s.hidden id="navigationId" name="navigationId"/>
<@s.hidden id="factura.proveedor.nroProveedor" name="factura.proveedor.nroProveedor"/>
<@s.hidden id="factura.proveedor.detalleDePersona.razonSocial" name="factura.proveedor.detalleDePersona.razonSocial"/>
<@s.hidden id="factura.proveedor.detalleDePersona.nombre" name="factura.proveedor.detalleDePersona.nombre"/>
<@s.hidden id="totalFacturas" name="totalFacturas"/>



<#assign conOPAutorizadaEnable="false">
<#assign conOPAutorizadaClass="textarea">
<@s.if test="factura.estado.ordinal() == 1 || factura.estado.ordinal() == 2 || factura.estado.ordinal() == 4">
	<#assign conOPAutorizadaEnable="true">
	<#assign conOPAutorizadaClass="textareagris">
</@s.if>				

<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
        	<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
			<tr>
				<td><b>Datos del Comprobante: </b></td>
				<td>
					<div align="right">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0641" 
							enabled="true" 
							cssClass="item" 
							id="crear"										
							href="javascript://nop/">
							<b>Agregar</b>										
							<img src="${request.contextPath}/common/images/agregar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
						</@security.a>						
					</div>
				</td>
			</tr>
		</table>

		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
	    		<td class="textoCampo" colspan="4">&nbsp;</td>
	    	</tr>		
			<tr>
				<td class="textoCampo">N&uacute;mero:</td>
	      		<td class="textoDato">
	      			<@s.textfield 
	      					templateDir="custontemplates" 
							id="factura.numeroFactura" 
							cssClass="textarea"
							cssStyle="width:160px" 
							name="factura.numeroFactura" 
							title="N&uacute;mero Factura" />
					</td>							
	      			<td class="textoCampo">Proveedor:</td>
					<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="factura.proveedor.detalleDePersona.razonSocial" />
						<@s.property default="&nbsp;" escape=false value="factura.proveedor.detalleDePersona.nombre" />
						<@s.a templateDir="custontemplates" id="seleccionarProveedor" name="seleccionarProveedor" href="javascript://nop/" cssClass="ocultarIcono">
								<img src="${request.contextPath}/common/images/buscar.gif"  border="0" align="absmiddle" hspace="3" >
						</@s.a>	
					</td>						
			</tr>
			<tr>
				<td class="textoCampo">Origen Comprobante:</td>
	      		<td class="textoDato">
	      			<@s.select 
						templateDir="custontemplates" 
						id="factura.origenComprobante" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="factura.origenComprobante" 
						list="origenComprobanteList" 
						listKey="key" 
						listValue="description" 
						value="factura.origenComprobante.ordinal()"
						title="Origen Comprobante"
						headerKey="0"
						headerValue="Todos"							
						/>
				</td>
    
				<td class="textoCampo">Estado:</td>
      			<td class="textoDato">
      			<@s.select 
						templateDir="custontemplates" 
						id="estado" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="estado" 
						list="estadoList" 
						listKey="key" 
						listValue="description" 
						value="factura.estado.ordinal()"
						title="Estado"
						headerKey="0"
						headerValue="Todos"			
						onchange="javascript:estadoComprobanteSelect(this);"				
						/>
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Fecha Ingreso Desde:</td>
      			<td class="textoDato">
					<@vc.rowCalendar 
					templateDir="custontemplates" 
					id="fechaIngresoDesde" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="fechaIngresoDesde" 
					title="Fecha Ingreso Desde" />
				</td>
				<td class="textoCampo">Fecha Ingreso Hasta:</td>
      			<td class="textoDato">
				<@vc.rowCalendar 
					templateDir="custontemplates" 
					id="fechaIngresoHasta" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="fechaIngresoHasta" 
					title="Fecha Ingreso Hasta" />
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Fecha Emisi&oacute;n Desde:</td>
      			<td class="textoDato">
					<@vc.rowCalendar 
					templateDir="custontemplates" 
					id="fechaDesde" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="fechaDesde" 
					title="Fecha Recepcion Desde" />
				</td>
				<td class="textoCampo">Fecha Emisi&oacute;n Hasta:</td>
      			<td class="textoDato">
				<@vc.rowCalendar 
					templateDir="custontemplates" 
					id="fechaHasta" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="fechaHasta" 
					title="Fecha Recepcion Hasta" />
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Clase Comprobante:</td>
	      		<td class="textoDato">
	      			<@s.select 
						templateDir="custontemplates" 
						id="factura.claseComprobante" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="factura.claseComprobante" 
						list="claseComprobanteList" 
						listKey="key" 
						listValue="description" 
						value="factura.claseComprobante.ordinal()"
						title="Clase Comprobante"
						headerKey="0"
						headerValue="Todos"							
						/>
					</td>
					
					
				<td class="textoCampo">Con OP Emitida:</td>
	      		<td class="textoDato">
	      			<@s.select 
						templateDir="custontemplates" 
						id="factura.conOPAutorizada" 
						cssClass="${conOPAutorizadaClass}"
						disabled="${conOPAutorizadaEnable}"
						cssStyle="width:165px" 
						name="factura.conOPAutorizada" 
						list="conOPAutorizadaList" 
						listKey="key" 
						listValue="description" 
						value="factura.conOPAutorizada.ordinal()"
						title="Con OP Autorizada"
						headerKey="0"
						headerValue="Todos"							
						/>
					</td>
				</tr>
			<tr>
				<td class="textoCampo">Palabra Clave:</td>
	      		<td class="textoDato">
	      			<@s.textfield 
	      					templateDir="custontemplates" 
							id="palabraClave" 
							cssClass="textarea"
							cssStyle="width:160px" 
							name="palabraClave" 
							title="Palabra Clave" />
					</td>							
			</tr>				
			<tr>
    			<td colspan="4" class="lineaGris" align="right">
      			<input id="limpiar" type="button" name="btnVolver" value="Limpiar" class="boton"/>		
      			<input id="buscar" type="button" name="btnVolver" value="Buscar" class="boton"/>
    			</td>
			</tr>	
		</table>
	</div>	
	<!-- Resultado Filtro -->
	<@s.if test="facturaList!=null">
		<#assign facturaNumero =0>
			<#if factura.numeroFactura?exists>
				<#assign facturaNumero =factura.numeroFactura>
			</#if>
			
			<#assign facturaProveedor =0>
			<#if factura.proveedor?exists && factura.proveedor.nroProveedor?exists>
				<#assign facturaProveedor =factura.proveedor.nroProveedor>
			</#if>
			
			<#assign facturaOrigen =0>
			<#if factura.origenComprobante?exists && factura.origenComprobante.ordinal()?exists>
				<#assign facturaOrigen =factura.origenComprobante.ordinal()>
			</#if>
			
			
			<#assign facturaEstado =0>
			<#if factura.estado?exists && factura.estado.ordinal()?exists>
				<#assign facturaEstado =factura.estado.ordinal()>
			</#if>
			
			<#assign facturaClase =0>
			<#if factura.claseComprobante?exists && factura.claseComprobante.ordinal()?exists>
				<#assign facturaClase =factura.claseComprobante.ordinal()>
			</#if>
			

			<#assign fechaD = "" />
			<@s.if test="fechaDesde != null">
				<#assign fechaD = "${fechaDesde?string('dd/MM/yyyy')}" />
			</@s.if>

			<#assign fechaH = "" />
			<@s.if test="fechaHasta != null">
				<#assign fechaH = "${fechaHasta?string('dd/MM/yyyy')}" />
			</@s.if>
			
			<#assign fechaDI = "" />
			<@s.if test="fechaIngresoDesde != null">
				<#assign fechaDI = "${fechaIngresoDesde?string('dd/MM/yyyy')}" />
			</@s.if>

			<#assign fechaHI = "" />
			<@s.if test="fechaIngresoHasta != null">
				<#assign fechaHI = "${fechaIngresoHasta?string('dd/MM/yyyy')}" />
			</@s.if>
		<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
				<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>Comprobantes encontrados</td>
					<td>
						<div class="alineacionDerechaBig">
							<@security.a 
								templateDir="custontemplates" 
								securityCode="CUF0782" 
								enabled="true" 
								cssClass="item" 
								href="${request.contextPath}/comprobante/factura/printIVACreditoFiscalXLS.action?factura.numeroFactura=${facturaNumero?c}&factura.proveedor.nroProveedor=${facturaProveedor?c}&factura.origenComprobante=${facturaOrigen}&factura.claseComprobante=${facturaClase}&fechaDesde=${fechaD}&fechaHasta=${fechaH}&fechaIngresoDesde=${fechaDI}&fechaIngresoHasta=${fechaHI}&palabraClave=${palabraClave}">
								<img src="${request.contextPath}/common/images/imprimirExcel.gif" title="Exportar el listado de Comprobantes en formato XLS" align="absmiddle" border="0" hspace="3">
							</@security.a>
						</div>
						<div class="alineacionDerechaBig">
						<b>IVA Cr&eacute;dito Fiscal</b>
								<@security.a 
									templateDir="custontemplates" 
									securityCode="CUF0782" 
									enabled="true" 
									cssClass="item" 
									href="${request.contextPath}/comprobante/factura/printIVACreditoFiscalPDF.action?factura.numeroFactura=${facturaNumero?c}&factura.proveedor.nroProveedor=${facturaProveedor?c}&factura.origenComprobante=${facturaOrigen}&factura.claseComprobante=${facturaClase}&fechaDesde=${fechaD}&fechaHasta=${fechaH}&fechaIngresoDesde=${fechaDI}&fechaIngresoHasta=${fechaHI}&palabraClave=${palabraClave}">
									<img src="${request.contextPath}/common/images/imprimirPdf.gif" title="Exportar el listado de Comprobantes en formato PDF" align="absmiddle" border="0" hspace="3">
								</@security.a>
						</div>
						<div class="alineacionDerechaBig">
							<@security.a 
									templateDir="custontemplates" 
									securityCode="CUF0781" 
									enabled="true" 
									cssClass="item" 
									href="${request.contextPath}/comprobante/factura/printPorProveedorXLS.action?factura.numeroFactura=${facturaNumero?c}&factura.proveedor.nroProveedor=${facturaProveedor?c}&factura.origenComprobante=${facturaOrigen}&factura.claseComprobante=${facturaClase}&fechaDesde=${fechaD}&fechaHasta=${fechaH}&fechaIngresoDesde=${fechaDI}&fechaIngresoHasta=${fechaHI}&factura.estado=${facturaEstado}&palabraClave=${palabraClave}">
									<img src="${request.contextPath}/common/images/imprimirExcel.gif" title="Exportar listado de Comprobantes por Proveedor Excel" align="absmiddle" border="0"  hspace="3" >
								</@security.a>
								<b> - </b>&nbsp;
						</div>
						<div class="alineacionDerechaBig">
							&nbsp;<b>-</b>&nbsp;
							<b>Por Proveedor</b>
								<@security.a 
									templateDir="custontemplates" 
									securityCode="CUF0781" 
									enabled="true" 
									cssClass="item" 
									href="${request.contextPath}/comprobante/factura/printPorProveedorPDF.action?factura.numeroFactura=${facturaNumero?c}&factura.proveedor.nroProveedor=${facturaProveedor?c}&factura.origenComprobante=${facturaOrigen}&factura.claseComprobante=${facturaClase}&fechaDesde=${fechaD}&fechaHasta=${fechaH}&fechaIngresoDesde=${fechaDI}&fechaIngresoHasta=${fechaHI}&factura.estado=${facturaEstado}&palabraClave=${palabraClave}">
									<img src="${request.contextPath}/common/images/imprimirPdf.gif" title="Exportar listado de Comprobantes por Proveedor PDF" align="absmiddle" border="0" hspace="3" >
								</@security.a>
						</div>
						
						<div class="alineacionDerechaBig">
							<b>Autorizar Comprobantes
								<@security.a 
									templateDir="custontemplates" 
									securityCode="CUF0650" 
									enabled="true" 
									cssClass="item" 
									id="autorizar"
									name="autorizar"												
									href="javascript://nop/">																				
									<img src="${request.contextPath}/common/images/autorizar.png" title="Autorizar Comprobantes" align="absmiddle" border="0" hspace="3" />
								</@security.a>		
							</b>
						</div>
							
					</td>
				</tr>
			</table>	
			
			<@vc.anchors target="contentTrx">			
          		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="facturaList" id="factura" pagesize=15 defaultsort=2 partialList=true size="recordSize" keepStatus=true excludedParams="resetFlag">

					<@display.column headerClass="tbl-contract-service-select" class="botoneraAnchoCon6" title="Acciones">
						
						<div class="alineacionMenor">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0643" 
							enabled="factura.readable" 
							cssClass="item" 
							href="${request.contextPath}/comprobante/factura/readView.action?factura.pkFactura.nro_factura=${factura.pkFactura.nro_factura?c}&factura.pkFactura.nro_proveedor=${factura.pkFactura.nro_proveedor?c}&factura.pkFactura.sucursal=${factura.pkFactura.sucursal?c}&factura.pkFactura.tip_docum=${factura.pkFactura.tip_docum?c}&navigationId=factura-visualizar&flowControl=regis&navegacionIdBack=${navigationId}">
							<img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver"  border="0">
						</@security.a>
						</div>

					<!-- Modificar: si el estado es Ingresado 3 o Con Oblicacion de pago generada 5: permite modificar (cancelar la autorizacion o la obligacion de pago) -->
				<#if (factura.puedeOperar) >
					<#if ((factura.estado.ordinal() == 3 || factura.estado.ordinal() == 5) && (factura.origenComprobante.ordinal() != 3 && factura.origenComprobante.ordinal() != 4)) >
						<div class="alineacionMenor">		
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0644" 
							enabled="factura.readable"
							cssClass="item"  
							href="${request.contextPath}/comprobante/factura/administrarModificarView.action?factura.pkFactura.nro_factura=${factura.pkFactura.nro_factura?c}&factura.pkFactura.nro_proveedor=${factura.pkFactura.nro_proveedor?c}&factura.pkFactura.sucursal=${factura.pkFactura.sucursal?c}&factura.pkFactura.tip_docum=${factura.pkFactura.tip_docum?c}&navigationId=factura-administrar&flowControl=regis&navegacionIdBack=${navigationId}">
							<img  src="${request.contextPath}/common/images/administrar.gif" alt="Administrar" title="Administrar"  border="0">
						</@security.a>						
						</div>
					<#else>
					  <!-- Administrar -->
					  	<div class="alineacionMenor">		
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0644" 
							enabled="factura.updatable"
							cssClass="item"  
							href="${request.contextPath}/comprobante/factura/administrarView.action?factura.pkFactura.nro_factura=${factura.pkFactura.nro_factura?c}&factura.pkFactura.nro_proveedor=${factura.pkFactura.nro_proveedor?c}&factura.pkFactura.sucursal=${factura.pkFactura.sucursal?c}&factura.pkFactura.tip_docum=${factura.pkFactura.tip_docum?c}&navigationId=factura-administrar&flowControl=regis&navegacionIdBack=${navigationId}">
							<img  src="${request.contextPath}/common/images/administrar.gif" alt="Administrar" title="Administrar"  border="0">
						</@security.a>						
						</div>
					</#if>
				</#if>

						<div class="alineacionMenor">
						<#if (factura.puedeOperar) >
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0645" 
							enabled="factura.eraseable"
							cssClass="item"  
							href="${request.contextPath}/comprobante/factura/deleteView.action?factura.pkFactura.nro_factura=${factura.pkFactura.nro_factura?c}&factura.pkFactura.nro_proveedor=${factura.pkFactura.nro_proveedor?c}&factura.pkFactura.sucursal=${factura.pkFactura.sucursal?c}&factura.pkFactura.tip_docum=${factura.pkFactura.tip_docum?c}&navigationId=factura-eliminar&flowControl=regis">
							<img  src="${request.contextPath}/common/images/eliminar.gif" alt="Eliminar" title="Eliminar"  border="0">
						</@security.a>
						</#if>				
						</div>
						<div class="alineacionMenor">
						<#if (factura.puedeOperar) >
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0646" 
							enabled="factura.enable"
							cssClass="item"  
							href="${request.contextPath}/comprobante/factura/createNotaView.action?factura.pkFactura.nro_factura=${factura.pkFactura.nro_factura?c}&factura.pkFactura.nro_proveedor=${factura.pkFactura.nro_proveedor?c}&factura.pkFactura.sucursal=${factura.pkFactura.sucursal?c}&factura.pkFactura.tip_docum=${factura.pkFactura.tip_docum?c}&navigationId=factura-agregarNota&flowControl=regis&navegacionIdBack=${navigationId}">
							<img  src="${request.contextPath}/common/images/agregarNota.gif" alt="Agregar Nota" title="Agregar Nota"  border="0">
						</@security.a>
						</#if>						
						</div>
						
						<div class="alineacionMenor">
						<#assign tieneDiferencia = 'false'>
						<#if factura.estado?exists && factura.estado.ordinal() == 2 && factura.puedeOperar>
							<#assign tieneDiferencia = 'true'>
						</#if>
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0647" 
							enabled="${tieneDiferencia}" 
							cssClass="item" 
							href="${request.contextPath}/comprobante/factura/resolverDiferenciaView.action?factura.pkFactura.nro_factura=${factura.pkFactura.nro_factura?c}&factura.pkFactura.nro_proveedor=${factura.pkFactura.nro_proveedor?c}&factura.pkFactura.sucursal=${factura.pkFactura.sucursal?c}&factura.pkFactura.tip_docum=${factura.pkFactura.tip_docum?c}&navigationId=factura-resolverDiferencia&flowControl=regis&navegacionIdBack=${navigationId}">
							<img  src="${request.contextPath}/common/images/advertencia.gif" alt="Resolver Diferencia" title="Resolver Diferencia"  border="0">
						</@security.a>
						</div>

						<div class="alineacion">
						<#assign autorizable = 'false'>
						<#if factura.estado?exists && factura.estado.ordinal() == 3 && factura.origenComprobante.ordinal() != 4 && factura.puedeOperar>
							<#assign autorizable = 'true'>
						</#if>
						
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0650" 
							enabled="${autorizable}"
							cssClass="item"  
							href="${request.contextPath}/comprobante/factura/authorizationView.action?factura.pkFactura.nro_factura=${factura.pkFactura.nro_factura?c}&factura.pkFactura.nro_proveedor=${factura.pkFactura.nro_proveedor?c}&factura.pkFactura.sucursal=${factura.pkFactura.sucursal?c}&factura.pkFactura.tip_docum=${factura.pkFactura.tip_docum?c}&navigationId=factura-autorizar&flowControl=regis&navegacionIdBack=${navigationId}">
							<img  src="${request.contextPath}/common/images/usuarioAutoriza.png" alt="Autorizar" title="Autorizar"  border="0">
						</@security.a>
						</div>
						<!--ordenPagoQuitado 
						<div class="alineacionMenor">
						<#assign generaOP = 'false'>
						<#if factura.estado?exists && factura.estado.ordinal() == 3 && factura.origenComprobante?exists && factura.origenComprobante.ordinal() != 3 && factura.origenComprobante?exists && factura.origenComprobante.ordinal() != 4 && factura.puedeOperar>
							<#assign generaOP = 'true'>
						</#if>
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0648" 
							enabled="${generaOP}" 
							cssClass="item"  
							href="${request.contextPath}/pago/createDesdeFacturaView.action?factura.pkFactura.nro_factura=${factura.pkFactura.nro_factura?c}&factura.pkFactura.nro_proveedor=${factura.pkFactura.nro_proveedor?c}&factura.pkFactura.sucursal=${factura.pkFactura.sucursal?c}&factura.pkFactura.tip_docum=${factura.pkFactura.tip_docum?c}&navigationId=pago-crear&flowControl=regis&navegacionIdBack=${navigationId}">
							<img  src="${request.contextPath}/common/images/ordenPago.gif" alt="Generar Orden de Pago" title="Generar Orden de Pago"  border="0">
						</@security.a>				
						</div>
						-->
					</@display.column>

					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="fechaIngreso" format="{0,date,dd/MM/yy}"  title="F. Ingreso" />

					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero"  property="numeroString" title="Suc - Nro."/>

					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="fechaEmision" format="{0,date,dd/MM/yy}"  title="F. Emisi&oacute;n" />
									
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTextoAnchoFijo" title="Proveedor">  
						${factura.proveedor.detalleDePersona.razonSocial}
						<#if factura.proveedor.detalleDePersona.nombre?exists>
						 ${factura.proveedor.detalleDePersona.nombre}
						 </#if>
					</@display.column>
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" title="Total">
						<#if factura.totalFactura?exists>
						<#assign totalFactura = factura.totalFactura> 
						${totalFactura?string(",##0.00")}
						<#else>	
						&nbsp;
						</#if>	
						
					</@display.column>
					<#--
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto"  title="Orden de Pago" />
					-->
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" style="width:80px;"  property="origenComprobante" title="Origen" />
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="claseComprobante" title="Clase" />
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="estado" title="Estado" />
					
					<@display.footer>
						<tr>
						    <td colspan=6 aling="right">
						       <b>Total Facturas:</b>
						    </td>
						    <td style="text-align: right; font:bold 13px Verdana,Tahoma,Arial,sans-serif; "  id="tdTotalFacturasEncontradas">
							   <b>
								${totalFacturasEncontradas?string(",##0.00")}
								</b>
							</td>
						<tr>
					</@display.footer>
					
					
				</@display.table>
			</@vc.anchors>
		</div>	
	</@s.if>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/comprobante/factura/search.action"
  source="buscar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="resetFlag=true,navigationId={navigationId},flowControl=regis,factura.estado={estado},factura.proveedor.nroProveedor={factura.proveedor.nroProveedor},factura.proveedor.detalleDePersona.razonSocial={factura.proveedor.detalleDePersona.razonSocial},factura.proveedor.detalleDePersona.nombre={factura.proveedor.detalleDePersona.nombre},factura.numeroFactura={factura.numeroFactura},fechaDesde={fechaDesde},fechaHasta={fechaHasta},factura.origenComprobante={factura.origenComprobante},factura.claseComprobante={factura.claseComprobante},fechaIngresoDesde={fechaIngresoDesde},fechaIngresoHasta={fechaIngresoHasta},factura.conOPAutorizada={factura.conOPAutorizada},palabraClave={palabraClave}"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/comprobante/factura/view.action"
  source="limpiar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=regis"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/comprobante/factura/createView.action" 
  source="crear" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=factura-crear,flowControl=regis,navegacionIdBack=${navigationId}"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/comprobante/factura/selectProveedorSearch.action" 
  source="seleccionarProveedor" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=change,factura.estado={estado},factura.proveedor.nroProveedor={factura.proveedor.nroProveedor},factura.proveedor.detalleDePersona.razonSocial={factura.proveedor.detalleDePersona.razonSocial},factura.proveedor.detalleDePersona.nombre={factura.proveedor.detalleDePersona.nombre},factura.numeroFactura={factura.numeroFactura},fechaDesde={fechaDesde},fechaHasta={fechaHasta},factura.origenComprobante={factura.origenComprobante},fechaIngresoDesde={fechaIngresoDesde},fechaIngresoHasta={fechaIngresoHasta},factura.conOPAutorizada={factura.conOPAutorizada},palabraClave={palabraClave}"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/comprobante/factura/selectComprobantesAutorizar.action" 
  source="autorizar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=comprobante-autorizar,flowControl=regis,navegacionIdBack=buscar-factura,factura.estado={estado},factura.proveedor.nroProveedor={factura.proveedor.nroProveedor},factura.proveedor.detalleDePersona.razonSocial={factura.proveedor.detalleDePersona.razonSocial},factura.proveedor.detalleDePersona.nombre={factura.proveedor.detalleDePersona.nombre},factura.numeroFactura={factura.numeroFactura},fechaDesde={fechaDesde},fechaHasta={fechaHasta},factura.origenComprobante={factura.origenComprobante},factura.claseComprobante={factura.claseComprobante},fechaIngresoDesde={fechaIngresoDesde},fechaIngresoHasta={fechaIngresoHasta},factura.conOPAutorizada={factura.conOPAutorizada},palabraClave={palabraClave}"/>
    