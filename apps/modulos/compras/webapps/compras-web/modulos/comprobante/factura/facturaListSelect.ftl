<#setting locale="es_AR">

<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>

<@s.hidden id="navigationId" name="navigationId"/>
<@s.hidden id="factura.proveedor.nroProveedor" name="factura.proveedor.nroProveedor"/>
<@s.hidden id="factura.proveedor.detalleDePersona.razonSocial" name="factura.proveedor.detalleDePersona.razonSocial"/>
<@s.hidden id="factura.proveedor.detalleDePersona.nombre" name="factura.proveedor.detalleDePersona.nombre"/>
<@s.hidden id="modoBusqueda" name="modoBusqueda.ordinal()"/>
<@s.hidden id="seleccionMultiple" name="seleccionMultiple"/>
<@s.hidden id="estadoDuro" name="estadoDuro"/>
<@s.hidden id="claseDura" name="claseDura"/>
<@s.hidden id="origenDuro" name="origenDuro"/>
<@s.hidden id="proveedorDuro" name="proveedorDuro"/>
<@s.hidden id="bien.oid" name="bien.oid"/>

<#if estadoDuro?exists && estadoDuro>
	<@s.hidden id="factura.estado" name="factura.estado.ordinal()"/>
</#if>

<#if claseDura?exists && claseDura>
	<@s.hidden id="factura.claseComprobante" name="factura.claseComprobante.ordinal()"/>
</#if>


<#if origenDuro?exists && origenDuro>
	<@s.hidden id="factura.origenComprobante" name="factura.origenComprobante.ordinal()"/>
</#if>

<div id="capaTituloCuerpo" class="capaTituloCuerpo">
	<table id="tablaTituloMenu" class="tablaTituloMenu">
		<tr>
			<td><@s.text name="${navegacion}"/></td>
		</tr>
	</table>
</div>
<div id="capaTituloAccion" class="capaTituloAccion">
	<table id="tablaTituloAccion" class="tablaTituloAccion">
    	<tr>
        	<td><div id="errorTrx" align="left"></div></td>
       	</tr>
       	<tr>
			<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" /><@s.text name="${titulo}" /></td>
		</tr>
	</table>
</div>	
	
<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
	<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
		<tr>
			<td><b>Datos del Comprobante</b></td>
		</tr>
	</table>
	<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
		<tr>
    		<td class="textoCampo" colspan="4">&nbsp;</td>
    	</tr>
		<tr>
			<td class="textoCampo">N&uacute;mero:</td>
      		<td class="textoDato">
      			<@s.textfield 
      					templateDir="custontemplates" 
						id="factura.numeroFactura" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="factura.numeroFactura" 
						title="N&uacute;mero Factura" />
			</td>							
      		<td class="textoCampo">Proveedor:</td>
			<td class="textoDato">
				<@s.property default="&nbsp;" escape=false value="factura.proveedor.detalleDePersona.razonSocial" />
				<@s.property default="&nbsp;" escape=false value="factura.proveedor.detalleDePersona.nombre" />
			</td>						
		</tr>
		<tr>
			<td class="textoCampo">Origen Comprobante:</td>
   			<td class="textoDato">
     			<@s.select 
						templateDir="custontemplates" 
						id="factura.origenComprobante" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="factura.origenComprobante" 
						list="origenComprobanteList" 
						listKey="key" 
						listValue="description" 
						value="factura.origenComprobante.ordinal()"
						title="Origen Comprobante"
						headerKey="0"
						headerValue="Todos"							
						/>
			</td>
			<td class="textoCampo">Estado:</td>
   			<td class="textoDato">
	     		<#if estadoDuro?exists && estadoDuro>
					<@s.property default="&nbsp;" escape=false value="estadosDuros" />
	      		<#else>					      					      				      	
		      		<@s.select 
						templateDir="custontemplates" 
						id="factura.estado"
						name="factura.estado"  
						cssClass="textarea"
						cssStyle="width:165px" 
						list="estadoList" 
						listKey="key" 
						listValue="description" 
						value="factura.estado.ordinal()"
						title="Estado"
						headerKey="0"
						headerValue="Todos"							
						/>		      				      	
				</#if>					
			</td>
		</tr>
		<tr>
			<td class="textoCampo">Fecha Emisi&oacute;n Desde:</td>
     			<td class="textoDato">
				<@vc.rowCalendar 
				templateDir="custontemplates" 
				id="fechaDesde" 
				cssClass="textarea"
				cssStyle="width:160px" 
				name="fechaDesde" 
				title="Fecha Recepcion Desde" />
			</td>
			<td class="textoCampo">Fecha Emisi&oacute;n Hasta:</td>
     			<td class="textoDato">
			<@vc.rowCalendar 
				templateDir="custontemplates" 
				id="fechaHasta" 
				cssClass="textarea"
				cssStyle="width:160px" 
				name="fechaHasta" 
				title="Fecha Recepcion Hasta" />
			</td>
		</tr>
		<tr>
			<td class="textoCampo">Clase Comprobante:</td>
   			<td class="textoDato" colspan="3">
     			<@s.select 
						templateDir="custontemplates" 
						id="factura.claseComprobante" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="factura.claseComprobante" 
						list="claseComprobanteList" 
						listKey="key" 
						listValue="description" 
						value="factura.claseComprobante.ordinal()"
						title="Clase Comprobante"
						headerKey="0"
						headerValue="Todos"							
				/>
			</td>
		</tr>
		<tr>
  			<td colspan="4" class="lineaGris" align="right">	
    			<input id="buscar" type="button" name="btnVolver" value="Buscar" class="boton"/>
  			</td>
		</tr>	
	</table>
</div>	
		
<!-- Resultado Filtro -->
<@s.if test="facturaList!=null">
	<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
		<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
			<tr>
				<td>Comprobantes encontrados</td>
			</tr>
		</table>		
		
		<@vc.anchors target="contentTrx">			
         	<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="facturaList" id="factura" defaultsort=2>
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select" title="Acciones">  				
 					<div class="alineacion">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0643" 
							enabled="factura.readable" 
							cssClass="item" 
							id="select"
							name="select"
							href="${request.contextPath}/comprobante/factura/readView.action?factura.pkFactura.nro_factura=${factura.pkFactura.nro_factura?c}&factura.pkFactura.nro_proveedor=${factura.pkFactura.nro_proveedor?c}&factura.pkFactura.sucursal=${factura.pkFactura.sucursal?c}&factura.pkFactura.tip_docum=${factura.pkFactura.tip_docum?c}&navigationId=factura-visualizar&flowControl=regis&navegacionIdBack=${navigationId}">
							<img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver"  border="0">
						</@security.a>
					
		         		<#if seleccionMultiple?exists && seleccionMultiple==true>
							<@s.checkbox												
							label="${factura.pkFactura.nro_factura?c}_${factura.pkFactura.nro_proveedor?c}_${factura.pkFactura.sucursal?c}_${factura.pkFactura.tip_docum?c}" 
							fieldValue="${factura.pkFactura.nro_factura?c}_${factura.pkFactura.nro_proveedor?c}_${factura.pkFactura.sucursal?c}_${factura.pkFactura.tip_docum?c}" 
							name="facturaListString" />
						<#else>
							<#assign ref="${request.contextPath}/comprobante/factura/seleccionBack.action?">		
							<@s.a href="${ref}factura.pkFactura.nro_factura=${factura.pkFactura.nro_factura?c}&factura.pkFactura.nro_proveedor=${factura.pkFactura.nro_proveedor?c}&factura.pkFactura.sucursal=${factura.pkFactura.sucursal?c}&factura.pkFactura.tip_docum=${factura.pkFactura.tip_docum?c}&navegacionIdBack=${navegacionIdBack}&navigationId=${navigationId}"><img src="${request.contextPath}/common/images/seleccionar.gif" alt="Seleccionar" title="Seleccionar" width="16" height="16" border="0"></@s.a>
							&nbsp;
						</#if>			
					</div>	
				</@display.column>
			
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="fechaIngreso" format="{0,date,dd/MM/yy}"  title="F. Ingreso" />
									
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero"  property="numeroString" class="botoneraAnchoCon4" title="Suc - Nro."/>
					
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="fechaEmision" format="{0,date,dd/MM/yyyy}"  title="Fecha Emisi&oacute;n" />
								
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" title="Proveedor">  
						${factura.proveedor.detalleDePersona.razonSocial}
						<#if factura.proveedor.detalleDePersona.nombre?exists>
						 ${factura.proveedor.detalleDePersona.nombre}
						 </#if>
				</@display.column>					
					 
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" title="Total">
						<#if factura.totalFactura?exists>
						<#assign totalFactura = factura.totalFactura> 
						${totalFactura?string(",##0.00")}
						<#else>	
						&nbsp;
						</#if>	
						
				</@display.column>
				
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="origenComprobante" title="Origen" />
				
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="claseComprobante" title="Clase Comprob." />
					
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="rubro.descripcion" title="Rubro Padre" />
						
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="estado" title="Estado" />
				
			</@display.table>
		</@vc.anchors>
	</div>
</@s.if> 
<!-- Fin Resultado Filtro -->
	
<div id="capaBotonera" class="capaBotonera">
	<#if seleccionMultiple?exists && seleccionMultiple==true>
		<table id="tablaBotonera" class="tablaBotonera">
				<tr> 
					<td align="left">
						<input id="cancel" type="button" name="btnVolver" value="Volver" class="boton"/>
						<input id="seleccionar" type="button" name="btnSeleccionar" value="Seleccionar Comprobante" class="boton"/>
					</td>
				</tr>	
		</table>
				
		<@vc.htmlContent 
 				baseUrl="${request.contextPath}/comprobante/factura/selecccionMultipleFactura.action" 
 				source="seleccionar" 
 				success="contentTrx" 
 				failure="errorTrx" 
 				parameters="facturaListString={facturaListString},navegacionIdBack=${navegacionIdBack},oidParameter=${oidParameter},modoBusqueda={modoBusqueda},seleccionMultiple={seleccionMultiple},claseDura={claseDura},origenDuro={origenDuro}"/>
	<#else>
		<table id="tablaBotonera" class="tablaBotonera">
			<tr>
				<td align="left">
						<input id="cancel" type="button" name="btnVolver" value="Volver" class="boton"/>
				</td>												
			</tr>	
		</table>
	</#if>
</div>
			
<@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId=${navegacionIdBack},flowControl=back"/>
 
 <@vc.htmlContent 
   baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancelar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=${navegacionIdBack},flowControl=back"/>
 