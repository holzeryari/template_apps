<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancel" type="button" name="btnCancelar" value="Cancelar" class="boton"/>
				<input id="modificar" type="button" name="btnModificar" value="Modificar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=${navegacionIdBack},flowControl=back"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/comprobante/factura/updateFormaMedioPago.action"  
  source="modificar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="facturaMedioPago.oid={facturaMedioPago.oid},facturaMedioPago.factura.pkFactura.nro_proveedor={facturaMedioPago.factura.pkFactura.nro_proveedor}, facturaMedioPago.factura.pkFactura.nro_factura={facturaMedioPago.factura.pkFactura.nro_factura}, facturaMedioPago.factura.pkFactura.sucursal={facturaMedioPago.factura.pkFactura.sucursal}, facturaMedioPago.factura.pkFactura.tip_docum={facturaMedioPago.factura.pkFactura.tip_docum}, facturaMedioPago.medioPago={medioPago},facturaMedioPago.porcentaje={facturaMedioPago.porcentaje},facturaMedioPago.montoAsignado={facturaMedioPago.montoAsignado}, navegacionIdBack=${navegacionIdBack},facturaMedioPago.formaPago={formaPago}, facturaMedioPago.observaciones={facturaMedioPago.observaciones}, facturaMedioPago.fechaAsignada={fechaAsignada}, facturaMedioPago.cantidadDias={cantidadDias}, facturaMedioPago.formaPago.conFechaAsignada={facturaMedioPago.formaPago.conFechaAsignada}"/>

<@ajax.updateField
  baseUrl="${request.contextPath}/comprobante/factura/refreshFacturaFormaPago.action" 
  source="formaPago" 
  target="facturaMedioPago.formaPago.conFechaAsignada"
  action="formaPago"
  parameters="facturaMedioPago.formaPago={formaPago}"
  eventType="change"
  postFunction="refreshFacturaFormaPago"
  parser="new ResponseXmlParser()"/>

  <@ajax.updateField
  baseUrl="${request.contextPath}/comprobante/factura/refreshFechaAsignada.action" 
  source="fechaAsignada" 
  target="cantidadDias"
  action="fechaAsignada"
  parameters="facturaMedioPago.fechaAsignada={fechaAsignada}"
  eventType="change"
  parser="new ResponseXmlParser()"/>
  
  <@ajax.updateField
  baseUrl="${request.contextPath}/comprobante/factura/refreshFechaAsignada.action" 
  source="fechaAsignada" 
  target="cantidadDias"
  action="fechaAsignada"
  parameters="facturaMedioPago.fechaAsignada={fechaAsignada}"
  eventType="blur"
  parser="new ResponseXmlParser()"/>
  
  
  <@ajax.updateField
  baseUrl="${request.contextPath}/comprobante/factura/refreshCantidadDias.action" 
  source="cantidadDias" 
  target="fechaAsignada"
  action="cantidadDias"
  parameters="facturaMedioPago.cantidadDias={cantidadDias}"
  eventType="blur"
  parser="new ResponseXmlParser()"/>
  