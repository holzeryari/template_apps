<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign tiles=JspTaglibs["/WEB-INF/tlds/tiles-jsp.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<@s.hidden id="navigationId" name="navigationId"/>

<@s.hidden id="factura.pkFactura.nro_factura" name="factura.pkFactura.nro_factura"/>
<@s.hidden id="factura.pkFactura.nro_proveedor" name="factura.pkFactura.nro_proveedor"/>
<@s.hidden id="factura.pkFactura.sucursal" name="factura.pkFactura.sucursal"/>
<@s.hidden id="factura.pkFactura.tip_docum" name="factura.pkFactura.tip_docum"/>


<div id="capaSeparadora" class="capaSeparadora">&nbsp;</div>

<!-- @vc.anchors target="contentTrx" -->	

	<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
		<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>Medios de Pago</td>
				</tr>
		</table>
		
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
	    		<td class="textoCampo" colspan="4">&nbsp;</td>
	    	</tr>			
			<tr>
				<td class="textoCampo">Fecha Sugerida de Pago:</td>
  				<td class="textoDato">
					<@vc.rowCalendar 
					templateDir="custontemplates" 
					id="factura.fechaVencimiento" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="factura.fechaVencimiento" 
					title="Fecha Sugerida de Pago" />
				</td>
				<td class="textoCampo">&nbsp;</td>
	  			<td class="textoDato">&nbsp;</td>
			</tr>
			<tr>
	    		<td class="textoCampo" colspan="4">&nbsp;</td>
	    	</tr>			
		</table>
	</div>
		
<!-- /@vc.anchors -->


