<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancel" type="button" name="btnCancelar" value="Cancelar" class="boton"/>
				<input id="store" type="button" name="btnAgregar" value="Agregar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=${navegacionIdBack},flowControl=back"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/comprobante/factura/createFacturaRubroCtaContable.action" 
  source="store" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="factura.proveedor.nroProveedor={factura.proveedor.nroProveedor},factura.numeroFactura={factura.numeroFactura},factura.cai={factura.cai},factura.fechaCai={factura.fechaCai},factura.sucursal={factura.sucursal},factura.fechaEmision={factura.fechaEmision},factura.fechaVencimiento={factura.fechaVencimiento},factura.tipoAdquisicion={factura.tipoAdquisicion},factura.esOriginal={factura.esOriginal},factura.formaPago.pk.secuencia={factura.formaPago.pk.secuencia},factura.tipoComprobante.codigo={factura.tipoComprobante.codigo},factura.origenComprobante={factura.origenComprobante},navegacionIdBack=${navegacionIdBack},oidParameter=${oidParameter?c},facturaRubroCtaContable={facturaRubroCtaContable},facturaDetalle.cuentaContable.codigo={facturaDetalle.cuentaContable.codigo},facturaDetalle.rubro.oid={facturaDetalle.rubro.oid},facturaDetalle.importeNoGravado={facturaDetalle.importeNoGravado},facturaDetalle.importeGravado={facturaDetalle.importeGravado},facturaDetalle.descripcion={facturaDetalle.descripcion},factura.cliente.oid={factura.cliente.oid},factura.cliente.descripcion={factura.cliente.descripcion},factura.proveedor.detalleDePersona.pk.secuencia={factura.proveedor.detalleDePersona.pk.secuencia},factura.proveedor.detalleDePersona.pk.identificador={factura.proveedor.detalleDePersona.pk.identificador},factura.proveedor.detalleDePersona.persona.oid={factura.proveedor.detalleDePersona.persona.oid},factura.proveedor.nombreFantasia={factura.proveedor.nombreFantasia},factura.proveedor.detalleDePersona.razonSocial={factura.proveedor.detalleDePersona.razonSocial},factura.proveedor.detalleDePersona.persona.docFiscal={factura.proveedor.detalleDePersona.persona.docFiscal},  factura.proveedor.detalleDePersona.persona.situacionFiscal.codigo={factura.proveedor.detalleDePersona.persona.situacionFiscal.codigo},facturaDetalle.importeExento={facturaDetalle.importeExento}"/>
  
  
  <@vc.htmlContent 
  baseUrl="${request.contextPath}/comprobante/factura/selectProveedorABM.action" 
  source="seleccionarProveedor" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId=${navigationId},flowControl=change,factura.proveedor.nroProveedor={factura.proveedor.nroProveedor},factura.proveedor.razonSocial={factura.proveedor.razonSocial},factura.proveedor.nombre={factura.proveedor.nombre},factura.numeroFactura={factura.numeroFactura},factura.cai={factura.cai},factura.fechaCai={factura.fechaCai},factura.sucursal={factura.sucursal},factura.fechaEmision={factura.fechaEmision},factura.fechaVencimiento={factura.fechaVencimiento},factura.tipoAdquisicion={factura.tipoAdquisicion},factura.esOriginal={factura.esOriginal},factura.formaPago.pk.secuencia={factura.formaPago.pk.secuencia},factura.tipoComprobante.codigo={factura.tipoComprobante.codigo},factura.origenComprobante={factura.origenComprobante},navegacionIdBack=${navegacionIdBack},oidParameter=${oidParameter?c},facturaRubroCtaContable={facturaRubroCtaContable},facturaDetalle.cuentaContable.codigo={facturaDetalle.cuentaContable.codigo},facturaDetalle.rubro.oid={facturaDetalle.rubro.oid},facturaDetalle.importeNoGravado={facturaDetalle.importeNoGravado},facturaDetalle.importeGravado={facturaDetalle.importeGravado},facturaDetalle.descripcion={facturaDetalle.descripcion},factura.cliente.oid={factura.cliente.oid},factura.cliente.descripcion={factura.cliente.descripcion},facturaDetalle.importeExento={facturaDetalle.importeExento}"/>
  
  
  <@vc.htmlContent 
  baseUrl="${request.contextPath}/comprobante/factura/selectRubro.action" 
  source="seleccionarRubro" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId=${navigationId},flowControl=change,factura.proveedor.nroProveedor={factura.proveedor.nroProveedor},factura.proveedor.razonSocial={factura.proveedor.razonSocial},factura.proveedor.nombre={factura.proveedor.nombre},factura.numeroFactura={factura.numeroFactura},factura.cai={factura.cai},factura.fechaCai={factura.fechaCai},factura.sucursal={factura.sucursal},factura.fechaEmision={factura.fechaEmision},factura.fechaVencimiento={factura.fechaVencimiento},factura.tipoAdquisicion={factura.tipoAdquisicion},factura.esOriginal={factura.esOriginal},factura.formaPago.pk.secuencia={factura.formaPago.pk.secuencia},factura.tipoComprobante.codigo={factura.tipoComprobante.codigo},factura.origenComprobante={factura.origenComprobante},navegacionIdBack=${navegacionIdBack},oidParameter=${oidParameter?c},facturaRubroCtaContable={facturaRubroCtaContable},facturaDetalle.cuentaContable.codigo={facturaDetalle.cuentaContable.codigo},facturaDetalle.rubro.oid={facturaDetalle.rubro.oid},facturaDetalle.importeNoGravado={facturaDetalle.importeNoGravado},facturaDetalle.importeGravado={facturaDetalle.importeGravado},facturaDetalle.descripcion={facturaDetalle.descripcion},factura.cliente.oid={factura.cliente.oid},factura.cliente.descripcion={factura.cliente.descripcion},factura.proveedor.detalleDePersona.pk.secuencia={factura.proveedor.detalleDePersona.pk.secuencia},factura.proveedor.detalleDePersona.pk.identificador={factura.proveedor.detalleDePersona.pk.identificador},factura.proveedor.detalleDePersona.persona.oid={factura.proveedor.detalleDePersona.persona.oid},factura.proveedor.nombreFantasia={factura.proveedor.nombreFantasia},factura.proveedor.detalleDePersona.razonSocial={factura.proveedor.detalleDePersona.razonSocial},factura.proveedor.detalleDePersona.persona.docFiscal={factura.proveedor.detalleDePersona.persona.docFiscal},factura.proveedor.detalleDePersona.persona.situacionFiscal.codigo={factura.proveedor.detalleDePersona.persona.situacionFiscal.codigo},facturaDetalle.importeExento={facturaDetalle.importeExento}"/>
 

  <@vc.htmlContent 
  baseUrl="${request.contextPath}/comprobante/factura/validarCodigoBarraFacturaRubroCtaContable.action" 
  source="validarCodigoBarra" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId=${navigationId},flowControl=change,factura.proveedor.nroProveedor={factura.proveedor.nroProveedor},factura.proveedor.razonSocial={factura.proveedor.razonSocial},factura.proveedor.nombre={factura.proveedor.nombre},factura.numeroFactura={factura.numeroFactura},factura.cai={factura.cai},factura.fechaCai={factura.fechaCai},factura.sucursal={factura.sucursal},factura.fechaEmision={factura.fechaEmision},factura.fechaVencimiento={factura.fechaVencimiento},factura.tipoAdquisicion={factura.tipoAdquisicion},factura.esOriginal={factura.esOriginal},factura.formaPago.pk.secuencia={factura.formaPago.pk.secuencia},factura.tipoComprobante.codigo={factura.tipoComprobante.codigo},factura.origenComprobante={factura.origenComprobante},navegacionIdBack=${navegacionIdBack},oidParameter=${oidParameter?c},facturaRubroCtaContable={facturaRubroCtaContable},facturaDetalle.cuentaContable.codigo={facturaDetalle.cuentaContable.codigo},facturaDetalle.rubro.oid={facturaDetalle.rubro.oid},facturaDetalle.importeNoGravado={facturaDetalle.importeNoGravado},facturaDetalle.importeGravado={facturaDetalle.importeGravado},facturaDetalle.descripcion={facturaDetalle.descripcion},factura.cliente.oid={factura.cliente.oid},factura.cliente.descripcion={factura.cliente.descripcion},codigoBarra={codigoBarra},facturaDetalle.importeExento={facturaDetalle.importeExento}"/>

  
  <@vc.htmlContent 
  baseUrl="${request.contextPath}/comprobante/factura/limpiarFacturaRubroCtaContable.action" 
  source="limpiarDatosProveedor" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId=${navigationId},flowControl=change,factura.numeroFactura={factura.numeroFactura},factura.fechaEmision={factura.fechaEmision},factura.fechaVencimiento={factura.fechaVencimiento},factura.tipoAdquisicion={factura.tipoAdquisicion},factura.esOriginal={factura.esOriginal},factura.origenComprobante={factura.origenComprobante},navegacionIdBack=${navegacionIdBack},oidParameter=${oidParameter?c},facturaRubroCtaContable={facturaRubroCtaContable},facturaDetalle.cuentaContable.codigo={facturaDetalle.cuentaContable.codigo},facturaDetalle.rubro.oid={facturaDetalle.rubro.oid},facturaDetalle.importeNoGravado={facturaDetalle.importeNoGravado},facturaDetalle.importeGravado={facturaDetalle.importeGravado},facturaDetalle.descripcion={facturaDetalle.descripcion},factura.cliente.oid={factura.cliente.oid},factura.cliente.descripcion={factura.cliente.descripcion},facturaDetalle.importeExento={facturaDetalle.importeExento}"/>


  <@vc.htmlContent 
  baseUrl="${request.contextPath}/comprobante/factura/validarNroProveedorFacturaRubroCtaContable.action" 
  source="validarNroProveedor" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId=${navigationId},flowControl=change,factura.proveedor.nroProveedor={factura.proveedor.nroProveedor},factura.proveedor.razonSocial={factura.proveedor.razonSocial},factura.proveedor.nombre={factura.proveedor.nombre},factura.numeroFactura={factura.numeroFactura},factura.cai={factura.cai},factura.fechaCai={factura.fechaCai},factura.sucursal={factura.sucursal},factura.fechaEmision={factura.fechaEmision},factura.fechaVencimiento={factura.fechaVencimiento},factura.tipoAdquisicion={factura.tipoAdquisicion},factura.esOriginal={factura.esOriginal},factura.formaPago.pk.secuencia={factura.formaPago.pk.secuencia},factura.tipoComprobante.codigo={factura.tipoComprobante.codigo},factura.origenComprobante={factura.origenComprobante},navegacionIdBack=${navegacionIdBack},oidParameter=${oidParameter?c},facturaRubroCtaContable={facturaRubroCtaContable},facturaDetalle.cuentaContable.codigo={facturaDetalle.cuentaContable.codigo},facturaDetalle.rubro.oid={facturaDetalle.rubro.oid},facturaDetalle.importeNoGravado={facturaDetalle.importeNoGravado},facturaDetalle.importeGravado={facturaDetalle.importeGravado},facturaDetalle.descripcion={facturaDetalle.descripcion},factura.cliente.oid={factura.cliente.oid},factura.cliente.descripcion={factura.cliente.descripcion},nroProveedor={nroProveedor},facturaDetalle.importeExento={facturaDetalle.importeExento}"/>  
      
  <@ajax.select
  baseUrl="${request.contextPath}/comprobante/factura/refrescarTipoFactura.action" 
  source="factura.proveedor.detalleDePersona.persona.situacionFiscal.codigo" 
  target="factura.tipoComprobante.codigo"
  parameters="factura.proveedor.detalleDePersona.persona.situacionFiscal.codigo={factura.proveedor.detalleDePersona.persona.situacionFiscal.codigo}"
  parser="new ResponseXmlParser()"/>
  
