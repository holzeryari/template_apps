<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancelar" type="button" name="btnCancelar" value="Cancelar" class="boton"/>
				<input id="modificar" type="button" name="btnModificar" value="Modificar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/comprobante/factura/updateFacturaTotales.action" 
  source="modificar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="factura.pkFactura.nro_proveedor=${factura.pkFactura.nro_proveedor?c},factura.pkFactura.nro_factura=${factura.pkFactura.nro_factura?c},factura.pkFactura.sucursal=${factura.pkFactura.sucursal?c},factura.pkFactura.tip_docum=${factura.pkFactura.tip_docum?c},factura.percepciones={factura.percepciones},factura.percIb={factura.percIb},factura.percSUSS={factura.percSUSS},factura.percMunic={factura.percMunic},factura.impInternos={factura.impInternos},navegacionIdBack=${navegacionIdBack}"/>
  
 
 <@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancelar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=${navegacionIdBack},flowControl=back"/>
  
 
 