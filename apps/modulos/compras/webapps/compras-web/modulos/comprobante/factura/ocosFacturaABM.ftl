<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign tiles=JspTaglibs["/WEB-INF/tlds/tiles-jsp.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<@s.hidden id="navigationId" name="navigationId"/>
<@s.hidden id="facturaOCOS.oid" name="facturaOCOS.oid"/>
<@s.hidden id="facturaOCOS.versionNumber" name="facturaOCOS.versionNumber"/>

<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
			<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	

	<@tiles.insertAttribute name="factura"/>
				
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos de la Orden de Compra</b></td>
			</tr>
		</table>

		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>			
			<tr>
	  			<td class="textoCampo">N&uacute;mero:</td>
	  			<td class="textoDato">
	  			<@s.property default="&nbsp;" escape=false value="facturaOCOS.ocos.numero"/></td>
	  			<td  class="textoCampo">Fecha: </td>
				<td class="textoDato">
				<@s.if test="facturaOCOS.ocos.fecha != null">
				<#assign fecha = facturaOCOS.ocos.fecha> 
				${fecha?string("dd/MM/yyyy")} 	
				</@s.if>
				<@s.else>
					&nbsp;
				</@s.else>									
				</td>	      			
			</tr>	

			<tr>
				<td class="textoCampo">Tipo:</td>
	      		<td class="textoDato">
	      			<@s.property default="&nbsp;" escape=false value="facturaOCOS.ocos.tipo"/>
	      		</td>
				<td class="textoCampo">Proveedor:</td>
	      		<td class="textoDato">	      			
	      			<@s.property default="&nbsp;" escape=false value="facturaOCOS.ocos.proveedor.detalleDePersona.razonSocial" />
	      			<@s.property default="&nbsp;" escape=false value="facturaOCOS.ocos.proveedor.detalleDePersona.nombre" />							
				</td>
	    	</tr>
	    		
	    	<tr>
				<td class="textoCampo">Fecha de Entrega/Prestaci&oacute;n:</td>
      			<td class="textoDato" colspan="3">
      			<@s.if test="facturaOCOS.ocos.fechaEntregaPrestacion != null">
				<#assign fecha = facturaOCOS.ocos.fechaEntregaPrestacion> 
				${fecha?string("dd/MM/yyyy")} 	
				</@s.if>
				<@s.else>
					&nbsp;
				</@s.else>
				</td>	
			</tr>
				
			<tr>
				<td class="textoCampo">Lugar de Entrega/Prestaci&oacute;n:</td>
      			<td  class="textoDato" colspan="3">
      				<@s.property default="&nbsp;" escape=false value="facturaOCOS.ocos.lugarEntregaPrestacion" />	
								&nbsp;			 
				</td>					
    		</tr>
    		
	    	<tr>
			<td class="textoCampo">Observaciones:</td>
	      	<td  class="textoDato" colspan="3">
				<@s.property default="&nbsp;" escape=false value="facturaOCOS.ocos.observaciones" />			 
					&nbsp;
			</td>					
	    </tr>
	    		
	    <tr>
			<td class="textoCampo">Estado:</td>
	      	<td  class="textoDato">
				<@s.property default="&nbsp;" escape=false value="facturaOCOS.ocos.estado"/>
			</td>
					
			<#-- estado de la oc_os = "Intervenida"-->
	    	<#if facturaOCOS.ocos?exists && facturaOCOS.ocos.estado.ordinal() == 7>
				<tr>
					<td class="textoCampo">Observaciones Intervenci&oacute;n:</td>
					<td  colspan="3" class="textoDato">
						<@s.property default="&nbsp;" escape=false value="facturaOCOS.ocos.observacionTipoInt"/>
					</td>
				</tr>		
			<#else>
				<td class="textoCampo">&nbsp;</td>
	      		<td class="textoDato">&nbsp;</td>
	      	</#if>
	    </tr>
	    <tr><td colspan="4" class="textoCampo">&nbsp;</td></tr>
	  </table>		
	</div> 
	<#if facturaOCOS.ocos.tipo.ordinal()==1>		
		<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
				<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
					<tr>
						<td>Productos/Bienes de la Orden de Compra</td>
					</tr>
				</table>										
										
				<!-- Resultado Filtro -->						
				<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="facturaOCOS.ocos.oc_osDetalleList" id="oc_osDetalle" defaultsort=2>	
						
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="numero" title="N&uacute;mero" />
			
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="productoBien.oid" title="C&oacute;digo" />
			 	
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="productoBien.descripcion" title="Descripci&oacute;n" />									
					
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="productoBien.marca" title="Marca" />
				
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="productoBien.modelo" title="Modelo" />
				
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="productoBien.rubro.descripcion" title="Rubro" />
			
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="cantidadPedida" title="Cantidad" />			
			
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="precio" title="Precio" />
				
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="subtotal" title="Subtotal" />
					
			</@display.table>
		</div>	
			
	<#else>
		<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
				<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
					<tr>
						<td>Servicios de la Orden de Servicio</td>
					</tr>
				</table>	
				
				<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="facturaOCOS.ocos.oc_osDetalleList" id="oc_osDetalle" defaultsort=2>	
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="numero" title="N&uacute;mero" />
				
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="servicio.oid" title="C&oacute;digo" />
				 	
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="servicio.descripcion" title="Descripci&oacute;n" />									
						
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="servicio.rubro.descripcion" title="Rubro" />
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="servicio.tipo" title="Tipo" />
				
					<#--<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="cantidadPedida" title="Cantidad" />-->			
				
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="precio" title="Precio" />
					
					<#--<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="subtotal" title="Subtotal" />-->
				
					
				</@display.table>
			</div>	
		
		</#if>
										
		