
<#setting locale="es_AR">

<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>

<div id="capaSeparadora" class="capaSeparadora">&nbsp;</td></tr></div>
<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">

<@s.if test="ocosPendienteFacturacion!=null && ocosPendienteFacturacion.size()>0">

				<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>OC/OS Pendiente de Facturacion del Proveedor</td>
				<tr>
				</table>


		
          		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="ocosPendienteFacturacion" id="oc_os" decorator="ar.com.riouruguay.web.actions.compraContratacion.oc_os.decorators.OC_OSDecorator" defaultsort=2 >					
				
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="numero" title="N&uacute;mero" />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="fecha" format="{0,date,dd/MM/yyyy}"  title="Fecha"/>					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" title="Proveedor">  
						${oc_os.proveedor.detalleDePersona.razonSocial} 
						
						<#if oc_os.proveedor.detalleDePersona.nombre?exists> 
				 			${oc_os.proveedor.detalleDePersona.nombre}
						</#if>
						
					</@display.column> 
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="importe" title="Importe" />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="estado" title="Estado" />					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="recibidaParcialString" title="Recibida Parcial" />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="recibidaTotalString" title="Recibida Total" />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="facturadaParcialStringSeleccion" title="Facturada Parcial" />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="facturadaTotalStringSeleccion" title="Facturada Total" />
					
					
				</@display.table>

</@s.if>
 </div>