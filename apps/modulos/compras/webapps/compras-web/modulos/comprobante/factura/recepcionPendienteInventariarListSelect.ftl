
<#setting locale="es_AR">

<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>


<@s.hidden id="navigationId" name="navigationId"/>
<@s.hidden id="recepcionSelecccionList" name="recepcionSelecccionList"/>
<@s.hidden id="factura.cliente.oid" name="factura.cliente.oid"/>
<@s.hidden id="factura.cliente.descripcion" name="factura.cliente.descripcion"/>
<@s.hidden id="factura.proveedor.nroProveedor" name="factura.proveedor.nroProveedor"/>
<@s.hidden id="factura.proveedor.detalleDePersona.razonSocial" name="factura.proveedor.detalleDePersona.razonSocial"/>
<@s.hidden id="factura.proveedor.detalleDePersona.nombre" name="factura.proveedor.detalleDePersona.nombre"/>

<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
			
			<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	

    <!-- Resultado Filtro -->
	<@s.if test="facturaList!=null">
		<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
				<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
					<tr>
						<td>Recepciones de Bienes pendientes de inventariar</td>
					</tr>
				</table>	
	
				<@vc.anchors target="contentTrx">			
          		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="facturaList" id="factura" defaultsort=2 >
          		
  					<@display.column headerClass="tbl-contract-service-select" class="botoneraAnchoCon2" title="Acciones">
						<div class="alineacion">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0443" 
							enabled="factura.readable" 
							cssClass="item" 
							href="${request.contextPath}/comprobante/factura/readView.action?factura.pkFactura.nro_factura=${factura.pkFactura.nro_factura?c}&factura.pkFactura.nro_proveedor=${factura.pkFactura.nro_proveedor?c}&factura.pkFactura.sucursal=${factura.pkFactura.sucursal?c}&factura.pkFactura.tip_docum=${factura.pkFactura.tip_docum?c}&navigationId=factura-visualizar&flowControl=regis&navegacionIdBack=${navigationId}">
							<img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver"  border="0">
						</@security.a>
						<@s.checkbox label="${factura.oid}" fieldValue="${factura.oid}" name="checkRecepcionSeleccion"/>
						</div>
						
					</@display.column>


					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="numero" title="Nro." />				
										
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" title="Proveedor">  
						${factura.proveedor.detalleDePersona.razonSocial} 
						<#if factura.proveedor.detalleDePersona.nombre?exists> 
							${factura.proveedor.detalleDePersona.nombre}
						</#if>
						
						
						
					</@display.column>
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="fechaRecepcion" format="{0,date,dd/MM/yyyy}"  title="Fecha Recepci&oacute;n" /> 
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="comprobante.descripcion" title="Dep&oacute;sito" />
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="cliente.descripcion" title="Cliente" />

					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="estado" title="Estado" />		

				</@display.table>
			</@vc.anchors>
		</div>	
	</@s.if>				
	
	<div id="capaBotonera" class="capaBotonera">
		<table id="tablaBotonera" class="tablaBotonera">
			<tr> 
				<td align="left">
					<input id="cancel" type="button" name="btnVolver" value="Volver" class="boton"/>
					<input id="seleccionar" type="button" name="btnSeleccionar" value="Seleccionar Recepciones" class="boton"/>
				</td>
			</tr>	
		</table>
	</div>	
			         		
	  		
<@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId=${navegacionIdBack},flowControl=back"/>
  
  <@vc.htmlContent 
  baseUrl="${request.contextPath}/comprobante/factura/selecccionMultipleRecepcion.action" 
  source="seleccionar" 
  success="contentTrx" 
  failure="errorTrx" 
  preFunction="listaRecepcionesPendientes"
  parameters="navigationId=${navigationId},navegacionIdBack=${navegacionIdBack},recepcionSelecccionList={recepcionSelecccionList}"/>
  
 
 

