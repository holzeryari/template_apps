<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign tiles=JspTaglibs["/WEB-INF/tlds/tiles-jsp.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>

<@s.hidden id="navigationId" name="navigationId"/>
<@s.hidden id="navegacionIdBack" name="navegacionIdBack"/>

<@s.hidden id="fondoFijo.oid" name="fondoFijo.oid"/>
<@s.hidden id="fondoFijo.versionNumber" name="fondoFijo.versionNumber"/>

<@s.hidden id="fondoFijo.cliente.oid" name="fondoFijo.cliente.oid"/>
<@s.hidden id="fondoFijo.cliente.descripcion" name="fondoFijo.cliente.descripcion"/>
<@s.hidden id="fondoFijo.cliente.bolsaCorreo" name="fondoFijo.cliente.bolsaCorreo"/>

<@s.hidden id="fondoFijo.detallePersona.pk.secuencia" name="fondoFijo.detallePersona.pk.secuencia"/>
<@s.hidden id="fondoFijo.detallePersona.pk.identificador" name="fondoFijo.detallePersona.pk.identificador"/>

<@s.hidden id="fondoFijo.detallePersona.razonSocial" name="fondoFijo.detallePersona.razonSocial"/>
<@s.hidden id="fondoFijo.detallePersona.nombre" name="fondoFijo.detallePersona.nombre"/>

<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
			<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos del Fondo Fijo</b></td>
				<td>
					<#if fondoFijo.estado?exists && fondoFijo.estado.ordinal() == 1 >			
					<div align="right">
						<@s.a templateDir="custontemplates" id="modificarFondoFijo" name="modificarFondoFijo" href="javascript://nop/" cssClass="ocultarIcono">
							<b>Modificar</b><img src="${request.contextPath}/common/images/modificar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
						</@s.a>
					</div>
					</#if>
				</td>
			</tr>
		</table>

		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>		
			<tr>
      			<td class="textoCampo">N&uacute;mero:</td>
      			<td class="textoDato">
      			<@s.property default="&nbsp;" escape=false value="fondoFijo.numero"/></td>
      			<td  class="textoCampo">Fecha Comprobante: </td>
				<td class="textoDato">
				<@s.if test="fondoFijo.fechaComprobante != null">
				<#assign fecha = fondoFijo.fechaComprobante> 
				${fecha?string("dd/MM/yyyy")}	
				</@s.if>	&nbsp;								
				</td>	      			
			</tr>		     
	    	<tr>      			      		
				<td class="textoCampo">Cliente:</td>
      			<td class="textoDato" colspan="3">
				<@s.property default="&nbsp;" escape=false value="fondoFijo.cliente.descripcion"/>
						<@s.a templateDir="custontemplates" id="seleccionarCliente" name="seleccionarCliente" href="javascript://nop/" cssClass="ocultarIcono">
								<img src="${request.contextPath}/common/images/buscar.gif" align="top" border="0" hspace="3">
						</@s.a>
										
				</td>	
			</tr>
			<tr>
				<td class="textoCampo">Responsable Agencia:</td>
      			<td class="textoDato">
				<@s.property default="&nbsp;" escape=false value="fondoFijo.detallePersona.razonSocial" />
				<@s.property default="&nbsp;" escape=false value="fondoFijo.detallePersona.nombre" />
						<@s.a templateDir="custontemplates" id="seleccionarPersona" name="seleccionarPersona" href="javascript://nop/" cssClass="ocultarIcono">
								<img src="${request.contextPath}/common/images/buscar.gif" align="top" border="0" hspace="3">
						</@s.a>
										
					</td>	
				<td class="textoCampo">Fecha Rendici&oacute;n:</td>
      			<td class="textoDato">
					<@vc.rowCalendar 
					templateDir="custontemplates" 
					id="fondoFijo.fechaRendicion" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="fondoFijo.fechaRendicion" 
					title="Fecha Rendicion" />
				</td>
    		</tr>
			<tr>
				<td class="textoCampo">Fecha Per&iacute;odo Desde:</td>
      			<td class="textoDato">
					<@vc.rowCalendar 
					templateDir="custontemplates" 
					id="fondoFijo.fechaDesde" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="fondoFijo.fechaDesde" 
					title="Fecha Desde" />
				</td>
				<td class="textoCampo">Fecha Per&iacute;odo Hasta:</td>
      			<td class="textoDato">
				<@vc.rowCalendar 
					templateDir="custontemplates" 
					id="fondoFijo.fechaHasta" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="fondoFijo.fechaHasta" 
					title="Fecha Hasta" />
				</td>
			</tr>
	    	<tr>
	    		<td class="textoCampo">Importe Total:</td>
	      		<td  class="textoDato">
					<@s.if test="fondoFijo.totalFondoFijo != null">
					<#assign totalFondoFijo = fondoFijo.totalFondoFijo> 
					${totalFondoFijo?string(",##0.00")}	
					</@s.if>	&nbsp;	
				</td>
				
				<#if fondoFijo.ordenPago?exists && fondoFijo.ordenPago.oid?exists>
					<td class="textoCampo">Orden de Pago:</td>
	      			<td  class="textoDato" colspan="3" >
					<@s.property default="&nbsp;" escape=false value="fondoFijo.ordenPago.numero"/>
					
					<@vc.anchors target="contentTrx">	
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0443" 
							enabled="fondoFijo.ordenPago.readable" 
							cssClass="item" 
							id="ver"
							name="ver"
							href="${request.contextPath}/pago/readView.action?ordenPago.oid=${fondoFijo.ordenPago.oid?c}&navigationId=ordenPago-visualizar&flowControl=regis&navegacionIdBack=${navigationId}">
							<img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver"  border="0">
						</@security.a>
					</@vc.anchors>
					</td>
				<#else>
					<td class="textoCampo">&nbsp;</td>
	      			<td  class="textoDato">&nbsp;</td>
				</#if>
			</tr>
			<tr>    		
				<td class="textoCampo">Estado:</td>
	      		<td  class="textoDato">
					<@s.property default="&nbsp;" escape=false value="fondoFijo.estado"/>
				</td>
				
				<td class="textoCampo">Nro. Bolsa:</td>
      			<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="fondoFijo.cliente.bolsaCorreo"/>
				</td>
	    	</tr>
	    	<tr><td colspan="4" class="textoCampo">&nbsp;</td></tr>
		</table>	
	</div>					
		