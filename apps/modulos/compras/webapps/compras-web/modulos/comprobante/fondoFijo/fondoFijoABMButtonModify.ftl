<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>

<div id="capaSeparadora" class="capaSeparadora">&nbsp;</div>
<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="left">
				<input id="cancelar" type="button" name="btnVolver" value="Volver" class="boton"/>
				<@security.button
						templateDir="custontemplates"
						securityCode="CUF0709" 
						enabled="true"
						name="btnActivar"
						id="store"
						value="Cancelar Fondo Fijo"
						cssClass="boton">
				</@security.button>&nbsp;
	

		        <#if fondoFijo.estado.ordinal() == 7  >			
					<@security.button
						templateDir="custontemplates"
						securityCode="CUF0823" 
						enabled="true"
						name="btnVerificar"
						id="verificar"
						value="Verificar Fondo Fijo"
						cssClass="boton">
					</@security.button>&nbsp;
				</#if>
			</td>
		</tr>	
	</table>
</div>
  
  
    <@vc.htmlContent 
 baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancelar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=${navegacionIdBack},flowControl=back"/>
  
   <@vc.htmlContent 
  baseUrl="${request.contextPath}/comprobante/fondoFijo/verificacionCCentral.action" 
  source="verificar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="fondoFijo.oid={fondoFijo.oid}"/>
  
  
  <@vc.htmlContent 
  baseUrl="${request.contextPath}/comprobante/fondoFijo/modifyFondoFijo.action" 
  source="store" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="fondoFijo.oid={fondoFijo.oid}"/>
