
<#setting locale="es_AR">

<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>

<@s.hidden id="navigationId" name="navigationId"/>
<@s.hidden id="fondoFijo.cliente.oid" name="fondoFijo.cliente.oid"/>
<@s.hidden id="fondoFijo.cliente.descripcion" name="fondoFijo.cliente.descripcion"/>

<@s.hidden id="fondoFijo.detallePersona.pk.secuencia" name="fondoFijo.detallePersona.pk.secuencia"/>
<@s.hidden id="fondoFijo.detallePersona.pk.identificador" name="fondoFijo.detallePersona.pk.identificador"/>

<@s.hidden id="fondoFijo.detallePersona.razonSocial" name="fondoFijo.detallePersona.razonSocial"/>
<@s.hidden id="fondoFijo.detallePersona.nombre" name="fondoFijo.detallePersona.nombre"/>

<@s.hidden id="clienteDuro" name="clienteDuro"/>


<@s.if test="clienteDuro == true">
	<@s.hidden id="seleccionarCliente" name="fondoFijo.cliente.descripcion"/>
</@s.if>

<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
        	<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
			<tr>
				<td><b>Datos del Fondo Fijo</b></td>
				<td>
					<div align="right">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0701" 
							enabled="fondoFijo.readable" 
							cssClass="item" 
							id="crear"										
							href="javascript://nop/">
							<b >Agregar</b>										
							<img src="${request.contextPath}/common/images/agregar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
						</@security.a>						
					</div>
				</td>
			</tr>
		</table>

		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
	    		<td class="textoCampo" colspan="4">&nbsp;</td>
	    	</tr>		
			<tr>
				<td class="textoCampo">N&uacute;mero:</td>
	      		<td class="textoDato">
      			<@s.textfield 
      					templateDir="custontemplates" 
						id="fondoFijo.numero" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="fondoFijo.numero" 
						title="N&uacute;mero" />
				</td>				

               	<td class="textoCampo">Cliente:</td>
      			<td class="textoDato">	      			
      			<@s.property default="&nbsp;" escape=false value="fondoFijo.cliente.descripcion"/>	      	
      			    <@s.a templateDir="custontemplates" id="seleccionarCliente" name="seleccionarCliente" href="javascript://nop/">		
					    <img src="${request.contextPath}/common/images/buscar.gif" border="0" align="absmiddle" hspace="3" >
					</@s.a>		
					</td>	
			</tr>
			<tr>
				<td class="textoCampo">Estado:</td>
      			<td class="textoDato">
      			<@s.select 
							templateDir="custontemplates" 
							id="fondoFijo.estado" 
							cssClass="textarea"
							cssStyle="width:165px" 
							name="fondoFijo.estado" 
							list="estadoList" 
							listKey="key" 
							listValue="description" 
							value="fondoFijo.estado.ordinal()"
							title="Estado"
							headerKey="0"
							headerValue="Todos"							
							/>
				</td>
				<td class="textoCampo">Responsable Agencia:</td>
      			<td class="textoDato">
				<@s.property default="&nbsp;" escape=false value="fondoFijo.detallePersona.razonSocial" />
				<@s.property default="&nbsp;" escape=false value="fondoFijo.detallePersona.nombre" />
					<@s.a templateDir="custontemplates" id="seleccionarPersona" name="seleccionarPersona" href="javascript://nop/" cssClass="ocultarIcono">
							<img src="${request.contextPath}/common/images/buscar.gif" align="top" border="0" hspace="3">
					</@s.a>
				</td>	
			</tr>
			<tr>
				<td class="textoCampo">Fecha Rendicion Desde:</td>
      			<td class="textoDato">
					<@vc.rowCalendar 
					templateDir="custontemplates" 
					id="fechaRendicionDesde" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="fondoFijo.fechaRendicionDesde" 
					value="fechaRendicionDesde"
					title="Fecha Desde" />
				</td>
				<td class="textoCampo">Fecha Rendicion Hasta:</td>
      			<td class="textoDato">
				<@vc.rowCalendar 
					templateDir="custontemplates" 
					id="fechaRendicionHasta" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="fondoFijo.fechaRendicionHasta" 
					value="fechaRendicionHasta"
					title="Fecha Hasta" />
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Fecha Per&iacute;odo Desde:</td>
      			<td class="textoDato">
					<@vc.rowCalendar 
					templateDir="custontemplates" 
					id="fondoFijo.fechaDesde" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="fondoFijo.fechaDesde" 
					value="fondoFijo.fechaDesde"
					title="Fecha Desde" />
				</td>
				<td class="textoCampo">Fecha Per&iacute;odo Hasta:</td>
      			<td class="textoDato">
				<@vc.rowCalendar 
					templateDir="custontemplates" 
					id="fondoFijo.fechaHasta" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="fondoFijo.fechaHasta" 
					value="fondoFijo.fechaHasta"
					title="Fecha Hasta" />
				</td>
				
			</tr>			
			<tr>	
				<td class="textoCampo">Despachado:</td>
				<td class="textoDato" colspan="3">
				    <@s.select				    
				    		templateDir="custontemplates" 
							id="fondoFijo.esDespachado" 
							cssClass="textarea"
							cssStyle="width:165px" 
							name="fondoFijo.esDespachado" 
							list="esDespachadoList" 
							listKey="key" 
							listValue="description" 
							value="fondoFijo.esDespachado.ordinal()"
							title="Es Despachado"
							headerKey="0"
							headerValue="Seleccionar"							
					/>	
				</td>					
			</tr>					
						
			<tr>
    			<td colspan="4" class="lineaGris" align="right">
      			<input id="limpiar" type="button" name="btnVolver" value="Limpiar" class="boton"/>		
      			<input id="buscar" type="button" name="btnVolver" value="Buscar" class="boton"/>
    			</td>
			</tr>	
		</table>
	</div>	
			
	<!-- Resultado Filtro -->
	<@s.if test="fondoFijoList!=null">
		<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
				<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>Fondos Fijos encontrados</td>					
			
				</tr>
			</table>	
			
			<@vc.anchors target="contentTrx">			
          		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="fondoFijoList" id="fondoFijo" pagesize=15  defaultsort=2 partialList=true size="recordSize" keepStatus=true excludedParams="resetFlag">
          			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select" class="botoneraAnchoCon4" title="Acciones">
          			  <!-- Ver -->
						<div class="alineacion">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0703" 
							enabled="fondoFijo.readable" 
							cssClass="item" 
							href="${request.contextPath}/comprobante/fondoFijo/readView.action?fondoFijo.oid=${fondoFijo.oid?c}&navegacionIdBack=${navigationId}&navigationId=fondoFijo-visualizar&flowControl=regis">
							<img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver"  border="0">
						</@security.a>
						</div>
						
					<!-- Modificar: si el estado es Ingresado 2 o Con Obligacion de pago generada 4: permite modificar (cancelar la autorizacion o la obligacion de pago) -->
					<!-- Se agregan aca: Verificado en Casa central 8(equiv. ingresado), "Pendiente de Autorizacion en Agencia"),//6
					     "Autorizado en agencia"),//7
					                        -->
					<#if fondoFijo.estado.ordinal() == 2 || fondoFijo.estado.ordinal() == 4  ||
					     fondoFijo.estado.ordinal() == 7 || fondoFijo.estado.ordinal() == 8 >
						<div class="alineacion">							
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0708" 
							enabled="fondoFijo.enable"
							cssClass="item"  
							href="${request.contextPath}/comprobante/fondoFijo/administrarView.action?fondoFijo.oid=${fondoFijo.oid?c}&navegacionIdBack=${navigationId}&navigationId=fondoFijo-administrar&flowControl=regis">
							<img  src="${request.contextPath}/common/images/administrar.gif" alt="Administrar" title="Administrar"  border="0">
						</@security.a>
						</div>
					<#else>
					  <!-- Administrar -->	
						<div class="alineacion">			
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0704" 
							enabled="fondoFijo.updatable"
							cssClass="item"  
							href="${request.contextPath}/comprobante/fondoFijo/administrarView.action?fondoFijo.oid=${fondoFijo.oid?c}&navegacionIdBack=${navigationId}&navigationId=fondoFijo-administrar&flowControl=regis">
							<img  src="${request.contextPath}/common/images/administrar.gif" alt="Administrar" title="Administrar"  border="0">
						</@security.a>
						</div>
					</#if>
						
					  <!-- Eliminar -->	
						<div class="alineacion">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0705" 
							enabled="fondoFijo.eraseable"
							cssClass="item"  
							href="${request.contextPath}/comprobante/fondoFijo/deleteView.action?fondoFijo.oid=${fondoFijo.oid?c}&navigationId=fondoFijo-eliminar&flowControl=regis">
							<img  src="${request.contextPath}/common/images/eliminar.gif" alt="Eliminar" title="Eliminar"  border="0">
						</@security.a>				
						</div>
						
					  <!-- Autorizar en Agencia -->
						<div class="alineacion">
						<#assign autorizable = 'false'>
						<#if fondoFijo.estado?exists && fondoFijo.estado.ordinal() == 6>
							<#assign autorizable = 'true'>
						</#if>
											
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0822" 
							enabled="${autorizable}"
							cssClass="item"  
							href="${request.contextPath}/comprobante/fondoFijo/authorizationViewAgencia.action?fondoFijo.oid=${fondoFijo.oid?c}&navegacionIdBack=${navigationId}">
							<img  src="${request.contextPath}/common/images/pino.gif" alt="Autorizar Fondo Fijo" title="Autorizar en Agencia Fondo Fijo"  border="0">
						</@security.a>
						</div>						
						
					<!-- Autorizar -->
					<!-- Se agregan aca: Verificado en Casa central 8 (equiv. ingresado), "Ingresado"), 2-->
						<div class="alineacion">
						<#assign autorizable = 'false'>
						<#if fondoFijo.estado?exists && (fondoFijo.estado.ordinal() == 2 || fondoFijo.estado.ordinal() == 8)>
							<#assign autorizable = 'true'>
						</#if>
						
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0710" 
							enabled="${autorizable}"
							cssClass="item"  
							href="${request.contextPath}/comprobante/fondoFijo/authorizationView.action?fondoFijo.oid=${fondoFijo.oid?c}&navegacionIdBack=${navigationId}">
							<img  src="${request.contextPath}/common/images/usuarioAutoriza.png" alt="Autorizar Fondo Fijo" title="Autorizar Fondo Fijo"  border="0">
						</@security.a>
						</div>
					
					<!-- ordenPagoQuitado 
						<div class="alineacion">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0709" 
							enabled="fondoFijo.enable"
							cssClass="item"  
							href="${request.contextPath}/pago/createDesdeFondoFijoView.action?fondoFijo.oid=${fondoFijo.oid?c}&navegacionIdBack=${navigationId}">
							<img  src="${request.contextPath}/common/images/ordenPago.gif" alt="Generar Orden de Pago" title="Generar Orden de Pago"  border="0">
						</@security.a>
						</div>
					-->
					
					<!-- Imprimir -->
					<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0703" 
							enabled="fondoFijo.readable" 
							cssClass="no-rewrite" 
							href="${request.contextPath}/comprobante/fondoFijo/printFondoFijoPDF.action?fondoFijo.oid=${fondoFijo.oid?c}">
							<img src="${request.contextPath}/common/images/imprimir.gif" title="Imprimir Fondo Fijo" alt="Imprimir"  align="absmiddle" border="0" hspace="3" >
			
						</@security.a>	
					</@display.column>


					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="numero" title="Nro." />				
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="fechaRendicion" format="{0,date,dd/MM/yyyy}" title="Fecha Rendicion" />
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="cliente.descripcion" title="Cliente" />
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" title="Responsable Agencia">  
						${fondoFijo.detallePersona.razonSocial}
						<#if fondoFijo.detallePersona.nombre?exists>
						 ${fondoFijo.detallePersona.nombre}
						 </#if>
					</@display.column>
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="fechaDesde" format="{0,date,dd/MM/yyyy}" title="Fecha Desde" />
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="fechaHasta" format="{0,date,dd/MM/yyyy}" title="Fecha Hasta" />				
										
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="estado" title="Estado" />		
					
				</@display.table>
			</@vc.anchors>
		</div>	
	</@s.if>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/comprobante/fondoFijo/search.action"
  source="buscar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="resetFlag=true,navigationId={navigationId},flowControl=regis,fondoFijo.estado={fondoFijo.estado},fondoFijo.cliente.oid={fondoFijo.cliente.oid},fondoFijo.cliente.descripcion={fondoFijo.cliente.descripcion},fondoFijo.numero={fondoFijo.numero},fechaRendicionDesde={fechaRendicionDesde},fechaRendicionHasta={fechaRendicionHasta},fondoFijo.fechaDesde={fondoFijo.fechaDesde},fondoFijo.fechaHasta={fondoFijo.fechaHasta},fondoFijo.detallePersona.pk.secuencia={fondoFijo.detallePersona.pk.secuencia},fondoFijo.detallePersona.pk.identificador={fondoFijo.detallePersona.pk.identificador},fondoFijo.detallePersona.razonSocial={fondoFijo.detallePersona.razonSocial},fondoFijo.detallePersona.nombre={fondoFijo.detallePersona.nombre},fondoFijo.esDespachado={fondoFijo.esDespachado}"/>
  		      

<@vc.htmlContent 
  baseUrl="${request.contextPath}/comprobante/fondoFijo/view.action"
  source="limpiar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=regis"/>
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/comprobante/fondoFijo/createView.action" 
  source="crear" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=fondoFijo-create,flowControl=regis"/>
  
  
  <@vc.htmlContent 
  baseUrl="${request.contextPath}/comprobante/fondoFijo/selectClienteSearch.action" 
  source="seleccionarCliente" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=change,fondoFijo.estado={fondoFijo.estado},fondoFijo.cliente.oid={fondoFijo.cliente.oid},fondoFijo.cliente.descripcion={fondoFijo.cliente.descripcion},fondoFijo.numero={fondoFijo.numero},fechaRendicionDesde={fechaRendicionDesde},fechaRendicionHasta={fechaRendicionHasta},fondoFijo.fechaDesde={fondoFijo.fechaDesde},fondoFijo.fechaHasta={fondoFijo.fechaHasta},fondoFijo.detallePersona.pk.secuencia={fondoFijo.detallePersona.pk.secuencia},fondoFijo.detallePersona.pk.identificador={fondoFijo.detallePersona.pk.identificador},fondoFijo.detallePersona.razonSocial={fondoFijo.detallePersona.razonSocial},fondoFijo.detallePersona.nombre={fondoFijo.detallePersona.nombre}"/>
  

  <@vc.htmlContent 
  baseUrl="${request.contextPath}/comprobante/fondoFijo/selectPersona.action" 
  source="seleccionarPersona" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=change,fondoFijo.estado={fondoFijo.estado},fondoFijo.cliente.oid={fondoFijo.cliente.oid},fondoFijo.cliente.descripcion={fondoFijo.cliente.descripcion},fondoFijo.numero={fondoFijo.numero},fechaRendicionDesde={fechaRendicionDesde},fechaRendicionHasta={fechaRendicionHasta},fondoFijo.fechaDesde={fondoFijo.fechaDesde},fondoFijo.fechaHasta={fondoFijo.fechaHasta},fondoFijo.detallePersona.pk.secuencia={fondoFijo.detallePersona.pk.secuencia},fondoFijo.detallePersona.pk.identificador={fondoFijo.detallePersona.pk.identificador},fondoFijo.detallePersona.razonSocial={fondoFijo.detallePersona.razonSocial},fondoFijo.detallePersona.nombre={fondoFijo.detallePersona.nombre}"/>
  
  