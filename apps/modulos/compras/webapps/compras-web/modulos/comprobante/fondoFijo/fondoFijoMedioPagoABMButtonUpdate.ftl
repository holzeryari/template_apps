<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancel" type="button" name="btnCancelar" value="Cancelar" class="boton"/>
				<input id="modificar" type="button" name="btnModificar" value="Modificar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=${navegacionIdBack},flowControl=back"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/comprobante/fondoFijo/updateFormaMedioPago.action"  
  source="modificar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="fondoFijoMedioPago.oid={fondoFijoMedioPago.oid},fondoFijoMedioPago.medioPago={medioPago},fondoFijoMedioPago.porcentaje={fondoFijoMedioPago.porcentaje},navegacionIdBack=${navegacionIdBack},fondoFijoMedioPago.formaPago={formaPago},fondoFijoMedioPago.observaciones={fondoFijoMedioPago.observaciones}, fondoFijoMedioPago.fechaAsignada={fechaAsignada}, fondoFijoMedioPago.cantidadDias={cantidadDias}, fondoFijoMedioPago.formaPago.conFechaAsignada={fondoFijoMedioPago.formaPago.conFechaAsignada}" />

<@ajax.updateField
  baseUrl="${request.contextPath}/comprobante/fondoFijo/refreshFondoFijoFormaPago.action" 
  source="formaPago" 
  target="fondoFijoMedioPago.formaPago.conFechaAsignada"
  action="formaPago"
  parameters="fondoFijoMedioPago.formaPago={formaPago}"
  eventType="change"
  postFunction="refreshFondoFijoFormaPago"
  parser="new ResponseXmlParser()"/>

  <@ajax.updateField
  baseUrl="${request.contextPath}/comprobante/fondoFijo/refreshFechaAsignada.action" 
  source="fechaAsignada" 
  target="cantidadDias"
  action="fechaAsignada"
  parameters="fondoFijoMedioPago.fechaAsignada={fechaAsignada}"
  eventType="change"
  parser="new ResponseXmlParser()"/>
  
  <@ajax.updateField
  baseUrl="${request.contextPath}/comprobante/fondoFijo/refreshFechaAsignada.action" 
  source="fechaAsignada" 
  target="cantidadDias"
  action="fechaAsignada"
  parameters="fondoFijoMedioPago.fechaAsignada={fechaAsignada}"
  eventType="blur"
  parser="new ResponseXmlParser()"/>
  
  
  <@ajax.updateField
  baseUrl="${request.contextPath}/comprobante/fondoFijo/refreshCantidadDias.action" 
  source="cantidadDias" 
  target="fechaAsignada"
  action="cantidadDias"
  parameters="fondoFijoMedioPago.cantidadDias={cantidadDias}"
  eventType="blur"
  parser="new ResponseXmlParser()"/>
    