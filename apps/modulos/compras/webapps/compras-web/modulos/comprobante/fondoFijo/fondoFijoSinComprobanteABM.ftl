<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign tiles=JspTaglibs["/WEB-INF/tlds/tiles-jsp.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<@s.hidden id="navigationId" name="navigationId"/>
<@s.hidden id="navegacionIdBack" name="navegacionIdBack"/>

<@s.hidden id="fondoFijoSinComprobante.oid" name="fondoFijoSinComprobante.oid"/>
<@s.hidden id="fondoFijoSinComprobante.versionNumber" name="fondoFijoSinComprobante.versionNumber"/>
<@s.hidden id="fondoFijoSinComprobante.fondoFijo.oid" name="fondoFijoSinComprobante.fondoFijo.oid"/>


<@s.hidden id="fondoFijoSinComprobante.rubro.oid" name="fondoFijoSinComprobante.rubro.oid"/>
<@s.hidden id="fondoFijoSinComprobante.rubro.descripcion" name="fondoFijoSinComprobante.rubro.descripcion"/>

<#if cuentaContableList.size()==1>
<@s.hidden id="fondoFijoSinComprobante.cuentaContable.codigo" name="fondoFijoSinComprobante.cuentaContable.codigo"/>
</#if>

<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
			<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos del Fondo Fijo</b></td>
			</tr>
		</table>

		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>		
			<tr>
      			<td class="textoCampo">N&uacute;mero:</td>
      			<td class="textoDato">
      			<@s.property default="&nbsp;" escape=false value="fondoFijoSinComprobante.fondoFijo.numero"/></td>
      			<td  class="textoCampo">Fecha Comprobante: </td>
				<td class="textoDato">
				<@s.if test="fondoFijoSinComprobante.fondoFijo.fechaComprobante != null">
				<#assign fechaComprobante = fondoFijoSinComprobante.fondoFijo.fechaComprobante> 
				${fechaComprobante?string("dd/MM/yyyy")}	
				</@s.if>	&nbsp;							
				</td>	      			
			</tr>		     
	    	<tr>      			      		
				<td class="textoCampo">Cliente:</td>
	  			<td class="textoDato">
				
				<@s.property default="&nbsp;" escape=false value="fondoFijoSinComprobante.fondoFijo.cliente.descripcion"/>		
					</td>	
				</td>
				
				<td class="textoCampo">Fecha Rendicion:</td>
	  			<td class="textoDato">
	
				<@s.if test="fondoFijoSinComprobante.fondoFijo.fechaRendicion != null">
				<#assign fechaRendicion = fondoFijoSinComprobante.fondoFijo.fechaRendicion> 
				${fechaRendicion?string("dd/MM/yyyy")}	
				</@s.if>	&nbsp;	
	
				</td>
			</tr>
			<tr>
				<td class="textoCampo" >Fecha Per&iacute;odo Desde:</td>
      			<td class="textoDato">

				<@s.if test="fondoFijoSinComprobante.fondoFijo.fechaDesde != null">
				<#assign fechaDesde = fondoFijoSinComprobante.fondoFijo.fechaDesde> 
				${fechaDesde?string("dd/MM/yyyy")}	
				</@s.if>	&nbsp;	
	
				</td>
				<td class="textoCampo">Fecha Per&iacute;odo Hasta:</td>
      			<td class="textoDato">
				
	
				<@s.if test="fondoFijoSinComprobante.fondoFijo.fechaHasta != null">
				<#assign fechaHasta = fondoFijoSinComprobante.fondoFijo.fechaHasta> 
				${fechaHasta?string("dd/MM/yyyy")}	
				</@s.if>	&nbsp;	
				</td>
			</tr>	
			<tr>
				<td class="textoCampo">Estado:</td>
      			<td  class="textoDato" colspan="3">
					<@s.property default="&nbsp;" escape=false value="fondoFijoSinComprobante.fondoFijo.estado"/>
				</td>
			</tr>
			<tr><td colspan="4" class="textoCampo">&nbsp;</td></tr>			
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos del Fondo Fijo Sin Comprobante</b></td>
			</tr>
		</table>

		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>		    		
	    	<tr>
				<td class="textoCampo">Descripci&oacute;n: </td>
				<td  class="textoDato">
    			<@s.textfield 
	      					templateDir="custontemplates" 
							id="fondoFijoSinComprobante.descripcion" 
							cssClass="textarea"
							cssStyle="width:160px" 
							name="fondoFijoSinComprobante.descripcion" 
							title="Descripcion" />						      			
				</td>				    		
				<td class="textoCampo">Importe: </td>
				<td  class="textoDato" >
				<@s.textfield									
					templateDir="custontemplates"
					template="textMoney"  
					id="fondoFijoSinComprobante.importe" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="fondoFijoSinComprobante.importe" 
					title="Importe" />
											      			
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Rubro:</td>
      			<td class="textoDato">
				<@s.property default="&nbsp;" escape=false value="fondoFijoSinComprobante.rubro.descripcion"/>
					<@s.a templateDir="custontemplates" id="seleccionarRubro" name="seleccionarRubro" href="javascript://nop/" cssClass="ocultarIcono">
						<img src="${request.contextPath}/common/images/buscar.gif" border="0" align="absmiddle" hspace="3" >
					</@s.a>		
				</td>
				
	  			<td class="textoCampo">Cuenta Contable:</td>
      			<td class="textoDato">
					<@s.select 
						templateDir="custontemplates" 
						id="fondoFijoSinComprobante.cuentaContable.codigo" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="fondoFijoSinComprobante.cuentaContable.codigo" 
						list="cuentaContableList" 
						listKey="codigo" 
						listValue="descripcion" 
						value="fondoFijoSinComprobante.cuentaContable.codigo"
						title="Cuenta Contable"
						headerKey="0" 
                        headerValue="Seleccionar" />
			</td>	
		</tr>
		<tr><td colspan="4" class="textoCampo">&nbsp;</td></tr>	
	</table>	
	</div>					
		