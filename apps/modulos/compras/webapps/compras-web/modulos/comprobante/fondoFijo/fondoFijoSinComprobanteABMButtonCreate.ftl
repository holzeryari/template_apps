<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancel" type="button" name="btnCancelar" value="Cancelar" class="boton"/>
				<input id="store" type="button" name="btnAgregar" value="Agregar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/comprobante/fondoFijo/createSinComprobante.action" 
  source="store" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="fondoFijoSinComprobante.importe={fondoFijoSinComprobante.importe},fondoFijoSinComprobante.rubro.oid={fondoFijoSinComprobante.rubro.oid},fondoFijoSinComprobante.cuentaContable.codigo={fondoFijoSinComprobante.cuentaContable.codigo},fondoFijoSinComprobante.descripcion={fondoFijoSinComprobante.descripcion},fondoFijoSinComprobante.fondoFijo.oid={fondoFijoSinComprobante.fondoFijo.oid}"/>
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=${navegacionIdBack},flowControl=back"/>
<@vc.htmlContent 
  baseUrl="${request.contextPath}/comprobante/fondoFijo/selectRubro.action" 
  source="seleccionarRubro" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId={navigationId},flowControl=change,fondoFijoSinComprobante.importe={fondoFijoSinComprobante.importe},fondoFijoSinComprobante.rubro.oid={fondoFijoSinComprobante.rubro.oid},fondoFijoSinComprobante.cuentaContable.codigo={fondoFijoSinComprobante.cuentaContable.codigo},fondoFijoSinComprobante.descripcion={fondoFijoSinComprobante.descripcion},fondoFijoSinComprobante.fondoFijo.oid={fondoFijoSinComprobante.fondoFijo.oid},navegacionIdBack={navegacionIdBack}"/>
  