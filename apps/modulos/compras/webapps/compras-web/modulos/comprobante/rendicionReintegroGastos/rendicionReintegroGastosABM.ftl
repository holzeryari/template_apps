<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign tiles=JspTaglibs["/WEB-INF/tlds/tiles-jsp.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<@s.hidden id="navigationId" name="navigationId"/>
<@s.hidden id="navegacionIdBack" name="navegacionIdBack"/>

<@s.hidden id="rendicionReintegroGastos.oid" name="rendicionReintegroGastos.oid"/>
<@s.hidden id="rendicionReintegroGastos.versionNumber" name="rendicionReintegroGastos.versionNumber"/>

<@s.hidden id="rendicionReintegroGastos.cliente.oid" name="rendicionReintegroGastos.cliente.oid"/>
<@s.hidden id="rendicionReintegroGastos.cliente.descripcion" name="rendicionReintegroGastos.cliente.descripcion"/>


<@s.hidden id="rendicionReintegroGastos.detallePersona.pk.secuencia" name="rendicionReintegroGastos.detallePersona.pk.secuencia"/>
<@s.hidden id="rendicionReintegroGastos.detallePersona.pk.identificador" name="rendicionReintegroGastos.detallePersona.pk.identificador"/>

<@s.hidden id="rendicionReintegroGastos.detallePersona.razonSocial" name="rendicionReintegroGastos.detallePersona.razonSocial"/>
<@s.hidden id="rendicionReintegroGastos.detallePersona.nombre" name="rendicionReintegroGastos.detallePersona.nombre"/>

<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
			<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos de la Rendici&oacute;n y Reintegro de Gastos </b></td>
				<td>
					<div align="right">
						<@s.a templateDir="custontemplates" id="modificarRendicionReintegroGastos" name="modificarRendicionReintegroGastos" href="javascript://nop/" cssClass="ocultarIcono">
							<b>Modificar</b><img src="${request.contextPath}/common/images/modificar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
						</@s.a>
					</div>
				</td>
			</tr>
		</table>

		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>		
	
			<tr>
      			<td class="textoCampo">N&uacute;mero:</td>
      			<td class="textoDato">
      			<@s.property default="&nbsp;" escape=false value="rendicionReintegroGastos.numero"/></td>
      			<td  class="textoCampo">Fecha Comprobante: </td>
				<td class="textoDato">
				<@s.if test="rendicionReintegroGastos.fechaComprobante != null">
				<#assign fecha = rendicionReintegroGastos.fechaComprobante> 
				${fecha?string("dd/MM/yyyy")}	
				</@s.if>	&nbsp;								
				</td>	      			
			</tr>		     
	    		
	    	<tr>      			      		
				<td class="textoCampo">Cliente:</td>
	      		<td class="textoDato" colspan="3">
				<@s.property default="&nbsp;" escape=false value="rendicionReintegroGastos.cliente.descripcion"/>
					<@s.a templateDir="custontemplates" id="seleccionarCliente" name="seleccionarCliente" href="javascript://nop/" cssClass="ocultarIcono">
					<img src="${request.contextPath}/common/images/buscar.gif" align="top" border="0" hspace="3">
					</@s.a>
				</td>	
			</tr>
			<tr>
				<td class="textoCampo">Persona:</td>
	  			<td class="textoDato">
				<@s.property default="&nbsp;" escape=false value="rendicionReintegroGastos.detallePersona.razonSocial" />
				<@s.property default="&nbsp;" escape=false value="rendicionReintegroGastos.detallePersona.nombre" />
						<@s.a templateDir="custontemplates" id="seleccionarPersona" name="seleccionarPersona" href="javascript://nop/" cssClass="ocultarIcono">
								<img src="${request.contextPath}/common/images/buscar.gif" align="top" border="0" hspace="3">
						</@s.a>
										
					</td>	
				</td>
				<td class="textoCampo">Fecha Rendicion:</td>
	  			<td class="textoDato">
					<@vc.rowCalendar 
					templateDir="custontemplates" 
					id="rendicionReintegroGastos.fechaRendicion" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="rendicionReintegroGastos.fechaRendicion" 
					title="Fecha Rendicion" />
				</td>
			</tr>
			<tr>
			<td class="textoCampo">Importe Total:</td>
	  		<td class="textoDato">
				<@s.if test="rendicionReintegroGastos.totalRendicion != null">
					<#assign totalRendicion = rendicionReintegroGastos.totalRendicion> 
					${totalRendicion?string(",##0.00")}	
				</@s.if>	&nbsp;	
					
			</td>	    		
			<td class="textoCampo">Estado:</td>
	  		<td  class="textoDato">
				<@s.property default="&nbsp;" escape=false value="rendicionReintegroGastos.estado"/></td>
			</td>
		</tr>
		<tr><td colspan="4" class="textoCampo">&nbsp;</td></tr>
	</table>	
</div>	
		
<@vc.htmlContent 
  baseUrl="${request.contextPath}/comprobante/rendicionReintegroGastos/updateView.action" 
  source="modificarRendicionReintegroGastos" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=rendicionReintegroGastos-Update,flowControl=regis,rendicionReintegroGastos.oid={rendicionReintegroGastos.oid},navegacionIdBack=${navigationId}"/>



