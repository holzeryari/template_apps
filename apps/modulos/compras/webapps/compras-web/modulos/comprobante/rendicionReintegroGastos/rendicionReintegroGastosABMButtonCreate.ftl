<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancel" type="button" name="btnCancelar" value="Cancelar" class="boton"/>
				<input id="store" type="button" name="btnAgregar" value="Agregar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/comprobante/rendicionReintegroGastos/create.action" 
  source="store" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="rendicionReintegroGastos.fechaRendicion={rendicionReintegroGastos.fechaRendicion},rendicionReintegroGastos.cliente.oid={rendicionReintegroGastos.cliente.oid},rendicionReintegroGastos.detallePersona.pk.secuencia={rendicionReintegroGastos.detallePersona.pk.secuencia},rendicionReintegroGastos.detallePersona.pk.identificador={rendicionReintegroGastos.detallePersona.pk.identificador}"/>
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=buscar-rendicionReintegroGastos,flowControl=back"/>
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/comprobante/rendicionReintegroGastos/selectClienteABM.action" 
  source="seleccionarCliente" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=change,rendicionReintegroGastos.fechaRendicion={rendicionReintegroGastos.fechaRendicion},rendicionReintegroGastos.cliente.oid={rendicionReintegroGastos.cliente.oid},rendicionReintegroGastos.detallePersona.pk.secuencia={rendicionReintegroGastos.detallePersona.pk.secuencia},rendicionReintegroGastos.detallePersona.pk.identificador={rendicionReintegroGastos.detallePersona.pk.identificador}"/>


<@vc.htmlContent 
  baseUrl="${request.contextPath}/comprobante/rendicionReintegroGastos/selectPersona.action" 
  source="seleccionarPersona" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=change,rendicionReintegroGastos.fechaRendicion={rendicionReintegroGastos.fechaRendicion},rendicionReintegroGastos.cliente.oid={rendicionReintegroGastos.cliente.oid},rendicionReintegroGastos.detallePersona.pk.secuencia={rendicionReintegroGastos.detallePersona.pk.secuencia},rendicionReintegroGastos.detallePersona.pk.identificador={rendicionReintegroGastos.detallePersona.pk.identificador}"/>

  