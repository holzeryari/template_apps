<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancelar" type="button" name="btnCancelar" value="Cancelar" class="boton"/>
				<input id="update" type="button" name="btnModificar" value="Modificar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/comprobante/rendicionReintegroGastos/update.action" 
  source="update" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="rendicionReintegroGastos.oid={rendicionReintegroGastos.oid},rendicionReintegroGastos.fechaRendicion={rendicionReintegroGastos.fechaRendicion}"/>
  

<@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancelar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=rendicionReintegroGastos-administrar,flowControl=back"/>
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/comprobante/rendicionReintegroGastos/selectPersona.action" 
  source="seleccionarPersona" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=change,rendicionReintegroGastos.oid={rendicionReintegroGastos.oid},rendicionReintegroGastos.fechaRendicion={rendicionReintegroGastos.fechaRendicion},rendicionReintegroGastos.detallePersona.pk.secuencia={rendicionReintegroGastos.detallePersona.pk.secuencia},rendicionReintegroGastos.detallePersona.pk.identificador={rendicionReintegroGastos.detallePersona.pk.identificador},rendicionReintegroGastos.detallePersona.razonSocial={rendicionReintegroGastos.detallePersona.razonSocial},rendicionReintegroGastos.detallePersona.nombre={rendicionReintegroGastos.detallePersona.nombre}"/>


  
  