<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign tiles=JspTaglibs["/WEB-INF/tlds/tiles-jsp.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<@s.hidden id="navigationId" name="navigationId"/>
<@s.hidden id="navegacionIdBack" name="navegacionIdBack"/>

<@s.hidden id="rendicionReintegroGastosFactura.oid" name="rendicionReintegroGastosFactura.oid"/>
<@s.hidden id="rendicionReintegroGastosFactura.versionNumber" name="rendicionReintegroGastosFactura.versionNumber"/>

<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
			<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos de la Rendici&oacute;n y Reintegro de Gastos </b></td>
			</tr>
		</table>

		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>		
			<tr>
      			<td class="textoCampo">N&uacute;mero:</td>
      			<td class="textoDato">
      			<@s.property default="&nbsp;" escape=false value="rendicionReintegroGastosFactura.rendicionReintegroGastos.numero"/></td>
      			<td  class="textoCampo">Fecha Comprobante: </td>
				<td class="textoDato">
				<@s.if test="rendicionReintegroGastosFactura.rendicionReintegroGastos.fechaComprobante != null">
				<#assign fechaComprobante = rendicionReintegroGastosFactura.rendicionReintegroGastos.fechaComprobante> 
				${fechaComprobante?string("dd/MM/yyyy")}	
				</@s.if>	&nbsp;							
				</td>	      			
			</tr>		     
	    		
    		<tr>      			      		
				<td class="textoCampo">Cliente:</td>
      			<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="rendicionReintegroGastosFactura.rendicionReintegroGastos.cliente.descripcion"/>		
				</td>	
								
				<td class="textoCampo">Fecha Rendicion:</td>
      			<td class="textoDato">

				<@s.if test="rendicionReintegroGastosFactura.rendicionReintegroGastos.fechaRendicion != null">
				<#assign fechaRendicion = rendicionReintegroGastosFactura.rendicionReintegroGastos.fechaRendicion> 
				${fechaRendicion?string("dd/MM/yyyy")}	
				</@s.if>	&nbsp;	
	
				</td>
    		</tr>

			<tr>
				<td class="textoCampo">Fecha Per&iacute;odo Desde:</td>
      			<td class="textoDato">

				<@s.if test="rendicionReintegroGastosFactura.rendicionReintegroGastos.fechaDesde != null">
				<#assign fechaDesde = rendicionReintegroGastosFactura.rendicionReintegroGastos.fechaDesde> 
				${fechaDesde?string("dd/MM/yyyy")}	
				</@s.if>	&nbsp;	
	
				</td>
				<td class="textoCampo">Fecha Per&iacute;odo Hasta:</td>
      			<td class="textoDato">
				
	
				<@s.if test="rendicionReintegroGastosFactura.rendicionReintegroGastos.fechaHasta != null">
				<#assign fechaHasta = rendicionReintegroGastosFactura.rendicionReintegroGastos.fechaHasta> 
				${fechaHasta?string("dd/MM/yyyy")}	
				</@s.if>	&nbsp;	
				</td>
			</tr>	
			<tr>
				<td class="textoCampo">Estado:</td>
      			<td  class="textoDato" colspan="3">
					<@s.property default="&nbsp;" escape=false value="rendicionReintegroGastosFactura.rendicionReintegroGastos.estado"/>
				</td>
			</tr>
		
	    	<tr><td colspan="4" class="textoCampo">&nbsp;</td></tr>
		</table>
		
	</div>
		
	<@tiles.insertAttribute name="factura"/>			
		
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Medio de Pago de la Factura</b></td>
			</tr>
		</table>

		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>					
	
			<tr>
				<td class="textoCampo">Medio de Pago:</td>
	  			<td class="textoDato">						
				<@s.select 
							templateDir="custontemplates" 
							id="rendicionReintegroGastosFactura.medioPago" 
							cssClass="textarea"
							cssStyle="width:165px" 
							name="rendicionReintegroGastosFactura.medioPago" 
							list="medioPagoList" 
							listKey="key" 
							listValue="description" 
							value="rendicionReintegroGastosFactura.medioPago.ordinal()"
							title="Medio Pago"
							headerKey="0"
							headerValue="Seleccionar"							
							/>
				</td
				<td class="textoCampo">&nbsp;</td>
				<td class="textoDato">&nbsp;</td>
			</tr>
			<tr><td colspan="4" class="textoCampo">&nbsp;</td></tr>	
		</table>	
	</div>					
		


