
<#setting locale="es_AR">

<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>

<@s.hidden id="navigationId" name="navigationId"/>
<@s.hidden id="rendicionReintegroGastos.cliente.oid" name="rendicionReintegroGastos.cliente.oid"/>
<@s.hidden id="rendicionReintegroGastos.cliente.descripcion" name="rendicionReintegroGastos.cliente.descripcion"/>

<@s.hidden id="rendicionReintegroGastos.detallePersona.pk.secuencia" name="rendicionReintegroGastos.detallePersona.pk.secuencia"/>
<@s.hidden id="rendicionReintegroGastos.detallePersona.pk.identificador" name="rendicionReintegroGastos.detallePersona.pk.identificador"/>

<@s.hidden id="rendicionReintegroGastos.detallePersona.razonSocial" name="rendicionReintegroGastos.detallePersona.razonSocial"/>
<@s.hidden id="rendicionReintegroGastos.detallePersona.nombre" name="rendicionReintegroGastos.detallePersona.nombre"/>

<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
        	<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
			<tr>
				<td><b>Datos de la Rendici&oacute;n y Reintegro de Gastos</b></td>
				<td>
					<div align="right">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0721" 
							enabled="rendicionReintegroGastos.readable" 
							cssClass="item" 
							id="crear"										
							href="javascript://nop/">
							<b>Agregar</b>										
							<img src="${request.contextPath}/common/images/agregar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
						</@security.a>						
					</div>
				</td>
			</tr>
		</table>

		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
	    		<td class="textoCampo" colspan="4">&nbsp;</td>
	    	</tr>	
			<tr>
				<td class="textoCampo">N&uacute;mero:</td>
      			<td class="textoDato">
      			<@s.textfield 
      					templateDir="custontemplates" 
						id="rendicionReintegroGastos.numero" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="rendicionReintegroGastos.numero" 
						title="N&uacute;mero" />
				</td>				

               	<td class="textoCampo">Cliente:</td>
      			<td class="textoDato">	      			
      			<@s.property default="&nbsp;" escape=false value="rendicionReintegroGastos.cliente.descripcion"/>	      										
					<@s.a templateDir="custontemplates" id="seleccionarCliente" name="seleccionarCliente" href="javascript://nop/">
						<img src="${request.contextPath}/common/images/buscar.gif" border="0" align="absmiddle" hspace="3" >
					</@s.a>		
					</td>	
			</tr>
			<tr>
				<td class="textoCampo">Persona:</td>
      			<td class="textoDato">
				<@s.property default="&nbsp;" escape=false value="rendicionReintegroGastos.detallePersona.razonSocial" />
				<@s.property default="&nbsp;" escape=false value="rendicionReintegroGastos.detallePersona.nombre" />
						<@s.a templateDir="custontemplates" id="seleccionarPersona" name="seleccionarPersona" href="javascript://nop/" cssClass="ocultarIcono">
								<img src="${request.contextPath}/common/images/buscar.gif" align="top" border="0" hspace="3">
						</@s.a>
										
					</td>	
				</td>
				<td class="textoCampo">Estado:</td>
      			<td class="textoDato">
      			<@s.select 
							templateDir="custontemplates" 
							id="rendicionReintegroGastos.estado" 
							cssClass="textarea"
							cssStyle="width:165px" 
							name="rendicionReintegroGastos.estado" 
							list="estadoList" 
							listKey="key" 
							listValue="description" 
							value="rendicionReintegroGastos.estado.ordinal()"
							title="Estado"
							headerKey="0"
							headerValue="Todos"							
							/>
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Fecha Rendicion Desde:</td>
      			<td class="textoDato">
					<@vc.rowCalendar 
					templateDir="custontemplates" 
					id="fechaDesde" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="fechaDesde" 
					title="Fecha Desde" />
				</td>
				<td class="textoCampo">Fecha Rendicion Hasta:</td>
      			<td class="textoDato">
				<@vc.rowCalendar 
					templateDir="custontemplates" 
					id="fechaHasta" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="fechaHasta" 
					title="Fecha Hasta" />
				</td>
				
			</tr>
			
			<tr>
    			<td colspan="4" class="lineaGris" align="right">
      			<input id="limpiar" type="button" name="btnVolver" value="Limpiar" class="boton"/>		
      			<input id="buscar" type="button" name="btnVolver" value="Buscar" class="boton"/>
    			</td>
			</tr>	
		</table>
	</div>	
	 <!-- Resultado Filtro -->
	<@s.if test="rendicionReintegroGastosList!=null">
			<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
				<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>Rendiciones y Reintegros de Gastos encontrados</td>			
					
				</tr>
			</table>		

			<@vc.anchors target="contentTrx">			
          		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="rendicionReintegroGastosList" id="rendicionReintegroGastos" pagesize=15  defaultsort=2 partialList=true size="recordSize" keepStatus=true excludedParams="resetFlag">
  		
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select" class="botoneraAnchoCon3" title="Acciones">
						<div class="alineacion">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0723" 
							enabled="rendicionReintegroGastos.readable" 
							cssClass="item" 
							href="${request.contextPath}/comprobante/rendicionReintegroGastos/readView.action?rendicionReintegroGastos.oid=${rendicionReintegroGastos.oid?c}&navegacionIdBack=${navigationId}&navigationId=rendicionReintegroGastos-visualizar&flowControl=regis">
							<img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver"  border="0">
						</@security.a>
						</div>
						<div class="alineacion">	
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0724" 
							enabled="rendicionReintegroGastos.updatable"
							cssClass="item"  
							href="${request.contextPath}/comprobante/rendicionReintegroGastos/administrarView.action?rendicionReintegroGastos.oid=${rendicionReintegroGastos.oid?c}&navigationId=rendicionReintegroGastos-administrar&flowControl=regis">
							<img  src="${request.contextPath}/common/images/administrar.gif" alt="Administrar" title="Administrar"  border="0">
						</@security.a>
						</div>
						<div class="alineacion">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0725" 
							enabled="rendicionReintegroGastos.eraseable"
							cssClass="item"  
							href="${request.contextPath}/comprobante/rendicionReintegroGastos/deleteView.action?rendicionReintegroGastos.oid=${rendicionReintegroGastos.oid?c}&navigationId=rendicionReintegroGastos-eliminar&flowControl=regis">
							<img  src="${request.contextPath}/common/images/eliminar.gif" alt="Eliminar" title="Eliminar"  border="0">
						</@security.a>				
						</div>		
						
					</@display.column>


					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="numero" title="Nro." />				
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="fechaRendicion" format="{0,date,dd/MM/yyyy}" title="Fecha Rendicion" />
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="cliente.descripcion" title="Cliente" />
					
						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" title="Persona">  
						${rendicionReintegroGastos.detallePersona.razonSocial}
						<#if rendicionReintegroGastos.detallePersona.nombre?exists>
						 ${rendicionReintegroGastos.detallePersona.nombre}
						 </#if>
					</@display.column>									
																				
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="estado" title="Estado" />		

				</@display.table>
			</@vc.anchors>
		</div>	
	</@s.if>
			
		

<@vc.htmlContent 
  baseUrl="${request.contextPath}/comprobante/rendicionReintegroGastos/search.action"
  source="buscar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="resetFlag=true,navigationId={navigationId},flowControl=regis,rendicionReintegroGastos.numero={rendicionReintegroGastos.numero},rendicionReintegroGastos.estado={rendicionReintegroGastos.estado},rendicionReintegroGastos.cliente.oid={rendicionReintegroGastos.cliente.oid},rendicionReintegroGastos.detallePersona.pk.identificador={rendicionReintegroGastos.detallePersona.pk.identificador},rendicionReintegroGastos.detallePersona.pk.secuencia={rendicionReintegroGastos.detallePersona.pk.secuencia},fechaDesde={fechaDesde},fechaHasta={fechaHasta},rendicionReintegroGastos.cliente.descripcion={rendicionReintegroGastos.cliente.descripcion},rendicionReintegroGastos.detallePersona.razonSocial={rendicionReintegroGastos.detallePersona.razonSocial},rendicionReintegroGastos.detallePersona.nombre={rendicionReintegroGastos.detallePersona.nombre}"/>
  		      

<@vc.htmlContent 
  baseUrl="${request.contextPath}/comprobante/rendicionReintegroGastos/view.action"
  source="limpiar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=regis"/>
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/comprobante/rendicionReintegroGastos/createView.action" 
  source="crear" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=rendicionReintegroGastos-create,flowControl=regis"/>
  
  
  <@vc.htmlContent 
  baseUrl="${request.contextPath}/comprobante/rendicionReintegroGastos/selectClienteSearch.action" 
  source="seleccionarCliente" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=change,rendicionReintegroGastos.numero={rendicionReintegroGastos.numero},rendicionReintegroGastos.estado={rendicionReintegroGastos.estado},rendicionReintegroGastos.cliente.oid={rendicionReintegroGastos.cliente.oid},rendicionReintegroGastos.detallePersona.pk.identificador={rendicionReintegroGastos.detallePersona.pk.identificador},rendicionReintegroGastos.detallePersona.pk.secuencia={rendicionReintegroGastos.detallePersona.pk.secuencia},fechaDesde={fechaDesde},fechaHasta={fechaHasta},rendicionReintegroGastos.cliente.descripcion={rendicionReintegroGastos.cliente.descripcion},rendicionReintegroGastos.detallePersona.razonSocial={rendicionReintegroGastos.detallePersona.razonSocial},rendicionReintegroGastos.detallePersona.nombre={rendicionReintegroGastos.detallePersona.nombre}"/>
  
  
  
  <@vc.htmlContent 
  baseUrl="${request.contextPath}/comprobante/rendicionReintegroGastos/selectPersona.action" 
  source="seleccionarPersona" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=change,rendicionReintegroGastos.numero={rendicionReintegroGastos.numero},rendicionReintegroGastos.estado={rendicionReintegroGastos.estado},rendicionReintegroGastos.cliente.oid={rendicionReintegroGastos.cliente.oid},rendicionReintegroGastos.detallePersona.pk.identificador={rendicionReintegroGastos.detallePersona.pk.identificador},rendicionReintegroGastos.detallePersona.pk.secuencia={rendicionReintegroGastos.detallePersona.pk.secuencia},fechaDesde={fechaDesde},fechaHasta={fechaHasta},rendicionReintegroGastos.cliente.descripcion={rendicionReintegroGastos.cliente.descripcion},rendicionReintegroGastos.detallePersona.razonSocial={rendicionReintegroGastos.detallePersona.razonSocial},rendicionReintegroGastos.detallePersona.nombre={rendicionReintegroGastos.detallePersona.nombre}"/>


  
  
