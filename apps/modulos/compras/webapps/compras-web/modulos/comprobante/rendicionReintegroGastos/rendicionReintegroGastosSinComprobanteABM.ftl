<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign tiles=JspTaglibs["/WEB-INF/tlds/tiles-jsp.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<@s.hidden id="navigationId" name="navigationId"/>
<@s.hidden id="navegacionIdBack" name="navegacionIdBack"/>

<@s.hidden id="rendicionReintegroGastosSinComprobante.oid" name="rendicionReintegroGastosSinComprobante.oid"/>
<@s.hidden id="rendicionReintegroGastosSinComprobante.versionNumber" name="rendicionReintegroGastosSinComprobante.versionNumber"/>
<@s.hidden id="rendicionReintegroGastosSinComprobante.rendicionReintegroGastos.oid" name="rendicionReintegroGastosSinComprobante.rendicionReintegroGastos.oid"/>


<@s.hidden id="rendicionReintegroGastosSinComprobante.rubro.oid" name="rendicionReintegroGastosSinComprobante.rubro.oid"/>
<@s.hidden id="rendicionReintegroGastosSinComprobante.rubro.descripcion" name="rendicionReintegroGastosSinComprobante.rubro.descripcion"/>

<#if cuentaContableList.size()==1 ||  cuentaContableList.size()==0>
<@s.hidden id="rendicionReintegroGastosSinComprobante.cuentaContable.codigo" name="rendicionReintegroGastosSinComprobante.cuentaContable.codigo"/>
</#if>

<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
			<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos de la Rendici&oacute;n y Reintegro de Gastos</b></td>
			</tr>
		</table>

		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>	
			<tr>
	  			<td class="textoCampo">N&uacute;mero:</td>
	  			<td class="textoDato">
	  			<@s.property default="&nbsp;" escape=false value="rendicionReintegroGastosSinComprobante.rendicionReintegroGastos.numero"/></td>
	  			<td  class="textoCampo">Fecha Comprobante: </td>
				<td class="textoDato">
				<@s.if test="rendicionReintegroGastosSinComprobante.rendicionReintegroGastos.fechaComprobante != null">
				<#assign fechaComprobante = rendicionReintegroGastosSinComprobante.rendicionReintegroGastos.fechaComprobante> 
				${fechaComprobante?string("dd/MM/yyyy")}	
				</@s.if>	&nbsp;							
				</td>	      			
			</tr>		     
	    	<tr>      			      		
				<td class="textoCampo">Cliente:</td>
	      		<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="rendicionReintegroGastosSinComprobante.rendicionReintegroGastos.cliente.descripcion"/>		
				</td>	
				</td>
				<td class="textoCampo">Fecha Rendicion:</td>
	      		<td class="textoDato">
					<@s.if test="rendicionReintegroGastosSinComprobante.rendicionReintegroGastos.fechaRendicion != null">
						<#assign fechaRendicion = rendicionReintegroGastosSinComprobante.rendicionReintegroGastos.fechaRendicion> 
						${fechaRendicion?string("dd/MM/yyyy")}	
						</@s.if>	&nbsp;	
				</td>
	    	</tr>

			<tr>
				<td class="textoCampo">Fecha Per&iacute;odo Desde:</td>
      			<td class="textoDato">

				<@s.if test="rendicionReintegroGastosSinComprobante.rendicionReintegroGastos.fechaDesde != null">
				<#assign fechaDesde = rendicionReintegroGastosSinComprobante.rendicionReintegroGastos.fechaDesde> 
				${fechaDesde?string("dd/MM/yyyy")}	
				</@s.if>	&nbsp;	
	
				</td>
				<td class="textoCampo">Fecha Per&iacute;odo Hasta:</td>
      			<td class="textoDato">
				<@s.if test="rendicionReintegroGastosSinComprobante.rendicionReintegroGastos.fechaHasta != null">
				<#assign fechaHasta = rendicionReintegroGastosSinComprobante.rendicionReintegroGastos.fechaHasta> 
				${fechaHasta?string("dd/MM/yyyy")}	
				</@s.if>	&nbsp;	
				</td>
			</tr>		
			<tr>
				<td class="textoCampo">Estado:</td>
      			<td  class="textoDato" colspan="3">
					<@s.property default="&nbsp;" escape=false value="rendicionReintegroGastosSinComprobante.rendicionReintegroGastos.estado"/>
				</td>
			</tr>
			<tr><td colspan="4" class="textoCampo">&nbsp;</td></tr>		
		</table>	
	  </div>
	  <div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos de la Rendici&oacute;n y Reintegro de Gastos Sin Comprobante</b></td>
			</tr>
		</table>

		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>	  		
			<tr>
				<td class="textoCampo">Descripci&oacute;n: </td>
				<td  class="textoDato">
				<@s.textfield 
	      					templateDir="custontemplates" 
							id="rendicionReintegroGastosSinComprobante.descripcion" 
							cssClass="textarea"
							cssStyle="width:160px" 
							name="rendicionReintegroGastosSinComprobante.descripcion" 
							title="Descripcion" />						      			
				</td>				    		
				<td class="textoCampo">Importe: </td>
				<td  class="textoDato">
					<@s.textfield									
					templateDir="custontemplates"
					template="textMoney"  
					id="rendicionReintegroGastosSinComprobante.importe" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="rendicionReintegroGastosSinComprobante.importe" 
					title="Importe" />
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Rubro:</td>
      			<td class="textoDato">
				<@s.property default="&nbsp;" escape=false value="rendicionReintegroGastosSinComprobante.rubro.descripcion"/>
					<@s.a templateDir="custontemplates" id="seleccionarRubro" name="seleccionarRubro" href="javascript://nop/" cssClass="ocultarIcono">
						<img src="${request.contextPath}/common/images/buscar.gif" border="0" align="absmiddle" hspace="3" >
					</@s.a>		
				</td>
				<td class="textoCampo">Cuenta Contable:</td>
      			<td class="textoDato">
					<@s.select 
						templateDir="custontemplates" 
						id="rendicionReintegroGastosSinComprobante.cuentaContable.codigo" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="rendicionReintegroGastosSinComprobante.cuentaContable.codigo" 
						list="cuentaContableList" 
						listKey="codigo" 
						listValue="descripcion" 
						value="rendicionReintegroGastosSinComprobante.cuentaContable.codigo"
						title="Cuenta Contable"
						headerKey="0" 
                        headerValue="Seleccionar" />
				</td>	
			</tr>
			<tr>
				<td class="textoCampo">Medio de Pago:</td>
	      		<td class="textoDato" colspan="3">      			
					<@s.select 
						templateDir="custontemplates" 
						id="rendicionReintegroGastosSinComprobante.medioPago" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="rendicionReintegroGastosSinComprobante.medioPago" 
						list="medioPagoList" 
						listKey="key" 
						listValue="description" 
						value="rendicionReintegroGastosSinComprobante.medioPago.ordinal()"
						title="Medio Pago"
						headerKey="0"
						headerValue="Seleccionar"							
						/>
				</td>
			</tr>
			<tr><td colspan="4" class="textoCampo">&nbsp;</td></tr>	
	</table>	
	</div>					
	

