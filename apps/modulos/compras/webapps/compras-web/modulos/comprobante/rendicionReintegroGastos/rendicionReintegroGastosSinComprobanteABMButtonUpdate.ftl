<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancelar" type="button" name="btnCancelar" value="Cancelar" class="boton"/>
				<input id="update" type="button" name="btnModificar" value="Modificar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/comprobante/rendicionReintegroGastos/updateSinComprobante.action" 
  source="update" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="rendicionReintegroGastosSinComprobante.oid={rendicionReintegroGastosSinComprobante.oid},rendicionReintegroGastosSinComprobante.importe={rendicionReintegroGastosSinComprobante.importe},rendicionReintegroGastosSinComprobante.rubro.oid={rendicionReintegroGastosSinComprobante.rubro.oid},rendicionReintegroGastosSinComprobante.cuentaContable.codigo={rendicionReintegroGastosSinComprobante.cuentaContable.codigo},rendicionReintegroGastosSinComprobante.descripcion={rendicionReintegroGastosSinComprobante.descripcion},rendicionReintegroGastosSinComprobante.rendicionReintegroGastos.oid={rendicionReintegroGastosSinComprobante.rendicionReintegroGastos.oid},rendicionReintegroGastosSinComprobante.medioPago={rendicionReintegroGastosSinComprobante.medioPago}"/>
  

<@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancelar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=rendicionReintegroGastos-administrar,flowControl=back"/>
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/comprobante/rendicionReintegroGastos/selectRubro.action" 
  source="seleccionarRubro" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId={navigationId},flowControl=change,rendicionReintegroGastosSinComprobante.oid={rendicionReintegroGastosSinComprobante.oid},rendicionReintegroGastosSinComprobante.importe={rendicionReintegroGastosSinComprobante.importe},rendicionReintegroGastosSinComprobante.rubro.oid={rendicionReintegroGastosSinComprobante.rubro.oid},rendicionReintegroGastosSinComprobante.cuentaContable.codigo={rendicionReintegroGastosSinComprobante.cuentaContable.codigo},rendicionReintegroGastosSinComprobante.descripcion={rendicionReintegroGastosSinComprobante.descripcion},rendicionReintegroGastosSinComprobante.rendicionReintegroGastos.oid={rendicionReintegroGastosSinComprobante.rendicionReintegroGastos.oid},navegacionIdBack={navegacionIdBack}"/>
  