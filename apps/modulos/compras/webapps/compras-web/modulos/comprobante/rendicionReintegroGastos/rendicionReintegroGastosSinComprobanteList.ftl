
<#setting locale="es_AR">

<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>

<@s.hidden id="navigationId" name="navigationId"/>
<@vc.anchors target="contentTrx">	

<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
		<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
			<tr>
				<td>Sin Comprobantes</td>
				<td>	
					<div align="right">
						<@s.a href="${request.contextPath}/comprobante/rendicionReintegroGastos/createSinComprobanteView.action?navigationId=factura-crearSinComprobante&rendicionReintegroGastosSinComprobante.rendicionReintegroGastos.oid=${rendicionReintegroGastos.oid?c}&flowControl=regis&navegacionIdBack=${navigationId}" templateDir="custontemplates" id="agregarRendicionReintegroGastosSinComprobante" name="agregarRendicionReintegroGastosSinComprobante" cssClass="ocultarIcono">
						<b>Agregar</b><img src="${request.contextPath}/common/images/agregar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
						</@s.a>		
					</div>
				</td>
			</tr>
		</table>
			
		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="rendicionReintegroGastos.rendicionReintegroSinComprobanteList" id="rendicionReintegroGastosSinComprobante" defaultsort=2 >
        	<@display.column headerClass="tbl-contract-service-select" class="botoneraAnchoCon3" title="Acciones">
						<div class="alineacion">
						<@s.a 
							templateDir="custontemplates"
							cssClass="item" 
							id="ver"
							name="ver"
							href="${request.contextPath}/comprobante/rendicionReintegroGastos/readSinComprobanteView.action?rendicionReintegroGastosSinComprobante.oid=${rendicionReintegroGastosSinComprobante.oid?c}&navigationId=rendicionReintegroGastosSinComprobante-visualizar&flowControl=regis&navegacionIdBack=${navigationId}">
							<img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver"  border="0">
						</@s.a>
						</div>
						<div class="alineacion">	
						<@s.a 
							templateDir="custontemplates"
							cssClass="item"
							id="admin"
							name="admin"  
								href="${request.contextPath}/comprobante/rendicionReintegroGastos/updateSinComprobanteView.action?rendicionReintegroGastosSinComprobante.oid=${rendicionReintegroGastosSinComprobante.oid?c}&navigationId=rendicionReintegroGastosSinComprobante-modificar&flowControl=regis&navegacionIdBack=${navigationId}">
							<img  src="${request.contextPath}/common/images/modificar.gif" alt="Modificar" title="Modificar"  border="0">
						</@s.a>
						</div>
						<div class="alineacion">
						<@s.a 
							templateDir="custontemplates"
							cssClass="item"
							id="admin"
							name="admin"  
								href="${request.contextPath}/comprobante/rendicionReintegroGastos/deleteSinComprobanteView.action?rendicionReintegroGastosSinComprobante.oid=${rendicionReintegroGastosSinComprobante.oid?c}&navigationId=rendicionReintegroGastosSinComprobante-modificar&flowControl=regis&navegacionIdBack=${navigationId}">
							<img  src="${request.contextPath}/common/images/eliminar.gif" alt="Eliminar" title="Eliminar"  border="0">
						</@s.a>				
						</div>
				
					</@display.column>
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" title="Descripcion" property="descripcion"/>
									

					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" title="Rubro" property="rubro.descripcion"/>
										
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="cuentaContable.descripcion"  title="Cuenta Contable" />
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="medioPago" title="Medio Pago" />
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" title="Importe" property="importe"/>
									
				</@display.table>
			</div>	
		</@vc.anchors>
	