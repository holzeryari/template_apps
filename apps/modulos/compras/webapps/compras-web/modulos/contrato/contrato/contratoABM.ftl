<#setting locale="es_AR">

<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<@s.hidden id="webFlow" name="webFlow"/>
<@s.hidden id="viewState" name="viewState"/>

<@s.hidden id="contrato.oid" name="contrato.oid"/>
<@s.hidden id="contrato.estado" name="contrato.estado"/>
<@s.hidden id="contrato.versionNumber" name="contrato.versionNumber"/>
<@s.hidden id="contrato.cliente.oid" name="contrato.cliente.oid"/>
<@s.hidden id="contrato.cliente.descripcion" name="contrato.cliente.descripcion"/>
<@s.hidden id="contrato.rubro.oid" name="contrato.rubro.oid"/>
<@s.hidden id="contrato.rubro.descripcion" name="contrato.rubro.descripcion"/>
<@s.hidden id="contrato.cartel.oid" name="contrato.cartel.oid"/>
<@s.hidden id="contrato.cartel.descripcion" name="contrato.cartel.descripcion"/>
<@s.hidden id="contrato.inmueble.oid" name="contrato.inmueble.oid"/>
<@s.hidden id="contrato.inmueble.descripcion" name="contrato.inmueble.descripcion"/>
<@s.hidden id="contrato.contactoRU.pk.identificador" name="contrato.contactoRU.pk.identificador"/>
<@s.hidden id="contrato.contactoRU.pk.secuencia" name="contrato.contactoRU.pk.secuencia"/>
<@s.hidden id="contrato.contactoRU.razonSocial" name="contrato.contactoRU.razonSocial" />
<@s.hidden id="contrato.contactoRU.nombre" name="contrato.contactoRU.nombre" />
<@s.hidden id="contrato.proveedor.nroProveedor" name="contrato.proveedor.nroProveedor"/>
<@s.hidden id="contrato.proveedor.detalleDePersona.razonSocial" name="contrato.proveedor.detalleDePersona.razonSocial" />
<@s.hidden id="contrato.proveedor.detalleDePersona.nombre" name="contrato.proveedor.detalleDePersona.nombre" />
<@s.hidden id="contrato.personaSecuencia.pk.identificador" name="contrato.personaSecuencia.pk.identificador"/>
<@s.hidden id="contrato.personaSecuencia.pk.secuencia" name="contrato.personaSecuencia.pk.secuencia"/>
<@s.hidden id="contrato.personaSecuencia.razonSocial" name="contrato.personaSecuencia.razonSocial" />
<@s.hidden id="contrato.personaSecuencia.nombre" name="contrato.personaSecuencia.nombre" />

<#if cuentaContableList.size()==1>
<@s.hidden id="cuentaContable" name="contrato.cuentaContable.codigo" />
</#if>

<@s.if test="contrato.tipoContrato == null || contrato.tipoContrato.ordinal() == 0"> <#-- No Aplica -->
	<#assign bloqueAlquilerStyle="visibility:hidden; position:absolute;">
	<#assign bloquePublicidadStyle="visibility:hidden; position:absolute;">
	<#assign bloqueDonacionStyle="visibility:hidden; position:absolute;">
</@s.if>
<@s.if test="contrato.tipoContrato.ordinal() == 1"> <#-- Alquiler -->
	<#assign bloqueAlquilerStyle="visibility:visible; position:relative;">
	<#assign bloquePublicidadStyle="visibility:hidden; position:absolute;">
	<#assign bloqueDonacionStyle="visibility:hidden; position:absolute;">
	<#assign bloqueAlquilerLabelOtroStyle="visibility:visible; position:relative;">
	<#assign bloqueAlquilerDescripcionOtroStyle="visibility:visible; position:relative;">
</@s.if>
<@s.if test="contrato.tipoContrato.ordinal() == 2"> <#-- Donacion -->
	<#assign bloqueAlquilerStyle="visibility:hidden; position:absolute;">
	<#assign bloquePublicidadStyle="visibility:hidden; position:absolute;">
	<#assign bloqueDonacionStyle="visibility:visible; position:relative;">
</@s.if>
<@s.if test="contrato.tipoContrato.ordinal() == 3"> <#-- LocacionDeObra -->
	<#assign bloqueAlquilerStyle="visibility:hidden; position:absolute;">
	<#assign bloquePublicidadStyle="visibility:hidden; position:absolute;">
	<#assign bloqueDonacionStyle="visibility:hidden; position:absolute;">
	<#assign bloqueAlquilerLabelOtroStyle="visibility:hidden; position:absolute;">
	<#assign bloqueAlquilerDescripcionOtroStyle="visibility:hidden; position:absolute;">
</@s.if>
<@s.if test="contrato.tipoContrato.ordinal() == 4"> <#-- LocacionDeServicio -->
	<#assign bloqueAlquilerStyle="visibility:hidden; position:absolute;">
	<#assign bloquePublicidadStyle="visibility:hidden; position:absolute;">
	<#assign bloqueDonacionStyle="visibility:hidden; position:absolute;">
	<#assign bloqueAlquilerLabelOtroStyle="visibility:hidden; position:absolute;">
	<#assign bloqueAlquilerDescripcionOtroStyle="visibility:hidden; position:absolute;">
</@s.if>
<@s.if test="contrato.tipoContrato.ordinal() == 5"> <#-- Publicidad -->
	<#assign bloqueAlquilerStyle="visibility:hidden; position:absolute;">
	<#assign bloquePublicidadStyle="visibility:visible; position:relative;">
	<#assign bloqueDonacionStyle="visibility:hidden; position:absolute;">
</@s.if>

<@s.if test="contrato.tipoAlquiler == null || contrato.tipoAlquiler.ordinal() == 0"> <#-- No Aplica -->
	<#assign bloqueAlquilerLabelOtroStyle="visibility:hidden; position:absolute;">
	<#assign bloqueAlquilerDescripcionOtroStyle="visibility:hidden; position:absolute;">
	<#assign labelInmuebleStyle="visibility:hidden; position:absolute;">
	<#assign descripcionInmuebleStyle="visibility:hidden; position:absolute;">
	<#assign labelCartelStyle="visibility:hidden; position:absolute;">
	<#assign descripcionCartelStyle="visibility:hidden; position:absolute;">
</@s.if>
<@s.if test="contrato.tipoAlquiler.ordinal() == 1"> <#-- Cartel -->
	<#assign bloqueAlquilerLabelOtroStyle="visibility:hidden; position:absolute;">
	<#assign bloqueAlquilerDescripcionOtroStyle="visibility:hidden; position:absolute;">
	<#assign labelInmuebleStyle="visibility:hidden; position:absolute;">
	<#assign descripcionInmuebleStyle="visibility:hidden; position:absolute;">
	<#assign labelCartelStyle="visibility:visible; position:relative;">
	<#assign descripcionCartelStyle="visibility:visible; position:relative;">
</@s.if>
<@s.if test="contrato.tipoAlquiler.ordinal() == 2"> <#-- Equipo -->
	<#assign bloqueAlquilerLabelOtroStyle="visibility:visible; position:relative;">
	<#assign bloqueAlquilerDescripcionOtroStyle="visibility:visible; position:relative;">
	<#assign labelInmuebleStyle="visibility:hidden; position:absolute;">
	<#assign descripcionInmuebleStyle="visibility:hidden; position:absolute;">
	<#assign labelCartelStyle="visibility:hidden; position:absolute;">
	<#assign descripcionCartelStyle="visibility:hidden; position:absolute;">
</@s.if>
<@s.if test="contrato.tipoAlquiler.ordinal() == 3"> <#-- Inmueble -->
	<#assign bloqueAlquilerLabelOtroStyle="visibility:hidden; position:absolute;">
	<#assign bloqueAlquilerDescripcionOtroStyle="visibility:hidden; position:absolute;">
	<#assign labelInmuebleStyle="visibility:visible; position:relative;">
	<#assign descripcionInmuebleStyle="visibility:visible; position:relative;">
	<#assign labelCartelStyle="visibility:hidden; position:absolute;">
	<#assign descripcionCartelStyle="visibility:hidden; position:absolute;">
</@s.if>

<@s.if test="(contrato.tipoContrato != null && contrato.tipoContrato.ordinal() == 1) && (contrato.tipoAlquiler == null || contrato.tipoAlquiler.ordinal() == 0)">
	<#assign bloqueAlquilerLabelOtroStyle="visibility:visible; position:relative;">
	<#assign bloqueAlquilerDescripcionOtroStyle="visibility:visible; position:relative;">
</@s.if>

<@s.if test="contrato.tipoBeneficiario == null || contrato.tipoBeneficiario.ordinal() == 0"> <#-- No Aplica -->
	<#assign bloqueBeneficiarioLabelOtroStyle="visibility:visible; position:relative;">
	<#assign bloqueBeneficiarioDescripcionOtroStyle="visibility:visible; position:relative;">
	<#assign labelProveedorStyle="visibility:hidden; position:absolute;">
	<#assign descripcionProveedorStyle="visibility:hidden; position:absolute;">
	<#assign labelPersonaStyle="visibility:hidden; position:absolute;">
	<#assign descripcionPersonaStyle="visibility:hidden; position:absolute;">
	<#assign bloqueBeneficiarioCategoriaBeneficiarioStyle="visibility:hidden; position:absolute;">
</@s.if>
<@s.if test="contrato.tipoBeneficiario.ordinal() == 1"> <#-- Proveedor -->
	<#assign bloqueBeneficiarioLabelOtroStyle="visibility:hidden; position:absolute;">
	<#assign bloqueBeneficiarioDescripcionOtroStyle="visibility:hidden; position:absolute;">
	<#assign labelProveedorStyle="visibility:visible; position:relative;">
	<#assign descripcionProveedorStyle="visibility:visible; position:relative;">
	<#assign labelPersonaStyle="visibility:hidden; position:absolute;">
	<#assign descripcionPersonaStyle="visibility:hidden; position:absolute;">
	<#assign bloqueBeneficiarioCategoriaBeneficiarioStyle="visibility:visible; position:relative;">
</@s.if>
<@s.if test="contrato.tipoBeneficiario.ordinal() == 2"> <#-- Persona -->
	<#assign bloqueBeneficiarioLabelOtroStyle="visibility:hidden; position:absolute;">
	<#assign bloqueBeneficiarioDescripcionOtroStyle="visibility:hidden; position:absolute;">
	<#assign labelProveedorStyle="visibility:hidden; position:absolute;">
	<#assign descripcionProveedorStyle="visibility:hidden; position:absolute;">
	<#assign labelPersonaStyle="visibility:visible; position:relative;">
	<#assign descripcionPersonaStyle="visibility:visible; position:relative;">
	<#assign bloqueBeneficiarioCategoriaBeneficiarioStyle="visibility:visible; position:relative;">
</@s.if>

<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
			
			<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos del Contrato</b></td>
				<td>
					<div align="right">
						<@s.a templateDir="custontemplates" id="modificarContrato" name="modificarContrato" href="javascript://nop/" cssClass="ocultarIcono">
							<b>Modificar</b><img src="${request.contextPath}/common/images/modificar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
						</@s.a>
					</div>
				</td>
			</tr>
		</table>
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        		
        	</tr>
			<tr>
      			<td class="textoCampo">N&uacute;mero:</td>
      			<td class="textoDato"><@s.property default="&nbsp;" escape=false value="contrato.oid"/></td>
      			<td class="textoCampo">Descripci&oacute;n:</td>
				<td class="textoDato">
					<@s.textfield 
						templateDir="custontemplates" 
						id="descripcion" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="contrato.descripcion" 
						title="Descripci&oacute;n" />					
				</td>	      			
			</tr>	

			<tr>
      			<td class="textoCampo">Tipo documento:</td>
      			<td class="textoDato">
	      			<@s.select 
						templateDir="custontemplates" 
						id="tipoDocumento" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="contrato.tipoDocumento" 
						list="tipoDocumentoList" 
						listKey="key" 
						listValue="description" 
						value="contrato.tipoDocumento.ordinal()"
						title="Tipo de documento"
						headerKey="0"
						headerValue="Seleccionar"   
						<#-- onchange="javascript:contratoSelect(this);" -->
						/>
	      		</td>
      			<td class="textoCampo">Tipo Contrato:</td>
				<td class="textoDato">
					<@s.select 
						templateDir="custontemplates" 
						id="tipoContrato" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="contrato.tipoContrato" 
						list="tipoContratoList" 
						listKey="key" 
						listValue="description" 
						value="contrato.tipoContrato.ordinal()"
						title="Tipo contrato"
						headerKey="0"
						headerValue="Seleccionar"   
						onchange="javascript:tipoContratoSelect(this);"
						/>
				</td>
    		</tr>

			<#--    ALQUILER    -->

			<tr id="bloqueAlquiler" style="${bloqueAlquilerStyle}">
				<td class="textoCampo">Tipo Alquiler:</td>
      			<td  class="textoDato">
					<@s.select 
						templateDir="custontemplates" 
						id="tipoAlquiler" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="contrato.tipoAlquiler" 
						list="tipoAlquilerList" 
						listKey="key" 
						listValue="description" 
						value="contrato.tipoAlquiler.ordinal()"
						title="Tipo Alquiler"
						headerKey="0"
						headerValue="Seleccionar"   
						onchange="javascript:tipoAlquilerSelect(this);"
						/>
				</td>

				<td class="textoCampo" align="right" id="bloqueAlquiler.labelOtro" style="${bloqueAlquilerLabelOtroStyle}">&nbsp;</td>
				<td class="textoDato" align="left" id="bloqueAlquiler.descripcionOtro" style="${bloqueAlquilerDescripcionOtroStyle}">&nbsp;</td>

				<td class="textoCampo" align="right" id="labelInmueble" style="${labelInmuebleStyle}">Inmueble: </td>
				<td class="textoDato" align="left" id="descripcionInmueble" style="${descripcionInmuebleStyle}">
				<div id="inmuebleTexto" style="float:left;">
					<@s.property default="&nbsp;" escape=false value="contrato.inmueble.descripcion"/>
				</div>
				<@s.a templateDir="custontemplates" id="seleccionarInmueble" name="seleccionarInmueble" href="javascript://nop/" cssClass="ocultarIcono">
					<img src="${request.contextPath}/common/images/buscar.gif" border="0" align="absmiddle" hspace="3" >
				</@s.a>	
			</td>
			<td class="textoCampo" align="right" id="labelCartel" style="${labelCartelStyle}">Cartel: </td>
			<td class="textoDato" align="left" id="descripcionCartel" style="${descripcionCartelStyle}">
				<div id="contratoTexto" style="float:left;">
					<@s.property default="&nbsp;" escape=false value="contrato.cartel.descripcion"/>
				</div>
				<@s.a templateDir="custontemplates" id="seleccionarCarteleria" name="seleccionarCarteleria" href="javascript://nop/" cssClass="ocultarIcono">
					<img src="${request.contextPath}/common/images/buscar.gif" border="0" align="absmiddle" hspace="3" >
				</@s.a>	
			</td>
		</tr>

		<#--    PUBLICIDAD    -->

		<tr id="bloquePublicidad" style="${bloquePublicidadStyle}">
			<td  class="textoCampo">Tipo publicidad: </td>
			<td class="textoDato" colspan="3">
				<@s.select 
					templateDir="custontemplates" 
					id="tipoPublicidad" 
					cssClass="textarea"
					cssStyle="width:165px" 
					name="contrato.tipoPublicidad" 
					list="tipoPublicidadList" 
					listKey="key" 
					listValue="description" 
					value="contrato.tipoPublicidad.ordinal()"
					title="Tipo Publicidad"
					headerKey="0"
					headerValue="Seleccionar"   
					/>
			</td>
		</tr>

		<#--    DONACION    -->

		<tr id="bloqueDonacion" style="${bloqueDonacionStyle}">
			<td class="textoCampo">Tipo donaci&oacute;n: </td>
			<td class="textoDato" colspan="3">
      			<@s.select 
					templateDir="custontemplates" 
					id="tipoDonacion" 
					cssClass="textarea"
					cssStyle="width:165px" 
					name="contrato.tipoDonacion" 
					list="tipoDonacionList" 
					listKey="key" 
					listValue="description" 
					value="contrato.tipoDonacion.ordinal()"
					title="Tipo Donaci&oacute;n"
					headerKey="0"
					headerValue="Seleccionar"			
					/>
			</td>
		</tr>

		<#--    CLIENTE    -->

		<tr>
			<td class="textoCampo">Cliente:</td>
			<td class="textoDato">
				<@s.property default="&nbsp;" escape=false value="contrato.cliente.descripcion"/>
				<@s.a templateDir="custontemplates" id="seleccionarCliente" name="seleccionarCliente" href="javascript://nop/" cssClass="ocultarIcono">
					<img src="${request.contextPath}/common/images/buscar.gif" border="0" align="absmiddle" hspace="3" >
				</@s.a>	
			</td>

  			<td class="textoCampo">Contacto RU:</td>
			<td class="textoDato">
				<@s.property default="&nbsp;" escape=false value="contrato.contactoRU.razonSocial" /><@s.property default="&nbsp;" escape=false value="contrato.contactoRU.nombre" />
				<@s.a templateDir="custontemplates" id="seleccionarContactoRU" name="seleccionarContactoRU" href="javascript://nop/" cssClass="ocultarIcono">
					<img src="${request.contextPath}/common/images/buscar.gif"  border="0" align="absmiddle" hspace="3" >
				</@s.a>	
			</td>
		</tr>

		<#--    BENEFICIARIO    -->

		<tr>
  			<td class="textoCampo">Tipo Beneficiario:</td>
			<td class="textoDato">
		    	<@s.select 
					templateDir="custontemplates" 
					id="tipoBeneficiario" 
					cssClass="textarea"
					cssStyle="width:165px" 
					name="contrato.tipoBeneficiario" 
					list="tipoBeneficiarioList" 
					listKey="key" 
					listValue="description" 
					value="contrato.tipoBeneficiario.ordinal()"
					title="Tipo Beneficiario"
					headerKey="0"
					headerValue="Seleccionar"			
					onchange="javascript:tipoBeneficiarioSelect(this);"
					/>
			</td>

			<td class="textoCampo" align="right" id="bloqueBeneficiario.labelOtro" style="${bloqueBeneficiarioLabelOtroStyle}">&nbsp;</td>
			<td class="textoDato" align="left" id="bloqueBeneficiario.descripcionOtro" style="${bloqueBeneficiarioDescripcionOtroStyle}">&nbsp;</td>

  			<td class="textoCampo" align="right" id="labelProveedor" style="${labelProveedorStyle}">Proveedor:</td>
			<td class="textoDato" align="left" id="descripcionProveedor" style="${descripcionProveedorStyle}">
				<div id="proveedorTexto" style="float:left;">
					<@s.property default="&nbsp;" escape=false value="contrato.proveedor.detalleDePersona.razonSocial" /><@s.property default="&nbsp;" escape=false value="contrato.proveedor.detalleDePersona.nombre" />
				</div>
				<@s.a templateDir="custontemplates" id="seleccionarProveedor" name="seleccionarProveedor" href="javascript://nop/" cssClass="ocultarIcono">
					<img src="${request.contextPath}/common/images/buscar.gif" border="0" align="absmiddle" hspace="3" >
				</@s.a>	
			</td>
			<td class="textoCampo" id="labelPersona" style="${labelPersonaStyle}">Persona:</td>
			<td class="textoDato" id="descripcionPersona" style="${descripcionPersonaStyle}">
				<div id="personaTexto" style="float:left;">
					<@s.property default="&nbsp;" escape=false value="contrato.personaSecuencia.razonSocial" /><@s.property default="&nbsp;" escape=false value="contrato.personaSecuencia.nombre" />
				</div>
				<@s.a templateDir="custontemplates" id="seleccionarPersonaSecuencia" name="seleccionarPersonaSecuencia" href="javascript://nop/" cssClass="ocultarIcono">
					<img src="${request.contextPath}/common/images/buscar.gif" border="0" align="absmiddle" hspace="3" >
				</@s.a>	
			</td>
		</tr>
		<tr id="bloqueBeneficiario.categoriaBeneficiario" style="${bloqueBeneficiarioCategoriaBeneficiarioStyle}">
			<td class="textoCampo">Categor&iacute;a de Beneficiario:</td>
			<td class="textoDato" colspan="3">
		    	<@s.select 
					templateDir="custontemplates" 
					id="categoriaBeneficiario" 
					cssClass="textarea"
					cssStyle="width:165px" 
					name="contrato.categoriaBeneficiario" 
					list="categoriaBeneficiarioList" 
					listKey="key" 
					listValue="description" 
					value="contrato.categoriaBeneficiario.ordinal()"
					title="Categoria de Beneficiario"
					headerKey="0"
					headerValue="Seleccionar"			
					/>
			</td>
  		</tr>

		<#--    FECHA    -->

		<tr>
  			<td class="textoCampo">Fecha Desde:</td>
  			<td class="textoDato">
      			<@vc.rowCalendar 
					templateDir="custontemplates" 
					id="fechaDesde" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="contrato.fechaVigenciaDesde" 
					title="Fecha Desde" />
			</td>

  			<td class="textoCampo" >Fecha Hasta:</td>
			<td class="textoDato">
				<@vc.rowCalendar 
					templateDir="custontemplates" 
					id="fechaHasta" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="contrato.fechaVigenciaHasta" 
					title="Fecha Hasta" />				
			</td>	 
		</tr>	
		<tr>
      		<td class="textoCampo">Frecuencia de Pago:</td>
			<td class="textoDato">
		    	<@s.select 
					templateDir="custontemplates" 
					id="frecuenciaPago" 
					cssClass="textarea"
					cssStyle="width:165px" 
					name="contrato.frecuenciaPago" 
					list="frecuenciaPagoList" 
					listKey="key" 
					listValue="description" 
					value="contrato.frecuenciaPago.ordinal()"
					title="Frecuencia Pago"
					headerKey="0"
					headerValue="Seleccionar"			
					/>
			</td>
			<td class="textoCampo">D&iacute;a de pago:</td>
  			<td class="textoDato">
      			<@s.textfield 
					templateDir="custontemplates" 
					id="diaPago" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="contrato.diaPago" 
					title="D&iacute;a de pago" />
			</td>
		</tr>

		<#--    RUBRO    -->

		<tr>
  			<td class="textoCampo">Rubro:</td>
			<td class="textoDato">
				<@s.property default="&nbsp;" escape=false value="contrato.rubro.descripcion"/>
				<@s.a templateDir="custontemplates" id="seleccionarRubro" name="seleccionarRubro" href="javascript://nop/" cssClass="ocultarIcono">
					<img src="${request.contextPath}/common/images/buscar.gif"  border="0" align="absmiddle" hspace="3" >
				</@s.a>	
			</td>
			<td class="textoCampo">Cuenta Contable:</td>
			<td class="textoDato">
				<@s.select 
					templateDir="custontemplates" 
					id="cuentaContable" 
					cssClass="textarea"
					cssStyle="width:165px" 
					name="contrato.cuentaContable.codigo" 
					list="cuentaContableList" 
					listKey="codigo" 
					listValue="descripcion" 
					value="contrato.cuentaContable.codigo"
					title="Cuenta Contable"
					headerKey="0"
					headerValue="Seleccionar"							
					/>		
				<#if cuentaContableList.size() == 1>
					<@s.hidden id="cuentaContable" name="contrato.cuentaContable.codigo"/>
				</#if>
			</td>
		</tr>

		<#--    PAGO    -->

		<tr>
			<td class="textoCampo">Comprobante de pago requerido:</td>
			<td class="textoDato">
		    	<@s.select 
					templateDir="custontemplates" 
					id="comprobanteReqPag" 
					cssClass="textarea"
					cssStyle="width:165px" 
					name="contrato.comprobanteReqPag" 
					list="comprobanteReqPagList" 
					listKey="key" 
					listValue="description" 
					value="contrato.comprobanteReqPag.ordinal()"
					title="Comprobante de pago requerido"
					headerKey="0"
					headerValue="Seleccionar"			
					/>
			</td>
  			<td class="textoCampo">Importe del Per&iacute;odo:</td>
			<td class="textoDato">
		       <@s.textfield 
  					templateDir="custontemplates" 
					id="importePeriodo" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="contrato.importePeriodo"
					disabled="false" 
					title="Importe Per&iacute;odo" />									
			</td>
		</tr>
				
		<tr>
			<td class="textoCampo">Forma de Pago:</td>
			<td class="textoDato">
				<@s.select 
					templateDir="custontemplates" 
					id="formaPago" 
					cssClass="textarea"
					cssStyle="width:165px" 
					name="contrato.formaPago.pk.idSec" 
					list="formaPagoList" 
					listKey="pk.idSec" 
					listValue="descripcion" 
					title="Forma de Pago"
					headerKey="-1"
					headerValue="Seleccionar"   
					/>
			</td>
			
		  	<td class="textoCampo">Medio de Pago:</td>
			<td class="textoDato">
				<@s.select 
					templateDir="custontemplates" 
					id="medioPago" 
					cssClass="textarea"
					cssStyle="width:165px" 
					name="contrato.medioPago.pk.idSec" 
					list="medioPagoList" 
					listKey="pk.idSec" 
					listValue="descripcion" 
					title="Medio de Pago"
					headerKey="-1"
					headerValue="Seleccionar"   
					/>
			</td>

		</tr>
				
		<tr>
			<td class="textoCampo">Estado:</td>
  			<td  class="textoDato">
				<@s.property default="&nbsp;" escape=false value="contrato.estado"/>
			</td>
			
			<@s.if test="contrato.fechaSugeridaPago != null"> 
			<td class="textoCampo">Fecha Sugerida de Pago:</td>
  			<td class="textoDato">
      			<@vc.rowCalendar 
					templateDir="custontemplates" 
					id="fechaSugeridaPago" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="contrato.fechaSugeridaPago" 
					title="Fecha Sugerida de Pago" />
			</td>
			</@s.if>
			<@s.else> 
			<td class="textoCampo">&nbsp;</td>
  			<td class="textoDato">&nbsp;</td>
			</@s.else>
		</tr>
		<tr><td colspan="4" class="textoCampo">&nbsp;</td></tr>
	</table>
	</div>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/contrato/updateView.action" 
  source="modificarContrato" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="contrato.oid={contrato.oid},contrato.versionNumber={contrato.versionNumber}"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/contrato/selectInmueble.action" 
  source="seleccionarInmueble" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId=${navigationId},flowControl=change,navegacionIdBack=${navigationId},contrato.oid={contrato.oid},contrato.versionNumber={contrato.versionNumber},contrato.estado=${contrato.estado.ordinal()},contrato.descripcion={descripcion},contrato.tipoDocumento={tipoDocumento},contrato.tipoContrato={tipoContrato},contrato.tipoAlquiler={tipoAlquiler},contrato.inmueble.oid={contrato.inmueble.oid},contrato.inmueble.descripcion={contrato.inmueble.descripcion},contrato.cartel.oid={contrato.cartel.oid},contrato.cartel.descripcion={contrato.cartel.descripcion},contrato.tipoPublicidad={tipoPublicidad},contrato.tipoDonacion={tipoDonacion},contrato.cliente.oid={contrato.cliente.oid},contrato.cliente.descripcion={contrato.cliente.descripcion},contrato.contactoRU.pk.identificador={contrato.contactoRU.pk.identificador},contrato.contactoRU.pk.secuencia={contrato.contactoRU.pk.secuencia},contrato.contactoRU.razonSocial={contrato.contactoRU.razonSocial},contrato.contactoRU.nombre={contrato.contactoRU.nombre},contrato.tipoBeneficiario={tipoBeneficiario},contrato.proveedor.nroProveedor={contrato.proveedor.nroProveedor},contrato.proveedor.detalleDePersona.razonSocial={contrato.proveedor.detalleDePersona.razonSocial},contrato.proveedor.detalleDePersona.nombre={contrato.proveedor.detalleDePersona.nombre},contrato.personaSecuencia.pk.identificador={contrato.personaSecuencia.pk.identificador},contrato.personaSecuencia.pk.secuencia={contrato.personaSecuencia.pk.secuencia},contrato.personaSecuencia.razonSocial={contrato.personaSecuencia.razonSocial},contrato.personaSecuencia.nombre={contrato.personaSecuencia.nombre},contrato.categoriaBeneficiario={categoriaBeneficiario},contrato.fechaVigenciaDesde={fechaDesde},contrato.fechaVigenciaHasta={fechaHasta},contrato.frecuenciaPago={frecuenciaPago},contrato.diaPago={diaPago},contrato.rubro.oid={contrato.rubro.oid},contrato.rubro.descripcion={contrato.rubro.descripcion},contrato.cuentaContable.codigo={cuentaContable},contrato.comprobanteReqPag={comprobanteReqPag},contrato.formaPago={formaPago},contrato.medioPago={medioPago},contrato.importePeriodo={importePeriodo}"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/contrato/selectCarteleria.action" 
  source="seleccionarCarteleria" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId=${navigationId},flowControl=change,navegacionIdBack=${navigationId},contrato.oid={contrato.oid},contrato.versionNumber={contrato.versionNumber},contrato.estado=${contrato.estado.ordinal()},contrato.descripcion={descripcion},contrato.tipoDocumento={tipoDocumento},contrato.tipoContrato={tipoContrato},contrato.tipoAlquiler={tipoAlquiler},contrato.inmueble.oid={contrato.inmueble.oid},contrato.inmueble.descripcion={contrato.inmueble.descripcion},contrato.cartel.oid={contrato.cartel.oid},contrato.cartel.descripcion={contrato.cartel.descripcion},contrato.tipoPublicidad={tipoPublicidad},contrato.tipoDonacion={tipoDonacion},contrato.cliente.oid={contrato.cliente.oid},contrato.cliente.descripcion={contrato.cliente.descripcion},contrato.contactoRU.pk.identificador={contrato.contactoRU.pk.identificador},contrato.contactoRU.pk.secuencia={contrato.contactoRU.pk.secuencia},contrato.contactoRU.razonSocial={contrato.contactoRU.razonSocial},contrato.contactoRU.nombre={contrato.contactoRU.nombre},contrato.tipoBeneficiario={tipoBeneficiario},contrato.proveedor.nroProveedor={contrato.proveedor.nroProveedor},contrato.proveedor.detalleDePersona.razonSocial={contrato.proveedor.detalleDePersona.razonSocial},contrato.proveedor.detalleDePersona.nombre={contrato.proveedor.detalleDePersona.nombre},contrato.personaSecuencia.pk.identificador={contrato.personaSecuencia.pk.identificador},contrato.personaSecuencia.pk.secuencia={contrato.personaSecuencia.pk.secuencia},contrato.personaSecuencia.razonSocial={contrato.personaSecuencia.razonSocial},contrato.personaSecuencia.nombre={contrato.personaSecuencia.nombre},contrato.categoriaBeneficiario={categoriaBeneficiario},contrato.fechaVigenciaDesde={fechaDesde},contrato.fechaVigenciaHasta={fechaHasta},contrato.frecuenciaPago={frecuenciaPago},contrato.diaPago={diaPago},contrato.rubro.oid={contrato.rubro.oid},contrato.rubro.descripcion={contrato.rubro.descripcion},contrato.cuentaContable.codigo={cuentaContable},contrato.comprobanteReqPag={comprobanteReqPag},contrato.formaPago={formaPago},contrato.medioPago={medioPago},contrato.importePeriodo={importePeriodo}"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/contrato/selectCliente.action" 
  source="seleccionarCliente" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId=${navigationId},flowControl=change,navegacionIdBack=${navigationId},contrato.oid={contrato.oid},contrato.versionNumber={contrato.versionNumber},contrato.estado=${contrato.estado.ordinal()},contrato.descripcion={descripcion},contrato.tipoDocumento={tipoDocumento},contrato.tipoContrato={tipoContrato},contrato.tipoAlquiler={tipoAlquiler},contrato.inmueble.oid={contrato.inmueble.oid},contrato.inmueble.descripcion={contrato.inmueble.descripcion},contrato.cartel.oid={contrato.cartel.oid},contrato.cartel.descripcion={contrato.cartel.descripcion},contrato.tipoPublicidad={tipoPublicidad},contrato.tipoDonacion={tipoDonacion},contrato.cliente.oid={contrato.cliente.oid},contrato.cliente.descripcion={contrato.cliente.descripcion},contrato.contactoRU.pk.identificador={contrato.contactoRU.pk.identificador},contrato.contactoRU.pk.secuencia={contrato.contactoRU.pk.secuencia},contrato.contactoRU.razonSocial={contrato.contactoRU.razonSocial},contrato.contactoRU.nombre={contrato.contactoRU.nombre},contrato.tipoBeneficiario={tipoBeneficiario},contrato.proveedor.nroProveedor={contrato.proveedor.nroProveedor},contrato.proveedor.detalleDePersona.razonSocial={contrato.proveedor.detalleDePersona.razonSocial},contrato.proveedor.detalleDePersona.nombre={contrato.proveedor.detalleDePersona.nombre},contrato.personaSecuencia.pk.identificador={contrato.personaSecuencia.pk.identificador},contrato.personaSecuencia.pk.secuencia={contrato.personaSecuencia.pk.secuencia},contrato.personaSecuencia.razonSocial={contrato.personaSecuencia.razonSocial},contrato.personaSecuencia.nombre={contrato.personaSecuencia.nombre},contrato.categoriaBeneficiario={categoriaBeneficiario},contrato.fechaVigenciaDesde={fechaDesde},contrato.fechaVigenciaHasta={fechaHasta},contrato.frecuenciaPago={frecuenciaPago},contrato.diaPago={diaPago},contrato.rubro.oid={contrato.rubro.oid},contrato.rubro.descripcion={contrato.rubro.descripcion},contrato.cuentaContable.codigo={cuentaContable},contrato.comprobanteReqPag={comprobanteReqPag},contrato.formaPago={formaPago},contrato.medioPago={medioPago},contrato.importePeriodo={importePeriodo}"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/contrato/selectRubro.action" 
  source="seleccionarRubro" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId=${navigationId},flowControl=change,navegacionIdBack=${navigationId},contrato.oid={contrato.oid},contrato.versionNumber={contrato.versionNumber},contrato.estado=${contrato.estado.ordinal()},contrato.descripcion={descripcion},contrato.tipoDocumento={tipoDocumento},contrato.tipoContrato={tipoContrato},contrato.tipoAlquiler={tipoAlquiler},contrato.inmueble.oid={contrato.inmueble.oid},contrato.inmueble.descripcion={contrato.inmueble.descripcion},contrato.cartel.oid={contrato.cartel.oid},contrato.cartel.descripcion={contrato.cartel.descripcion},contrato.tipoPublicidad={tipoPublicidad},contrato.tipoDonacion={tipoDonacion},contrato.cliente.oid={contrato.cliente.oid},contrato.cliente.descripcion={contrato.cliente.descripcion},contrato.contactoRU.pk.identificador={contrato.contactoRU.pk.identificador},contrato.contactoRU.pk.secuencia={contrato.contactoRU.pk.secuencia},contrato.contactoRU.razonSocial={contrato.contactoRU.razonSocial},contrato.contactoRU.nombre={contrato.contactoRU.nombre},contrato.tipoBeneficiario={tipoBeneficiario},contrato.proveedor.nroProveedor={contrato.proveedor.nroProveedor},contrato.proveedor.detalleDePersona.razonSocial={contrato.proveedor.detalleDePersona.razonSocial},contrato.proveedor.detalleDePersona.nombre={contrato.proveedor.detalleDePersona.nombre},contrato.personaSecuencia.pk.identificador={contrato.personaSecuencia.pk.identificador},contrato.personaSecuencia.pk.secuencia={contrato.personaSecuencia.pk.secuencia},contrato.personaSecuencia.razonSocial={contrato.personaSecuencia.razonSocial},contrato.personaSecuencia.nombre={contrato.personaSecuencia.nombre},contrato.categoriaBeneficiario={categoriaBeneficiario},contrato.fechaVigenciaDesde={fechaDesde},contrato.fechaVigenciaHasta={fechaHasta},contrato.frecuenciaPago={frecuenciaPago},contrato.diaPago={diaPago},contrato.rubro.oid={contrato.rubro.oid},contrato.rubro.descripcion={contrato.rubro.descripcion},contrato.cuentaContable.codigo={cuentaContable},contrato.comprobanteReqPag={comprobanteReqPag},contrato.formaPago={formaPago},contrato.medioPago={medioPago},contrato.importePeriodo={importePeriodo}"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/contrato/selectProveedor.action" 
  source="seleccionarProveedor" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId=${navigationId},flowControl=change,navegacionIdBack=${navigationId},contrato.oid={contrato.oid},contrato.versionNumber={contrato.versionNumber},contrato.estado=${contrato.estado.ordinal()},contrato.descripcion={descripcion},contrato.tipoDocumento={tipoDocumento},contrato.tipoContrato={tipoContrato},contrato.tipoAlquiler={tipoAlquiler},contrato.inmueble.oid={contrato.inmueble.oid},contrato.inmueble.descripcion={contrato.inmueble.descripcion},contrato.cartel.oid={contrato.cartel.oid},contrato.cartel.descripcion={contrato.cartel.descripcion},contrato.tipoPublicidad={tipoPublicidad},contrato.tipoDonacion={tipoDonacion},contrato.cliente.oid={contrato.cliente.oid},contrato.cliente.descripcion={contrato.cliente.descripcion},contrato.contactoRU.pk.identificador={contrato.contactoRU.pk.identificador},contrato.contactoRU.pk.secuencia={contrato.contactoRU.pk.secuencia},contrato.contactoRU.razonSocial={contrato.contactoRU.razonSocial},contrato.contactoRU.nombre={contrato.contactoRU.nombre},contrato.tipoBeneficiario={tipoBeneficiario},contrato.proveedor.nroProveedor={contrato.proveedor.nroProveedor},contrato.proveedor.detalleDePersona.razonSocial={contrato.proveedor.detalleDePersona.razonSocial},contrato.proveedor.detalleDePersona.nombre={contrato.proveedor.detalleDePersona.nombre},contrato.personaSecuencia.pk.identificador={contrato.personaSecuencia.pk.identificador},contrato.personaSecuencia.pk.secuencia={contrato.personaSecuencia.pk.secuencia},contrato.personaSecuencia.razonSocial={contrato.personaSecuencia.razonSocial},contrato.personaSecuencia.nombre={contrato.personaSecuencia.nombre},contrato.categoriaBeneficiario={categoriaBeneficiario},contrato.fechaVigenciaDesde={fechaDesde},contrato.fechaVigenciaHasta={fechaHasta},contrato.frecuenciaPago={frecuenciaPago},contrato.diaPago={diaPago},contrato.rubro.oid={contrato.rubro.oid},contrato.rubro.descripcion={contrato.rubro.descripcion},contrato.cuentaContable.codigo={cuentaContable},contrato.comprobanteReqPag={comprobanteReqPag},contrato.formaPago={formaPago},contrato.medioPago={medioPago},contrato.importePeriodo={importePeriodo}"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/contrato/selectPersonaSecuencia.action" 
  source="seleccionarPersonaSecuencia" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId=${navigationId},flowControl=change,navegacionIdBack=${navigationId},contrato.oid={contrato.oid},contrato.versionNumber={contrato.versionNumber},contrato.estado=${contrato.estado.ordinal()},contrato.descripcion={descripcion},contrato.tipoDocumento={tipoDocumento},contrato.tipoContrato={tipoContrato},contrato.tipoAlquiler={tipoAlquiler},contrato.inmueble.oid={contrato.inmueble.oid},contrato.inmueble.descripcion={contrato.inmueble.descripcion},contrato.cartel.oid={contrato.cartel.oid},contrato.cartel.descripcion={contrato.cartel.descripcion},contrato.tipoPublicidad={tipoPublicidad},contrato.tipoDonacion={tipoDonacion},contrato.cliente.oid={contrato.cliente.oid},contrato.cliente.descripcion={contrato.cliente.descripcion},contrato.contactoRU.pk.identificador={contrato.contactoRU.pk.identificador},contrato.contactoRU.pk.secuencia={contrato.contactoRU.pk.secuencia},contrato.contactoRU.razonSocial={contrato.contactoRU.razonSocial},contrato.contactoRU.nombre={contrato.contactoRU.nombre},contrato.tipoBeneficiario={tipoBeneficiario},contrato.proveedor.nroProveedor={contrato.proveedor.nroProveedor},contrato.proveedor.detalleDePersona.razonSocial={contrato.proveedor.detalleDePersona.razonSocial},contrato.proveedor.detalleDePersona.nombre={contrato.proveedor.detalleDePersona.nombre},contrato.personaSecuencia.pk.identificador={contrato.personaSecuencia.pk.identificador},contrato.personaSecuencia.pk.secuencia={contrato.personaSecuencia.pk.secuencia},contrato.personaSecuencia.razonSocial={contrato.personaSecuencia.razonSocial},contrato.personaSecuencia.nombre={contrato.personaSecuencia.nombre},contrato.categoriaBeneficiario={categoriaBeneficiario},contrato.fechaVigenciaDesde={fechaDesde},contrato.fechaVigenciaHasta={fechaHasta},contrato.frecuenciaPago={frecuenciaPago},contrato.diaPago={diaPago},contrato.rubro.oid={contrato.rubro.oid},contrato.rubro.descripcion={contrato.rubro.descripcion},contrato.cuentaContable.codigo={cuentaContable},contrato.comprobanteReqPag={comprobanteReqPag},contrato.formaPago={formaPago},contrato.medioPago={medioPago},contrato.importePeriodo={importePeriodo}"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/contrato/selectContactoRU.action" 
  source="seleccionarContactoRU" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId=${navigationId},flowControl=change,navegacionIdBack=${navigationId},contrato.oid={contrato.oid},contrato.versionNumber={contrato.versionNumber},contrato.estado=${contrato.estado.ordinal()},contrato.descripcion={descripcion},contrato.tipoDocumento={tipoDocumento},contrato.tipoContrato={tipoContrato},contrato.tipoAlquiler={tipoAlquiler},contrato.inmueble.oid={contrato.inmueble.oid},contrato.inmueble.descripcion={contrato.inmueble.descripcion},contrato.cartel.oid={contrato.cartel.oid},contrato.cartel.descripcion={contrato.cartel.descripcion},contrato.tipoPublicidad={tipoPublicidad},contrato.tipoDonacion={tipoDonacion},contrato.cliente.oid={contrato.cliente.oid},contrato.cliente.descripcion={contrato.cliente.descripcion},contrato.contactoRU.pk.identificador={contrato.contactoRU.pk.identificador},contrato.contactoRU.pk.secuencia={contrato.contactoRU.pk.secuencia},contrato.contactoRU.razonSocial={contrato.contactoRU.razonSocial},contrato.contactoRU.nombre={contrato.contactoRU.nombre},contrato.tipoBeneficiario={tipoBeneficiario},contrato.proveedor.nroProveedor={contrato.proveedor.nroProveedor},contrato.proveedor.detalleDePersona.razonSocial={contrato.proveedor.detalleDePersona.razonSocial},contrato.proveedor.detalleDePersona.nombre={contrato.proveedor.detalleDePersona.nombre},contrato.personaSecuencia.pk.identificador={contrato.personaSecuencia.pk.identificador},contrato.personaSecuencia.pk.secuencia={contrato.personaSecuencia.pk.secuencia},contrato.personaSecuencia.razonSocial={contrato.personaSecuencia.razonSocial},contrato.personaSecuencia.nombre={contrato.personaSecuencia.nombre},contrato.categoriaBeneficiario={categoriaBeneficiario},contrato.fechaVigenciaDesde={fechaDesde},contrato.fechaVigenciaHasta={fechaHasta},contrato.frecuenciaPago={frecuenciaPago},contrato.diaPago={diaPago},contrato.rubro.oid={contrato.rubro.oid},contrato.rubro.descripcion={contrato.rubro.descripcion},contrato.cuentaContable.codigo={cuentaContable},contrato.comprobanteReqPag={comprobanteReqPag},contrato.formaPago={formaPago},contrato.medioPago={medioPago},contrato.importePeriodo={importePeriodo}"/>
