<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>

<div id="capaSeparadora" class="capaSeparadora">&nbsp;</div>
<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="left">
				<input id="cancelar" type="button" name="btnVolver" value="Volver" class="boton"/>
				<@security.button
					templateDir="custontemplates"
					securityCode="CUF0527" 
					enabled="true"
					name="btnActivar"
					id="store"
					value="Autorizar"
					cssClass="boton">
				</@security.button>&nbsp;
			</td>
		</tr>	
	</table>
</div>
  
  
  <@vc.htmlContent 
 baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancelar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=${navegacionIdBack},flowControl=back"/>
  
  <@vc.htmlContent 
  baseUrl="${request.contextPath}/contrato/authorizationContrato.action" 
  source="store" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="contrato.oid={contrato.oid},contrato.fechaSugeridaPago={fechaSugeridaPago}"/>
  