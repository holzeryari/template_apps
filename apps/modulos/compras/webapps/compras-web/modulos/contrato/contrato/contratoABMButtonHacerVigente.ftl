<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancel" type="button" name="btnCancelar" value="Cancelar" class="boton"/>
				<input id="hacerVigente" type="button" name="btnHacerVigente" value="Hacer Vigente" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/contrato/hacerVigenteContrato.action" 
  source="hacerVigente" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="contrato.oid={contrato.oid},contrato.versionNumber={contrato.versionNumber}"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action"  
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=buscar-contrato,flowControl=back"/>
