<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>


<#if (contrato.obligacionPagoHelperList?exists && contrato.obligacionPagoHelperList.size()>0) || businessExceptionObligacionPagoSis?exists >

<div id="capaSeparadora" class="capaSeparadora">&nbsp;</div>

<@vc.anchors target="contentTrx" ajaxFlag="ajax">
	<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
		<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
			<tr><td>Obligaciones / Ordenes de Pago relacionadas</td></tr>
		</table>	
		
		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="contrato.obligacionPagoHelperList" id="obligacionPago" defaultsort=1>
		
			<@display.column headerClass="tbl-contract-service-select" class="botoneraAnchoCon1" title="Acciones">
				<div class="alineacion">
					<#if obligacionPago.id?exists>
						<a href="javascript://nop/" 
							class="item">
							<img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver"  border="0" 
								onclick="window.open('${obligacionPago.tesoreriaObligacionPagoURLCompleta}','_blank','width=800,height=280,resizable=yes,scrollbars=yes,left=100,top=80,toolbar=no,menubar=no,location=no'); return false;">
						</a>
					</#if>
				</div>
			</@display.column>
		

			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="numero" title="Nro." />				

			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="persona" title="Beneficiario" />  
			
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="importe" title="Importe" />
			
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="saldo" title="Saldo" />

			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="fechaPago" title="Fecha Pago" /> 
			
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="fechaSugeridaPago" title="Fecha Sugerida Pago" />
			
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="estado" title="Estado" />		

		</@display.table>
		
			<#if businessExceptionObligacionPagoSis?exists>
				<#list businessExceptionObligacionPagoSis.descriptions as description >
					<#if description?exists>
						<span class="busquedaSinResultado">${description}</span>
						
					</#if>
				</#list>
			</#if>
	</div>
</@vc.anchors>
</#if>
