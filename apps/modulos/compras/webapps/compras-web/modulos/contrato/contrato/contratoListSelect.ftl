<#setting locale="es_AR">

<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>

<@s.hidden id="navigationId" name="navigationId"/>
<@s.hidden id="navegacionIdBack" name="navegacionIdBack"/>

<@s.hidden id="comprobanteReqDuro" name="comprobanteReqDuro"/>
<#if comprobanteReqDuro?exists && comprobanteReqDuro>
	<@s.hidden id="contrato.comprobanteReqPag" name="contrato.comprobanteReqPag.ordinal()"/>
</#if>
<@s.hidden id="estadoDuro" name="estadoDuro"/>
<#if comprobanteReqDuro?exists && comprobanteReqDuro>
	<@s.hidden id="contrato.estado" name="contrato.estado.ordinal()"/>
</#if>
<@s.hidden id="contrato.proveedor.nroProveedor" name="contrato.proveedor.nroProveedor"/>
<@s.hidden id="contrato.proveedor.detalleDePersona.razonSocial" name="contrato.proveedor.detalleDePersona.razonSocial"/>
<@s.hidden id="contrato.proveedor.detalleDePersona.nombre" name="contrato.proveedor.detalleDePersona.nombre"/>
<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
        	<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
			<tr>
				<td><b>Datos del Contrato, Acuerdo o Convenio</b></td>
			</tr>
		</table>

		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
	    		<td class="textoCampo" colspan="4">&nbsp;</td>
	    	</tr>
			<tr>
  				<td class="textoCampo">N&uacute;mero:</td>
      			<td class="textoDato">
      			<@s.textfield 
      					templateDir="custontemplates" 
						id="contrato.oid" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="contrato.oid" 
						title="N&uacute;mero" />
				</td>
				
				<td class="textoCampo">Descripci&oacute;n:</td>
      			<td class="textoDato">
      			<@s.textfield 
      					templateDir="custontemplates" 
						id="contrato.descripcion" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="contrato.descripcion" 
						title="Descripci&oacute;n" />
				</td>	
			</tr>
			<tr>					
				<td class="textoCampo">Tipo Documento:</td>
      			<td class="textoDato">
	      			<@s.select 
						templateDir="custontemplates" 
						id="contrato.tipoDocumento" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="contrato.tipoDocumento" 
						list="tipoDocumentoList" 
						listKey="key" 
						listValue="description" 
						value="contrato.tipoDocumento.ordinal()"
						title="Tipo Documento"
						headerKey="0"
						headerValue="Todos"							
						/>
				</td>
				
				<td class="textoCampo">Tipo Contrato:</td>
      			<td class="textoDato">
	      			<@s.select 
						templateDir="custontemplates" 
						id="contrato.tipoContrato" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="contrato.tipoContrato" 
						list="tipoContratoList" 
						listKey="key" 
						listValue="description" 
						value="contrato.tipoContrato.ordinal()"
						title="Tipo Contrato"
						headerKey="0"
						headerValue="Todos"							
						/>
				</td>					
			</tr>

			<tr>
				<td class="textoCampo">Comprobante Pago Req.:</td>
      			<td class="textoDato">
	      			<@s.select 
						templateDir="custontemplates" 
						id="contrato.comprobanteReqPag" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="contrato.comprobanteReqPag" 
						list="comprobanteReqPagList" 
						listKey="key" 
						listValue="description" 
						value="contrato.comprobanteReqPag.ordinal()"
						title="Comprobante Pago Req."
						headerKey="0"
						headerValue="Todos"							
						/>
				</td>
				<td class="textoCampo">Estado:</td>
      			<td class="textoDato">
	      			<@s.select 
						templateDir="custontemplates" 
						id="contrato.estado" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="contrato.estado" 
						list="estadoList" 
						listKey="key" 
						listValue="description" 
						value="contrato.estado.ordinal()"
						title="Estado"
						headerKey="0"
						headerValue="Todos"							
						/>
				</td>
			
			</tr>
			<tr>
				<td class="textoCampo">Proveedor:</td>
      			<td class="textoDato" colspan="3">
				<@s.property default="&nbsp;" escape=false value="contrato.proveedor.detalleDePersona.razonSocial" />
				<@s.property default="&nbsp;" escape=false value="contrato.proveedor.detalleDePersona.nombre" />
						<@s.a templateDir="custontemplates" id="seleccionarPersona" name="seleccionarPersona" href="javascript://nop/" cssClass="ocultarIcono">
								<img src="${request.contextPath}/common/images/buscar.gif" align="top" border="0" hspace="3">
						</@s.a>
				</td>	
			</tr>				
			<tr>
    			<td colspan="4" class="lineaGris" align="right">
      			<input id="limpiar" type="button" name="btnVolver" value="Limpiar" class="boton"/>		
      			<input id="buscar" type="button" name="btnVolver" value="Buscar" class="boton"/>
    			</td>
			</tr>	
			</table>
		</div>	
		
         <!-- Resultado Filtro -->
			
		<@s.if test="contratoList!=null">
			<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
				<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>Contratos, Acuerdos o Convenios encontrados</td>
				</tr>
				</table>
				<@ajax.anchors target="contentTrx">			
          		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="contratoList" id="contratoSelect" pagesize=15 defaultsort=2 partialList=true size="recordSize" keepStatus=true excludedParams="resetFlag">
          		
  		
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select" class="botoneraAnchoCon1" title="Acciones">
						<#assign ref="${request.contextPath}/contrato/selectContratoBack.action?">
								<@s.a href="${ref}contrato.oid=${contratoSelect.oid?c}&cliente.descripcion=${contratoSelect.descripcion}&navegacionIdBack=${navegacionIdBack}&navigationId=${navigationId}"><img src="${request.contextPath}/common/images/seleccionar.gif" alt="Seleccionar" title="Seleccionar" border="0"></@s.a>
								&nbsp;					
					</@display.column>

					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="oid" title="N&uacute;mero" />

					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="descripcion" title="Descripci&oacute;n" />				

					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="tipoDocumento" title="Tipo Documento" />

					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="tipoContrato" title="Tipo Contrato" />

					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="estado" title="Estado" />		

				</@display.table>
			</@ajax.anchors>
			</div>
		</@s.if>
			
		<div id="capaBotonera" class="capaBotonera">
			<table id="tablaBotonera" class="tablaBotonera">
				<tr> 
					<td align="left">
						<input id="cancel" type="button" name="btnVolver" value="Volver" class="boton"/>
					
					</td>
				</tr>	
			</table>
		</div>					
			
<@vc.htmlContent 
  baseUrl="${request.contextPath}/contrato/selectSearch.action"
  source="buscar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="resetFlag=true,navigationId={navigationId},flowControl=regis,contrato.oid={contrato.oid},contrato.descripcion={contrato.descripcion},contrato.tipoDocumento={contrato.tipoDocumento},contrato.tipoContrato={contrato.tipoContrato},contrato.estado={contrato.estado},contrato.comprobanteReqPag={contrato.comprobanteReqPag},proveedorDuro={proveedorDuro},estadoDuro={estadoDuro},comprobanteReqDuro={comprobanteReqDuro},contrato.proveedor.nroProveedor={contrato.proveedor.nroProveedor},contrato.proveedor.detalleDePersona.razonSocial={contrato.proveedor.detalleDePersona.razonSocial},contrato.proveedor.detalleDePersona.nombre={contrato.proveedor.detalleDePersona.nombre},navegacionIdBack={navegacionIdBack}"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/contrato/selectView.action"
  source="limpiar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=regis,contrato.estado={contrato.estado},contrato.comprobanteReqPag={contrato.comprobanteReqPag},proveedorDuro={proveedorDuro},estadoDuro={estadoDuro},comprobanteReqDuro={comprobanteReqDuro},contrato.proveedor.nroProveedor={contrato.proveedor.nroProveedor},contrato.proveedor.detalleDePersona.razonSocial={contrato.proveedor.detalleDePersona.razonSocial},contrato.proveedor.detalleDePersona.nombre={contrato.proveedor.detalleDePersona.nombre},navegacionIdBack={navegacionIdBack}"/>
 
 <@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navegacionIdBack},flowControl=back"/>
 
 