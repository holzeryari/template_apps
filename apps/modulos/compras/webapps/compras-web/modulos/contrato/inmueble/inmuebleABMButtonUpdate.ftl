<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancel" type="button" name="btnVolver" value="Cancelar" class="boton"/>
				<input id="update" type="button" name="btnModificar" value="Modificar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/inmueble/updateInmueble.action" 
  source="update" 
  success="contentTrx" 
  failure="errorTrx"  
   parameters="inmueble.oid={inmueble.oid},inmueble.versionNumber={inmueble.versionNumber},inmueble.descripcion={inmueble.descripcion}"/>
  
  <@vc.htmlContent 
baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=buscar-inmueble,flowControl=back"/>
 
  
  <@vc.htmlContent 
  baseUrl="${request.contextPath}/inmueble/selectProveedor.action" 
  source="seleccionarProveedor" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId=inmueble-update,flowControl=change,navegacionIdBack=inmueble-update,inmueble.descripcion={inmueble.descripcion},inmueble.oid={inmueble.oid},inmueble.estado={estadoInmueble},inmueble.proveedor.nroProveedor={inmueble.proveedor.nroProveedor}"/>
  
  
  