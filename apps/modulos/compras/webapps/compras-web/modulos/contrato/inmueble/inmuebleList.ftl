
<#setting locale="es_AR">

<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>

<@s.hidden id="navigationId" name="navigationId"/>


<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
        	<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
			<tr>
				<td><b>Datos del Inmueble</b></td>
				<td>
					<div align="right">										
					<@security.a 
						templateDir="custontemplates" 
						securityCode="CUF0541" 
						enabled="deposito.readable" 
						cssClass="item" 
						id="crear"										
						href="javascript://nop/">
						<b>Agregar</b>										
						<img src="${request.contextPath}/common/images/agregar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
					</@security.a>
					</div>
				</td>
			</tr>
		</table>
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>
			<tr>
				<td class="textoCampo">C&oacute;digo:</td>
      			<td class="textoDato">
      			<@s.textfield 
  					templateDir="custontemplates" 
					id="inmueble.oid" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="inmueble.oid" 
					title="C&oacute;digo" /></td>
      		
      			<td class="textoCampo">Descripci&oacute;n</td>
      			<td class="textoDato"><@s.textfield 
  					templateDir="custontemplates" 
					id="inmueble.descripcion" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="inmueble.descripcion" 
					title="Descripcion" /></td>
    		</tr>					
							
			<tr>
				<td class="textoCampo">Estado:</td>
      			<td class="textoDato" colspan="3">
	      			<@s.select 
						templateDir="custontemplates" 
						id="inmueble.estado" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="inmueble.estado" 
						list="estadoList" 
						listKey="key" 
						listValue="description" 
						value="inmueble.estado.ordinal()"
						title="Estado"
						headerKey="0"
						headerValue="Todos"							
						/>
				</td>
			</tr>
						
			<tr>
	    		<td colspan="4" class="lineaGris" align="right">
	      			<input id="limpiar" type="button" name="btnVolver" value="Limpiar" class="boton"/>		
	      			<input id="buscar" type="button" name="btnVolver" value="Buscar" class="boton"/>
	    		</td>
			</tr>	
		</table>
	</div>						
			
	<@s.if test="inmuebleList!=null">
					
		<#assign inmuebleOid="0" />
		<@s.if test="inmueble.oid!=null">
			<#assign inmuebleOid="${inmueble.oid}" />
		</@s.if>

		<#assign inmuebleDescripcion="" />
		<@s.if test="inmueble.descripcion!=null">
			<#assign inmuebleDescripcion="${inmueble.descripcion}" />
		</@s.if>

		<#assign inmuebleEstado="0" />
		<@s.if test="inmueble.estado != null">
			<#assign inmuebleEstado="${inmueble.estado.ordinal()}" />
		</@s.if>

		
		<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
		
		<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>Inmuebles encontrados</td>
					<td>
						<div class="alineacionDerecha">
							<@security.a 
								templateDir="custontemplates" 
								securityCode="CUF0542" 
								cssClass="item" 
								href="${request.contextPath}/inmueble/printXLS.action?inmueble.oid=${inmuebleOid}&inmueble.descripcion=${inmuebleDescripcion}&inmueble.estado=${inmuebleEstado}">
								<img src="${request.contextPath}/common/images/imprimirExcel.gif" title="Exportar a Excel" align="absmiddle" border="0" hspace="3">
							</@security.a>	
						</div>
						<div class="alineacionDerecha">
							<b>Imprimir</b>
							<@security.a 
								templateDir="custontemplates" 
								securityCode="CUF0542" 
								cssClass="item" 
								href="${request.contextPath}/inmueble/printPDF.action?inmueble.oid=${inmuebleOid}&inmueble.descripcion=${inmuebleDescripcion}&inmueble.estado=${inmuebleEstado}">
								<img src="${request.contextPath}/common/images/imprimirPdf.gif" title="Exportar a PDF" align="absmiddle" border="0" hspace="3">
							</@security.a>
						</div>	
						
					</td>
				</tr>
			</table>
							
			<@vc.anchors target="contentTrx" ajaxFlag="ajax">
          		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="inmuebleList" id="inmueble" pagesize=15 defaultsort=2 partialList=true size="recordSize" keepStatus=true excludedParams="resetFlag">
					
					<@display.column headerClass="tbl-contract-service-select"  class="botoneraAnchoCon4" title="Acciones">
					<div class="alineacion">				
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0543" 
							enabled="inmueble.readable" 
							cssClass="item" 
							href="${request.contextPath}/inmueble/readInmuebleView.action?inmueble.oid=${inmueble.oid?c}">
							<img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver" border="0">
						</@security.a>
						</div>
						<div class="alineacion">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0544" 
							enabled="inmueble.updatable"
							cssClass="item"  
							href="${request.contextPath}/inmueble/updateInmuebleView.action?inmueble.oid=${inmueble.oid?c}&navigationId=inmueble-update&flowControl=regis">
							<img  src="${request.contextPath}/common/images/modificar.gif" alt="Modificar" title="Modificar" border="0">
						</@security.a>									
						</div>
						<div class="alineacion">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0545" 
							enabled="inmueble.eraseable"
							cssClass="item"  
							href="${request.contextPath}/inmueble/deleteInmuebleView.action?inmueble.oid=${inmueble.oid?c}">
							<img  src="${request.contextPath}/common/images/eliminar.gif" alt="Eliminar" title="Eliminar" border="0">
						</@security.a>									
						</div>
					<div class="alineacion">
					<#if inmueble.estado == "Activo">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0546" 
							enabled="true"
							cssClass="item"  
							href="${request.contextPath}/inmueble/cambiarEstadoView.action?inmueble.oid=${inmueble.oid?c}">
							<img src="${request.contextPath}/common/images/desactivar.gif" alt="Desactivar" title="Desactivar" border="0">
						</@security.a>			
					<#else>
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0546" 
							enabled="true"
							cssClass="item"  
							href="${request.contextPath}/inmueble/cambiarEstadoView.action?inmueble.oid=${inmueble.oid?c}">
							<img src="${request.contextPath}/common/images/activar.gif" alt="Activar" title="Activar" border="0">
						</@security.a>								
					</#if>	
					</div>							
					</@display.column>

					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="oid" title="C&oacute;digo" />

					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="descripcion" title="Descripci&oacute;n" />

					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="estado" title="Estado" />					
					
				</@display.table>
			</@vc.anchors>
		</div>	
	</@s.if>
			

<@vc.htmlContent 
  baseUrl="${request.contextPath}/inmueble/search.action" 
  source="buscar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="resetFlag=true,navigationId={navigationId},flowControl=regis,inmueble.oid={inmueble.oid},inmueble.descripcion={inmueble.descripcion},inmueble.estado={inmueble.estado}"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/inmueble/view.action" 
  source="limpiar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=regis"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/inmueble/createInmuebleView.action" 
  source="crear" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=inmueble-create,flowControl=regis"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/inmueble/selectProveedor.action" 
  source="seleccionarProveedor" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=change,navegacionIdBack={navigationId},inmueble.oid={inmueble.oid},inmueble.descripcion={inmueble.descripcion},inmueble.estado={inmueble.estado}"/>
