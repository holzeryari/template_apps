
<#setting locale="es_AR">

<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>

<@s.hidden id="navigationId" name="navigationId"/>
<@s.hidden id="estadoDuro" name="estadoDuro"/>
<@s.if test="estadoDuro == true">
	<@s.hidden id="inmueble.estado" name="inmueble.estado.ordinal()"/>
</@s.if>
<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
        	<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
			<tr>
				<td><b>Datos del Inmueble</b></td>
			</tr>
		</table>
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>
				
			<tr>
				<td class="textoCampo">C&oacute;digo:</td>
      			<td class="textoDato">
      			<@s.textfield 
  					templateDir="custontemplates" 
					id="inmueble.oid" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="inmueble.oid" 
					title="C&oacute;digo" /></td>
      		
      			<td class="textoCampo">Descripci&oacute;n</td>
      			<td class="textoDato"><@s.textfield 
  					templateDir="custontemplates" 
					id="inmueble.descripcion" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="inmueble.descripcion" 
					title="Descripcion" /></td>
    		</tr>					

			<tr>
				<td class="textoCampo">Estado:</td>
      			<td class="textoDato" colspan="3">
	      			<@s.select 
						templateDir="custontemplates" 
						id="inmueble.estado" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="inmueble.estado" 
						list="estadoList" 
						listKey="key" 
						listValue="description" 
						value="inmueble.estado.ordinal()"
						title="Estado"
						headerKey="0"
						headerValue="Todos"							
						/>
				</td>
			</tr>
			<tr>
	    		<td colspan="4" class="lineaGris" align="right">
	      			<input id="limpiar" type="button" name="btnVolver" value="Limpiar" class="boton"/>		
	      			<input id="buscar" type="button" name="btnVolver" value="Buscar" class="boton"/>
	    		</td>
			</tr>	
		</table>
	</div>			
						
	<!-- Resultado Filtro -->
	<@s.if test="inmuebleList!=null">
					
		<#assign inmuebleOid="0" />
		<@s.if test="inmueble.oid!=null">
			<#assign inmuebleOid="${inmueble.oid}" />
			<#assign inmuebleDescripcion="${inmueble.descripcion}" />
			<#assign inmuebleEstado = "${inmueble.estado.ordinal()}" />
		</@s.if>
		<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
			<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>Inmuebles encontrados</td>
				</tr>
			</table>
			
			<@vc.anchors target="contentTrx" ajaxFlag="ajax">
          		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="inmuebleList" id="inmuebleSelect" pagesize=15 defaultsort=2 partialList=true size="recordSize" keepStatus=true excludedParams="resetFlag">

					<@s.if test="nameSpaceSelect!=''">								
						<#assign ref="${request.contextPath}/inmueble/selectInmueble.action?">										
						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select" style=" text-align:center; display:block" title="Acciones">			
							<@s.a href="${ref}inmueble.oid=${inmuebleSelect.oid?c}&inmueble.descripcion=${inmuebleSelect.descripcion}&navegacionIdBack=${navegacionIdBack}&navigationId=${navigationId}&nameSpaceSelect=${nameSpaceSelect}&actionNameSelect=${actionNameSelect}&oidParameter=${oidParameter}"><img src="${request.contextPath}/common/images/seleccionar.gif" alt="Seleccionar" title="Seleccionar" width="16" height="16" border="0"></@s.a>
							&nbsp;			
						</@display.column>								  															
					</@s.if>

					<@s.else>
						<#assign ref="${request.contextPath}/inmueble/selectInmuebleBack.action?">											
						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select" style=" text-align:center; display:block" title="Acciones">			
							<@s.a href="${ref}inmueble.oid=${inmuebleSelect.oid?c}&inmueble.descripcion=${inmuebleSelect.descripcion}&navegacionIdBack=${navegacionIdBack}&navigationId=${navigationId}"><img src="${request.contextPath}/common/images/seleccionar.gif" alt="Seleccionar" title="Seleccionar" width="16" height="16" border="0"></@s.a>
							&nbsp;			
						</@display.column>	
						
					</@s.else>

					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="oid" title="C&oacute;digo" />

					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="descripcion" title="Descripci&oacute;n" />

					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="estado" title="Estado" />					

				</@display.table>
			</@vc.anchors>
		</div>
	</@s.if>
	
	<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="left">
				<input id="cancel" type="button" name="btnVolver" value="Volver" class="boton"/>
			</td>
		</tr>	
	</table>
	</div>						

		  

<@vc.htmlContent 
  baseUrl="${request.contextPath}/inmueble/selectSearchInmueble.action" 
  source="buscar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="resetFlag=true,navigationId={navigationId},flowControl=regis,inmueble.descripcion={inmueble.descripcion},inmueble.oid={inmueble.oid},inmueble.estado={inmueble.estado},navegacionIdBack=${navegacionIdBack},nameSpaceSelect=${nameSpaceSelect},actionNameSelect=${actionNameSelect},oidParameter=${oidParameter},estadoDuro={estadoDuro}"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/inmueble/selectViewInmueble.action" 
  source="limpiar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=regis,navegacionIdBack=${navegacionIdBack},estadoDuro={estadoDuro}"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId=${navegacionIdBack},flowControl=back"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/inmueble/selectProveedor.action" 
  source="seleccionarProveedor" 
  success="contentTrx" 
  failure="errorTrx" 
    parameters="navigationId={navigationId},flowControl=change,navegacionIdBack={navigationId},inmueble.descripcion={inmueble.descripcion},inmueble.oid={inmueble.oid},inmueble.estado={inmueble.estado},navegacionIdBack=${navegacionIdBack},nameSpaceSelect=${nameSpaceSelect},actionNameSelect=${actionNameSelect},oidParameter=${oidParameter},estadoDuro={estadoDuro}"/>
