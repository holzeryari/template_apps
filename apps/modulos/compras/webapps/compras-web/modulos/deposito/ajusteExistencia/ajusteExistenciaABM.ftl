<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign tiles=JspTaglibs["/WEB-INF/tlds/tiles-jsp.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<@s.hidden id="oid" name="ajusteExistencia.oid"/>
<@s.hidden id="versionNumber" name="ajusteExistencia.versionNumber"/>

	<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
			<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos del Ajuste de Existencia</b></td>
				<td>
					<div align="right">
						<@s.a templateDir="custontemplates" id="modificarAjusteExistencia" 
						name="modificarAjusteExistencia" href="javascript://nop/" cssClass="ocultarIcono">
							<b>Modificar</b><img src="${request.contextPath}/common/images/modificar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
						</@s.a>
					</div>
				</td>
			</tr>
		</table>
		
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>				
				
			<tr>
      			<td class="textoCampo">N&uacute;mero:</td>
      			<td class="textoDato">
      				<@s.property default="&nbsp;" escape=false value="ajusteExistencia.numero"/>
      			</td>
      			<td  class="textoCampo">Fecha: </td>
				<td class="textoDato">
					<@s.if test="ajusteExistencia.fecha != null">
						<#assign fechaAjuste = ajusteExistencia.fecha> 
						${fechaAjuste?string("dd/MM/yyyy")}	
					</@s.if>	&nbsp;								
				</td>	      			
			</tr>	

			<tr>
      			<td class="textoCampo">Dep&oacute;sito:</td>
	      		<td class="textoDato">
	      			<@s.select 
						templateDir="custontemplates" 
						id="deposito" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="ajusteExistencia.deposito.oid" 
						list="depositoList" 
						listKey="oid" 
						listValue="descripcion" 
						value="oid"
						title="Deposito"
						headerKey="0" 
            			headerValue="Seleccionar"
            			value="ajusteExistencia.deposito.oid"/>
     			</td>		      		
				<td class="textoCampo">Motivo:</td>
	      		<td class="textoDato">
					<@s.select 
						templateDir="custontemplates" 
						id="motivo" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="ajusteExistencia.motivo" 
						list="motivoAjusteExistenciaList" 
						listKey="key" 
						listValue="description" 
						value="ajusteExistencia.motivo.ordinal()"
						headerKey="0" 
	                    headerValue="Seleccionar" 
						title="Motivo" />
				</td>
	    	</tr>
	    	<tr>
				<td class="textoCampo">Observaciones:</td>
	      		<td  class="textoDato" colspan="3">
					<@s.textarea	
						templateDir="custontemplates" 						  
						cols="89" rows="4"	      					
						cssClass="textarea"
						id="observaciones"							 
						name="ajusteExistencia.observaciones"  
						label="Observaciones"														
						 />
				</td>					
	    	</tr>
			<tr>
				<td class="textoCampo">Estado:</td>
	      		<td  class="textoDato" colspan="3">
					<@s.property default="&nbsp;" escape=false value="ajusteExistencia.estado"/></td>
				</td>
			</tr>
			<tr><td colspan="4" class="textoCampo">&nbsp;</td></tr>
		</table>		
		
	</div> 
		
	<@tiles.insertAttribute name="productos"/>	
				
			
 
<@vc.htmlContent 
  baseUrl="${request.contextPath}/deposito/ajusteExistencia/updateAjusteExistenciaView.action" 
  source="modificarAjusteExistencia" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="ajusteExistencia.oid={oid},navigationId=ajusteExistencia-actualizar"/>



