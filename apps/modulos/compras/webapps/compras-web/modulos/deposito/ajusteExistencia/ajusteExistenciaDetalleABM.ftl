<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<@s.hidden id="webFlow" name="webFlow"/>
<@s.hidden id="viewState" name="viewState"/>
<@s.hidden id="ajusteExistenciaDetalle.ajusteExistencia.oid" name="ajusteExistenciaDetalle.ajusteExistencia.oid"/>
<@s.hidden id="ajusteExistenciaDetalle.oid" name="ajusteExistenciaDetalle.oid"/>
<@s.hidden id="ajusteExistenciaDetalle.producto.oid" name="ajusteExistenciaDetalle.producto.oid"/>

<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
			<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos del Ajuste de Existencia</b></td>
			</tr>
		</table>
	
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>
			<tr>
  				<td class="textoCampo">N&uacute;mero:</td>
  				<td class="textoDato">
  					<@s.property default="&nbsp;" escape=false value="ajusteExistenciaDetalle.ajusteExistencia.numero"/>
  				</td>
  				<td  class="textoCampo">Fecha: </td>
				<td class="textoDato">
					<@s.if test="ajusteExistenciaDetalle.ajusteExistencia.fecha != null">
						<#assign fechaAjuste = ajusteExistenciaDetalle.ajusteExistencia.fecha> 
						${fechaAjuste?string("dd/MM/yyyy")}	
					</@s.if> &nbsp;   			
			</tr>	
			<tr>
	  			<td class="textoCampo">Dep&oacute;sito:</td>
	      			<td class="textoDato">
	      			<@s.property default="&nbsp;" escape=false value="ajusteExistenciaDetalle.ajusteExistencia.deposito.descripcion"/>	                         
	      		</td>		      		
				<td class="textoCampo">Motivo:</td>
	  			<td class="textoDato">
	  				<@s.property default="&nbsp;" escape=false value="ajusteExistenciaDetalle.ajusteExistencia.motivo"/>
				</td>
			</tr>
	    	<tr>
				<td class="textoCampo">Observaciones:</td>
      			<td  class="textoDato" colspan="3">
      			<@s.property default="&nbsp;" escape=false value="ajusteExistenciaDetalle.ajusteExistencia.observaciones"/>
				</td>					
    		</tr>
			<tr>
				<td class="textoCampo">Estado:</td>
      			<td  class="textoDato" colspan="3">
					<@s.property default="&nbsp;" escape=false value="ajusteExistenciaDetalle.ajusteExistencia.estado"/></td>
				</td>
    		</tr>
    		<tr><td colspan="4" class="textoCampo">&nbsp;</td></tr>
		</table>												
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos del Producto</b></td>
			</tr>
		</table>
	
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>					
			<tr>
      			<td class="textoCampo">C&oacute;digo:</td>
      			<td class="textoDato">
      				<@s.property default="&nbsp;" escape=false value="ajusteExistenciaDetalle.producto.oid"/>
      			</td>
      			<td  class="textoCampo">Descripci&oacute;n: </td>
				<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="ajusteExistenciaDetalle.producto.descripcion"/>					
				</td>	      			
			</tr>
			
			<tr>
				<td class="textoCampo">Marca: </td>
				<td  class="textoDato">
					<@s.property default="&nbsp;" escape=false value="ajusteExistenciaDetalle.producto.marca"/>	
				</td>
				<td class="textoCampo">Modelo: </td>
				<td  class="textoDato">
					<@s.property default="&nbsp;" escape=false value="ajusteExistenciaDetalle.producto.modelo"/>
				</td>
    		</tr>
			<tr>
				<td class="textoCampo">Rubro: </td>									
				<td  class="textoDato">
					<@s.property default="&nbsp;" escape=false value="ajusteExistenciaDetalle.producto.rubro.descripcion"/></td>
				</td>
				<td class="textoCampo">Es Cr&iacute;tico: </td>
				<td  class="textoDato">
					<@s.property default="&nbsp;" escape=false value="ajusteExistenciaDetalle.producto.critico"/>						      			
				</td>
    		</tr>
			<tr>
				<td class="textoCampo">Reposici&oacute;n Autm&aacute;tica: </td>
				<td  class="textoDato">
					<@s.property default="&nbsp;" escape=false value="ajusteExistenciaDetalle.producto.reposicionAutomatica"/>						      			
				</td>
				<td class="textoCampo">Existencia M&iacute;nima: </td>
				<td  class="textoDato">
				<@s.textfield 
					templateDir="custontemplates"
					template="text" 
					id="ajusteExistenciaDetalle.producto.existenciaMinima" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="ajusteExistenciaDetalle.producto.existenciaMinima"
					title="Existencia M&iacute;nima" />									
				</td>
    		</tr>
	    	<tr>
				<td class="textoCampo">C&oacute;digo de barras: </td>
				<td  class="textoDato">
					<@s.property default="&nbsp;" escape=false value="ajusteExistenciaDetalle.producto.codigoBarra"/>						      			
				</td>					    		
	    		<td class="textoCampo">Tipo Unidad:</td>
	    		<td  class="textoDato">
						<@s.property default="&nbsp;" escape=false value="ajusteExistenciaDetalle.producto.tipoUnidad.codigo"/>
				</td>
    		</tr>
			<tr>
      			<td class="textoCampo">Deposito:</td>
      			<td class="textoDato">
      				<@s.property default="&nbsp;" escape=false value="ajusteExistenciaDetalle.ajusteExistencia.deposito.descripcion"/></td>							
				</td>	      			
			
				<td class="textoCampo">Existencia: </td>
				<td  class="textoDato">					
				<@s.textfield 
					templateDir="custontemplates"
					template="textMoney"   
					id="existencia.cantidad" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="existencia.cantidad"
					title="Existencia" />									
				</td>									
    		</tr>
			<tr>
				<td class="textoCampo">Tipo Ajuste: </td>									
				<td  class="textoDato">
					<@s.select 
							templateDir="custontemplates" 
							id="ajusteExistencia.tipoAjuste" 
							cssClass="textarea"
							cssStyle="width:160px" 
							name="ajusteExistenciaDetalle.tipoAjuste" 
							list="tipoAjusteExistenciaDetalleList" 
							listKey="key" 
							listValue="description" 
							value="ajusteExistenciaDetalle.tipoAjuste.ordinal()"
							headerKey="0"
							headerValue="Seleccionar" 
							title="Tipo Ajuste" />
				</td>
				<td class="textoCampo">Cantidad ajustada: </td>
				<td  class="textoDato">
					<@s.textfield									
						templateDir="custontemplates"
						template="textMoney"  
						id="ajusteExistenciaDetalle.cantidadAjustada" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="ajusteExistenciaDetalle.cantidadAjustada" 
						title="Cantidad Ajustada" />							      			
				</td>
    		</tr>
	    	<tr><td colspan="4" class="textoCampo">&nbsp;</td></tr>
    	</table>			    	
	</div>
				    	
	<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
		<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
			<tr>
				<td>Ubicaciones del Producto</td>
			</tr>
		</table>
		
		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="ajusteExistenciaDetalle.producto.productoDepositoUbicacionList" id="productoDepositoUbicacion" decorator="ar.com.riouruguay.web.actions.maestro.productobien.decorators.DepositoUbicacionDecorator">
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="depositoUbicacion.deposito.descripcion" title="Dep&oacute;sito" />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="depositoUbicacion.deposito.tipoOrganizacion" title="Tipo Organizaci&oacute;n" />						
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="organizacion" title="Tipo Organizaci&oacute;n" />
		</@display.table>
	</div>

