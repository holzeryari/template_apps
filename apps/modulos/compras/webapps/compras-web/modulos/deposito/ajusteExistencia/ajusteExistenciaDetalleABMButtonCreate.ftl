<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<div id="capaSeparadora" class="capaSeparadora">&nbsp;</div>
<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancelar" type="button" name="btnVolver" value="Cancelar" class="boton"/>
				<input id="store" type="button" name="btnAgregar" value="Agregar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>


<@vc.htmlContent 
  baseUrl="${request.contextPath}/deposito/ajusteExistencia/createAjusteExistenciaDetalle.action" 
  source="store" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="ajusteExistenciaDetalle.ajusteExistencia.oid={ajusteExistenciaDetalle.ajusteExistencia.oid},ajusteExistenciaDetalle.tipoAjuste={ajusteExistenciaDetalle.tipoAjuste},ajusteExistenciaDetalle.cantidadAjustada={ajusteExistenciaDetalle.cantidadAjustada},ajusteExistenciaDetalle.producto.oid={ajusteExistenciaDetalle.producto.oid}"/>
  

<@vc.htmlContent 
    baseUrl="${request.contextPath}/compras/flowControl.action"  
  source="cancelar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=ajusteExistencia-administrar,flowControl=back"/>
