<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<div id="capaSeparadora" class="capaSeparadora">&nbsp;</div>
<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancelar" type="button" name="btnVolver" value="Cancelar" class="boton"/>
				<input id="modificar" type="button" name="btnModificar" value="Modificar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/deposito/ajusteExistencia/updateAjusteExistenciaDetalle.action" 
  source="modificar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="ajusteExistenciaDetalle.oid={ajusteExistenciaDetalle.oid},ajusteExistenciaDetalle.tipoAjuste={ajusteExistenciaDetalle.tipoAjuste},ajusteExistenciaDetalle.cantidadAjustada={ajusteExistenciaDetalle.cantidadAjustada}"/>
  
<@vc.htmlContent 
    baseUrl="${request.contextPath}/compras/flowControl.action"  
  source="cancelar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=ajusteExistencia-administrar,flowControl=back"/>
