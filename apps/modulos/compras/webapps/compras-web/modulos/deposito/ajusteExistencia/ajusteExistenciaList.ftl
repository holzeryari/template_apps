<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>

<@s.hidden id="navigationId" name="navigationId"/>

	<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
        	<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
			<tr>
				<td><b>Datos del Ajuste de Existencia</b></td>
				<td>
					<div align="right">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0146" 
							enabled="true" 
							cssClass="item" 
							id="crear"										
							href="javascript://nop/">
							<b>Agregar</b>										
							<img src="${request.contextPath}/common/images/agregar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
						</@security.a>						
					</div>
				</td>
			</tr>
		</table>
		
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
	    		<td class="textoCampo" colspan="4">&nbsp;</td>
	    	</tr>
			<tr>
				<td class="textoCampo">N&uacute;mero:</td>
	      		<td class="textoDato">
	      			<@s.textfield 
      					templateDir="custontemplates" 
						id="ajusteExistencia.numero" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="ajusteExistencia.numero" 
						title="N&uacute;mero" />
				</td>							
	      		<td class="textoCampo">Dep&oacute;sito</td>
	      		<td class="textoDato">
					<@s.select 
						templateDir="custontemplates" 
						id="ajusteExistencia.deposito.oid" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="ajusteExistencia.deposito.oid" 
						list="depositoList" 
						listKey="oid" 
						listValue="descripcion" 
						value="ajusteExistencia.deposito.oid"
						title="Dep&oacute;sito"
						headerKey="" 
                        headerValue="Todos" />
	            </td>
			</tr>
			<tr>
				<td class="textoCampo">Motivo:</td>
	      		<td class="textoDato">
					<@s.select 
						templateDir="custontemplates" 
						id="motivo" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="motivo" 
						list="motivoAjusteExistenciaList" 
						listKey="key" 
						listValue="description" 
						value="ajusteExistencia.motivo.ordinal()"
						title="Motivo"
						headerKey="0" 
	                    headerValue="Todos"
						 />
				</td>
				<td class="textoCampo">Estado:</td>
	      		<td class="textoDato">
	      			<@s.select 
						templateDir="custontemplates" 
						id="estado" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="estado" 
						list="estadoList" 
						listKey="key" 
						listValue="description" 
						value="ajusteExistencia.estado.ordinal()"
						title="Estado"
						headerKey="0"
						headerValue="Todos"							
						/>
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Fecha Desde:</td>
	      		<td class="textoDato">
					<@vc.rowCalendar 
						templateDir="custontemplates" 
						id="fechaDesde" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="fechaDesde" 
						title="Fecha Desde" />
				</td>
				<td class="textoCampo">Fecha Hasta:</td>
	      		<td class="textoDato">
					<@vc.rowCalendar 
						templateDir="custontemplates" 
						id="fechaHasta" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="fechaHasta" 
						title="Fecha Hasta" />
				</td>
			</tr>
			<tr>
	    		<td colspan="4" class="lineaGris" align="right">
	      			<input id="limpiar" type="button" name="btnVolver" value="Limpiar" class="boton"/>		
	      			<input id="buscar" type="button" name="btnVolver" value="Buscar" class="boton"/>
	    		</td>
			</tr>	
		</table>
	</div>
			
	<!-- Resultado Filtro -->
	<@s.if test="ajusteExistenciaList!=null">
		<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
			<#assign ajusteExistenciaNumero = "0" />
			<@s.if test="ajusteExistencia.numero != null">
				<#assign ajusteExistenciaNumero = "${ajusteExistencia.numero}" />
			</@s.if>
			<#assign ajusteExistenciaDeposito = "0" />
			<@s.if test="ajusteExistencia.deposito != null && ajusteExistencia.deposito.oid != null">
				<#assign ajusteExistenciaDeposito = "${ajusteExistencia.deposito.oid}" />
			</@s.if>
			<#assign ajusteExistenciaMotivo = "${ajusteExistencia.motivo.ordinal()}" />
			<#assign ajusteExistenciaEstado = "${ajusteExistencia.estado.ordinal()}" />

			<#assign ajusteExistenciaFechaDesde = "" />
			<@s.if test="fechaDesde != null">
				<#assign ajusteExistenciaFechaDesde = "${fechaDesde?string('dd/MM/yyyy')}" />
			</@s.if>

			<#assign ajusteExistenciaFechaHasta = "" />
			<@s.if test="fechaHasta != null">
				<#assign ajusteExistenciaFechaHasta = "${fechaHasta?string('dd/MM/yyyy')}" />
			</@s.if>
				
			<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>Ajustes de Existencias encontrados</td>
					<td>
						<div class="alineacionDerecha">
							<@security.a 
								templateDir="custontemplates" 
								securityCode="CUF0145" 
								cssClass="item" 
								href="${request.contextPath}/deposito/ajusteExistencia/printXLS.action?ajusteExistencia.numero=${ajusteExistenciaNumero}&ajusteExistencia.deposito.oid=${ajusteExistenciaDeposito}&ajusteExistencia.motivo=${ajusteExistenciaMotivo}&ajusteExistencia.estado=${ajusteExistenciaEstado}&fechaDesde=${ajusteExistenciaFechaDesde}&fechaHasta=${ajusteExistenciaFechaHasta}">
								<img src="${request.contextPath}/common/images/imprimirExcel.gif" title="Exportar a Excel" align="absmiddle" border="0" hspace="3" >
							</@security.a>	
						</div>
						<div class="alineacionDerecha">
							<b>Imprimir</b>
							<@security.a 
								templateDir="custontemplates" 
								securityCode="CUF0145" 
								cssClass="item" 
								href="${request.contextPath}/deposito/ajusteExistencia/printPDF.action?ajusteExistencia.numero=${ajusteExistenciaNumero}&ajusteExistencia.deposito.oid=${ajusteExistenciaDeposito}&ajusteExistencia.motivo=${ajusteExistenciaMotivo}&ajusteExistencia.estado=${ajusteExistenciaEstado}&fechaDesde=${ajusteExistenciaFechaDesde}&fechaHasta=${ajusteExistenciaFechaHasta}">
								<img src="${request.contextPath}/common/images/imprimirPdf.gif" title="Exportar a PDF" align="absmiddle" border="0" hspace="3" >
							</@security.a>
						</div>	
					</td>
				</tr>
			</table>

			<@vc.anchors target="contentTrx">			

          		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="ajusteExistenciaList" id="ajusteExistencia" pagesize=15 defaultsort=2 partialList=true size="recordSize" keepStatus=true excludedParams="resetFlag">
          			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select" class="botoneraAnchoCon4" title="Acciones">

						<div class="alineacion">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0141" 
							enabled="ajusteExistencia.readable" 
							cssClass="item" 
							href="${request.contextPath}/deposito/ajusteExistencia/readAjusteExistenciaView.action?ajusteExistencia.oid=${ajusteExistencia.oid?c}&navigationId=ajusteExistencia-visualizar&flowControl=regis&navegacionIdBack=${navigationId}">
							<img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver"  border="0">
						</@security.a>
						</div>
						<div class="alineacion">	
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0142" 
							enabled="ajusteExistencia.updatable"
							cssClass="item"  
							href="${request.contextPath}/deposito/ajusteExistencia/administrarAjusteExistenciaView.action?ajusteExistencia.oid=${ajusteExistencia.oid?c}&navigationId=ajusteExistencia-administrar&flowControl=regis">
							<img  src="${request.contextPath}/common/images/administrar.gif" alt="Administrar" title="Administrar"  border="0">
						</@security.a>
						</div>
						<div class="alineacion">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0143" 
							enabled="ajusteExistencia.eraseable"
							cssClass="item"  
							href="${request.contextPath}/deposito/ajusteExistencia/eliminarAjusteExistenciaView.action?ajusteExistencia.oid=${ajusteExistencia.oid?c}&navigationId=ajusteExistencia-eliminar&flowControl=regis">
							<img  src="${request.contextPath}/common/images/eliminar.gif" alt="Eliminar" title="Eliminar"  border="0">
						</@security.a>				
						</div>
						<div class="alineacion">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0144" 
							enabled="ajusteExistencia.readable" 
							cssClass="no-rewrite" 
							href="${request.contextPath}/deposito/ajusteExistencia/printAjusteExistenciaPDF.action?ajusteExistencia.oid=${ajusteExistencia.oid?c}">
							<img  src="${request.contextPath}/common/images/imprimir.gif" alt="Imprimir" title="Imprimir"  border="0">
						</@security.a>		
									
						</div>
					</@display.column>
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="numero" title="Nro." />				
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="deposito.descripcion" title="Dep&oacute;sito" />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="motivo" title="Motivo"/>
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="fecha" format="{0,date,dd/MM/yyyy}"  title="Fecha Ajuste" />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="estado" title="Estado" />		
				</@display.table>
			</@vc.anchors>
		</div>	
	</@s.if>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/deposito/ajusteExistencia/search.action"
  source="buscar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="resetFlag=true,navigationId={navigationId},flowControl=regis,ajusteExistencia.estado={estado},ajusteExistencia.motivo={motivo},ajusteExistencia.deposito.oid={ajusteExistencia.deposito.oid},ajusteExistencia.numero={ajusteExistencia.numero},fechaDesde={fechaDesde},fechaHasta={fechaHasta}"/>
  		      

<@vc.htmlContent 
  baseUrl="${request.contextPath}/deposito/ajusteExistencia/view.action"
  source="limpiar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=regis"/>
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/deposito/ajusteExistencia/createAjusteExistenciaView.action" 
  source="crear" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=ajusteExistencia-crear"/>
