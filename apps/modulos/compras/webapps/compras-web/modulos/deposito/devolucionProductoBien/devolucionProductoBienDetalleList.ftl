<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
<@vc.anchors target="contentTrx" ajaxFlag="ajax">
	<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
			<tr>
				<td>Productos/Bienes</td>
			</tr>
		</table>
		<!-- Resultado Filtro -->						
		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="devolucionProductoBien.devolucionProductoBienDetalleList" id="devolucionProductoBienDetalle" defaultsort=2>	
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select"  class="botoneraAnchoCon3" title="Acciones">
				<a href="${request.contextPath}/deposito/devolucionProductoBien/readDetalleView.action?devolucionProductoBienDetalle.oid=${devolucionProductoBienDetalle.oid?c}&navegacionIdBack=${navigationId}&navegacionIdBack=${navigationId}"><img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver" border="0"></a>
				&nbsp;
				<a href="${request.contextPath}/deposito/devolucionProductoBien/updateDetalleView.action?devolucionProductoBienDetalle.oid=${devolucionProductoBienDetalle.oid?c}&navegacionIdBack=${navigationId}&navegacionIdBack=${navigationId}"><img  src="${request.contextPath}/common/images/modificar.gif" alt="Modificar" title="Modificar" border="0"></a>
				&nbsp;
				<a href="${request.contextPath}/deposito/devolucionProductoBien/deletedetalleView.action?devolucionProductoBienDetalle.oid=${devolucionProductoBienDetalle.oid?c}&navegacionIdBack=${navigationId}&navegacionIdBack=${navigationId}"><img  src="${request.contextPath}/common/images/eliminar.gif" alt="Eliminar" title="Eliminar" border="0"></a>
			</@display.column>
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="numero" title="N&uacute;mero Item" />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="productoBien.oid" title="C&oacute;digo" />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="productoBien.tipo" title="Tipo" />	
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="productoBien.descripcion" title="Descripci&oacute;n" />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="productoBien.marca" title="Marca" />					
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="productoBien.modelo" title="Modelo" />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="cantidadRecibidaTotal" title="Cant. Recibida" />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="cantidadDevuelta" title="Cant. a Devolver"/>
		</@display.table>
</@vc.anchors>
</div>
<div id="capaSeparadora" class="capaSeparadora">&nbsp;</div>
