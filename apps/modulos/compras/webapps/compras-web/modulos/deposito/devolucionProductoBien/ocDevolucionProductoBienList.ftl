<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>


<@vc.anchors target="contentTrx" ajaxFlag="ajax">
<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
	<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>Ordenes de Compra</td>
					<td>
						<div align="right">
							<@s.a href="${request.contextPath}/deposito/devolucionProductoBien/selectOC.action?devolucionProductoBien.oid=${devolucionProductoBien.oid?c}&devolucionProductoBien.proveedor.nroProveedor=${devolucionProductoBien.proveedor.nroProveedor}&devolucionProductoBien.proveedor.detalleDePersona.nombre=${devolucionProductoBien.proveedor.detalleDePersona.nombre}&devolucionProductoBien.proveedor.detalleDePersona.razonSocial=${devolucionProductoBien.proveedor.detalleDePersona.razonSocial}&navigationId=${navigationId}" templateDir="custontemplates" id="agregarIngresoProductoDetalle" name="agregarIngresoProductoDetalle" cssClass="ocultarIcono">
								<b>Agregar</b><img src="${request.contextPath}/common/images/agregar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
							</@s.a>		
	
						</div>
					</td>
				</tr>
	</table>

	<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="devolucionProductoBien.OCList" id="ocos" defaultsort=2>	
							
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select"  class="botoneraAnchoCon2" title="Acciones">
			
			<a href="${request.contextPath}/compraContratacion/oc_os/readOC_OSView.action?oc_os.oid=${ocos.oid?c}&navigationId=oc_os-visualizar&flowControl=regis&navegacionIdBack=${navigationId}"><img  src="${request.contextPath}/common/images/ver.gif" alt="Eliminar" title="Eliminar" border="0"></a>
			&nbsp;						
			<a href="${request.contextPath}/deposito/devolucionProductoBien/deleteOCView.action?devolucionProductoBien.oid=${devolucionProductoBien.oid?c}&oc.oid=${ocos.oid?c}&navigationId=ocdevolucion-eliminar&flowControl=regis&navegacionIdBack=${navigationId}"><img  src="${request.contextPath}/common/images/eliminar.gif" alt="Eliminar" title="Eliminar" border="0"></a>
																		
		</@display.column>
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="numero" title="N&uacute;mero" />
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="fecha" format="{0,date,dd/MM/yyyy}"  title="Fecha" />	
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="importe" title="Importe" />
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="estado" title="Estado" />
					
	</@display.table>
	</div>
	</@vc.anchors>
<div id="capaSeparadora" class="capaSeparadora">&nbsp;</div>