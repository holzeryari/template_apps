<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign tiles=JspTaglibs["/WEB-INF/tlds/tiles-jsp.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<@s.hidden id="navigationId" name="navigationId"/>
<@s.hidden id="navegacionIdBack" name="navegacionIdBack"/>

<@s.hidden id="egresoProducto.oid" name="egresoProducto.oid"/>

<@s.hidden id="egresoProductoEstado" name="egresoProducto.estado.ordinal()"/>
<@s.hidden id="egresoProductoTipoDestino" name="egresoProducto.tipoDestino.ordinal()"/>
<@s.hidden id="egresoProductoClienteDestino.oid" name="egresoProducto.clienteDestino.oid"/>
<#if  egresoProducto?exists && egresoProducto.depositoOrigen?exists && egresoProducto.depositoOrigen.oid?exists>
<@s.hidden id="egresoProductoDepositoOrigen.oid" name="egresoProducto.depositoOrigen.oid"/>
</#if>
<@s.hidden id="egresoProducto.versionNumber" name="egresoProducto.versionNumber"/>

<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
			<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos del Egreso de Producto</b></td>
				<td>
					<div align="right">
						<@s.a templateDir="custontemplates" id="modificarEgresoProducto" name="modificarEgresoProducto" href="javascript://nop/" cssClass="ocultarIcono">
							<b>Modificar</b><img src="${request.contextPath}/common/images/modificar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
						</@s.a>
					</div>
				</td>
			</tr>
		</table>
		
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>	
			<tr>
      			<td class="textoCampo">N&uacute;mero:</td>
      			<td class="textoDato">
      				<@s.property default="&nbsp;" escape=false value="egresoProducto.numero"/>
      			</td>
      			<td  class="textoCampo">Fecha: </td>
				<td class="textoDato">
					<@s.if test="egresoProducto.fecha != null">
					<#assign fechaEgreso = egresoProducto.fecha> 
					${fechaEgreso?string("dd/MM/yyyy")}	
					</@s.if>	&nbsp;								
				</td>	      			
			</tr>	
			<#assign clienteClass="textareagris">
			<#assign depositoEnable="true">
			<#assign depositoClass="textareagris">
			<#assign classLink="ocultarIcono">
  			<@s.if test="egresoProducto.tipoDestino.ordinal() == 1">									
				<#assign depositoEnable="false">
				<#assign depositoClass="textarea">									
			</@s.if>
			<@s.if test="egresoProducto.tipoDestino.ordinal() == 2">									
				<#assign clienteClass="textarea">		
				<#assign classLink="ocultarIcono">																		
			</@s.if>
			<#assign classLinkFormulario="ocultarIcono">
  			
  			<tr>
      			<td class="textoCampo">Dep&oacute;sito Origen:</td>
	      		<td class="textoDato">		      						
					<@s.select 
							templateDir="custontemplates" 
							id="egresoProducto.depositoOrigen.oid" 
							cssClass="textarea"
							cssStyle="width:165px" 
							name="egresoProducto.depositoOrigen.oid" 
							list="depositoOrigenList" 
							listKey="oid" 
							listValue="descripcion" 
							value="oid"
							title="Deposito"
							headerKey="0" 
                            headerValue="Seleccionar"
                            value="egresoProducto.depositoOrigen.oid"/>
                </td>		      		
				<td class="textoCampo">Tipo Destino:</td>
      			<td class="textoDato">
					<@s.select 
						templateDir="custontemplates" 
						id="egresoProducto.tipoDestino" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="egresoProducto.tipoDestino" 
						list="tipoDestinoList" 
						listKey="key" 
						listValue="description" 
						value="egresoProducto.tipoDestino.ordinal()"
						onchange="javascript:tipoDestino(this);" 
						headerKey="0" 
                    	headerValue="Seleccionar" 
						title="Ripo Destino" />
				</td>
    		</tr>
	    	<tr>
      			<td class="textoCampo">Dep&oacute;sito Destino:</td>
	      		<td class="textoDato">
	      			<@s.select 
							templateDir="custontemplates" 
							id="egresoProducto.depositoDestino.oid" 
							cssClass="${depositoClass}"
							cssStyle="width:165px" 
							name="egresoProducto.depositoDestino.oid" 
							list="depositoDestinoList" 
							listKey="oid" 
							listValue="descripcion" 
							value="oid"
							title="Deposito"
							headerKey="0" 
                            headerValue="Seleccionar"
                            value="egresoProducto.depositoDestino.oid"
                            disabled="${depositoEnable}"	                            
                            />
                </td>		      		
				<td class="textoCampo">Cliente Destino:</td>
      			<td class="textoDato">

					<#assign clienteDestinoBusquedaStyle="display: none;">
					<@s.if test="egresoProducto.tipoDestino.ordinal() == 2">																		
						<#assign clienteDestinoBusquedaStyle="display: block;">
					</@s.if>

      				<@s.hidden id="egresoProducto.clienteDestino.descripcion" name="egresoProducto.clienteDestino.descripcion"/>

					<div id="clienteDestinoTexto" style="float:left;">
	      				<@s.property default="&nbsp;" escape=false value="egresoProducto.clienteDestino.descripcion"/>
					</div>
					<div id="clienteDestinoBusqueda" style="${clienteDestinoBusquedaStyle}">
						<@s.a templateDir="custontemplates" id="seleccionarCliente" name="seleccionarCliente" href="javascript://nop/" cssClass="${classLink}">
							<img src="${request.contextPath}/common/images/buscar.gif" align="top" border="0" hspace="3">
						</@s.a>
					</div>
				</td>	
			</tr>
    		<tr>				
				<td class="textoCampo">FSC:</td>
      			<td class="textoDato" colspan="3">

					<#assign fscBusquedaStyle="display: none;">
					<@s.if test="egresoProducto.tipoDestino.ordinal() == 2">																		
						<#assign fscBusquedaStyle="display: block;">
					</@s.if>

      				<@s.hidden id="egresoProducto.fsc_fss.oid" name="egresoProducto.fsc_fss.oid"/>
      				<@s.hidden id="egresoProducto.fsc_fss.oid" name="egresoProducto.fsc_fss.numero"/>

					<div id="fscTexto" style="float:left;">
	      				<@s.property default="&nbsp;" escape=false value="egresoProducto.fsc_fss.numero"/>
					</div>
					<div id="fscBusqueda" style="${fscBusquedaStyle}">
						<@s.a templateDir="custontemplates" id="seleccionarFormulario" name="seleccionarFormulario" href="javascript://nop/" cssClass="${classLinkFormulario}">
							<img src="${request.contextPath}/common/images/buscar.gif" align="top" border="0"  hspace="3" >
						</@s.a>
					</div>
				</td>	
			</tr>
    		<tr>
				<td class="textoCampo">Observaciones:</td>
      			<td  class="textoDato" colspan="3">
				<@s.textarea	
						templateDir="custontemplates" 						  
						cols="89" rows="4"	      					
						cssClass="textarea"
						id="egresoProducto.observaciones"							 
						name="egresoProducto.observaciones"  
						label="Observaciones"														
						 />
				</td>					
    		</tr>

    		<tr>
				<td class="textoCampo">Estado:</td>
      			<td  class="textoDato" colspan="3">
					<@s.property default="&nbsp;" escape=false value="egresoProducto.estado"/></td>
				</td>
    		</tr>
    		<tr><td colspan="4" class="textoCampo">&nbsp;</td></tr>
		</table>		
		
	</div>
		
	<@tiles.insertAttribute name="productos"/>		
			
		
<@vc.htmlContent 
  baseUrl="${request.contextPath}/deposito/egresoProducto/updateEgresoProductoView.action" 
  source="modificarEgresoProducto" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=egresoProductoUpdate,flowControl=regis,egresoProducto.oid={egresoProducto.oid}"/>



