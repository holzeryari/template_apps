<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancel" type="button" name="btnVolver" value="Cancelar" class="boton"/>
				<input id="store" type="button" name="btnAgregar" value="Agregar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/deposito/egresoProducto/createEgresoProducto.action" 
  source="store" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="egresoProducto.oid={egresoProducto.oid},egresoProducto.depositoOrigen.oid={egresoProducto.depositoOrigen.oid},egresoProducto.depositoDestino.oid={egresoProducto.depositoDestino.oid},egresoProducto.clienteDestino.oid={egresoProducto.clienteDestino.oid},egresoProducto.observaciones={egresoProducto.observaciones},egresoProducto.tipoDestino={egresoProducto.tipoDestino},egresoProducto.fsc_fss.numero={egresoProducto.fsc_fss.numero},egresoProducto.fsc_fss.oid={egresoProducto.fsc_fss.oid}"/>
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=buscar-egresoProducto,flowControl=back"/>
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/deposito/egresoProducto/selectClienteABM.action" 
  source="seleccionarCliente" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=change,egresoProducto.oid={egresoProducto.oid},egresoProducto.tipoDestino={egresoProducto.tipoDestino},egresoProducto.clienteDestino.oid={egresoProducto.clienteDestino.oid},egresoProducto.clienteDestino.descripcion={egresoProducto.clienteDestino.descripcion},egresoProducto.depositoDestino.oid={egresoProducto.depositoDestino.oid},egresoProducto.depositoOrigen.oid={egresoProducto.depositoOrigen.oid},egresoProducto.observaciones={egresoProducto.observaciones},egresoProducto.estado={egresoProductoEstado},egresoProducto.fsc_fss.numero={egresoProducto.fsc_fss.numero},egresoProducto.fsc_fss.oid={egresoProducto.fsc_fss.oid}"/>
  
  
  <@vc.htmlContent 
  baseUrl="${request.contextPath}/deposito/egresoProducto/selectFormulario.action" 
  source="seleccionarFormulario" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=change,egresoProducto.oid={egresoProducto.oid},egresoProducto.tipoDestino={egresoProducto.tipoDestino},egresoProducto.clienteDestino.oid={egresoProducto.clienteDestino.oid},egresoProducto.clienteDestino.descripcion={egresoProducto.clienteDestino.descripcion},egresoProducto.depositoDestino.oid={egresoProducto.depositoDestino.oid},egresoProducto.depositoOrigen.oid={egresoProducto.depositoOrigen.oid},egresoProducto.observaciones={egresoProducto.observaciones},egresoProducto.estado={egresoProductoEstado},egresoProducto.fsc_fss.numero={egresoProducto.fsc_fss.numero},egresoProducto.fsc_fss.oid={egresoProducto.fsc_fss.oid}"/>
  