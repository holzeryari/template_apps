<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<div id="capaSeparadora" class="capaSeparadora">&nbsp;</div>
<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancelar" type="button" name="btnVolver" value="Cancelar" class="boton"/>
				<input id="store" type="button" name="btnAgregar" value="Egresar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/deposito/egresoProducto/realizarEgresoProducto.action" 
  source="store" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="egresoProducto.oid={egresoProducto.oid}"/>
  
  <@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancelar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=buscar-egresoProducto,flowControl=back"/>