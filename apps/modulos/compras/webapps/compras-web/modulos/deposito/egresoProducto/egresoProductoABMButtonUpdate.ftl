<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>


<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancelar" type="button" name="btnVolver" value="Cancelar" class="boton"/>
				<input id="update" type="button" name="btnModificar" value="Modificar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>


<@vc.htmlContent 
  baseUrl="${request.contextPath}/deposito/egresoProducto/updateEgresoProducto.action" 
  source="update" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="egresoProducto.oid={egresoProducto.oid},egresoProducto.tipoDestino={egresoProductoTipoDestino},egresoProducto.clienteDestino.oid={egresoProducto.clienteDestino.oid},egresoProducto.clienteDestino.descripcion={egresoProducto.clienteDestino.descripcion},egresoProducto.depositoDestino.oid={egresoProducto.depositoDestino.oid},egresoProducto.depositoOrigen.oid={egresoProductoDepositoOrigen.oid},egresoProducto.observaciones={egresoProducto.observaciones}"/>
  

<@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancelar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=egresoProducto-administrar,flowControl=back"/>
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/deposito/egresoProducto/selectClienteABMUpdate.action" 
  source="seleccionarCliente" 
  success="contentTrx" 
  failure="errorTrx" 
 parameters="navegacionIdBack={navegacionIdBack},navigationId={navigationId},flowControl=change,egresoProducto.oid={egresoProducto.oid},egresoProducto.tipoDestino={egresoProductoTipoDestino},egresoProducto.clienteDestino.oid={egresoProducto.clienteDestino.oid},egresoProducto.clienteDestino.descripcion={egresoProducto.clienteDestino.descripcion},egresoProducto.depositoDestino.oid={egresoProducto.depositoDestino.oid},egresoProducto.depositoOrigen.oid={egresoProductoDepositoOrigen.oid},egresoProducto.observaciones={egresoProducto.observaciones},egresoProducto.estado={egresoProductoEstado}"/>
  