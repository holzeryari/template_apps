<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<@s.hidden id="webFlow" name="webFlow"/>
<@s.hidden id="viewState" name="viewState"/>

<@s.hidden id="egresoProductoDetalle.egresoProducto.oid" name="egresoProductoDetalle.egresoProducto.oid"/>
<@s.hidden id="egresoProductoDetalle.oid" name="egresoProductoDetalle.oid"/>
<@s.hidden id="egresoProductoDetalle.producto.oid" name="egresoProductoDetalle.producto.oid"/>
<@s.hidden id="egresoProductoDetalle.cantidadEgresada" name="egresoProductoDetalle.cantidadEgresada"/>

<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
			<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="CONFIRMACION DE EGRESO DE PRODUCTO" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">

		
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>
				Esta seguro que desea egresar el producto
				
					<@s.property default="&nbsp;" escape=false value="egresoProductoDetalle.producto.descripcion"/>	
					
				  en cantidad 
				 
				 	<@s.property default="&nbsp;" escape=false value="egresoProductoDetalle.cantidadEgresada"/>
				 
				 
				 </b></td>
			</tr>
		</table>
	
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>
			<tr>
      			<td class="textoCampo">N&uacute;mero:</td>
      			<td class="textoDato">
      				<@s.property default="&nbsp;" escape=false value="egresoProductoDetalle.egresoProducto.numero"/></td>
      			<td  class="textoCampo">Fecha: </td>
				<td class="textoDato">
				<@s.if test="egresoProductoDetalle.egresoProducto.fecha != null">
					<#assign fechaAjuste = egresoProductoDetalle.egresoProducto.fecha> 
					${fechaAjuste?string("dd/MM/yyyy")}	
				</@s.if>&nbsp;
			</tr>	
			<tr>
      			<td class="textoCampo">Dep&oacute;sito Origen:</td>
	      		<td class="textoDato">
	      			<@s.property default="&nbsp;" escape=false value="egresoProductoDetalle.egresoProducto.depositoOrigen.descripcion"/>	                         
	      		</td>		      		
				<td class="textoCampo">Tipo Destino:</td>
      			<td class="textoDato">
      				<@s.property default="&nbsp;" escape=false value="egresoProductoDetalle.egresoProducto.tipoDestino"/>
				</td>
    		</tr>
    		<tr>
      			<td class="textoCampo">Dep&oacute;sito Destino:</td>
	      		<td class="textoDato">
	      			<@s.property default="&nbsp;" escape=false value="egresoProductoDetalle.egresoProducto.depositoDestino.descripcion"/>	                         
	      		</td>		      		
				<td class="textoCampo">Cliente Destino:</td>
      			<td class="textoDato">
      				<@s.property default="&nbsp;" escape=false value="egresoProductoDetalle.egresoProducto.clienteDestino.descripcion"/>
				</td>
    		</tr>
    		<tr>
				<td class="textoCampo">Observaciones:</td>
      			<td  class="textoDato" colspan="3">
      				<@s.property default="&nbsp;" escape=false value="egresoProductoDetalle.egresoProducto.observaciones"/>
      				&nbsp;					
				</td>					
    		</tr>
			<tr>
				<td class="textoCampo">Estado:</td>
      			<td  class="textoDato" colspan="3">
					<@s.property default="&nbsp;" escape=false value="egresoProductoDetalle.egresoProducto.estado"/></td>
				</td>
    		</tr>
    		<tr><td colspan="4" class="textoCampo">&nbsp;</td></tr>
		</table>			
	</div>								
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos del Producto a Egresar</b></td>
			</tr>
		</table>
	
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>		
			<tr>
      			<td class="textoCampo">C&oacute;digo:</td>
      			<td class="textoDato">
      				<@s.property default="&nbsp;" escape=false value="egresoProductoDetalle.producto.oid"/>
      			</td>
      			<td  class="textoCampo">Descripci&oacute;n:</td>
				<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="egresoProductoDetalle.producto.descripcion"/>					
				</td>	      			
			</tr>
			<tr>
				<td class="textoCampo">Marca: </td>
				<td  class="textoDato">
					<@s.property default="&nbsp;" escape=false value="egresoProductoDetalle.producto.marca"/>	
				</td>
				<td class="textoCampo">Modelo: </td>
				<td  class="textoDato">
					<@s.property default="&nbsp;" escape=false value="egresoProductoDetalle.producto.modelo"/>
				</td>
    		</tr>
			<tr>
				<td class="textoCampo">Rubro: </td>									
				<td  class="textoDato">
					<@s.property default="&nbsp;" escape=false value="egresoProductoDetalle.producto.rubro.descripcion"/>													
				</td>
				<td class="textoCampo">Es Cr&iacute;tico: </td>
				<td  class="textoDato">
					<@s.property default="&nbsp;" escape=false value="egresoProductoDetalle.producto.critico"/>						      			
				</td>
    		</tr>
	    	<tr>
	    		<td class="textoCampo">Tipo Unidad:</td>
	    		<td  class="textoDato" colspan="3">
					<@s.property default="&nbsp;" escape=false value="egresoProductoDetalle.producto.tipoUnidad.codigo"/>
				</td>
	    	</tr>
	    	<tr>
				<td class="textoCampo">Reposici&oacute;n Autm&aacute;tica: </td>
				<td  class="textoDato">
					<@s.property default="&nbsp;" escape=false value="egresoProductoDetalle.producto.reposicionAutomatica"/>						      			
				</td>
				<td class="textoCampo">Existencia M&iacute;nima: </td>
				<td  class="textoDato">
					<@s.textfield 
						templateDir="custontemplates"
						template="text" 
						id="egresoProductoDetalle.producto.existenciaMinima" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="egresoProductoDetalle.producto.existenciaMinima"
						title="Existencia M&iacute;nima" />									
				</td>
    		</tr>
	    	<tr>
				<td class="textoCampo">C&oacute;digo de barras: </td>
				<td  class="textoDato" colspan="3">
					<@s.property default="&nbsp;" escape=false value="egresoProductoDetalle.producto.codigoBarra"/>						      			
				</td>
    		</tr>
			<tr>
      			<td class="textoCampo">Deposito:</td>
      			<td class="textoDato">
      				<@s.property default="&nbsp;" escape=false value="egresoProductoDetalle.egresoProducto.depositoOrigen.descripcion"/>
      			</td>
 			
				<td class="textoCampo">Existencia: </td>
				<td  class="textoDato">					
				<@s.textfield 
					templateDir="custontemplates"
					template="textMoney"   
					id="existencia.cantidad" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="existencia.cantidad"
					title="Existencia" />									
																							
				</td>									
    		</tr>
			<tr>
				<td class="textoCampo">Cantidad a egresar: </td>
				<td class="textoDato" colspan="3">
				<@s.textfield									
						templateDir="custontemplates"
						template="textMoney"  
						id="egresoProductoDetalle.cantidadEgresada" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="egresoProductoDetalle.cantidadEgresada" 
						title="Cantidad Egresada" />
													      			
				</td>
    		</tr>
	    	<tr><td colspan="4" class="textoCampo">&nbsp;</td></tr>
    	</table>
	</div>
	<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
		<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
			<tr>
				<td>Ubicaciones del Producto</td>
			</tr>
		</table>
		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="egresoProductoDetalle.producto.productoDepositoUbicacionList" id="productoDepositoUbicacion" decorator="ar.com.riouruguay.web.actions.maestro.productobien.decorators.DepositoUbicacionDecorator">
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="depositoUbicacion.deposito.descripcion" title="Dep&oacute;sito" />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="depositoUbicacion.deposito.tipoOrganizacion" title="Tipo Organizaci&oacute;n" />						
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="organizacion" title="Tipo Organizaci&oacute;n" />
		</@display.table>
	</div>				    									
