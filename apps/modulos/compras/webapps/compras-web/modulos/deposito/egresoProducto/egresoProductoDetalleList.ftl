<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>


<@ajax.anchors target="contentTrx">
<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
		<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>Productos a Egresar</td>
					<td>	
						<div align="right">
							<#if !(egresoProducto.fsc_fss?exists)>			
								<@s.a href="${request.contextPath}/deposito/egresoProducto/selectProducto.action?egresoProducto.oid=${egresoProducto.oid?c}&egresoProducto.depositoOrigen.oid=${egresoProducto.depositoOrigen.oid?c}&egresoProducto.depositoOrigen.descripcion=${egresoProducto.depositoOrigen.descripcion}&navigationId=${navigationId}" templateDir="custontemplates" id="agregarEgresoProductoDetalle" name="agregarEgresoProducto" cssClass="ocultarIcono">
									<b>Agregar</b><img src="${request.contextPath}/common/images/agregar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
								</@s.a>		
							</#if>	
						</div>
					</td>	
				</tr>
		</table>	
	
		<!-- Resultado Filtro -->						
		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="egresoProducto.egresoProductoDetalleList" id="egresoProductoDetalle" defaultsort=2 decorator="ar.com.riouruguay.web.actions.deposito.egresoProducto.decorators.CantidadEgresadaDecorator">	
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select" class="botoneraAnchoCon3" title="Acciones">
				<a href="${request.contextPath}/deposito/egresoProducto/readEgresoProductoDetalleView.action?egresoProductoDetalle.oid=${egresoProductoDetalle.oid?c}&navegacionIdBack=${navigationId}"><img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver" border="0"></a>
				&nbsp;
				<a href="${request.contextPath}/deposito/egresoProducto/updateEgresoProductoDetalleView.action?egresoProductoDetalle.oid=${egresoProductoDetalle.oid?c}&navegacionIdBack=${navigationId}"><img  src="${request.contextPath}/common/images/modificar.gif" alt="Modificar" title="Modificar" border="0"></a>
				&nbsp;
				<a href="${request.contextPath}/deposito/egresoProducto/deleteEgresoProductoDetalleView.action?egresoProductoDetalle.oid=${egresoProductoDetalle.oid?c}&navegacionIdBack=${navigationId}"><img  src="${request.contextPath}/common/images/eliminar.gif" alt="Eliminar" title="Eliminar" border="0"></a>
			</@display.column>
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="numero" title="Nro. Item" defaultorder="ascending"/>
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="producto.oid" title="Cod." />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="producto.descripcion" title="Descripci&oacute;n" />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="producto.rubro.descripcion" title="Rubro" />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="producto.marca" title="Marca" />					
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="producto.modelo" title="Modelo" />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="cantidadEgresada" title="Cantidad a Egresar" />
		</@display.table>
	</div>
</@ajax.anchors>
