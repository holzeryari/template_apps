<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancelar" type="button" name="btnVolver" value="Cancelar" class="boton"/>
				<input id="modificar" type="button" name="btnAgregar" value="Agregar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/deposito/ingresoProducto/createIngresoProductoDetalle.action" 
  source="modificar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="ingresoProductoDetalle.ingresoProducto.oid={ingresoProductoDetalle.ingresoProducto.oid},ingresoProductoDetalle.cantidadIngresada={ingresoProductoDetalle.cantidadIngresada},ingresoProductoDetalle.producto.oid={ingresoProductoDetalle.producto.oid}"/>
  
<@vc.htmlContent 
    baseUrl="${request.contextPath}/compras/flowControl.action"  
  source="cancelar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=ingresoProducto-administrar,flowControl=back"/>
