
<#setting locale="es_AR">

<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>

<@s.hidden id="navigationId" name="navigationId"/>
<@s.hidden id="ingresoProducto.egresoProducto.oid" name="ingresoProducto.egresoProducto.oid"/>
<@s.hidden id="ingresoProducto.egresoProducto.numero" name="ingresoProducto.egresoProducto.numero"/>

<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
        	<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
			<tr>
				<td><b>Datos del Ingreso de Producto</b></td>
				<td>
					<div align="right">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0181" 
							enabled="ingresoProducto.readable" 
							cssClass="item" 
							id="crear"										
							href="javascript://nop/">
							<b>Agregar</b>										
							<img src="${request.contextPath}/common/images/agregar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
						</@security.a>						
					</div>
				</td>
			</tr>
		</table>

		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
	    		<td class="textoCampo" colspan="4">&nbsp;</td>
	    	</tr>	
			<tr>
				<td class="textoCampo">N&uacute;mero:</td>
	      		<td class="textoDato">
	      			<@s.textfield 
	      					templateDir="custontemplates" 
							id="ingresoProducto.numero" 
							cssClass="textarea"
							cssStyle="width:160px" 
							name="ingresoProducto.numero" 
							title="N&uacute;mero" />
				</td>	
				<td class="textoCampo">Dep&oacute;sito Ingreso:</td>
	      		<td class="textoDato">
							<@s.select 
								templateDir="custontemplates" 
								id="ingresoProducto.depositoIngreso.oid" 
								cssClass="textarea"
								cssStyle="width:165px" 
								name="ingresoProducto.depositoIngreso.oid" 
								list="depositoIngresoList" 
								listKey="oid" 
								listValue="descripcion" 
								value="ingresoProducto.depositoIngreso.oid"
								title="Deposito Ingreso"
								headerKey="" 
	                            headerValue="Todos" />
	            </td>						
	    	</tr>
			<tr>
				<td class="textoCampo">Egreso Asociado:</td>
	      		<td class="textoDato">
	      			<@s.property default="&nbsp;" escape=false value="ingresoProducto.egresoProducto.numero"/>
					<@s.a templateDir="custontemplates" id="seleccionarEgresoProducto" name="seleccionarEgresoProducto" href="javascript://nop/">
						<img src="${request.contextPath}/common/images/buscar.gif"  border="0" align="absmiddle" hspace="3" >
					</@s.a>		
						</td>				
				<td class="textoCampo">Estado:</td>
	      		<td class="textoDato" colspan="3">
	      			<@s.select 
						templateDir="custontemplates" 
						id="ingresoProducto.estado" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="ingresoProducto.estado" 
						list="estadoList" 
						listKey="key" 
						listValue="description" 
						value="ingresoProducto.estado.ordinal()"
						title="Estado"
						headerKey="0"
						headerValue="Todos"							
						/>
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Fecha Desde:</td>
	      		<td class="textoDato">
					<@vc.rowCalendar 
						templateDir="custontemplates" 
						id="fechaDesde" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="fechaDesde" 
						title="Fecha Desde" />
				</td>
				<td class="textoCampo">Fecha Hasta:</td>
	      		<td class="textoDato">
					<@vc.rowCalendar 
						templateDir="custontemplates" 
						id="fechaHasta" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="fechaHasta" 
						title="Fecha Hasta" />
				</td>
			</tr>
			<tr>
	    		<td colspan="4" class="lineaGris" align="right">
	      			<input id="limpiar" type="button" name="btnVolver" value="Limpiar" class="boton"/>		
	      			<input id="buscar" type="button" name="btnVolver" value="Buscar" class="boton"/>
	    		</td>
			</tr>	
		</table>
	</div>	
			
	<!-- Resultado Filtro -->
	<@s.if test="ingresoProductoList!=null">
		<#assign ingresoNumero = "0" />
		<@s.if test="ingresoProducto.numero != null">
			<#assign ingresoNumero = "${ingresoProducto.numero}" />
		</@s.if>

		<#assign ingresoEgresoAsociado = "0" />
		<@s.if test="ingresoProducto.egresoProducto != null && ingresoProducto.egresoProducto.oid != null">
			<#assign ingresoEgresoAsociado = "${ingresoProducto.egresoProducto.oid}" />
		</@s.if>
				
		<#assign ingresoDepositoIngreso = "0" />
		<@s.if test="ingresoProducto.depositoIngreso != null && ingresoProducto.depositoIngreso.oid != null">
			<#assign ingresoDepositoIngreso = "${ingresoProducto.depositoIngreso.oid}" />
		</@s.if>			

		<#assign ingresoEstado = "0" />
		<@s.if test="ingresoProducto.estado != null ">
			<#assign ingresoEstado = "${ingresoProducto.estado.ordinal()}" />
		</@s.if>

		<#assign ingresoFechaDesde = "" />
		<@s.if test="fechaDesde != null">
			<#assign ingresoFechaDesde = "${fechaDesde?string('dd/MM/yyyy')}" />
		</@s.if>

		<#assign ingresoFechaHasta = "" />
		<@s.if test="fechaHasta != null">
			<#assign ingresoFechaHasta = "${fechaHasta?string('dd/MM/yyyy')}" />
		</@s.if>
				
		<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
			<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>Ingresos de Productos encontrados</td>
					<td>
						<div class="alineacionDerecha">
							<@security.a 
								templateDir="custontemplates" 
								securityCode="CUF0182" 
								cssClass="item" 
								href="${request.contextPath}/deposito/ingresoProducto/printXLS.action?ingresoProducto.estado=${ingresoEstado}&ingresoProducto.egresoProducto.oid=${ingresoEgresoAsociado}&ingresoProducto.depositoIngreso.oid=${ingresoDepositoIngreso}&ingresoProducto.numero=${ingresoNumero}&fechaDesde=${ingresoFechaDesde}&fechaHasta=${ingresoFechaHasta}">
								<img src="${request.contextPath}/common/images/imprimirExcel.gif" title="Exportar a Excel" align="absmiddle" border="0" hspace="3">
							</@security.a>
						</div>
						<div class="alineacionDerecha">
							<b>Imprimir</b>
							<@security.a 
								templateDir="custontemplates" 
								securityCode="CUF0182" 
								cssClass="item" 
								href="${request.contextPath}/deposito/ingresoProducto/printPDF.action?ingresoProducto.estado=${ingresoEstado}&ingresoProducto.egresoProducto.oid=${ingresoEgresoAsociado}&ingresoProducto.depositoIngreso.oid=${ingresoDepositoIngreso}&ingresoProducto.numero=${ingresoNumero}&fechaDesde=${ingresoFechaDesde}&fechaHasta=${ingresoFechaHasta}">
								<img src="${request.contextPath}/common/images/imprimirPdf.gif" title="Exportar a PDF" align="absmiddle" border="0" hspace="3">
							</@security.a>
						</div>	
					</td>
				</tr>
			</table>
			<@vc.anchors target="contentTrx">			
        		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="ingresoProductoList" id="ingresoProducto" pagesize=15  defaultsort=2 partialList=true size="recordSize" keepStatus=true excludedParams="resetFlag">	
        			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select" class="botoneraAnchoCon4" title="Acciones">
						<div class="alineacion">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0183" 
							enabled="ingresoProducto.readable" 
							cssClass="item" 
							href="${request.contextPath}/deposito/ingresoProducto/readIngresoProductoView.action?ingresoProducto.oid=${ingresoProducto.oid?c}&navigationId=ingresoProducto-visualizar&flowControl=regis&navegacionIdBack=${navigationId}">
							<img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver"  border="0">
						</@security.a>
						</div>
						<div class="alineacion">	
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0184" 
							enabled="ingresoProducto.updatable"
							cssClass="item"  
							href="${request.contextPath}/deposito/ingresoProducto/administrarIngresoProductoView.action?ingresoProducto.oid=${ingresoProducto.oid?c}&navigationId=ingresoProducto-administrar&flowControl=regis">
							<img  src="${request.contextPath}/common/images/administrar.gif" alt="Administrar" title="Administrar"  border="0">
						</@security.a>
						</div>
						<div class="alineacion">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0185" 
							enabled="ingresoProducto.eraseable"
							cssClass="item"  
							href="${request.contextPath}/deposito/ingresoProducto/eliminarIngresoProductoView.action?ingresoProducto.oid=${ingresoProducto.oid?c}&navigationId=ingresoProducto-eliminar&flowControl=regis">
							<img  src="${request.contextPath}/common/images/eliminar.gif" alt="Eliminar" title="Eliminar"  border="0">
						</@security.a>				
						</div>
						<div class="alineacion">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0186" 
							enabled="ingresoProducto.readable" 
							cssClass="no-rewrite"
							href="${request.contextPath}/deposito/ingresoProducto/printIngresoProductoPDF.action?ingresoProducto.oid=${ingresoProducto.oid?c}">
							<img  src="${request.contextPath}/common/images/imprimir.gif" alt="Imprimir" title="Imprimir"  border="0">
						</@security.a>		
									
						</div>
					</@display.column>


					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="numero" title="N&uacute;mero" />				
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="fecha" format="{0,date,dd/MM/yyyy}" title="Fecha Ingreso" />
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="depositoIngreso.descripcion" title="Dep&oacute;sito Ingreso" />							
								
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="egresoProducto.numero" title="Egreso Asociado" />

					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="estado" title="Estado" />		

				</@display.table>
			</@vc.anchors>
			</div>
			</@s.if>
			
	
<@vc.htmlContent 
  baseUrl="${request.contextPath}/deposito/ingresoProducto/search.action"
  source="buscar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="resetFlag=true,navigationId={navigationId},flowControl=regis,ingresoProducto.estado={ingresoProducto.estado},ingresoProducto.egresoProducto.oid={ingresoProducto.egresoProducto.oid},ingresoProducto.egresoProducto.numero={ingresoProducto.egresoProducto.numero},ingresoProducto.depositoIngreso.oid={ingresoProducto.depositoIngreso.oid},ingresoProducto.numero={ingresoProducto.numero},fechaDesde={fechaDesde},fechaHasta={fechaHasta},egresoProducto.oid={ingresoProducto.egresoProducto.oid},egresoProducto.numero={ingresoProducto.egresoProducto.numero}"/>
  		      

<@vc.htmlContent 
  baseUrl="${request.contextPath}/deposito/ingresoProducto/view.action"
  source="limpiar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=regis"/>
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/deposito/ingresoProducto/createIngresoProductoView.action" 
  source="crear" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=abm-ingresoProducto-create,flowControl=regis"/>
  
  <@vc.htmlContent 
  baseUrl="${request.contextPath}/deposito/ingresoProducto/selectEgresoProductoSearch.action" 
  source="seleccionarEgresoProducto" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=change,ingresoProducto.oid={ingresoProducto.oid},ingresoProducto.estado={ingresoProducto.estado},ingresoProducto.numero={ingresoProducto.numero},ingresoProducto.egresoProducto.oid={ingresoProducto.egresoProducto.oid},ingresoProducto.egresoProducto.numero={ingresoProducto.egresoProducto.numero},ingresoProducto.depositoIngreso.oid={ingresoProducto.depositoIngreso.oid},fechaDesde={fechaDesde},fechaHasta={fechaHasta}"/>
  
