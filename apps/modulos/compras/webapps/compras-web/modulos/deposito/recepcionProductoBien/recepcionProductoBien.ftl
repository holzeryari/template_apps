<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

	<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
		<tr>
        	<td class="textoCampo" colspan="4">&nbsp;</td>
        </tr>	
		<tr>
  			<td class="textoCampo">N&uacute;mero:</td>
  			<td class="textoDato">
  				<@s.property default="&nbsp;" escape=false value="recepcionProductoBien.numero"/></td>
  			<td class="textoCampo">Fecha: </td>
			<td class="textoDato">
				<@s.if test="recepcionProductoBien.fechaComprobante != null">
					<#assign fechaComprobante = recepcionProductoBien.fechaComprobante> 
					${fechaComprobante?string("dd/MM/yyyy")}	
				</@s.if>&nbsp;					
			</td>	      			
		</tr>	
		<tr>
      		<td class="textoCampo">Proveedor:</td>
			<td class="textoDato">
				<@s.property default="&nbsp;" escape=false value="recepcionProductoBien.proveedor.detalleDePersona.razonSocial" />
				<@s.property default="&nbsp;" escape=false value="recepcionProductoBien.proveedor.detalleDePersona.nombre" />
				<@s.a templateDir="custontemplates" id="seleccionarProveedor" name="seleccionarProveedor" href="javascript://nop/" cssClass="ocultarIcono">
					<img src="${request.contextPath}/common/images/buscar.gif"  border="0" align="absmiddle" hspace="3" >
				</@s.a>	
			</td>	
			<td class="textoCampo">Fecha Recepci&oacute;n:</td>
	      	<td class="textoDato" align="left" >
				<@vc.rowCalendar 
					templateDir="custontemplates" 
					id="recepcionProductoBien.fechaRecepcion" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="recepcionProductoBien.fechaRecepcion" 
					title="Fecha Recepcion" />
			</td>
	    </tr>
	    <tr>
			<td class="textoCampo">Dep&oacute;sito:</td>
	      	<td class="textoDato">
				<@s.select 
					templateDir="custontemplates" 
					id="recepcionProductoBien.deposito.oid" 
					cssClass="textarea"
					cssStyle="width:165px" 
					name="recepcionProductoBien.deposito.oid" 
					list="depositoList" 
					listKey="oid" 
					listValue="descripcion" 
					value="recepcionProductoBien.deposito.oid"
					title="Dep&oacute;sito"
					headerKey="" 
	                headerValue="Todos" />
	     	</td>
			<td class="textoCampo">Nro. Factura:</td>
	      	<td class="textoDato">
	      		<@s.textfield 
	      			templateDir="custontemplates" 
					id="recepcionProductoBien.numeroFactura" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="recepcionProductoBien.numeroFactura" 
					title="N&uacute;mero Factura" />
			</td>	
		</tr>
		<tr>
			<td class="textoCampo">Nro. Remito:</td>
	      	<td class="textoDato"colspan="3" >
	      		<@s.textfield 
	      			templateDir="custontemplates" 
					id="recepcionProductoBien.numeroRemito" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="recepcionProductoBien.numeroRemito" 
					title="N&uacute;mero Remito" />
			</td>
		</tr>		
		<tr>
			<td class="textoCampo">Observaciones:</td>
	      	<td  class="textoDato" colspan="3">
					<@s.textarea	
						templateDir="custontemplates" 						  
						cols="89" rows="4"	      					
						cssClass="textarea"
						id="recepcionProductoBien.observaciones"							 
						name="recepcionProductoBien.observaciones"  
						label="Observaciones"														
						 />
			</td>					
	    </tr>
	    <tr>
			<td class="textoCampo">Estado:</td>
	      	<td  class="textoDato" colspan="3">
				<@s.property default="&nbsp;" escape=false value="recepcionProductoBien.estado"/></td>
			</td>
		</tr>
		<tr><td colspan="4" class="textoCampo">&nbsp;</td></tr>	    	
	</table>		
			

