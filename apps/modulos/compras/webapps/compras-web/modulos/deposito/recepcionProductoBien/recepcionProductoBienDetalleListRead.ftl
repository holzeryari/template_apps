<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<@vc.anchors target="contentTrx" ajaxFlag="ajax">

<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
	<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>Productos/Bienes</td>
				</tr>
	</table>
			
	<!-- Resultado Filtro -->						
	<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="recepcionProductoBien.recepcionProductoBienDetalleList" id="recepcionProductoBienDetalle" defaultsort=2>	
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select"  class="botoneraAnchoCon1" title="Acciones">
			<a href="${request.contextPath}/deposito/recepcionProductoBien/readDetalleView.action?recepcionProductoBienDetalle.oid=${recepcionProductoBienDetalle.oid?c}&navegacionIdBack=${navigationId}&navegacionIdBack=${navigationId}"><img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver" border="0"></a>&nbsp;
		</@display.column>
	
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="numero" title="N&uacute;mero Item" />
	
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="productoBien.oid" title="C&oacute;digo" />
	
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="productoBien.tipo" title="Tipo" />	
		
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="productoBien.descripcion" title="Descripci&oacute;n" />
	
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="productoBien.marca" title="Marca" />					
	
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="productoBien.modelo" title="Modelo" />
	
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="cantidadPedida" title="Cant. Solicitada" />
	
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero"property="cantidadPendiente" title="Cant. Pendiente" />
	
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="cantidadRecibida" title="Cant. a Recibir" />
		
	</@display.table>
	</div>
</@vc.anchors>
<div id="capaSeparadora" class="capaSeparadora">&nbsp;</div>
