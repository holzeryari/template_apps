<#setting locale="es_AR">

<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>

<@s.hidden id="navigationId" name="navigationId"/>
<@s.hidden id="carteleria.proveedor.nroProveedor" name="carteleria.proveedor.nroProveedor"/>
<@s.hidden id="carteleria.proveedor.detalleDePersona.razonSocial" name="carteleria.proveedor.detalleDePersona.razonSocial" />
<@s.hidden id="carteleria.proveedor.detalleDePersona.nombre" name="carteleria.proveedor.detalleDePersona.nombre" />

<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
        	<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
			<tr>
				<td><b>Datos del Cartel</b></td>
				<td>
					<div align="right">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0300" 
							enabled="carteleria.readable" 
							cssClass="item" 
							id="crear"	
							name="crear"									
							href="javascript://nop/">
							<b >Agregar</b>										
							<img src="${request.contextPath}/common/images/agregar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
						</@security.a>		
					</div>
				</td>
			</tr>
		</table>

		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
	    		<td class="textoCampo" colspan="4">&nbsp;</td>
	    	</tr>
			<tr>
				<td class="textoCampo">N&uacute;mero:</td>
      			<td class="textoDato">
      			<@s.textfield 
      					templateDir="custontemplates" 
						id="carteleria.oid" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="carteleria.oid" 
						title="N&uacute;mero" />
				</td>
					
				<td class="textoCampo">Descripci&oacute;n:</td>
      			<td class="textoDato">
      			<@s.textfield 
      					templateDir="custontemplates" 
						id="carteleria.descripcion" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="carteleria.descripcion" 
						title="Descripci&oacute;n" />
				</td>	
			</tr>
			<tr>
				<td class="textoCampo">N&uacute;mero Ruta:</td>
      			<td class="textoDato">
      			<@s.textfield 
      					templateDir="custontemplates" 
						id="carteleria.numeroRuta" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="carteleria.numeroRuta" 
						title="N&uacute;mero Ruta" />
				</td>
				
				<td class="textoCampo">Km Ruta:</td>
      			<td class="textoDato">
      			<@s.textfield 
      					templateDir="custontemplates" 
						id="carteleria.kmRuta" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="carteleria.kmRuta" 
						title="Km Ruta" />
				</td>	
			</tr>
			<tr>					
				<td class="textoCampo">Propiedad:</td>
      			<td class="textoDato">
	      			<@s.select 
						templateDir="custontemplates" 
						id="carteleria.propiedadCartel" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="carteleria.propiedadCartel" 
						list="propiedadCartelList" 
						listKey="key" 
						listValue="description" 
						value="carteleria.propiedadCartel.ordinal()"
						title="Propiedad Cartel"
						headerKey="0"
						headerValue="Todos"							
						/>
				</td>
				
				<td class="textoCampo">Proveedor:</td>
      			<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="carteleria.proveedor.detalleDePersona.razonSocial" /><@s.property default="&nbsp;" escape=false value="carteleria.proveedor.detalleDePersona.nombre" />
					<@s.a templateDir="custontemplates" id="seleccionarProveedor" name="seleccionarProveedor" href="javascript://nop/" cssClass="ocultarIcono">
						<img src="${request.contextPath}/common/images/buscar.gif" border="0" align="absmiddle" hspace="3" >
					</@s.a>
				</td>	
				
			</tr>

			<tr>
				<td class="textoCampo">Fecha Desde:</td>
      			<td class="textoDato">
					<@vc.rowCalendar 
					templateDir="custontemplates" 
					id="fechaDesde" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="fechaDesde" 
					title="Fecha Desde" />
				</td>
				<td class="textoCampo">Fecha Hasta:</td>
      			<td class="textoDato">
				<@vc.rowCalendar 
					templateDir="custontemplates" 
					id="fechaHasta" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="fechaHasta" 
					title="Fecha Hasta" />
				</td>
				
			</tr>
			<tr>
				<td class="textoCampo">Estado:</td>
      			<td class="textoDato" colspan="3">
					<@s.select 
						templateDir="custontemplates" 
						id="carteleria.estado" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="carteleria.estado" 
						list="estadoList" 
						listKey="key" 
						listValue="description" 
						value="carteleria.estado.ordinal()"
						headerKey="0" 
	                    headerValue="Todos"
						title="Estado" />
				</td>
			</tr>
				<tr>
	    			<td colspan="4" class="lineaGris" align="right">
	      			<input id="limpiar" type="button" name="btnVolver" value="Limpiar" class="boton"/>		
	      			<input id="buscar" type="button" name="btnVolver" value="Buscar" class="boton"/>
	    			</td>
				</tr>	
			</table>
		</div>	
			
		<!-- Resultado Filtro -->
		<@s.if test="carteleriaList!=null">
				
			<#assign carteleriaOid="0" />
			<@s.if test="carteleria.oid!=null">
				<#assign carteleriaOid="${carteleria.oid}" />
			</@s.if>

			<#assign carteleriaDescripcion="" />
			<@s.if test="carteleria.descripcion!=null">
				<#assign carteleriaDescripcion="${carteleria.descripcion}" />
			</@s.if>

			<#assign carteleriaNumeroRuta="0" />
			<@s.if test="carteleria.numeroRuta != null">
				<#assign carteleriaNumeroRuta="${carteleria.numeroRuta}" />
			</@s.if>

			<#assign carteleriaKmRuta="0" />
			<@s.if test="carteleria.kmRuta != null">
				<#assign carteleriaKmRuta="${carteleria.kmRuta}" />
			</@s.if>

			<#assign carteleriaPropiedadCartel="0" />
			<@s.if test="carteleria.propiedadCartel != null">
				<#assign carteleriaPropiedadCartel="${carteleria.propiedadCartel.ordinal()}" />
			</@s.if>

			<#assign carteleriaFechaDesde = "" />
			<@s.if test="fechaDesde != null">
				<#assign carteleriaFechaDesde = "${fechaDesde?string('dd/MM/yyyy')}" />
			</@s.if>

			<#assign carteleriaFechaHasta = "" />
			<@s.if test="fechaHasta != null">
				<#assign carteleriaFechaHasta = "${fechaHasta?string('dd/MM/yyyy')}" />
			</@s.if>

			<#assign carteleriaProveedorNroProveedor = "" />
			<@s.if test="carteleria.proveedor.nroProveedor != null">
				<#assign carteleriaProveedorNroProveedor="${carteleria.proveedor.nroProveedor}" />
			</@s.if>

			<#assign carteleriaProveedorRazonSocial="" />
			<@s.if test="carteleria.proveedor.detalleDePersona.razonSocial!=null">
				<#assign carteleriaProveedorRazonSocial="${carteleria.proveedor.detalleDePersona.razonSocial}" />
			</@s.if>

			<#assign carteleriaProveedorNombre="" />
			<@s.if test="carteleria.proveedor.detalleDePersona.nombre!=null">
				<#assign carteleriaProveedorNombre="${carteleria.proveedor.detalleDePersona.nombre}" />
			</@s.if>

			<#assign carteleriaEstado="" />
			<@s.if test="carteleria.estado != null">
				<#assign carteleriaEstado="${carteleria.estado.ordinal()}" />
			</@s.if>
			
			<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
				<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>Carteles encontrados</td>
					<td>
						<div class="alineacionDerecha">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0301" 
							enabled="true" 
							cssClass="item" 
							href="${request.contextPath}/maestro/carteleria/printXLS.action?carteleria.oid=${carteleriaOid}&carteleria.descripcion=${carteleriaDescripcion}&carteleria.numeroRuta=${carteleriaNumeroRuta}&carteleria.kmRuta=${carteleriaKmRuta}&carteleria.propiedadCartel=${carteleriaPropiedadCartel}&fechaDesde=${carteleriaFechaDesde}&fechaHasta=${carteleriaFechaHasta}&carteleria.proveedor.nroProveedor=${carteleriaProveedorNroProveedor}&carteleria.proveedor.detalleDePersona.razonSocial=${carteleriaProveedorRazonSocial}&carteleria.proveedor.detalleDePersona.nombre=${carteleriaProveedorNombre}&carteleria.estado=${carteleriaEstado}">
							<img src="${request.contextPath}/common/images/imprimirExcel.gif" title="Exportar a Excel" align="absmiddle" border="0" hspace="3">
						</@security.a>
								
						</div>
						<div class="alineacionDerecha">
							<b>Imprimir</b>
							<@security.a 
								templateDir="custontemplates" 
								securityCode="CUF0301" 
								enabled="true" 
								cssClass="item" 
								href="${request.contextPath}/maestro/carteleria/printPDF.action?carteleria.oid=${carteleriaOid}&carteleria.descripcion=${carteleriaDescripcion}&carteleria.numeroRuta=${carteleriaNumeroRuta}&carteleria.kmRuta=${carteleriaKmRuta}&carteleria.propiedadCartel=${carteleriaPropiedadCartel}&fechaDesde=${carteleriaFechaDesde}&fechaHasta=${carteleriaFechaHasta}&carteleria.proveedor.nroProveedor=${carteleriaProveedorNroProveedor}&carteleria.proveedor.detalleDePersona.razonSocial=${carteleriaProveedorRazonSocial}&carteleria.proveedor.detalleDePersona.nombre=${carteleriaProveedorNombre}&carteleria.estado=${carteleriaEstado}">
								<img src="${request.contextPath}/common/images/imprimirPdf.gif" title="Exportar a PDF" align="absmiddle" border="0" hspace="3">
							</@security.a>
						</div>	
					</td>
				</tr>
			</table>	
			<@ajax.anchors target="contentTrx">			
          		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="carteleriaList" id="carteleria" pagesize=15 defaultsort=2 partialList=true size="recordSize" keepStatus=true excludedParams="resetFlag">
          		
  		
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select" class="botoneraAnchoCon4" title="Acciones">
						<div class="alineacion">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0302" 
							enabled="carteleria.readable" 
							cssClass="item" 
							href="${request.contextPath}/maestro/carteleria/readCarteleriaView.action?carteleria.oid=${carteleria.oid?c}&navigationId=ver-carteleria&flowControl=regis">
							<img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver"  border="0">
						</@security.a>
						</div>
						<div class="alineacion">	
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0303" 
							enabled="carteleria.updatable"
							cssClass="item"  
							href="${request.contextPath}/maestro/carteleria/administrarCarteleriaView.action?carteleria.oid=${carteleria.oid?c}&navigationId=administrar-carteleria&flowControl=regis">
							<img  src="${request.contextPath}/common/images/administrar.gif" alt="Administrar" title="Administrar"  border="0">
						</@security.a>
						</div>
						<div class="alineacion">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0304" 
							enabled="carteleria.eraseable"
							cssClass="item"  
							href="${request.contextPath}/maestro/carteleria/deleteCarteleriaView.action?carteleria.oid=${carteleria.oid?c}&navigationId=borrar-carteleria&flowControl=regis">
							<img  src="${request.contextPath}/common/images/eliminar.gif" alt="Eliminar" title="Eliminar"  border="0">
						</@security.a>				
						</div>
						<div class="alineacion">
						<#if carteleria.estado == "Activo">
							<#assign imagen="desactivar.gif">
							<#assign alternativa="Desactivar">
							<#assign titulo="Desactivar">
							<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0305" 
							enabled="true" 
							cssClass="item" 
							href="${request.contextPath}/maestro/carteleria/desactivarCarteleriaView.action?carteleria.oid=${carteleria.oid?c}&navigationId=cambiarEstado-carteleria&flowControl=regis">
							<img src="${request.contextPath}/common/images/${imagen}" alt="${alternativa}" title="${titulo}" border="0">
						</@security.a>
						<#else>
							<#assign imagen="activar.gif">
							<#assign alternativa="Activar">
							<#assign titulo="Activar">
							<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0305" 
							enabled="true" 
							cssClass="item" 
							href="${request.contextPath}/maestro/carteleria/activarCarteleriaView.action?carteleria.oid=${carteleria.oid?c}&navigationId=cambiarEstado-carteleria&flowControl=regis">
							<img src="${request.contextPath}/common/images/${imagen}" alt="${alternativa}" title="${titulo}" border="0">
						</@security.a>
						</#if>
						</div>
					</@display.column>


					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="oid" title="N&uacute;mero" />

					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="descripcion" title="Descripci&oacute;n" />				

					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="fecha" format="{0,date,dd/MM/yyyy}" title="Fecha Ingreso" />

					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="numeroRuta" title="N&uacute;mero Ruta" />

					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="kmRuta" title="Km Ruta" />

					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="propiedadCartel" title="Propiedad" />	

					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" title="Proveedor" >											

						<#if carteleria.proveedor?exists>
							<#if carteleria.proveedor.detalleDePersona.nombre?exists>
								${carteleria.proveedor.detalleDePersona.razonSocial} ${carteleria.proveedor.detalleDePersona.nombre}
							<#else>
								${carteleria.proveedor.detalleDePersona.razonSocial}
							</#if>
						</#if>

					</@display.column>

					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="estado" title="Estado" />		

				</@display.table>
			</@ajax.anchors>
			</div>
		</@s.if>
			
<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/carteleria/search.action"
  source="buscar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="resetFlag=true,navigationId={navigationId},flowControl=regis,carteleria.oid={carteleria.oid},carteleria.descripcion={carteleria.descripcion},carteleria.numeroRuta={carteleria.numeroRuta},carteleria.kmRuta={carteleria.kmRuta},carteleria.propiedadCartel={carteleria.propiedadCartel},fechaDesde={fechaDesde},fechaHasta={fechaHasta},carteleria.proveedor.nroProveedor={carteleria.proveedor.nroProveedor},carteleria.proveedor.detalleDePersona.razonSocial={carteleria.proveedor.detalleDePersona.razonSocial},carteleria.proveedor.detalleDePersona.nombre={carteleria.proveedor.detalleDePersona.nombre},carteleria.estado={carteleria.estado}"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/carteleria/view.action"
  source="limpiar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=regis"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/carteleria/createCarteleriaView.action" 
  source="crear" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=abm-carteleria-create,flowControl=regis"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/carteleria/selectProveedor.action" 
  source="seleccionarProveedor" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=change,navegacionIdBack={navigationId},carteleria.oid={carteleria.oid},carteleria.descripcion={carteleria.descripcion},carteleria.numeroRuta={carteleria.numeroRuta},carteleria.kmRuta={carteleria.kmRuta},carteleria.propiedadCartel={carteleria.propiedadCartel},fechaDesde={fechaDesde},fechaHasta={fechaHasta},carteleria.proveedor.nroProveedor={carteleria.proveedor.nroProveedor},carteleria.proveedor.detalleDePersona.razonSocial={carteleria.proveedor.detalleDePersona.razonSocial},carteleria.proveedor.detalleDePersona.nombre={carteleria.proveedor.detalleDePersona.nombre},carteleria.estado={carteleria.estado}"/>
