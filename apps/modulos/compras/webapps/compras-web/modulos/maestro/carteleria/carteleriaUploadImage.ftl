<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign tiles=JspTaglibs["/WEB-INF/tlds/tiles-jsp.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
        	<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	

<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos del Cartel</b></td>
				<td>
					<div align="right">
							<@s.a templateDir="custontemplates" id="modificarCarteleria" name="modificarCarteleria" href="javascript://nop/" cssClass="ocultarIcono">
								<b>Modificar</b><img src="${request.contextPath}/common/images/modificar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
							</@s.a>
					</div>
				</td>
			</tr>
		</table>
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        		
        	</tr>
			<tr>
      			<td class="textoCampo">N&uacute;mero:</td>
      			<td class="textoDato"><@s.property default="&nbsp;" escape=false value="carteleria.oid"/></td>
      			<td  class="textoCampo">Fecha: </td>
				<td class="textoDato">
					<@s.if test="carteleria.fecha != null">
						<#assign fecha = carteleria.fecha> 
						${fecha?string("dd/MM/yyyy")}	
					</@s.if>									
					&nbsp;
				</td>	      			
			</tr>	
			<tr>
				<td  class="textoCampo">Descripci&oacute;n: </td>
				<td class="textoDato">
				<@s.property default="&nbsp;" escape=false value="carteleria.descripcion"/>
									
				</td>	 
      			
      			<td class="textoCampo">Fotografia Digital:</td>
      			<td class="textoDato">				
					<#if carteleria.fotografiaDigital?exists && carteleria.fotografiaDigital.oid?exists>
						<a  
								id="imagenView" 
								name="imagenView"
								target="_blank" 
								href="${request.contextPath}/imagenes/imagenView.action?imagen.oid=${carteleria.fotografiaDigital.oid?c}">
									<img src="${request.contextPath}/common/images/ver.gif" border="0" align="absmiddle" hspace="3" >
						</a>		
					</#if>
							
					
					</td>	
				</td>		
			</tr>
			<tr>
      			<td class="textoCampo">Propiedad:</td>
      			<td class="textoDato">		      						
				<@s.property default="&nbsp;" escape=false value="carteleria.propiedadCartel"/>
		
                </td>		      		

				<td class="textoCampo">Proveedor:</td>
      			<td class="textoDato">
					<#assign proveedorBusquedaStyle="display:none;float:left;">
					<@s.if test="carteleria.propiedadCartel.ordinal() == 2">																		
						<#assign proveedorBusquedaStyle="display:block;float:left;">
					</@s.if>

					<div id="proveedorTexto" style="float:left;">
						<@s.property default="&nbsp;" escape=false value="carteleria.proveedor.detalleDePersona.razonSocial" /><@s.property default="&nbsp;" escape=false value="carteleria.proveedor.detalleDePersona.nombre" />
					</div>
				</td>				
					
    		</tr>
	    	<tr>
				<td class="textoCampo">Observaciones:</td>
      			<td  class="textoDato" colspan="3">
      					<@s.property default="&nbsp;" escape=false value="carteleria.observaciones"/>
			
				</td>
    		</tr>

    		<tr>
				<td class="textoCampo">Estado:</td>
      			<td  class="textoDato">
					<@s.property default="&nbsp;" escape=false value="carteleria.estado"/></td>
				</td>

				<td class="textoCampo">&nbsp;</td>
      			<td class="textoDato">&nbsp;</td>
    		</tr>
	    	<tr><td colspan="4" class="textoCampo">&nbsp;</td></tr>	
	    </div>
	    
	    <div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Dimensiones</b></td>
			</tr>
		</table>
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>
	    	    		
    		<tr>
    			<td  class="textoCampo">Ancho[m]: </td>
				<td class="textoDato">
				<@s.property default="&nbsp;" escape=false value="carteleria.ancho"/>
									
				</td>	

				<td  class="textoCampo">Alto[m]: </td>
				<td class="textoDato">
				<@s.property default="&nbsp;" escape=false value="carteleria.alto"/>										
				</td>	
    		</tr>

    		<tr>
    			<td  class="textoCampo">Superficie[m2]: </td>
				<td class="textoDato">
				<@s.property default="&nbsp;" escape=false value="carteleria.superficie"/>
				</td>	
				
				<td class="textoCampo">&nbsp;</td>
      			<td class="textoDato" >&nbsp;</td>
    		</tr>
    		<tr><td colspan="4"class="textoCampo">&nbsp;</td></tr>
    	</table>
    </div>
    <div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Condiciones F&iacute;sicas</b></td>
			</tr>
		</table>
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>		
    		<tr>
    			<td  class="textoCampo">Estructura de: </td>
				<td class="textoDato">
				<@s.property default="&nbsp;" escape=false value="carteleria.estructura"/>
										
				</td>	
    		
    			<td class="textoCampo">Condici&oacute;n:</td>
      			<td class="textoDato">		      		
      			<@s.property default="&nbsp;" escape=false value="carteleria.condicionEst"/>				
					
                </td>	
            </tr>    

			<tr>
    			<td  class="textoCampo">Fundaci&oacute;n de: </td>
				<td class="textoDato" align="left" width="30%">
				<@s.property default="&nbsp;" escape=false value="carteleria.fundacion"/>
										
				</td>	
    		
    			<td class="textoCampo">Condici&oacute;n:</td>
      			<td class="textoDato">
      			<@s.property default="&nbsp;" escape=false value="carteleria.condicionEst"/>		      						
					
                </td>	
            </tr>
	            
            <tr>
    			<td  class="textoCampo">Pintura de Fondo: </td>
				<td class="textoDato">
				<@s.property default="&nbsp;" escape=false value="carteleria.pinturaFondo"/>
							
				</td>	
    		
    			<td class="textoCampo">Condici&oacute;n:</td>
      			<td class="textoDato">	
      			<@s.property default="&nbsp;" escape=false value="carteleria.condicionPintura"/>	      						
					
                </td>	
            </tr>
	    		
    		<tr>
				<td class="textoCampo">Letras:</td>
      			<td class="textoDato" colspan="3">
      			<@s.property default="&nbsp;" escape=false value="carteleria.tipoLetra"/>		      						
					
                </td>
			</tr>
			<tr>
                <td class="textoCampo" colspan="4">&nbsp;</td>
      		</tr>
           </table>
          </div>  
	    	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
				<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            		<tr>
						<td><b>Ubicaci&oacute;n</b></td>
					</tr>
				</table>
				<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
					<tr>
        				<td class="textoCampo" colspan="4">&nbsp;</td>
        			</tr>	
	    			<tr>
		    			<td  class="textoCampo">N&uacute;mero Ruta: </td>
						<td class="textoDato">
						<@s.property default="&nbsp;" escape=false value="carteleria.numeroRuta"/>
								
						</td>	
					
						<td  class="textoCampo">Km Ruta: </td>
						<td class="textoDato">
							<@s.property default="&nbsp;" escape=false value="carteleria.kmRuta"/>
										
						</td>	
		    		</tr>
		    		<tr><td colspan="4" class="textoCampo">&nbsp;</td></tr>
		    </table>		
		</div>				

  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action"  
  source="cancelar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=buscar-carteleria,flowControl=back"/>
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
			<tr>
				<td><b>Upload de Imagen</b></td>
			</tr>
		</table>
		<@s.form 
		   action="${request.contextPath}/imagenes/doUpload.action" 
		   method="post" 
		   enctype="multipart/form-data"  theme="ajax"  id="editProject" name="editProject">
		   		<@s.hidden id="navegacionIdBack" name="navegacionIdBack"/>
		   		<@s.hidden id="navigationId" name="navigationId"/>
				<@s.hidden id="carteleria.oid" name="carteleria.oid"/>				
	       		<@s.file name="upload" label="File"/> 
	       		<@s.submit  theme="ajax"/>		       
   	   </@s.form>	

	</div>	


   	<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="left">
				<input id="cancelar" type="button" name="btnVolver" value="Volver" class="boton"/>
			</td>
		</tr>	
	</table>
</div>
  

  
  

