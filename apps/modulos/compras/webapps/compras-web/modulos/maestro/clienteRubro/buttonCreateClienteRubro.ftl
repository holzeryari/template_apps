<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancel" type="button" name="btnVolver" value="Cancelar" class="boton"/>
				<input id="store" type="button" name="btnAgregar" value="Agregar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>


<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/clienteRubro/create.action" 
  source="store" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="usuario.oid={usuario.oid},clienteRubro.usuario.oid={clienteRubro.usuario.oid},clienteRubro.rubro.oid={clienteRubro.rubro.oid},clienteRubro.tipoAsignacion={clienteRubro.tipoAsignacion}"/>
 
<@vc.htmlContent baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=buscar-clienteRubro,flowControl=back"/>
    
<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/clienteRubro/selectRubroCreateUpdate.action" 
  source="seleccionarRubro" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId=crear-clienteRubro,flowControl=change,navegacionIdBack=crear-clienteRubro,usuario.oid={usuario.oid},clienteRubro.rubro.oid={clienteRubro.rubro.oid},clienteRubro.rubro.descripcion={clienteRubro.rubro.descripcion},clienteRubro.tipoAsignacion={clienteRubro.tipoAsignacion}"/>


        
