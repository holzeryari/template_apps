<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancel" type="button" name="btnVolver" value="Cancelar" class="boton"/>
				<input id="delete" type="button" name="btnEliminar" value="Eliminar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/clienteRubro/delete.action" 
  source="delete" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="usuario.oid={usuario.oid},clienteRubro.usuario.oid={clienteRubro.usuario.oid},clienteRubro.oid={clienteRubro.oid},clienteRubro.versionNumber={clienteRubro.versionNumber}"/>
  
<@vc.htmlContent baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=buscar-clienteRubro,flowControl=back"/>





