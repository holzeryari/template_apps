<#setting locale="es_AR">

<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>

<@s.hidden id="navigationId" name="navigationId"/>
<@s.hidden id="navegacionIdBack" name="navegacionIdBack"/>

<@s.hidden id="clienteRubro.rubro.oid" name="clienteRubro.rubro.oid"/>
<@s.hidden id="clienteRubro.oid" name="clienteRubro.oid"/>
<@s.hidden id="clienteRubro.usuario.oid" name="usuario.oid"/>
<@s.hidden id="usuario.oid" name="usuario.oid"/>

	<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
        	<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>
				
	<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">	
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
			<tr>
				<td><b>Datos del rubro asociado</b></td>
			</tr>
		</table>

		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>
        		
        	<tr>
        		<td class="textoCampo">Rubro:</td>
      			<@s.hidden id="clienteRubro.rubro.descripcion" name="clienteRubro.rubro.descripcion"/>
      			<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="clienteRubro.rubro.descripcion" />
					<@s.a templateDir="custontemplates" id="seleccionarRubro" name="seleccionarRubro" href="javascript://nop/" cssClass="ocultarIcono">
						<img src="${request.contextPath}/common/images/buscar.gif" border="0" align="absmiddle" hspace="3" >
					</@s.a>
				</td>

				<td class="textoCampo">Tipo Asignacion:</td>
               <td class="textoDato">
                        <@s.select 
                                        templateDir="custontemplates" 
                                        id="clienteRubro.tipoAsignacion" 
                                        cssClass="textarea"                                             
                                        cssStyle="width:165px" 
                                        name="clienteRubro.tipoAsignacion" 
                                        list="tipoAsignacionList"
                                        listKey="key" 
                                        listValue="description" 
                                        value="clienteRubro.tipoAsignacion.ordinal()"
                                        title="tipo Asignacion"
                                        headerKey="0"
                                        headerValue="Todos"                                                             
        
                                        />
                </td> 				
        	</tr>
        </table>
	</div>        
	
	