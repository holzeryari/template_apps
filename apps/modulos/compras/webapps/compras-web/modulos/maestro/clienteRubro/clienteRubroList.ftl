<#setting locale="es_AR">

<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>

<@s.hidden id="webFlow" name="webFlow"/>
<@s.hidden id="viewState" name="viewState"/>
<@s.hidden id="navigationId" name="navigationId"/>

	<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>

	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
			<tr>
				<td><b>Rubros Asociados al usuario</b></td>
				<td>					
					<div align="right">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0001" 
							enabled="true" 
							cssClass="item" 
							id="crear"										
							href="javascript://nop/">
							<b>Agregar</b>										
							<img src="${request.contextPath}/common/images/agregar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
						</@security.a>
					</div>
				</td>
			</tr>
		</table>
	</div> 

 	<@s.if test="clienteRubroList!=null">
	  	<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">									
			<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>Cliente Rubro</td>
				</tr>	
			</table> 
			<@vc.anchors target="contentTrx">	
				<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="clienteRubroList" id="clienteRubro" pagesize=15 partialList=true size="recordSize" keepStatus=true excludedParams="resetFlag">
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select" class="botoneraAnchoCon3" title="Acciones" >
							<div class="alineacion">
								<@security.a 
									templateDir="custontemplates" 
									securityCode="CUF0001" 
									enabled="clienteRubro.eraseable"
									cssClass="item"  
									href="${request.contextPath}/maestro/clienteRubro/deleteView.action?clienteRubro.oid=${clienteRubro.oid}&navigationId=clienteRubro-delete&flowControl=regis&navegacionIdBack=buscar-clienteRubro">
									<img  src="${request.contextPath}/common/images/eliminar.gif" alt="Eliminar" title="Eliminar" border="0">
								</@security.a>									
							</div>
					</@display.column>

					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="usuario.nombreUsuario" title="Usuario" />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="rubro.descripcion" title="Rubro" />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="tipoAsignacion" title="Tipo Asignacion" />		
			
				</@display.table>
			</@vc.anchors>
	  	</div>
	</@s.if>
	  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/clienteRubro/createView.action" 
  source="crear" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=crear-clienteRubro,flowControl=regis,usuario.oid=${usuario.oid?c}"/>
  