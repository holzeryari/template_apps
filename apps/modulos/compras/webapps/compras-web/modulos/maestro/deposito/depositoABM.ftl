<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign tiles=JspTaglibs["/WEB-INF/tlds/tiles-jsp.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<@s.hidden id="navigationId" name="navigationId"/>
<@s.hidden id="oid" name="deposito.oid"/>
<@s.hidden id="versionNumber" name="deposito.versionNumber"/>

<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
			<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	

	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos del Dep&oacute;sito</b></td>
				<td>
					<div align="right">
						<@s.a templateDir="custontemplates" id="modificarDeposito" name="modificarDeposito" href="javascript://nop/" cssClass="ocultarIcono">
						<b>Modificar</b><img src="${request.contextPath}/common/images/modificar.gif" border="0" align="absmiddle" hspace="3" >
						</@s.a>
					</div>
				</td>
			</tr>
		</table>

		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<#assign seccionClass="textareagris">
			<#assign filaColumnaClass="textareagris">									
				<@s.if test="deposito.tipoOrganizacion.ordinal() == 1">
					<#assign seccionClass="textarea">
					<#assign filaColumnaClass="textareagris">		
				</@s.if>
				<@s.if test="deposito.tipoOrganizacion.ordinal() == 2">
					<#assign seccionClass="textareagris">
					<#assign filaColumnaClass="textarea">		
				</@s.if>
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>
			<tr>
      			<td class="textoCampo">C&oacute;digo:</td>
      			<td class="textoDato">
      				<@s.property default="&nbsp;" escape=false value="deposito.oid"/>
      			</td>
      			<td class="textoCampo">Tipo de Organizaci&oacute;n:</td>
	      		<td class="textoDato" align="left">
	      			<@s.select 
							templateDir="custontemplates" 
							id="tipoOrganizacion" 
							cssClass="textarea"
							cssStyle="width:165px" 
							name="deposito.tipoOrganizacion"
							onchange="javascript:depositoSelect(this);" 
							list="tipoOrganizacionList" 
							listKey="key" 
							listValue="description" 
							value="deposito.tipoOrganizacion.ordinal()"
							title="Tipo de Organizaci&oacute;n"
							headerKey="0"
							headerValue="Seleccionar" 
							/>
	      		</td>		      		
			</tr>	
			<tr>
      			<td  class="textoCampo">Descripci&oacute;n: </td>
				<td class="textoDato">
					<@s.textfield 
						templateDir="custontemplates" 
						id="descripcion" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="deposito.descripcion" 
						title="Descripci&oacute;n"
						maxLength="100"/>					
				</td>	      		
				
				<td class="textoCampo">Estado:</td>
      			<td  class="textoDato">
      				<@s.property default="&nbsp;" escape=false value="deposito.estado"/>									
				</td>
			</tr>	
			</tr>
			<tr>
				<td  class="textoCampo">Cantidad secciones: </td>
				<td  class="textoDato" colspan="3" >
					<@s.textfield 
						templateDir="custontemplates" 
						id="cantidadSeccion" 
						cssClass="${seccionClass}"
						cssStyle="width:160px" 
						name="deposito.cantidadSeccion" 
						title="Cantidad de Secciones"
						disabled="true"/>
				</td>
			</tr>
			<tr>
				<td  class="textoCampo">Cantidad filas: </td>
				<td  class="textoDato">
					<@s.textfield 
						templateDir="custontemplates" 
						id="cantidadFila" 
						cssClass="${filaColumnaClass}"
						cssStyle="width:160px" 
						name="deposito.cantidadFila" 
						title="Cantidad Filas" 
						disabled="true"/>
				</td>
				<td  class="textoCampo">Cantidad columnas:</td>
				<td  class="textoDato">
					<@s.textfield 
						templateDir="custontemplates" 
						id="cantidadColumna" 
						cssClass="${filaColumnaClass}"
						cssStyle="width:160px" 
						name="deposito.cantidadColumna" 
						title="Cantidad de Columnas"
						disabled="true" />
				</td>
			</tr>
		
			<#-- renglon de separacion -->
			<tr><td colspan="4">&nbsp;</td></tr>
		</table>
	</div>
	
	<@tiles.insertAttribute name="ubicaciones"/>			
	
		 
<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/deposito/updateView.action" 
  source="modificarDeposito" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="deposito.oid={oid},navigationId=actualizar-deposito,flowControl=regis"/>






