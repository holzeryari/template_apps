<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<@s.hidden id="navigationId" name="navigationId"/>
<@s.hidden id="filaColumna.oid" name="filaColumna.oid"/>
<@s.hidden id="filaColumna.deposito.oid" name="filaColumna.deposito.oid"/>

	<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
			<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	

	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos del Dep&oacute;sito</b></td>
			</tr>
		</table>
	
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr><td class="textoCampo" colspan="4">&nbsp;</td></tr>
			<tr>
      			<td class="textoCampo">C&oacute;digo:</td>
      			<td class="textoDato">
      				<@s.property default="&nbsp;" escape=false value="filaColumna.deposito.oid"/>
      			</td>
      			<td  class="textoCampo">Descripci&oacute;n: </td>
				<td class="textoDato">					
					<@s.property default="&nbsp;" escape=false value="filaColumna.deposito.descripcion"/>										
				</td>	      			
			</tr>	
			<tr>
      			<td class="textoCampo">Tipo de Organizaci&oacute;n:</td>
	      		<td class="textoDato">
	      			<@s.property default="&nbsp;" escape=false value="filaColumna.deposito.tipoOrganizacion"/>
	      		</td>		
	      		<td class="textoCampo">Estado:</td>
      			<td  class="textoDato">
      				<@s.property default="&nbsp;" escape=false value="filaColumna.deposito.estado"/>
				</td>
    		</tr>
			<#-- <tr>
				<td  class="textoCampo">Secciones: </td>
				<td  class="textoDato" colspan="3" >
					<@s.property default="&nbsp;" escape=false value="filaColumna.eposito.cantidadSeccion"/>
				</td>
			</tr>-->
			<tr>
				<td  class="textoCampo">Filas: </td>
				<td  class="textoDato">
					<@s.property default="&nbsp;" escape=false value="filaColumna.deposito.cantidadFila"/>
				</td>
				<td  class="textoCampo">Columnas:</td>
				<td  class="textoDato">
					<@s.property default="&nbsp;" escape=false value="filaColumna.deposito.cantidadColumna"/>
				</td>
			</tr>
			<tr>
      			<td class="textoCampo">Ubicaciones:</td>
      			<td  class="textoDato" colspan="3">
      				<@s.property default="&nbsp;" escape=false value="filaColumna.deposito.cantidadUbicacion"/>
				</td>
			</tr>
			<tr><td class="textoCampo" colspan="4">&nbsp;</td></tr>
		</table>		
				
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos de la Fila-Columna-Ubicaci&oacute;n</b></td>
			</tr>
		</table>						
	
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr><td class="textoCampo" colspan="4">&nbsp;</td></tr>
			<tr>
			 	<td class="textoCampo">C&oacute;digo:</td>
				<td class="textoDato" colspan="3">
					<@s.property default="&nbsp;" escape=false value="filaColumna.oid"/>
				</td>
			</tr>
			<tr>
				<td  class="textoCampo">Fila: </td>
				<td  class="textoDato">
					<@s.textfield 
						templateDir="custontemplates" 
						id="fila" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="filaColumna.fila" 
						title="Fila"
						maxLength="1" />
				</td>								
				<td  class="textoCampo">Columna:</td>
				<td  class="textoDato">
					<@s.textfield 
						templateDir="custontemplates" 
						id="columna" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="filaColumna.columna" 
						title="Columna" />
				</td>											
			</tr>							
			<tr>
			 	<td class="textoCampo">Ubicaci&oacute;n:</td>
				<td  class="textoDato" colspan="3">
					<@s.textfield 
						templateDir="custontemplates" 
						id="ubicacion" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="filaColumna.ubicacion" 
						title="Ubicaci&oacute;n"/>
				</td>
			</tr>	
			<tr><td class="textoCampo" colspan="4">&nbsp;</td></tr>			
		</table>
	</div>	
		
	      

