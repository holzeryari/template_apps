<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>


<@ajax.anchors target="contentTrx">	

<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
	<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
		<tr>
			<td>Ubicaciones</td>
			<td>
				<div align="right">
				<@s.a href="${request.contextPath}/maestro/deposito/createFilaColumnaView.action?filaColumna.deposito.oid=${deposito.oid?c}">
					<b>Agregar</b><img src="${request.contextPath}/common/images/agregar.gif" border="0" align="absmiddle" hspace="3" >
				</@s.a>
				</div>
			</td>
		</tr>
	</table>	
						
	<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="deposito.depositoUbicacionList" id="filaColumna" defaultsort=2 >	
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select" style=" text-align:center; display:block" title="Acciones">
			<a href="${request.contextPath}/maestro/deposito/deleteFilaColumnaView.action?filaColumna.oid=${filaColumna.oid?c}">
				<img  src="${request.contextPath}/common/images/eliminar.gif" alt="Eliminar" title="Eliminar" border="0">
			</a>
		</@display.column>			
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="fila" title="Fila"/>
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="columna" title="Columna" />
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="ubicacion" title="Ubicaci&oacute;n" />
										
	</@display.table>
</div>

</@ajax.anchors>
