<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>


<@vc.anchors target="contentTrx" ajaxFlag="ajax">

<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
	<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
		<tr>
			<td>Ubicaciones</td>
			<td>
				<div align="right">
				<@s.a href="${request.contextPath}/maestro/deposito/createSeccionView.action?seccion.deposito.oid=${deposito.oid?c}">
					<b>Agregar</b><img src="${request.contextPath}/common/images/agregar.gif" border="0" align="absmiddle" hspace="3" >
				</@s.a>
				</div>
			</td>
		</tr>
	</table>	

	<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="deposito.depositoUbicacionList" id="seccion" defaultsort=2>	
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select" style=" text-align:center; display:block" title="Acciones">
			<a href="${request.contextPath}/maestro/deposito/deleteSeccionView.action?seccion.oid=${seccion.oid?c}">
			<img  src="${request.contextPath}/common/images/eliminar.gif" alt="Eliminar" title="Eliminar" border="0"></a>
		</@display.column>		
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="seccion" title="Seccion" />					
	</@display.table>
</div>
</@vc.anchors>

