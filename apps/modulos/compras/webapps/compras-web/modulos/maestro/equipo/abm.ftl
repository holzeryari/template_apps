<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<@s.hidden id="webFlow" name="webFlow"/>
<@s.hidden id="viewState" name="viewState"/>

<@s.hidden id="equipoIngreso.oid" name="equipoIngreso.oid"/>
<@s.hidden id="equipoIngreso.nombre" name="equipoIngreso.nombre"/>
<@s.hidden id="equipoIngreso.versionNumber" name="equipoIngreso.versionNumber"/>

	<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
			<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos del Equipo de Ingreso</b></td>
				<td>
					<div align="right">
						<@s.a templateDir="custontemplates" id="modificarEquipo" name="modificarEquipo" href="javascript://nop/" cssClass="ocultarIcono">
						<b>Modificar</b><img src="${request.contextPath}/common/images/modificar.gif" border="0" align="absmiddle" hspace="3" >
						</@s.a>
					</div>
				</td>				
			</tr>
		</table>

		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>
			<tr>
      			<td class="textoCampo">Nombre:</td>
      			<td class="textoDato">
     					<@s.textfield 
						templateDir="custontemplates" 
						id="nombre" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="equipoIngreso.nombre" 
						title="Nombre" />	
						</td>
			</tr>						
			<#-- renglon de separacion -->
			<tr><td colspan="4">&nbsp;</td></tr>				
		</table>
	
		</div>
		
<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/equipo/updateView.action" 
  source="modificarEquipo" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="equipoIngreso.oid={equipoIngreso.oid},equipoIngreso.versionNumber={equipoIngreso.versionNumber},navegacionIdBack=administrar-equipos,navigationId=equipo-update"/>		