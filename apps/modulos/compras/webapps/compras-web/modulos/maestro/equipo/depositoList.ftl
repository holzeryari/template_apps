<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>
<#assign tiles=JspTaglibs["/WEB-INF/tlds/tiles-jsp.tld"]>

<@s.hidden id="navigationId" name="navigationId"/>
<@s.hidden id="equipoIngreso.oid" name="equipoIngreso.oid"/>

<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
        	<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>

	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
			<tr>
				<td><b>Datos del Dep&oacute;sito</b></td>
			</tr>
		</table>
		
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>
				<td class="textoCampo">C&oacute;digo:</td>
				<td class="textoDato">
					<@s.textfield 
 						templateDir="custontemplates" 
						id="oid" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="deposito.oid" 
						title="C&oacute;digo"/>
				</td>
				<td class="textoCampo">Descripci&oacute;n:</td>
      			<td class="textoDato">
      				<@s.textfield 
      					templateDir="custontemplates" 
						id="descripcion" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="deposito.descripcion" 
						title="Descripcion" />
				</td>
			</tr>
			<tr>
	    		<td colspan="4" class="lineaGris" align="right">
	      			<input id="limpiar" type="button" name="btnVolver" value="Limpiar" class="boton"/>		
	      			<input id="buscar" type="button" name="btnVolver" value="Buscar" class="boton"/>
	    		</td>
			</tr>
		</table>
	  </div>
				
	  <@s.if test="depositoList!=null">									
		<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">			
			<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>Dep&oacute;sitos encontrados</td>
				</tr>	
			</table>
			
			<@vc.anchors target="contentTrx">	
					<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="depositoList" id="depositoSelect" pagesize=15 defaultsort=1 >

					<#assign ref="${request.contextPath}/maestro/equipo/createEquipoDepositoView.action?">
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select" style=" text-align:center; display:block" title="Acciones">
						<@s.a href="${ref}deposito.oid=${depositoSelect.oid?c}&deposito.descripcion=${depositoSelect.descripcion}&deposito.estado=${depositoSelect.estado}&deposito.tipoOrganizacion=${depositoSelect.tipoOrganizacion}&navigationId=${navigationId}&namespace=${namespace}&actionName=${actionName}&equipoIngreso.oid=${equipoIngreso.oid?c}">
							<img src="${request.contextPath}/common/images/seleccionar.gif" alt="Seleccionar" title="Seleccionar" width="16" height="16" border="0">
						</@s.a>
					</@display.column>						  															
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="oid" title="C&oacute;digo" />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="descripcion" title="Descripci&oacute;n" />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service"  class="estiloTexto" property="tipoOrganizacion" title="Tipo Organizaci&oacute;n" />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="estado" title="Estado" />					
				</@display.table>
			</@vc.anchors>
		</div>
	  </@s.if>
			
 <@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="flowControl=back"/>			  