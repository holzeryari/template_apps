<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<@vc.anchors target="contentTrx" ajaxFlag="ajax">
	<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
		<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
			<tr>
				<td>Usuarios</td>
				<td>
					<div align="right">
						<@s.a href="${request.contextPath}/maestro/equipo/createSecurityUserView.action?equipoIngreso.oid=${equipoIngreso.oid?c}&navegacionIdBack=${navigationId}">
						<b>Agregar</b><img src="${request.contextPath}/common/images/agregar.gif" border="0" align="absmiddle" hspace="3" >
						</@s.a>
					</div>
				</td>	
			</tr>
		</table>
		
		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="equipoIngreso.usuarioList" id="usuario">	
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select" style=" text-align:center; display:block" title="Acciones">
				<a href="${request.contextPath}/maestro/equipo/deleteSecurityUserView.action?usuario.oid=${usuario.oid?c}&equipoIngreso.oid=${equipoIngreso.oid?c}&usuario.descripcion=${usuario.descripcion}&usuario.nombre=${usuario.nombre}&usuario.apellido=${usuario.apellido}">
					<img  src="${request.contextPath}/common/images/eliminar.gif" alt="Eliminar" title="Eliminar" border="0"></a>
			</@display.column>		
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="descripcion" title="Descripci&oacute;n" />					
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="nombre" title="Nombre" />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="apellido" title="Apellido" />
		</@display.table>
	</div>	
</@vc.anchors>