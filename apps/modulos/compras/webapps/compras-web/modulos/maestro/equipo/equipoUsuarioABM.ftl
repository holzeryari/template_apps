<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>


<@s.hidden id="webFlow" name="webFlow"/>
<@s.hidden id="viewState" name="viewState"/>
<@s.hidden id="equipoIngreso.oid" name="equipoIngreso.oid"/>
<@s.hidden id="usuario.oid" name="usuario.oid"/>

	<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
			<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td colspan="4"><b>Datos del Equipo de Ingreso</b></td>
			</tr>
		</table>
	
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>	
			<tr>
      			<td class="textoCampo">C&oacute;digo:</td>
      			<td class="textoDato"><@s.property default="&nbsp;" escape=false value="equipoIngreso.oid"/></td>			
      			<td class="textoCampo">Nombre:</td>
	  			<td class="textoDato">${equipoIngreso.nombre}</td>
			</tr>	
    		<#-- renglon de separacion -->
			<tr><td colspan="4">&nbsp;</td></tr>
		</table>
	</div>
	
	<!--datos de usuario -->
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td colspan="4"><b>Datos del Usuario</b></td>
			</tr>
		</table>
	
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
				<td class="textoCampo">C&oacute;digo:</td>
	    		<td class="textoDato"><@s.property default="&nbsp;" escape=false value="usuario.oid"/></td>
	      		<td class="textoCampo">Descripci&oacute;n</td>
				<td class="textoDato">
					${usuario.descripcion}					
				</td>	      			
			</tr>	
			<tr>
	      		<td class="textoCampo">Nombre:</td>
    	  		<td class="textoDato">
		      		${usuario.nombre}
		      	</td>
	      		<td class="textoCampo">Apellido:</td>
				<td class="textoDato">
					${usuario.apellido}
				</td>
    		</tr>
    		<tr><td class="textoCampo" colspan="4">&nbsp;</td></tr>
		</table>
	</div>