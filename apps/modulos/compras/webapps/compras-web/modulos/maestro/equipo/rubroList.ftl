<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>
<#assign tiles=JspTaglibs["/WEB-INF/tlds/tiles-jsp.tld"]>

<@s.hidden id="navigationId" name="navigationId"/>
<@s.hidden id="equipoIngreso.oid" name="equipoIngreso.oid"/>

<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
        	<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>

	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
			<tr>
				<td><b>Datos del Rubro</b></td>
			</tr>
		</table>
		
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>
			<tr>
	      		<td class="textoCampo">C&oacute;digo:</td>
	      		<td class="textoDato">
				     <@s.textfield 
	      				templateDir="custontemplates" 
						id="codigo" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="rubro.codigo" 
						title="C&oacute;digo" />
				</td>
	      		<td class="textoCampo">Descripci&oacute;n:</td>
	      		<td class="textoDato">
	      			<@s.textfield 
	      				templateDir="custontemplates" 
						id="descripcion" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="rubro.descripcion" 
						title="Descripcion" />
				</td>
				
				<tr>					
	      			<td class="textoCampo">Rubro:</td>
	      			<td class="textoDato">
				     <@s.select 
						templateDir="custontemplates" 
						id="rubroNivel0" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="rubroNivel0.oid" 
						list="rubroNivel0List" 
						listKey="oid" 
						listValue="descripcion" 
						value="rubroNivel0.oid"
						title="Rubro nivel 0"
						headerKey="0"
						headerValue="Todos"						
						 /> 
					</td>
	    			<td class="textoCampo">Rubro nivel 1:</td>
					<td class="textoDato">
				     	<@s.select 
							templateDir="custontemplates" 
							id="rubroNivel1" 
							cssClass="textarea"
							cssStyle="width:165px" 
							name="rubroNivel1.oid" 
							list="rubroNivel1List" 
							listKey="oid" 
							listValue="descripcion" 
							title="Rubro nivel 1"
							headerKey="0"
							headerValue="Todos"							
						 	/> 
					</td>
				</tr>
				<tr>
					<td class="textoCampo">Rubro nivel 2:</td>
					<td class="textoDato">
				     <@s.select 
						templateDir="custontemplates" 
						id="rubroNivel2" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="rubroNivel2.oid" 
						list="rubroNivel2List" 
						listKey="oid" 
						listValue="descripcion" 
						title="Rubro nivel 2"
						headerKey="0"
						headerValue="Todos"  
						 /> 
					</td>
					<td class="textoCampo">Rubro nivel 3:</td>
					<td class="textoDato">
				     <@s.select 
						templateDir="custontemplates" 
						id="rubroNivel3" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="rubroNivel3.oid" 
						list="rubroNivel3List" 
						listKey="oid" 
						listValue="descripcion" 
						title="Rubro nivel 3"
						headerKey="0"
						headerValue="Todos"						  
						 /> 
					</td>
				</tr>
				
	    	</tr>
			<tr>
	    		<td colspan="4" class="lineaGris" align="right">
	      			<input id="limpiar" type="button" name="btnVolver" value="Limpiar" class="boton"/>		
	      			<input id="buscar" type="button" name="btnVolver" value="Buscar" class="boton"/>
	    		</td>
			</tr>
		</table>
	  </div>
				
	  <@s.if test="rubroList!=null">									
		<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">			
			<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>Rubros encontrados</td>
				</tr>	
			</table>
			
			<@vc.anchors target="contentTrx">	
					<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="rubroList" id="rubroSelect" pagesize=15 partialList=true size="recordSize" keepStatus=true excludedParams="resetFlag">

					<#assign ref="${request.contextPath}/maestro/equipo/createEquipoRubroView.action?">
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select" style=" text-align:center; display:block" title="Acciones">
						<@s.a href="${ref}rubro.oid=${rubroSelect.oid?c}&rubro.descripcion=${rubroSelect.descripcion}&rubro.codigo=${rubroSelect.codigo}&navigationId=${navigationId}&namespace=${namespace}&actionName=${actionName}&equipoIngreso.oid=${equipoIngreso.oid?c}">
							<img src="${request.contextPath}/common/images/seleccionar.gif" alt="Seleccionar" title="Seleccionar" width="16" height="16" border="0">
						</@s.a>
					</@display.column>						  															
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="codigo" title="C&oacute;digo" />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="descripcion" title="Descripci&oacute;n" />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="imputable" title="Imputable" />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="cuentaContableListAsString" title="Cuenta Contable" />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="estado" title="Estado" />					
				</@display.table>
			</@vc.anchors>
		</div>
	  </@s.if>
			
<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/equipo/selectSearchRubro.action" 
  source="buscar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="resetFlag=true,navigationId={navigationId},flowControl=regis,namespace=${namespace},actionName=${actionName},rubro.descripcion={descripcion},rubro.codigo={codigo},rubroNivel0.oid={rubroNivel0},rubroNivel1.oid={rubroNivel1},rubroNivel2.oid={rubroNivel2},rubroNivel3.oid={rubroNivel3},rubro.estado={rubro.estado},rubro.estado={rubroEstado},rubro.imputable={rubro.imputable},estadoDuro={estadoDuro},imputableDuro={imputableDuro},rubro.fondoFijo={rubro.fondoFijo},fondoFijoDuro={fondoFijoDuro},rubroPadreDuro={rubroPadreDuro}"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/equipo/selectViewRubro.action" 
  source="limpiar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=regis,namespace=${namespace},actionName=${actionName},estadoDuro={estadoDuro},imputableDuro={imputableDuro},rubro.fondoFijo={rubro.fondoFijo},fondoFijoDuro={fondoFijoDuro},rubro.imputable={rubro.imputable},rubro.estado={rubro.estado}"/>
    
 <@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="flowControl=back"/>
  
<@ajax.select
  baseUrl="${request.contextPath}/maestro/equipo/refrescarSubrubro1.action" 
  source="rubroNivel0" 
  target="rubroNivel1"
  parameters="rubroNivel0.oid={rubroNivel0},navegacionIdBack={navegacionIdBack}"
  parser="new ResponseXmlParser()"/>

<@ajax.select
  baseUrl="${request.contextPath}/maestro/equipo/refrescarSubrubro2.action" 
  source="rubroNivel1" 
  target="rubroNivel2"
  parameters="rubroNivel1.oid={rubroNivel1}"
  parser="new ResponseXmlParser()"/>
  
<@ajax.select
  baseUrl="${request.contextPath}/maestro/equipo/refrescarSubrubro3.action" 
  source="rubroNivel2" 
  target="rubroNivel3"
  parameters="rubroNivel2.oid={rubroNivel2}"
  parser="new ResponseXmlParser()"/>  			  