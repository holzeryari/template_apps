
<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<@s.hidden id="equipoIngreso.oid" name="equipoIngreso.oid"/>

	<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
        	<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	

	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
			<tr>
				<td><b>Datos del Usuario</b></td>
			</tr>
		</table>
		
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>
			<tr>
	      		<td class="textoCampo">Nombre:</td>
	      		<td class="textoDato">
					<@s.textfield 
	      				templateDir="custontemplates" 
						id="usuarioSeguridad.username" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="usuarioSeguridad.username" 
						title="Nombre" />
				</td>
	      		<td class="textoCampo">Descripci&oacute;n</td>
	      		<td class="textoDato">
	      			<@s.textfield 
	      				templateDir="custontemplates" 
						id="usuarioSeguridad.fullname" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="usuarioSeguridad.fullname" 
						title="Descripci&oacute;n" />
				</td>
	    	</tr>					
			<tr>
				<td class="textoCampo">Apellido:</td>
	      		<td class="textoDato" colspan="3">
	      			<@s.textfield 
	      				templateDir="custontemplates" 
						id="usuarioSeguridad.name" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="usuarioSeguridad.name" 
						title="Apellido" />
				</td>
			</tr>
			<tr>
	    		<td colspan="4" class="lineaGris" align="right">
	      			<input id="limpiar" type="button" name="btnVolver" value="Limpiar" class="boton"/>		
	      			<input id="buscar" type="button" name="btnVolver" value="Buscar" class="boton"/>
	    		</td>
			</tr>	
		
		</table>
	</div>

	<!-- Resultado Filtro -->
	<@s.if test="usersList!=null">
	<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
		<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
			<tr>
				<td>Usuarios encontrados</td>
			</tr>
		</table>
									
		<@vc.anchors target="contentTrx" ajaxFlag="ajax">
        	<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="usersList" id="usuarioSeguridad" pagesize=15 defaultsort=1 >
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select" style=" text-align:center; display:block" title="Acciones">
					<a href="${request.contextPath}/maestro/equipo/createEquipoUsuarioView.action?usuario.oid=${usuarioSeguridad.oid?c}&usuario.nombreUsuario=${usuarioSeguridad.username}&usuario.nombre=${usuarioSeguridad.username}&usuario.apellido=${usuarioSeguridad.fullname}&usuario.descripcion=${usuarioSeguridad.fullname}&equipoIngreso.oid=${equipoIngreso.oid?c}">
					<img  src="${request.contextPath}/common/images/seleccionar.gif" alt="Seleccionar" title="Seleccionar" border="0"></a>
				</@display.column>
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="name" title="Nombre" />
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="fullname" title="Apellido" />
			</@display.table>
		</@vc.anchors>
	</div>			
	</@s.if>