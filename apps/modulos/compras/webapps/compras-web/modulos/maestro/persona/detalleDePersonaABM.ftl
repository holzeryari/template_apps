<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign tiles=JspTaglibs["/WEB-INF/tlds/tiles-jsp.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign tiles=JspTaglibs["/WEB-INF/tlds/tiles-jsp.tld"]>

<@s.hidden id="navigationId" name="navigationId"/>
<@s.hidden id="identificador" name="detalleDePersona.pk.identificador"/>
<@s.hidden id="secuencia" name="detalleDePersona.pk.secuencia"/>

	<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
			<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos de la Persona</b></td>
			
			</tr>
		</table>

	<@tiles.insertAttribute name="persona"/>
	
	</div>
	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos de la Secuencia</b></td>
				<td>
					<div align="right">
						<@s.a templateDir="custontemplates" id="updateView" name="updateView" href="javascript://nop/" cssClass="ocultarIcono">
							<b>Modificar</b><img src="${request.contextPath}/common/images/modificar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3">
						</@s.a>						
					</div>
				</td>
			</tr>
		</table>				
						
		<@tiles.insertAttribute name="detalle"/>
		
	</div>	
		
<@tiles.insertAttribute name="formaDeComunicacion"/>
<@tiles.insertAttribute name="buttons"/>
<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/persona/detalle/updateView.action" 
  source="updateView" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=abm-detalleDePersona-updateView,flowControl=regis,navegacionIdBack={navigationId},detalleDePersona.pk.secuencia={secuencia},detalleDePersona.pk.identificador={identificador}"/>