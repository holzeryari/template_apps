<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign tiles=JspTaglibs["/WEB-INF/tlds/tiles-jsp.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

	<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
		<tr>
        	<td class="textoCampo" colspan="4">&nbsp;</td>
        </tr>
		<tr>
			<td class="textoCampo">Forma de comunicaci&oacute;n:</td>
			<td class="textoDato">
  				<@s.select 
					templateDir="custontemplates" 
					id="formaDeComunicacion" 
					cssClass="textarea"
					cssStyle="width:165px" 
					name="formaDeComunicacionDePersona.formaDeComunicacion.codigo" 
					list="formaDeComunicacionList" 
					listKey="codigo" 
					listValue="descripcion" 
					title="Forma de comunicación"
					headerKey="0"
					headerValue="Seleccionar"  
					/>
			</td>
			<td class="textoCampo">Dato:</td>
			<td class="textoDato">
  				<@s.textfield 
					templateDir="custontemplates" 
					id="formaDeComunicacionDePersona" 
					cssClass="textarea"
					cssStyle="width:160px" 
					maxLength="35"
					name="formaDeComunicacionDePersona.id.descripcion" 
					title="Calle" />
			</td>
		</tr>
		<tr><td colspan="4" class="textoCampo">&nbsp;</td></tr>
	</table>				

