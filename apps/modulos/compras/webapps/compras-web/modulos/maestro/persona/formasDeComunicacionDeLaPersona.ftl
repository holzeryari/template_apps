<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>


<@ajax.anchors target="contentTrx">	
	
	<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
		<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
	        <tr>
				<td><b>Formas de Comunicaci&oacute;n de la Persona</b></td>
				<td>
					<div align="right">
						<@s.a href="${request.contextPath}/maestro/persona/formaDeComunicacionDePersona/createView.action?formaDeComunicacionDePersona.id.identificador=${detalleDePersona.pk.identificador?c}&formaDeComunicacionDePersona.id.secuencia=${detalleDePersona.pk.secuencia?c}&navigationId=agregarFormaComunicacion&flowControl=regis&navegacionIdBack=${navigationId}">
							<b>Agregar</b><img src="${request.contextPath}/common/images/agregar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
						</@s.a>
					</div>
				</td>
			</tr>
		</table>

		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="detalleDePersona.formasDeComunicacionDePersona" id="formaDeComunicacionDePersona" defaultsort=2>
			<@display.column headerClass="tbl-contract-service-select" style=" text-align:center; display:block" title="Acciones">
<#--
				<@security.a 
					templateDir="custontemplates" 
					securityCode="CUF0001" 
					enabled="formaDeComunicacionDePersona.eraseable"
					cssClass="item"  
					name="eraseable"
					href="${request.contextPath}/maestro/persona/detalle/deleteFormaDeComunicacionDePersona.action?navigationId=${navigationId}&detalleDePersona.pk.identificador=${detalleDePersona.pk.identificador?c}&detalleDePersona.pk.secuencia=${detalleDePersona.pk.secuencia?c}&formaDeComunicacionDePersona.id.codigoDeFormaDeComunicacion=${formaDeComunicacionDePersona.id.codigoDeFormaDeComunicacion?c}&formaDeComunicacionDePersona.id.identificador=${formaDeComunicacionDePersona.id.identificador?c}&formaDeComunicacionDePersona.id.secuencia=${formaDeComunicacionDePersona.id.secuencia?c}&formaDeComunicacionDePersona.id.descripcion=${formaDeComunicacionDePersona.id.descripcion}">
					<img  src="${request.contextPath}/common/images/eliminar.gif" alt="Eliminar" title="Eliminar"  border="0">
				</@security.a>
-->
				<@s.a id="deleteFormaComunicacion" 
					name="deleteFormaComunicacion"
					href="${request.contextPath}/maestro/persona/detalle/deleteFormaDeComunicacionDePersona.action?navigationId=${navigationId}&detalleDePersona.pk.identificador=${detalleDePersona.pk.identificador?c}&detalleDePersona.pk.secuencia=${detalleDePersona.pk.secuencia?c}&formaDeComunicacionDePersona.id.codigoDeFormaDeComunicacion=${formaDeComunicacionDePersona.id.codigoDeFormaDeComunicacion?c}&formaDeComunicacionDePersona.id.identificador=${formaDeComunicacionDePersona.id.identificador?c}&formaDeComunicacionDePersona.id.secuencia=${formaDeComunicacionDePersona.id.secuencia?c}&formaDeComunicacionDePersona.id.descripcion=${formaDeComunicacionDePersona.id.descripcion}"
					cssClass="item"  
					templateDir="custontemplates">
					<img  src="${request.contextPath}/common/images/eliminar.gif" alt="Eliminar" title="Eliminar"  border="0">
				</@s.a>				

			</@display.column>			

			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="formaDeComunicacion.codigo" title="C&oacute;digo" />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="formaDeComunicacion.descripcion" title="Descripci&oacute;n" />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="id.descripcion" title="Descripci&oacute;n" />

	</@display.table>
</div>

</@ajax.anchors>

