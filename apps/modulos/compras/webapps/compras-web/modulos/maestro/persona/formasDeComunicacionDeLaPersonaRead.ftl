<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>

	<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
		<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
		    <tr>
				<td><b>Formas de Comunicaci&oacute;n de la Persona</b></td>
			</tr>
		</table>			

	<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="detalleDePersona.formasDeComunicacionDePersona" id="formaDeComunicacionDePersona" defaultsort=1>
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="formaDeComunicacion.codigo" title="C&oacute;digo" />
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="formaDeComunicacion.descripcion" title="Descripci&oacute;n" />
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="id.descripcion" title="Descripci&oacute;n" />
	</@display.table>
	</div>