<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancel" type="button" name="btnVolver" value="Cancelar" class="boton"/>
				<input id="update" type="button" name="btnModificar" value="Modificar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/persona/update.action" 
  source="update" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=${navigationId},navegacionIdBack=${navegacionIdBack},persona.oid={oid},persona.tipoPersona={tipoPersona},persona.pais.codigo={pais},persona.fechaNacimiento={fechaNacimiento},persona.sexo={sexo},persona.venceFormDGI={venceFormDGI},persona.docFiscal={docFiscal},persona.inscriptoGanancia={inscriptoGanancia},persona.docPersonal={docPersonal},persona.situacionFiscal.codigo={situacionFiscal}"/>
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=${navegacionIdBack},flowControl=back"/>
  
<#--<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/persona/validateDocsSearch.action" 
  source="personaDocList" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=${navigationId},navegacionIdBack=${navegacionIdBack},flowControl=change,persona.tipoPersona={tipoPersona},persona.pais.codigo={pais},persona.fechaNacimiento={fechaNacimiento},persona.sexo={sexo},persona.venceFormDGI={venceFormDGI},persona.docFiscal={docFiscal},persona.inscriptoGanancia={inscriptoGanancia},persona.docPersonal={docPersonal},persona.situacionFiscal.codigo={situacionFiscal},detalleDePersona.razonSocial={razonSocial},detalleDePersona.nombre={nombre},detalleDePersona.letrasCP={letrasCP},detalleDePersona.calle={calle},detalleDePersona.numeroFinca={numeroFinca},detalleDePersona.aptoCasa={aptoCasa},detalleDePersona.entreCalles={entreCalles},formaDeComunicacionDePersona.formaDeComunicacion.codigo={formaDeComunicacion},formaDeComunicacionDePersona.descripcion={formaDeComunicacionDePersona},detalleDePersona.codigoPostal.pk.secuencia={secuenciaCP},detalleDePersona.codigoPostal.pk.identificador={identificadorCP},detalleDePersona.codigoPostal.provincia.pais.codigo={pais}"/>
  -->
 
  <@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/persona/validateDocSearch.action" 
  source="personaDocList" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=${navigationId},navegacionIdBack=${navegacionIdBack},flowControl=change,persona.tipoPersona={tipoPersona},persona.pais.codigo={pais},persona.fechaNacimiento={fechaNacimiento},persona.sexo={sexo},persona.venceFormDGI={venceFormDGI},persona.docFiscal={docFiscal},persona.inscriptoGanancia={inscriptoGanancia},persona.docPersonal={docPersonal},persona.situacionFiscal.codigo={situacionFiscal},detalleDePersona.razonSocial={razonSocial},detalleDePersona.nombre={nombre},detalleDePersona.letrasCP={letrasCP},detalleDePersona.calle={calle},detalleDePersona.numeroFinca={numeroFinca},detalleDePersona.aptoCasa={aptoCasa},detalleDePersona.entreCalles={entreCalles},formaDeComunicacionDePersona.formaDeComunicacion.codigo={formaDeComunicacion},formaDeComunicacionDePersona.descripcion={formaDeComunicacionDePersona},detalleDePersona.codigoPostal.pk.secuencia={secuenciaCP},detalleDePersona.codigoPostal.pk.identificador={identificadorCP},detalleDePersona.codigoPostal.provincia.pais.codigo={pais},persona.oid={oid}"/>
  
  <@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/persona/validateCUITSearch.action" 
  source="personaDocFisList" 
  success="contentTrx" 
  failure="errorTrx"
  parameters="navigationId=${navigationId},navegacionIdBack=${navegacionIdBack},flowControl=change,persona.tipoPersona={tipoPersona},persona.pais.codigo={pais},persona.fechaNacimiento={fechaNacimiento},persona.sexo={sexo},persona.venceFormDGI={venceFormDGI},persona.docFiscal={docFiscal},persona.inscriptoGanancia={inscriptoGanancia},persona.docPersonal={docPersonal},persona.situacionFiscal.codigo={situacionFiscal},detalleDePersona.razonSocial={razonSocial},detalleDePersona.nombre={nombre},detalleDePersona.letrasCP={letrasCP},detalleDePersona.calle={calle},detalleDePersona.numeroFinca={numeroFinca},detalleDePersona.aptoCasa={aptoCasa},detalleDePersona.entreCalles={entreCalles},formaDeComunicacionDePersona.formaDeComunicacion.codigo={formaDeComunicacion},formaDeComunicacionDePersona.descripcion={formaDeComunicacionDePersona},detalleDePersona.codigoPostal.pk.secuencia={secuenciaCP},detalleDePersona.codigoPostal.pk.identificador={identificadorCP},detalleDePersona.codigoPostal.provincia.pais.codigo={pais},persona.oid={oid}"/> 