<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign tiles=JspTaglibs["/WEB-INF/tlds/tiles-jsp.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>

	<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>
			<tr>
				<td class="textoCampo">Identificador:</td>
				<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="detalleDePersona.pk.identificador"/>
					<@s.hidden id="identificador" name="detalleDePersona.pk.identificador"/>
				</td>
				<td class="textoCampo">Secuencia:</td>
				<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="detalleDePersona.pk.secuencia"/>
					<@s.hidden id="secuencia" name="detalleDePersona.pk.secuencia"/>
				</td>	      			
			</tr>	
			<tr>
				<td class="textoCampo">Tipo de Persona:</td>
				<td class="textoDato" colspan="3">
		  			<@s.select 
						templateDir="custontemplates" 
						id="tipoPersona" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="detalleDePersona.persona.tipoPersona" 
						list="tipoPersonaList" 
						listKey="key" 
						listValue="description" 
						value="detalleDePersona.persona.tipoPersona.ordinal()"
						title="Tipo de Persona"
						headerKey="-1"
						headerValue="Seleccionar"  
						/>
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Raz&oacute;n social:</td>
				<td class="textoDato">
		  			<@s.textfield 
						templateDir="custontemplates" 
						id="razonSocial" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="detalleDePersona.razonSocial" 
						title="Raz�n social" />
					</td>
						
				<td class="textoCampo">Nombre:</td>
				<td class="textoDato">
		  			<@s.textfield 
						templateDir="custontemplates" 
						id="nombre" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="detalleDePersona.nombre" 
						title="Nombre" />
					</td>
			</tr>
			<tr>
				<td class="textoCampo">N&uacute;mero de Documento:</td>
				<td class="textoDato">
					<@s.textfield 
						templateDir="custontemplates" 
						id="docPersonal" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="detalleDePersona.persona.docPersonal" 
						title="N&uacute;mero de Documento" />	
				</td>	
				<td class="textoCampo">CUIT/CUIL:</td>
				<td class="textoDato">
					<@s.textfield 
						templateDir="custontemplates" 
						id="docFiscal" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="detalleDePersona.persona.docFiscal" 
						title="CUIT/CUIL" />	
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Fecha Nacimiento/Inicio:</td>
				<td class="textoDato">
					<@vc.rowCalendar 
						templateDir="custontemplates" 
						id="fechaNacimiento" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="detalleDePersona.persona.fechaNacimiento" 
						title="Fecha de Nacimiento" 
						/>
				</td>
				<td class="textoCampo">Sexo:</td>
				<td class="textoDato">
		  			<@s.select 
						templateDir="custontemplates" 
						id="sexo" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="detalleDePersona.persona.sexo" 
						list="sexoList" 
						listKey="key" 
						listValue="description" 
						value="detalleDePersona.persona.sexo.ordinal()"
						title="Sexo"
						headerKey=""
						headerValue="Seleccionar"
						/></td>
			</tr>
			<tr><td colspan="4" class="textoCampo">&nbsp;</td></tr>
		</table>				
										
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Direcci&oacute;n Fiscal</b></td>
				
			</tr>
		</table>
		
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>
			<tr>
				<td class="textoCampo">C&oacute;digo postal:</td>
				<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="detalleDePersona.codigoPostal.pk.identificador"/>
					<@s.hidden id="identificadorCP" name="detalleDePersona.codigoPostal.pk.identificador"/>
					<@s.hidden id="secuenciaCP" name="detalleDePersona.codigoPostal.pk.secuencia"/>
					<@s.a templateDir="custontemplates" id="codigoPostalDPView" name="codigoPostalDPView" href="javascript://nop/" cssClass="ocultarIcono">
						<img src="${request.contextPath}/common/images/buscar.gif" title="C&oacute;digo postal" align="top" border="0" hspace="3">	
					</@s.a>	
				</td>
				<td class="textoCampo">Localidad:</td>
				<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="detalleDePersona.codigoPostal.descripcion"/>
				</td>										
			</tr>
			<tr>
				<td class="textoCampo">Provincia:</td>
				<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="detalleDePersona.codigoPostal.provincia.descripcion"/></td>
				<td class="textoCampo">Pais:</td>
				<td class="textoDato"><@s.property default="&nbsp;" escape=false value="detalleDePersona.codigoPostal.provincia.pais.descripcion"/></td>
			</tr>
			<tr>
				<td class="textoCampo">Secuencia CP:</td>
				<td class="textoDato"><@s.property default="&nbsp;" escape=false value="detalleDePersona.codigoPostal.pk.secuencia"/></td>
				<td class="textoCampo">Letra CP:</td>
				<td class="textoDato">
					<@s.textfield 
						templateDir="custontemplates" 
						id="letrasCP" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="detalleDePersona.letrasCP" 
						title="C�digo postal" />
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Calle:</td>
				<td class="textoDato">
		  			<@s.textfield 
						templateDir="custontemplates" 
						id="calle" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="detalleDePersona.calle" 
						title="Calle" />
				</td>
				<td class="textoCampo">N&uacute;mero de Finca:</td>
				<td class="textoDato">
					<@s.textfield 
						templateDir="custontemplates" 
						id="numeroFinca" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="detalleDePersona.numeroFinca" 
						title="N�mero de Finca" /></td>
			</tr>
			<tr>	
				<td class="textoCampo">Depto:</td>
				<td class="textoDato">
		  			<@s.textfield 
						templateDir="custontemplates" 
						id="aptoCasa" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="detalleDePersona.aptoCasa" 
						title="Depto" /></td>
				
				<td class="textoCampo">Entre Calles:</td>
				<td class="textoDato">
					<@s.textfield 
						templateDir="custontemplates" 
						id="entreCalles" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="detalleDePersona.entreCalles" 
						title="Depto" /></td>
			</tr>
			<tr><td colspan="4" class="textoCampo">&nbsp;</td></tr>
		</table>
		
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Condici&oacute;n Fiscal</b></td>
				
			</tr>
		</table>		
	
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>
        	<tr>
				<td class="textoCampo">Fecha Vencimiento DGI:</td>
				<td class="textoDato">
					<@vc.rowCalendar 
						templateDir="custontemplates" 
						id="venceFormDGI" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="detalleDePersona.persona.venceFormDGI" 
						title="Fecha Vencimiento DGI" />
				</td>
				<td class="textoCampo">Condici&oacute;n Fiscal API:</td>
				<td class="textoDato">
					<@s.select 
						templateDir="custontemplates" 
						id="inscriptoGanancia" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="detalleDePersona.persona.inscriptoGanancia"
						value="detalleDePersona.persona.inscriptoGanancia.ordinal()" 
						list="inscriptoGananciaList" 
						listKey="key" 
						listValue="description"  
						title="Condici�n Fiscal Impuesto Ganancias"
						headerKey="-1"
						headerValue="Seleccionar"
						/></td>		
			</tr>
			<tr>	
				<td class="textoCampo">Condici&oacute;n Fiscal IVA:</td>
				<td class="textoDato" colsapn="3">
		  			<@s.select 
						templateDir="custontemplates" 
						id="situacionFiscal" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="detalleDePersona.persona.situacionFiscal.codigo" 
						list="situacionFiscalList" 
						listKey="codigo" 
						listValue="descripcion" 
						title="Documento Fiscal V&aacute;lido"
						headerKey=""
						headerValue="Seleccionar"  
						/></td>
			</tr>
		</table>