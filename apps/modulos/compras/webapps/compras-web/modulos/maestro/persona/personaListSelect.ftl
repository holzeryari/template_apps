<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>

<@s.hidden id="navigationId" name="navigationId"/>
<@s.hidden id="navegacionIdBack" name="navegacionIdBack"/>


<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
        	<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
			<tr>
				<td><b>Datos de la Persona</b></td>
				<td>					
					<div align="right">
						<#if navegacionIdBack=='buscar-proveedores' >
							<@s.a id="create" 
								name="create"
								href="javascript://nop/" 
								cssClass="ocultarIcono" 
								templateDir="custontemplates">
								<b>Agregar</b><img src="${request.contextPath}/common/images/agregar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
							</@s.a>
						</#if>
									
					</div>
				</td>
			</tr>
		</table>
			
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>		
			<tr>
				<td class="textoCampo">Tipo de Persona:</td>
				<td class="textoDato">
	      			<@s.select 
						templateDir="custontemplates" 
						id="tipoPersona" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="persona.tipoPersona" 
						list="tipoPersonaList" 
						listKey="key" 
						listValue="description" 
						value="persona.tipoPersona.ordinal()"
						title="Tipo de Persona"
						headerKey="-1"
						headerValue="Todos"  
						/></td>
				
				<td class="textoCampo">Raz&oacute;n Social:</td>
      			<td class="textoDato">
      				<@s.textfield 
      					templateDir="custontemplates" 
						id="razonSocial" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="detalleDePersona.razonSocial" 
						title="Raz&oacute;n Social" /></td>
			</tr>
			
			<tr>
	  			<td class="textoCampo">Nombre:</td>
      			<td class="textoDato">
      				<@s.textfield 
      					templateDir="custontemplates" 
						id="nombre" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="detalleDePersona.nombre" 
						title="Nombre" /></td>
														
				<td class="textoCampo">Sexo:</td>
      			<td class="textoDato">
	      			<@s.select 
						templateDir="custontemplates" 
						id="sexo" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="persona.sexo" 
						list="sexoList" 
						listKey="key" 
						listValue="description" 
						value="persona.sexo.ordinal()"
						title="Sexo"
						headerKey="0"
						headerValue="Todos"  
						/></td>
			</tr>

			<tr>	
				<td class="textoCampo">CUIT/CUIL:</td>
      			<td class="textoDato">
      				<@s.textfield 
      					templateDir="custontemplates" 
						id="docFiscal" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="persona.docFiscal" 
						title="CUIT/CUIL" /></td>

				<td class="textoCampo">N&uacute;mero de Documento:</td>
      			<td class="textoDato">
      				<@s.textfield 
      					templateDir="custontemplates" 
						id="docPersonal" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="persona.docPersonal" 
						title="N&uacute;mero de Documento" /></td>
			</tr>
			
			<tr>
    			<td class="textoCampo">Pais:</td>
				<td class="textoDato">
	      			<@s.select 
						templateDir="custontemplates" 
						id="detalleDePersona.codigoPostal.provincia.pais.codigo" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="detalleDePersona.codigoPostal.provincia.pais.codigo" 
						list="paisList" 
						listKey="codigo" 
						listValue="descripcion" 
						title="Pais"
						headerKey="-1"
						headerValue="Todos"  
						value="detalleDePersona.codigoPostal.provincia.pais.codigo"
						/></td>
						
    			<td class="textoCampo">Provincia:</td>
      			<td class="textoDato">
      				<@s.select 
						templateDir="custontemplates" 
						id="detalleDePersona.codigoPostal.provincia.codigo" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="detalleDePersona.codigoPostal.provincia.codigo" 
						list="provinciaList" 
						listKey="codigo" 
						listValue="descripcion"  
						title="Provincia"
						headerKey="-1"
						headerValue="Todos"
						value="detalleDePersona.codigoPostal.provincia.codigo"
						/></td>
			</tr>
			<tr>
				<td class="textoCampo">C&oacute;digo postal:</td>
				<td class="textoDato"colspan="3">
					<@s.property default="&nbsp;" escape=false value="detalleDePersona.codigoPostal.pk.identificador"/> <#if detalleDePersona?exists && detalleDePersona.codigoPostal?exists && detalleDePersona.codigoPostal.pk.identificador?exists > - </#if> <@s.property default="&nbsp;" escape=false value="detalleDePersona.codigoPostal.descripcion" />	
					<@s.hidden id="secuenciaCP" name="detalleDePersona.codigoPostal.pk.secuencia"/>
					<@s.hidden id="descripcionCodigoPostal" name="detalleDePersona.codigoPostal.descripcion"/>
					<@s.hidden id="codigoPostal" name="detalleDePersona.codigoPostal.pk.identificador"/>
					<@s.a templateDir="custontemplates" id="codigoPostalView" name="codigoPostalView" href="javascript://nop/" cssClass="ocultarIcono">
						<img src="${request.contextPath}/common/images/buscar.gif" title="C&oacute;digo postal" align="top" border="0" hspace="3">	
					</@s.a>
				</td>
			</tr>
			
			<tr>
	    		<td colspan="4" class="lineaGris" align="right">
	      			<input id="limpiar" type="button" name="btnVolver" value="Limpiar" class="boton"/>		
	      			<input id="buscar" type="button" name="btnVolver" value="Buscar" class="boton"/>
	    		</td>
			</tr>	
		</table>
	</div>
	<!-- Resultado Filtro -->
	<@s.if test="detalleDePersonaList!=null">
	<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">			
			<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>Personas encontradas</td>
				</tr>
			</table>				
			<@ajax.anchors target="contentTrx">			
				<@display.table 
          			class="tablaDetalleCuerpo" 
          			cellpadding="3" 
          			name="detalleDePersonaList" 
          			id="detalleDePersonaSelect" 
          			pagesize=15 
          			defaultsort=1
          			partialList=true 
          			size="recordSize"
          			keepStatus=true
          			excludedParams="resetFlag">
					
					<@s.if test="nameSpaceSelect!=''">
						<#assign ref="${request.contextPath}/maestro/persona/select.action?">
						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select" class="botoneraAnchoCon1" title="Acciones">
						<div class="alineacion">
											
					<@s.a id="select${detalleDePersonaSelect.pk.identificador?c}select${detalleDePersonaSelect.pk.secuencia?c}" href="javascript://nop/" name="select${detalleDePersonaSelect.pk.identificador?c}select${detalleDePersonaSelect.pk.secuencia?c}" cssClass="no-rewrite" >
						<img src="${request.contextPath}/common/images/seleccionar.gif" alt="Seleccionar" title="Seleccionar" width="16" height="16" border="0">
					</@s.a>
					<@vc.htmlContent 
					  baseUrl="${request.contextPath}/maestro/persona/select.action" 
					  source="select${detalleDePersonaSelect.pk.identificador?c}select${detalleDePersonaSelect.pk.secuencia?c}" 
					  success="contentTrx" 
					  failure="errorTrx" 
					  parameters="detalleDePersona.pk.identificador=${detalleDePersonaSelect.pk.identificador?c},detalleDePersona.pk.secuencia=${detalleDePersonaSelect.pk.secuencia?c},navegacionIdBack=${navegacionIdBack},navigationId=${navigationId},nameSpaceSelect=${nameSpaceSelect},actionNameSelect=${actionNameSelect}"/>									
							
						</div>
						</@display.column>						  															
					</@s.if>
					<@s.else>
						<#assign ref="${request.contextPath}/maestro/persona/selectBack.action?">
						<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select"  title="Acciones">
						<div class="alineacion">
							<@s.a href="${ref}detalleDePersona.pk.identificador=${detalleDePersonaSelect.pk.identificador?c}&detalleDePersona.pk.secuencia=${detalleDePersonaSelect.pk.secuencia?c}&navegacionIdBack=${navegacionIdBack}&navigationId=${navigationId}">
								<img src="${request.contextPath}/common/images/seleccionar.gif" alt="Seleccionar" title="Seleccionar" width="16" height="16" border="0">
							</@s.a>
						</div>
						</@display.column>
					</@s.else>
					
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="persona.tipoPersona" title="Tipo De persona" />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="razonSocial" title="Raz&oacute;n Social" />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="nombre" title="Nombre" /> 
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="persona.sexo" title="Sexo" />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="persona.docPersonal" title="Documento" />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="persona.docFiscal" title="CUIT/CUIL" />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="codigoPostal.pk.identificador" title="CP" />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="codigoPostal.descripcion" title="Localidad" />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="codigoPostal.provincia.descripcion" title="Provincia" />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="codigoPostal.provincia.pais.descripcion" title="Pais" />
			
					
				</@display.table>
			</@ajax.anchors>
			</div>
		</@s.if>
		<div id="capaBotonera" class="capaBotonera">
			<table id="tablaBotonera" class="tablaBotonera">
				<tr> 
					<td align="left">
						<input id="cancel" type="button" name="btnVolver" value="Volver" class="boton"/>
					</td>
				</tr>	
			</table>
		</div>	
								 
		<@vc.htmlContent 
		  baseUrl="${request.contextPath}/compras/flowControl.action" 
		  source="cancel" 
		  success="contentTrx" 
		  failure="errorTrx" 
		  parameters="navigationId={navegacionIdBack},flowControl=back"/>
								  									
							
<@ajax.select
  baseUrl="${request.contextPath}/maestro/persona/codigoPostal/provinciasView.action"
  source="detalleDePersona.codigoPostal.provincia.pais.codigo"
  target="detalleDePersona.codigoPostal.provincia.codigo"
  parameters="pais.codigo={detalleDePersona.codigoPostal.provincia.pais.codigo}" 
  parser="new ResponseXmlParser()"/>
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/persona/selectSearch.action" 
  source="buscar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="resetFlag=true,navigationId={navigationId},navegacionIdBack={navegacionIdBack},flowControl=regis,nameSpaceSelect=${nameSpaceSelect},actionNameSelect=${actionNameSelect},persona.tipoPersona={tipoPersona},detalleDePersona.nombre={nombre},persona.docFiscal={docFiscal},detalleDePersona.razonSocial={razonSocial},persona.sexo={sexo},persona.docPersonal={docPersonal},detalleDePersona.codigoPostal.provincia.codigo={detalleDePersona.codigoPostal.provincia.codigo},detalleDePersona.codigoPostal.provincia.pais.codigo={detalleDePersona.codigoPostal.provincia.pais.codigo},detalleDePersona.codigoPostal.pk.identificador={codigoPostal},detalleDePersona.codigoPostal.pk.secuencia={secuenciaCP}"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/persona/selectView.action" 
  source="limpiar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},navegacionIdBack={navegacionIdBack},flowControl=regis,nameSpaceSelect=${nameSpaceSelect},actionNameSelect=${actionNameSelect}"/>
  
  <@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/persona/selectCodigoPostalSelectPersona.action" 
  source="codigoPostalView" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},navegacionIdBack={navegacionIdBack},flowControl=change,nameSpaceSelect=${nameSpaceSelect},actionNameSelect=${actionNameSelect},persona.tipoPersona={tipoPersona},detalleDePersona.nombre={nombre},persona.docFiscal={docFiscal},detalleDePersona.razonSocial={razonSocial},persona.sexo={sexo},persona.docPersonal={docPersonal},detalleDePersona.codigoPostal.provincia.codigo={detalleDePersona.codigoPostal.provincia.codigo},detalleDePersona.codigoPostal.provincia.pais.codigo={detalleDePersona.codigoPostal.provincia.pais.codigo},detalleDePersona.codigoPostal.pk.identificador={codigoPostal},detalleDePersona.codigoPostal.pk.secuencia={secuenciaCP}"/>
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/persona/createViewFromSelectPersona.action" 
  source="create" 
  success="contentTrx" 
  failure="errorTrx"  
    parameters="navigationId={navigationId},flowControl=change,navegacionIdBack={navegacionIdBack},nameSpaceSelect=${nameSpaceSelect},actionNameSelect=${actionNameSelect},persona.tipoPersona={tipoPersona},detalleDePersona.nombre={nombre},persona.docFiscal={docFiscal},detalleDePersona.razonSocial={razonSocial},persona.sexo={sexo},persona.docPersonal={docPersonal},detalleDePersona.codigoPostal.provincia.codigo={detalleDePersona.codigoPostal.provincia.codigo},detalleDePersona.codigoPostal.provincia.pais.codigo={detalleDePersona.codigoPostal.provincia.pais.codigo},detalleDePersona.codigoPostal.pk.identificador={codigoPostal},detalleDePersona.codigoPostal.pk.secuencia={secuenciaCP}"/>
