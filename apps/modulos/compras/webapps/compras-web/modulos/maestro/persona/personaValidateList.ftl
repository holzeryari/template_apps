<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>
<@s.hidden id="navigationId" name="navigationId"/>


<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>

	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
        	<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	

	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
			<tr>
				<td><b>Datos de la Persona</b></td>	
			</tr>
		</table>
		
			<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>
        	
        	<tr>
				<td class="textoCampo">Tipo de Persona:</td>
				<td class="textoDato">
	      			<@s.select 
						templateDir="custontemplates" 
						id="tipoPersona" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="persona.tipoPersona" 
						list="tipoPersonaList" 
						listKey="key" 
						listValue="description" 
						value="persona.tipoPersona.ordinal()"
						title="Tipo de Persona"
						headerKey="-1"
						headerValue="Seleccionar"  
						/></td>
				
				<td class="textoCampo">Raz&oacute;n Social:</td>
      			<td class="textoDato"><@s.textfield 
      					templateDir="custontemplates" 
						id="razonSocial" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="detalleDePersona.razonSocial" 
						title="Raz&oacute;n Social" /></td>
			</tr>
			
			<tr>
	  			<td class="textoCampo">Nombre:</td>
      			<td class="textoDato"><@s.textfield 
      					templateDir="custontemplates" 
						id="nombre" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="detalleDePersona.nombre" 
						title="Nombre" /></td>
														
				<td class="textoCampo">Sexo:</td>
      			<td class="textoDato">
	      			<@s.select 
						templateDir="custontemplates" 
						id="sexo" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="persona.sexo" 
						list="sexoList" 
						listKey="key" 
						listValue="description" 
						value="persona.sexo.ordinal()"
						title="Sexo"
						headerKey="0"
						headerValue="Seleccionar"  
						/></td>
			</tr>

			<tr>	
				<td class="textoCampo">CUIT/CUIL:</td>
      			<td class="textoDato"><@s.textfield 
      					templateDir="custontemplates" 
						id="docFiscal" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="persona.docFiscal" 
						title="CUIT/CUIL" /></td>

				<td class="textoCampo">N&uacute;mero de Documento:</td>
      			<td class="textoDato"><@s.textfield 
      					templateDir="custontemplates" 
						id="docPersonal" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="persona.docPersonal" 
						title="N&uacute;mero de Documento" /></td>
			</tr>
			
			<tr>
    			<td class="textoCampo">Pais:</td>
				<td class="textoDato">
	      			<@s.select 
						templateDir="custontemplates" 
						id="detalleDePersona.codigoPostal.provincia.pais.codigo" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="detalleDePersona.codigoPostal.provincia.pais.codigo" 
						list="paisList" 
						listKey="codigo" 
						listValue="descripcion" 
						title="Pais"
						headerKey="0"
						headerValue="Seleccionar"  
						/></td>
						
    			<td class="textoCampo">Provincia:</td>
      			<td class="textoDato"> 
      				<@s.select 
						templateDir="custontemplates" 
						id="detalleDePersona.codigoPostal.provincia.codigo" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="detalleDePersona.codigoPostal.provincia.codigo" 
						list="provinciaList" 
						listKey="codigo" 
						listValue="descripcion"  
						title="Provincia"
						headerKey="0"
						headerValue="Seleccionar"
						/></td>
			</tr>
			<tr>
				<td class="textoCampo">C&oacute;digo postal:</td>
				<td class="textoDato">
					<#if (detalleDePersona?exists && detalleDePersona.codigoPostal?exists && detalleDePersona.codigoPostal.pk?exists && detalleDePersona.codigoPostal.pk.identificador?exists)>
					
						<@s.property default="&nbsp;" escape=false value="detalleDePersona.codigoPostal.pk.identificador"/>
							-									 
						<@s.property default="&nbsp;" escape=false value="detalleDePersona.codigoPostal.descripcion" />										
						<@s.hidden id="secuenciaCP" name="detalleDePersona.codigoPostal.pk.secuencia"/>
						<@s.hidden id="descripcionCodigoPostal" name="detalleDePersona.codigoPostal.descripcion"/>
						<@s.hidden id="codigoPostal" name="detalleDePersona.codigoPostal.pk.identificador"/>
					
					</#if>
					<@s.a templateDir="custontemplates" id="codigoPostalView" name="codigoPostalView" href="javascript://nop/" cssClass="texto1">
						<img src="${request.contextPath}/common/images/buscar.gif" title="C&oacute;digo postal" align="top" border="0" hspace="3">	
					</@s.a>
				</td>
						
				<td class="textoCampo">&nbsp;</td>
				<td class="textoDato">&nbsp;</td>
				
			</tr>	
	
		</table>
	</div>

					
			            <!-- Resultado Filtro -->
			<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">			
				<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
					<tr>
						<td>Personas encontradas</td>
					</tr>
				</table>
									
						<@ajax.anchors target="contentTrx">	

			          		<@display.table    
			          		class="tablaDetalleCuerpo"  cellpadding="3" 
			          		name="detalleDePersonaList" id="detalleDePersona" pagesize=15 defaultsort=1 >
								
								<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select" style=" text-align:center; display:block" title="Acciones">
									<@security.a templateDir="custontemplates" 
										securityCode="CUF0001" 
										name="selectView"
										enabled="persona.updatable"
										cssClass="item" 
										href="${request.contextPath}/maestro/persona/administrarView.action?persona.oid=${detalleDePersona.persona.oid?c}&navigationId=administrar-persona&flowControl=regis"
										><img src="${request.contextPath}/common/images/seleccionar.gif" alt="Seleccionar" title="Seleccionar" border="0"></@security.a>&nbsp;
								</@display.column>
								<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="persona.tipoPersona" title="Tipo De persona" />
								<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="razonSocial" title="Raz&oacute;n Social" />
								<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="nombre" title="Nombre" /> 
								<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="persona.sexo" title="Sexo" />
								<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="persona.docPersonal" title="Documento" />
								<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="persona.docFiscal" title="CUIT/CUIL" />
								<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="codigoPostal.pk.identificador" title="CP" />
								<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="codigoPostal.descripcion" title="Localidad" />
								<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="codigoPostal.provincia.descripcion" title="Provincia" />
								<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="codigoPostal.provincia.pais.descripcion" title="Pais" />
								
							</@display.table>
						</@ajax.anchors>
						</div>	
						
						<div id="capaBotonera" class="capaBotonera">
						<table id="tablaBotonera" class="tablaBotonera">
							<tr> 
								<td align="left">
									<input id="cancel" type="button" name="btnVolver" value="Volver" class="boton"/>
								</td>
							</tr>	
						</table>
					</div>	


 <@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=back"/>
