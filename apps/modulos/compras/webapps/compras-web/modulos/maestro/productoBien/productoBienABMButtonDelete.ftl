<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>



<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancel" type="button" name="btnVolver" value="Cancelar" class="boton"/>
				<input id="store" type="button" name="btnEliminar" value="Eliminar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/productoBien/delete.action" 
  source="store" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="productoBien.oid={productoBien.oid},productoBien.tipo={productoBien.tipoProductoBien},productoBien.versionNumber={productoBien.versionNumber}"/>
  
  <@vc.htmlContent 
baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=buscar-productosBienes,flowControl=back"/>