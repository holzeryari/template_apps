<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign tiles=JspTaglibs["/WEB-INF/tlds/tiles-jsp.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>


<@s.hidden id="productoBien.rubro.oid" name="productoBien.rubro.oid"/>
<@s.hidden id="estadoDuro" name="estadoDuro"/>
<@s.hidden id="busquedaPorRubros" name="busquedaPorRubros"/>
<@s.hidden id="productoBien.rubro.descripcion" name="productoBien.rubro.descripcion"/>

  <@s.if test="estadoDuro == true">
	<@s.hidden id="productoBien.estado" name="productoBien.estado.ordinal()"/> 
  </@s.if>

<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
        	<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
			<tr>
				<td><b>Datos del Producto/Bien</b></td>
			</tr>		
		</table>				
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>
        	<tr>
  				<td class="textoCampo">C&oacute;digo:</td>
  				<td class="textoDato">
  					<@s.textfield 
					templateDir="custontemplates" 
					id="productoBien.oid"
					cssClass="textarea"
					cssStyle="width:160px" 
					name="productoBien.oid" 
					title="Codigo" />
			</td>
			<td class="textoCampo">Descripci&oacute;n:</td>
			<td class="textoDato">
				<@s.textfield 
  					templateDir="custontemplates" 
					id="productoBien.descripcion"
					cssClass="textarea"
					cssStyle="width:160px" 
					name="productoBien.descripcion" 
					title="Descripcion"/>
			</td>
		</tr>
		<tr>
			<td class="textoCampo">Rubro:</td>
  			<td class="textoDato">
				<@s.property default="&nbsp;" escape=false value="productoBien.rubro.descripcion" />
				<@s.a templateDir="custontemplates" id="seleccionarRubro" name="seleccionarRubro" href="javascript://nop/">
				<img src="${request.contextPath}/common/images/buscar.gif" border="0" align="absmiddle" hspace="3" >
				</@s.a>		
			</td>
			<td class="textoCampo">Tipo:</td>
			<td class="textoDato">
				<@s.select 
					templateDir="custontemplates" 
					id="productoBien.tipo" 
					cssClass="textarea"
					cssStyle="width:165px" 
					name="productoBien.tipo" 
					list="tipoList" 
					listKey="key" 
					listValue="description" 
					value="productoBien.tipo.ordinal()"
					title="Tipo de Producto/Bien"
					headerKey="0"
					headerValue="Todos"   
					/>
			</td>
		</tr>
		<tr>
			<td class="textoCampo">Es Cr&iacute;tico: </td>
				<td  class="textoDato">
	      			<@s.select 
						templateDir="custontemplates" 
						id="productoBien.critico" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="productoBien.critico" 
						list="criticoList" 
						listKey="key" 
						listValue="description" 
						value="productoBien.critico.ordinal()"
						title="Es Critico"
						headerKey="0"
						headerValue="Seleccionar" 
						 />
			</td>
			<td class="textoCampo">Estado:</td>
  				<td class="textoDato">
  				<@s.select 
					templateDir="custontemplates" 
					id="productoBien.estado" 
					cssClass="textarea"
					cssStyle="width:165px" 
					name="productoBien.estado" 
					list="estadoList" 
					listKey="key" 
					listValue="description" 
					value="productoBien.estado.ordinal()"
					title="Estado"
					headerKey="0"
					headerValue="Todos"							
					/>
			</td>
		</tr>	
		<tr>
	    	<td colspan="4" class="lineaGris" align="right">
	      		<input id="limpiar" type="button" name="btnVolver" value="Limpiar" class="boton"/>		
	      		<input id="buscar" type="button" name="btnVolver" value="Buscar" class="boton"/>
	    	</td>
		</tr>	
	</table>
	</div>
	
	<!-- Resultado Filtro -->
	<@s.if test="productoBienList!=null">
		<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
		<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>Productos/Bienes encontrados</td>
				</tr>
		</table>
		
		<@ajax.anchors target="contentTrx">
		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="productoBienList" id="productoBienSelect" pagesize=15 defaultsort=1 partialList=true size="recordSize" keepStatus=true excludedParams="resetFlag">
								
		<@s.if test="nameSpaceSelect!=''">								
			<#assign ref="${request.contextPath}/maestro/productoBien/selectProductoBien.action?">										
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select" style=" text-align:center; display:block" title="Acciones">			
			<@s.a href="${ref}productoBien.oid=${productoBienSelect.oid?c}&productoBien.descripcion=${productoBienSelect.descripcion}&productoBien.tipo=${productoBienSelect.tipo.ordinal()}&navegacionIdBack=${navegacionIdBack}&navigationId=${navigationId}&nameSpaceSelect=${nameSpaceSelect}&actionNameSelect=${actionNameSelect}&oidParameter=${oidParameter?c}"><img src="${request.contextPath}/common/images/seleccionar.gif" alt="Seleccionar" title="Seleccionar" width="16" height="16" border="0"></@s.a>
			</@display.column>								  															
		</@s.if>
		<@s.else>
			<#assign ref="${request.contextPath}/maestro/productoBien/selectProductoBienBack.action?">											
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select" style=" text-align:center; display:block" title="Acciones">			
			<@s.a href="${ref}productoBien.oid=${productoBienSelect.oid?c}&productoBien.descripcion=${productoBienSelect.descripcion}&productoBien.tipo=${productoBienSelect.tipo.ordinal()}&navegacionIdBack=${navegacionIdBack}&navigationId=${navigationId}"><img src="${request.contextPath}/common/images/seleccionar.gif" alt="Seleccionar" title="Seleccionar" width="16" height="16" border="0"></@s.a>
			</@display.column>	
		</@s.else>
								
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="oid" title="C&oacute;digo" />
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="descripcion" title="Descripci&oacute;n" />
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="rubro.descripcion" title="Rubro" />
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="tipo" title="Tipo" />								
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="estado" title="Estado" />
		</@display.table>								
		</@ajax.anchors>	
		</div>
		</@s.if>

		<div id="capaBotonera" class="capaBotonera">
		<table id="tablaBotonera" class="tablaBotonera">
			<tr> 
				<td align="left">
					<input id="cancel" type="button" name="btnVolver" value="Volver" class="boton"/>
				</td>
			</tr>	
		</table>
		</div>
						
<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/productoBien/selectProductoBienSearch.action" 
  source="buscar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="resetFlag=true,navigationId=${navigationId},flowControl=regis,productoBien.oid={productoBien.oid},productoBien.descripcion={productoBien.descripcion},productoBien.rubro.oid={productoBien.rubro.oid},productoBien.rubro.descripcion={productoBien.rubro.descripcion},productoBien.estado={productoBien.estado},navegacionIdBack=${navegacionIdBack},nameSpaceSelect=${nameSpaceSelect},actionNameSelect=${actionNameSelect},oidParameter=${oidParameter?c},estadoDuro={estadoDuro},busquedaPorRubros={busquedaPorRubros},productoBien.tipo={productoBien.tipo},productoBien.critico={productoBien.critico}"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/productoBien/selectProductoBienView.action" 
  source="limpiar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navegacionIdBack=${navegacionIdBack},navigationId=${navigationId},flowControl=regis,nameSpaceSelect=${nameSpaceSelect},actionNameSelect=${actionNameSelect},oidParameter=${oidParameter?c},estadoDuro={estadoDuro},busquedaPorRubros={busquedaPorRubros},productoBien.estado={productoBien.estado}"/>
  
  <@vc.htmlContent 	
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId=${navegacionIdBack},flowControl=back"/>
 
 <@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/productoBien/selectRubroSearch.action" 
  source="seleccionarRubro" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId=${navigationId},flowControl=change,productoBien.oid={productoBien.oid},productoBien.descripcion={productoBien.descripcion},productoBien.rubro.oid={productoBien.rubro.oid},productoBien.rubro.descripcion={productoBien.rubro.descripcion},productoBien.estado={productoBien.estado},navegacionIdBack=${navegacionIdBack},nameSpaceSelect=${nameSpaceSelect},actionNameSelect=${actionNameSelect},oidParameter=${oidParameter?c},estadoDuro={estadoDuro},productoBien.tipo={productoBien.tipo},productoBien.critico={productoBien.critico}"/>
  
