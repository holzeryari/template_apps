<#setting locale="es_AR">

<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign tiles=JspTaglibs["/WEB-INF/tlds/tiles-jsp.tld"]>
<@s.hidden id="producto.oid" name="producto.oid"/>


	<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
			<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos del Producto/Bien</b></td>
				
			</tr>
		</table>
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>										
			<tr>
      			<td class="textoCampo">C&oacute;digo:</td>
      			<td class="textoDato"><@s.property default="&nbsp;" escape=false value="producto.oid"/></td>
      			<td  class="textoCampo">Descripci&oacute;n: </td>
				<td class="textoDato">
				<@s.property default="&nbsp;" escape=false value="producto.descripcion"/>											
				</td>	      			
			</tr>	
			<tr>
      			<td class="textoCampo">Tipo:</td>
	      		<td class="textoDato">
	      		<@s.property default="&nbsp;" escape=false value="producto.tipo"/>						      			
	      		</td>		      		
										
			<td class="textoCampo">Rubro:</td>
  			<td class="textoDato">
  			<@s.property default="&nbsp;" escape=false value="producto.rubro.descripcion"/>							     		
				</td>
    		</tr>
			<tr>
				<td class="textoCampo">Marca: </td>
				<td  class="textoDato">
				<@s.property default="&nbsp;" escape=false value="producto.marca"/>										
				</td>
				<td class="textoCampo">Modelo: </td>
				<td  class="textoDato">
				<@s.property default="&nbsp;" escape=false value="producto.modelo"/>
				</td>
    		</tr>
			<tr>
				<td class="textoCampo">C&oacute;digo de Barra: </td>
				<td  class="textoDato">
				<@s.property default="&nbsp;" escape=false value="producto.codigoBarra"/>										
				</td>
				<td class="textoCampo">Es Cr&iacute;tico: </td>
				<td  class="textoDato">
				<@s.property default="&nbsp;" escape=false value="producto.critico"/>						      			
				</td>
    		</tr>
			<tr>
				<td class="textoCampo">Reposici&oacute;n Autm&aacute;tica: </td>
				<td  class="textoDato">
				<@s.property default="&nbsp;" escape=false value="producto.reposicionAutomatica"/>						      				
				</td>
				<td class="textoCampo">Existencia M&iacute;nima: </td>
				<td  class="textoDato">
				<@s.property default="&nbsp;" escape=false value="producto.existenciaMinima"/>										
				</td>
    		</tr>
    		<tr>
    			<td class="textoCampo">Tipo Unidad:</td>
    			<td  class="textoDato" colspan="3">
					<@s.property default="&nbsp;" escape=false value="producto.tipoUnidad.codigo"/>
				</td>
    		</tr>
			<tr>
				<td class="textoCampo" >Es Registrable: </td>
				<td  class="textoDato">&nbsp;</td>
				<td class="textoCampo">Dominio: </td>
				<td  class="textoDato">&nbsp;</td>
    		</tr>
			<tr>
				<td class="textoCampo">Es Veh&iacute;culo: </td>
				<td  class="textoDato">&nbsp;</td>
				<td class="textoCampo">Precio Mercado: </td>
				<td  class="textoDato">&nbsp;</td>
    		</tr>
			<tr>
				<td class="textoCampo">Amortizaci&oacute;n (%): </td>
				<td  class="textoDato">&nbsp;									
				</td>
				<td class="textoCampo">Estado: </td>
      			<td class="textoDato"><@s.property default="&nbsp;" escape=false value="producto.estado"/></td>
    		</tr>
		</table>
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos del Dep&oacute;sito</b></td>
			</tr>
		</table>
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>	
			<tr>
				<td class="textoCampo">Dep&oacute;sito:</td>
				<td class="textoDato">
					<@s.select 
						templateDir="custontemplates" 
						id="deposito" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="deposito" 
						list="depositoList" 
						listKey="oid" 
						listValue="descripcion" 
						value="oid"
						title="Deposito"
						headerKey="" 
                        headerValue="Seleccionar" />
				</td>
				<td class="textoCampo">&nbsp;</td>
				<td class="textoDato">&nbsp;</td>
			</tr>
		</table>
		</div>
		
		<div id="ubicacion"><@tiles.insertAttribute name="ubicacion"/></div>
				
		<div id="capaBotonera" class="capaBotonera">
			<table id="tablaBotonera" class="tablaBotonera">
			<tr> 
			<td align="right">
				<input id="cancel" type="button" name="btnVolver" value="Cancelar" class="boton"/>
				<input id="store" type="button" name="btnAgregar" value="Agregar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

<@vc.htmlContent 
baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId=producto-administrar,flowControl=back"/>
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/productoBien/createProductoDepositoUbicacion.action" 
  source="store" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="producto.oid={producto.oid},deposito.tipoOrganizacion={tipo},deposito.oid={deposito},seccion.oid={seccion},filaDesde.fila={filaDesde},filaHasta.fila={filaHasta},columnaDesde.columna={columnaDesde},columnaHasta.columna={columnaHasta},ubicacionDesde.ubicacion={ubicacionDesde},ubicacionHasta.ubicacion={ubicacionHasta}"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/productoBien/refrescarUbicacion.action" 
  source="deposito" 
  success="ubicacion" 
  failure="errorTrx" 
  parameters="deposito.oid={deposito},deposito.tipoOrganizacion={tipoOrganizacion},producto.oid={producto.oid}"
  eventType="change"
  />
