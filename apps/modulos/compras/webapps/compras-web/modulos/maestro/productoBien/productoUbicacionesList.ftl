<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>


<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
	<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
		<tr>
			<td>Destino</td>
			<td>
				<div align="right">
				<@s.a href="javascript://nop/" templateDir="custontemplates" id="agregarProductoDepositoUbicacion" name="agregarProductoDepositoUbicacion" cssClass="ocultarIcono">
				<b>Agregar</b><img src="${request.contextPath}/common/images/agregar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
				</@s.a>			
				</div>
			</td>
		</tr>
	</table>	

	<!-- Resultado Filtro -->						
	<@ajax.anchors target="contentTrx">
	<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="productoBien.productoDepositoUbicacionList" id="productoDepositoUbicacion" decorator="ar.com.riouruguay.web.actions.maestro.productobien.decorators.DepositoUbicacionDecorator"  defaultsort=2>
	
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select" style=" text-align:center; display:block" title="Acciones">
			<a href="${request.contextPath}/maestro/productoBien/deleteProductoDepositoUbicacionView.action?productoDepositoUbicacion.oid=${productoDepositoUbicacion.oid?c}&productoDepositoUbicacion.versionNumber=${productoDepositoUbicacion.versionNumber?c}">
			<img  src="${request.contextPath}/common/images/eliminar.gif" alt="Eliminar" title="Eliminar" border="0"></a>
		</@display.column>
	
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="depositoUbicacion.deposito.descripcion" title="Dep&oacute;sito" />
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="depositoUbicacion.deposito.tipoOrganizacion" title="Tipo Organizaci&oacute;n" />
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="organizacion" title="Organizaci&oacute;n" />
	</@display.table>
	</@ajax.anchors>
	
	</div>
<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/productoBien/createProductoDepositoUbicacionView.action" 
  source="agregarProductoDepositoUbicacion" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="producto.oid={productoBien.oid}"/>
