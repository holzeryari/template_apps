<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>

<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
	<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
		<tr>
			<td>Destino</td>
		</tr>
	</table>	
			

<!-- Resultado Filtro -->						
<@ajax.anchors target="contentTrx">
<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="productoBien.productoDepositoUbicacionList" id="productoDepositoUbicacion" decorator="ar.com.riouruguay.web.actions.maestro.productobien.decorators.DepositoUbicacionDecorator">
	<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="depositoUbicacion.deposito.descripcion" title="Dep&oacute;sito" />
	<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="depositoUbicacion.deposito.tipoOrganizacion" title="Tipo Organizaci&oacute;n" />
	<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="organizacion" title="Organizaci&oacute;n" />
</@display.table>
</@ajax.anchors>
</div>
