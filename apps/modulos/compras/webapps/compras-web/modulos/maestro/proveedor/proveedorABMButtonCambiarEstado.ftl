<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
	<#if proveedor.estado == "Activo">
		<#assign titulo="Desactivar">
	<#else>
		<#assign titulo="Activar">
	</#if>



<div id="capaSeparadora" class="capaSeparadora">&nbsp;</div>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancel" type="button" name="btnVolver" value="Cancelar" class="boton"/>
				<input id="status" type="button" name="btnStatus" value="${titulo}" class="boton"/>
			</td>
		</tr>
	</table>
</div>



<@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=buscar-proveedores,flowControl=back"/>
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/proveedor/cambiarEstado.action" 
  source="status" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="proveedor.nroProveedor=${proveedor.nroProveedor?c},navigationId=${navigationId},navegacionIdBack=${navegacionIdBack}"/>
  