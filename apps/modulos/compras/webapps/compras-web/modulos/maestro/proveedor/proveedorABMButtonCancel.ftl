<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>


<div id="capaSeparadora" class="capaSeparadora">&nbsp;</div>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="left">
				<input id="cancel" type="button" name="btnOk" value="Volver" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=${navegacionIdBack},flowControl=back"/>
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/proveedor/updateView.action" 
  source="updateProveedor" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=update-proveedor,navegacionIdBack=${navigationId},flowControl=regis,proveedor.nroProveedor=${proveedor.nroProveedor?c}"/>
 

  