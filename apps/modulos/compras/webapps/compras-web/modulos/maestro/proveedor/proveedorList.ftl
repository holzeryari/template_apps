<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>
<@s.hidden id="navigationId" name="navigationId"/>

<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
        	<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
			<tr>
				<td><b>Datos del Proveedor</b></td>
				<td>
					<div align="right">
						<@security.a id="crear" 
							name="crear"
							href="javascript://nop/" 
							cssClass="ocultarIcono" 
							templateDir="custontemplates" 
							securityCode="CUF0121">
							<b>Agregar</b><img src="${request.contextPath}/common/images/agregar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
						</@security.a>
					</div>
				</td>
			</tr>
		</table>

		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>		
			<tr>
				<td class="textoCampo">N&uacute;mero:</td>
      			<td class="textoDato">
      				<@s.textfield 
      					templateDir="custontemplates" 
						id="nroProveedor" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="proveedor.nroProveedor" 
						title="N�mero" /></td>
						
				<td class="textoCampo">Tipo de Persona:</td>
				<td class="textoDato">
	      			<@s.select 
						templateDir="custontemplates" 
						id="tipoPersona" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="proveedor.detalleDePersona.persona.tipoPersona" 
						list="tipoPersonaList" 
						listKey="key" 
						listValue="description" 
						value="proveedor.detalleDePersona.persona.tipoPersona.ordinal()"
						title="Tipo de Persona"
						headerKey="-1"
						headerValue="Todos"  
						/></td>
			</tr>
			<tr>
	  			<td class="textoCampo">Nombre de Fantasia:</td>
      			<td class="textoDato">
      				<@s.textfield 
      					templateDir="custontemplates" 
						id="nombreFantasia" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="proveedor.nombreFantasia" 
						title="Nombre de Fantasia" /></td>
														
				<td class="textoCampo">CUIT/CUIL:</td>
      			<td class="textoDato"><@s.textfield 
      					templateDir="custontemplates" 
						id="docFiscal" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="proveedor.detalleDePersona.persona.docFiscal" 
						title="CUIT/CUIL" /></td>
			</tr>
			<tr>	
				<td class="textoCampo">Raz&oacute;n Social/Apellido:</td>
      			<td class="textoDato">
      				<@s.textfield 
      					templateDir="custontemplates" 
						id="razonSocial" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="proveedor.detalleDePersona.razonSocial" 
						title="Raz&oacute;n Social" /></td>
				
				<td class="textoCampo">Nombre:</td>
      			<td class="textoDato">
      				<@s.textfield 
      					templateDir="custontemplates" 
						id="nombre" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="proveedor.detalleDePersona.nombre" 
						title="Nombre" /></td>
			</tr>
			<tr>
				<td class="textoCampo">Estado:</td>
				<td class="textoDato">
					<@s.select 
						templateDir="custontemplates" 
						id="estado" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="proveedor.estado" 
						list="tipoEstadoList" 
						listKey="key" 
						listValue="description" 
						value="proveedor.estado.ordinal()"
						title="Estado"
						headerKey="-1"
						headerValue="Todos"  
						/>
				</td>
    			<td class="textoCampo">N&uacute;mero de Documento:</td>
      			<td class="textoDato">
      				<@s.textfield 
      					templateDir="custontemplates" 
						id="docPersonal" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="proveedor.detalleDePersona.persona.docPersonal" 
						title="N&uacute;mero de Documento" /></td>
			</tr>	
			
			<tr>
				<td class="textoCampo">Proveedor Servicio/Producto/Bien Cr&iacute;tico:</td>
				<td class="textoDato">
					<@s.select 
						templateDir="custontemplates" 
						id="estado" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="proveedorCritico" 
						list="proveedorCriticoList" 
						listKey="key" 
						listValue="description" 
						value="proveedorCritico.ordinal()"
						title="Proveedor Cr&iacute;tico"
						/>
				</td>
				<td class="textoCampo">Evaluado por Log&iacute;stica y Servicio:</td>
      			<td class="textoDato">
      			<@s.select 
						templateDir="custontemplates" 
						id="proveedorEvaluadoLyS" 
						cssClass="textarea"
						cssStyle="width:165px"
						name="proveedorEvaluadoLyS" 
						list="proveedorEvaluadoLySList" 
						listKey="key" 
						listValue="description" 
						value="proveedorEvaluadoLyS.ordinal()"
						title="Proveedor evaluado por Logistica y Servicio"
						headerKey="0"
						headerValue="Todos"					
						/>
				</td>    
			</tr>				
			<tr>
	    		<td colspan="4" class="lineaGris" align="right">
	      			<input id="limpiar" type="button" name="btnVolver" value="Limpiar" class="boton"/>		
	      			<input id="buscar" type="button" name="btnVolver" value="Buscar" class="boton"/>
	    		</td>
			</tr>	
		</table>
	</div>
	
	
	<@s.if test="proveedorList!=null">
    	<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
    		<#assign nroProveedorF = "0" />
			<@s.if test="proveedor.nroProveedor != null">
				<#assign nroProveedorF = "${proveedor.nroProveedor}" />
			</@s.if>
			<#assign tipoPersonaF = "-1" />
			<@s.if test="proveedor.detalleDePersona.persona.tipoPersona != null">
				<#assign tipoPersonaF = "${proveedor.detalleDePersona.persona.tipoPersona}" />
			</@s.if>
			<#assign nombreFantasiaF = "" />
			<@s.if test="proveedor.nombreFantasia != null">
				<#assign nombreFantasiaF = "${proveedor.nombreFantasia}" />
			</@s.if>
			<#assign docFiscalF = "0" />
			<@s.if test="proveedor.detalleDePersona.persona.docFiscal != null">
				<#assign docFiscalF = "${proveedor.detalleDePersona.persona.docFiscal?c}" />
			</@s.if>
			<#assign docPersonalF = "0" />
			<@s.if test="proveedor.detalleDePersona.persona.docPersonal != null">
				<#assign docPersonalF = "${proveedor.detalleDePersona.persona.docPersonal?c}" />
			</@s.if>
			<#assign razonSocialF = "" />
			<@s.if test="proveedor.detalleDePersona.razonSocial != null">
				<#assign razonSocialF = "${proveedor.detalleDePersona.razonSocial}" />
			</@s.if>
			<#assign nombreF = "" />
			<@s.if test="proveedor.detalleDePersona.nombre != null">
				<#assign nombreF = "${proveedor.detalleDePersona.nombre}" />
			</@s.if>
			<#assign estadoF = "-1" />
			<@s.if test="proveedor.estado!= null">
				<#assign estadoF = "${proveedor.estado}" />
			</@s.if>
			<#assign rubroF = "0" />
			<@s.if test="rubro.oid != null">
				<#assign rubroF = "${rubro.oid}" />
			</@s.if>
			<#assign rubroDescripcionF = "" />
			<@s.if test="rubro.descripcion != null">
				<#assign rubroDescripcionF = "${rubro.descripcion}" />
			</@s.if>
			<#assign proveedorCriticoF = "${proveedorCritico.ordinal()}" />
			<#assign proveedorEvaluadoLySF = "${proveedorEvaluadoLyS.ordinal()}" />
			
    		
    		<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>Proveedores encontrados</td>
					<td>
    					<div class="alineacionDerechaBig">
							<@security.a 
								templateDir="custontemplates" 
								securityCode="CUF0382" 
								cssClass="item" 
								href="${request.contextPath}/maestro/proveedor/printLySXLS.action?proveedor.nroProveedor=${nroProveedorF}&persona.tipoPersona=${tipoPersonaF}&proveedor.nombreFantasia=${nombreFantasiaF}&proveedor.detalleDePersona.persona.docFiscal=${docFiscalF}&proveedor.detalleDePersona.razonSocial=${razonSocialF}&proveedor.detalleDePersona.nombre=${nombreF}&proveedor.detalleDePersona.persona.docPersonal=${docPersonalF}&proveedor.estado=${estadoF}&rubro.oid=${rubroF}&rubro.descripcion=${rubroDescripcionF}&proveedorCritico=${proveedorCriticoF}">
								<img src="${request.contextPath}/common/images/imprimirProveedorLySXLS.gif" title="Exporta a Excel el listado de Proveedor de Logistica y Servicio" align="absmiddle" border="0" hspace="1">
							</@security.a>	
			
						</div>
    					<div class="alineacionDerechaBig">
    					<b>De Log&iacute;stica y Servicio</b>
							<@security.a 
								templateDir="custontemplates" 
								securityCode="CUF0382" 
								cssClass="item" 
								href="${request.contextPath}/maestro/proveedor/printLySPDF.action?proveedor.nroProveedor=${nroProveedorF}&persona.tipoPersona=${tipoPersonaF}&proveedor.nombreFantasia=${nombreFantasiaF}&proveedor.detalleDePersona.persona.docFiscal=${docFiscalF}&proveedor.detalleDePersona.razonSocial=${razonSocialF}&proveedor.detalleDePersona.nombre=${nombreF}&proveedor.detalleDePersona.persona.docPersonal=${docPersonalF}&proveedor.estado=${estadoF}&rubro.oid=${rubroF}&rubro.descripcion=${rubroDescripcionF}&proveedorCritico=${proveedorCriticoF}&proveedorEvaluadoLyS=${proveedorEvaluadoLySF}">
								<img src="${request.contextPath}/common/images/imprimirProveedorLySPdf.gif" title="Exportar a PDF el listado de Proveedor de Logistica y Servicio" align="absmiddle" border="0" hspace="1">
							</@security.a>	
						</div>
    					<div class="alineacionDerechaBig">
							<@security.a 
								templateDir="custontemplates" 
								securityCode="CUF0382" 
								cssClass="item" 
								href="${request.contextPath}/maestro/proveedor/printXLS.action?proveedor.nroProveedor=${nroProveedorF}&persona.tipoPersona=${tipoPersonaF}&proveedor.nombreFantasia=${nombreFantasiaF}&proveedor.detalleDePersona.persona.docFiscal=${docFiscalF}&proveedor.detalleDePersona.razonSocial=${razonSocialF}&proveedor.detalleDePersona.nombre=${nombreF}&proveedor.detalleDePersona.persona.docPersonal=${docPersonalF}&proveedor.estado=${estadoF}&rubro.oid=${rubroF}&rubro.descripcion=${rubroDescripcionF}&proveedorCritico=${proveedorCriticoF}&proveedorEvaluadoLyS=${proveedorEvaluadoLySF}">
								<img src="${request.contextPath}/common/images/imprimirExcel.gif" title="Exportar a Excel" align="absmiddle" border="0" hspace="1">
							</@security.a>
							<b> - </b>&nbsp;
						</div>
						<div class="alineacionDerechaBig">
							<b>General</b>
							<@security.a 
								templateDir="custontemplates" 
								securityCode="CUF0382" 
								cssClass="item" 
								href="${request.contextPath}/maestro/proveedor/printPDF.action?proveedor.nroProveedor=${nroProveedorF}&persona.tipoPersona=${tipoPersonaF}&proveedor.nombreFantasia=${nombreFantasiaF}&proveedor.detalleDePersona.persona.docFiscal=${docFiscalF}&proveedor.detalleDePersona.razonSocial=${razonSocialF}&proveedor.detalleDePersona.nombre=${nombreF}&proveedor.detalleDePersona.persona.docPersonal=${docPersonalF}&proveedor.estado=${estadoF}&rubro.oid=${rubroF}&rubro.descripcion=${rubroDescripcionF}&proveedorCritico=${proveedorCriticoF}&proveedorEvaluadoLyS=${proveedorEvaluadoLySF}">
								<img src="${request.contextPath}/common/images/imprimirPdf.gif" title="Exportar a PDF" align="absmiddle" border="0" hspace="1" >
							</@security.a>
						</div>
				</tr>
			</table>			
			<@ajax.anchors target="contentTrx">			
				<@display.table 
          			class="tablaDetalleCuerpo" 
          			cellpadding="3" 
          			name="proveedorList" 
          			id="proveedor" 
          			pagesize=15 
          			defaultsort=1
          			partialList=true 
          			size="recordSize"
          			keepStatus=true
          			excludedParams="resetFlag">
								
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select" class="botoneraAnchoCon3" title="Acciones">
						<div align="center">	
							<div class="alineacion">
							<@security.a templateDir="custontemplates" 
								securityCode="CUF0122" 
								enabled="proveedor.readable"
								cssClass="item"
								id="read" 
								name="read" 
								href="${request.contextPath}/maestro/proveedor/readView.action?proveedor.nroProveedor=${proveedor.nroProveedor?c}&navigationId=read-proveedor&navegacionIdBack=buscar-proveedores&flowControl=regis"
								><img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver"  border="0"
								></@security.a>
							</div>
							<div class="alineacion">
							<@security.a templateDir="custontemplates" 
								securityCode="CUF0123" 
								enabled="proveedor.updatable"
								id="admin" 
								name="admin"
								cssClass="item" 
								href="${request.contextPath}/maestro/proveedor/administrarView.action?proveedor.nroProveedor=${proveedor.nroProveedor?c}&navigationId=admin-proveedor&navegacionIdBack=buscar-proveedores&flowControl=regis"
								><img src="${request.contextPath}/common/images/administrar.gif" alt="Administrar" title="Administrar"  border="0"
								></@security.a>
							</div>
							<div class="alineacion">
							<#if proveedor.estado == "Activo">
								<#assign imagen="desactivar.gif">
								<#assign alternativa="Desactivar">
								<#assign titulo="Desactivar">
								<@security.a 
								templateDir="custontemplates" 
								securityCode="CUF0124" 
								enabled="true" 
								cssClass="item" 
								href="${request.contextPath}/maestro/proveedor/desactivarView.action?proveedor.nroProveedor=${proveedor.nroProveedor?c}&navigationId=admin-proveedor&navegacionIdBack=buscar-proveedores&flowControl=regis">
								<img src="${request.contextPath}/common/images/${imagen}" alt="${alternativa}" title="${titulo}" border="0">
							</@security.a>
							<#else>
								<#assign imagen="activar.gif">
								<#assign alternativa="Activar">
								<#assign titulo="Activar">
								<@security.a 
								templateDir="custontemplates" 
								securityCode="CUF0125" 
								enabled="true" 
								cssClass="item" 
								href="${request.contextPath}/maestro/proveedor/activarView.action?proveedor.nroProveedor=${proveedor.nroProveedor?c}&navigationId=admin-proveedor&navegacionIdBack=buscar-proveedores&flowControl=regis">
								<img src="${request.contextPath}/common/images/${imagen}" alt="${alternativa}" title="${titulo}" border="0">
							</@security.a>
							</#if>
							</div>
						</div>
					</@display.column>
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="nroProveedor" title="Nro." />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="detalleDePersona.persona.tipoPersona" title="Tipo De persona" />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="nombreFantasia" title="Nombre de Fantasia" />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="detalleDePersona.razonSocial" title="Raz&oacute;n Social/Apellido" />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="detalleDePersona.nombre" title="Nombre" /> 
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="detalleDePersona.persona.docPersonal" title="Documento" />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="detalleDePersona.persona.docFiscal" title="CUIT/CUIL" />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="estado" title="Estado" />
					
				</@display.table>
			</@ajax.anchors>
			</div>
			</@s.if>
			
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/proveedor/search.action" 
  source="buscar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="resetFlag=true,navigationId=buscar-proveedores,flowControl=regis,proveedor.nroProveedor={nroProveedor},persona.tipoPersona={tipoPersona},proveedor.nombreFantasia={nombreFantasia},proveedor.detalleDePersona.persona.docFiscal={docFiscal},proveedor.detalleDePersona.razonSocial={razonSocial},proveedor.detalleDePersona.nombre={nombre},proveedor.detalleDePersona.persona.docPersonal={docPersonal},proveedor.estado={estado},rubro.oid={rubro.oid},rubro.descripcion={rubro.descripcion},proveedorCritico={proveedorCritico},proveedorEvaluadoLyS={proveedorEvaluadoLyS}"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/proveedor/view.action" 
  source="limpiar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId=buscar-proveedores,flowControl=regis"/>
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/persona/selectView.action" 
  source="crear" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=seleccionar-persona,navegacionIdBack=buscar-proveedores,flowControl=regis,nameSpaceSelect=/maestro/proveedor,actionNameSelect=preCreateView,proveedorCritico={proveedorCritico},proveedorEvaluadoLyS={proveedorEvaluadoLyS}"/>
  
  
