<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>


<@ajax.anchors target="contentTrx">	

	<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
		<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
            <tr>
				<td><b>Rubros</b></td>
				<td>
					<div align="right">
						<@s.a id="addRubro" 
							name="addRubro"
							href="${request.contextPath}/maestro/proveedor/selectRubro.action?navigationId=${navigationId}&navegacionIdBack=${navegacionIdBack}&proveedor.nroProveedor=${proveedor.nroProveedor?c}&flowControl=change&namespace=/maestro/proveedor,actionName=addRubro" 
							cssClass="ocultarIcono" 
							templateDir="custontemplates">
							<b>Agregar</b>
							<img src="${request.contextPath}/common/images/agregar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
						</@s.a>				
					</div>
				</td>
			</tr>
		</table>

		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="proveedor.listaRubro" id="rubro">
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select" style=" text-align:center; display:block" title="Acciones">
<#--
				<@security.a 
					templateDir="custontemplates" 
					securityCode="CUF0001" 
					enabled="rubro.eraseable"
					cssClass="item"  
					name="eraseable"
					href="${request.contextPath}/maestro/proveedor/deleteRubro.action?proveedor.nroProveedor=${proveedor.nroProveedor?c}&rubro.oid=${rubro.oid?c}&navigationId=${navigationId}&navegacionIdBack=${navegacionIdBack}">
					<img src="${request.contextPath}/common/images/eliminar.gif" alt="Eliminar" title="Eliminar"  border="0">
				</@security.a>
-->
				<@s.a id="deleteRubro" 
					name="deleteRubro"
					href="${request.contextPath}/maestro/proveedor/deleteRubro.action?proveedor.nroProveedor=${proveedor.nroProveedor?c}&rubro.oid=${rubro.oid?c}&navigationId=${navigationId}&navegacionIdBack=${navegacionIdBack}"
					cssClass="item"  
					templateDir="custontemplates">
					<img src="${request.contextPath}/common/images/eliminar.gif" alt="Eliminar" title="Eliminar" border="0">
				</@s.a>				

			</@display.column>			
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="codigo" title="C&oacute;digo" />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="descripcion" title="Descripci&oacute;n" />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="imputable" title="Es imputable" />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="estado" title="Estado" />
		</@display.table>
	</div>
</@ajax.anchors>
  