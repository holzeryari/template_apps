<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<@s.hidden id="webFlow" name="webFlow"/>
<@s.hidden id="viewState" name="viewState"/>

<@s.hidden id="rubro.oid" name="rubro.oid"/>
<@s.hidden id="rubro.padre.oid" name="rubro.padre.oid"/>
<@s.hidden id="codigo" name="rubro.codigo"/>
<@s.hidden id="versionNumber" name="rubro.versionNumber"/>
<@s.hidden id="rubro.estado" name="rubro.estado.ordinal()"/>
<@s.hidden id="rubro.imputable" name="rubro.imputable.ordinal()"/>

	<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
			<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos del Rubro</b></td>
				<td>
					<div align="right">
						<@s.a templateDir="custontemplates" id="modificarRubro" name="modificarRubro" href="javascript://nop/" cssClass="ocultarIcono">
						<b>Modificar</b><img src="${request.contextPath}/common/images/modificar.gif" border="0" align="absmiddle" hspace="3" >
						</@s.a>
					</div>
				</td>
			</tr>
		</table>

		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			
			<#list padreList?sort_by("codigo") as padre>
				<#if padre_index != 0>
					<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
						<tr>
							<td colspan="4" bgcolor="#DFE7F1">
								<b>Datos del Subrubro Nivel ${padre_index}</b>
							</td>
						<tr>
					</table>	
				</#if>
				<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
					<tr><td colspan="4" class="textoCampo">&nbsp;</td></tr>
					<tr>
	      				<td class="textoCampo">C&oacute;digo:</td>
	      				<td class="textoDato">${padre.codigo}</td>
	      				<td class="textoCampo"><@s.text name="rubro.descripcion"/></td>
						<td class="textoDato">${padre.descripcion}</td>
					</tr>
					<tr>
						<td class="textoCampo">Es Imputable: </td>
						<td class="textoDato">${padre.imputable}</td>
						<td class="textoCampo">Estado:</td>
						<td  class="textoDato">${padre.estado}</td>
					</tr>
					<tr><td colspan="4" class="textoCampo">&nbsp;</td></tr>
				</table>	
				<#if !padre_has_next>
					<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
					<tr>
						<td><b>Datos del Subrubro Nivel ${padre_index + 1}</b></td>
					<tr>
					</table>
				</#if>
			</#list>
			<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
				<tr><td colspan="4" class="textoCampo">&nbsp;</td></tr>
				<tr>
	      			<td class="textoCampo">C&oacute;digo:</td>
	      			<td class="textoDato"><@s.property default="&nbsp;" escape=false value="rubro.codigo"/></td>
	      			<td class="textoCampo"><@s.text name="rubro.descripcion" /></td>
					<td class="textoDato">
						<@s.textfield 
								templateDir="custontemplates" 
								id="descripcion" 
								cssClass="textarea"
								cssStyle="width:160px" 
								name="rubro.descripcion" 
								title="Descripci&oacute;n" />					
					</td>	      			
				</tr>
	      		<tr>
					<td class="textoCampo">Es Imputable: </td>
					<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="rubro.imputable"/>
					</td>
					<td class="textoCampo">Estado:</td>
					<td  class="textoDato">
					<@s.property default="&nbsp;" escape=false value="rubro.estado"/>
					</td>
				</tr>		
	      			      			
			<#if (padreList.size() >= 3)>
				<tr>
					<td class="textoCampo">Utilizable Fondo Fijo:</td>
		      		<td  class="textoDato" colspan="3">
						<@s.select 
								templateDir="custontemplates" 
								id="rubro.fondoFijo" 
								cssStyle="width:170px" 
								cssClass="textarea"
								name="rubro.fondoFijo" 
								list="fondoFijoList" 
								listKey="key" 
								listValue="description" 
								value="rubro.fondoFijo.ordinal()"
								title="Fondo Fijo"
								headerKey="0"
								headerValue="Seleccionar"   
								/>
					</td>
				</tr>
		
			
			<#else>
				<@s.hidden id="rubro.fondoFijo" name="rubro.fondoFijo.ordinal()"/>
				
			
			</#if>
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>
		</table>
	
		</div>


<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/rubro/updateView.action" 
  source="modificarRubro" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="rubro.oid={rubro.oid},rubro.versionNumber={versionNumber},navegacionIdBack=administrar${rubro.nivel},navigationId=rubro-actualizar"/>
