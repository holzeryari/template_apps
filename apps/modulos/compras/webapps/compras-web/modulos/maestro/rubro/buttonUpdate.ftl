<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>


<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancel" type="button" name="btnVolver" value="Cancelar" class="boton"/>
				<input id="update" type="button" name="btnModificar" value="Modificar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/rubro/update.action" 
  source="update" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="rubro.descripcion={descripcion},rubro.codigo={codigo},rubro.estado={estado},rubro.oid={rubro.oid},rubro.padre.oid={rubro.padre.oid},rubro.imputable={imputable},rubro.fondoFijo={rubro.fondoFijo},rubro.versionNumber={versionNumber},navegacionIdBack=${navegacionIdBack}"/>
<@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=${navegacionIdBack},flowControl=back"/>
