<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>
<#assign tiles=JspTaglibs["/WEB-INF/tlds/tiles-jsp.tld"]>

<@s.hidden id="webFlow" name="webFlow"/>
<@s.hidden id="viewState" name="viewState"/>
<@s.hidden id="navigationId" name="navigationId"/>
<@s.hidden id="rubro.usuario.oid" name="rubro.usuario.oid"/>
<@s.hidden id="rubro.filtraPorUsuario" name="rubro.filtraPorUsuario"/>
	
	<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
        	<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
			<tr>
				<td><b>Datos del Rubro</b></td>
				<td>					
					<div align="right">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0046" 
							enabled="true" 
							cssClass="item" 
							id="crear"										
							href="javascript://nop/">
							<b>Agregar</b>										
							<img src="${request.contextPath}/common/images/agregar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
						</@security.a>
					</div>
				</td>
			</tr>
		</table>
		
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>
			<tr>
	      		<td class="textoCampo">C&oacute;digo:</td>
	      		<td class="textoDato">
				     <@s.textfield 
	      				templateDir="custontemplates" 
						id="codigo" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="rubro.codigo" 
						title="C&oacute;digo" />
				</td>
	      		<td class="textoCampo">Descripci&oacute;n:</td>
	      		<td class="textoDato">
	      			<@s.textfield 
	      				templateDir="custontemplates" 
						id="descripcion" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="rubro.descripcion" 
						title="Descripcion" />
				</td>
	    	</tr>				
	    	<tr>
	      		<td class="textoCampo">Es imputable:</td>
	      		<td class="textoDato">
					<@s.select 
						templateDir="custontemplates" 
						id="imputable" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="rubro.imputable" 
						list="imputableList" 
						listKey="key" 
						listValue="description" 
						value="rubro.imputable.ordinal()"
						title="Es Imputable"
						headerKey="0"
						headerValue="Todos"  
						 />
				</td>
	      		<td class="textoCampo">Rubro:</td>
	      		<td class="textoDato">
				     <@s.select 
						templateDir="custontemplates" 
						id="rubroNivel0" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="rubroNivel0.oid" 
						list="rubroNivel0List" 
						listKey="oid" 
						listValue="descripcion" 
						value="rubroNivel0.oid"
						title="Rubro nivel 0"
						 /> 
				</td>
	    	</tr>	
	    	<tr>
				<td class="textoCampo">Rubro nivel 1:</td>
				<td class="textoDato">
				    <@s.select 
						templateDir="custontemplates" 
						id="rubroNivel1" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="rubroNivel1.oid" 
						list="rubroNivel1List" 
						listKey="oid" 
						listValue="descripcion" 
						title="Rubro nivel 1"
						 /> 
				</td>
				<td class="textoCampo">Rubro nivel 2:</td>
				<td class="textoDato">
				     <@s.select 
						templateDir="custontemplates" 
						id="rubroNivel2" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="rubroNivel2.oid" 
						list="rubroNivel2List" 
						listKey="oid" 
						listValue="descripcion" 
						title="Rubro nivel 2"
						 /> 
				</td>
			</tr>	
			<tr>
				<td class="textoCampo">Rubro nivel 3:</td>
				<td class="textoDato">
				     <@s.select 
						templateDir="custontemplates" 
						id="rubroNivel3" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="rubroNivel3.oid" 
						list="rubroNivel3List" 
						listKey="oid" 
						listValue="descripcion" 
						title="Rubro nivel 3"
						 /> 
				</td>
				<td class="textoCampo">Estado:</td>
	      		<td class="textoDato" colspan="3">
	      			<@s.select 
							templateDir="custontemplates" 
							id="rubro.estado" 
							cssClass="textarea"
							cssStyle="width:165px" 
							name="rubro.estado" 
							list="estadoList" 
							listKey="key" 
							listValue="description" 
							value="rubro.estado.ordinal()"
							title="Estado"
							headerKey="0"
							headerValue="Todos"							
							/>
				</td>
			</tr>
			<tr>
	    		<td colspan="4" class="lineaGris" align="right">
	      			<input id="limpiar" type="button" name="btnVolver" value="Limpiar" class="boton"/>		
	      			<input id="buscar" type="button" name="btnVolver" value="Buscar" class="boton"/>
	    		</td>
			</tr>
		</table>
	  </div>  	
	  <@s.if test="rubroList!=null">
	  	<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">									
			<#assign rubroCodigo="${rubro.codigo}" />
			<#assign rubroDescripcion="${rubro.descripcion}" />

			<#assign rubroImputable = "0" />
			<@s.if test="rubro.imputable != null">
				<#assign rubroImputable = "${rubro.imputable.ordinal()}" />
			</@s.if>
			<#assign rubroNivel0Oid = "0" />
			<@s.if test="rubroNivel0 != null && rubroNivel0.oid != null">
				<#assign rubroNivel0Oid = "${rubroNivel0.oid}" />
			</@s.if>
			<#assign rubroNivel1Oid = "0" />
			<@s.if test="rubroNivel1 != null && rubroNivel1.oid != null">
				<#assign rubroNivel1Oid = "${rubroNivel1.oid}" />
			</@s.if>
			<#assign rubroNivel2Oid = "0" />
			<@s.if test="rubroNivel2 != null && rubroNivel2.oid != null">
				<#assign rubroNivel2Oid = "${rubroNivel2.oid}" />
			</@s.if>
			<#assign rubroNivel3Oid = "0" />
			<@s.if test="rubroNivel3 != null && rubroNivel3.oid != null">
				<#assign rubroNivel3Oid = "${rubroNivel3.oid}" />
			</@s.if>
			<#assign rubroEstado = "0" />
			<@s.if test="rubro.estado != null">
				<#assign rubroEstado = "${rubro.estado.ordinal()}" />
			</@s.if>

			
			<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>Rubros encontrados</td>
					<td>
						<div class="alineacionDerecha">
								<@security.a 
									templateDir="custontemplates" 
									securityCode="CUF0047" 
									name="CUF0047"
									cssClass="item" 
									href="${request.contextPath}/maestro/rubro/printXLS.action?rubro.descripcion=${rubroDescripcion}&rubro.codigo=${rubroCodigo}&rubro.imputable=${rubroImputable}&rubroNivel0.oid=${rubroNivel0Oid}&rubroNivel1.oid=${rubroNivel1Oid}&rubroNivel2.oid=${rubroNivel2Oid}&rubroNivel3.oid=${rubroNivel3Oid}&rubro.estado=${rubroEstado}">
									<img src="${request.contextPath}/common/images/imprimirExcel.gif" title="Exportar a Excel" align="absmiddle" border="0" hspace="3" >
								</@security.a>
						</div>
						<div class="alineacionDerecha">
							<b>Imprimir</b>
								<@security.a 
									templateDir="custontemplates" 
									securityCode="CUF0047" 
									name="CUF0047"
									cssClass="item" 
									href="${request.contextPath}/maestro/rubro/printPDF.action?rubro.descripcion=${rubroDescripcion}&rubro.codigo=${rubroCodigo}&rubro.imputable=${rubroImputable}&rubroNivel0.oid=${rubroNivel0Oid}&rubroNivel1.oid=${rubroNivel1Oid}&rubroNivel2.oid=${rubroNivel2Oid}&rubroNivel3.oid=${rubroNivel3Oid}&rubro.estado=${rubroEstado}">
									<img src="${request.contextPath}/common/images/imprimirPdf.gif" title="Exportar a PDF" align="absmiddle" border="0" hspace="3" >
								</@security.a>
							</div>										
					</td>
				</tr>	
			</table>
			<@vc.anchors target="contentTrx">	
				<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="rubroList" id="rubro" pagesize=15 partialList=true size="recordSize" keepStatus=true excludedParams="resetFlag">
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select" class="botoneraAnchoCon4" title="Acciones">
						<div class="alineacion">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0042" 
							name="CUF0042"
							enabled="rubro.readable" 
							cssClass="item" 
							href="${request.contextPath}/maestro/rubro/readView.action?rubro.oid=${rubro.oid?c}&rubro.versionNumber=${rubro.versionNumber?c}&navegacionIdBack=buscar-rubros">
							<img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver" border="0">
						</@security.a>
						</div>
						<div class="alineacion">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0043" 
							name="CUF0043"
							enabled="rubro.updatable" 
							cssClass="item" 
							href="${request.contextPath}/maestro/rubro/administrarView.action?rubro.oid=${rubro.oid?c}&rubro.versionNumber=${rubro.versionNumber?c}&navigationId=administrar${rubro.nivel}&flowControl=regis&navegacionIdBack=buscar-rubros">
							<img  src="${request.contextPath}/common/images/administrar.gif" alt="Administrar" title="Administrar"  border="0">
						</@security.a>
						</div>
						<div class="alineacion">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0044" 
							name="CUF0044"
							enabled="rubro.eraseable" 
							cssClass="item" 
							href="${request.contextPath}/maestro/rubro/deleteView.action?rubro.oid=${rubro.oid?c}&rubro.versionNumber=${rubro.versionNumber}">
							<img  src="${request.contextPath}/common/images/eliminar.gif" alt="Eliminar" title="Eliminar" border="0">
						</@security.a>
						</div>
						<div class="alineacion">
							<#if rubro.estado == "Activo">
								<#assign imagen="desactivar.gif">
								<#assign alternativa="Desactivar">
								<#assign titulo="Desactivar">
								<#assign link="${request.contextPath}/maestro/rubro/cambiarEstadoView.action?rubro.oid=${rubro.oid?c}&navegacionIdBack=buscar-rubros">
							<#else>
								<#assign imagen="activar.gif">
								<#assign alternativa="Activar">
								<#assign titulo="Activar">
								<#assign link="${request.contextPath}/maestro/rubro/cambiarEstadoView.action?rubro.oid=${rubro.oid?c}&navegacionIdBack=buscar-rubros">
							</#if>

							<@security.a 
								templateDir="custontemplates" 
								securityCode="CUF0045" 
								name="CUF0045"
								enabled="rubro.enable" 
								cssClass="item" 
								href="${link}"><img src="${request.contextPath}/common/images/${imagen}" alt="${alternativa}" title="${titulo}" border="0">
							</@security.a>
						</div>
					</@display.column>
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="codigo" title="C&oacute;digo" />

					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="descripcion" title="Descripci&oacute;n" />
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="imputable" title="Imputable" />
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="imputable" title="Imputable" />
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="cuentaContableListAsString" title="Cuenta Contable" />
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="estado" title="Estado" />					
					
				</@display.table>
			</@vc.anchors>
			</div>
		</@s.if>
		   

<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/rubro/search.action" 
  source="buscar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="resetFlag=true,navigationId=buscar-rubros,flowControl=regis,rubro.descripcion={descripcion},rubro.codigo={codigo},rubro.imputable={imputable},rubroNivel0.oid={rubroNivel0},rubroNivel1.oid={rubroNivel1},rubroNivel2.oid={rubroNivel2},rubroNivel3.oid={rubroNivel3},rubro.estado={rubro.estado},rubro.filtraPorUsuario={rubro.filtraPorUsuario},rubro.usuario.oid={rubro.usuario.oid}"/>


<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/rubro/view.action" 
  source="limpiar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId=buscar-rubros,flowControl=regis"/>
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/rubro/createView.action" 
  source="crear" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=crear-rubros,navegacionIdBack=buscar-rubros,flowControl=regis"/>


<@ajax.select
  baseUrl="${request.contextPath}/maestro/rubro/refrescarSubrubro1.action" 
  source="rubroNivel0" 
  target="rubroNivel1"
  parameters="rubroNivel0.oid={rubroNivel0}"
  parser="new ResponseXmlParser()"/>

<@ajax.select
  baseUrl="${request.contextPath}/maestro/rubro/refrescarSubrubro2.action" 
  source="rubroNivel1" 
  target="rubroNivel2"
  parameters="rubroNivel1.oid={rubroNivel1}"
  parser="new ResponseXmlParser()"/>
  
<@ajax.select
  baseUrl="${request.contextPath}/maestro/rubro/refrescarSubrubro3.action" 
  source="rubroNivel2" 
  target="rubroNivel3"
  parameters="rubroNivel2.oid={rubroNivel2}"
  parser="new ResponseXmlParser()"/>

