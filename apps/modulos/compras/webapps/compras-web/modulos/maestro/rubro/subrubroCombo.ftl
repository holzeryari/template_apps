<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign tiles=JspTaglibs["/WEB-INF/tlds/tiles-jsp.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<table width="100%" border="0" cellspacing="0" cellpadding="3">
	<tr>
		<td class="texto_datos" width="20%" align="right">Rubro nivel 1:</td>
		<td class="texto" align="left">
	     <@s.select 
				templateDir="custontemplates" 
				id="rubroNivel1" 
				cssClass="textarea"
				cssStyle="width:170px" 
				name="rubroNivel1" 
				list="rubroNivel1List" 
				listKey="oid" 
				listValue="descripcion" 
				value="oid"
				title="Rubro nivel 1"
				headerKey="0"
				headerValue="Todos"  
				 /> 
		</td>
				<td class="texto_datos" width="20%" align="right">Rubro nivel 2:</td>
		<td class="texto" align="left">
	     <@s.select 
				templateDir="custontemplates" 
				id="rubroNivel2" 
				cssClass="textarea"
				cssStyle="width:170px" 
				name="rubroNivel2" 
				list="rubroNivel2List" 
				listKey="oid" 
				listValue="descripcion" 
				value="oid"
				title="Rubro nivel 2"
				headerKey="0"
				headerValue="Todos"  
				 /> 
		</td>
	</tr>
</table>

 <@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/rubro/refrescarSubrubro2.action" 
  source="rubroNivel1" 
  success="subrubroCombo2" 
  failure="errorTrx" 
  parameters="navigationId=buscar-rubros,flowControl=regis,rubroNivel0.oid={rubroNivel0},rubroNivel1.oid={rubroNivel1}"
  eventType="change"
  />
