<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>

<@vc.anchors target="contentTrx" ajaxFlag="ajax">
 <@s.if test="rubro.imputable.ordinal() == 1">
 	
<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
	<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
		<tr>
			<td>Subrubros</td>
			<td>
				<div align="right">
				<@s.a  templateDir="custontemplates" id="agregarRubro" name="agregarRubro" href="${request.contextPath}/maestro/rubro/createHijoView.action?rubro.padre.oid=${rubro.oid?c}&navegacionIdBack=administrar${rubro.nivel}&navigationId=subRubro-create">
					<b>Agregar</b><img src="${request.contextPath}/common/images/agregar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
					</@s.a>
				</div>
			</td>
		</tr>
	</table>
</div>
</@s.if>
<@s.if test="rubro.imputable.ordinal() == 2">
 	<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
	<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
		<tr>
			<td>Cuentas Contables</td>
			<td>
				<div align="right">
				<@s.a  templateDir="custontemplates" id="agregarCuentaContable" name="agregarCuentaCotnable" href="${request.contextPath}/maestro/rubro/cuentaContableView.action?rubro.oid=${rubro.oid}&navegacionIdBack=administrar${rubro.nivel}&navigationId=cuentaContable-seleccionar" cssClass="texto1">
					<b>Agregar</b><img src="${request.contextPath}/common/images/agregar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
					</@s.a>
				</div>
			</td>
		</tr>
	</table>
 			
	<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="rubro.cuentaContableList" id="cuentaContable" defaultsort=2>	
	<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select" style=" text-align:center; display:block" title="Acciones">
				
			<a href="${request.contextPath}/maestro/rubro/borrarCuentaContableRubroView.action?cuentaContable.codigo=${cuentaContable.codigo}&rubro.oid=${rubro.oid?c}&navegacionIdBack=administrar${rubro.nivel}&navigationId=cuentaContable-borrar">
			<img  src="${request.contextPath}/common/images/eliminar.gif" alt="Eliminar" title="Eliminar" border="0"></a>
									
	</@display.column>		

	<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="codigo" title="C&oacute;digo" />					
	<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="descripcion" title="Descripci&oacute;n" />					

</@display.table>
</div>
</@s.if>
		
</@vc.anchors>
