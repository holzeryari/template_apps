<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>


<@vc.anchors target="contentTrx" ajaxFlag="ajax">
	
	
	
 <@s.if test="rubro.imputable.ordinal() == 1">
 	<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
		<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
			<tr>
				<td>Subrubros</td>
			</tr>
		</table>		
		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="rubro.hijoList" id="subrubro" defaultsort=2>
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="codigo" title="C&oacute;digo" />		
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="descripcion" title="Descripci&oacute;n" />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="estado" title="Estado" />
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="imputable" title="Imputable" />		
		</@display.table>
	</div>
</@s.if>
<@s.if test="rubro.imputable.ordinal() == 2">
 	<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
	<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
		<tr>
			<td>Cuentas Contables</td>
			
		</tr>
	</table>
 	
 	<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="rubro.cuentaContableList" id="cuentaContable" defaultsort=2>	
	<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="codigo" title="C&oacute;digo" />					
	<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="descripcion" title="Descripci&oacute;n" />					

</@display.table>
</div>
</@s.if>
		
</@vc.anchors>
