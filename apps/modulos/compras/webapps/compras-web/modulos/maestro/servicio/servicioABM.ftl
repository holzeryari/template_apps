<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>

<@s.hidden id="navigationId" name="navigationId"/>
<@s.hidden id="servicio.versionNumber" name="servicio.versionNumber"/>
<@s.hidden id="servicio.rubro.oid" name="servicio.rubro.oid"/>
<@s.hidden id="servicio.oid" name="servicio.oid"/>
<@s.hidden id="estadoServicio" name="servicio.estado.ordinal()"/>
<@s.hidden id="servicio.proveedor.nroProveedor" name="servicio.proveedor.nroProveedor"/>
<@s.hidden id="servicio.proveedor.detalleDePersona.razonSocial" name="servicio.proveedor.detalleDePersona.razonSocial"/>
<@s.hidden id="servicio.proveedor.detalleDePersona.nombre" name="servicio.proveedor.detalleDePersona.nombre"/>


<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
			<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos del Servicio</b></td>
			</tr>
		</table>
		
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>				
			<tr>
      			<td class="textoCampo">C&oacute;digo:</td>
      			<td class="textoDato"><@s.property default="&nbsp;" escape=false value="servicio.oid"/></td>
				<td class="textoCampo">Tipo:</td>
      			<td class="textoDato">
	      			<@s.select 
						templateDir="custontemplates" 
						id="servicio.tipo" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="servicio.tipo" 
						list="tipoList" 
						listKey="key" 
						listValue="description" 
						value="servicio.tipo.ordinal()"
						title="Tipo de servicio"
						headerKey="0" 
                		headerValue="Seleccionar"
						 />
	      		</td>	
			<tr>
      			<td class="textoCampo">Descripci&oacute;n</td>
				<td class="textoDato">
				<@s.textfield 
						templateDir="custontemplates" 
						id="servicio.descripcion" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="servicio.descripcion" 
						title="Descripci&oacute;n" />					
				</td>	      			
				<td class="textoCampo">Rubro:</td>
      			<@s.hidden id="servicio.rubro.descripcion" name="servicio.rubro.descripcion"/>
      			<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="servicio.rubro.descripcion" />
					<@s.a templateDir="custontemplates" id="seleccionarRubro" name="seleccionarRubro" href="javascript://nop/" cssClass="ocultarIcono">
						<img src="${request.contextPath}/common/images/buscar.gif" border="0" align="absmiddle" hspace="3" >
					</@s.a>
				</td>
			</tr>	
			<tr>
				<td class="textoCampo">Cr&iacute;tico: </td>
				<td class="textoDato" colspan="3">
					<@s.select 
							templateDir="custontemplates" 
							id="servicio.critico" 
							cssClass="textarea"
							cssStyle="width:165px" 
							name="servicio.critico" 
							list="criticoList" 
							listKey="key" 
							listValue="description" 
							value="servicio.critico.ordinal()"
							title="Cr&iacute;tico"
							headerKey="0" 
                    		headerValue="Seleccionar" 
							/>
				</td>
			</tr>

			<tr>
      			<td class="textoCampo">Proveedor &Uacute;nico:</td>
				<td class="textoDato">
					<@s.select 
							templateDir="custontemplates" 
							id="servicio.proveedorUnico" 
							cssClass="textarea"
							cssStyle="width:165px" 
							name="servicio.proveedorUnico" 
							list="criticoList" 
							listKey="key" 
							listValue="description" 
							value="servicio.proveedorUnico.ordinal()"
							title="Proveedor &Uacute;nico"
							onchange="javascript:proveedorUnicoSelect(this);"
							headerKey="0" 
                    		headerValue="Seleccionar"
							 />
							</td>
			<td class="textoCampo">Proveedor:</td>
			<td class="textoDato">
				<#assign proveedorClass="textareagris">
				<#assign proveeBusquedaStyle="display: none;">
				<@s.if test="servicio.proveedorUnico.ordinal() == 2">																		
					<#assign proveedorClass="textarea">
					<#assign proveeBusquedaStyle="display: block;">
				</@s.if>
					
				<div id="provUnico">
						<@s.property default="&nbsp;" escape=false value="servicio.proveedor.detalleDePersona.razonSocial" /><@s.property default="&nbsp;" escape=false value="servicio.proveedor.detalleDePersona.nombre" />
				</div>

				<div id="proveeBusqueda" style="${proveeBusquedaStyle}">
					<@s.a templateDir="custontemplates" id="seleccionarProveedor" name="seleccionarProveedor" href="javascript://nop/" cssClass="ocultarIcono">
						<img src="${request.contextPath}/common/images/buscar.gif"  border="0" align="absmiddle" hspace="3" >
					</@s.a>		
				</div>
			</td>
		</tr>
    	<tr>			
	    	<td class="textoCampo">Estado:</td>
			<td class="textoDato" colspan="3">
				<@s.property default="&nbsp;" escape=false value="servicio.estado"/>
			</td>
		</tr>	
		<tr><td colspan="4" class="textoCampo">&nbsp;</td></tr>				
	</table>
	</div>	
