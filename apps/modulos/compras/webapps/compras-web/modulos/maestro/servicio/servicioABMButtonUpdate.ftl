<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancel" type="button" name="btnVolver" value="Cancelar" class="boton"/>
				<input id="update" type="button" name="btnModificar" value="Modificar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/servicio/update.action" 
  source="update" 
  success="contentTrx" 
  failure="errorTrx"  
   parameters="servicio.oid={servicio.oid},servicio.versionNumber={servicio.versionNumber},servicio.descripcion={servicio.descripcion},servicio.rubro.oid={servicio.rubro.oid},servicio.critico={servicio.critico},servicio.tipo={servicio.tipo},servicio.proveedorUnico={servicio.proveedorUnico},servicio.proveedor.nroProveedor={servicio.proveedor.nroProveedor}"/>
  
  <@vc.htmlContent 
baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=buscar-servicios,flowControl=back"/>
 
<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/servicio/selectRubroCreateUpdate.action" 
  source="seleccionarRubro" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId=servicio-update,flowControl=change,navegacionIdBack=servicio-update,servicio.descripcion={servicio.descripcion},servicio.rubro.oid={servicio.rubro.oid},servicio.rubro.descripcion={servicio.rubro.descripcion},servicio.critico={servicio.critico},servicio.tipo={servicio.tipo},servicio.proveedorUnico={servicio.proveedorUnico},servicio.oid={servicio.oid},servicio.estado={estadoServicio},servicio.proveedor.nroProveedor={servicio.proveedor.nroProveedor}"/>
  
  
  <@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/servicio/selectProveedorABM.action" 
  source="seleccionarProveedor" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId=servicio-update,flowControl=change,navegacionIdBack=servicio-update,servicio.descripcion={servicio.descripcion},servicio.rubro.oid={servicio.rubro.oid},servicio.rubro.descripcion={servicio.rubro.descripcion},servicio.critico={servicio.critico},servicio.tipo={servicio.tipo},servicio.proveedorUnico={servicio.proveedorUnico},servicio.oid={servicio.oid},servicio.estado={estadoServicio},servicio.proveedor.oid={servicio.proveedor.oid}"/>
  
  
  