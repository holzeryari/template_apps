
<#setting locale="es_AR">

<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>

<@s.hidden id="navigationId" name="navigationId"/>
<@s.hidden id="servicio.rubro.oid" name="servicio.rubro.oid"/>
<@s.hidden id="servicio.rubro.descripcion" name="servicio.rubro.descripcion"/>
<@s.hidden id="busquedaPorRubros" name="busquedaPorRubros"/>
<@s.hidden id="estadoDuro" name="estadoDuro"/>
<@s.if test="estadoDuro == true">
	<@s.hidden id="servicio.estado" name="servicio.estado.ordinal()"/>
</@s.if>


<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
        	<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
			<tr>
				<td><b>Datos del Servicio</b></td>
			</tr>
		</table>
		
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>
			<tr>
				<td class="textoCampo">C&oacute;digo:</td>
      			<td class="textoDato">
      			<@s.textfield 
  					templateDir="custontemplates" 
					id="servicio.oid" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="servicio.oid" 
					title="C&oacute;digo" /></td>
      		
      			<td class="textoCampo">Descripci&oacute;n</td>
      			<td class="textoDato">
      				<@s.textfield 
  					templateDir="custontemplates" 
					id="servicio.descripcion" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="servicio.descripcion" 
					title="Descripcion" />
				</td>
    		</tr>					
			<tr>
				<td class="textoCampo">Rubro:</td>
      			<td class="textoDato">
						<@s.hidden id="servicio.rubro.descripcion" name="servicio.rubro.descripcion"/>
						<@s.property default="&nbsp;" escape=false value="servicio.rubro.descripcion" />									
					<@s.a templateDir="custontemplates" id="seleccionarRubro" name="seleccionarRubro" href="javascript://nop/">
						<img src="${request.contextPath}/common/images/buscar.gif"  border="0" align="absmiddle" hspace="3" >
					</@s.a>		
					</td>
				<td class="textoCampo">Tipo:</td>
      			<td class="textoDato">
      			<@s.select 
						templateDir="custontemplates" 
						id="servicio.tipo" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="servicio.tipo" 
						list="tipoList" 
						listKey="key" 
						listValue="description" 
						value="servicio.tipo.ordinal()"										
						headerKey="0" 
	                    headerValue="Todos"
	                    title="Tipo de servicio"
			       		 />
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Es Cr&iacute;tico: </td>
				<td  class="textoDato">
	      			<@s.select 
						templateDir="custontemplates" 
						id="servicio.critico" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="servicio.critico" 
						list="criticoList" 
						listKey="key" 
						listValue="description" 
						value="servicio.critico.ordinal()"
						title="Es Critico"
						headerKey="0"
						headerValue="Seleccionar" 
						 />
				</td>
				<td class="textoCampo">Estado:</td>
      			<td class="textoDato">
      			<@s.select 
							templateDir="custontemplates" 
							id="servicio.estado" 
							cssClass="textarea"
							cssStyle="width:165px" 
							name="servicio.estado" 
							list="estadoList" 
							listKey="key" 
							listValue="description" 
							value="servicio.estado.ordinal()"
							title="Estado"
							headerKey="0"
							headerValue="Todos"							
							/>
				</td>
			</tr>
			<tr>
	    		<td colspan="4" class="lineaGris" align="right">
	      			<input id="limpiar" type="button" name="btnVolver" value="Limpiar" class="boton"/>		
	      			<input id="buscar" type="button" name="btnVolver" value="Buscar" class="boton"/>
	    		</td>
			</tr>	
		</table>
	</div>			
	<!-- Resultado Filtro -->
	<@s.if test="servicioList!=null">
		<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
			<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>Servicios encontrados</td>
				</tr>
			</table>
		
		<@vc.anchors target="contentTrx" ajaxFlag="ajax">
      		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="servicioList" id="servicioSelect" pagesize=15 defaultsort=2 partialList=true size="recordSize" keepStatus=true excludedParams="resetFlag">
				<@s.if test="nameSpaceSelect!=''">								
					<#assign ref="${request.contextPath}/maestro/servicio/selectServicio.action?">										
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select" style=" text-align:center; display:block" title="Acciones">			
						<@s.a href="${ref}servicio.oid=${servicioSelect.oid?c}&servicio.descripcion=${servicioSelect.descripcion}&navegacionIdBack=${navegacionIdBack}&navigationId=${navigationId}&nameSpaceSelect=${nameSpaceSelect}&actionNameSelect=${actionNameSelect}&oidParameter=${oidParameter?c}"><img src="${request.contextPath}/common/images/seleccionar.gif" alt="Seleccionar" title="Seleccionar" width="16" height="16" border="0"></@s.a>
						&nbsp;			
					</@display.column>								  															
				</@s.if>
				
				<@s.else>
					<#assign ref="${request.contextPath}/maestro/servicio/selectServicioBack.action?">											
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select" style=" text-align:center; display:block" title="Acciones">			
						<@s.a href="${ref}servicio.oid=${servicioSelect.oid?c}&servicio.descripcion=${servicioSelect.descripcion}&navegacionIdBack=${navegacionIdBack}&navigationId=${navigationId}"><img src="${request.contextPath}/common/images/seleccionar.gif" alt="Seleccionar" title="Seleccionar" width="16" height="16" border="0"></@s.a>
						&nbsp;			
					</@display.column>	
					
				</@s.else>
				
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="oid" title="C&oacute;digo" />
				
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="descripcion" title="Descripci&oacute;n" />
				
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="rubro.descripcion" title="Rubro" />
				
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="tipo" title="Tipo" />
				
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="txt-004" property="estado" title="Estado" />					
				
			</@display.table>
		</@vc.anchors>
		</div>
	</@s.if>
			
		<div id="capaBotonera" class="capaBotonera">
		<table id="tablaBotonera" class="tablaBotonera">
			<tr> 
				<td align="left">
					<input id="cancel" type="button" name="btnVolver" value="Volver" class="boton"/>
				</td>
			</tr>	
		</table>
	</div>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/servicio/selectServicioSearch.action" 
  source="buscar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="resetFlag=true,navigationId=servicio-seleccionar,flowControl=regis,servicio.tipo={servicio.tipo},servicio.descripcion={servicio.descripcion},servicio.oid={servicio.oid},servicio.rubro.oid={servicio.rubro.oid},servicio.rubro.descripcion={servicio.rubro.descripcion},servicio.estado={servicio.estado},navegacionIdBack=${navegacionIdBack},nameSpaceSelect=${nameSpaceSelect},actionNameSelect=${actionNameSelect},oidParameter=${oidParameter?c},estadoDuro={estadoDuro},busquedaPorRubros={busquedaPorRubros},servicio.critico={servicio.critico}"/>


<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/servicio/selectRubroSearchSelect.action" 
  source="seleccionarRubro" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId=servicio-seleccionar,flowControl=change,navegacionIdBack={navigationId},servicio.tipo={servicio.tipo},servicio.descripcion={servicio.descripcion},servicio.oid={servicio.oid},servicio.rubro.oid={servicio.rubro.oid},servicio.rubro.descripcion={servicio.rubro.descripcion},servicio.estado={servicio.estado},navegacionIdBack=${navegacionIdBack},nameSpaceSelect=${nameSpaceSelect},actionNameSelect=${actionNameSelect},oidParameter=${oidParameter?c},estadoDuro={estadoDuro},busquedaPorRubros={busquedaPorRubros},servicio.critico={servicio.critico}"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/servicio/selectServicioView.action" 
  source="limpiar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId=servicio-seleccionar,flowControl=regis,navegacionIdBack=${navegacionIdBack},nameSpaceSelect=${nameSpaceSelect},actionNameSelect=${actionNameSelect},oidParameter=${oidParameter?c},estadoDuro={estadoDuro},busquedaPorRubros={busquedaPorRubros}"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId=${navegacionIdBack},flowControl=back"/>
