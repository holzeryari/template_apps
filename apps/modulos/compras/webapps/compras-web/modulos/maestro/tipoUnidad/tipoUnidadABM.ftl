<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>

<@s.hidden id="navigationId" name="navigationId"/>
<@s.hidden id="tipoUnidad.versionNumber" name="tipoUnidad.versionNumber"/>
<@s.hidden id="tipoUnidad.oid" name="tipoUnidad.oid"/>
<@s.hidden id="estadoTipoUnidad" name="tipoUnidad.estado.ordinal()"/>


<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
			<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos del Tipo de Unidad</b></td>
			</tr>
		</table>
		
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>
			<tr>
      			<td class="textoCampo">C&oacute;digo:</td>
      			<td class="textoDato">
     					<@s.textfield 
						templateDir="custontemplates" 
						id="tipoUnidad.codigo" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="tipoUnidad.codigo" 
						title="C&oacute;digo" />	
						</td>
				
      			<td class="textoCampo">Descripci&oacute;n</td>
				<td class="textoDato">
				<@s.textfield 
						templateDir="custontemplates" 
						id="tipoUnidad.descripcion" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="tipoUnidad.descripcion" 
						title="Descripci&oacute;n" />					
				</td>
			</tr>						
			<tr>		
				<td class="textoCampo">Estado:</td>
				<td class="textoDato" colspan="3">
					<@s.property default="&nbsp;" escape=false value="tipoUnidad.estado"/>
				</td>
			</tr>	
			<#-- renglon de separacion -->
			<tr><td colspan="4">&nbsp;</td></tr>				
		</table>
	</div>

