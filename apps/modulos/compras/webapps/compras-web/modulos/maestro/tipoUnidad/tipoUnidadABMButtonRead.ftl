<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="left">
				<input id="ok" type="button" name="btnOk" value="Volver" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

<@vc.htmlContent 
baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="ok" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=buscar-tipoUnidad,flowControl=back"/>
  