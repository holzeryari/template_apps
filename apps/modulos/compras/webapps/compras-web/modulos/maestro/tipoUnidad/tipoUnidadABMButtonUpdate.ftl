<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>


<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancel" type="button" name="btnVolver" value="Cancelar" class="boton"/>
				<input id="update" type="button" name="btnModificar" value="Modificar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>
<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/tipoUnidad/update.action" 
  source="update" 
  success="contentTrx" 
  failure="errorTrx"  
   parameters="tipoUnidad.oid={tipoUnidad.oid},tipoUnidad.versionNumber={tipoUnidad.versionNumber},tipoUnidad.descripcion={tipoUnidad.descripcion},tipoUnidad.codigo={tipoUnidad.codigo}"/>
  
  <@vc.htmlContent 
baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=buscar-tipoUnidad,flowControl=back"/>
 
<@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/tipoUnidad/selectRubroCreateUpdate.action" 
  source="seleccionarRubro" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId=tipoUnidad-update,flowControl=change,navegacionIdBack=tipoUnidad-update,tipoUnidad.descripcion={tipoUnidad.descripcion},tipoUnidad.rubro.oid={tipoUnidad.rubro.oid},tipoUnidad.rubro.descripcion={tipoUnidad.rubro.descripcion},tipoUnidad.critico={tipoUnidad.critico},tipoUnidad.tipo={tipoUnidad.tipo},tipoUnidad.proveedorUnico={tipoUnidad.proveedorUnico},tipoUnidad.oid={tipoUnidad.oid},tipoUnidad.estado={estadoTipoUnidad},tipoUnidad.proveedor.nroProveedor={tipoUnidad.proveedor.nroProveedor}"/>
  
  
  <@vc.htmlContent 
  baseUrl="${request.contextPath}/maestro/tipoUnidad/selectProveedorABM.action" 
  source="seleccionarProveedor" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId=tipoUnidad-update,flowControl=change,navegacionIdBack=tipoUnidad-update,tipoUnidad.descripcion={tipoUnidad.descripcion},tipoUnidad.rubro.oid={tipoUnidad.rubro.oid},tipoUnidad.rubro.descripcion={tipoUnidad.rubro.descripcion},tipoUnidad.critico={tipoUnidad.critico},tipoUnidad.tipo={tipoUnidad.tipo},tipoUnidad.proveedorUnico={tipoUnidad.proveedorUnico},tipoUnidad.oid={tipoUnidad.oid},tipoUnidad.estado={estadoTipoUnidad},tipoUnidad.proveedor.oid={tipoUnidad.proveedor.oid}"/>
  
  
  