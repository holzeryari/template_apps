<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>

<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
        	<#if mensajeAviso?exists && (mensajeAviso.length()>0)>
				<tr>
					<td class="estiloMensajeAviso"><@s.text name="${mensajeAviso}"/></td>
				</tr>
			</#if>
        	<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<#-- Lista de OC-->
	<@s.hidden id="ordenPagoList" name="ordenPagoList"/>
	<@s.hidden id="opStringList" name="opStringList"/>
	<@vc.anchors target="contentTrx">	
		
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
			<tr>
				<td><b>Datos de la Orden de Pago</b></td>
			</tr>
		</table>

		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
	    		<td class="textoCampo" colspan="4">&nbsp;</td>
	    	</tr>
			<tr>
				<td class="textoCampo">N&uacute;mero:</td>
	      		<td class="textoDato">
	      			<@s.property default="&nbsp;" escape=false value="ordenPago.numero" />
	      		</td>							
	      		<td class="textoCampo">Proveedor:</td>
						<td class="texto" align="left"  width="30%" >
						<@s.property default="&nbsp;" escape=false value="ordenPago.proveedor.detalleDePersona.razonSocial" />
						<@s.property default="&nbsp;" escape=false value="ordenPago.proveedor.detalleDePersona.nombre" />
				</td>						
			</tr>
			<tr>
				<td class="textoCampo">Beneficiario:</td>
      			<td class="textoDato">						
					<@s.property default="&nbsp;" escape=false value="ordenPago.beneficiario.razonSocial" />
					<@s.property default="&nbsp;" escape=false value="ordenPago.beneficiario.nombre" />						
               </td>
        
				<td class="textoCampo">Estado:</td>
      			<td class="textoDato">
      			<@s.property default="&nbsp;" escape=false value="ordenPago.estado" />	      			
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Fecha Emisi&oacute;n Desde:</td>
	      		<td class="textoDato">
	      			<@s.if test="fechaDesde != null">					 
					${fechaDesde?string("dd/MM/yyyy")}	
					</@s.if>	&nbsp;				
				</td>
				<td class="textoCampo">Fecha Emisi&oacute;n Hasta:</td>
      			<td class="textoDato">
      			<@s.if test="fechaHasta != null">					 
				${fechaHasta?string("dd/MM/yyyy")}	
				</@s.if>	&nbsp;		
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Origen:</td>
      			<td class="textoDato" colspan="3">
      			<@s.property default="&nbsp;" escape=false value="ordenPago.origen" />	      			
				</td>
			</tr>
			<tr>
				<td class="textoCampo" colspan="4">&nbsp;</td>
      			
			</tr>

		</table>
	</div>
	<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
		<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
			<tr>
				<td><b>Ordenes de Pago encontradas</b></td>			
			</tr>
		</table>				
	<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="ordenPagoList" id="ordenPago" defaultsort=2>	

		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select"  class="botoneraAnchoCon3" title="Acciones">		
			
			<a href="${request.contextPath}/pago/readView.action?ordenPago.oid=${ordenPago.oid?c}&navigationId=ordenPago-visualizar&flowControl=regis&navegacionIdBack=${navigationId}">
			<img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver" border="0"></a>
			&nbsp;		
			
			<@s.hidden id="${ordenPago.oid?c}" name=""/>
			<@s.checkbox label="${ordenPago.oid?c}" fieldValue="${ordenPago.oid?c}" name="opSelect" value="true" />
		</@display.column>
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="numero" title="Nro." />				
										
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" title="Proveedor">
		<#if ordenPago.proveedor?exists>  
			${ordenPago.proveedor.detalleDePersona.razonSocial}
			<#if ordenPago.proveedor.detalleDePersona.nombre?exists> 
				 ${ordenPago.proveedor.detalleDePersona.nombre}
			</#if>
		</#if>
				 
		</@display.column>
					
												
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" title="Beneficiario">  
			<#if ordenPago.beneficiario?exists>
				${ordenPago.beneficiario.razonSocial}
				<#if ordenPago.beneficiario.nombre?exists> 
				 	${ordenPago.beneficiario.nombre}
				</#if>
			</#if>
			 
		</@display.column>
		
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="fechaEmision" format="{0,date,dd/MM/yyyy}"  title="Fecha Emisi&oacute;n" /> 

		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="estado" title="Estado" />		

		</@display.table>
	
		</div>
	</@vc.anchors>
	
	<div id="capaLink" class="capaLink">
		<div id="linkCheckUncheckAllOP" name="linkImprimirOC" class="alineacionIzquierda">					
			<@s.a 
				templateDir="custontemplates"	
				cssClass="no-rewrite" 
				id="checkAllOP"
				name="checkAllOP"	
				onclick="javascript:seleccionarOPs();"
				href="javascript://nop/"
				disabled="true">
				<b id="linkSeleccionarOP" name="linkSeleccionarOP" >Seleccionar</b>
				<img src="${request.contextPath}/common/images/seleccionar.gif" border="0" align="absmiddle" hspace="3" alt="Seleccionar todo" title="Seleccionar todo">
			</@s.a>	
			<b id="espacioIntermedio" name="espacioIntermedio" >&nbsp;</b>
			<@s.a 
				templateDir="custontemplates"	
				cssClass="no-rewrite" 
				id="uncheckAllOP"
				name="uncheckAllOP"	
				onclick="javascript:deseleccionarOPs();"
				href="javascript://nop/"
				disabled="true">
				<b id="linkDeseleccionarOP" name="linkDeseleccionarOP" >Deseleccionar</b>
				<img src="${request.contextPath}/common/images/ico_titulos.gif" border="0" align="absmiddle" hspace="3" alt="Deseleccionar todo" title="Deseleccionar todo">
			</@s.a>	
		</div>

		<div id="linkImprimirOC" name="linkImprimirOC" class="alineacionDerecha">					
			<@s.a 
				templateDir="custontemplates"	
				cssClass="no-rewrite" 
				id="imprimirOP"
				name="imprimirOP"	
				onclick="javascript:listaOPSelectLink(this);"									
				href="${request.contextPath}/pago/imprimirOP.action?_"
				disabled="true">
				<b id="linkImprimirOPTexto" name="linkImprimirOPTexto" >Imprimir</b>										
				<img src="${request.contextPath}/common/images/imprimir.gif" border="0" align="absmiddle" hspace="3" >&nbsp;
			</@s.a>	
		</div>
	</div>		
	
	<div id="capaBotonera" class="capaBotonera">
		<table id="tablaBotonera" class="tablaBotonera">
			<tr> 
				<td align="left">
					<input id="cancelar" type="button" name="btnVolver" value="Volver" class="boton"/>
				</td>
			</tr>	
		</table>
	</div>

  <@vc.htmlContent 
   baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancelar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=${navegacionIdBack},flowControl=back"/>
  

	  	  				

  