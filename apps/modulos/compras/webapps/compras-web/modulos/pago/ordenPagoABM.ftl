<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign tiles=JspTaglibs["/WEB-INF/tlds/tiles-jsp.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>

<@s.hidden id="navigationId" name="navigationId"/>
<@s.hidden id="ordenPago.oid" name="ordenPago.oid"/>
<@s.hidden id="ordenPago.versionNumber" name="ordenPago.versionNumber"/>

<@s.hidden id="ordenPago.proveedor.nroProveedor" name="ordenPago.proveedor.nroProveedor"/>
<@s.hidden id="ordenPago.proveedor.detalleDePersona.razonSocial" name="ordenPago.proveedor.detalleDePersona.razonSocial"/>
<@s.hidden id="ordenPago.proveedor.detalleDePersona.nombre" name="ordenPago.proveedor.detalleDePersona.nombre"/>

<@s.hidden id="ordenPago.beneficiario.pk.identificador" name="ordenPago.beneficiario.pk.identificador"/>
<@s.hidden id="ordenPago.beneficiario.pk.secuencia" name="ordenPago.beneficiario.pk.secuencia"/>
<@s.hidden id="ordenPago.beneficiario.razonSocial" name="ordenPago.beneficiario.razonSocial"/>
<@s.hidden id="ordenPago.beneficiario.nombre" name="ordenPago.beneficiario.nombre"/>


<@s.hidden id="ordenPago.rubro.oid" name="ordenPago.rubro.oid"/>
<@s.hidden id="ordenPago.rubro.descripcion" name="ordenPago.rubro.descripcion"/>

<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
			<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos de la Orden de Pago</b></td>
				<td>
					<div align="right">
						<@s.a templateDir="custontemplates" id="modificarOrdenPago" name="modificarOrdenPago" href="javascript://nop/" cssClass="ocultarIcono">
							<b>Modificar</b><img src="${request.contextPath}/common/images/modificar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
						</@s.a>

						<@s.if test="ordenPago.estado.ordinal() == 2 || ordenPago.estado.ordinal() == 3 || ordenPago.estado.ordinal() == 4">
							<a target="_blank" 
								href="${request.contextPath}/pago/detalleOPView.action?ordenPago.numero=${ordenPago.numero?c}" id="detalleOPTesoreria" name="detalleOPTesoreria">				
								<b>Ver OP Tesorer&iacute;a</b><img src="${request.contextPath}/common/images/verCuentas.gif" border="0" align="absmiddle" alt="Ver OP Tesoreria">
							</a>				
						</@s.if>					
					</div>
				</td>
			</tr>
		</table>

		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>	
			<tr>
      			<td class="textoCampo">N&uacute;mero:</td>
      			<td class="textoDato">
      			<@s.property default="&nbsp;" escape=false value="ordenPago.numero"/></td>
      			<td  class="textoCampo">Fecha: </td>
				<td class="textoDato">
				<@s.if test="ordenPago.fechaComprobante != null">
				<#assign fechaComprobante = ordenPago.fechaComprobante> 
				${fechaComprobante?string("dd/MM/yyyy")}	
				</@s.if>				&nbsp;					
				</td>	      			
			</tr>	
			<tr>
				<td class="textoCampo">Fecha Emisi&oacute;n:</td>
      			<td class="textoDato">
					<@vc.rowCalendar 
					templateDir="custontemplates" 
					id="ordenPago.fechaEmision" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="ordenPago.fechaEmision" 
					title="Fecha Emision" />
				</td>
					
				<td class="textoCampo">Rubro:</td>
				<td class="textoDato">
					<@s.property default="&nbsp;" escape=false value="ordenPago.rubro.descripcion"/>
				</td>
      		</tr>
			<tr>
	      		<td class="textoCampo">Proveedor:</td>
				<td class="textoDato">
				<@s.property default="&nbsp;" escape=false value="ordenPago.proveedor.detalleDePersona.razonSocial" />
				<@s.property default="&nbsp;" escape=false value="ordenPago.proveedor.detalleDePersona.nombre" />
				<@s.a templateDir="custontemplates" id="seleccionarProveedor" name="seleccionarProveedor" href="javascript://nop/" cssClass="ocultarIcono">
						<img src="${request.contextPath}/common/images/buscar.gif"  border="0" align="absmiddle" hspace="3" >
				</@s.a>	

				</td>
			
				<td class="textoCampo">Beneficiario:</td>
				<td class="textoDato">
				<@s.property default="&nbsp;" escape=false value="ordenPago.beneficiario.razonSocial" />


			</td>						

		</tr>    		
		<tr>
   			<td  class="textoCampo">Origen:</td>
			<td class="textoDato">
			<@s.property default="&nbsp;" escape=false value="ordenPago.origen" />
			</td>
  			<td  class="textoCampo">Proceso:</td>
			<td class="textoDato">
			<@s.property default="&nbsp;" escape=false value="ordenPago.seccionEmisora" />
			</td>
		</tr>
		<tr>
			<td class="textoCampo">Importe a Pagar:</td>
	      	<td  class="textoDato">
			<@s.property default="&nbsp;" escape=false value="ordenPago.importeTotal"/></td>
			</td>
					
			<#if ordenPago.contrato?exists>
				<td class="textoCampo">Contrato:</td>					
				<td  class="textoDato">
				<@s.property default="&nbsp;" escape=false value="ordenPago.contrato.oid"/> -
				<@s.property default="&nbsp;" escape=false value="ordenPago.contrato.descripcion"/>
					<@vc.anchors target="contentTrx">	
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0523" 
							enabled="contrato.readable" 
							cssClass="item" 
							id="ver"
							name="ver"
							href="${request.contextPath}/contrato/readContratoView.action?contrato.oid=${ordenPago.contrato.oid?c}&navigationId=contrato-visualizar&flowControl=regis&navegacionIdBack=${navigationId}">
							<img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver"  border="0">
						</@security.a>
				</@vc.anchors>
				</td>					
			<#elseif ordenPago.fondoFijo?exists>
				<td class="textoCampo">FondoFijo:</td>										
				<td  class="textoDato">
					<@s.property default="&nbsp;" escape=false value="ordenPago.fondoFijo.numero"/> 
					<@vc.anchors target="contentTrx">						
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0703" 
							enabled="fondoFijo.readable" 
							cssClass="item" 
							id="ver"
							name="ver"
							href="${request.contextPath}/comprobante/fondoFijo/readView.action?fondoFijo.oid=${ordenPago.fondoFijo.oid?c}&navegacionIdBack=${navigationId}&navigationId=fondoFijo-visualizar&flowControl=regis">
							<img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver"  border="0">
						</@security.a>
					</@vc.anchors>
				</td>					
			<#else>
				<td class="textoCampo">&nbsp;</td>					
				<td  class="textoDato">&nbsp;</td>
			</#if>
		</tr>
		<tr>
			<td class="textoCampo">Forma Envio Correspondencia:</td>
			<td class="textoDato">
			<@s.select 
				templateDir="custontemplates" 
				id="ordenPago.formaEnvioCorrespondencia" 
				cssClass="textarea"
				cssStyle="width:165px" 
				name="ordenPago.formaEnvioCorrespondencia"
				value="ordenPago.formaEnvioCorrespondencia.ordinal()"
				list="formaEnvioComunicacionList" 
				listKey="key" 
				listValue="description" 
				title="Forma Envio Correspondencia"
				headerKey="0"
				headerValue="Seleccionar"  
				onchange="javascript:formaEnvioCorrespondenciaOP(this);"
				/>
			</td>
			<td class="textoCampo">Bolsa de Correo:</td>
			<td class="textoDato">
				<#assign bolsaDisabled="true">
				<#assign bolsaClass="textareagris">
				<@s.if test="ordenPago.formaEnvioCorrespondencia.ordinal()==1">
					<#assign bolsaDisabled="false">
					<#assign bolsaClass="textarea">	
				</@s.if>
	
				<@s.select 
					templateDir="custontemplates" 
					id="ordenPago.bolsaCorreo.codigo" 
					cssClass="${bolsaClass}"
					cssStyle="width:165px" 
					name="ordenPago.bolsaCorreo.codigo" 
					list="bolsaCorreoList" 
					listKey="codigo" 
					listValue="descripcion"
					title="Deposito"
					headerKey="0" 
                    headerValue="Seleccionar"
                    value="ordenPago.bolsaCorreo.codigo"
                    disabled="${bolsaDisabled}"	                            
                    />
	                            	  		
	  		
			</td>
		</tr>
				
		<tr>
			<td class="textoCampo">Estado:</td>
  			<td  class="textoDato" colspan="3">
				<@s.property default="&nbsp;" escape=false value="ordenPago.estado"/></td>
			</td>
		</tr>	    	
	    <tr><td colspan="4" class="textoCampo">&nbsp;</td></tr>		
	</table>		
</div>		

				
			
	