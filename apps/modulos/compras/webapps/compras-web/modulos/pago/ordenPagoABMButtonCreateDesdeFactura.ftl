<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancel" type="button" name="btnCancelar" value="Cancelar" class="boton"/>
				<input id="store" type="button" name="btnGenerar" value="Generar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>


<@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=${navegacionIdBack},flowControl=back"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/pago/generarOrdenPagoDesdeFactura.action" 
  source="store" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="ordenPago.proveedor.nroProveedor={ordenPago.proveedor.nroProveedor},ordenPago.proveedor.detalleDePersona.razonSocial={ordenPago.proveedor.detalleDePersona.razonSocial},ordenPago.proveedor.detalleDePersona.nombre={ordenPago.proveedor.detalleDePersona.nombre},ordenPago.numero={ordenPago.numero},ordenPago.fechaEmision={ordenPago.fechaEmision},navegacionIdBack=${navegacionIdBack},ordenPago.formaEnvioCorrespondencia={ordenPago.formaEnvioCorrespondencia},ordenPago.bolsaCorreo.codigo={ordenPago.bolsaCorreo.codigo},factura.pkFactura.nro_proveedor=${factura.pkFactura.nro_proveedor?c},factura.pkFactura.nro_factura=${factura.pkFactura.nro_factura?c},factura.pkFactura.sucursal=${factura.pkFactura.sucursal?c},factura.pkFactura.tip_docum=${factura.pkFactura.tip_docum?c}"/>
  
