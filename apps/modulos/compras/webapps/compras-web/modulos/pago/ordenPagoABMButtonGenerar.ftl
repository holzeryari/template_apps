<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>


<div id="capaSeparadora" class="capaSeparadora">&nbsp;</div>
<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				
				<@security.button
					templateDir="custontemplates"
					securityCode="CUF0768" 
					enabled="true"
					name="cancelar"
					id="cancelar"
					value="Aceptar"
					cssClass="boton">
				</@security.button>
				
				
				<@security.button
					templateDir="custontemplates"
					securityCode="CUF0767" 
					enabled="true"
					name="cancelar"
					id="cancelar"
					value="Cancelar"
					cssClass="boton">
				</@security.button>
				
				<@security.button
					templateDir="custontemplates"
					securityCode="CUF0767" 
					enabled="true"
					name="btnActivar"
					id="store"
					value="Autorizar"
					cssClass="boton">
				</@security.button>
			</td>
		</tr>	
	</table>
</div>

  
  
   <@vc.htmlContent 
    baseUrl="${request.contextPath}/compras/flowControl.action"  
  source="cancelar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=buscar-ordenPago,flowControl=back"/>
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/pago/generarOrdenPago.action" 
  source="store" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="ordenPago.oid={ordenPago.oid}"/>
  
  <@vc.htmlContent 
  baseUrl="${request.contextPath}/pago/updateView.action" 
  source="modificarOrdenPago" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="ordenPago.oid={ordenPago.oid},navigationId=fondoFijo-actualizar,flowControl=regis"/>