
<#setting locale="es_AR">

<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>


<@s.hidden id="navigationId" name="navigationId"/>
<div id="capaSeparadora" class="capaSeparadora">&nbsp;</div>
<@vc.anchors target="contentTrx">	
	<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
		<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
			<tr>
				<td>Cuentas Contables</td>
			</tr>
		</table>
		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="ordenPago.ordenPagoCuentaContableList" id="ordenPagoCuentaContable" pagesize=15>
        	<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="cuentaContable.codigo" title="C&oacute;digo" />
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="cuentaContable.descripcion" title="Descripci&oacute;n" />
									
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" title="Debe">
						<#if ordenPagoCuentaContable.tipo.ordinal()==0> <#-- //credito -->
							${ordenPagoCuentaContable.importe?string(",##0.00")}
						</#if>						
					</@display.column>
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" title="Haber">
						<#if ordenPagoCuentaContable.tipo.ordinal()==1> <#-- //debito -->
							${ordenPagoCuentaContable.importe?string(",##0.00")}
						</#if>
						
					</@display.column>	
						

				</@display.table>
			</div>	
		</@vc.anchors>
	