
<#setting locale="es_AR">

<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>


<@s.hidden id="navigationId" name="navigationId"/>

<@s.hidden id="ordenPago.proveedor.nroProveedor" name="ordenPago.proveedor.nroProveedor"/>
<@s.hidden id="ordenPago.proveedor.detalleDePersona.razonSocial" name="ordenPago.proveedor.detalleDePersona.razonSocial"/>
<@s.hidden id="ordenPago.proveedor.detalleDePersona.nombre" name="ordenPago.proveedor.detalleDePersona.nombre"/>

<@s.hidden id="ordenPago.beneficiario.pk.identificador" name="ordenPago.beneficiario.pk.identificador"/>
<@s.hidden id="ordenPago.beneficiario.pk.secuencia" name="ordenPago.beneficiario.pk.secuencia"/>
<@s.hidden id="ordenPago.beneficiario.razonSocial" name="ordenPago.beneficiario.razonSocial"/>
<@s.hidden id="ordenPago.beneficiario.nombre" name="ordenPago.beneficiario.nombre"/>

<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
        	<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
			<tr>
				<td><b>Datos de la Orden de Pago</b></td>
				<td>
					<!-- ordenPagoQuitado  
					<div align="right">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0761" 
							enabled="true" 
							cssClass="item" 
							id="crear"										
							href="javascript://nop/">
							<b>Agregar</b>										
							<img src="${request.contextPath}/common/images/agregar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
						</@security.a>					
					</div>
					-->
				</td>
			</tr>
		</table>

		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
	    		<td class="textoCampo" colspan="4">&nbsp;</td>
	    	</tr>
			<tr>
				<td class="textoCampo">N&uacute;mero:</td>
      			<td class="textoDato">
      			<@s.textfield 
      					templateDir="custontemplates" 
						id="ordenPago.numero" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="ordenPago.numero" 
						title="N&uacute;mero" />
				</td>							
      			<td class="textoCampo">Proveedor:</td>
				<td class="textoDato">
				<@s.property default="&nbsp;" escape=false value="ordenPago.proveedor.detalleDePersona.razonSocial" />
				<@s.property default="&nbsp;" escape=false value="ordenPago.proveedor.detalleDePersona.nombre" />
				<@s.a templateDir="custontemplates" id="seleccionarProveedor" name="seleccionarProveedor" href="javascript://nop/" cssClass="ocultarIcono">
						<img src="${request.contextPath}/common/images/buscar.gif"  border="0" align="absmiddle" hspace="3" >
				</@s.a>	
				</td>						
			</tr>
			<tr>
				<td class="textoCampo" >Beneficiario:</td>
      			<td class="textoDato">						
					<@s.property default="&nbsp;" escape=false value="ordenPago.beneficiario.razonSocial" />
					<@s.property default="&nbsp;" escape=false value="ordenPago.beneficiario.nombre" />
					<@s.a templateDir="custontemplates" id="seleccionarBeneficiario" name="seleccionarBeneficiario" href="javascript://nop/" cssClass="ocultarIcono">
							<img src="${request.contextPath}/common/images/buscar.gif"  border="0" align="absmiddle" hspace="3" >
					</@s.a>	
               </td>
        
				<td class="textoCampo">Estado:</td>
      			<td class="textoDato">
      			<@s.select 
							templateDir="custontemplates" 
							id="ordenPago.estado" 
							cssClass="textarea"
							cssStyle="width:165px" 
							name="ordenPago.estado" 
							list="estadoList" 
							listKey="key" 
							listValue="description" 
							value="ordenPago.estado.ordinal()"
							title="Estado"
							headerKey="0"
							headerValue="Todos"							
							/>
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Fecha Emisi&oacute;n Desde:</td>
      			<td class="textoDato">
					<@vc.rowCalendar 
					templateDir="custontemplates" 
					id="fechaDesde" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="fechaDesde" 
					title="Fecha Recepcion Desde" />
				</td>
				<td class="textoCampo">Fecha Emisi&oacute;n Hasta:</td>
      			<td class="textoDato">
				<@vc.rowCalendar 
					templateDir="custontemplates" 
					id="fechaHasta" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="fechaHasta" 
					title="Fecha Recepcion Hasta" />
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Origen:</td>
      			<td class="textoDato">
      			<@s.select 
						templateDir="custontemplates" 
						id="ordenPago.origen" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="ordenPago.origen" 
						list="origenList" 
						listKey="key" 
						listValue="description" 
						value="ordenPago.origen.ordinal()"
						title="Origen"
						headerKey="0"
						headerValue="Todos"							
						/>
				</td>
				
				<td class="textoCampo">Enviada/Impresa:</td>
      			<td class="textoDato">
      			<@s.select 
						templateDir="custontemplates" 
						id="ordenPago.enviadaImpresa" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="ordenPago.enviadaImpresa" 
						list="enviadaImpresaList" 
						listKey="key" 
						listValue="description" 
						value="ordenPago.enviadaImpresa.ordinal()"
						title="Enviada/Impresa"
						headerKey="0"
						headerValue="Todos"							
						/>
				</td>
			</tr>
			<tr>
    			<td colspan="4" class="lineaGris" align="right">
      			<input id="limpiar" type="button" name="btnVolver" value="Limpiar" class="boton"/>		
      			<input id="buscar" type="button" name="btnVolver" value="Buscar" class="boton"/>
    			</td>
			</tr>	
		</table>
	</div>	
	<!-- Resultado Filtro -->
	<@s.if test="ordenPagoList!=null">
		<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
				<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>Ordenes de Pago encontradas</td>
					<td>
						<div class="alineacionDerecha">
							<b>Imprimir</b>
							<@security.a 
										templateDir="custontemplates" 
										securityCode="CUF0762" 
										enabled="true" 
										cssClass="item" 
										id="imprimirAutorizadas"
										name="imprimirAutorizadas"												
										href="javascript://nop/">																				
										<img src="${request.contextPath}/common/images/enviarImprimir.gif" title="Imprimir/Enviar mail OC/OS Autorizadas" align="absmiddle" border="0" hspace="1" >
									</@security.a>
						</div>
						
					</td>
				</tr>
			</table>	
			<@vc.anchors target="contentTrx">			
          		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="ordenPagoList" id="ordenPago" pagesize=15 defaultsort=2 partialList=true size="recordSize" keepStatus=true excludedParams="resetFlag">
          		
					<@display.column headerClass="tbl-contract-service-select" class="botoneraAnchoCon2" title="Acciones">
						<div class="alineacion">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0763" 
							enabled="ordenPago.readable" 
							cssClass="item" 
							href="${request.contextPath}/pago/readView.action?ordenPago.oid=${ordenPago.oid?c}&navigationId=ordenPago-visualizar&flowControl=regis&navegacionIdBack=${navigationId}">
							<img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver"  border="0">
						</@security.a>
						</div>
						<!-- 
						<div class="alineacion">	
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0764" 
							enabled="ordenPago.updatable"
							cssClass="item"  
							href="${request.contextPath}/pago/administrarView.action?ordenPago.oid=${ordenPago.oid?c}&navigationId=ordenPago-administrar&flowControl=regis">
							<img  src="${request.contextPath}/common/images/administrar.gif" alt="Administrar" title="Administrar"  border="0">
						</@security.a>
						</div>
						
						<div class="alineacion">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0765" 
							enabled="ordenPago.eraseable"
							cssClass="item"  
							href="${request.contextPath}/pago/deleteView.action?ordenPago.oid=${ordenPago.oid?c}&navigationId=ordenPago-eliminar&flowControl=regis">
							<img  src="${request.contextPath}/common/images/eliminar.gif" alt="Eliminar" title="Eliminar"  border="0">
						</@security.a>				
						</div>
						 -->
						<div class="alineacion">
						<#if ordenPago.enable>
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0766" 
							enabled="ordenPago.enable" 
							cssClass="no-rewrite" 
							href="${request.contextPath}/pago/printPDF.action?ordenPago.oid=${ordenPago.oid?c}">
							<img  src="${request.contextPath}/common/images/imprimir.gif" alt="Imprimir" title="Imprimir"  border="0">
						</@security.a>		
						<#else>
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0766" 
							enabled="ordenPago.enable"
							cssClass="item"  
							href="">
							<img  src="${request.contextPath}/common/images/imprimir.gif" alt="Eliminar" title="Eliminar"  border="0">
						</@security.a>						
						</#if>
						</div>
					</@display.column>


					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="numero" title="Nro." />				
										
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" title="Proveedor">
					<#if ordenPago.proveedor?exists>  
						${ordenPago.proveedor.detalleDePersona.razonSocial}
						<#if ordenPago.proveedor.detalleDePersona.nombre?exists> 
							 ${ordenPago.proveedor.detalleDePersona.nombre}
						</#if>
					</#if>
						 
					</@display.column>
					
															
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" title="Beneficiario">  
						<#if ordenPago.beneficiario?exists>
							${ordenPago.beneficiario.razonSocial}
							<#if ordenPago.beneficiario.nombre?exists> 
							 	${ordenPago.beneficiario.nombre}
							</#if>
						</#if>
						 
					</@display.column>
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="fechaEmision" format="{0,date,dd/MM/yyyy}"  title="Fecha Emisi&oacute;n" /> 

					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="estado" title="Estado" />		

				</@display.table>
			</@vc.anchors>
		</div>	
	</@s.if>
	
<@vc.htmlContent 
  baseUrl="${request.contextPath}/pago/search.action"
  source="buscar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="resetFlag=true,navigationId=buscar-ordenPago,flowControl=regis,ordenPago.estado={ordenPago.estado},ordenPago.proveedor.nroProveedor={ordenPago.proveedor.nroProveedor},ordenPago.proveedor.detalleDePersona.razonSocial={ordenPago.proveedor.detalleDePersona.razonSocial},ordenPago.proveedor.detalleDePersona.nombre={ordenPago.proveedor.detalleDePersona.nombre},ordenPago.numero={ordenPago.numero},fechaDesde={fechaDesde},fechaHasta={fechaHasta},ordenPago.beneficiario.pk.identificador={ordenPago.beneficiario.pk.identificador},ordenPago.beneficiario.pk.secuencia={ordenPago.beneficiario.pk.secuencia},ordenPago.origen={ordenPago.origen},ordenPago.enviadaImpresa={ordenPago.enviadaImpresa}"/>
  		      

<@vc.htmlContent 
  baseUrl="${request.contextPath}/pago/view.action"
  source="limpiar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId=buscar-ordenPago,flowControl=regis"/>
  
  
 <!-- ordenPagoQuitado 
<@vc.htmlContent 
  baseUrl="${request.contextPath}/pago/createView.action" 
  source="crear" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=ordenPago-crear,flowControl=regis"/>
 -->
  
  <@vc.htmlContent 
  baseUrl="${request.contextPath}/pago/selectProveedorSearch.action" 
  source="seleccionarProveedor" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=change,ordenPago.estado={ordenPago.estado},ordenPago.proveedor.nroProveedor={ordenPago.proveedor.nroProveedor},ordenPago.proveedor.detalleDePersona.razonSocial={ordenPago.proveedor.detalleDePersona.razonSocial},ordenPago.proveedor.detalleDePersona.nombre={ordenPago.proveedor.detalleDePersona.nombre},ordenPago.numero={ordenPago.numero},fechaDesde={fechaDesde},fechaHasta={fechaHasta},ordenPago.beneficiario.pk.identificador={ordenPago.beneficiario.pk.identificador},ordenPago.beneficiario.pk.secuencia={ordenPago.beneficiario.pk.secuencia},ordenPago.origen={ordenPago.origen},ordenPago.enviadaImpresa={ordenPago.enviadaImpresa}"/>

  
  <@vc.htmlContent 
  baseUrl="${request.contextPath}/pago/selectPersonaSearch.action" 
  source="seleccionarBeneficiario" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=change,ordenPago.estado={ordenPago.estado},ordenPago.proveedor.nroProveedor={ordenPago.proveedor.nroProveedor},ordenPago.proveedor.detalleDePersona.razonSocial={ordenPago.proveedor.detalleDePersona.razonSocial},ordenPago.proveedor.detalleDePersona.nombre={ordenPago.proveedor.detalleDePersona.nombre},ordenPago.numero={ordenPago.numero},fechaDesde={fechaDesde},fechaHasta={fechaHasta},ordenPago.beneficiario.pk.identificador={ordenPago.beneficiario.pk.identificador},ordenPago.beneficiario.pk.secuencia={ordenPago.beneficiario.pk.secuencia},ordenPago.origen={ordenPago.origen},ordenPago.enviadaImpresa={ordenPago.enviadaImpresa}"/>
  
  
  <@vc.htmlContent 
  baseUrl="${request.contextPath}/pago/selectOPAutorizadas.action" 
  source="imprimirAutorizadas" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=ordenPago-visualizarOPImprimir,flowControl=regis,navegacionIdBack=${navigationId},ordenPago.estado={ordenPago.estado},ordenPago.proveedor.nroProveedor={ordenPago.proveedor.nroProveedor},ordenPago.proveedor.detalleDePersona.razonSocial={ordenPago.proveedor.detalleDePersona.razonSocial},ordenPago.proveedor.detalleDePersona.nombre={ordenPago.proveedor.detalleDePersona.nombre},ordenPago.numero={ordenPago.numero},fechaDesde={fechaDesde},fechaHasta={fechaHasta},ordenPago.beneficiario.pk.identificador={ordenPago.beneficiario.pk.identificador},ordenPago.beneficiario.pk.secuencia={ordenPago.beneficiario.pk.secuencia},ordenPago.origen={ordenPago.origen}"/>
  
  