<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancel" type="button" name="btnCancelar" value="Cancelar" class="boton"/>
				<input id="store" type="button" name="btnAgregar" value="Agregar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=${navegacionIdBack},flowControl=back"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/pago/createMedioPago.action" 
  source="store" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="ordenPagoMedioPago.ordenPago.oid={ordenPagoMedioPago.ordenPago.oid},ordenPagoMedioPago.medioPago={medioPago},ordenPagoMedioPago.porcentaje={ordenPagoMedioPago.porcentaje},navegacionIdBack=${navegacionIdBack},ordenPagoMedioPago.formaPago={formaPago},ordenPagoMedioPago.observaciones={ordenPagoMedioPago.observaciones}"/>
  
  
