
<#setting locale="es_AR">

<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>


<@s.hidden id="navigationId" name="navigationId"/>
<div id="capaSeparadora" class="capaSeparadora">&nbsp;</div>
<@vc.anchors target="contentTrx">	

	<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
		<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>Medios de Pago</td>
					<td>	
						<div align="right">
							<@s.a href="${request.contextPath}/pago/createMedioPagoView.action?navegacionIdBack=${navigationId}&ordenPagoMedioPago.ordenPago.oid=${ordenPago.oid?c}" templateDir="custontemplates" id="agregarComprobante" name="agregarComprobante" cssClass="ocultarIcono">
								<b>Agregar</b><img src="${request.contextPath}/common/images/agregar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
							</@s.a>		
						</div>
					</td>
				</tr>
		</table>
				
        <@display.table class="tablaDetalleCuerpo" cellpadding="3" name="ordenPago.ordenPagoMedioPagoList" id="ordenPagoMedioPago" pagesize=15 defaultsort=2 >
        	<@display.column headerClass="tbl-contract-service-select" class="botoneraAnchoCon1" title="Acciones">
						<div class="alineacion">
						<@s.a 
							templateDir="custontemplates" 
							cssClass="item" 
							id="eliminar"
							name="eliminar" 
							href="${request.contextPath}/pago/deleteMedioPago.action?ordenPagoMedioPago.oid=${ordenPagoMedioPago.oid?c}&navegacionIdBack=${navigationId}">
							<img  src="${request.contextPath}/common/images/eliminar.gif" alt="Eliminar" title="Eliminar"  border="0">
						</@s.a>				
						</div>
				
					</@display.column>
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="formaPago.descripcion" title="Forma Pago" />
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="medioPago.descripcion" title="Medio Pago" />
							
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="observaciones" title="Observaciones" />
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="porcentaje" title="Porcentaje" />
			</@display.table>
		</div>	
	</@vc.anchors>
			