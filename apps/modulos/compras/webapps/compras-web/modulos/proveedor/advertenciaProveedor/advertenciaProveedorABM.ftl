<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign tiles=JspTaglibs["/WEB-INF/tlds/tiles-jsp.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<@s.hidden id="advertenciaProveedor.oid" name="advertenciaProveedor.oid"/>
<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
		    <#if mensajeAviso?exists && (mensajeAviso.length()>0)>
				<tr>
					<td class="estiloMensajeAviso"><@s.text name="${mensajeAviso}"/></td>
				</tr>
			</#if>
			<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos de la Advertencia generada al Proveedor</b></td>
			</tr>
		</table>
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>
			<tr>
  				<td  class="textoCampo">Descripci&oacute;n: </td>
  				<td  class="textoDato" colspan="3">
					<@s.property default="&nbsp;" escape=false value="advertenciaProveedor.descripcion"/>
				</td>					
  				
			</tr>
			<tr>
				<td  class="textoCampo">Estado Advertencia: </td>
  				<td  class="textoDato">
					<@s.property default="&nbsp;" escape=false value="advertenciaProveedor.estado"/>
				</td>
				<td class="textoCampo">Proveedor:</td>
  				<td class="textoDato"><@s.property default="&nbsp;" escape=false value="advertenciaProveedor.proveedor.detalleDePersona.razonSocial"/><@s.property default="&nbsp;" escape=false value="advertenciaProveedor.proveedor.detalleDePersona.nombre"/></td>
			</tr>	
			<tr>
  				<td  class="textoCampo">Email Proveedor: </td>
  				<td  class="textoDato">
					<@s.property default="&nbsp;" escape=false value="advertenciaProveedor.proveedor.email"/>
				</td>
  				<td class="textoCampo">Situaci&oacute;n Proveedor:</td>
  				<#if advertenciaProveedor.evaluacionProveedor.tipoEvaluacion?exists && advertenciaProveedor.evaluacionProveedor.tipoEvaluacion.ordinal() == 1>
  					<td class="textoDato"><@s.property default="&nbsp;" escape=false value="advertenciaProveedor.proveedor.situacionICP"/>
  				<#else>
  					<td class="textoDato"><@s.property default="&nbsp;" escape=false value="advertenciaProveedor.proveedor.situacionICS"/>
  				</#if>
  			</tr>
  			<tr>
  				<td  class="textoCampo">Nro. Evaluaci&oacute;n: </td>
				<td  class="textoDato">
					<@s.property default="&nbsp;" escape=false value="advertenciaProveedor.evaluacionProveedor.numero"/>
				</td>					
				<td class="textoCampo">Fecha Evaluaci&oacute;n:</td>
  				<td class="textoDato">
  				<@s.if test="advertenciaProveedor.evaluacionProveedor.fechaEvaluacion != null">
					<#assign fecha = advertenciaProveedor.evaluacionProveedor.fechaEvaluacion> 
						${fecha?string("dd/MM/yyyy")} 	
				</@s.if>
				<@s.else>
					&nbsp;
				</@s.else>	
				</td>	
  			</tr>	
  			<tr>
  				<td class="textoCampo">Fecha Inicio Per&iacute;odo de Evaluaci&oacute;n:</td>
  				<td class="textoDato">
  				<@s.if test="advertenciaProveedor.evaluacionProveedor.periodoEvaluacion.fechaInicio != null">
					<#assign fecha = advertenciaProveedor.evaluacionProveedor.periodoEvaluacion.fechaInicio> 
						${fecha?string("dd/MM/yyyy")} 	
				</@s.if>
				<@s.else>
					&nbsp;
				</@s.else>	
  				</td>
  				<td  class="textoCampo">Fecha Fin Per&iacute;odo de Evaluaci&oacute;n: </td>
				<td  class="textoDato">
				<@s.if test="advertenciaProveedor.evaluacionProveedor.periodoEvaluacion.fechaFin != null">
					<#assign fecha = advertenciaProveedor.evaluacionProveedor.periodoEvaluacion.fechaFin> 
						${fecha?string("dd/MM/yyyy")} 	
				</@s.if>
				<@s.else>
					&nbsp;
				</@s.else>	
				</td>
			</tr>
			<tr>
  				<td  class="textoCampo">Tipo Evaluaci&oacute;n: </td>
				<td  class="textoDato" colspan="3">
					<@s.property default="&nbsp;" escape=false value="advertenciaProveedor.evaluacionProveedor.tipoEvaluacion"/>
				</td>	
			</tr>
			<tr><td colspan="4" class="textoCampo">&nbsp;</td></tr>
		</table>		
	</div>