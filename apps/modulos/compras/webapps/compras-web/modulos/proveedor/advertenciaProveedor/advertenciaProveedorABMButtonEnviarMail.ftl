<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancelar" type="button" name="btnVolver" value="Volver" class="boton"/>
				<input id="store" type="button" name="btnAgregar" value="Enviar Mail" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

  <@vc.htmlContent 
   baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancelar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=${navegacionIdBack},flowControl=back"/>
  
  <@vc.htmlContent 
  baseUrl="${request.contextPath}/proveedor/advertenciaProveedor/enviarAdvertenciaProveedor.action" 
  source="store" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="advertenciaProveedor.oid=${advertenciaProveedor.oid},navegacionIdBack=buscar-advertenciaProveedor"/>
  
  
  