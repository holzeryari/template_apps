<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>

<@s.hidden id="navigationId" name="navigationId"/>
	<div id="capaTituloCuerpo" class="capaTituloCuerpo">   		
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">

        	<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
			<tr>
				<td><b>Datos de la Advertencia a Proveedor</b></td>
			</tr>
		</table>

		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
	    		<td class="textoCampo" colspan="4">&nbsp;</td>
	    	</tr>		
			<tr>
	  			<td class="textoCampo">Per&iacute;odo de Evaluaci&oacute;n:</td>
      			<td class="textoDato">
      			<@s.hidden id="evaluacionProveedor" name="advertenciaProveedor.evaluacionProveedor.oid"/>
      			<@s.select 
						templateDir="custontemplates" 
						id="advertenciaProveedor.evaluacionProveedor.periodoEvaluacion.oid" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="advertenciaProveedor.evaluacionProveedor.periodoEvaluacion.oid" 
						list="periodoEvaluacionList" 
						listKey="oid" 
						listValue="fechasPeriodoEvaluacion" 
						value="advertenciaProveedor.evaluacionProveedor.periodoEvaluacion.oid" 
						title="Tipo"
						headerKey="0"
						headerValue="Todos"/>
				</td>						
	      			
      			<td class="textoCampo">Proveedor:</td>
      			<td class="textoDato">
      				<@s.property default="&nbsp;" escape=false value="advertenciaProveedor.proveedor.detalleDePersona.razonSocial" /><@s.property default="&nbsp;" escape=false value="advertenciaProveedor.proveedor.detalleDePersona.nombre" />							
					<@s.hidden id="nroProveedor" name="advertenciaProveedor.proveedor.nroProveedor"/>
					<@s.hidden id="razonSocial" name="advertenciaProveedor.proveedor.detalleDePersona.razonSocial"/>
					<@s.hidden id="nombre" name="advertenciaProveedor.proveedor.detalleDePersona.nombre"/>
					<@s.a templateDir="custontemplates" id="seleccionarProveedor" name="seleccionarProveedor" href="javascript://nop/" title="Buscar Proveedor">
						<img src="${request.contextPath}/common/images/buscar.gif"  border="0" align="absmiddle" hspace="3" >
					</@s.a>		
				</td>	
			</tr>
	      		
			<tr>
	    			<td colspan="4" class="lineaGris" align="right">
	      			<input id="limpiar" type="button" name="btnVolver" value="Limpiar" class="boton"/>		
	      			<input id="buscar" type="button" name="btnVolver" value="Buscar" class="boton"/>
	    			</td>
				</tr>	
			</table>
		</div>	
		
        <!-- Resultado Filtro -->
		<@s.if test="advertenciaProveedorList!=null">
				
			<#assign advertenciaProveedorRazonSocialProveedor = "" />
			<@s.if test=" advertenciaProveedor.proveedor!= null">
				<#assign advertenciaProveedorRazonSocialProveedor = "${advertenciaProveedor.proveedor.detalleDePersona.razonSocial}" />
			</@s.if>
			
			<#assign advertenciaProveedorNombreProveedor = "" />
			<@s.if test=" advertenciaProveedor.proveedor!= null">
				<#assign advertenciaProveedorNombreProveedor = "${advertenciaProveedor.proveedor.detalleDePersona.nombre}" />
			</@s.if>
			
			<#assign advertenciaProveedorPeriodo = "" />
			<@s.if test=" advertenciaProveedor.evaluacionProveedor.periodoEvaluacion.oid!= null">
				<#assign advertenciaProveedorPeriodo = "${advertenciaProveedor.evaluacionProveedor.periodoEvaluacion.oid}" />
			</@s.if>
			
			<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
				<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>Advertencias a Proveedores encontradas</td>
					<td>
						<div class="alineacionDerecha">
							<@security.a 
								templateDir="custontemplates" 
								securityCode="CUF0661" 
								cssClass="item" 
								href="${request.contextPath}/proveedor/advertenciaProveedor/printXLS.action?advertenciaProveedor.proveedor.detalleDePersona.razonSocial=${advertenciaProveedorRazonSocialProveedor}&advertenciaProveedor.proveedor.detalleDePersona.nombre=${advertenciaProveedorNombreProveedor}&advertenciaProveedor.evaluacionProveedor.periodoEvaluacion.oid=${advertenciaProveedorPeriodo}">
								<img src="${request.contextPath}/common/images/imprimirExcel.gif" title="Exportar a Excel" align="absmiddle" border="0" hspace="1">
							</@security.a>
						</div>
						<div class="alineacionDerecha">
							<b>Imprimir</b>
							<@security.a 
								templateDir="custontemplates" 
								securityCode="CUF0661" 
								cssClass="item" 
								href="${request.contextPath}/proveedor/advertenciaProveedor/printPDF.action?advertenciaProveedor.proveedor.detalleDePersona.razonSocial=${advertenciaProveedorRazonSocialProveedor}&advertenciaProveedor.proveedor.detalleDePersona.nombre=${advertenciaProveedorNombreProveedor}&advertenciaProveedor.evaluacionProveedor.periodoEvaluacion.oid=${advertenciaProveedorPeriodo}">
								<img src="${request.contextPath}/common/images/imprimirPdf.gif" title="Exportar a PDF" align="absmiddle" border="0" hspace="1" >
							</@security.a>
						</div>	
					</td>
				</tr>
			</table>	
				
			<@vc.anchors target="contentTrx">	
          		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="advertenciaProveedorList" id="advertenciaProveedor" pagesize=15 defaultsort=2 partialList=true size="recordSize" keepStatus=true excludedParams="resetFlag">
          		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select" class="botoneraAnchoCon4" title="Acciones">
					<div align="center">	
					<div class="alineacion">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0662" 
							enabled="advertenciaProveedor.readable" 
							cssClass="item" 
							href="${request.contextPath}/proveedor/advertenciaProveedor/readAdvertenciaProveedorView.action?advertenciaProveedor.oid=${advertenciaProveedor.oid?c}&navigationId=advertenciaProveedor-verAdvertencia&flowControl=regis&navegacionIdBack=${navigationId}">
							<img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver"  border="0">
						</@security.a>
					</div>
					
					<div class="alineacion">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0123" 
							enabled="true" 
							cssClass="item" 
							href="${request.contextPath}/maestro/proveedor/updateView.action?proveedor.nroProveedor=${advertenciaProveedor.proveedor.nroProveedor?c}&navigationId=advertenciaProveedor-modificarProveedor&flowControl=regis&navegacionIdBack=${navigationId}">
							<img  src="${request.contextPath}/common/images/modificar.gif" alt="Modificar" title="Modificar"  border="0">
						</@security.a>
					</div>
				
					<div class="alineacion">
						<@security.a  
							templateDir="custontemplates" 
							securityCode="CUF0663" 
							enabled="advertenciaProveedor.enable"
							cssClass="item"  
							href="${request.contextPath}/proveedor/advertenciaProveedor/enviarMailAdvertenciaProveedorView.action?advertenciaProveedor.oid=${advertenciaProveedor.oid?c}&navigationId=advertenciaProveedor-enviarMailAdvertencia&flowControl=regis&navegacionIdBack=${navigationId}">
							<img  src="${request.contextPath}/common/images/enviarmail.gif" alt="Enviar Mail con Advertencia" title="Enviar Mail con Advertencia"  border="0">
						</@security.a>	
					</div>
				
					<div class="alineacion">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0664" 
							enabled="true" 
							cssClass="no-rewrite" 
							href="${request.contextPath}/proveedor/advertenciaProveedor/imprimirAdvertenciaProveedor.action?advertenciaProveedor.oid=${advertenciaProveedor.oid?c}&navigationId=advertenciaProveedor-imprimirAdvertenciaProveedor&flowControl=regis&navegacionIdBack=${navigationId}">
							<img  src="${request.contextPath}/common/images/imprimir.gif" alt="Imprimir Advertencia" title="Imprimir Advertencia"  border="0">
						</@security.a>
					</div>
					</div>
					</@display.column>
					
					<#--<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="descripcion" title="Descripci&oacute;n" />-->
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" title="Proveedor">  
						${advertenciaProveedor.proveedor.detalleDePersona.razonSocial} 
						
						<#if advertenciaProveedor.proveedor.detalleDePersona.nombre?exists> 
							 ${advertenciaProveedor.proveedor.detalleDePersona.nombre}
						</#if>
						
						
					</@display.column> 
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="proveedor.email" title="Email" />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="evaluacionProveedor.numero" title="Nro Evaluaci&oacute;n" />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="evaluacionProveedor.fechaEvaluacion" format="{0,date,dd/MM/yyyy}"  title="F. Evaluaci&oacute;n"/>
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="evaluacionProveedor.periodoEvaluacion.fechaInicio" format="{0,date,dd/MM/yyyy}"  title="F. Inicio Per&iacute;odo"/>
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="evaluacionProveedor.periodoEvaluacion.fechaFin" format="{0,date,dd/MM/yyyy}"  title="F. Fin Per&iacute;odo"/>
					<#--<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="evaluacionProveedor.situacionProveedorAnterior" title="Sit. Anterior Prov." />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="evaluacionProveedor.situacionProveedorPosterior" title="Sit. Actual Prov." />-->
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="estado" title="Estado" />
												
				</@display.table>
			</div>
		</@vc.anchors>
	</@s.if>
			
<@vc.htmlContent 
  baseUrl="${request.contextPath}/proveedor/advertenciaProveedor/selectProveedorSearch.action" 
  source="seleccionarProveedor" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=change,advertenciaProveedor.oid={advertenciaProveedor.oid}, advertenciaProveedor.evaluacionProveedor.periodoEvaluacion.oid={advertenciaProveedor.evaluacionProveedor.periodoEvaluacion.oid}"/>


<@vc.htmlContent 
  baseUrl="${request.contextPath}/proveedor/advertenciaProveedor/search.action"
  source="buscar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="resetFlag=true,navigationId={navigationId},flowControl=regis,advertenciaProveedor.proveedor.nroProveedor={advertenciaProveedor.proveedor.nroProveedor}, advertenciaProveedor.proveedor.detalleDePersona.razonSocial={advertenciaProveedor.proveedor.detalleDePersona.razonSocial},advertenciaProveedor.proveedor.detalleDePersona.nombre={advertenciaProveedor.proveedor.detalleDePersona.nombre},advertenciaProveedor.evaluacionProveedor.periodoEvaluacion.oid={advertenciaProveedor.evaluacionProveedor.periodoEvaluacion.oid}"/>
  		      

<@vc.htmlContent 
  baseUrl="${request.contextPath}/proveedor/advertenciaProveedor/view.action"
  source="limpiar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=regis"/>
  

  
 
  