<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign tiles=JspTaglibs["/WEB-INF/tlds/tiles-jsp.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<@s.hidden id="calificacionEvaluacionProveedor.oid" name="calificacionEvaluacionProveedor.oid"/>
<@s.hidden id="oc_os.versionNumber" name="oc_os.versionNumber"/>

<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
			<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos de la Calificaci&oacute;n de Proveedores</b></td>
			</tr>
		</table>
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>
							
			<tr>
  				<td class="textoCampo">Calificaci&oacute;n:</td>
  				<td class="textoDato">
  					<@s.textfield 
  					templateDir="custontemplates" 
					id="califcacionEvaluacionProveedor.calificacion" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="calificacionEvaluacionProveedor.calificacion" 
					title="Calificacion" />
				</td>	
				<td class="textoCampo">Porcentaje:</td>
  				<td class="textoDato">
  					<@s.textfield 
  					templateDir="custontemplates" 
					id="califcacionEvaluacionProveedor.porcentaje" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="calificacionEvaluacionProveedor.porcentaje" 
					title="Porcentaje" />
				</td>
  			</tr>	
			<tr>
				<td class="textoCampo">Estado:</td>
  				<td  class="textoDato" colspan="3">
					<@s.property default="&nbsp;" escape=false value="calificacionEvaluacionProveedor.estado"/>
				</td>
			</tr>
			<tr><td colspan="4" class="textoCampo">&nbsp;</td>									      		      	
			</tr>
		</table>		
	</div>		

