<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>


<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancel" type="button" name="btnCancelar" value="Cancelar" class="boton"/>
				<input id="store" type="button" name="btnAgregar" value="Agregar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>


<@vc.htmlContent 
  baseUrl="${request.contextPath}/proveedor/calificacionEvaluacionProveedor/createCalificacionEvaluacionProveedor.action" 
  source="store" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="calificacionEvaluacionProveedor.calificacion={calificacionEvaluacionProveedor.calificacion}, calificacionEvaluacionProveedor.porcentaje={calificacionEvaluacionProveedor.porcentaje}"/>
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=buscar-calificacionEvaluacionProveedor,flowControl=back"/>
  