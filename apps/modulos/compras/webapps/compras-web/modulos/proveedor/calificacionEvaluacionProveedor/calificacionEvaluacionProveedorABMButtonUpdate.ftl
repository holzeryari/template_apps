<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>


<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancel" type="button" name="btnCancelar" value="Cancelar" class="boton"/>
				<input id="store" type="button" name="btnModificar" value="Modificar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/proveedor/calificacionEvaluacionProveedor/updateCalificacionEvaluacionProveedor.action" 
  source="store" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="calificacionEvaluacionProveedor.oid={calificacionEvaluacionProveedor.oid},calificacionEvaluacionProveedor.calificacion={calificacionEvaluacionProveedor.calificacion},calificacionEvaluacionProveedor.porcentaje={calificacionEvaluacionProveedor.porcentaje}"/>
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=buscar-calificacionEvaluacionProveedor,flowControl=back"/>
  