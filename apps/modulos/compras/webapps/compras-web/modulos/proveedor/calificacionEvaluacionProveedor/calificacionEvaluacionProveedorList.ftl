<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>

<@s.hidden id="navigationId" name="navigationId"/>
<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
        	<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
			<tr>
				<td><b>Datos de la Calificaci&oacute;n de Proveedores</b></td>
				<td>
					<div align="right">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0681" 
							enabled="true" 
							cssClass="item" 
							id="crear"										
							href="javascript://nop/">
							<b>Agregar</b>										
							<img src="${request.contextPath}/common/images/agregar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
						</@security.a>		
					</div>
				</td>
			</tr>
		</table>

		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
	    		<td class="textoCampo" colspan="4">&nbsp;</td>
	    	</tr>		
			<tr>
	  			<td class="textoCampo">Calificaci&oacute;n:</td>
      			<td class="textoDato">
      			<@s.textfield 
      				templateDir="custontemplates" 
					id="califcacionEvaluacionProveedor.calificacion" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="calificacionEvaluacionProveedor.calificacion" 
					title="Calificacion" />
				</td>							
      		
      			<td class="textoCampo">Estado:</td>
      			<td class="textoDato">
      			<@s.select 
						templateDir="custontemplates" 
						id="calificacionEvaluacionProveedor.estado" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="calificacionEvaluacionProveedor.estado" 
						list="estadoList" 
						listKey="key" 
						listValue="description" 
						value="calificacionEvaluacionProveedor.estado.ordinal()"
						title="Estado"
						headerKey="0"
						headerValue="Todos"							
						/>
				</td>
				
			</tr>
				
			<tr>
	    			<td colspan="4" class="lineaGris" align="right">
	      			<input id="limpiar" type="button" name="btnVolver" value="Limpiar" class="boton"/>		
	      			<input id="buscar" type="button" name="btnVolver" value="Buscar" class="boton"/>
	    			</td>
				</tr>	
			</table>
		</div>	
			
		
            <!-- Resultado Filtro -->
			
			<@s.if test="calificacionEvaluacionProveedorList!=null">
				
				<#assign calificacionEvaluacionProveedorEstado = "0" />
				<@s.if test="calificacionEvaluacionProveedor.estado != null">
						<#assign calificacionEvaluacionProveedorEstado = "${calificacionEvaluacionProveedor.estado.ordinal()}" />
				</@s.if>
				<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
				<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>Calificaciones de Proveedores encontradas</td>
					<td>
						<div class="alineacionDerecha">
							<@security.a 
								templateDir="custontemplates" 
								securityCode="CUF0682" 
								cssClass="item" 
								href="${request.contextPath}/proveedor/calificacionEvaluacionProveedor/printXLS.action?calificacionEvaluacionProveedor.calificacion=${calificacionEvaluacionProveedor.calificacion}&calificacionEvaluacionProveedor.estado=${calificacionEvaluacionProveedorEstado}">
								<img src="${request.contextPath}/common/images/imprimirExcel.gif" title="Exportar a Excel" align="absmiddle" border="0" hspace="1">
							</@security.a>
								
						</div>
						<div class="alineacionDerecha">
							<b>Imprimir</b>
							<@security.a 
								templateDir="custontemplates" 
								securityCode="CUF0682" 
								cssClass="item" 
								href="${request.contextPath}/proveedor/calificacionEvaluacionProveedor/printPDF.action?calificacionEvaluacionProveedor.calificacion=${calificacionEvaluacionProveedor.calificacion}&calificacionEvaluacionProveedor.estado=${calificacionEvaluacionProveedorEstado}">
								<img src="${request.contextPath}/common/images/imprimirPdf.gif" title="Exportar a PDF" align="absmiddle" border="0" hspace="1" >
							</@security.a>
						</div>	
					</td>
				</tr>
			</table>	
				
				<@vc.anchors target="contentTrx">	
          		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="calificacionEvaluacionProveedorList" id="calificacionEvaluacionProveedor" pagesize=15 defaultsort=3 partialList=true size="recordSize" keepStatus=true excludedParams="resetFlag">
          		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select" class="botoneraAnchoCon4" title="Acciones">
						<div class="alineacion">
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0683" 
							enabled="calificacionEvaluacionProveedor.readable" 
							cssClass="item" 
							href="${request.contextPath}/proveedor/calificacionEvaluacionProveedor/readCalificacionEvaluacionProveedorView.action?calificacionEvaluacionProveedor.oid=${calificacionEvaluacionProveedor.oid?c}&navigationId=calificacionEvaluacionProveedor-visualizar&flowControl=regis&navegacionIdBack=${navigationId}">
							<img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver"  border="0">
						</@security.a>
						</div>
						
						<div class="alineacion">	
						<@security.a 
							templateDir="custontemplates" 
							securityCode="CUF0684" 
							enabled="calificacionEvaluacionProveedor.updatable"
							cssClass="item"  
							href="${request.contextPath}/proveedor/calificacionEvaluacionProveedor/updateCalificacionEvaluacionProveedorView.action?calificacionEvaluacionProveedor.oid=${calificacionEvaluacionProveedor.oid?c}&navigationId=calificacionEvaluacionProveedor-modificar&flowControl=regis">
							<img  src="${request.contextPath}/common/images/modificar.gif" alt="Modificar" title="Modificar"  border="0">
						</@security.a>
						</div>
						
						<div class="alineacion">
						<@security.a  
								templateDir="custontemplates" 
								securityCode="CUF0685" 
								enabled="calificacionEvaluacionProveedor.eraseable"
								cssClass="item"  
								href="${request.contextPath}/proveedor/calificacionEvaluacionProveedor/deleteCalificacionEvaluacionProveedorView.action?calificacionEvaluacionProveedor.oid=${calificacionEvaluacionProveedor.oid?c}&navigationId=calificacionEvaluacionProveedor-eliminar&flowControl=regis">
								<img  src="${request.contextPath}/common/images/eliminar.gif" alt="Eliminar" title="Eliminar"  border="0">
							</@security.a>	
						</div>	
							
						 <div class="alineacion">
							<#if calificacionEvaluacionProveedor.estado == "Activo">
								<@security.a 
									templateDir="custontemplates" 
									securityCode="CUF0686" 
									enabled="true"
									cssClass="item"  
									href="${request.contextPath}/proveedor/calificacionEvaluacionProveedor/desactivarCalificacionEvaluacionProveedorView.action?calificacionEvaluacionProveedor.oid=${calificacionEvaluacionProveedor.oid?c}&navigationId=calificacionEvaluacionProveedor-desactivar&flowControl=regis">
									<img src="${request.contextPath}/common/images/desactivar.gif" alt="Desactivar" title="Desactivar" border="0">
								</@security.a>			
							<#else>
								<@security.a 
									templateDir="custontemplates" 
									securityCode="CUF0686" 
									enabled="true"
									cssClass="item"  
									href="${request.contextPath}/proveedor/calificacionEvaluacionProveedor/activarCalificacionEvaluacionProveedorView.action?calificacionEvaluacionProveedor.oid=${calificacionEvaluacionProveedor.oid?c}&navigationId=calificacionEvaluacionProveedor-activar&flowControl=regis">
									<img src="${request.contextPath}/common/images/activar.gif" alt="Activar" title="Activar" border="0">
								</@security.a>								
							</#if>	
						</div>
						
					</@display.column>
					
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="calificacion" title="Calificaci&oacute;n" />
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="porcentaje" title="Porcentaje" />	
					<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="estado" title="Estado" />
											
				</@display.table>
			</@vc.anchors>
			</div>
		</@s.if>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/proveedor/calificacionEvaluacionProveedor/search.action"
  source="buscar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="resetFlag=true,navigationId={navigationId},flowControl=regis,calificacionEvaluacionProveedor.calificacion={calificacionEvaluacionProveedor.calificacion},calificacionEvaluacionProveedor.estado={calificacionEvaluacionProveedor.estado}"/>
  		      

<@vc.htmlContent 
  baseUrl="${request.contextPath}/proveedor/calificacionEvaluacionProveedor/view.action"
  source="limpiar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=regis"/>
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/proveedor/calificacionEvaluacionProveedor/createCalificacionEvaluacionProveedorView.action" 
  source="crear" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=calificacionEvaluacionProveedor-creacion,flowControl=regis"/>
  
 
  