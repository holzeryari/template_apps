
<#setting locale="es_AR">

<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>

<@s.hidden id="navigationId" name="navigationId"/>
<@s.hidden id="navegacionIdBack" name="navegacionIdBack"/>
<@s.hidden id="proveedorDuro" name="proveedorDuro"/>
<@s.hidden id="nroProveedor" name="consultaCuentaProveedor.proveedor.nroProveedor"/>
<@s.hidden id="razonSocial" name="consultaCuentaProveedor.proveedor.detalleDePersona.razonSocial"/>
<@s.hidden id="nombre" name="consultaCuentaProveedor.proveedor.detalleDePersona.nombre"/>

<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
        	<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
			<tr>
				<td><b>Datos de los Comprobantes</b></td>
			</tr>
		</table>
			
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
	    		<td class="textoCampo" colspan="4">&nbsp;</td>
	    	</tr>

			<tr>
      			<td class="textoCampo">Proveedor:</td>
      			<td class="textoDato" colspan="3">
      			<@s.property default="&nbsp;" escape=false value="consultaCuentaProveedor.proveedor.detalleDePersona.razonSocial" />
      			<@s.property default="&nbsp;" escape=false value="consultaCuentaProveedor.proveedor.detalleDePersona.nombre" />
					<@s.a templateDir="custontemplates" 
					id="seleccionarProveedor" name="seleccionarProveedor" href="javascript://nop/" title="Buscar Proveedor" cssClass="ocultarIcono">
						<img src="${request.contextPath}/common/images/buscar.gif"  border="0" align="absmiddle" hspace="3" >
					</@s.a>		
				</td>	
			</tr>
			<tr>
				<td class="textoCampo">Fecha Emisi&oacute;n Desde:</td>
      			<td class="textoDato">
				<@vc.rowCalendar 
					templateDir="custontemplates" 
					id="fechaDesde" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="fechaDesde" 
					title="Fecha Entrega Desde" />
				</td>
				<td class="textoCampo">Fecha Emisi&oacute;n Hasta:</td>
      			<td class="textoDato">
				<@vc.rowCalendar 
					templateDir="custontemplates" 
					id="fechaHasta" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="fechaHasta" 
					title="Fecha Entrega Hasta" />
				</td>
			</tr>
			<tr>
				<td colspan="4" class="gris11-linea-bot" align="right">
				<#if !(proveedorDuro?exists && proveedorDuro)>
					<input id="limpiar" type="button" name="btnVolver" value="Limpiar" class="boton"/>
				</#if>		
					<input id="buscar" type="button" name="btnBuscar" value="Buscar" class="boton"/>
				</td>
			</tr>
		</table>
	</div>

		<!-- Resultado Filtro -->
		<@s.if test="consultaCuentaProveedorList!=null">
			<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
			<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
				<tr>
					<td>Comprobantes encontrados</td>
				</tr>
			</table>
			
					
			<@vc.anchors target="contentTrx">
			<table class="tablaDetalleCuerpo" cellpadding="3" >	
				<tr>
					<th align="center"  class="botoneraAnchoCon1" >Acciones</th>												
					<th align="center">Fecha Emisi&oacute;n</th>
					<th align="center">Clase Comprobante</th>
					<th align="center">Numero</th>
					<th align="center">Estado</th>
					<th align="center">Debe</th>
					<th align="center">Haber</th>
				</tr>
					
				<#assign totalDebe =0>
				<#assign totalHaber =0>
				<#assign consultaLista = consultaCuentaProveedorList>
				<#list consultaLista as consulta>	
				
				<#if consulta.clase.ordinal()==1 || consulta.clase.ordinal()==2 || consulta.clase.ordinal()==3 || consulta.clase.ordinal()==5>
						
						<tr>
						<td>
							<div class="alineacion">
							<@security.a 
								templateDir="custontemplates" 
								securityCode="CUF0643" 
								enabled="factura.readable" 
								cssClass="item" 
								id="ver"
								name="ver"
								href="${request.contextPath}/comprobante/factura/readView.action?factura.pkFactura.nro_factura=${consulta.factura.pkFactura.nro_factura?c}&factura.pkFactura.nro_proveedor=${consulta.factura.pkFactura.nro_proveedor?c}&factura.pkFactura.sucursal=${consulta.factura.pkFactura.sucursal?c}&factura.pkFactura.tip_docum=${consulta.factura.pkFactura.tip_docum?c}&navigationId=factura-visualizar&flowControl=regis&navegacionIdBack=${navigationId}">
								<img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver"  border="0">
							</@security.a>
							</div>						
						</td>
						
						<td>${consulta.factura.fechaEmision?string("dd/MM/yyyy")}</td>	
						<td>${consulta.clase}</td>
						<td class="estiloNumero">${consulta.factura.numeroString}</td>
						<td>${consulta.factura.estado}</td>
						<td class="estiloNumero">
							<#if consulta.clase.ordinal()==2 >
								<#assign totalFactura = consulta.factura.totalFactura> 
								${totalFactura?string(",##0.00")}
								<#assign totalDebe =totalFactura+totalDebe>
							<#else>	
								&nbsp;
							</#if>
						</td>								
						<td class="estiloNumero">
							<#if consulta.clase.ordinal()==1 || consulta.clase.ordinal()==3 || consulta.clase.ordinal()==5>
								<#assign totalFactura = consulta.factura.totalFactura> 
								${totalFactura?string(",##0.00")}
									<#assign totalHaber =totalFactura+totalHaber>
							<#else>	
								&nbsp;
							</#if>
						</td>
		
					</tr>
					</#if>
					
					<#if consulta.clase.ordinal()==4>
							
						<tr>
						<td>
						<div class="alineacion">
							<@security.a 
								templateDir="custontemplates" 
								securityCode="CUF0763" 
								enabled="ordenPago.readable" 
								cssClass="item" 
								id="ver"
								name="ver"
								href="${request.contextPath}/pago/readView.action?ordenPago.oid=${consulta.ordenPago.oid?c}&navigationId=ordenPago-visualizar&flowControl=regis&navegacionIdBack=${navigationId}">
								<img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver"  border="0">
							</@security.a>
						</div>						
						</td>
						
						<td>${consulta.ordenPago.fechaEmision?string("dd/MM/yyyy")}</td>							
						<td>${consulta.clase}</td>
						<td class="estiloNumero">${consulta.ordenPago.numero}</td>
						<td>${consulta.ordenPago.estado}</td>
						<td class="estiloNumero">
							<#assign total = consulta.ordenPago.importeTotal> 
							${total?string(",##0.00")}
								<#assign totalDebe =total+totalDebe>
						</td>								
						<td>&nbsp;</td>
		
					</tr>
					</#if>
				</#list>
				<tr>
				<td colspan="5">&nbsp;</td>
				<td  class="estiloNumeroColor">${totalDebe?string(",##0.00")}</td>
				<td  class="estiloNumeroColor">${totalHaber?string(",##0.00")}</td>
				</tr>
				<tr>
				<td colspan="5" class="estiloNumero">Diferencia:</td>
				<td colspan="2"  class="estiloNumeroColor">
				<#assign dif =totalDebe-totalHaber>
				${dif?string(",##0.00")}</td>				
				</tr>
				
				</@vc.anchors>
				</@s.if>	
			</div>	
			<#if proveedorDuro?exists && proveedorDuro>
				<div id="capaBotonera" class="capaBotonera">
					<table id="tablaBotonera" class="tablaBotonera">
						<tr> 
							<td align="left">
								<input id="cancel" type="button" name="btnVolver" value="Volver" class="boton"/>
							</td>
						</tr>	
					</table>
				</div>
				
			  <@vc.htmlContent 
				  baseUrl="${request.contextPath}/compras/flowControl.action" 
				  source="cancel" 
				  success="contentTrx" 
				  failure="errorTrx" 
				  parameters="navigationId=${navegacionIdBack},flowControl=back"/>
			</#if>
			
<@vc.htmlContent 
  baseUrl="${request.contextPath}/proveedor/consultaCuentaProveedor/search.action" 
  source="buscar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=regis,fechaDesde={fechaDesde},fechaHasta={fechaHasta},consultaCuentaProveedor.proveedor.nroProveedor={nroProveedor},consultaCuentaProveedor.proveedor.detalleDePersona.razonSocial={razonSocial},consultaCuentaProveedor.proveedor.detalleDePersona.nombre={nombre},proveedorDuro={proveedorDuro},navegacionIdBack={navegacionIdBack}"/>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/proveedor/consultaCuentaProveedor/view.action" 
  source="limpiar" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=regis"/>

  <@vc.htmlContent 
  baseUrl="${request.contextPath}/proveedor/consultaCuentaProveedor/selectProveedorSearch.action" 
  source="seleccionarProveedor" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId={navigationId},flowControl=change,fechaDesde={fechaDesde},fechaHasta={fechaHasta},consultaCuentaProveedor.proveedor.nroProveedor={nroProveedor},consultaCuentaProveedor.proveedor.detalleDePersona.razonSocial={razonSocial},consultaCuentaProveedor.proveedor.detalleDePersona.nombre={nombre}"/>
  