<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>

<@vc.anchors target="contentTrx" ajaxFlag="ajax">
<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
		<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
			<tr>
				<td>Evaluaciones anteriores del Proveedor</td>	
			</tr>
		</table>
		<!-- Resultado Filtro -->						
		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="evaluacionAntProveedorList" id="evaluacionProveedor" defaultsort=2>	
	
	
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="numero" title="N&uacute;mero" />
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="fechaEvaluacion" format="{0,date,dd/MM/yyyy}"  title="F. Eval."/>
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="esCargaInicial" title="Carga Inicial" />
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="periodoEvaluacion.fechasPeriodoEvaluacion" title="Per&iacute;odo"/>
		<#--<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="periodoEvaluacion.fechaFin" format="{0,date,dd/MM/yyyy}"  title="Fecha Fin Per&iacute;odo"/>-->
		<#--<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="tipoEvaluacion" title="Tipo" />-->
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="ICP" title="ICP[%]" />
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="ICS" title="ICS[%]" /> 									
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="indiceEvaluador" title="Ind. Eval.[%]" />
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="situacionProveedorAnterior" title="Sit. Ant. Prov." />
		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="situacionProveedorPosterior" title="Sit. Post. Prov." />	
	</@display.table>
	</div>	
</@vc.anchors>





