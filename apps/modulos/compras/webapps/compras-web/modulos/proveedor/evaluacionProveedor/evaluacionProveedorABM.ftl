<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign tiles=JspTaglibs["/WEB-INF/tlds/tiles-jsp.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<@s.hidden id="evaluacionProveedor.oid" name="evaluacionProveedor.oid"/>
<@s.hidden id="evaluacionProveedor.versionNumber" name="evaluacionProveedor.versionNumber"/>
<@s.hidden id="evaluacionProveedor.periodoEvaluacion.fechaInicioStr" name="evaluacionProveedor.periodoEvaluacion.fechaInicioStr"/>
<@s.hidden id="evaluacionProveedor.periodoEvaluacion.fechaInicio" name="evaluacionProveedor.periodoEvaluacion.fechaInicio"/>
<@s.hidden id="evaluacionProveedor.periodoEvaluacion.oid" name="evaluacionProveedor.periodoEvaluacion.oid"/>
<@s.hidden id="evaluacionProveedor.periodoEvaluacion.fechaFin" name="evaluacionProveedor.periodoEvaluacion.fechaFin"/>
<@s.hidden id="evaluacionProveedor.periodoEvaluacion.fechaFinStr" name="evaluacionProveedor.periodoEvaluacion.fechaFinStr"/>
<@s.hidden id="evaluacionProveedor.fechaEvaluacion" name="evaluacionProveedor.fechaEvaluacion"/>
<@s.hidden id="evaluacionProveedor.fechaEvaluacionStr" name="evaluacionProveedor.fechaEvaluacionStr"/>
<@s.hidden id="nroProveedor" name="evaluacionProveedor.proveedor.nroProveedor"/>
<@s.hidden id="razonSocial" name="evaluacionProveedor.proveedor.detalleDePersona.razonSocial"/>
<@s.hidden id="nombre" name="evaluacionProveedor.proveedor.detalleDePersona.nombre"/>

<#-- esCargaInicial = Seleccionar -->
	<#--<@s.if test="evaluacionProveedor.esCargaInicial.ordinal() == 0">--> 
		<#assign tituloFechaInicioStyle="display:none;">
		<#assign campoFechaInicioStyle="display:none;">
		<#assign tituloFechaFinStyle="display:none;">
		<#assign campoFechaFinStyle="display:none;">
		<#assign tituloTipoEvaluacionStyle="display:none;">
		<#assign campoTipoEvaluacionStyle="display:none;">
		<#assign tituloProveedorStyle="display:none;">
		<#assign campoProveedorStyle="display:none;">
		<#assign tituloICPStyle="display:none;">
		<#assign tituloICSStyle="display:none;">
		<#assign campoICPStyle="display:none;">
		<#assign campoICSStyle="display:none;">
		<#assign tituloEvaluacionStyle="display:none;">
		<#assign campoEvaluacionStyle="display:none;">
		<#--<#assign iconoCargaInicialStyle="display:none;">-->
		<#--<#assign iconoEvaluacionPeriodoStyle="display:none;">-->
		<#assign proveedorBusquedaStyle="display:none;">
		<#assign FADisabled="true">
		<#assign ICPDisabled="true">
		<#assign ICSDisabled="true">
		<#assign FAClass="textareagris">
		<#assign ICPClass="textareagris">
		<#assign ICSClass="textareagris">
		<#assign tituloFAStyle="display:none;">
		<#assign tituloDPStyle="display:none;">
		<#assign comboCalificacionStyle="display:none;">
		<#assign comboCargaInicialDisabled="false">
		<#assign comboCargaInicialClass="textarea">	
		<#assign comboTipoEvaluacionDisabled="false">
		<#assign comboTipoEvaluacionClass="textarea">	
	<#--</@s.if>-->
	
	<#-- esCargaInicial = NO -->
	<@s.if test="evaluacionProveedor.esCargaInicial.ordinal() == 1"> 
		<#assign tituloFechaInicioStyle="display:block;">
		<#assign campoFechaInicioStyle="display:block;">
		<#assign tituloFechaFinStyle="display:block;">
		<#assign campoFechaFinStyle="display:block;">
		<#assign tituloTipoEvaluacionStyle="display:block;">
		<#assign campoTipoEvaluacionStyle="display:block;">
		<#assign tituloProveedorStyle="display:block;">
		<#assign campoProveedorStyle="display:block;">
		<#assign tituloICPStyle="display:none;">
		<#assign tituloICSStyle="display:none;">
		<#assign campoICPStyle="display:none;">
		<#assign campoICSStyle="display:none;">
		<#assign tituloEvaluacionStyle="display:block;">
		<#assign campoEvaluacionStyle="display:block;">
		<#--<#assign iconoCargaInicialStyle="display:none;">
		<#assign iconoEvaluacionPeriodoStyle="display:block;">-->
		<#assign proveedorBusquedaStyle="display:block;">
		<#assign tituloFAStyle="display:block;">
		<#assign tituloDPStyle="display:block;">
		<#assign comboCalificacionStyle="display:block;">
		<#assign comboCargaInicialDisabled="true">
		<#assign comboCargaInicialClass="textareagris">
		<#assign comboTipoEvaluacionDisabled="true">
		<#assign comboTipoEvaluacionClass="textareagris">
		
		
		
		
		<#-- tipoEvaluacion = seleccionar -->
		<@s.if test="evaluacionProveedor.tipoEvaluacion.ordinal() == 0">
		 		<#assign FADisabled="true">
				<#assign FAClass="textareagris">
		</@s.if>
		<#-- tipoEvaluacion = desempe�o de proveedor -->
		<@s.if test="evaluacionProveedor.tipoEvaluacion.ordinal() == 1">
		 	<#assign FADisabled="false">
			<#assign FAClass="textarea">
		</@s.if>
		<#-- tipoEvaluacion = post servicio -->
		<@s.if test="evaluacionProveedor.tipoEvaluacion.ordinal() == 2">
		 	<#assign FADisabled="false">
			<#assign FAClass="textarea">
		</@s.if>
	</@s.if>
	
	<#-- esCargaInicial = SI -->
	<@s.if test="evaluacionProveedor.esCargaInicial.ordinal() == 2"> 
		<#assign tituloFechaInicioStyle="display:none;">
		<#assign campoFechaInicioStyle="display:none;">
		<#assign tituloFechaFinStyle="display:none;">
		<#assign campoFechaFinStyle="display:none;">
		<#assign tituloTipoEvaluacionStyle="display:block;">
		<#assign campoTipoEvaluacionStyle="display:block;">
		<#assign tituloProveedorStyle="display:block;">
		<#assign campoProveedorStyle="display:block;">
		<#assign tituloICPStyle="display:block;">
		<#assign tituloICSStyle="display:block;">
		<#assign campoICPStyle="display:block;">
		<#assign campoICSStyle="display:block;">
		<#assign tituloEvaluacionStyle="display:none;">
		<#assign campoEvaluacionStyle="display:none;">
		<#--<#assign iconoCargaInicialStyle="display:block;">
		<#assign iconoEvaluacionPeriodoStyle="display:none;">-->
		<#assign tituloFAStyle="display:none;">
		<#assign tituloDPStyle="display:none;">
		<#assign comboCalificacionStyle="display:none;">
		<#assign comboCargaInicialDisabled="true">
		<#assign comboCargaInicialClass="textareagris">
		
		<#if evaluacionProveedor.proveedor?exists>
			<#assign comboTipoEvaluacionDisabled="true">
			<#assign comboTipoEvaluacionClass="textareagris">
			<#assign proveedorBusquedaStyle="display:block;">
		<#else>
			<#assign comboTipoEvaluacionDisabled="false">
			<#assign comboTipoEvaluacionClass="textarea">
			<#assign proveedorBusquedaStyle="display:none;">
		</#if>
		<#assign FADisabled="true">
		<#assign ICPDisabled="true">
		<#assign ICSDisabled="true">
		<#assign FAClass="textareagris">
		<#assign ICPClass="textareagris">
		<#assign ICSClass="textareagris">
		
		
		<#-- tipoEvaluacion = seleccionar -->
		<@s.if test="evaluacionProveedor.tipoEvaluacion.ordinal() == 0">
		 		<#assign ICPDisabled="true">
				<#assign ICSDisabled="true">
				<#assign ICPClass="textareagris">
				<#assign ICSClass="textareagris">	
		</@s.if>
		<#-- tipoEvaluacion = desempe�o de proveedor -->
		<@s.if test="evaluacionProveedor.tipoEvaluacion.ordinal() == 1">
		 	<#assign ICPDisabled="false">
			<#assign ICPClass="textarea">
		</@s.if>
		<#-- tipoEvaluacion = post servicio -->
		<@s.if test="evaluacionProveedor.tipoEvaluacion.ordinal() == 2">
		 	<#assign ICSDisabled="false">
			<#assign ICSClass="textarea">
		</@s.if>
	</@s.if>
	
	
	<@s.if test="evaluacionProveedor.tipoEvaluacion.ordinal() == 1">																		
		<#assign iconoCargaInicialStyle="display:block;float:left;">
	</@s.if>
	<@s.if test="evaluacionProveedor.tipoEvaluacion.ordinal() == 2">																		
		<#assign iconoEvaluacionPeriodoStyle="display:block;float:left;">
	</@s.if>
	

							
	<@s.if test="evaluacionProveedor.tipoEvaluacion.ordinal() == 1">																		
		<#assign tituloFAStyle="display:block;">
		<#assign tituloDPStyle="display:none;">
		<#assign comboCalificacionStyle="display:block;">
		<#assign proveedorBusquedaStyle="display:block;">
	</@s.if>
	<@s.if test="evaluacionProveedor.tipoEvaluacion.ordinal() == 2">																		
		<#assign tituloDPStyle="display:block;">
		<#assign tituloFAStyle="display:none;">
		<#assign comboCalificacionStyle="display:block;">
		<#assign proveedorBusquedaStyle="display:block;">
	</@s.if>



<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	
	<div id="capaTituloAccion" class="capaTituloAccion">
		
		<table id="tablaTituloAccion" class="tablaTituloAccion">
			<#if mensajeAviso?exists && (mensajeAviso.length()>0)>
				<tr>
					<td class="estiloMensajeAviso"><@s.text name="${mensajeAviso}"/></td>
				</tr>
			</#if>
			<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos de la Evaluaci&oacute;n del Proveedor</b></td>
				<td>
					<div align="right">
						<@s.a templateDir="custontemplates" id="limpiarCargaInicial" name="limpiarCargaInicial" href="javascript://nop/" cssClass="ocultarIcono">
							<b>Limpiar</b><img src="${request.contextPath}/common/images/limpiar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
						</@s.a>
					</div>
				</td>	
			</tr>
		</table>
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>
			<tr>
      			<td class="textoCampo">N&uacute;mero:</td>
      			<td class="textoDato"><@s.property default="&nbsp;" escape=false value="evaluacionProveedor.numero"/></td>
      			<td  class="textoCampo">Fecha Evaluaci&oacute;n: </td>
				<td class="textoDato">
      				<@s.if test="evaluacionProveedor.fechaEvaluacion != null">
					<#assign fechaEvaluacion = evaluacionProveedor.fechaEvaluacion> 
					${fechaEvaluacion?string("dd/MM/yyyy")}	
					</@s.if>	
      				<#--<@s.property default="&nbsp;" escape=false value="evaluacionProveedor.fechaEvaluacion"/>-->
				</td>
			</tr>
					
			<tr>
				<td class="textoCampo">Es Carga Inicial:</td>
      			<td class="textoDato" colspan="3">
      				<@s.select 
						templateDir="custontemplates" 
						id="evaluacionProveedor.esCargaInicial" 
						cssClass="${comboCargaInicialClass}"
						cssStyle="width:165px" 
						name="evaluacionProveedor.esCargaInicial" 
						list="esCargaInicialList" 
						listKey="key" 
						listValue="description" 
						value="evaluacionProveedor.esCargaInicial.ordinal()"
						title="Carga Inicial"
						headerKey="0"
						headerValue="Seleccionar"
						disabled="${comboCargaInicialDisabled}"	
						onchange="javascript:cargaInicialEvaluacion(this);"/>
				</td>
			</tr>	
					
			<tr>
				<td  class="textoCampo"><div id="tituloFechaInicio" style="${tituloFechaInicioStyle}">Fecha Inicio Per&iacute;odo:</div></td> 
				<td class="textoDato">
      				<div id="campoFechaInicio" style="${campoFechaInicioStyle}">
      					<@s.property default="&nbsp;" escape=false value="evaluacionProveedor.periodoEvaluacion.fechaInicioStr"/>
      				</div>	
				</td>										
			
				<td  class="textoCampo"><div id="tituloFechaFin" style="${tituloFechaFinStyle}">Fecha Fin Per&iacute;odo:</div></td> 
				<td class="textoDato">
      				<div id="campoFechaFin" style="${campoFechaFinStyle}">
      					<@s.property default="&nbsp;" escape=false value="evaluacionProveedor.periodoEvaluacion.fechaFinStr"/>
      				</div>	
				</td>
			</tr>
			
			<tr>
				<td class="textoCampo">
					<div id="tituloTipoEvaluacion" style="${tituloTipoEvaluacionStyle}">
						Tipo Evaluaci&oacute;n:
					</div>
				</td>
      			<td class="textoDato">
      				<div id="campoTipoEvaluacion" style="${campoTipoEvaluacionStyle}">
      					<@s.select 
							templateDir="custontemplates" 
							id="evaluacionProveedor.tipoEvaluacion" 
							cssClass="${comboTipoEvaluacionClass}"
							cssStyle="width:165px" 
							name="evaluacionProveedor.tipoEvaluacion" 
							list="tipoEvaluacionList" 
							listKey="key" 
							listValue="description" 
							value="evaluacionProveedor.tipoEvaluacion.ordinal()"
							title="Tipo"
							headerKey="0"
							headerValue="Seleccionar"
							disabled="${comboTipoEvaluacionDisabled}"		
							onchange="javascript:tipoEvaluacion(this);"						
							/>
					</div>
				</td>
					
				<td class="textoCampo">
					<div id="tituloProveedor" style="${tituloProveedorStyle}">Proveedor:</div>
				</td>
      			<td class="textoDato">
					<div id="campoProveedor" style="${campoProveedorStyle}">	      				
	      				<div id="proveedorTexto" style="float:left;">
	      					<@s.property default="&nbsp;" escape=false value="evaluacionProveedor.proveedor.detalleDePersona.razonSocial" /><@s.property default="&nbsp;" escape=false value="evaluacionProveedor.proveedor.detalleDePersona.nombre" />							
						</div>

						<div id="proveedorBusqueda" style="${proveedorBusquedaStyle}">
						<#--<div id="iconoCargaInicial" style="${iconoCargaInicialStyle}">
							<@s.a templateDir="custontemplates" id="seleccionarProveedorInicial" name="seleccionarProveedorInicial" href="javascript://nop/"  cssClass="texto1">
								<img src="${request.contextPath}/common/images/buscar.gif"  border="0" align="absmiddle" hspace="3" >
							</@s.a>
						</div>
						<div id="iconoEvaluacionPeriodo" style="${iconoEvaluacionPeriodoStyle}">
							<@s.a templateDir="custontemplates" id="seleccionarProveedor" name="seleccionarProveedor" href="javascript://nop/"  cssClass="texto1">
								<img src="${request.contextPath}/common/images/buscar.gif"  border="0" align="absmiddle" hspace="3" >
							</@s.a>
						</div>-->
							<@s.a templateDir="custontemplates" id="seleccionarProveedor" name="seleccionarProveedor" href="javascript://nop/"  cssClass="ocultarIcono">
								<img src="${request.contextPath}/common/images/buscar.gif"  border="0" align="absmiddle" hspace="3" >
							</@s.a>
						</div>	
					</div>	
				</td>
					
			</tr>
					
			<tr>
				<td class="textoCampo">
					<div id="tituloICP" style="${tituloICPStyle}">Indice de Calidad de Productos/Bienes [%]:</div>
				</td>
      			<td class="textoDato">
      				<div id="campoICP" style="${campoICPStyle}">
	      				<@s.textfield 
	      					templateDir="custontemplates" 
							id="evaluacionProveedor.ICP" 
							cssClass="${ICPClass}"
							cssStyle="width:160px" 
							name="evaluacionProveedor.ICP" 
							title="ICP" 
							disabled="${ICPDisabled}"/>
					</div>		
				</td>
				<td class="textoCampo">
					<div id="tituloICS" style="${tituloICSStyle}">Indice de Calidad de Servicios [%]:</div>
				</td>
      			<td class="textoDato">
      				<div id="campoICS" style="${campoICSStyle}">
  							<@s.textfield 
  								templateDir="custontemplates" 
								id="evaluacionProveedor.ICS" 
								cssClass="${ICSClass}"
								cssStyle="width:160px" 
								name="evaluacionProveedor.ICS" 
								title="DP"
								disabled="${ICSDisabled}" />
					</div>		
				</td>
			</tr>
			<tr>
				<td class="textoCampo">
					<div id="tituloEvaluacion" style="${tituloEvaluacionStyle}">
						<div id="tituloFA" style="${tituloFAStyle}">Flexibilidad y Atenci&oacute;n:</div>
						<div id="tituloDP" style="${tituloDPStyle}">Desempe&ntilde;o:</div>
					</div>
				</td>
      			<td class="textoDato" colspan="3">
					<div id="campoEvaluacion" style="${campoEvaluacionStyle}">	      			
      					<div id="comboCalificacion" style="${comboCalificacionStyle}">	
      						<@s.select 
								templateDir="custontemplates" 
								id="evaluacionProveedor.calificacionEvaluacionProveedor.oid" 
								cssClass="${FAClass}"
								cssStyle="width:165px" 
								name="evaluacionProveedor.calificacionEvaluacionProveedor.oid" 
								list="calificacionEvaluacionProveedorList" 
								listKey="oid" 
								listValue="calificacion" 
								value="evaluacionProveedor.calificacionEvaluacionProveedor.oid"
								title="FA"
								headerKey="0"
								disabled="${FADisabled}"
								headerValue="Seleccionar"/>
						</div>
					</div>			
				</td>
			</tr>
			<tr><td colspan="4" class="textoCampo">&nbsp;</td></tr>
		</table>		
	</div>			
	
	<#if evaluacionProveedor.proveedor?exists && evaluacionProveedor.esCargaInicial.ordinal() == 1 >
				
				<@tiles.insertAttribute name="oc_osEvaluacion"/>
				<div id="capaSeparadora" class="capaSeparadora">&nbsp;</div>	
				<@tiles.insertAttribute name="evaluacionAnterior"/>
				<div id="capaSeparadora" class="capaSeparadora">&nbsp;</div>	
	</#if>
		
<@vc.htmlContent 
  baseUrl="${request.contextPath}/proveedor/evaluacionProveedor/seleccionarProveedorView.action" 
  source="seleccionarProveedor" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId=${navigationId},flowControl=change,evaluacionProveedor.fechaEvaluacion={evaluacionProveedor.fechaEvaluacionStr}, evaluacionProveedor.periodoEvaluacion.oid = {evaluacionProveedor.periodoEvaluacion.oid}, evaluacionProveedor.periodoEvaluacion.fechaInicio={evaluacionProveedor.periodoEvaluacion.fechaInicioStr}, evaluacionProveedor.periodoEvaluacion.fechaFin={evaluacionProveedor.periodoEvaluacion.fechaFinStr} , evaluacionProveedor.tipoEvaluacion={evaluacionProveedor.tipoEvaluacion}, evaluacionProveedor.esCargaInicial={evaluacionProveedor.esCargaInicial}"/>

<#--<@vc.htmlContent 
  baseUrl="${request.contextPath}/proveedor/evaluacionProveedor/seleccionarProveedorView.action" 
  source="seleccionarProveedorInicial" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId=${navigationId},flowControl=change,evaluacionProveedor.fechaEvaluacion={evaluacionProveedor.fechaEvaluacionStr}, evaluacionProveedor.tipoEvaluacion={evaluacionProveedor.tipoEvaluacion}, evaluacionProveedor.esCargaInicial={evaluacionProveedor.esCargaInicial}"/>-->

<@vc.htmlContent 
  baseUrl="${request.contextPath}/proveedor/evaluacionProveedor/createEvaluacionProveedorView.action"
  source="limpiarCargaInicial" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=evaluacionProveedor-create,flowControl=regis"/>



