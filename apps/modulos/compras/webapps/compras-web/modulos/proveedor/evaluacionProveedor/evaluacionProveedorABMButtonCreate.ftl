<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>


<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancel" type="button" name="btnCancelar" value="Cancelar" class="boton"/>
				<input id="store" type="button" name="btnAgregar" value="Agregar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/proveedor/evaluacionProveedor/createEvaluacionProveedor.action" 
  source="store" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="evaluacionProveedor.fechaEvaluacion={evaluacionProveedor.fechaEvaluacionStr},evaluacionProveedor.proveedor.nroProveedor={evaluacionProveedor.proveedor.nroProveedor}, evaluacionProveedor.proveedor.detalleDePersona.razonSocial={evaluacionProveedor.proveedor.detalleDePersona.razonSocial},evaluacionProveedor.proveedor.detalleDePersona.nombre={evaluacionProveedor.proveedor.detalleDePersona.nombre} ,evaluacionProveedor.tipoEvaluacion={evaluacionProveedor.tipoEvaluacion}, evaluacionProveedor.periodoEvaluacion.oid={evaluacionProveedor.periodoEvaluacion.oid}, evaluacionProveedor.periodoEvaluacion.fechaFin={evaluacionProveedor.periodoEvaluacion.fechaFinStr},evaluacionProveedor.periodoEvaluacion.fechaInicio={evaluacionProveedor.periodoEvaluacion.fechaInicioStr},evaluacionProveedor.ICP={evaluacionProveedor.ICP}, evaluacionProveedor.ICS={evaluacionProveedor.ICS}, evaluacionProveedor.calificacionEvaluacionProveedor.oid={evaluacionProveedor.calificacionEvaluacionProveedor.oid},evaluacionProveedor.esCargaInicial={evaluacionProveedor.esCargaInicial},formularioList={formularioList}"/>
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=buscar-evaluacionProveedor,flowControl=back"/>
  