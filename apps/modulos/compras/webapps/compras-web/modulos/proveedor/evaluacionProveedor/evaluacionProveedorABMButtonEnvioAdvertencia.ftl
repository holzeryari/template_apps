<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancel" type="button" name="btnVolver" value="Volver" class="boton"/>
				<input id="store" type="button" name="btnEnviar" value="Enviar Mail con Advertencia" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=buscar-evaluacionProveedor,flowControl=back"/>

  <@vc.htmlContent 
  baseUrl="${request.contextPath}/proveedor/advertenciaProveedor/enviarAdvertenciaProveedor.action" 
  source="store" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="advertenciaProveedor.oid=${evaluacionProveedor.advertencia.oid},navegacionIdBack=buscar-evaluacionProveedor"/>