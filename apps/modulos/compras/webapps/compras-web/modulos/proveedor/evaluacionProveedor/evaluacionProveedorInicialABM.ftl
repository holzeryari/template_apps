<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign tiles=JspTaglibs["/WEB-INF/tlds/tiles-jsp.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<@s.hidden id="evaluacionProveedor.oid" name="evaluacionProveedor.oid"/>
<@s.hidden id="evaluacionProveedor.versionNumber" name="evaluacionProveedor.versionNumber"/>
<@s.hidden id="evaluacionProveedor.periodoEvaluacion.oid" name="evaluacionProveedor.periodoEvaluacion.oid"/>
<@s.hidden id="evaluacionProveedor.periodoEvaluacion.fechaInicioStr" name="evaluacionProveedor.periodoEvaluacion.fechaInicioStr"/>

<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
			
			<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
			<tr>
				<td><b>Datos de la Evaluaci&oacute;n del Proveedor</b></td>
			</tr>
		</table>
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>
	

			<#assign ICPDisabled="true">
			<#assign ICSDisabled="true">
			<#assign ICPClass="textareagris">
			<#assign ICSClass="textareagris">
	
			<#if evaluacionProveedor.tipoEvaluacion?exists && evaluacionProveedor.tipoEvaluacion.ordinal() == 0>
				<#assign ICPDisabled="true">
				<#assign ICSDisabled="true">
				<#assign ICPClass="textareagris">
				<#assign ICSClass="textareagris">	
			</#if>
			<#if evaluacionProveedor.tipoEvaluacion?exists && evaluacionProveedor.tipoEvaluacion.ordinal() == 1>
				<#assign ICPDisabled="false">
				<#assign ICPClass="textarea">
			</#if>
			<#if evaluacionProveedor.tipoEvaluacion?exists && evaluacionProveedor.tipoEvaluacion.ordinal() == 2>
				<#assign ICSDisabled="false">
				<#assign ICSClass="textarea">
			</#if>
	
			<tr>
				<td class="textoCampo">N&uacute;mero:</td>
  				<td class="textoDato"><@s.property default="&nbsp;" escape=false value="evaluacionProveedor.numero"/></td>
  				<td  class="textoCampo">Fecha Evaluaci&oacute;n: </td>
				<td class="textoDato">
  					<@s.property default="&nbsp;" escape=false value="evaluacionProveedor.fechaEvaluacion"/>
					<@s.hidden id="evaluacionProveedor.fechaEvaluacion" name="evaluacionProveedor.fechaEvaluacion"/>
					<@s.hidden id="evaluacionProveedor.fechaEvaluacionStr" name="evaluacionProveedor.fechaEvaluacionStr"/>
				</td>
			</tr>
			<tr>
				<td class="textoCampo">Tipo Evaluaci&oacute;n:</td>
  				<td class="textoDato">
  				<@s.select 
						templateDir="custontemplates" 
						id="evaluacionProveedor.tipoEvaluacion" 
						cssClass="textarea"
						cssStyle="width:165px" 
						name="evaluacionProveedor.tipoEvaluacion" 
						list="tipoEvaluacionList" 
						listKey="key" 
						listValue="description" 
						value="evaluacionProveedor.tipoEvaluacion.ordinal()"
						title="Tipo"
						headerKey="0"
						headerValue="Todos"	
						onchange="javascript:tipoEvaluacion(this);"						
						/>
				</td>
				<td class="textoCampo">Proveedor:</td>
  				<td class="textoDato">
  				<@s.hidden id="nroProveedor" name="evaluacionProveedor.proveedor.nroProveedor"/>
				<@s.hidden id="razonSocial" name="evaluacionProveedor.proveedor.detalleDePersona.razonSocial"/>
				<@s.hidden id="nombre" name="evaluacionProveedor.proveedor.detalleDePersona.nombre"/>

  				<#assign proveedorBusquedaStyle="display:none;float:left;">
				<@s.if test="evaluacionProveedor.tipoEvaluacion.ordinal() == 1 || evaluacionProveedor.tipoEvaluacion.ordinal() == 2">																		
					<#assign proveedorBusquedaStyle="display:block;float:left;">
				</@s.if>

				<div id="proveedorTexto" style="float:left;">
  				<@s.property default="&nbsp;" escape=false value="evaluacionProveedor.proveedor.detalleDePersona.razonSocial" /><@s.property default="&nbsp;" escape=false value="evaluacionProveedor.proveedor.detalleDePersona.nombre" />							
				</div>

				<div id="proveedorBusqueda" style="${proveedorBusquedaStyle}">
				<@s.a templateDir="custontemplates" id="seleccionarProveedor" name="seleccionarProveedor" href="javascript://nop/"  cssClass="ocultarIcono">
						<img src="${request.contextPath}/common/images/buscar.gif"  border="0" align="absmiddle" hspace="3" >
				</@s.a>
				</div>	
					
				</td>
						
			</tr>
					
			<tr>
				<#-- periodo de evaluacion inicial -->
				<td class="textoCampo">Indice de Calidad de Productos/Bienes [%]:</td>
	      		<td class="textoDato">
  						<@s.textfield 
  							templateDir="custontemplates" 
							id="evaluacionProveedor.ICP" 
							cssClass="${ICPClass}"
							cssStyle="width:160px" 
							name="evaluacionProveedor.ICP" 
							title="ICP" 
							disabled="${ICPDisabled}"/>
				</td>
				<td class="textoCampo">Indice de Calidad de Servicios [%]:</td>
  				<td class="textoDato">
  						<@s.textfield 
  							templateDir="custontemplates" 
							id="evaluacionProveedor.ICS" 
							cssClass="${ICSClass}"
							cssStyle="width:160px" 
							name="evaluacionProveedor.ICS" 
							title="DP"
							disabled="${ICSDisabled}" />
				</td>
			</tr>
			<tr><td colspan="4" class="textoCampo">&nbsp;</td></tr>	
		</table>		
	</div>	
<@vc.htmlContent 
  baseUrl="${request.contextPath}/proveedor/evaluacionProveedor/seleccionarProveedorView.action" 
  source="seleccionarProveedor" 
  success="contentTrx" 
  failure="errorTrx" 
  parameters="navigationId=${navigationId},flowControl=change,evaluacionProveedor.fechaEvaluacion={evaluacionProveedor.fechaEvaluacionStr}, evaluacionProveedor.tipoEvaluacion={evaluacionProveedor.tipoEvaluacion}, evaluacionProveedor.icp={evaluacionProveedor.icp}, evaluacionProveedor.ics={evaluacionProveedor.ics}"/>



