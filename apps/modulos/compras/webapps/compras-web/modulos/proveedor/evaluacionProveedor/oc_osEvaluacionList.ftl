<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>

<@vc.anchors target="contentTrx" ajaxFlag="ajax">
	<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
		<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
			<tr>
				<td>OC/OS incluidas en el Per&iacute;odo de Evaluaci&oacute;n</td>	
			</tr>
		</table>
		
		<!-- Resultado Filtro -->						
		<@display.table class="tablaDetalleCuerpo" cellpadding="3" name="evaluacionProveedor.oc_osList" id="oc_os" defaultsort=2>
			<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-select"  class="botoneraAnchoCon4" title="Acciones">
				<div class="alineacion">
				<@s.checkbox label="${oc_os.oid}" fieldValue="${oc_os.oid?c}" name="formularioList" value="true" />
				</div>
				<div class="alineacion">
				<@security.a 
					templateDir="custontemplates" 
					securityCode="CUF0383" 
					enabled="oc_os.readable" 
					cssClass="item"
					id="read"
					name="read" 
					href="${request.contextPath}/compraContratacion/oc_os/readOC_OSView.action?oc_os.oid=${oc_os.oid?c}&navigationId=oc_os-visualizar&flowControl=regis&navegacionIdBack=${navigationId}">
					<img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver"  border="0">
				</@security.a>
				</div>
				
				<div class="alineacion">
				<#-- estado "En Preparacion"-->
				<#if oc_os.estado.ordinal() == 1>
					<@security.a  
						templateDir="custontemplates" 
						securityCode="CUF0385" 
						enabled="oc_os.eraseable"
						cssClass="item" 
						id="delete"
						name="delete"  
						href="${request.contextPath}/compraContratacion/oc_os/deleteOC_OSView.action?oc_os.oid=${oc_os.oid?c}&navigationId=oc_os-delete&flowControl=regis&navegacionIdBack=${navigationId}">
						<img  src="${request.contextPath}/common/images/eliminar.gif" alt="Eliminar" title="Eliminar"  border="0">
					</@security.a>
				<#--estado "Pendiente autorizacion"-->
				<#elseif oc_os.estado.ordinal() == 2>
					<@security.a 
						templateDir="custontemplates" 
						securityCode="CUF0389" 
						enabled="oc_os.updatable"
						cssClass="item"
						id="autorizar"
						name="autorizar"   
						href="${request.contextPath}/compraContratacion/oc_os/autorizarOC_OSView.action?oc_os.oid=${oc_os.oid?c}&navigationId=autorizar-oc_os&flowControl=regis&navegacionIdBack=${navigationId}">
						<img  src="${request.contextPath}/common/images/administrar.gif" alt="Autorizar/Rechazar" title="Autorizar/Rechazar"  border="0">
					</@security.a>	
				<#--estado "Autorizado"-->
				<#elseif oc_os.estado.ordinal() == 3>
					<@security.a  
						templateDir="custontemplates" 
						securityCode="CUF0385" 
						enabled="oc_os.eraseable"
						cssClass="item"
						id="anular"
						name="anular"   
						href="${request.contextPath}/compraContratacion/oc_os/anularOC_OSView.action?oc_os.oid=${oc_os.oid?c}&navigationId=oc_os-anular&flowControl=regis&navegacionIdBack=${navigationId}">
						<img  src="${request.contextPath}/common/images/eliminar.gif" alt="Anular" title="Anular"  border="0">
					</@security.a>
				<#--estado "Enviada/Impresa"-->
				<#elseif oc_os.estado.ordinal() == 6>
					<@security.a  
						templateDir="custontemplates" 
						securityCode="CUF0385" 
						enabled="oc_os.eraseable"
						cssClass="item"
						id="anular"
						name="anular"   
						href="${request.contextPath}/compraContratacion/oc_os/anularOC_OSView.action?oc_os.oid=${oc_os.oid?c}&navigationId=oc_os-anular&flowControl=regis&navegacionIdBack=${navigationId}">
						<img  src="${request.contextPath}/common/images/eliminar.gif" alt="Anular" title="Anular"  border="0">
					</@security.a>
				<#--estado "En Curso" y prestadaTotal = No-->
				<#elseif oc_os.estado.ordinal() == 4 && oc_os.osPrestadaTotal.ordinal() == 1>
					<@security.a 
						templateDir="custontemplates" 
						securityCode="CUF0386" 
						enabled="oc_os.enable"
						cssClass="item"
						id="intervenir"
						name="intervenir"   
						href="${request.contextPath}/compraContratacion/oc_os/intervenirOCOSPorRecepcionView.action?oc_os.oid=${oc_os.oid?c}&navigationId=oc_os-intervenir&flowControl=regis&navegacionIdBack=${navigationId}">
						<img  src="${request.contextPath}/common/images/intervenir.gif" alt="Intervenir" title="Intervenir"  border="0">
					</@security.a>	
				</#if>
				</div>
				
				<#--estado "Enviada/Impresa"-->
				
				<div class="alineacion">
				<#if oc_os.estado.ordinal() == 6>
				
					<@security.a 
						templateDir="custontemplates" 
						securityCode="CUF0386" 
						enabled="oc_os.enable"
						cssClass="item"
						id="intervenir"
						name="intervenir"   
						href="${request.contextPath}/compraContratacion/oc_os/intervenirOCOSPorRecepcionView.action?oc_os.oid=${oc_os.oid?c}&navigationId=oc_os-intervenir&flowControl=regis&navegacionIdBack=${navigationId}">
						<img  src="${request.contextPath}/common/images/intervenir.gif" alt="Intervenir" title="Intervenir"  border="0">
					</@security.a>	
				
				</#if>
				</div>
				
				<#--estado "Intervenida"-->
				
				<div class="alineacion">
				<#if oc_os.estado.ordinal() == 7 && oc_os.tipoIntervencionRecepcion?exists && oc_os.tipoIntervencionRecepcion.ordinal() == 1>
				
					<@security.a 
						templateDir="custontemplates" 
						securityCode="CUF0386" 
						enabled="oc_os.enable"
						cssClass="item"
						id="intervenir"
						name="intervenir"   
						href="${request.contextPath}/compraContratacion/oc_os/intervenirOCOSPorRecepcionView.action?oc_os.oid=${oc_os.oid?c}&navigationId=oc_os-intervenir&flowControl=regis&navegacionIdBack=${navigationId}">
						<img  src="${request.contextPath}/common/images/intervenir.gif" alt="Intervenir" title="Intervenir"  border="0">
					</@security.a>	
				
				</#if>
				</div>
				
				<#-- si la orden es de servicio, no est� evaluada y  tiene estado finalizada o intervenida -->
				<#if oc_os.tipo.ordinal() == 2 && !(oc_os.evaluacionOS?exists) && (oc_os.estado.ordinal() == 5 || oc_os.estado.ordinal() == 7 )>
				<div class="alineacion">
				<@security.a 
					templateDir="custontemplates" 
					securityCode="CUF0601" 
					enabled="true" 
					cssClass="item"
					id="evaluar"
					name="evaluar" 
					href="${request.contextPath}/servicio/evaluacionOS/createEvaluacionOSView.action?evaluacionOS.oc_os.oid=${oc_os.oid?c}&navigationId=evaluacionOS-agregar&flowControl=regis&navegacionIdBack=${navigationId}">
					<img  src="${request.contextPath}/common/images/advertencia.gif" alt="Evaluar OS" title="Evaluar OS" border="0">
				</@security.a>
				</div>
				</#if>

			</@display.column>
			
			
		
			
			<#-- orden es de tipo=Compra -->
			<#if oc_os.tipo.ordinal()== 1>
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="numero" title="N&uacute;mero" />
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="fecha" format="{0,date,dd/MM/yyyy}"  title="Fecha"/>
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="tipo" title="Tipo" />
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="estado" title="Estado" />
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="osPrestadaTotal" title="Recibida Total" />
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="tipoIntervencionRecepcion" title="Recepci&oacute;n Intervenida" />
		 		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="importe" title="Importe" />
		 		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="demorada" title="Demorada" />
		 		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="conformidad" title="Tiene No Conformidad" />
		 	</#if>
		 	
		 	<#-- orden es de tipo=Servicio -->										
				<#if oc_os.tipo.ordinal()== 2>
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="numero" title="N&uacute;mero" />
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="fecha" format="{0,date,dd/MM/yyyy}"  title="Fecha"/>
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="tipo" title="Tipo" />
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="estado" title="Estado" />
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="osPrestadaTotal" title="Rec. Total" />
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="tipoIntervencionRecepcion" title="Intervenida" />
		 		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="importe" title="Importe" />
		 		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="demorada" title="Demorada" />
		 		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="conformidad" title="No Conformidad" />
		 		<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloTexto" property="evaluacionOS.calificacionCC" title="CC"/>
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="evaluacionOS.calificacionPCa" title="PCa"/>
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="evaluacionOS.calificacionPE" title="PE"/>
				<@display.column decorator="ar.com.riouruguay.web.common.actions.decorators.CommonColumnDecorator" headerClass="tbl-contract-service-service" class="estiloNumero" property="evaluacionOS.FA" property="evaluacionOS.calificacionFA" title="FM"/>
					
		 	</#if>
				
				
		</@display.table>
	</div>	
</@vc.anchors>
