<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign tiles=JspTaglibs["/WEB-INF/tlds/tiles-jsp.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<@s.hidden id="periodoEvaluacion.oid" name="periodoEvaluacion.oid"/>
<@s.hidden id="oc_os.versionNumber" name="oc_os.versionNumber"/>

<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
			<#if mensajeAviso?exists && (mensajeAviso.length()>0)>
				<tr>
					<td class="estiloMensajeAviso"><@s.text name="${mensajeAviso}"/></td>
				</tr>
			</#if>
			<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos del Per&iacute;odo de Evaluaci&oacute;n</b></td>
			</tr>
		</table>
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>
			<tr>
  				<td class="textoCampo">N&uacute;mero:</td>
  				<td class="textoDato"><@s.property default="&nbsp;" escape=false value="periodoEvaluacion.numero"/></td>
  				<td  class="textoCampo">Situaci&oacute;n: </td>
				<td  class="textoDato">
					<@s.property default="&nbsp;" escape=false value="periodoEvaluacion.situacion"/>
				</td>
				<@s.hidden id="periodoEvaluacion.situacion" name="periodoEvaluacion.situacion.ordinal()"/>					
			</tr>	
			<tr>
				<td class="textoCampo">Estado:</td>
  				<td  class="textoDato" colspan="3">
					<@s.property default="&nbsp;" escape=false value="periodoEvaluacion.estado"/>
				</td>
				<@s.hidden id="periodoEvaluacion.estado" name="periodoEvaluacion.estado.ordinal()"/>
			</tr>
			<tr>
				<@s.hidden id="periodoEvaluacion.fechaInicio" name="periodoEvaluacion.fechaInicio" />
				<@s.hidden id="periodoEvaluacion.fechaInicioF" name="periodoEvaluacion.fechaInicioStr"/>
				 <#if periodoEvaluacion.fechaInicio?exists>
					<td  class="textoCampo">Fecha Inicio Per&iacute;odo: </td>
					<td class="textoDato">
  						<@vc.rowCalendar 
						templateDir="custontemplates" 
						id="periodoEvaluacion.fechaInicio" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="periodoEvaluacion.fechaInicio" 
						title="Fecha Inicio" />
					</td>
					<td  class="textoCampo">Fecha Fin Per&iacute;odo: </td>
					<td class="textoDato">
  						<@vc.rowCalendar 
						templateDir="custontemplates" 
						id="periodoEvaluacion.fechaFin" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="periodoEvaluacion.fechaFin" 
						title="Fecha Fin" />
					</td>
				<#else>
					<td  class="textoCampo">Fecha Fin Per&iacute;odo: </td>
					<td class="textoDato">
  						<@vc.rowCalendar 
						templateDir="custontemplates" 
						id="periodoEvaluacion.fechaFin" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="periodoEvaluacion.fechaFin" 
						title="Fecha Fin" />
					</td>				
					<td class="textoCampo">&nbsp;</td>
					<td class="textoDato">&nbsp;</td>
				</#if>
			</tr>
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>
		</table>		
	</div>	
	<#if periodoEvaluacion.fechaInicio?exists>
			<div id="capaSeparadora" class="capaSeparadora">&nbsp;</div>		
			<@tiles.insertAttribute name="proveedorPendienteEvaluacion"/>
	</#if>			
 




