<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
	<#if periodoEvaluacion.estado == "Activo">
		<#assign titulo="Desactivar">
	<#else>
		<#assign titulo="Activar">
	</#if>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancel" type="button" name="btnCancelar" value="Cancelar" class="boton"/>
				<input id="update" type="button" name="btnActivarDesactivar" value="${titulo}" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/proveedor/periodoEvaluacion/cambiarEstado.action" 
  source="update" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="periodoEvaluacion.oid={periodoEvaluacion.oid}"/>
  
  <@vc.htmlContent 
baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=buscar-periodoEvaluacion,flowControl=back"/>
  
  