<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancel" type="button" name="btnCancelar" value="Cancelar" class="boton"/>
				<input id="store" type="button" name="btnAgregar" value="Agregar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>


<@vc.htmlContent 
  baseUrl="${request.contextPath}/servicio/evaluacionOS/createEvaluacionOS.action" 
  source="store" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="evaluacionOS.fechaEvaluacion={evaluacionOS.fechaEvaluacionStr}, evaluacionOS.calEvaProCC.oid={evaluacionOS.calEvaProCC.oid},evaluacionOS.calEvaProPCa.oid={evaluacionOS.calEvaProPCa.oid},evaluacionOS.calEvaProPE.oid={evaluacionOS.calEvaProPE.oid},evaluacionOS.calEvaProFA.oid={evaluacionOS.calEvaProFA.oid}, evaluacionOS.oc_os.oid={evaluacionOS.oc_os.oid}, navegacionIdBack=${navegacionIdBack}"/>
  
<@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancel" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=${navegacionIdBack},flowControl=back"/>
  