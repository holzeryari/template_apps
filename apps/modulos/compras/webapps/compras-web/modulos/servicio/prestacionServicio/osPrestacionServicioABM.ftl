<#setting locale="es_AR">
<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign tiles=JspTaglibs["/WEB-INF/tlds/tiles-jsp.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<@s.hidden id="navigationId" name="navigationId"/>
<@s.hidden id="prestacionServicio.oid" name="prestacionServicio.oid"/>
<@s.hidden id="oc.oid" name="oc.oid"/>
<@s.hidden id="prestacionServicio.versionNumber" name="prestacionServicio.versionNumber"/>

<div id="capaTituloCuerpo" class="capaTituloCuerpo">
   		<table id="tablaTituloMenu" class="tablaTituloMenu">
			<tr>
				<td><@s.text name="${navegacion}"/></td>
			</tr>
		</table>
	</div>
	<div id="capaTituloAccion" class="capaTituloAccion">
		<table id="tablaTituloAccion" class="tablaTituloAccion">
			<tr>
            	<td><div id="errorTrx" align="left"></div></td>
           	</tr>
        	<tr>
				<td><img src="${request.contextPath}/common/images/flecha.gif" border="0" align="absmiddle" hspace="3" ><@s.text name="${titulo}" /></td>
			</tr>
		</table>
	</div>	
	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
		<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
            <tr>
				<td><b>Datos de la Recepci&oacute;n de Producto/Bien</b></td>
			</tr>
		</table>
		
		<@tiles.insertAttribute name="prestacion"/>
		
	</div>	
	<div id="capaCabeceraCuerpo" class="capaCabeceraCuerpo">
	<table id="tablaTituloCuerpo" class="tablaTituloCuerpo">
        <tr>
			<td><b>Datos de la Orden de Compra</b></td>
		</tr>
	</table>
		
		<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
			<tr>
        		<td class="textoCampo" colspan="4">&nbsp;</td>
        	</tr>
			<tr>
      			<td class="textoCampo">N&uacute;mero:</td>
      			<td class="textoDato">
      			<@s.property default="&nbsp;" escape=false value="oc.numero"/></td>
      			<td  class="textoCampo">Fecha: </td>
				<td class="textoDato">
				<@s.if test="oc.fecha != null">
				<#assign fecha = oc.fecha> 
				${fecha?string("dd/MM/yyyy")} 	
				</@s.if>
				<@s.else>
					&nbsp;
				</@s.else>									
				</td>	      			
			</tr>	

			<tr>
			
				<td class="textoCampo">Tipo:</td>
      			<td class="textoDato">
      			<@s.property default="&nbsp;" escape=false value="oc.tipo"/>
      			
				</td>
				<td class="textoCampo">Proveedor:</td>
      			<td class="textoDato">	      			
      				<@s.property default="&nbsp;" escape=false value="oc.proveedor.detalleDePersona.razonSocial" />
      				<@s.property default="&nbsp;" escape=false value="oc.proveedor.detalleDePersona.nombre" />							

				</td>
    		</tr>
	    		
	    		<#--<tr>
					<td class="texto_datos"  align="right">Fecha de Entrega/Prestaci&oacute;n:</td>
	      			<td class="texto" align="left" colspan="3">
						<@vc.rowCalendar 
						templateDir="custontemplates" 
						id="oc_os.fechaEntregaPrestacion" 
						cssClass="textarea"
						cssStyle="width:160px"						
						name="oc_os.fechaEntregaPrestacion" 
						title="Fecha Entrega" />
					</td>	
				</tr>-->
				

    		<tr>
				<td class="textoCampo">Lugar de Entrega/Prestaci&oacute;n:</td>
      			<td  class="textoDato" colspan="3">
      				<@s.property default="&nbsp;" escape=false value="oc.lugarEntregaPrestacion" />	
								&nbsp;			 
				</td>					
    		</tr>
	    		
    		<tr>
				<td class="textoCampo">Observaciones:</td>
      			<td  class="textoDato" colspan="3">
				<@s.property default="&nbsp;" escape=false value="oc.observaciones" />			 
				&nbsp;
				</td>					
    		</tr>
	    		
    		<tr>
				<td class="textoCampo">Estado:</td>
      			<td  class="textoDato">
					<@s.property default="&nbsp;" escape=false value="oc.estado"/>
				</td>
				
				<#-- estado de la oc_os = "Intervenida"-->
    			<#if oc.estado.ordinal() == 7>
					<tr>
						<td class="textoCampo">Observaciones Intervenci&oacute;n:</td>
						<td  colspan="3" class="textoDato">
							<@s.property default="&nbsp;" escape=false value="oc.observacionTipoInt"/>
						</td>
					</tr>		
				<#else>
					<td class="textoCampo">&nbsp;</td>
      				<td class="textoDato">&nbsp;</td>
      			</#if>
    		</tr>
		</table>		
	</div>				
	