<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign display=JspTaglibs["/WEB-INF/tlds/displaytag.tld"]>
<#assign ajax=JspTaglibs["/WEB-INF/tlds/ajaxtags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
<#assign security=JspTaglibs["/WEB-INF/tlds/security-tags.tld"]>
<@s.hidden id="entregaSeleccion" name="entregaSeleccion"/>

<@vc.anchors target="contentTrx" ajaxFlag="ajax">
<div id="capaDetalleCuerpo" class="capaDetalleCuerpo">
	<table id="tablaTituloDetalleCuerpo" class="tablaTituloDetalleCuerpo">
		<tr>
			<td>Ordenes de Servicio</td>
			<td>	
				<div align="right">
					<@s.a href="${request.contextPath}/servicio/prestacionServicio/selectOS.action?prestacionServicio.oid=${prestacionServicio.oid?c}&prestacionServicio.proveedor.nroProveedor=${prestacionServicio.proveedor.nroProveedor?c}&prestacionServicio.proveedor.detalleDePersona.nombre=${prestacionServicio.proveedor.detalleDePersona.nombre}&prestacionServicio.proveedor.detalleDePersona.razonSocial=${prestacionServicio.proveedor.detalleDePersona.razonSocial}&navigationId=${navigationId}" templateDir="custontemplates" id="agregarOS" name="agregarOS" cssClass="ocultarIcono">
					<b>Agregar</b><img src="${request.contextPath}/common/images/agregar.gif" width="16" height="16" border="0" align="absmiddle" hspace="3" >
					</@s.a>		
				</div>
			</td>
		</tr>
	</table>			

	<table class="tablaDetalleCuerpo" cellpadding="3">
		<tr>
			<th align="center">Acciones</th>
			<th align="center">Numero</th>
			<th align="center" >Fecha</th>
			<th align="center">Importe</th>
			<th align="center">Estado</th>							
		</tr>
			<#assign mostro='true'>
			<#assign prestacionServicioOSEPlista = prestacionServicio.prestacionServicioOSEPList>
			<#list prestacionServicioOSEPlista as prestacionServicioOSEP>
				<#if mostro=='false'>									
					<tr>
						<th align="center" >Acciones</th>
						<th align="center" >Numero</th>
						<th align="center" >Fecha</th>
						<th align="center">Importe</th>
						<th align="center">Estado</th>							
					</tr>
				</#if>								
				<tr>
					<td class="botoneraAnchoCon2"> 
						<div align="center">
							<div class="alineacion">
								<@security.a 
									templateDir="custontemplates" 
									securityCode="CUF0383" 
									enabled="oc_os.readable" 
									cssClass="item" 
									id="verOCOS"
									name="verOCOS"
									href="${request.contextPath}/compraContratacion/oc_os/readOC_OSView.action?oc_os.oid=${prestacionServicioOSEP.ocos.oid?c}&navigationId=oc_os-visualizar&flowControl=regis&navegacionIdBack=${navigationId}">
									<img  src="${request.contextPath}/common/images/ver.gif" alt="Ver" title="Ver"  border="0">
								</@security.a>	
							</div>
							<div class="alineacion">							
								<a href="${request.contextPath}/servicio/prestacionServicio/deleteOSView.action?prestacionServicio.oid=${prestacionServicio.oid?c}&oc.oid=${prestacionServicioOSEP.ocos.oid?c}&navigationId=osPrestacion-eliminar&flowControl=regis&navegacionIdBack=${navigationId}">
								<img  src="${request.contextPath}/common/images/eliminar.gif" alt="Eliminar" title="Eliminar" border="0"></a>
							</div>	
						</div>
					</td>	
					<td class="estiloNumero"><#if prestacionServicioOSEP.ocos.numero?exists>${prestacionServicioOSEP.ocos.numero}<#else>&nbsp;</#if></td>
					<td class="estiloNumero"><#if prestacionServicioOSEP.ocos.fecha?exists>${prestacionServicioOSEP.ocos.fecha?string("dd/MM/yyyy")}<#else>&nbsp;</#if></td>
					<td class="estiloNumero"><#if prestacionServicioOSEP.ocos.importe?exists>${prestacionServicioOSEP.ocos.importe}<#else>&nbsp;</#if></td>
					<td class="estiloTexto"><#if prestacionServicioOSEP.ocos.estado?exists>${prestacionServicioOSEP.ocos.estado}<#else>&nbsp;</#if></td>														   
				</tr>

			
				<tr>
					<td>&nbsp;</td>
					<th class="botoneraAnchoCon1">								
						<b>Entregas Parciales</b>									
					</th>
					<th align="center">Fecha</th>
					<th colspan="2" align="center">Observaciones</th>							
				</tr>
						
			<#assign entregaParcialLista =prestacionServicioOSEP.ocos.oc_osEntregaParcialList>
			<#list entregaParcialLista as entregaParcial>
						
			
					<#if !prestacionServicio.fechaPrestacion.after(entregaParcial.fecha)>						
					 	<#assign fechaValida='true'>
					 <#elseif entregaParcial.estaSeleccionada>
					 	<#assign fechaValida='false'>
					 <#else> 
					 	<#assign fechaValida='true'>
					 </#if>
				
					<#assign claseEntregaParcial="celdaHabilitada">							
					<#if fechaValida=='false'>
						<#assign claseEntregaParcial="celdaHabilitada">	
					</#if>		
					<tr>						
						<td>&nbsp;</td>
						<td class="${claseEntregaParcial}" align="center" width="60px">
							<#if fechaValida=='true'>
								<@s.checkbox label="${entregaParcial.oid}" fieldValue="${entregaParcial.oid}" name="checkEntregaSeleccion"/>
							<#else>
								&nbsp;	
							</#if>							
						</td>
						<td class="${claseEntregaParcial}" align="right">${entregaParcial.fecha?string("dd/MM/yyyy")}</td>
						<td colspan="2" class="${claseEntregaParcial}" align="left">
							<#if entregaParcial.observaciones?exists>${entregaParcial.observaciones}</#if>&nbsp;</td>
					</tr>	

				</#list>
				<tr><td  style="height:3px;" colspan="5"></td></tr>
						<#assign mostro='false'>
			
			
		</#list>						
	</table>
	</div>	
</@vc.anchors>

