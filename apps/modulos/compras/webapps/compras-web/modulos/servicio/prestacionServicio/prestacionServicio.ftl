<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>
	<table id="tablaDatosCuerpo" class="tablaDatosCuerpo">
		<tr>
			<td class="textoCampo" colspan="4">&nbsp;</td>
		</tr>
		<tr>
			<td class="textoCampo">N&uacute;mero:</td>
  			<td class="textoDato">
  			<@s.property default="&nbsp;" escape=false value="prestacionServicio.numero"/></td>
  			<td  class="textoCampo">Fecha: </td>
			<td class="textoDato">
			<@s.if test="prestacionServicio.fechaComprobante != null">
				<#assign fechaPrestacion = prestacionServicio.fechaPrestacion> 
				${fechaPrestacion?string("dd/MM/yyyy")}	
			</@s.if>				&nbsp;					
			</td>	      			
		</tr>	

		<tr>
	  		<td class="textoCampo">Proveedor:</td>
			<td class="textoDato">
				<@s.property default="&nbsp;" escape=false value="prestacionServicio.proveedor.detalleDePersona.razonSocial" />
				<@s.property default="&nbsp;" escape=false value="prestacionServicio.proveedor.detalleDePersona.nombre" />
				<@s.a templateDir="custontemplates" id="seleccionarProveedor" name="seleccionarProveedor" href="javascript://nop/" cssClass="ocultarIcono">
					<img src="${request.contextPath}/common/images/buscar.gif"  border="0" align="absmiddle" hspace="3" >
				</@s.a>	
			</td>	
	
			<td class="textoCampo">Fecha Prestaci&oacute;n:</td>
		  	<td class="textoDato">
				<@vc.rowCalendar 
				templateDir="custontemplates" 
				id="prestacionServicio.fechaPrestacion" 
				cssClass="textarea"
				cssStyle="width:160px" 
				name="prestacionServicio.fechaPrestacion" 
				title="Fecha Prestacion" />
			</td>
		</tr>
		<tr>
			<td class="textoCampo">Nro. Factura:</td>
		  	<td class="textoDato">
	  			<@s.textfield 
	  					templateDir="custontemplates" 
						id="prestacionServicio.numeroFactura" 
						cssClass="textarea"
						cssStyle="width:160px" 
						name="prestacionServicio.numeroFactura" 
						title="N&uacute;mero Factura" />
			</td>	
			<td class="textoCampo">Nro. Remito:</td>
  			<td class="textoDato">
  			<@s.textfield 
  					templateDir="custontemplates" 
					id="prestacionServicio.numeroRemito" 
					cssClass="textarea"
					cssStyle="width:160px" 
					name="prestacionServicio.numeroRemito" 
					title="N&uacute;mero Remito" />
			</td>
		</tr>		

		<tr>
			<td class="textoCampo">Observaciones:</td>
			<td  class="textoDato" colspan="3">
				<@s.textarea	
						templateDir="custontemplates" 						  
						cols="89" rows="4"	      					
						cssClass="textarea"
						id="prestacionServicio.observaciones"							 
						name="prestacionServicio.observaciones"  
						label="Observaciones"														
						 />
			</td>					
		</tr>

		<tr>
			<td class="textoCampo">Estado:</td>
		  	<td  class="textoDato" colspan="3">
				<@s.property default="&nbsp;" escape=false value="prestacionServicio.estado"/></td>
			</td>
		</tr>	    	
		<tr><td colspan="4" class="textoCampo">&nbsp;</td></tr>
</table>		

