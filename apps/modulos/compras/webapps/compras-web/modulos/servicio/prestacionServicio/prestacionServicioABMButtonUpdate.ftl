<#assign s=JspTaglibs["/WEB-INF/tlds/struts-tags.tld"]>
<#assign vc=JspTaglibs["/WEB-INF/tlds/view-controls.tld"]>

<div id="capaBotonera" class="capaBotonera">
	<table id="tablaBotonera" class="tablaBotonera">
		<tr> 
			<td align="right">
				<input id="cancelar" type="button" name="btnCancelar" value="Cancelar" class="boton"/>
				<input id="update" type="button" name="btnModificar" value="Modificar" class="boton"/>
			</td>
		</tr>	
	</table>
</div>

<@vc.htmlContent 
  baseUrl="${request.contextPath}/servicio/prestacionServicio/update.action" 
  source="update" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="prestacionServicio.versionNumber={prestacionServicio.versionNumber},prestacionServicio.oid={prestacionServicio.oid},prestacionServicio.cliente.oid={prestacionServicio.cliente.oid},prestacionServicio.cliente.descripcion={prestacionServicio.cliente.descripcion},prestacionServicio.servicio.oid={prestacionServicio.servicio.oid},prestacionServicio.proveedor.nroProveedor={prestacionServicio.proveedor.nroProveedor},prestacionServicio.proveedor.razonSocial={prestacionServicio.proveedor.razonSocial},prestacionServicio.proveedor.nombre={prestacionServicio.proveedor.nombre},prestacionServicio.numeroFactura={prestacionServicio.numeroFactura},prestacionServicio.numeroRemito={prestacionServicio.numeroRemito},prestacionServicio.observaciones={prestacionServicio.observaciones},prestacionServicio.fechaPrestacion={prestacionServicio.fechaPrestacion}"/>
  

<@vc.htmlContent 
  baseUrl="${request.contextPath}/compras/flowControl.action" 
  source="cancelar" 
  success="contentTrx" 
  failure="errorTrx"  
  parameters="navigationId=prestacionServicio-administrar,flowControl=back"/>
  


    