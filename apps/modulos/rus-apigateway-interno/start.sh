# properties file
source /home/tomcat/conf/conf.properties

export CATALINA_HOME=/usr/local/tomcat7

nohup java -XX:MaxRAM=500m -XX:+UseSerialGC -Xss512k -Xms64m -Xmx1584m -XX:MaxPermSize=1814m -XX:MaxMetaspaceSize=1200m -jar /home/tomcat/rus-apigateway-interno/rus-apigateway-interno.jar -Dsun.misc.URLClassPath.disableJarChecking=true 1>/home/tomcat/rus-apigateway-interno/logs/rus-apigateway-interno.log 2>&1 &
echo rus-apigateway-interno started
