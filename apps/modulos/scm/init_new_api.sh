#!/bin/bash

function show_help {
    echo "Script para cambiar las referencias correspondientes a las apis. Se debe indicar como parametros Nombre_API y versión de java (ej: api-erp java11)."
}

# Si no se ingresaron parametros se muestra la ayuda
if [ "$1" = "" ] || [ "$2" = "" ]
then
  show_help
  exit
fi


# Muestra el texto de ayuda y finalizar
if [ "$1" == -h ]; then
  show_help;
  exit
fi
if [ "$2" == -h ]; then
  show_help;
  exit
fi

API_NAME=$1
JAVA_VERSION=$2
API_HOME=/home/tomcat
MODULES_HOME=$API_HOME/modulos
S=$
print='{print $1}'

## Crea API y brinda permisos a directorios y subdirectorios

  mkdir -pm 755 $MODULES_HOME/$API_NAME/{logs,temp,work}

  ln -s $MODULES_HOME/$API_NAME /home/tomcat/$API_NAME

  echo "# properties file" >> $MODULES_HOME/$API_NAME/start.sh
  echo "source /home/tomcat/conf/conf.properties" >> $MODULES_HOME/$API_NAME/start.sh
  echo "nohup /$JAVA_VERSION/bin/java -XX:MaxRAM=500m -XX:+UseSerialGC -Xss512k -Xms64m -Xmx1584m -XX:MaxPermSize=1814m -XX:MaxMetaspaceSize=1200m -jar $MODULES_HOME/$API_NAME/$API_NAME.jar -Dsun.misc.URLClassPath.disableJarChecking=true 1>$MODULES_HOME/$API_NAME/logs/$API_NAME.log 2>&1 &" >> $MODULES_HOME/$API_NAME/start.sh
  echo "echo $API_NAME started">> $MODULES_HOME/$API_NAME/start.sh

  chown -R tomcat:tomcat /home/tomcat/$API_NAME
  chown -R tomcat:tomcat $MODULES_HOME
  chmod 754 /home/tomcat/$API_NAME/start.sh
  chmod 755 /home/tomcat/$API_NAME

## Agregar inicio de API en el archivo start_all.sh
echo -e "#$S{ROOT_PATH}/scm/start_tomcat.sh $API_NAME" >> $MODULES_HOME/scm/start_all.sh

## Agregar detencion de API el archivo stop_all.sh
echo "kill $S(ps aux | grep '$API_NAME' | grep -v grep | awk $print)" >> $MODULES_HOME/scm/stop_all.sh
