#!/bin/bash

function show_help {
    echo "Script para cambiar las referencias correspondientes al modulo y nombre del microservicio. Se debe indicar como parametros Nombre_Modulo Nombre_MS (ej: cobranzas recibosCobro)."
}

# Si no se ingresaron parametros se muestra la ayuda
if [ "$1" = "" ] || [ "$2" = "" ]
then
  show_help
  exit
fi


# Muestra el texto de ayuda y finalizar
if [ "$1" == -h ]; then
  show_help;
  exit
fi
if [ "$2" == -h ]; then
  show_help;
  exit
fi

MS_MODULO=$1
MS_NAME=$2
MS_HOME=/home/tomcat/microservices
TOMCAT_HOME=/home/tomcat
MODULES_HOME=$TOMCAT_HOME/modulos
S=$
print='{print $2}'

## Crea Microservicio y brinda permisos a directorios y subdirectorios

  mkdir -pm 755 $MS_HOME/$MS_MODULO/$MS_NAME/{logs,temp,work}

  ln -s $MS_HOME/$MS_MODULO/$MS_NAME /home/tomcat/ms-$MS_MODULO-$MS_NAME

  echo "# properties file" >> $MS_HOME/$MS_MODULO/$MS_NAME/start.sh
  echo "source /home/tomcat/conf/conf.properties" >> $MS_HOME/$MS_MODULO/$MS_NAME/start.sh
  echo "export MS_HOME=$MS_HOME/$MS_MODULO/$MS_NAME" >> $MS_HOME/$MS_MODULO/$MS_NAME/start.sh
  echo "nohup /java/bin/java -XX:MaxRAM=500m -XX:+UseSerialGC -Xss512k -Xms64m -Xmx1584m -XX:MaxPermSize=1814m -XX:MaxMetaspaceSize=1200m -jar $MS_HOME/$MS_MODULO/$MS_NAME/ms-$MS_MODULO-$MS_NAME.jar -Dsun.misc.URLClassPath.disableJarChecking=true 1>$MS_HOME/$MS_MODULO/$MS_NAME/logs/ms_$MS_MODULO_$MS_NAME.log 2>&1 &" >> $MS_HOME/$MS_MODULO/$MS_NAME/start.sh
  echo "echo ms-$MS_MODULO-$MS_NAME started">> $MS_HOME/$MS_MODULO/$MS_NAME/start.sh

  chown -R tomcat:tomcat /home/tomcat/ms-$MS_MODULO-$MS_NAME
  chown -R tomcat:tomcat $MS_HOME
  chmod 754 /home/tomcat/ms-$MS_MODULO-$MS_NAME/start.sh
  chmod 755 /home/tomcat/ms-$MS_MODULO-$MS_NAME

## Agregar inicio de MS en el archivo start_all.sh
echo -e "#$S{ROOT_PATH_MS}/$MS_MODULO/$MS_NAME/start.sh" >> $MODULES_HOME/scm/start_all.sh

## Agregar detencion de MS el archivo stop_all.sh
echo "kill $S(ps aux | grep 'ms-$MS_MODULO-$MS_NAME' | grep -v grep | awk $print)" >> $MODULES_HOME/scm/stop_all.sh
