echo "Stop All services"

ROOT_PATH=/home/tomcat/modulos

${ROOT_PATH}/scm/stop_tomcat.sh cas
${ROOT_PATH}/scm/stop_tomcat.sh contabilidad
${ROOT_PATH}/scm/stop_tomcat.sh compras
${ROOT_PATH}/scm/stop_tomcat.sh datoscomunes
${ROOT_PATH}/scm/stop_tomcat.sh rrhh
${ROOT_PATH}/scm/stop_tomcat.sh tesoreria
${ROOT_PATH}/scm/stop_tomcat.sh tesoreria-ws
${ROOT_PATH}/scm/stop_tomcat.sh emision
${ROOT_PATH}/scm/stop_tomcat.sh emision-ws
${ROOT_PATH}/scm/stop_tomcat.sh desarrollo-territorial
${ROOT_PATH}/scm/stop_tomcat.sh cesvi-ws
${ROOT_PATH}/scm/stop_tomcat.sh siniestros
${ROOT_PATH}/scm/stop_tomcat.sh rusmovil
${ROOT_PATH}/scm/stop_tomcat.sh auditoria
${ROOT_PATH}/scm/stop_tomcat.sh api-rus
${ROOT_PATH}/scm/stop_tomcat.sh notificaciones
${ROOT_PATH}/scm/stop_tomcat.sh claims-ws
${ROOT_PATH}/scm/stop_tomcat.sh infoauto-ws
${ROOT_PATH}/scm/stop_tomcat.sh rus-apigateway-interno
${ROOT_PATH}/scm/stop_tomcat.sh api-crm
