#!/bin/bash -x
export DIR_LOCAL=/home/tomcat/entregas/auto
#export DIR_REMOTO=/home/tomcat/entregas/auto

#rm -Rf $DIR_LOCAL/*.war
#scp root@$HOST_REMOTO:$DIR_REMOTO/*.war $DIR_LOCAL  && ssh root@$HOST_REMOTO "rm -Rf $DIR_REMOTO/*.war"

cd $DIR_LOCAL
for war in *.war
do
        case "$war" in
    tesoreria.war) echo tesoreria
	#cp /home/tomcat/tesoreria/webapps/tesoreria*.war /home/tomcat/tesoreria/webapps/back_tesoreria_war
        rm -Rf /home/tomcat/tesoreria/webapps/tesoreria*
        cp tesoreria.war /home/tomcat/tesoreria/webapps/tesoreria.war
        rm -Rf tesoreria.war
        ;;
    emision-sis.war) echo emision
	#cp /home/tomcat/emision/webapps/emision-sis*.war /home/tomcat/emision/webapps/back_emision-sis_war
        rm -Rf /home/tomcat/emision/webapps/emision-sis*
        cp emision-sis.war /home/tomcat/emision/webapps/emision-sis.war
        rm -Rf emision-sis.war
        ;;
    notificaciones.war) echo notificaciones
	#cp /home/tomcat/notificaciones/webapps/notificaciones.war /home/tomcat/notificaciones/webapps/back_notificaciones_war
	rm -Rf /home/tomcat/notificaciones/webapps/notificaciones*
	cp notificaciones.war /home/tomcat/notificaciones/webapps/notificaciones.war
	rm -Rf notificaciones.war
        ;;
    emision-ws.war) echo emision-ws
        #cp /home/tomcat/emision-ws/webapps/emision-ws.war /home/tomcat/emision-ws/webapps/back_emision-ws_war
        rm -Rf /home/tomcat/emision-ws/webapps/emision-ws*
        cp emision-ws.war /home/tomcat/emision-ws/webapps/emision-ws.war
        rm -Rf emision-ws.war
        ;;
    nuevozk.war) echo nuevozk
	#cp /home/tomcat/emision/webapps/nuevozk.war /home/tomcat/emision/webapps/back_nuevozk_war
        rm -Rf /home/tomcat/emision/webapps/nuevozk*
        cp nuevozk.war /home/tomcat/emision/webapps/nuevozk.war
        rm -Rf nuevozk.war
        ;;
    afip-ws.war) echo afip-ws
	#cp /home/tomcat/emision/webapps/afip-ws.war /home/tomcat/emision/webapps/back_afip-ws_war
	rm -Rf /home/tomcat/emision/webapps/afip-ws*
	cp afip-ws.war /home/tomcat/emision/webapps/afip-ws.war
	rm -Rf afip-ws.war
        ;;
    siniestro.war) echo siniestro
	#cp /home/tomcat/siniestros/webapps/siniestro.war /home/tomcat/siniestros/webapps/back_siniestro_war
        rm -Rf /home/tomcat/siniestros/webapps/siniestro*
        cp siniestro.war /home/tomcat/siniestros/webapps/siniestro.war
        rm -Rf siniestro.war
        ;;
    auditoria.war) echo auditoria
	#cp /home/tomcat/auditoria/webapps/auditoria.war /home/tomcat/auditoria/webapps/back_auditoria_war
        rm -Rf /home/tomcat/auditoria/webapps/auditoria*
        cp auditoria.war /home/tomcat/auditoria/webapps/auditoria.war
        rm -Rf auditoria.war
        ;;
    contabilidad.war) echo contabilidad
	#cp /home/tomcat/contabilidad/webapps/contabilidad.war /home/tomcat/contabilidad/webapps/back_contabilidad_war
        rm -Rf /home/tomcat/contabilidad/webapps/contabilidad*
        cp contabilidad.war /home/tomcat/contabilidad/webapps/contabilidad.war
        rm -Rf contabilidad.war
        ;;
    datos-comunes.war) echo datos-comunes
	#cp /home/tomcat/datoscomunes/webapps/datos-comunes.war /home/tomcat/datoscomunes/webapps/back_datos-comunes_war
        rm -Rf /home/tomcat/datoscomunes/webapps/datos-comunes*
        cp datos-comunes.war /home/tomcat/datoscomunes/webapps/datos-comunes.war
        rm -Rf datos-comunes.war
        ;;
    desarrolloTerritorial.war) echo desarrolloTerritorial
	#cp /home/tomcat/desarrollo-territorial/webapps/desarrolloTerritorial.war /home/tomcat/desarrollo-territorial/webapps/back_desarrolloTerritorial_war
        rm -Rf /home/tomcat/desarrollo-territorial/webapps/desarrolloTerritorial*
        cp desarrolloTerritorial.war /home/tomcat/desarrollo-territorial/webapps/desarrolloTerritorial.war
        rm -Rf desarrolloTerritorial.war
        ;;
	cas.war) echo cas
	#cp /home/tomcat/cas/webapps/cas.war /home/tomcat/cas/webapps/back_cas_war
        rm -Rf /home/tomcat/cas/webapps/cas*
        cp cas.war /home/tomcat/cas/webapps/cas.war
        rm -Rf cas.war
        ;;
	portal.war) echo portal
	#cp /home/tomcat/cas/webapps/portal.war /home/tomcat/cas/webapps/back_portal_war
        rm -Rf /home/tomcat/cas/webapps/portal*
        cp portal.war /home/tomcat/cas/webapps/portal.war
        rm -Rf portal.war
        ;;
    security-sis.war) echo security-sis
	#cp /home/tomcat/cas/webapps/security-sis.war /home/tomcat/cas/webapps/back_security-sis_war
        rm -Rf /home/tomcat/cas/webapps/security-sis*
        cp security-sis.war /home/tomcat/cas/webapps/security-sis.war
        rm -Rf security-sis.war
        ;;
    procesos.war) echo 
        #cp /home/tomcat/procesos/webapps/procesos.war /home/tomcat/procesos/webapps/back_procesos_war
        rm -Rf /home/tomcat/procesos/webapps/procesos*
        cp procesos.war /home/tomcat/procesos/webapps/procesos.war
        rm -Rf procesos.war
        ;;
    rrhh.war) echo rrhh
	#cp /home/tomcat/rrhh/webapps/rrhh.war /home/tomcat/rrhh/webapps/back_rrhh_war
        rm -Rf /home/tomcat/rrhh/webapps/rrhh*
        cp rrhh.war /home/tomcat/rrhh/webapps/rrhh.war
        rm -Rf rrhh.war
        ;;
    cesvi-ws-server.war) echo cesvi-ws
        #cp /home/tomcat/cesvi-ws/cesvi-ws-server.war /home/tomcat/cesvi-ws/back_cesvi-ws-server_war
        rm -Rf /home/tomcat/cesvi-ws/webapps/cesvi-ws-server*	
        cp cesvi-ws-server.war /home/tomcat/cesvi-ws/webapps/cesvi-ws-server.war
        rm -Rf cesvi-ws-server.war
        ;;
    rusmovil.war) echo rusmovil
	#cp /home/tomcat/rusmovil/webapps/rusmovil.war /home/tomcat/rusmovil/webapps/back_rusmovil_war
        rm -Rf /home/tomcat/rusmovil/webapps/movil*
        cp rusmovil.war /home/tomcat/rusmovil/webapps/rusmovil.war
        rm -Rf rusmovil.war
        ;;
    cotizador.war) echo cotizador
	#cp /home/tomcat/rusmovil/webapps/cotizador.war /home/tomcat/rusmovil/webapps/back_cotizador_war
        rm -Rf /home/tomcat/rusmovil/webapps/cotizador*
        cp cotizador.war /home/tomcat/rusmovil/webapps/cotizador.war
        rm -Rf cotizador.war
        ;;
    claims-ws.war) echo claims-ws
        #cp /home/tomcat/claims-ws/webapps/claims-ws.war /home/tomcat/claims-ws/webapps/back_claims-ws_war
        rm -Rf /home/tomcat/claims-ws/webapps/claims-ws*
        cp claims-ws.war /home/tomcat/claims-ws/webapps/claims-ws.war
        rm -Rf claims-ws.war
        ;;
    infoauto-ws.war) echo infoauto-ws
        #cp /home/tomcat/infoauto-ws/webapps/infoauto-ws.war /home/tomcat/infoauto-ws/webapps/back_infoauto-ws_war
        rm -Rf /home/tomcat/infoauto-ws/webapps/infoauto-ws*
        cp infoauto-ws.war /home/tomcat/infoauto-ws/webapps/infoauto-ws.war
        rm -Rf infoauto-ws.war
        ;;

        *) echo otro $war
        ;;
        esac
done

chown -R tomcat.tomcat /home/tomcat
chmod 644 -R /home/tomcat/*/webapps/*.war /home/tomcat/*/*.jar
chmod 644 -R /home/tomcat/*/logs/*
