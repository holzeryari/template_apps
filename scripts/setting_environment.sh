#!/bin/bash

function show_help {
    echo "Script para cambiar las referencias correspondientes al entorno . Se debe indicar como parametros el nombre del entorno (ej: cel2 pas2 portal2)."
}

# Si no se ingresaron parametros se muestra la ayuda
if [ "$1" = "" ] || [ "$2" = "" ] || [ "$3" = "" ]
then
  show_help
  exit
fi

# Muestra el texto de ayuda y finalizar
if [ "$1" == -h ]; then
  show_help;
  exit
fi
if [ "$2" == -h ]; then
  show_help;
  exit
fi
if [ "$3" == -h ]; then
  show_help;
  exit
fi

TOMCAT_HOME="/home/tomcat"
MODULES_HOME="$TOMCAT_HOME/modulos"
PORTAL_HOME="/var/www/html"
OLD_DOMAIN="cel-n"
DOMAIN=$1
NEW_PROXY_NAME="$1.sis.rus.com.ar"
OLD_PROXY_NAME="cel.*.sis.rus.com.ar"
NEW_PORTAL_PAS_URL="$2.sis.rus.com.ar"
OLD_PORTAL_PAS_URL="pas.*.sis.rus.com.ar"
NEW_PORTAL_ASEGURADOS_URL="$3.sis.rus.com.ar"
OLD_PORTAL_ASEGURADOS_URL="portal.*.sis.rus.com.ar"
URL_PROD="https://sis.rus.com.ar"
PROD_CONF=$(find $PORTAL_HOME -name "config.json" -type f -exec grep -H '"environment": "prod"' {} \;)
PROD_MAIN=$(find $PORTAL_HOME -name "main.js" -type f -exec grep -H 'environment:"prod"' {} \;)
CELN_CONF=$(find $PORTAL_HOME -name "config.json" -type f -exec grep -H '"environment": "cel-n"' {} \;)

# Cambia el proxy name de los server.xml de todos los tomcat, actualizandolo al valor correspondiente.
echo "Changing proxy name to:" $NEW_PROXY_NAME
sed -i "s/$OLD_PROXY_NAME/$NEW_PROXY_NAME/g" $(find $MODULES_HOME -name "server.xml" -type f)
for file in  $(find $MODULES_HOME -name "server.xml");
do
        echo "file:" $file
        grep "proxyName=\"cel.*.sis.rus.com.ar" "$file"
        echo -----------------------------------------------------------------------------
done

## Modifica los valores que apuntan a produccion por el nuevo dominio. 
## (Solo si la configuracion se descarga desde artifactory)
echo "Cambia configuraciones de los portales que quedan de la compilacion"

if [[ -z "$PROD_CONF" ]]; then
  echo "PROD_CONF is empty"
elif [[ -n "$PROD_CONF" ]]; then
  sed -i 's#"environment": "prod"#"environment": \"'$DOMAIN'\"#g' $PORTAL_HOME/autogestion/assets/config.json
  sed -i 's#"prod": {#\"'$DOMAIN'"\: {#g' $PORTAL_HOME/autogestion/assets/config.json
  sed -i 's#\"url\": \"'$URL_PROD'/movil/rest/\"#\"url\": \"https://'$NEW_PROXY_NAME'/movil/rest/\"#g' $PORTAL_HOME/autogestion/assets/config.json
  sed -i 's#\"GOOGLE_ID\": \"887921717600-v2susurg880cbjsnksgfm170o9u1usho.apps.googleusercontent.com\"#\"GOOGLE_ID\": \"1001765825933-k9fmqq4bspdenpv32gl74efgedvkhtth.apps.googleusercontent.com\"#g' $PORTAL_HOME/autogestion/assets/config.json
  sed -i 's#\"FACEBOOK_ID\": \"466384660446050\"#\"FACEBOOK_ID\": \"1255895451213252\"#g' $PORTAL_HOME/autogestion/assets/config.json
  sed -i 's#\"mercadopagoKey\": \"APP_USR-4442c1d0-ab2b-47a8-9212-9a6d33219818\"#\"mercadopagoKey\": \"TEST-c0bc2fc6-ba39-40d1-b111-38373d70773d\"#g' $PORTAL_HOME/autogestion/assets/config.json
  sed -i 's#\"embeddedAvisoSiniestrosFormURL\": \"https://www.riouruguay.com.ar/siniestrosPortal/\"#\"embeddedAvisoSiniestrosFormURL\": \"http://makemake.bombieri.com.ar/siniestrosPortal/\"#g' $PORTAL_HOME/autogestion/assets/config.json
  sed -i 's#\"sugarURL\": \"https://riouruguay.sugarondemand.com/rest/v11\"#\"sugarURL\": \"https://riouruguayuat.sugarondemand.com/rest/v11\"#g' $PORTAL_HOME/autogestion/assets/config.json
  sed -i 's#\"apiRusURL\": \"'$URL_PROD'/api-rus\"#\"apiRusURL\": \"https://'$NEW_PROXY_NAME'/api-rus\"#g' $PORTAL_HOME/autogestion/assets/config.json
  sed -i 's#"environment": "prod"#"environment": \"'$DOMAIN'\"#g' $PORTAL_HOME/pas/assets/config.json
  sed -i 's#"prod": {#\"'$DOMAIN'\": {#g' $PORTAL_HOME/pas/assets/config.json
  sed -i 's#\"url\": \"'$URL_PROD'/movil/rest/\"#\"url\": \"https://'$NEW_PROXY_NAME'/movil/rest/\"#g' $PORTAL_HOME/pas/assets/config.json
fi

if [[ -z "$PROD_MAIN" ]]; then
  echo "PROD_MAIN is empty"
elif [[ -n "$PROD_MAIN" ]]; then
  sed -i 's#environment:"prod"#environment:\"'$DOMAIN'\"#g' $PORTAL_HOME/autogestion/build/main.js
  sed -i 's#prod:{url:\"'$URL_PROD'/movil/rest/\"#\"'$DOMAIN'\":{url:\"https://'$NEW_PROXY_NAME'/movil/rest/\"#g' $PORTAL_HOME/autogestion/build/main.js
  sed -i 's#GOOGLE_ID:\"887921717600-v2susurg880cbjsnksgfm170o9u1usho.apps.googleusercontent.com\"#GOOGLE_ID:\"1001765825933-k9fmqq4bspdenpv32gl74efgedvkhtth.apps.googleusercontent.com\"#g' $PORTAL_HOME/autogestion/build/main.js
  sed -i 's#FACEBOOK_ID:\"466384660446050\"#FACEBOOK_ID:\"1255895451213252\"#g' $PORTAL_HOME/autogestion/build/main.js
  sed -i 's#mercadopagoKey:\"APP_USR-4442c1d0-ab2b-47a8-9212-9a6d33219818\"#mercadopagoKey:\"TEST-c0bc2fc6-ba39-40d1-b111-38373d70773d\"#g' $PORTAL_HOME/autogestion/build/main.js
  sed -i 's#embeddedAvisoSiniestrosFormURL:\"https://www.riouruguay.com.ar/siniestrosPortal/\"#embeddedAvisoSiniestrosFormURL:\"http://makemake.bombieri.com.ar/siniestrosPortal/\"#g' $PORTAL_HOME/autogestion/build/main.js
  sed -i 's#sugarURL:\"https://riouruguay.sugarondemand.com/rest/v11\"#sugarURL:\"https://riouruguayuat.sugarondemand.com/rest/v11\"#g' $PORTAL_HOME/autogestion/build/main.js
  sed -i 's#apiRusURL:\"'$URL_PROD'/api-rus\"#apiRusURL:\"https://'$NEW_PROXY_NAME'/api-rus\"#g' $PORTAL_HOME/autogestion/build/main.js
fi

echo -----------------------------------------------------------------------------

## Modifica los valores que apuntan a cel-n por el nuevo dominio. 
## (Sirve si se utiliza la VM Template)
if [[ -z "$CELN_CONF" ]]; then
  echo "CELN_CONF is empty"
elif [[ -n "$CELN_CONF" ]]; then
  sed -i 's/"environment": "$OLD_DOMAIN"/"environment": "$DOMAIN"/g' $PORTAL_HOME/autogestion/assets/config.json
  sed -i 's/"environment": "$OLD_DOMAIN"/"environment": "$DOMAIN"/g' $PORTAL_HOME/pas/assets/config.json
  sed -i "s/$OLD_DOMAIN/$DOMAIN/g" $PORTAL_HOME/autogestion/build/main.js
  sed -i "s/$OLD_DOMAIN/$DOMAIN/g" $PORTAL_HOME/autogestion/build/main.js.map
fi

echo -----------------------------------------------------------------------------  

echo "Borra todos los logs de /home/tomcat"
rm -Rf $TOMCAT_HOME/modulos/*/logs/*
rm -Rf $TOMCAT_HOME/microservices/*/*/logs/*

echo -----------------------------------------------------------------------------
# Cambia las referencias al entorno en el archivo de configuracion de variables globales de entorno
echo "Changing environment name in conf.properties file..."
sed -i "s/$OLD_DOMAIN/$DOMAIN/g" $TOMCAT_HOME/conf/conf.properties
sed -i "s/$OLD_PORTAL_PAS_URL/$NEW_PORTAL_PAS_URL/g" $TOMCAT_HOME/conf/conf.properties
sed -i "s/$OLD_PORTAL_ASEGURADOS_URL/$NEW_PORTAL_ASEGURADOS_URL/g" $TOMCAT_HOME/conf/conf.properties

echo -----------------------------------------------------------------------------
# Cambia las referencias al entorno de portales en el archivo mis_vhost.conf
echo "Changing environment name in mis_vhost.conf file..."
sed -i "s/$OLD_PORTAL_PAS_URL/$NEW_PORTAL_PAS_URL/g" /etc/httpd/conf.modules.d/mis_vhosts.conf
sed -i "s/$OLD_PORTAL_ASEGURADOS_URL/$NEW_PORTAL_ASEGURADOS_URL/g" /etc/httpd/conf.modules.d/mis_vhosts.conf

# Configura permisos de usuario y grupo para tomcat sobre su home:
chown -R tomcat:tomcat /home/tomcat

# Cambia las referencias al entorno en las variables de los start.sh
#echo "Changing environment name in start.sh files..."
#sed -i "s/$OLD_DOMAIN/$DOMAIN/g" $(find $MODULES_HOME/* -name "start.sh" -type f)